﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Threading;
using System.Runtime.Remoting.Messaging;
namespace BSystem
{
    public class Async<V>
    {

        private Func<V> StartFunc { get; set; }
        private Action<V> EndAction { get; set; }
        private Async(Func<V> func,Action<V> endAction)
        {
            this.StartFunc = func;
            this.EndAction = endAction;            
        }
        private void Run1()
        {
            AsyncCallback callback = new AsyncCallback(some_Completed);
            this.StartFunc.BeginInvoke(callback,StartFunc);
        }
        private void some_Completed(IAsyncResult async)
        {

            // AsyncResult にキャストします。
            AsyncResult asyncResult = (AsyncResult)async;

            // AsyncDelegate で SomeDelegate のインスタンスが取得できます。
            Func<V> some = (Func<V>)asyncResult.AsyncDelegate;
            var value = some.EndInvoke(async);
            this.EndAction.BeginInvoke(value,null,null);
        }

        public static void Run<T>(Func<T> func1, Action<T> func2)
        {
            var p = new Async<T>(func1,func2);
            p.Run1();
        }
    }
}
