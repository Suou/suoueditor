﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using BSystem.Structure;
using BSystem.Collections;
namespace BSystem
{
    
    public static class IOLibrary
    {
        public static Encoding ShiftJisEncoding = Encoding.GetEncoding(932);
        
        public static string Load(string path)
        {
            string text = null;
            var fi = new FileInfo(path);
            if (fi.Exists)
            {
                using (var p = new StreamReader(path, System.Text.Encoding.GetEncoding("shift_jis")))
                {
                    text = p.ReadToEnd();
                }

                return text;
            }
            else
            {
                return null;
            }

        }
        public static string Load(string path,System.Text.Encoding encode)
        {
            string text = null;
            using (var p = new StreamReader(path, encode))
            {
                text = p.ReadToEnd();
            }
            return text;
        }
        public static IEnumerable<string> LoadLines(string path, System.Text.Encoding encode)
        {
            using (var p = new StreamReader(path, encode))
            {
                string s;
                while ((s = p.ReadLine()) != null)
                {
                    yield return s;
                }
            }
        }



        public static void Save(string text, string path)
        {
            using (var p = new StreamWriter(path))
            {
                p.Write(text);
            }
        }
        public static void Save(string text, string path, Encoding enc)
        {
            Save(text, path, enc, false);
        }
        public static void Save(string text, string path, Encoding enc,bool append)
        {
            using (var p = new StreamWriter(path, append, enc, 1024))
            {
                p.Write(text);
            }
        }
        public static void WriteFile(this IEnumerable<string> text, string path, Encoding enc = null)
        {
            enc = enc ?? Encoding.Unicode;
            using (var p = new StreamWriter(path, false, enc, 1024))
            {
                text.ForEach((v) => p.WriteLine(v));
            }
        }
        
    }

    public static class IODirectory
    {
        public static bool CheckExistDirectory(string filepath)
        {
            var p = Path.GetDirectoryName(filepath);
            var r = new DirectoryInfo(p);
            return r.Exists;
        }
        public static bool CheckExistFile(string filepath)
        {
            var r = new FileInfo(filepath);
            return r.Exists;
        }
        public static IEnumerable<FileInfo> GetAllFiles(this DirectoryInfo dir)
        {
            foreach (var finfo in dir.GetFiles())
            {
                yield return finfo;
            }
            foreach (var subdir in dir.GetDirectories())
            {
                foreach (var subfinfo in GetAllFiles(subdir))
                {
                    yield return subfinfo;
                }
            }
        }
        public static IEnumerable<FileInfo> WhereExtension(this IEnumerable<FileInfo> items, string[] exts)
        {
            return items.Where((v) => exts.Contains(Path.GetExtension(v.Name)));
        }
        public static IEnumerable<FileInfo> WhereImage(this IEnumerable<FileInfo> items)
        {
            return items.WhereExtension(new[] { ".jpg", ".bmp", "png" });
        }


        public static void CreateFolder(string[] directories, string baseDic)
        {


            var dics = directories.GetSuffixes().Reverse().Select((v) => baseDic + "/" + String.Join("/", v));
            IEnumerable<DirectoryInfo> infos = null;
            try
            {
                infos = dics.Select((v) => new DirectoryInfo(v));
                foreach (var info in infos)
                {
                    if (!info.Exists)
                    {
                        var fi = new FileInfo(info.FullName);
                        if (!fi.Exists) info.Create();
                    }
                }
            }
            catch 
            {
                directories.WriteLine();
            }
            finally
            {
            }
        }

    }
}
