﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem
{
    public class LockAction
    {
        private bool _Lock = false;
        public bool Lock
        {
            get { return this._Lock; }
            set
            {
                this._Lock = value;
                if (!this._Lock && this.IsHavingLockedAction)
                {
                    this.Action();
                    this.IsHavingLockedAction = false;
                }
            }
        }
        public Action Action { get; private set; }
        public bool IsHavingLockedAction { get; set; }
        public LockAction(Action ac)
        {
            this.Action = ac;
        }
        public virtual void Do()
        {
            if (!this.Lock)
            {
                this.Action();
                this.Lock = true;
            }
            else
            {
                this.IsHavingLockedAction = true;
            }
        }
    }
    public class LockActionWindowsTimer:LockAction
    {
        public System.Windows.Forms.Timer Timer = new System.Windows.Forms.Timer() { Enabled = true,Interval = 1000};
        public LockActionWindowsTimer(Action ac,int interval)
            : base(ac)
        {
            this.Timer.Interval = interval;
            this.Timer.Tick += this.Ticked;
            this.Timer.Start();
            
        }
        protected virtual void Ticked(object sender, EventArgs e)
        {
            this.Lock = false;
        }
    }
    public class DelayLockActionWindowsTimer : LockActionWindowsTimer
    {
        public DelayLockActionWindowsTimer(Action ac,int interval):base(ac,interval)
        {
        }
        public override void Do()
        {
            if (!this.Lock)
            {
                this.IsHavingLockedAction = true;
                this.Lock = true;
            }
            this.Timer.Stop();
            this.Timer.Start();
        }
        protected override void Ticked(object sender, EventArgs e)
        {
            base.Ticked(sender, e);
            this.Timer.Stop();
        }
    }
}
