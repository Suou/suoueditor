﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BSystem
{
    public static class StringExtension
    {
        public static void Preview(this string text)
        {
            Preview(text, "temp.txt");
        }
        public static void Preview(this string text, string path)
        {
            using (var f = new StreamWriter(path, false, System.Text.Encoding.UTF8))
            {
                f.Write(text);
            }
            System.Diagnostics.Process.Start("notepad.exe", path);
        }
    }
}
