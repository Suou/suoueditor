﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.IO;


namespace BSystem.IO
{

    public interface IByteItem : IKumikomiMemoryItem<IByteItem>
    {
    }
    public interface IKumikomiMemoryItem<KumikomiType>
    {
        byte[] ToBytes();
        int ByteLength { get; }
        IKumikomiMemoryItem<KumikomiType> CreateItem(byte[] byteitem);
        IKumikomiMemoryItem<KumikomiType> CreateItem(KumikomiType byteitem);
        KumikomiType Value { get; }
        //IKumikomiMemoryItem<KumikomiType> CreateItem(byte[] byteitem);
        //int Compare(MemoryItem<HashType> a, MemoryItem<HashType> b);
    }

    public static class ILongListExtension
    {
        public static T[] BinarySearch<T>(this IReadOnlyLongArray<T> item, T hash) where T : IComparer<T>
        {
            List<T> r = new List<T>();
            var max = item.Length;
            long left = 0, pointer = max / 2, right = max - 1;
            while (!(left == pointer || pointer == right))
            {
                var value = item[pointer];
                if (value.Compare(value, hash) < 0)
                {
                    left = pointer;
                    pointer = (left + right) / 2;
                }
                else if (value.Compare(value, hash) > 0)
                {
                    right = pointer;
                    pointer = (left + right) / 2;
                }
                else
                {
                    r.Add(value);
                    foreach (var i in pointer.PlusCountUp()) { if (i >= max)break; var v = item[i]; if (v.Compare(v, hash) == 0) { r.Add(v); } else { break; } }
                    foreach (var i in pointer.MinusCountUp()) { if (i <= 0)break; var v = item[i]; if (v.Compare(v, hash) == 0) { r.Add(v); } else { break; } }
                    break;
                }
            }
            return r.ToArray();
        }
    }
}
