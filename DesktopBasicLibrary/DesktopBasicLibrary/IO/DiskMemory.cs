﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.IO;

namespace BSystem.IO
{


    public abstract class AbstractDiskFixedMemory<T> : IReadOnlyLongArray<T>
    {
        private FileInfo Info { get; set; }
        private FileStream Filer { get; set; }
        private int ArrayLength { get; set; }

        public abstract T CreateItem(byte[] item);
        public abstract byte[] TranslateByte(T item);
        public abstract byte[] TranslateByte(T[] item);

        public int ItemByteLength { get; set; }
        

        public AbstractDiskFixedMemory(FileInfo info, int byteLength)
        {
            this.Info = info;
            this.ItemByteLength = byteLength;
            //this.DefaultItem = new T();
            this.ArrayLength = int.MaxValue / this.ItemByteLength;
            this.Filer = this.Info.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite);
            if (!this.Info.Exists) throw new Exception();
        }
        private void Open(FileMode mode)
        {

            this.Filer = this.Info.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite);
        }
        public void Close()
        {
            this.Filer.Close();
        }

        public void Append(T item)
        {
            //var item2 = this.TranslateByte(item);
            //this.Open(FileMode.Append);
            var p = this.TranslateByte(item);
            this.Filer.Seek(0, SeekOrigin.End);
            Filer.Write(p, 0, p.Length);
        }
        public void Append(IEnumerable<T> items)
        {

            //this.Open(FileMode.Append);
            var p = items.SelectMany((v)=>this.TranslateByte(v)).ToArray();
            this.Filer.Seek(0, SeekOrigin.End);
            Filer.Write(p, 0, p.Length);
        }
        public T this[long i]
        {
            get
            {
                //this.Open(FileMode.Open);
                byte[] r = new byte[this.ItemByteLength];
                Filer.Seek(this.ItemByteLength * i, SeekOrigin.Begin);
                var n = Filer.Read(r, 0, this.ItemByteLength);
                return this.CreateItem(r);
            }
            set
            {
                this[i, 1] = new T[] { value };
            }
        }
        public T[] this[long nth, int length]
        {
            get
            {
                //this.Open(FileMode.Open);
                byte[] r = new byte[this.ItemByteLength * length];
                Filer.Seek(this.ItemByteLength * nth, SeekOrigin.Begin);
                var n = Filer.Read(r, 0, this.ItemByteLength * length);
                return this.Translate(this.Translate(r, length));
            }
            set
            {
                var p = value.SelectMany((v) => this.TranslateByte(v)).ToArray();
                var start = (int)nth*this.ItemByteLength;
                this.Filer.Seek(start, SeekOrigin.Begin);
                this.Filer.Write(p, 0, p.Length);
            }
        }
        private int BufferSize = 10000000;
        public void Set(long pos, IEnumerable<T> items)
        {
            var bytePos = pos * this.ItemByteLength;
            this.Filer.Seek(bytePos, SeekOrigin.Begin);

            int localPos = 0;
            T[] buffer = new T[this.BufferSize];
            
            foreach (var v in items)
            {
                buffer[localPos++] = v;
                if (localPos == buffer.Length)
                {
                    var byteBuffer = this.TranslateByte(buffer);
                    this.Filer.Write(byteBuffer, 0, byteBuffer.Length);
                    localPos = 0;
                }
            }
            if (localPos != 0)
            {
                var byteBuffer = this.TranslateByte(buffer.Take(localPos).ToArray());
                this.Filer.Write(byteBuffer, 0, byteBuffer.Length);
                localPos = 0;
            }
        }

        private byte[][] Translate(byte[] item, int length)
        {
            var r = new byte[length][];
            for (int i = 0; i < length; i++)
            {
                r[i] = new byte[this.ItemByteLength];
                for (int t = 0; t < this.ItemByteLength; t++) r[i][t] = item[i * this.ItemByteLength + t];
            }
            return r;
        }
        private T[] Translate(byte[][] item)
        {
            var len = item.Length;
            var r = new T[len];
            for (int i = 0; i < len; i++)
            {
                r[i] = this.CreateItem(item[i]);
            }
            return r;
        }
        public IEnumerator<T> GetEnumerator()
        {
            foreach (var v in this.GetCollection()) yield return v;
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var v in this.GetCollection()) yield return v;
        }
        public IEnumerable<T> GetCollection()
        {
            var nth = 0;
            while (nth < this.Length)
            {
                var len = nth + ArrayLength > this.Length ? (int)(this.Length - nth) : ArrayLength;
                var p = this[nth, len];
                for (int i = 0; i < len; i++) yield return p[i];
                nth += len;
            }
        }
        public long Length
        {
            get
            {

                return this.Filer.Length / this.ItemByteLength;
            }
        }
        public void Delete()
        {
            this.Close();
            this.Info.Delete();
        }
        protected void Clean()
        {
            this.Filer.SetLength(0);
        }
        public void Set(long length)
        {
            this.Clean();
            this.Filer.SetLength(length);
            
        }
        /*
        public static DiskMemory<T,NthType> Create(DirectoryInfo directory, string name)
        {
            return null;
        }
        */
    }
    public class StructDiskMemory<T> : AbstractDiskFixedMemory<T>
    {
        public StructDiskMemory(FileInfo info, IKumikomiMemoryItem<T> item):base(info,item.ByteLength)
        {
            this.DefaultItem = item;
        }
        private IKumikomiMemoryItem<T> DefaultItem { get; set; }
        public override T CreateItem(byte[] item)
        {
            return this.DefaultItem.CreateItem(item).Value;
        }
        public override byte[] TranslateByte(T item)
        {
            var p = this.DefaultItem.CreateItem(item);
            return p.ToBytes();
        }
        public override byte[] TranslateByte(T[] item)
        {
            var r = item.Select((v)=>this.DefaultItem.CreateItem(v)).SelectMany((v)=>v.ToBytes()).ToArray();
            return r;

        }
    }

    public class IntDiskMemory : AbstractDiskFixedMemory<int>
    {
        public IntDiskMemory(FileInfo info):base(info,sizeof(int))
        {
            
        }
        public override int CreateItem(byte[] item)
        {
            return BitConverter.ToInt32(item,0);
        }
        public override byte[] TranslateByte(int item)
        {
            return BitConverter.GetBytes(item);
        }
        public override byte[] TranslateByte(int[] item)
        {
            var r = new byte[item.Length * this.ItemByteLength];
            var pos = 0;
            for (int i = 0; i < item.Length; i++)
            {
                var p = BitConverter.GetBytes(item[i]);
                Array.Copy(p, 0, r, pos, p.Length);
                pos += this.ItemByteLength;
            }
            return r;
        }
    }

    
}
