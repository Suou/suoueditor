﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
using System.IO;
namespace BSystem.IO
{
    public class BoolDiskMemory : ILongArray<bool>
    {
        private FileInfo Info { get; set; }
        public FileStream Filer { get; set; }
        
        public BoolDiskMemory(FileInfo info,int len)
        {
            this.Info = info;
            this.Filer = this.Info.Open(FileMode.OpenOrCreate,FileAccess.ReadWrite);
            this.Filer.SetLength((len / 8) + 1);
        }
        public void Close()
        {
            this.Filer.Close();
        }
        public byte Get(long i)
        {
            this.Filer.Seek(i, SeekOrigin.Begin);
            return (byte)this.Filer.ReadByte();
        }
        public long Length
        {
            get
            {
                return this.Filer.Length * 8;
            }
        }

        public bool this[long i]
        {
            get
            {
                var byteIndex = i / 8;
                var bitIndex = (int)(i % 8);
                var p = this.Get(byteIndex);
                return ByteExtension.GetBit(p, bitIndex);
            }
            set
            {
                var byteIndex = i / 8;
                var bitIndex = (int)(i % 8);
                var p = this.Get(byteIndex);
                var newP = value ? (byte)(p | ByteExtension.BitIndexArray[bitIndex]) : (byte)(p & ByteExtension.InverseIndexArray[bitIndex]);

                this.Filer.Seek(byteIndex, SeekOrigin.Begin);
                this.Filer.WriteByte(newP);
            }
        }
        public bool this[long index,long len]
        {
            get
            {
                throw new Exception();
                /*
                var byteIndex = index / 8;
                var byteLen = len / 8;

                var bitIndex = (int)(index % 8);
                var bitLen = (int)len % 8;
                if (bitIndex != 0 && bitLen != 0) throw new Exception();

                bool[] r = new bool[len];
                this.Filer.Seek(byteIndex, SeekOrigin.Begin);
                this.Filer.Read(newP);
                */
            }
        }
        public IEnumerator<bool> GetEnumerator()
        {
            foreach (var v in this.GetCollection()) yield return v;
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            foreach (var v in this.GetCollection()) yield return v;
        }
        public IEnumerable<bool> GetCollection()
        {
            var nth = 0;
            while (nth < this.Length)
            {
                yield return this[nth];
                nth++;
            }
        }
    }
}
