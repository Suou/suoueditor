﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.IO;
namespace BSystem.Text
{
    public class FileTextItem : ITextItem
    {
        public FileInfo Info { get; set; }
        public System.Text.Encoding Encoding { get; set; }
        public string Name { get; private set; }
        public virtual string Text
        {
            get
            {
                return BSystem.IOLibrary.Load(this.Info.FullName, this.Encoding);
            }
        }
        public FileTextItem(FileInfo info, System.Text.Encoding encode)
        {
            this.Info = info;
            this.Encoding = encode;
            this.Name = info.Name;
        }
    }

    public class TextItem : ITextItem
    {
        public string Name { get; set; }
        public string Text { get; set; }
    }
}
