﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BSystem.Controls
{
    public partial class DockingPanelForm : SerializableForm
    {
        public DockingPanelForm()
        {
            InitializeComponent();
        }
        public DockingPanelForm(Control cc)
        {
            this.Controls.Add(cc);
            cc.Dock = DockStyle.Fill;
        }
        public Control HoldingItem { get; set; }
        public void RemoveHoldingItem()
        {
            this.Controls.Remove(this.HoldingItem);
        }
        public void SetHoldingItem()
        {
            this.Controls.Add(this.HoldingItem);
            this.HoldingItem.Dock = DockStyle.Fill;
        }
        public bool IsHolding
        {
            get { return this.Controls.Contains(this.HoldingItem); }
        }
        private void DockingPanelForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Visible = false;
                e.Cancel = true;
            }
        }
    }
}
