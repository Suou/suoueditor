﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Windows.Forms;
using System.Drawing;
namespace BSystem.Controls
{
    [Serializable()]
    public class FormSaveData
    {
        public Point Location;
        public Size Size;
        public FormWindowState WindowState;

        public FormSaveData()
        {
        }
        public FormSaveData(Form f)
        {
            this.Location = f.Location;
            this.Size = f.Size;
            this.WindowState = f.WindowState;
        }
        public static FormSaveData Create(Form f)
        {
            return new FormSaveData(f);
        }
        public void SetData(Form f)
        {
            f.Location = this.Location;
            f.Size = this.Size;
            f.WindowState = this.WindowState;
        }
    }
}
