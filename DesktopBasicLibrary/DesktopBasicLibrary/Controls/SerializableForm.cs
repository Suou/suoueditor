﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BSystem.Controls
{
    public partial class SerializableForm : Form
    {
        public SerializableForm()
        {
            InitializeComponent();
        }
        private Size HoldingSize { get; set; }
        public void SetHolding()
        {
            this.Size = this.HoldingSize;
            this.Location = this.HoldingLocation;            
        }
        private Point HoldingLocation { get; set; }
        private void TextEditorForm_SizeChanged(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Minimized) this.HoldingSize = this.Size;
        }
        private bool IsSettingSeted { get; set; }
        private bool IsFirstLocationChanged { get; set; }
        private void TextEditorForm_VisibleChanged(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Minimized) this.HoldingSize = this.Size;
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public virtual FormSaveData Setting
        {
            get
            {
                var p = new FormSaveData()
                {
                    Size = this.HoldingSize,
                    WindowState = this.WindowState,
                    Location = this.IsFirstLocationChanged || this.IsSettingSeted ? this.HoldingLocation : this.Location,
                };

                if (this.WindowState == FormWindowState.Minimized) p.WindowState = FormWindowState.Normal;
                return p;
            }
            set
            {
                if (value == null) return;
                this.Location = value.Location;
                this.HoldingLocation = value.Location;
                this.Size = value.Size;
                this.WindowState = value.WindowState;
                this.IsSettingSeted = true;
            }
        }

        private void SerializableForm_LocationChanged(object sender, EventArgs e)
        {
            if (this.WindowState != FormWindowState.Minimized && this.Visible)
            {
                this.HoldingLocation = this.Location;
                
                this.IsFirstLocationChanged = true;
            }
        }

        private void SerializableForm_Load(object sender, EventArgs e)
        {
            if (this.IsSettingSeted) this.Location = this.HoldingLocation;

        }
    }
}
