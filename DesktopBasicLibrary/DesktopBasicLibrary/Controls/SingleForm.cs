﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BSystem.Controls
{
    public partial class SingleForm : SerializableForm
    {
        public SingleForm()
        {
            InitializeComponent();
        }
        public SingleForm(Control c,string title):this()
        {
            this.Controls.Add(c);
            c.Dock = DockStyle.Fill;
            this.Text = title;
        }
    }
}
