﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
namespace BSystem.Controls.Interface
{
    public interface IDragMovableControl
    {
        bool IsDrag { get; set; }
        Point BasicLocation { get; set; }
        /*
        MouseEventHandler MouseDown;
        MouseEventHandler MouseMove;
        MouseEventHandler MouseUp;
        */
    }
    public static class IDragMovableControlExtension
    {
        public static void AppendMovale(this IDragMovableControl control)
        {
            var c = control as Control;
            c.MouseDown += IDragMovableControlExtension.Mouse_Down;
            c.MouseMove += IDragMovableControlExtension.Mouse_Move;
            c.MouseUp += IDragMovableControlExtension.Mouse_Up; 
        }
        private static void Mouse_Down(object sender, MouseEventArgs e)
        {
            var cont = sender as IDragMovableControl;
            var cont2 = sender as Control;
            if (e.Button == MouseButtons.Left)
            {
                cont.IsDrag = true;
                //cont.BasicLocation = new Point(cont2.Location.X + e.X, cont2.Location.Y + e.Y);
                cont.BasicLocation = e.Location;
            }
        }
        private static void Mouse_Move(object sender, MouseEventArgs e)
        {
            var cont = sender as IDragMovableControl;
            var cont2 = sender as Control;

            if (e.Button == MouseButtons.Left)
            {
                var p = new Point(e.X - cont.BasicLocation.X, e.Y - cont.BasicLocation.Y);
                cont2.Location = new Point(cont2.Location.X + p.X, cont2.Location.Y + p.Y);
            }
        }
        private static void Mouse_Up(object sender, MouseEventArgs e)
        {
            var cont = sender as IDragMovableControl;
            if (e.Button == MouseButtons.Left)
            {
                cont.IsDrag = false;
            }
        }

    }
}
