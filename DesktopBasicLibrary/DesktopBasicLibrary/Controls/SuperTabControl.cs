﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
namespace BSystem.Controls
{
    public partial class SuperTabControl : TabControl
    {
        public SuperTabControl()
        {
            InitializeComponent();
            this.MouseDown += this.tabControl1_MouseDown;

        }

        public SuperTabControl(IContainer container)
        {
            container.Add(this);
            
            InitializeComponent();
            this.MouseDown += this.tabControl1_MouseDown;
        }
        public ContextMenuStrip TabPageContextMenuStrip { get; set; }
        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                for (int i = 0; i < this.TabCount; i++)
                {
                    //タブとマウス位置を比較し、クリックしたタブを選択
                    if (this.GetTabRect(i).Contains(e.X, e.Y))
                    {
                        if (this.TabPageContextMenuStrip != null)
                        {
                            this.TabPageContextMenuStrip.Tag = this.TabPages[i]; 
                            this.TabPageContextMenuStrip.Show(this, e.Location);
                            
                        }
                        this.SelectedTab = this.TabPages[i];
                        break;
                    }
                }
            }
        }
    }
}
