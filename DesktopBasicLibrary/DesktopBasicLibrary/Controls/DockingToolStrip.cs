﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using BSystem.Collections;
using BSystem.Windows;
namespace BSystem.Controls
{
    public class DockingToolStrip:ToolStrip
    {

        #region Constructor
        public DockingToolStrip()
            : base()
        {
            this.InitializeComponent();
            this.InfoDic = new SerializableDictionary<string, DockingPanelInfo>();
            this._ContainerItems = new ObservableCollection<Control>();
            this.BorderSplitter = new Splitter();
            this.BorderSplitter.SplitterMoved += (a, b) => this.OpeningControlInfo.ControlSetting.Size = this.OpeningControlInfo.ControlSetting.Panel.Size;

        }
        #endregion
        #region Property

        private int _SensibleDistance = 50;
        public int SensibleDistance
        {
            get { return this._SensibleDistance; }
            set
            {
                this._SensibleDistance = value;
            }
        }
        private DockingPanelInfo _OpeningControlInfo = null;
        private DockingPanelInfo OpeningControlInfo
        {
            get { return this._OpeningControlInfo; }
            set
            {
                var info = value;
                if (this._OpeningControlInfo == info) return;
                if (this._OpeningControlInfo != null)
                {
                    this.Parent.Controls.Remove(this._OpeningControlInfo.ControlSetting.Panel);
                    if(this.OpeningControlInfo != null)this._OpeningControlInfo.ControlSetting.Opening = false;
                    this._OpeningControlInfo = null;

                }
                this._OpeningControlInfo = value;
                this.SetControl();
                this.SetSplitter();
            }
        }
        public bool isEternalOpening
        {
            get { return true;  }
        }
        private bool IsSplitting { get; set; }
        public bool IsAutoOpenAndShutAboutMouseMove { get; set; }
        private ToolStripMenuItem AutoOpenFlagMenuItem1;

        private Control _InnerControl = null;
        public Control InnerControl
        {
            get { return this._InnerControl; }
            set
            {
                this._InnerControl = value;
            }
        }
        private Splitter _BorderSplitter = null;
        public Splitter BorderSplitter
        {
            get { return this._BorderSplitter; }
            set
            {
                this._BorderSplitter = value;
                if (value != null)
                {
                    this._BorderSplitter.SplitterMoving += this.StartSplit;
                    this._BorderSplitter.SplitterMoved += this.EndSplit;
                }
            }
        }
        private SerializableDictionary<string, DockingPanelInfo> _InfoDic = null;

        public SerializableDictionary<string, DockingPanelInfo> InfoDic
        {
            get
            {
                if (_InfoDic != null) _InfoDic.Values.Where((v) => v.Form != null).ForEach((v) => v.PreDispose());
                return this._InfoDic;
            }
            set
            {
                if (value == null) return;
                this._InfoDic = value;
            }
        }
        private ContextMenuStrip contextMenuStrip1;
        private System.ComponentModel.IContainer components;
        private Timer timer1;

        [NonSerialized]
        private ObservableCollection<Control> _ContainerItems;
        private ObservableCollection<Control> ContainerItems
        {
            get { return this._ContainerItems; }
        }

        public Rectangle OpeningAndToolStripRectangle
        {
            get
            {
                if (this.OpeningControlInfo != null && !this.OpeningControlInfo.IsForm)
                {
                    if (this.Dock == DockStyle.Right)
                    {
                        var p = this.OpeningControlInfo.ControlSetting.Panel.OnFormRectangle();
                        var sz = this.Size;
                        return new Rectangle(p.X, p.Y, p.Width + sz.Width, sz.Height);
                    }
                    else if (this.Dock == DockStyle.Top)
                    {
                        throw new NotImplementedException();
                    }
                    else if (this.Dock == DockStyle.Left)
                    {
                        throw new NotImplementedException();
                    }
                    else if (this.Dock == DockStyle.Bottom)
                    {
                        throw new NotImplementedException();
                    }
                    else
                    {
                        return Rectangle.Empty;
                    }
                }
                else
                {
                    return Rectangle.Empty;
                }
            }
        }
        public bool IsOpen
        {
            get { return this.OpeningControlInfo != null; }
        }
        
        

        #endregion

        #region Event

        private void StartSplit(object sender, EventArgs e)
        {
            this.IsSplitting = true;
        }
        private void EndSplit(object sender, EventArgs e)
        {
            this.IsSplitting = false;
        }
        private void Panel_Leave(object sender, EventArgs e)
        {
            if (this.OpeningControlInfo != null)
            {
                if(this.OpeningControlInfo.IsFloating && !this.isEternalOpening)this.CloseOpeningPanel();
            }
        }
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.IsAutoOpenAndShutAboutMouseMove = !this.IsAutoOpenAndShutAboutMouseMove;
        }

        private void contextMenuStrip1_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.AutoOpenFlagMenuItem1.Checked = this.IsAutoOpenAndShutAboutMouseMove;
            
        }

        private void DockingToolStrip_MouseClick(object sender, MouseEventArgs e)
        {
            /*
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                this.contextMenuStrip1.Show();
            }
            */
        }

        private void ToolStripMenuItem_Clicked(object c, EventArgs e)
        {
            var ctrl = (Control)((ToolStripMenuItem)c).Tag;
            var item = this.InfoDic[ctrl.Name];
            if (item.IsForm)
            {
                if (item.HasOwner)
                {
                    if (item.Form.Visible)
                    {
                        item.Form.Visible = false;
                    }
                    else
                    {
                        if (item.Form.WindowState == FormWindowState.Minimized)
                        {
                            item.Form.WindowState = FormWindowState.Normal;
                            item.Form.SetHolding();
                        }
                        item.Form.Show();
                    }
                }
                else
                {
                    if (item.Form.Visible)
                    {
                        item.Form.Focus();
                    }
                    else
                    {
                        if (item.Form.WindowState == FormWindowState.Minimized)
                        {
                            item.Form.WindowState = FormWindowState.Normal;
                            item.Form.SetHolding();
                        }
                        item.Form.Show();
                    }
                }
            }
            else
            {
                this.ChangeMode(item);
                item.IsFloating = true;
            }
        }
        private void ToolStripMenuItem_MouseHover(object c, EventArgs e)
        {
            var ctrl = (Control)((ToolStripMenuItem)c).Tag;
            var item = this.InfoDic[ctrl.Name];
            if (item.IsForm || !this.IsAutoOpenAndShutAboutMouseMove || this.OpeningControlInfo != null || this.OpeningControlInfo == item)
            {
            }
            else
            {
                if (this.OpeningControlInfo != item)
                {
                    this.ChangeMode(item);
                }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.OpeningControlInfo != null && !this.OpeningControlInfo.IsForm && this.IsAutoOpenAndShutAboutMouseMove && !this.IsSplitting)
            {
                var p = this.Parent.PointToClient(Control.MousePosition);
                var dis = (int)PointExtension.GetDistance(p, this.OpeningAndToolStripRectangle);
                if (dis > this.SensibleDistance)
                {
                    this.CloseOpeningPanel();
                }
            }

        }
        #endregion

        #region Method

        private void SetSplitter()
        {
            if (this.OpeningControlInfo != null)
            {
                this.Parent.Controls.Add(this.BorderSplitter);

                this.BorderSplitter.Dock = DockStyle.Right;
                this.BorderSplitter.BringToFront();
            }
            else
            {
                this.Parent.Controls.Remove(this.BorderSplitter);
            }
        }
        public void AddControl(Control c, string name,Size defaultSize,bool isDefaultForm,Icon icon)
        {
            
            var ctrl = c;
            this.ContainerItems.Add(c);
            if (!this.InfoDic.ContainsKey(ctrl.Name))
            {
                var controlSetting = new DockingPanlelControlInfo() { Size = defaultSize };
                var formSetting = new FormSaveData() { Size = defaultSize};
                
                this.InfoDic[ctrl.Name] = new DockingPanelInfo() 
                { IsForm = isDefaultForm, Name = name, ControlSetting = controlSetting,FormSetting = formSetting,HasOwner = false };
            }
            var info = this.InfoDic[ctrl.Name];
            if (info.IsFloating) info.ControlSetting.Opening = false;
           
            info.ControlSetting.Panel = new DockingPanel() { HoldingItem = ctrl};
            info.SetForm(new DockingPanelForm() { HoldingItem = ctrl,Text = info.Name ,Owner = info.HasOwner ? this.FindForm() : null,Icon = icon});

            this.SetItem(info);
            this.SetToolStripMenuItem();
        }
        private void SetToolStripMenuItem()
        {
            this.Items.Clear();
            foreach (var v in this.ContainerItems)
            {
                var info = this.InfoDic[v.Name];
                var p = new ToolStripMenuItem() { Text = info.Name,Tag = v };
                p.Font = this.Font;
                p.TextDirection = ToolStripTextDirection.Vertical90;
                p.Click += this.ToolStripMenuItem_Clicked;
                p.MouseHover += this.ToolStripMenuItem_MouseHover;
                p.BackColor = Color.LightGreen;
                p.Margin = new Padding(0, 1, 0, 1);
                p.MouseDown += this.StripItem_MouseDown;
                this.Items.Add(p);
            }
        }
        private void SetControl()
        {
            if (this.OpeningControlInfo != null)
            {
                var panel = this.OpeningControlInfo.ControlSetting.Panel;
                panel.Leave += this.Panel_Leave;
                panel.Visible = false;
                //panel.isAdding = true;
                this.Parent.Controls.Add(panel);
                panel.Visible = true;

                //panel.isAdding = false;

                this.OpeningControlInfo.ControlSetting.Panel.Size = this.OpeningControlInfo.ControlSetting.Size;
                this.OpeningControlInfo.ControlSetting.Panel.Dock = DockStyle.Right;
                

                this._OpeningControlInfo.ControlSetting.Opening = true;
                this.OpeningControlInfo.ControlSetting.Panel.Visible = true;
                this.BringToFront();
                this.OpeningControlInfo.ControlSetting.Panel.BringToFront();
                if (this.OpeningControlInfo.IsFloating)
                {
                    this.InnerControl.Dock = DockStyle.None;
                    if (this.Dock == DockStyle.Right)
                    {
                        this.InnerControl.Size = new Size(this.Parent.Width-this.Width,this.Parent.Height);
                    }
                    else
                    {

                    }
                    this.InnerControl.Anchor = AnchorStyles.Bottom | AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;

                }
                else
                {
                    this.InnerControl.Dock = DockStyle.Fill;
                    //this.InnerControl.Anchor = AnchorStyles.None;
                    this.InnerControl.BringToFront();
                }
                this.OpeningControlInfo.ControlSetting.Panel.Focus();

            }
        }
        private void ChangeMode(DockingPanelInfo c)
        {
            if (this.OpeningControlInfo == c)
            {
                this.OpeningControlInfo = null;
            }
            else
            {
                this.OpeningControlInfo = c;
            }
        }
        private void CloseOpeningPanel()
        {
            if (this.OpeningControlInfo != null)
            {
                this.ChangeMode(this.OpeningControlInfo);
            }
        }

        private void SetItem(DockingPanelInfo c)
        {
            if (c.IsForm)
            {
                if (!c.Form.IsHolding)
                {
                    c.ControlSetting.Panel.RemoveHoldingItem();
                    c.Form.SetHoldingItem();
                }
                if (c.Form.Visible)
                {
                    c.Form.Visible = false;
                }
                else
                {
                    c.Form.Show();
                    c.Form.BringToFront();
                }
            }
            else
            {
                if (!c.ControlSetting.Panel.IsHolding)
                {
                    c.Form.RemoveHoldingItem();
                    c.ControlSetting.Panel.SetHoldingItem();
                }
                if (c.ControlSetting.Opening)
                {
                    this.OpeningControlInfo = c;
                }
            }
        }
        public void LoadSetting(string path)
        {
            this.ContainerItems.Clear();
            this.InfoDic.Clear();
            this.InfoDic = BSystem.XML.NonBinarySerializer.Load<SerializableDictionary<string, DockingPanelInfo>>(path)
                ?? new SerializableDictionary<string, DockingPanelInfo>();

        }
        public void SaveSetting(string path)
        {
            BSystem.XML.NonBinarySerializer.Save(path, this.InfoDic);
        }
        #endregion
        #region Event2
        private void StripItem_MouseDown(object sender , MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                var tag = (sender as ToolStripMenuItem).Tag as Control;
                var p = this.InfoDic[tag.Name];
                var context = new ContextMenuStrip();
                var title = p.IsForm ? "ドッキングパネルとして開く" : "別ウインドウで開く";
                var item = new ToolStripMenuItem() { Text = title , Tag = tag };
                item.Click += this.StripItem_FormItemClicked;
                context.Items.Add(item);


                var item2 = new ToolStripMenuItem() { Text = "固定", Tag = tag, Checked = !p.IsFloating, Enabled = !p.IsForm };
                var item3 = new ToolStripMenuItem() { Text = "常に手前に表示", Tag = tag, Checked = p.Form.Owner != null, Enabled = p.IsForm };
                item2.Click += this.StripItem_KoteiItemClicked;
                item3.Click += this.StripItem_TopMostClicked;
                context.Items.Add(item2);
                context.Items.Add(item3);
                context.Show(Control.MousePosition);
            }
        }
        private void StripItem_KoteiItemClicked(object sender, EventArgs e)
        {
            var tag = (sender as ToolStripMenuItem).Tag as Control;
            var p = this.InfoDic[tag.Name];
            if (p.IsForm)
            {
            }
            else
            {
                if (this.OpeningControlInfo == p)
                {
                    this.OpeningControlInfo = null;
                    p.IsFloating = false;
                    this.OpeningControlInfo = p;
                }
                else
                {
                    p.IsFloating = false;
                    this.OpeningControlInfo = p;
                }
            }
        }

        private void StripItem_TopMostClicked(object sender, EventArgs e)
        {
            var tag = (sender as ToolStripMenuItem).Tag as Control;
            var p = this.InfoDic[tag.Name];
            if (p.IsForm)
            {
                if (p.Form.Owner == null)
                {
                    p.Form.Owner = this.FindForm();
                    p.HasOwner = true;
                }
                else
                {
                    p.Form.Owner = null;
                    p.HasOwner = false;
                }
            }
        }
        private void StripItem_FormItemClicked(object sender, EventArgs e)
        {
            var tag = (sender as ToolStripMenuItem).Tag as Control;
            var p = this.InfoDic[tag.Name];
            if (p.IsForm)
            {
                if (p.Form.Visible) p.Form.Visible = false;
                p.Form.RemoveHoldingItem();
                p.ControlSetting.Panel.SetHoldingItem();
                this.OpeningControlInfo = p;
                
            }
            else
            {
                if (p.ControlSetting.Opening) this.OpeningControlInfo = null;
                p.ControlSetting.Panel.RemoveHoldingItem();
                p.Form.SetHoldingItem();
                if (p.Form.WindowState == FormWindowState.Minimized)
                {
                    p.Form.WindowState = FormWindowState.Normal;
                    p.Form.SetHolding();
                }
                p.Form.Show();
            }
            p.IsForm = !p.IsForm;
            
        }
        #endregion

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.AutoOpenFlagMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AutoOpenFlagMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // AutoOpenFlagMenuItem1
            // 
            this.AutoOpenFlagMenuItem1.Name = "AutoOpenFlagMenuItem1";
            this.AutoOpenFlagMenuItem1.Size = new System.Drawing.Size(124, 22);
            this.AutoOpenFlagMenuItem1.Text = "自動開閉";
            this.AutoOpenFlagMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // DockingToolStrip
            // 
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.DockingToolStrip_MouseClick);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }






    }
    enum OpeningType
    {
        None, Floating, Form, Kotei
    }
}
