﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BSystem.Windows;

namespace BSystem.Controls
{
    [ToolboxItem(false)]
    public partial class DockingPanel : Panel
    {
        public DockingPanel()
        {
            InitializeComponent();
        }
        public DockingPanel(Control cc):base()
        {
            this.Controls.Add(cc);
            cc.Dock = DockStyle.Fill;
            this.HoldingItem = cc;
        }
        private Control _HoldingItem = null;
        public Control HoldingItem
        {
            get { return this._HoldingItem; }
            set
            {
                this._HoldingItem = value;
                if (value != null)
                {
                    this.HoldingItem.MouseLeave += this.HoldingItem_MouseLeave;

                }
            }
        }
        public void RemoveHoldingItem()
        {
            this.Controls.Remove(this.HoldingItem);
        }
        public void SetHoldingItem()
        {
            this.Controls.Add(this.HoldingItem);
            this.HoldingItem.Dock = DockStyle.Fill;
        }
        public bool IsHolding
        {
            get { return this.Controls.Contains(this.HoldingItem); }
        }
        private void HoldingItem_MouseLeave(object sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }
    }
    [Serializable]
    public class DockingPanelInfo
    {
        public DockingPanelInfo()
        {
        }
        public DockingPanlelControlInfo ControlSetting { get; set; }
        public bool IsForm { get; set; }
        private bool _IsFloating = true;
        public bool HasOwner { get; set; }
        public bool IsFloating
        {
            get { return this._IsFloating; }
            set { this._IsFloating = value; }
        }
        public string Name { get; set; }
        public void SetForm(DockingPanelForm form)
        {
            this.Form = form;
            Form.Setting = this.FormSetting;
        }
        public void PreDispose()
        {
            this.FormSetting = this.Form.Setting;
        }

        [System.Xml.Serialization.XmlIgnore]
        public DockingPanelForm Form { get; set; }
        public FormSaveData FormSetting { get; set; }
    }
    [Serializable()]
    public class DockingPanlelControlInfo
    {
        private Size _Size = new Size(300, 300);
        public Size Size
        {
            get { return this._Size; }
            set { this._Size = value; }
        }
        public bool Opening { get; set; }
        private DockingPanel _Panel = null;

        [System.Xml.Serialization.XmlIgnore]
        public DockingPanel Panel
        {
            get { return this._Panel; }
            set {
                this._Panel = value;
            }
        }
    }
}
