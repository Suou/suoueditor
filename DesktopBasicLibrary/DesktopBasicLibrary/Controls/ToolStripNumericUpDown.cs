﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace BSystem.Controls
{
    public partial class ToolStripNumericUpDown : ToolStripControlHost
    {
        public ToolStripNumericUpDown():base(new NumericUpDown())
        {
            InitializeComponent();
        }

        public ToolStripNumericUpDown(IContainer container):base(new NumericUpDown())
        {
            container.Add(this);

            InitializeComponent();
        }
        /// <summary>
        /// ホストしているNumericUpDownコントロール
        /// </summary>
        public NumericUpDown NumericUpDown
        {
            get
            {
                return (NumericUpDown)Control;
            }
        }

        /// <summary>
        /// 値の設定と取得
        /// </summary>
        public decimal Value
        {
            get
            {
                return NumericUpDown.Value;
            }
            set
            {
                NumericUpDown.Value = value;
            }
        }

        //ホストしているNumericUpDownのイベントをサブスクライブする
        protected override void OnSubscribeControlEvents(Control control)
        {
            base.OnSubscribeControlEvents(control);
            NumericUpDown numControl = (NumericUpDown)control;
            numControl.ValueChanged +=
                new EventHandler(NumericUpDown_OnValueChanged);
        }

        //ホストしているNumericUpDownのイベントをアンサブスクライブする
        protected override void OnUnsubscribeControlEvents(Control control)
        {
            base.OnUnsubscribeControlEvents(control);
            NumericUpDown numControl = (NumericUpDown)control;
            numControl.ValueChanged -=
                new EventHandler(NumericUpDown_OnValueChanged);
        }

        /// <summary>
        /// 値が変化した時に発生するイベント
        /// </summary>
        public event EventHandler ValueChanged;

        //ValueChangedイベントを発生
        private void NumericUpDown_OnValueChanged(object sender, EventArgs e)
        {
            if (ValueChanged != null)
            {
                ValueChanged(this, e);
            }
        }
    }
}
