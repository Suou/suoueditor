﻿namespace BSystem.Controls
{
    partial class SerializableForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SerializableForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(379, 328);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SerializableForm";
            this.Text = "SerializableForm";
            this.Load += new System.EventHandler(this.SerializableForm_Load);
            this.LocationChanged += new System.EventHandler(this.SerializableForm_LocationChanged);
            this.SizeChanged += new System.EventHandler(this.TextEditorForm_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.TextEditorForm_VisibleChanged);
            this.ResumeLayout(false);

        }

        #endregion
    }
}