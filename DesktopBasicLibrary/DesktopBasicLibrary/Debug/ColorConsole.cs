﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Runtime.CompilerServices;

namespace BSystem.Debug
{
    public class ColorConsole
    {
        [DllImport("kernel32.dll")]
        static extern bool SetConsoleTextAttribute(
            IntPtr hConsoleOutput,
            ushort wAttributes);

        [DllImport("kernel32.dll", SetLastError = true)]
        static extern IntPtr GetStdHandle(
            int nStdHandle);

        [DllImport("kernel32.dll")]
        static extern bool GetConsoleScreenBufferInfo(
            IntPtr hConsoleOutput,
            out CONSOLE_SCREEN_BUFFER_INFO lpConsoleScreenBufferInfo);

        [StructLayout(LayoutKind.Sequential)]
        struct COORD
        {
            public short x;
            public short y;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct SMALL_RECT
        {
            public short Left;
            public short Top;
            public short Right;
            public short Bottom;
        }

        [StructLayout(LayoutKind.Sequential)]
        struct CONSOLE_SCREEN_BUFFER_INFO
        {
            public COORD dwSize;
            public COORD dwCursorPosition;
            public ushort wAttributes;
            public SMALL_RECT srWindow;
            public COORD dwMaximumWindowSize;
        }
        private static ushort GetBackGround(ConsoleColor color)
        {
            switch (color)
            {
                case ConsoleColor.Blue: return 16;
                case ConsoleColor.Green: return 32;
                case ConsoleColor.Red: return 64;
                case ConsoleColor.Intensity: return 128;
            }
            throw new Exception();
        }

        public static void WriteLine(string str, ConsoleColor foreGroundColor, ConsoleColor backGroundColor)
        {
            const int STD_OUTPUT_HANDLE = -11;
            /*
            const int FOREGROUND_GREEN = 2;
            const int FOREGROUND_INTENSITY = 8;
            const int BACKGROUND_BLUE = 16;
            */
            var fc = (ushort)foreGroundColor;
            var bc = GetBackGround(backGroundColor);

            CONSOLE_SCREEN_BUFFER_INFO bufferInfo
                = new CONSOLE_SCREEN_BUFFER_INFO();
            IntPtr hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

            GetConsoleScreenBufferInfo(hConsole, out bufferInfo);


            SetConsoleTextAttribute(
                hConsole,
                (ushort)(fc | bc) );

            Console.WriteLine(str);

            SetConsoleTextAttribute(hConsole, bufferInfo.wAttributes);

        }
        private static int tabCount = 0;
        public static void IncrementTabCount()
        {
            WriteLine("--START--");
            tabCount++;
        }
        public static void DecrementTabCount()
        {
            tabCount--;
            WriteLine("--END--");

        }

        public static void WriteLine(string str)
        {
            for(var i = 0; i < tabCount; i++)
            {
                str = "\t" + str;
            }
            System.Console.WriteLine(str);

        }

        /* WinCon.h
        #define FOREGROUND_BLUE      0x0001 
        #define FOREGROUND_GREEN     0x0002 
        #define FOREGROUND_RED       0x0004 
        #define FOREGROUND_INTENSITY 0x0008 
        #define BACKGROUND_BLUE      0x0010 
        #define BACKGROUND_GREEN     0x0020 
        #define BACKGROUND_RED       0x0040 
        #define BACKGROUND_INTENSITY 0x0080 
        //*/
    }
    public enum ConsoleColor
    {
        Blue = 1, Green = 2, Red = 4, Intensity = 8 
    }
}
