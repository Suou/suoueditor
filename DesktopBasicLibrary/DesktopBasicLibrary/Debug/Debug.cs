﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using BSystem.Collections;
using System.Windows.Forms;

namespace BSystem
{
    public static class Diagnostics
    {
        public static bool IsDesignMode(this Control ctrl)
        {
            bool returnFlag = false;
            if (System.ComponentModel.LicenseManager.UsageMode == System.ComponentModel.LicenseUsageMode.Designtime)
                returnFlag = true;
            else if (Process.GetCurrentProcess().ProcessName.ToUpper().Equals("DEVENV")
                || Process.GetCurrentProcess().ProcessName.ToUpper().Equals("VCSEXPRESS"))
                returnFlag = true;
            else if (AppDomain.CurrentDomain.FriendlyName == "DefaultDomain")
                returnFlag = true;
            return returnFlag;
        }
        public static void WriteFile<T>(IEnumerable<T> text, string path, Encoding enc = null)
        {
            enc = enc ?? Encoding.Unicode;
            using (var p = new StreamWriter(path, false, enc, 1024))
            {
                text.ForEach((v) => p.WriteLine(v));
            }
            System.Diagnostics.Process.Start(path);
        }
        private static string SamplePath = "Hogehoge.txt";
        public static List<string> MessageList = new List<string>();
        public static void WriteFile<T>(IEnumerable<T> text, Encoding enc = null)
        {
            WriteFile(text, SamplePath);
        }
        public static void AddMessage<T>(IEnumerable<T> text)
        {
            foreach (var v in text)
            {
                MessageList.Add(v.ToString());
            }
        }
        public static void AddMessage(string text)
        {
            MessageList.Add(text.ToString());
        }
        public static void SaveMessageList()
        {
            WriteFile(MessageList);
        }


        public static void WriteLog(string text)
        {
            //DefaultTraceListenerオブジェクトを取得
            DefaultTraceListener drl;
            drl = (DefaultTraceListener)Trace.Listeners["Default"];
            
            drl.LogFileName = "log.txt";
            drl.WriteLine(text);
            drl.WriteLine("----------------------------------");
        }
        public static TimeSpan StopWatch(Action pred)
        {
            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            pred();
            sw.Stop();
            return sw.Elapsed;
        }
        public static TimeSpan StopWatch<T>(Action<T> pred,T item)
        {
            Action t = () => pred(item);
            return StopWatch(t);
        }

        public static TimeSpan ConsoleStopWatch(Action pred,string name)
        {
            var p = StopWatch(pred);
            Console.WriteLine("Function {0} need {1} time",name ,p.TotalSeconds);
            return p;
        }
        public static TimeSpan ConsoleStopWatch<T>(Action<T> pred,T item , string name)
        {
            Action t = () => pred(item);
            return ConsoleStopWatch(t, name);
        }
        public static double ConsoleAverageStopWatch<T>(Action<T> pred,T item, int n, string name)
        {
            Action t = () => pred(item);
            return ConsoleAverageStopWatch(t, n, name);
            
        }
        public static double ConsoleAverageStopWatch(Action pred,int n,string name)
        {
            double p = 0;
            for (int i = 0; i < n; i++)
            {
                p += StopWatch(pred).TotalSeconds;
            }
            var q = p / n;
            Console.WriteLine("Function {0} need {1} average time", name, q);
            return q;
        }
    }
}
