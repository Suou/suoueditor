﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using BSystem.Structure;
using BSystem.Drawing;
using System.IO;
namespace BSystem.Structure
{
    public class Bval
    {
        Color[] Items { get; set; }
        int i = 0;
        public Bval(Color[] items)
        {
            this.Items = items;
        }
        public void Proc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            var p = this.Items[i++];
            
                b = (byte)p.B;
                g = (byte)p.G;
                r = (byte)p.R;
                a = (byte)p.A;
            
        }
    }
    public class BmpRLE:RleStructure<int>
    {
        Size Size { get; set; }
        
        public BmpRLE(Bitmap bmp):base(BmpRLE.Create(bmp))
        {
            this.Size = bmp.Size;
            var s = this.Size.Width*this.Size.Height;
            var p = (double)this.Items.Length / (double)s;
        }
        public static int[] Create(Bitmap bmp)
        {
            return bmp.GetPixels().Select((v) => v.ToArgb()).ToArray();
        }
        public new IEnumerator<Color> GetEnumerator()
        {
            foreach (var pair in this.Items)
            {
                for (int i = 0; i < pair.Value; i++) yield return Color.FromArgb(pair.Key);
            }
        }
        public IEnumerable<Color> GetColors()
        {
            foreach (var pair in this)
            {
                yield return pair;
            }
        }
        public Color[] GetColorsArray()
        {
            Color[] item = new Color[this.Length];
            int i = 0;
            foreach (var pair in this.Items)
            {
                item[i++] = Color.FromArgb(pair.Key);
            }
            return item;
        }
        public Bitmap CreateBitmap()
        {

            var bmp = new Bitmap(this.Size.Width, this.Size.Height);
            var f = new Bval(this.GetColorsArray());
            TurboArgbPixelFilter32.FastFiltering(bmp, f.Proc);
            return bmp;
        }
        

        public void Save(string filename)
        {
            var f = new FileInfo(filename);
            using (var str = f.OpenWrite())
            {
                using (var sw = new StreamWriter(str, Encoding.ASCII))
                {
                    sw.WriteLine(this.Size);
                    sw.WriteLine(this.CreateRLECode());
                }
            }
        }
        public void Load(string filename)
        {
        }
        
    }
}
