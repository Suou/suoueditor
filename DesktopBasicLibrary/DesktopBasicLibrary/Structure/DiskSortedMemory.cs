﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.IO;
using BSystem.IO;
namespace BSystem.Structure
{
    



    public interface IndexMemoryItem:IKumikomiMemoryItem<uint>
    {
    }
    /*
    public interface ILongList<T> where T : IComparer<T>
    {
        T this[long nth] { get; }
        long Length { get; }
    }
    */
    public class DiskSortedMemory<T> : AbstractDiskFixedMemory<uint> where T : IndexMemoryItem,IComparer<T>, new()
    {
        private readonly uint Interval;
        
        public readonly uint Divide = 100;
        public DirectoryInfo TemporaryFolder { get; set; }
        public DiskSortedMemory(string name):base(new FileInfo(name),sizeof(uint))
        {
            this.Interval = UInt32.MaxValue / this.Divide; 
        }
        public DiskSortedMemory(FileInfo fi, DirectoryInfo temporaryFolderInfo):base(fi,sizeof(uint))
        {
            this.Interval = UInt32.MaxValue / this.Divide;
            this.TemporaryFolder = temporaryFolderInfo;
            if (!this.TemporaryFolder.Exists) throw new Exception();
        }
        public override uint CreateItem(byte[] item)
        {
            return BitConverter.ToUInt32(item,0);
        }
        public override byte[] TranslateByte(uint item)
        {
            return BitConverter.GetBytes(item);
        }
        public override byte[] TranslateByte(uint[] item)
        {
            throw new NotImplementedException();
        }
        
        private DiskSortedMemory<T>[] CreateEmptyElements()
        {
            DiskSortedMemory<T>[] r = new DiskSortedMemory<T>[Divide];
            for (uint i = 0; i < Divide; i++)
            {
                uint minlimit = Interval * (i);
                uint maxlimit = Interval * (i + 1);
                r[i] = DiskSortedMemory<T>.CreateTemporaryItem(new FileInfo(this.TemporaryFolder.FullName + "/" + minlimit + "_" + maxlimit));
            }
            return r;
        }
        private DiskSortedMemory<T> CreateElements()
        {
            throw new Exception();
            /*
            var r = this.CreateEmptyElements();
            this.GetCollection().ForEach((v) => r[v / Interval].Append(v));
            return r;
            */
        }
        public void Sort()
        {
            this.Sort(() => { });
        }
        public void Sort(Action t)
        {
            throw new Exception();
            /*
            var r = this.CreateElements();
            this.Clean();
            r.SortWithMemory();
            r.ForEach2((v)=>t()).ForEach((v) => this.Append(v));
            r.ForEach((v) => v.Delete());
            this.DeleteTemporaryFolder();
            */
        }

        private T[] SortWithMemory()
        {
            throw new Exception();
            /*
            var v = this.GetCollection().ToList();
            v.Sort((a, b) => a.Compare(a,b));
            var p = v.ToArray();
            return p;
            */
        }
        private void DeleteTemporaryFolder()
        {
            this.TemporaryFolder.Delete();
        }
        private static DiskSortedMemory<T> CreateTemporaryItem(FileInfo fi)
        {
            return new DiskSortedMemory<T>(fi.FullName);
        }
        public static DiskSortedMemory<T> Create(FileInfo fi,DirectoryInfo temporaryFolder)
        {
            
            temporaryFolder.Create();
            if (temporaryFolder.Exists)
            {
                return new DiskSortedMemory<T>(fi, temporaryFolder);
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
