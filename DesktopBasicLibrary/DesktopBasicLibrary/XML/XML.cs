﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
namespace BSystem
{
    public static class XMLExtension
    {

        public static int AttributeIntValue(this XElement item, string name)
        {
            return int.Parse(item.Attribute(name).Value);
        }
        public static bool AttributeBoolValue(this XElement item, string name)
        {
            var g = item.Attribute(name);

            var p = g == null ? "false" : (g.Value);
            return (p == "true");
        }
        public static string BoolString(bool b)
        {
            return b ? "true" : "false";
        }
        public static bool BoolAttributeParse(this XElement x, string name, bool defaultValue)
        {
            if (x.Attribute(name) != null)
            {
                return bool.Parse(x.Attribute(name).Value);
            }
            else
            {
                return defaultValue;
            }
        }
        public static int IntAttributeParse(this XElement x, string name, int defaultValue)
        {
            if (x.Attribute(name) != null)
            {
                return int.Parse(x.Attribute(name).Value);
            }
            else
            {
                return defaultValue;
            }
        }
        public static string StringAttributeParse(this XElement x, string name, string defaultValue)
        {
            if (x.Attribute(name) != null)
            {
                return x.Attribute(name).Value;
            }
            else
            {
                return defaultValue;
            }
        }
        public static T EnumAttributeParse<T>(this XElement x, string name, T defaultValue)
        {
            if (x.Attribute(name) != null)
            {
                return (T)Enum.Parse(typeof(T),x.Attribute(name).Value);
            }
            else
            {
                return defaultValue;
            }
        }
    }
}
