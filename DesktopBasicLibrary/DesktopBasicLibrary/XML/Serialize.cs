﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
namespace BSystem.XML
{
    public static class NonBinarySerializer
    {
        public static void Save<T>(string filename, T obj)
        {
            string fileName = filename;
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.Create))
            {
                serializer.Serialize(fs, obj);
            }

        }
        public static T Load<T>(string filename)
        {
            string fileName = filename;

            var fi = new System.IO.FileInfo(filename);
            if (!fi.Exists) return default(T);
            T obj = default(T);
            XmlSerializer serializer = new XmlSerializer(typeof(T));
            using (FileStream fs = new FileStream(fileName, System.IO.FileMode.Open))
            {
                obj = (T)serializer.Deserialize(fs);
                
            }

            return obj;
        }
    }


    public class BinarySerialize
    {
        /// <summary>
        /// オブジェクトの内容をファイルから読み込み復元する
        /// </summary>
        /// <param name="path">読み込むファイル名</param>
        /// <returns>復元されたオブジェクト</returns>
        public static T LoadFromBinaryFile<T>(string path)
        {
            var fi = new FileInfo(path);
            if (!fi.Exists) return default(T);

            FileStream fs = new FileStream(path,
                FileMode.Open,
                FileAccess.Read);
            return LoadFromBinaryFile<T>(fs);
        }
        public static T LoadFromBinaryFile<T>(Stream fs)
        {
            
            T obj = default(T);
            BinaryFormatter f = new BinaryFormatter();
            try
            {
                //読み込んで逆シリアル化する
                obj = (T)f.Deserialize(fs);
            }
            finally
            {
                fs.Close();
            }
            return (T)obj;
        }
        /// <summary>
        /// オブジェクトの内容をファイルに保存する
        /// </summary>
        /// <param name="obj">保存するオブジェクト</param>
        /// <param name="path">保存先のファイル名</param>
        public static bool SaveToBinaryFile(object obj, string path)
        {
            FileStream fs = new FileStream(path,
                FileMode.Create,
                FileAccess.Write);
            return SaveToBinaryFile(obj, fs);
        }
        public static bool SaveToBinaryFile(object obj, Stream stream)
        {
            BinaryFormatter bf = new BinaryFormatter();
            bool b = false;
            //シリアル化して書き込む
            try
            {
                bf.Serialize(stream, obj);
                b = true;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
               stream.Close();
            }
            return b;
        }
    }
}
