﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using BSystem.Structure;
using BSystem.Collections;
namespace BSystem
{

    public static class WriterExtensions
    {
        public static void WriteLine(string text, string filename)
        {
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            Encoding utfEnc = Encoding.GetEncoding("utf-8");
            StreamWriter writer = new StreamWriter(filename, true, utfEnc);
            writer.WriteLine(text);
            writer.Close();
        }
        public static void WriteLine(string text, string filename, bool append)
        {
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            Encoding utfEnc = Encoding.GetEncoding("utf-8");
            StreamWriter writer = new StreamWriter(filename, append, utfEnc);
            writer.WriteLine(text);
            writer.Close();
        }


        public static string ToStringLine(this IEnumerable<int> text)
        {
            string s = "";
            s = string.Join(",", text.Select((v) => v.ToString()));
            s = "[" + s + "]";
            return s;
        }
    }
    


}
