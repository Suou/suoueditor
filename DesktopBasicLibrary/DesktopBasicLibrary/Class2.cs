﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
using BSystem.Text;
namespace BSystem
{

    public static class Encode2Extension
    {
        public static string Translate(Encoding preEncode, Encoding sufEncode, string text)
        {
            var f = preEncode.GetBytes(text);
            var g = Encoding.Convert(preEncode, sufEncode, f);

            return sufEncode.GetString(g);
        }
    }
    public static class Class2
    {
        public static void WriteLine<T>(this IEnumerable<T> Items)
        {
            
            Console.WriteLine(Items.FormatArray1("{", "}", System.Environment.NewLine));
            //Items.Write("{", "}", System.Environment.NewLine);
        }
        public static void Write<T>(this IEnumerable<T> Items)
        {
            Console.WriteLine(Items.FormatArray1());
            //Items.Write("{", "}", ",");
        }
        
    }
    public static class TimeExtension
    {
        public static long StopWatch(Action t)
        {
            var v = Stopwatch.StartNew();
            t();
            v.Stop();
            return v.ElapsedMilliseconds;
        }
    }
    public static class WebExtension
    {
        public static string ReadtoEnd(Stream stream)
        {
            var v = ReadBinaryData(stream);
            var enc = EncodeExtension.GetCode(v);

            var s = enc.GetString(v);
            return s;
        }
        public static byte[] ReadBinaryData(Stream st)
        {

            byte[] buf = new byte[32768]; // 一時バッファ

            using (MemoryStream ms = new MemoryStream())
            {

                while (true)
                {
                    // ストリームから一時バッファに読み込む
                    int read = st.Read(buf, 0, buf.Length);

                    if (read > 0)
                    {
                        // 一時バッファの内容をメモリ・ストリームに書き込む
                        ms.Write(buf, 0, read);
                    }
                    else
                    {
                        break;
                    }
                }
                // メモリ・ストリームの内容をバイト配列に格納
                return ms.ToArray();
            }
        }
    }
}
