﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Diagnostics;
using BSystem.Structure;
using BSystem.Collections;

namespace BSystem
{

    public static class FolderSuperVisor
    {
        public static IEnumerable<string> FileNameIterator(this DirectoryInfo di)
        {

            if (di.Exists)
            {
                var v = di.GetFiles();
                var r = di.GetDirectories();
                foreach (var w in r)
                {
                    var a = FileNameIterator(w);
                    foreach (var g in a)
                    {
                        yield return g;
                    }
                }
                foreach (var w in v)
                {
                    yield return w.FullName;
                }
            }
        }
        public static IEnumerable<FileInfo> FileInfoIterator(this DirectoryInfo di)
        {
            var v = di.GetFiles();
            var r = di.GetDirectories();
            foreach (var w in r)
            {
                var a = FileInfoIterator(w);
                foreach (var g in a)
                {
                    yield return g;
                }
            }
            foreach (var w in v)
            {
                yield return w;
            }
        }
        public static int FileCount(this DirectoryInfo di)
        {
            int t = 0;
            foreach (var fi in FileInfoIterator(di))
            {
                t++;
            }
            return t;
        }

        public static class GetHash
        {
            public static string Create(string s)
            {
                //文字列をbyte型配列に変換する
                byte[] data = System.Text.Encoding.UTF8.GetBytes(s);

                //MD5CryptoServiceProviderオブジェクトを作成
                System.Security.Cryptography.MD5CryptoServiceProvider md5 =
                    new System.Security.Cryptography.MD5CryptoServiceProvider();
                //または、次のようにもできる
                //System.Security.Cryptography.MD5 md5 =
                //    System.Security.Cryptography.MD5.Create();

                //ハッシュ値を計算する
                byte[] bs = md5.ComputeHash(data);

                //byte型配列を16進数の文字列に変換
                System.Text.StringBuilder result = new System.Text.StringBuilder();
                foreach (byte b in bs)
                {
                    result.Append(b.ToString("x2"));
                }
                //ここの部分は次のようにもできる
                //string result = BitConverter.ToString(bs).ToLower().Replace("-","");

                //結果を表示
                return result.ToString();
            }
        }
    }
}
