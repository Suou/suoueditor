﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
namespace BSystem
{
    public static class PointConverter
    {
        public static System.Drawing.Point ToDesktopPoint(Point2D p)
        {
            return new System.Drawing.Point(p.X, p.Y);
        }
        public static Point2D ToBasicPoint(Point p)
        {
            return new Point2D(p.X, p.Y);
        }
        public static Size ToDesktopSize(BSize size)
        {
            return new Size(size.Width,size.Height);
        }
        public static BSize ToBasicPoint(Size size)
        {
            return new BSize(size.Width, size.Height);
        }

    }
}
