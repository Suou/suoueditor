﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;
namespace BSystem.Collections
{
    [Serializable]
    public class SuperObservableCollection<T> : ObservableCollection<T>
    {
        [field: NonSerialized]
        public new event Action<object,SuperObservableCollectionEventArgs<T>> CollectionChanged;
        public SuperObservableCollection()
        {
            //base.CollectionChanged += this.Items_CollectionChanged;
        }
        public SuperObservableCollection(IEnumerable<T> items)
        {
            foreach (var v in items) this.Add(v);
            //base.CollectionChanged += this.Items_CollectionChanged;

        }
        public new void Add(T item)
        {            
            base.Add(item);
            var args = new SuperObservableCollectionEventArgs<T>(new[]{item},SuperObservableCollectionAction.Add);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        public void AddRange(IEnumerable<T> items)
        {
            foreach(var v in items)base.Add(v);
            var args = new SuperObservableCollectionEventArgs<T>(items.ToArray(), SuperObservableCollectionAction.Add);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        public new void Insert(int index, T item)
        {
            base.InsertItem(index, item);
            var args = new SuperObservableCollectionEventArgs<T>(new[] { item },null,index, SuperObservableCollectionAction.Insert);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        public void Insert(int index, IEnumerable<T> items)
        {
            var sndItems = items.ToArray();
            var fstIndex = index;
            foreach (var v in items) base.Insert(index++,v);
            var args = new SuperObservableCollectionEventArgs<T>(sndItems,null,fstIndex, SuperObservableCollectionAction.Insert);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        private int RemoveItem2(T item)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this[i].Equals(item))
                {
                    base.RemoveAt(i);
                    return i;
                }
            }
            return -1;
        }

        public new bool Remove(T item)
        {
            var b = this.RemoveItem2(item);
            
            if (b != -1)
            {
                var args = new SuperObservableCollectionEventArgs<T>(null,new[] { item },b, SuperObservableCollectionAction.Remove);
                if (this.CollectionChanged != null) this.CollectionChanged(this, args);
            }
            return b != -1;
        }
        public new void RemoveAt(int n)
        {
            var item = this[n];
            base.RemoveAt(n);

            var args = new SuperObservableCollectionEventArgs<T>(null, new[] { item }, n, SuperObservableCollectionAction.Remove);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        public void RemoveRange(IEnumerable<T> items)
        {
            var sndItems = items.ToArray();
            items.ForEach((v) => this.RemoveItem2(v));
            var args = new SuperObservableCollectionEventArgs<T>(null, sndItems, -1, SuperObservableCollectionAction.Remove);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);

        }
        protected override void SetItem(int index, T item)
        {
            var oldItem = this[index];
            base.SetItem(index, item);
            var args = new SuperObservableCollectionEventArgs<T>(new[] { item }, new[] { oldItem }, index, SuperObservableCollectionAction.Replace);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        protected new void RemoveItem(int index)
        {
            var removedItem = this[index];
            base.RemoveItem(index);
            var args = new SuperObservableCollectionEventArgs<T>(null, new[] { removedItem }, index, SuperObservableCollectionAction.Remove);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        protected new void ClearItems()
        {
            this.Clear();            
        }
        
        public new void Move(int oldIndex,int newIndex)
        {
            base.Move(oldIndex, newIndex);
            var args = new SuperObservableCollectionEventArgs<T>(oldIndex,newIndex, SuperObservableCollectionAction.Move);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);
        }
        public new void Clear()
        {
            var items = this.ToArray();
            base.Clear();
            var args = new SuperObservableCollectionEventArgs<T>(items, SuperObservableCollectionAction.Clear);
            if (this.CollectionChanged != null) this.CollectionChanged(this, args);

        }

    }
    public class SuperObservableCollectionEventArgs<T>:EventArgs
    {
        public IList<T> AddedItems { get; private set; }
        public IList<T> RemovedItems { get; private set; }
        public int ChangedIndex { get; private set; }
        public int MoveToIndex { get; private set; }
        public SuperObservableCollectionAction Action { get; private set; }
        public SuperObservableCollectionEventArgs(IList<T> items, SuperObservableCollectionAction action)
        {
            this.Action = action;
            switch (action)
            {
                case SuperObservableCollectionAction.Add:
                    this.ChangedIndex = -1;
                    this.AddedItems = items;
                    break;
                case SuperObservableCollectionAction.Clear:
                    this.RemovedItems = items;
                    this.ChangedIndex = -1;
                    break;
            }
        }
        public SuperObservableCollectionEventArgs(IList<T> newItems, IList<T> removedItems, int newIndex, SuperObservableCollectionAction action)
        {
            this.Action = action;
            switch (action)
            {
                case SuperObservableCollectionAction.Insert:
                    this.ChangedIndex = newIndex;
                    this.AddedItems = newItems;
                    break;

                case SuperObservableCollectionAction.Remove:
                    this.ChangedIndex = newIndex;
                    this.RemovedItems = removedItems;
                    break;
                case SuperObservableCollectionAction.Replace:                    
                    this.ChangedIndex = newIndex;
                    this.AddedItems = newItems;
                    this.RemovedItems = removedItems;
                    break;
            }
        }
        public SuperObservableCollectionEventArgs(int oldIndex,int newIndex, SuperObservableCollectionAction action)
        {
            this.Action = action;
            switch (action)
            {
                case SuperObservableCollectionAction.Move:
                    this.MoveToIndex = newIndex;
                    this.ChangedIndex = oldIndex;
                    break;
            }
        }
    }
    public enum SuperObservableCollectionAction
    {
        Add,Insert,Remove,Replace,Move,Clear
    }
}
