﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Collections;
namespace BSystem.Collections
{
    public class SortedMultiDictionary<KEY, VALUE> : IEnumerable<KeyValuePair<KEY,VALUE>>
    {
        private SortedDictionary<KEY, List<VALUE>> Dic = new SortedDictionary<KEY, List<VALUE>>();
        
        public void Add(KEY key, VALUE value)
        {
            if (!this.Dic.ContainsKey(key)) this.Dic[key] = new List<VALUE>();
            this.Dic[key].Add(value);
            this.Count++;
        }
        public void Remove(KEY key, VALUE value)
        {
            this.Dic[key].Remove(value);
            if (this.Dic[key].Count == 0) this.Dic.Remove(key);
            this.Count--;
        }
        public int Count { get; private set; }
        
        public IEnumerator<KeyValuePair<KEY, VALUE>> GetEnumerator()
        {
            foreach (var v in this.Dic)
            {
                foreach (var w in v.Value)
                {
                    yield return new KeyValuePair<KEY, VALUE>(v.Key, w);
                }
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
