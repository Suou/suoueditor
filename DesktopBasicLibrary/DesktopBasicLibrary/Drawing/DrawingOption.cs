﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace BSystem.Drawing
{
    public enum TextRenderOption
    {
        RightProportion, None
    }
    class ColorDelegate
    {
        public ColorDelegate(Color c)
        {
            this.Color = c;
        }
        private Color _Color;
        Color Color
        {
            get { return _Color; }
            set
            {
                _Color = value;

            }
        }
        public void Nuritubusi(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = Color.B;
            g = Color.G;
            r = Color.R;
            a = Color.A;
        }
    }
}
