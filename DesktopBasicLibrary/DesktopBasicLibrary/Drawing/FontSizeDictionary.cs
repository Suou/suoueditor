﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace BSystem.Drawing
{

    /// <summary>
    /// Word
    /// Text
    /// String
    /// Line
    /// </summary>
    public static class FontSizeDictionary
    {
        //public static Dictionary<string, Dictionary<float, Dictionary<char, int>>> FontDic = new Dictionary<string, Dictionary<float, Dictionary<char, int>>>();
        public static Dictionary<int, Dictionary<char, int>> FontDic = new Dictionary<int, Dictionary<char, int>>();
        private static object LockObject = new object();
        public static int GetSuperHashCode(this Font f)
        {
            int n = f.Name.ToList().Sum((c) => c * c);
            n = n * (int)f.SizeInPoints * (int)f.SizeInPoints;
            return n;
        }
        public static int Width(this Font f, char c)
        {
            lock (LockObject)
            {
                int n = f.GetHashCode();
                if (!FontDic.ContainsKey(n))
                {
                    FontDic[n] = new Dictionary<char, int>();
                    FontDic[n][c] = FontSize.Width(f, c);
                    return FontDic[n][c];

                }
                else if (!FontDic[n].ContainsKey(c))
                {
                    FontDic[n][c] = FontSize.Width(f, c);
                    return FontDic[n][c];
                }
                else
                {
                    return FontDic[n][c];
                }
            }

        }

        public static Size WordSize(this Font f, string word)
        {
            if (f == null) throw new Exception();
            return new Size(f.Width(word), f.Height);
        }
        public static Size WordSize(this Font f, char letter)
        {
            if (f == null) throw new Exception();
            return new Size(f.Width(letter), f.Height);
        }
        public static int[] GetLettersWidth(this Font f, string word)
        {
            int len = 0;
            var r = new int[word.Length];
            for (int i = 0; i < word.Length; i++)
            {
                r[i] = len;
                len += f.Width(word[i]);
            }
            return r;
        }
        public static int Width(this Font f, string s)
        {
            int x = 0;
            foreach (char c in s)
            {
                x += Width(f, c);
            }
            return x;
        }
        public static int[] DotIndexes(this Font f, string s)
        {
            var r = new int[s.Length];
            int x = 0;
            for (int i = 0; i < s.Length; i++)
            {
                x += Width(f, s[i]);
                r[i] = (char)x;
            }
            return r;
        }

        public static Size TextSize(this Font f, List<string> text, int yinterval)
        {
            var r = Size.Empty;
            foreach (string s in text)
            {
                var z = FontSizeDictionary.Width(f, s);
                if (r.Width < z) r.Width = z;
                r.Height += f.Height + yinterval;
            }
            r.Height -= yinterval;
            return r;
        }



        public static int RoundFromWidthToLength(this Font f, string s, int width)
        {
            int n = width;
            for (int i = 0; i < s.Length; i++)
            {
                if (n <= 0) return i;
                n -= f.Width(s[i]);
            }
            return s.Length;
        }
        public static int TruncateFromWidthToLength(this Font f, string s, int width)
        {
            int n = width;
            for (int i = 0; i < s.Length; i++)
            {
                n -= f.Width(s[i]);
                if (n < 0) return i;
            }
            return s.Length;
        }
        public static int TextLength(string[] Ltext)
        {
            int n = Ltext.Count() - 1;
            for (int i = 0; i < Ltext.Length; i++)
            {
                n += Ltext[i].Length;
            }
            return n;
        }
        public static string SubDotString(this Font f, string s, int startDotPos, int width)
        {
            string str = "";
            int n = 0;
            int EndDotPos = startDotPos + width;

            foreach (char c in s)
            {
                if (n + f.Width(c) > EndDotPos) break;
                if (n >= startDotPos && n <= EndDotPos) str += c;
                n += f.Width(c);
            }
            return str;
        }


    }
}
