﻿
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BSystem.Collections;

namespace BSystem.Drawing.Pixel24
{

    public class FilterProc24
    {
        public Pixel FromPixel { get; set; }
        public Pixel ToPixel { get; set; }
        public void DyeProc(ref byte b, ref byte g, ref byte r)
        {
            var pix = new Pixel(r, g, b,0);
            if (pix.r == r && pix.b == b && pix.g == g)
            {
                b = ToPixel.b;
                g = ToPixel.g;
                r = ToPixel.r;
            }
        }
        public static void InverseProc(ref byte b, ref byte g, ref byte r)
        {
            b = (byte)~b;
            g = (byte)~g;
            r = (byte)~r;
        }
    }
    public static class TurboArgbPixelFilter24
    {
        
        public static bool CheckFormat24(this Bitmap bmp)
        {
            if (bmp.PixelFormat != PixelFormat.Format24bppRgb)
            {
                throw new ArgumentException("bmpのPixelFormatはFormat24bppRgbのみ有効です。", "bmp");
            }
            return true;
        }
        /// <summary>
        /// 画像のすべてのピクセルをループして指定した処理をする。
        /// </summary>
        /// <param name="bmp">処理対象の画像。PixelFormat.Format32bppArgbのみ対応。</param>
        /// <param name="proc">ピクセルごとに行う処理。1ピクセルごとに呼び出される。</param>
        public static void FastFiltering(Bitmap bmp, PixelProc24 proc)
        {
            bmp.CheckFormat24();
            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                for (int i = 0; i < dataLength; i += 3)
                {
                    proc(ref *(pixel++), ref *(pixel++), ref *(pixel++));
                }
            }

            bmp.UnlockBits(bmpData);
        }
        #region FastFilter
        public static Bitmap InverseFiltering(Bitmap bmp)
        {
            var bmp2 = (Bitmap)bmp.Clone();
            FastFiltering(bmp2, FilterProc24.InverseProc);
            return bmp2;
        }
        
        #endregion


    }
    public delegate void PixelProc24(ref byte b, ref byte g, ref byte r);
}
