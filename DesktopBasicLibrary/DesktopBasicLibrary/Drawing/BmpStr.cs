﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections;
namespace BSystem.Drawing
{
    public class Bmpstr
    {
        public int Width, Height;
        public Pixel[][] pix;
        private int x, y;
        public Bmpstr(int width, int height)
        {
            Width = width;
            Height = height;
            pix = new Pixel[height][];
            for (int i = 0; i < this.Height; i++)
            {
                pix[i] = new Pixel[width];
            }
            x = 0; y = 0;
        }

        public void add(Pixel p)
        {
            pix[y][x] = p;
            if (++x == Width)
            {
                x = 0; y++;
            }
        }
        public BitArray[] Binarize()
        {
            BitArray[] r = new BitArray[pix.Length];
            for (int y = 0; y < pix.Length; y++)
            {
                r[y] = new BitArray(pix[y].Length);
                for (int x = 0; x < pix[y].Length; x++)
                {
                    r[y][x] = pix[y][x].IsBlack ? true : false; 
                }
            }
            return r;
        }
        public NinePoint GetPoint(Point p)
        {
            var v = new NinePoint();
            v.pix[0][0] = p.Y >= 1 && p.X > 0 ? this.pix[p.Y - 1][p.X - 1] : Pixel.Empty;
            v.pix[0][1] = p.Y >= 1 ? this.pix[p.Y - 1][p.X] : Pixel.Empty;
            v.pix[0][2] = p.Y >= 1 && p.X < this.Width - 1 ? this.pix[p.Y - 1][p.X + 1] : Pixel.Empty;
            v.pix[1][0] = p.X > 0 ? this.pix[p.Y][p.X - 1] : Pixel.Empty;
            v.pix[1][1] = this.pix[p.Y][p.X];
            v.pix[1][2] = p.X < this.Width - 1 ? this.pix[p.Y][p.X + 1] : Pixel.Empty;
            v.pix[2][0] = p.Y < this.Height - 1 && p.X > 0 ? this.pix[p.Y + 1][p.X - 1] : Pixel.Empty;
            v.pix[2][1] = p.Y < this.Height - 1 ? this.pix[p.Y + 1][p.X] : Pixel.Empty;
            v.pix[2][2] = p.Y < this.Height - 1 && p.X < this.Width - 1 ? this.pix[p.Y + 1][p.X + 1] : Pixel.Empty;
            return v;
        }
        NinePoint[][] GetPoints()
        {
            NinePoint[][] np = new NinePoint[this.Height][];
            for (int i = 0; i < this.Width; i++)
            {
                pix[i] = new Pixel[this.Width];
            }
            for (int y = 0; y < this.Height; y++)
            {
                for (int x = 0; x < this.Width; x++)
                {
                    np[y][x] = this.GetPoint(new Point(x, y));
                }
            }
            return np;
        }
        public void GetM()
        {
            foreach (var p in this.pix)
            {
                var s = String.Join(",", p.Select((v) => v.IsB));
            }
        }
    }
    public class NinePoint
    {
        public Pixel[][] pix;

        public NinePoint()
        {
            this.pix = new Pixel[3][];
            for (int i = 0; i < 3; i++)
            {
                pix[i] = new Pixel[3];
            }
        }
    }


    public struct Pixel
    {
        public static Pixel Empty
        {
            get
            {
                return new Pixel();
            }
        }
        public byte r, g, b, a;
        public Pixel(Color c):this(c.R,c.G,c.B,c.A)
        {
            
        }
        public Pixel(int r, int g, int b, int a)
        {
            this.r = (byte)r;
            this.g = (byte)g;
            this.b = (byte)b;
            this.a = (byte)a;
        }
        public Pixel(byte t1,byte t2)
        {
            ushort value = (ushort)(t1 * BSystem.MathExtension.Pow(2, 8) + t2);
            this.r = BSystem.ByteExtension.ToValue(value,0,5);
            this.g = BSystem.ByteExtension.ToValue(value, 5, 5);
            this.b = BSystem.ByteExtension.ToValue(value, 10, 5);
            this.a = 255;
        }
        public Pixel(bool w)
        {
            this.r = w ? (byte)0 : (byte)255;
            this.g = w ? (byte)0 : (byte)255;
            this.b = w ? (byte)0 : (byte)255;
            this.a = 255;
        }
        public bool IsBlack
        {
            get
            {
                return a == 255 && g == 0 && b == 0 && r == 0;
            }
        }
        public bool IsWhite
        {
            get
            {
                return a == 255 && g == 255 && b == 255 && r == 255;
            }
        }
        public Color GetColor()
        {
            var v = Color.FromArgb(a, r, g, b);
            return v;
        }
        public override string ToString()
        {
            var s = String.Format("[{0},{1},{2},{3}]", r, g, b, a);
            return s;
        }
        public string IsB
        {
            get
            {
                return this.IsBlack ? "B" : "T";
            }
        }
    }
}
