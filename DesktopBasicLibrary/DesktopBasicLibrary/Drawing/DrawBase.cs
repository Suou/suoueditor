﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

namespace BSystem.Drawing
{
    public struct PointLine
    {
        public Point A;
        public Point B;
        public PointLine(Point a, Point b)
        {
            this.A = a;
            this.B = b;
        }
    }
    public class DrawOut
    {
        [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
        public static extern bool TextOut(IntPtr hdc, int nXStart, int nYStart, string lpString, int cbString);
        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        [DllImport("gdi32.dll", ExactSpelling = true, PreserveSig = true, SetLastError = true)]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        [DllImport("gdi32.dll")]
        public static extern int SetBkMode(IntPtr hdc, int iBkMode);
        [DllImport("gdi32.dll")]
        public static extern uint SetTextColor(IntPtr hdc, int crColor);
        [DllImport("gdi32.dll")]
        private static extern bool LineTo(IntPtr hdc, int nXEnd, int nYEnd);
        [DllImport("gdi32.dll")]
        private static extern bool MoveToEx(IntPtr hdc, int X, int Y, IntPtr lpPoint);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreatePen(PenStyle fnPenStyle, int nWidth, int crColor);
        [DllImport("gdi32.dll")]
        static extern bool Rectangle(IntPtr hdc, int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateSolidBrush(uint crColor);

        public static int TRANSPARENT = 1;

        private enum PenStyle : int
        {
            PS_SOLID = 0, //The pen is solid.
            PS_DASH = 1, //The pen is dashed.
            PS_DOT = 2, //The pen is dotted.
            PS_DASHDOT = 3, //The pen has alternating dashes and dots.
            PS_DASHDOTDOT = 4, //The pen has alternating dashes and double dots.
            PS_NULL = 5, //The pen is invisible.
            PS_INSIDEFRAME = 6,// Normally when the edge is drawn, it’s centred on the outer edge meaning that half the width of the pen is drawn
            // outside the shape’s edge, half is inside the shape’s edge. When PS_INSIDEFRAME is specified the edge is drawn 
            //completely inside the outer edge of the shape.
            PS_USERSTYLE = 7,
            PS_ALTERNATE = 8,
            PS_STYLE_MASK = 0x0000000F,

            PS_ENDCAP_ROUND = 0x00000000,
            PS_ENDCAP_SQUARE = 0x00000100,
            PS_ENDCAP_FLAT = 0x00000200,
            PS_ENDCAP_MASK = 0x00000F00,

            PS_JOIN_ROUND = 0x00000000,
            PS_JOIN_BEVEL = 0x00001000,
            PS_JOIN_MITER = 0x00002000,
            PS_JOIN_MASK = 0x0000F000,

            PS_COSMETIC = 0x00000000,
            PS_GEOMETRIC = 0x00010000,
            PS_TYPE_MASK = 0x000F0000
        };


        public static void DrawLineRight(IntPtr hDC, Point p, int len, Color c)
        {

            IntPtr pen = CreatePen(PenStyle.PS_SOLID, 1, ColorTranslator.ToWin32(c));
            IntPtr hOldFont = DrawOut.SelectObject(hDC, pen);

            MoveToEx(hDC, p.X, p.Y, IntPtr.Zero);
            LineTo(hDC, p.X + len - 1, p.Y);

            DrawOut.DeleteObject(DrawOut.SelectObject(hDC, hOldFont));
        }

        public static void Draw(IntPtr hDC, IntPtr hFont, int x, int y, string str, Color c)
        {

            int len = str.Length;

            IntPtr hOldFont = DrawOut.SelectObject(hDC, hFont);
            DrawOut.SetBkMode(hDC, DrawOut.TRANSPARENT);

            DrawOut.SetBkMode(hDC, DrawOut.TRANSPARENT);
            DrawOut.SetTextColor(hDC, ColorTranslator.ToWin32(c));
            TextOut(hDC, x, y, str, len);
            DrawOut.DeleteObject(DrawOut.SelectObject(hDC, hOldFont));
        }
        public static void Draw(IntPtr hDC, IntPtr hFont, Point p, string str, Color c)
        {
            Draw(hDC, hFont, p.X, p.Y, str, c);
        }
        public static void DrawLine(IntPtr hDC, PointLine[] Lines, Color color)
        {
            IntPtr pen = CreatePen(PenStyle.PS_SOLID, 1, ColorTranslator.ToWin32(color));
            IntPtr hOldFont = DrawOut.SelectObject(hDC, pen);

            foreach (PointLine d in Lines)
            {
                MoveToEx(hDC, d.A.X, d.A.Y, IntPtr.Zero);
                LineTo(hDC, d.B.X, d.B.Y);
            }

            DrawOut.DeleteObject(DrawOut.SelectObject(hDC, hOldFont));

        }
        public static void DrawRectangle(IntPtr hDC, Rectangle r, Color color, bool Nuritubusi)
        {
            IntPtr pen = CreatePen(PenStyle.PS_SOLID, 1, ColorTranslator.ToWin32(color));
            IntPtr hOldFont = DrawOut.SelectObject(hDC, pen);
            IntPtr brush = DrawOut.CreateSolidBrush((uint)ColorTranslator.ToWin32(color));
            IntPtr hOldBrush;
            if (Nuritubusi)
            {
                hOldBrush = DrawOut.SelectObject(hDC, brush);
                DrawOut.Rectangle(hDC, r.X, r.Y, r.Right, r.Bottom);
                DrawOut.DeleteObject(DrawOut.SelectObject(hDC, hOldBrush));
            }
            else
            {
                DrawOut.Rectangle(hDC, r.X, r.Y, r.Right, r.Bottom);
            }
            DrawOut.DeleteObject(DrawOut.SelectObject(hDC, hOldFont));

        }


        

    }
}
