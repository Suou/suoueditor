﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.IO;
namespace BSystem.Drawing
{
    public class BitmapThipDirManager
    {
        public DirectoryInfo Info { get; set; }
        public BitmapDirManagerSetting Setting { get; private set; }
        public BitmapThipDirManager(string foldername)
        {
            this.Info = new DirectoryInfo(foldername);
            if (this.Info.Exists)
            {
                this.Setting = BSystem.XML.NonBinarySerializer.Load<BitmapDirManagerSetting>(this.SettingPath);
                if (this.Setting == null) throw new Exception();
            }
        }
        public BitmapThipDirManager(string dirPath, Size ThipSize, Size MapCountSize, System.Drawing.Imaging.PixelFormat format)
        {
            this.Info = new DirectoryInfo(dirPath);

            if (!this.Info.Exists)
            {
                this.Info.Create();
                this.Setting = new BitmapDirManagerSetting()
                {
                    Format = format,
                    _Img = "jpg",
                    MapCountSize = MapCountSize,
                    TipSize = ThipSize,
                    LastBitMapFileTipsCount = 0,
                    MapFileCount = 0
                };
                this.AddNewMap().Dispose();
                BSystem.XML.NonBinarySerializer.Save(this.SettingPath, this.Setting);
            }
            else
            {
                this.Setting = BSystem.XML.NonBinarySerializer.Load<BitmapDirManagerSetting>(this.SettingPath);
                if (this.Setting == null)
                {
                    this.Setting = new BitmapDirManagerSetting()
                    {
                        Format = format,
                        _Img = "jpg",
                        MapCountSize = MapCountSize,
                        TipSize = ThipSize,
                        LastBitMapFileTipsCount = 0,
                        MapFileCount = 0
                    };
                    this.AddNewMap().Dispose();
                    BSystem.XML.NonBinarySerializer.Save(this.SettingPath, this.Setting);

                }
            }

        }
        public void Dispose()
        {
            BSystem.XML.NonBinarySerializer.Save(this.SettingPath, this.Setting);
        }
        private string SettingPath
        {
            get { return this.Info.FullName + "/" + "setting.xml"; }
        }
        private IEnumerable<Bitmap> MapFiles
        {
            get
            {
                var f = this.Info.GetFiles().ToList();
                f.Sort();
                foreach (var fs in f)
                {
                    yield return MainDrawingMethod.LoadFrom(fs.FullName, this.Setting.Format);
                }
            }
        }
        private IEnumerable<BitMapsItem> Maps
        {
            get
            {
                return this.MapFiles.Select((v) => new BitMapsItem(v, this.Setting.TipSize, this.Setting.TipSize));
            }
        }
        private IEnumerable<Bitmap> Thips
        {
            get
            {
                return this.Maps.SelectMany((v) => v.Maps);
            }
        }

        private string GetMapName(int i)
        {
            return string.Format("{1}/{0}.png", i, this.Info.FullName);
        }
        /*
        private FileInfo GetBitmapFile(int i)
        {
            throw new NotSupportedException();
        }
        */
        public BitThipMemory GetMap(int i)
        {
            var f = this.GetMapName(i);
            if (i == this.Setting.MapFileCount - 1)
            {
                return BitThipMemory.LoadFrom(f, this.Setting.TipSize, this.Setting.LastBitMapFileTipsCount, this.Setting.ImageFormat, this.Setting.Format);
            }
            else
            {
                return BitThipMemory.LoadFrom(f, this.Setting.TipSize, this.Setting.TipCountOfBitmap, this.Setting.ImageFormat, this.Setting.Format);
            }
        }
        public Bitmap GetThip(int i)
        {
            var n = i / this.Setting.TipCountOfBitmap;
            var mod = i % this.Setting.TipCountOfBitmap;
            var bmps = this.GetMap(i);
            return bmps.Maps[mod];
        }
        /*
        public void AddBitmapThip(Bitmap map)
        {
            var f = this.GetMap(this.Setting.MapFileCount-1);
            
            if (f.IsMax)
            {
                f = this.AddNewMap();
                
                f.Add(map);
            }
            else
            {
                f.Add(map);
            }
            this.Setting.LastBitMapFileTipsCount++;
            f.Dispose();
            
        }
        */
        public void AddRange(IEnumerable<Bitmap> maps)
        {
            var f = this.GetMap(this.Setting.MapFileCount - 1);
            foreach (var v in maps)
            {
                if (f.IsMax)
                {
                    f.Dispose();
                    f = this.AddNewMap();
                }
                f.Add(v);
                this.Setting.LastBitMapFileTipsCount++;
            }
            f.Dispose();
            BSystem.XML.NonBinarySerializer.Save(this.SettingPath, this.Setting);
        }
        public BitThipMemory AddNewMap()
        {
            this.Setting.LastBitMapFileTipsCount = 0;
            var name = this.GetMapName(this.Setting.MapFileCount);
            this.Setting.MapFileCount++;
            var bmp = new Bitmap(this.Setting.MapSize.Width, this.Setting.MapSize.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            var f = BitThipMemory.Create(name, this.Setting.TipSize, this.Setting.MapCountSize, this.Setting.Format, this.Setting.ImageFormat);
            return f;
        }
    }


    [Serializable]
    public class BitmapDirManagerSetting
    {
        public BitmapDirManagerSetting()
        {
        }
        public System.Drawing.Imaging.PixelFormat Format { get; set; }
        public string _Img;
        public System.Drawing.Imaging.ImageFormat ImageFormat
        {
            get
            {
                if (this._Img == "png")
                {
                    return System.Drawing.Imaging.ImageFormat.Png;
                }
                else if (this._Img == "jpg")
                {
                    return System.Drawing.Imaging.ImageFormat.Jpeg;
                }
                throw new Exception();
            }
            /*
            set
            {
                if (value == System.Drawing.Imaging.ImageFormat.Png)
                {
                    this._Img = "png";
                }
            }
            */
        }
        public Size TipSize { get; set; }
        public Size MapCountSize { get; set; }
        public int TipCountOfBitmap
        {
            get
            {
                return this.MapCountSize.Width * this.MapCountSize.Height;
            }
        }
        public int MapFileCount { get; set; }
        public int BitmapTileCount
        {
            get
            {
                return this.TipCountOfBitmap * (this.MapFileCount - 1) + this.LastBitMapFileTipsCount;
            }
        }
        public Size MapSize
        {
            get
            {
                return new Size(this.MapCountSize.Width * this.TipSize.Width, this.MapCountSize.Height * this.TipSize.Height);
            }
        }
        public int LastBitMapFileTipsCount { get; set; }
    }
}
