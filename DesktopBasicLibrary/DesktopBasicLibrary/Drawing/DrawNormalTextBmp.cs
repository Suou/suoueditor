﻿using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using BSystem.Drawing;
namespace BSystem.Drawing
{

    public static class DrawTextBmp
    {
        public static Bitmap CreateCharBitmap(char c,Font f)
        {
            Bitmap bmp = new Bitmap(f.Width(c), f.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            List<string> items = new List<string>() { c.ToString() };
            DrawString(bmp, items, Point.Empty, f, 0);
            return bmp;
        }

        public static Bitmap CreateBitmap(string s, Font font, int ySpaceSize)
        {
            return CreateBitmap(s, font, ySpaceSize, SystemColors.ControlText);
        }
        public static Bitmap CreateBitmap(string s, Font font, int ySpaceSize, Color col)
        {

            List<string> slist = s.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None).ToList();
            Size size = font.TextSize(slist, ySpaceSize);
            if (size.Width == 0) size.Width = 1;
            if (size.Height == 0) size.Height = 1;
            Bitmap bmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            bmp.MakeTransparent();
            //Nuritubusiをするかしないかは選択できるように
            TurboArgbPixelFilter32.FastFiltering(bmp, new BSystem.Drawing.PixelProc32(Nuritubusi));
            var rbmp = DrawString(bmp, slist, new Point(0, 0), font, ySpaceSize, col);

            return rbmp;
        }
        public static Bitmap CreateBitmapRapid(List<string> text, Font font, int ySpaceSize, Color col)
        {
            return CreateBitmapRapid(text, font, ySpaceSize, col, Color.White);
        }
        public static Bitmap CreateBitmapRapid(string s, Font font, int ySpaceSize, Color col)
        {
            return CreateBitmapRapid(s, font, ySpaceSize, col, Color.White);
        }
        public static Bitmap CreateBitmapRapid(List<string> text, Font font, int ySpaceSize, Color col, Color BackColor)
        {
            ColorDelegate ColDel = new ColorDelegate(BackColor);

            Size size = font.TextSize(text, ySpaceSize);
            if (size.Width == 0) size.Width = 1;
            if (size.Height == 0) size.Height = 1;
            Bitmap bmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            bmp.MakeTransparent();
            //Nuritubusiをするかしないかは選択できるように
            TurboArgbPixelFilter32.FastFiltering(bmp, new BSystem.Drawing.PixelProc32(ColDel.Nuritubusi));
            var rbmp = DrawOutString(bmp, text, new Point(0, 0), font, ySpaceSize, col);

            return rbmp;
        }
        public static Bitmap CreateBitmapRapid(string s, Font font, int ySpaceSize, Color col, Color BackColor)
        {
            List<string> slist = s.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None).ToList();
            return CreateBitmapRapid(slist, font, ySpaceSize, col, BackColor);
        }
        

        public static Bitmap CreateBitmap24(string s, Font font, int ySpaceSize, Color col)
        {
            List<string> slist = s.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None).ToList();
            Size size = font.TextSize(slist, ySpaceSize);
            if (size.Width == 0) size.Width = 1;
            if (size.Height == 0) size.Height = 1;
            Bitmap bmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            //bmp.MakeTransparent();
            //Nuritubusiをするかしないかは選択できるように
            if (bmp.PixelFormat != PixelFormat.Format24bppRgb) throw new Exception();
            //TurboArgbPixelFilter24.FastFiltering(bmp, new TurboArgbPixelFilter24.PixelProc(Nuritubusi24));
            var rbmp = DrawString(bmp, slist, new Point(0, 0), font, ySpaceSize, col);

            return rbmp;
        }
        public static Bitmap CreateBitmap16(List<string> text, Font font, int ySpaceSize, Color col,Color BackColor)
        {
            ColorDelegate ColDel = new ColorDelegate(BackColor);

            Size size = font.TextSize(text, ySpaceSize);
            if (size.Width == 0) size.Width = 1;
            if (size.Height == 0) size.Height = 1;
            Bitmap bmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            
            using (var g = Graphics.FromImage(bmp))
            {
                g.Clear(BackColor);
            }
            var rbmp = DrawString(bmp, text, new Point(0, 0), font, ySpaceSize, col);
            //var rbmp = DrawOutString(bmp, text, new Point(0, 0), font, ySpaceSize, col);

            return rbmp;
        }

        public static void Nuritubusi(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = 255;
            g = 255;
            r = 255;
            a = 255;
        }
        public static void Nuritubusi24(ref byte b, ref byte g, ref byte r)
        {
            b = 255;
            g = 255;
            r = 255;
        }
        public static bool SelectB = true;
        public static void Select(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = (byte)~b;
            g = (byte)~g;
            r = (byte)~r;

        }
        public static Bitmap Mask(Bitmap bmp)
        {
            var rbmp = new Bitmap(bmp);
            TurboArgbPixelFilter32.FastFiltering(rbmp, Select);
            return rbmp;
        }




        private static Graphics CreateSampleGraphic()
        {
            Bitmap bmp = new Bitmap(100, 160, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            Graphics gg = Graphics.FromImage(bmp);
            return gg;
        }
        private static Size CreateSampleSize(Font font)
        {
            return new Size(100, font.Height);
        }

        private static Bitmap DrawString(Bitmap bmp1, List<string> Text, Point StartPoint, Font font, int ySpaceSize)
        {
            return DrawString(bmp1, Text, StartPoint, font, ySpaceSize, SystemColors.ControlText);
        }
        private static Bitmap DrawString(Bitmap bmp1, List<string> Text, Point StartPoint, Font font, int ySpaceSize, Color col)
        {
            Point p = StartPoint;
            int basex = p.X;

            var g = Graphics.FromImage(bmp1);
            var size = CreateSampleSize(font);
            for (int y = 0; y < Text.Count; y++)
            {
                p.X = basex;
                //描画
                TextRenderer.DrawText(g, Text[y], font, p, col, TextFormatFlags.NoPadding | TextFormatFlags.NoPrefix);


                p.Y += font.Height + ySpaceSize;
            }

            return bmp1;
        }
        private static Bitmap DrawOutString(Bitmap bmp1, List<string> Text, Point StartPoint, Font font, int ySpaceSize, Color col)
        {
            Point p = StartPoint;
            int basex = p.X;

            var g = Graphics.FromImage(bmp1);
            var hInt = g.GetHdc();

            var size = CreateSampleSize(font);
            for (int y = 0; y < Text.Count; y++)
            {
                p.X = basex;
                //描画
                var hFont = font.ToHfont();
                DrawOut.Draw(hInt, hFont, p, Text[y], col);


                p.Y += font.Height + ySpaceSize;
            }
            g.ReleaseHdc();

            return bmp1;
        }
    }
}
