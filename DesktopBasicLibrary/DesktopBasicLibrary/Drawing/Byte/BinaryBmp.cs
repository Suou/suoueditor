﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Collections;
using BSystem.Structure;
namespace BSystem.Drawing.Byte
{
    public class BinaryBmp
    {
        public BitArray[] Items { get; set; }
        public BinaryBmp(bool[][] items)
        {
            this.Items = items.Select((v)=>new BitArray(v)).ToArray();
        }
        public BinaryBmp(BitArray[] items)
        {
            this.Items = items;
        }
        public BinaryBmp(Bitmap bmp)
        {
            if (bmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format32bppArgb)
            {
                this.Items = BSystem.Drawing.TurboArgbPixelFilter32.FastCopyStruct(bmp).Binarize();
            }
            else if (bmp.PixelFormat == System.Drawing.Imaging.PixelFormat.Format1bppIndexed)
            {
                this.Items = TurboFillter2.ToBools(bmp);
            }
            else
            {
                throw new Exception();
            }
        }
        private BinaryBmp(bool[][][] items)
        {
            this.Items = new BitArray[items[0].Length];
            for (int y = 0; y < this.Items.Length; y++)
            {
                this.Items[y] = this.GetBool(items, y);
            }
        }
        public BinaryBmp(BinaryBmp[] items,int yspace,int height,int width)
        {
            this.Items = new BitArray[height];
            for (int i = 0; i < height; i++) this.Items[i] = new BitArray(width);
            int y = 0;
            for (int i = 0; i < items.Length; i++)
            {
                y += yspace;
                Copy(this.Items, items[i].Items, 0, y);
                y += items[i].Height;
            }
        }
        private void Copy(BitArray[] source, BitArray[] item, int px, int py)
        {
            for (int y = 0; y < item.Length; y++)
            {
                for (int x = 0; x < item[y].Length; x++)
                {
                    source[py + y][px + x] = item[y][x];
                }
            }
        }
        private BitArray GetBool(bool[][][] items, int y)
        {
            var width = items.Sum((v) => v[0].Length);
            var r = new BitArray(width);
            int x = 0;
            for (int z = 0; z < items.Length; z++)
            {
                for (int i = 0; i < items[z][y].Length; i++)
                {
                    r[x++] = items[z][y][i];
                }
            }
            return r;
        }
        public static Bitmap CreateBinaryBitmap(Bitmap bmp){
            var b = new BinaryBmp(bmp);
            return b.ToBinaryBitmap();
        }
        public int ByteWidth
        {
            get
            {
                int xbound = (this.Items[0].Length + 32 - (this.Items[0].Length % 32)) / 8;
                return xbound;
            }
        }
        public int Width
        {
            get
            {
                return this.Items[0].Length;
            }
        }
        public int Height
        {
            get { return this.Items.Length; }
        }
        public byte[][] Iterator()
        {
            byte[][] pr = new byte[this.Height][];
            int t = 0;
            foreach (var v in this.Items)
            {
                pr[t++] = BSystem.ByteExtension.ToBytes(v, new byte[ByteWidth]);
            }
            return pr;
        }
        public override string ToString()
        {
            throw new Exception();
        }
        public Bitmap ToBinaryBitmap()
        {
            var r = new Bitmap(this.Width, this.Height, System.Drawing.Imaging.PixelFormat.Format1bppIndexed);
            BSystem.Drawing.TurboFillter2.FastFiltering(r, this.Iterator());
            var pare = r.Palette;
            pare.Entries[0] = Color.White;
            pare.Entries[1] = Color.Black;
            r.Palette = pare;
            return r;
        }
        public RLEBitmapCache ToRLEBitmapCache()
        {
            return new RLEBitmapCache(this);

        }
    }

}
