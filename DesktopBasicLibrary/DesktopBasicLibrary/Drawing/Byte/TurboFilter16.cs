﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Collections;
namespace BSystem.Drawing.Byte
{
    public static class TurboFilter16
    {
        private static bool CheckFormat16(this Bitmap bmp)
        {
            if (bmp.PixelFormat != PixelFormat.Format16bppRgb555)
            {
                throw new ArgumentException("bmpのPixelFormatはFormat16bppRgb555のみ有効です。", "bmp");
            }
            return true;
        }
        public static Pixel[][] FastFiltering(Bitmap bmp)
        {
            bmp.CheckFormat16();
            Pixel[][] r = new Pixel[bmp.Height][];
            
            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadWrite, PixelFormat.Format16bppRgb555);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                for (int y = 0; y < bmp.Height; y++)
                {
                    int t=0;
                    r[y] = new Pixel[bmp.Width];
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        r[y][x] = new Pixel(*(pixel++), *(pixel++));
                        t += 2;
                    }
                    while (t < bmpData.Stride)
                    {
                        pixel++;
                        t++;
                    }
                }
            }

            bmp.UnlockBits(bmpData);
            return r;
        }
        public static Pixel[][] FastFilteringTo2(Bitmap bmp)
        {
            bmp.CheckFormat16();
            Pixel[][] r = new Pixel[bmp.Height][];

            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadWrite, PixelFormat.Format16bppRgb555);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                for (int y = 0; y < bmp.Height; y++)
                {
                    int t = 0;
                    r[y] = new Pixel[bmp.Width];
                    for (int x = 0; x < bmp.Width; x++)
                    {
                        r[y][x] = new Pixel(*(pixel++) != 255);
                        pixel++;
                        t += 2;
                    }
                    while (t < bmpData.Stride)
                    {
                        pixel++;
                        t++;
                    }
                }
            }

            bmp.UnlockBits(bmpData);
            return r;
        }
        /*
        private static bool RapidTranslate(byte a,byte b)
        {
            return a == 0;
        }
        */
        public static Bitmap ToBitmap2(Bitmap bmp)
        {
            return ToBinaryBmp(bmp).ToBinaryBitmap();
        }
        public static BinaryBmp ToBinaryBmp(Bitmap bmp)
        {
            var p = FastFilteringTo2(bmp);
            var bools = new BitArray[p.Length];
            for (int y = 0; y < p.Length; y++)
            {
                bools[y] = new BitArray(p[y].Length);
                for (int x = 0; x < p[y].Length; x++)
                {
                    bools[y][x] = p[y][x].IsBlack;
                }
            }
            var r = new BinaryBmp(bools);
            return r;
        }
        public static RLEBitmapCache ToRLEBitmapCache(Bitmap bmp)
        {
            return ToBinaryBmp(bmp).ToRLEBitmapCache();
        }

    }
}
