﻿
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Collections;
namespace BSystem.Drawing
{
    //public delegate void PixelProc(ref bool b);
    public delegate bool TranslatePixelProc(ref byte b, ref byte g, ref byte r, ref byte a);


    public static class TurboFillter2
    {
        private static bool TranslateProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            return b == 255;
        }
        private static bool pInd=false;
        private static void InverseTranslateProc(ref bool b)
        {
            b = pInd;
            pInd = !pInd;
        }

        public static void FastFiltering(Bitmap bmp, byte[][] copyItem)
        {
            bmp.CheckFormat2();
            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size),ImageLockMode.ReadWrite,PixelFormat.Format1bppIndexed);
            var bound = bmpData.Stride * bmpData.Height;
            int t = 0;
            unsafe
            {
                
                byte* pixel = (byte*)bmpData.Scan0;
                for (int y = 0; y < bmpData.Height; y++)
                {
                    for (int x = 0; x < bmpData.Stride; x++)
                    {
                        *(pixel++) = copyItem[y][x];
                        t++;
                    }
                }
            }
            if (bound != t) throw new Exception();
            bmp.UnlockBits(bmpData);
        }
        public static BitArray[] ToBools(Bitmap bmp)
        {
            bmp.CheckFormat2();
            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadWrite, PixelFormat.Format1bppIndexed);
            var r = new BitArray[bmpData.Height];
            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                for (int y = 0; y < bmpData.Height;y++)
                {
                    r[y] = new BitArray(bmpData.Stride * 8);
                    for (int x = 0; x < bmpData.Stride; x++)
                    {
                        BSystem.ByteExtension.ToBools(*(pixel++), r[y], x * 8);
                    }
                }
            }

            bmp.UnlockBits(bmpData);
            return Copy(r,bmp);
            //return r;
        }
        private static BitArray[] Copy(BitArray[] item, Bitmap bmp)
        {
            BitArray[] r = new BitArray[bmp.Height];
            for(int y=0;y<bmp.Height;y++){
                r[y] = (BitArray)item[y].Clone();
            }
            return r;
        }
        private static unsafe void SetIndexedPixel(int x, int y, BitmapData bmd, bool pixel)
        {
            byte* p = (byte*)bmd.Scan0.ToPointer();
            int index = y * bmd.Stride + (x >> 3);
            byte mask = (byte)(0x80 >> (x & 0x7));

            if (pixel)
                p[index] |= mask;
            else
                p[index] &= (byte)(mask ^ 0xff);
        }
        public static Bitmap ToPixelFormat2(this Bitmap bmp)
        {
            var r = new Bitmap(bmp.Width, bmp.Height, PixelFormat.Format1bppIndexed);
            var p = r.Palette;
            p.Entries[0] = Color.Black;
            p.Entries[1] = Color.White;
            r.Palette = p;

            //FastFiltering(r, InverseTranslateProc);
            //return r;
            

            bmp.CheckFormat32();
            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData rData = r.LockBits(new Rectangle(Point.Empty, r.Size), ImageLockMode.ReadWrite, PixelFormat.Format1bppIndexed);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                bool* rpixel = (bool*)rData.Scan0;

                int dataLength = rData.Stride * rData.Height;
                bool getb;
                for (int x = 0; x < r.Width; x++)
                {
                    for (int y = 0; y < r.Height; y++)
                    {
                        getb = TranslateProc(ref *(pixel++), ref *(pixel++), ref *(pixel++), ref *(pixel++));
                        SetIndexedPixel(x, y, rData, getb);
                    }
                }

            }
            r.UnlockBits(rData);
            bmp.UnlockBits(bmpData);
            return r;
        }
        private static bool CheckFormat2(this Bitmap bmp)
        {
            if (bmp.PixelFormat != PixelFormat.Format1bppIndexed)
            {
                throw new ArgumentException("bmpのPixelFormatはFormat2bppArgbのみ有効です。", "bmp");
            }
            return true;
        }

        public static Bitmap Create1bppImage(Bitmap img)
        {
            //1bppイメージを作成する
            Bitmap newImg = new Bitmap(img.Width, img.Height,
                PixelFormat.Format1bppIndexed);

            //Bitmapをロックする
            BitmapData bmpDate = newImg.LockBits(
                new Rectangle(0, 0, newImg.Width, newImg.Height),
                ImageLockMode.WriteOnly, newImg.PixelFormat);

            ////アンセーフコードを使うと、以下のようにもできる
            unsafe
            {
                byte* pixelPtr = (byte*)bmpDate.Scan0;
                for (int y = 0; y < bmpDate.Height; y++)
                {
                    for (int x = 0; x < bmpDate.Width; x++)
                    {
                        //明るさが0.5以上の時は白くする
                        if (0.5f <= img.GetPixel(x, y).GetBrightness())
                        {
                            //ピクセルデータの位置
                            int pos = (x >> 3) + bmpDate.Stride * y;
                            //白くする
                            pixelPtr[pos] |= (byte)(0x80 >> (x & 0x7));
                        }
                    }
                }
            }
            //作成したピクセルデータをコピーする
            /*
            IntPtr ptr = bmpDate.Scan0;
            System.Runtime.InteropServices.Marshal.Copy(pixels, 0, ptr, pixels.Length);
            */

            

            //ロックを解除する
            newImg.UnlockBits(bmpDate);

            return newImg;
        }
    }
}
