﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using BSystem.Structure;
using System.IO;
namespace BSystem.Drawing.Byte
{
    public class RLEBitmapCache:BitmapRLE
    {
        public RLEBitmapCache(BinaryBmp bmp):base(bmp.Items)
        {

        }
        public Bitmap CreateBitmap()
        {
            var p = new BinaryBmp(this.CreateBitArrays());
            return p.ToBinaryBitmap();
        }
        public void Save(FileInfo fi)
        {
            this.SaveBinary(new StreamWriter(fi.OpenWrite()));
        }
    }
}
