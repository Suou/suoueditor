﻿using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BSystem.Drawing;
using BSystem.Drawing.Pixel32;
using System.IO;
namespace BSystem.Drawing
{
    public static class MainDrawingMethod
    {
        public static Bitmap LoadFrom(string filename, System.Drawing.Imaging.PixelFormat format)
        {
            var r = LoadBitmap(filename);
            if (r.PixelFormat != format)
            {
                var r2 = new Bitmap(r.Width, r.Height, format);
                using (var g = Graphics.FromImage(r2))
                {
                    g.DrawImage(r, Point.Empty);
                }
                return r2;
            }
            else
            {
                return (Bitmap)r;
            }
        }
        public static Bitmap LoadBitmap(string fileName)
        {
            byte[] buffer = File.ReadAllBytes(fileName);

            MemoryStream stream = new MemoryStream(buffer);

            return (Bitmap)Image.FromStream(stream);
        }
        public static Bitmap Reverse(Bitmap bmp)
        {
            
            if (bmp.PixelFormat == PixelFormat.Format32bppArgb)
            {
                return TurboArgbPixelFilter32.InverseFiltering(bmp);
            }
            else if (bmp.PixelFormat == PixelFormat.Format24bppRgb)
            {
                var bmp2 = bmp.Clone(new RectangleF(0,0,bmp.Width,bmp.Height),PixelFormat.Format32bppArgb);

                return TurboArgbPixelFilter32.InverseFiltering(bmp2);
            }
            else if (bmp.PixelFormat == PixelFormat.Format16bppRgb555)
            {
                var bmp2 = bmp.Clone(new RectangleF(0, 0, bmp.Width, bmp.Height), PixelFormat.Format32bppArgb);

                return TurboArgbPixelFilter32.InverseFiltering(bmp2);
            }
            else
            {
                throw new Exception();
            }
        }
        public static Bitmap Dye(Bitmap bmp,Color from, Color to)
        {

            if (bmp.PixelFormat == PixelFormat.Format32bppArgb)
            {
                var func = new FilterProc() { FromPixel = new Pixel(from), ToPixel = new Pixel(to) };
                var bmp2 = bmp.Clone(new RectangleF(0, 0, bmp.Width, bmp.Height), PixelFormat.Format32bppArgb);
                TurboArgbPixelFilter32.FastFiltering(bmp2,func.DyeProc);
                return bmp2;
            }
            else if (bmp.PixelFormat == PixelFormat.Format24bppRgb)
            {
                
                var bmp2 = bmp.Clone(new RectangleF(0, 0, bmp.Width, bmp.Height), PixelFormat.Format24bppRgb);

                var func = new BSystem.Drawing.Pixel24.FilterProc24() { FromPixel = new Pixel(from), ToPixel = new Pixel(to) };
                BSystem.Drawing.Pixel24.TurboArgbPixelFilter24.FastFiltering(bmp2,func.DyeProc);
                return bmp2;
                
            }
            else if (bmp.PixelFormat == PixelFormat.Format16bppRgb555)
            {
                throw new NotSupportedException();
            }
            else
            {
                throw new Exception();
            }
        }
    }
}
