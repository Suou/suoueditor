﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;

namespace SuouBase.Text
{
    public class DotSizeApi
    {
        
        public DotSizeApi()
        {
        }
        [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
        public static extern bool TextOut(IntPtr hdc, int nXStart, int nYStart, string lpString, int cbString);
        [DllImport("gdi32.dll")]
        public static extern bool DeleteObject(IntPtr hObject);
        [DllImport("gdi32.dll", ExactSpelling = true, PreserveSig = true, SetLastError = true)]
        public static extern IntPtr SelectObject(IntPtr hdc, IntPtr hgdiobj);
        [DllImport("gdi32.dll")]
        public static extern int SetBkMode(IntPtr hdc, int iBkMode);
        [DllImport("gdi32.dll")]
        public static extern uint SetTextColor(IntPtr hdc, int crColor);
        [DllImport("gdi32.dll")]
        private static extern bool LineTo(IntPtr hdc, int nXEnd, int nYEnd);
        [DllImport("gdi32.dll")]
        private static extern bool MoveToEx(IntPtr hdc, int X, int Y, IntPtr lpPoint);
        [DllImport("gdi32.dll")]
        static extern bool Rectangle(IntPtr hdc, int nLeftRect, int nTopRect, int nRightRect, int nBottomRect);
        [DllImport("gdi32.dll")]
        static extern IntPtr CreateSolidBrush(uint crColor);
        [DllImport("gdi32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr GetTextExtentPoint32(IntPtr hdc, string lpString, int cbString, ref tagSize lpSize);

        public static int TRANSPARENT = 1;

        static Graphics DG = Graphics.FromImage(new Bitmap(300, 300));
        private static IntPtr hd = DG.GetHdc();
        private static Font GFont = null;
        private static IntPtr hOldFont;


        public struct tagSize
        {
            public int cx;
            public int cy;
        }
        public static Size MeasureLength(string str,Font argf)
        {
            if (GFont == null)
            {
                //IntPtr hd = DG.GetHdc();
                GFont = argf;
                var hFont = argf.ToHfont();

                hOldFont = SelectObject(hd, hFont);
                SetBkMode(hd, TRANSPARENT);
                //DeleteObject(SelectObject(hd, hOldFont));
                //DG.ReleaseHdc();
                return MeasureLength(hd, str);
            } 
            else if (!argf.Equals(GFont))
            {
                DeleteObject(SelectObject(hd, hOldFont));
                GFont = argf;
                var hFont = argf.ToHfont();

                hOldFont = SelectObject(hd, hFont);
                SetBkMode(hd, TRANSPARENT);

                return MeasureLength(hd, str);
            }
            else
            {
                return MeasureLength(hd, str);
            }
        }
        private static Size MeasureLength(IntPtr hDC, string str)
        {

            int len = str.Length;

            tagSize ts = new tagSize();
            GetTextExtentPoint32(hDC, str, len, ref ts);
            var f = new Size(ts.cx, ts.cy);
            return f;
        }
    }
}
