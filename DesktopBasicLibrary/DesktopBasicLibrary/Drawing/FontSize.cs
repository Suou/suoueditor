﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace BSystem.Drawing
{
    public static class FontSize
    {
        public static Graphics g = Graphics.FromImage(new Bitmap(200, 200));
        public static int Width(Font f, char c)
        {
            return FontSize.Width(f, c.ToString());
        }
        public static int Width(Font f, string str)
        {
            if (str == null) throw new ArgumentNullException();
            if (str.Length == 0) return 0;
            //Graphics g = Graphics.FromImage(new Bitmap(200, 200));
            if (g == null) throw new ArgumentNullException();
            if (f == null) throw new ArgumentNullException();

            var tf = TextFormatFlags.NoPrefix | TextFormatFlags.NoPadding;
            var z = TextRenderer.MeasureText(g, str, f, new Size(f.Height, f.Height), tf);
            return z.Width;
        }
        public static Size TextSize(Font f, List<string> text, int yinterval)
        {
            //Graphics g = Graphics.FromImage(new Bitmap(200, 200));
            var r = Size.Empty;
            foreach (string s in text)
            {
                var z = FontSize.Width(f, s);
                if (r.Width < z) r.Width = z;
                r.Height += f.Height + yinterval;
            }
            r.Height -= yinterval;
            return r;
        }


    }
}
