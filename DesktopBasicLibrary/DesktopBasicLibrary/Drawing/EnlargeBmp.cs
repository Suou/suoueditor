﻿using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using BSystem.Drawing;
namespace BSystem.Drawing
{


    public class EnlargeBmp
    {
        public static Bitmap NotEnlargeBitmap(Bitmap bmp, Size ImageSize, Color Haikei)
        {
            Bitmap rbmp = new Bitmap(ImageSize.Width, ImageSize.Height, PixelFormat.Format32bppArgb);
            var g = Graphics.FromImage(rbmp);
            g.Clear(Haikei);
            g.DrawImage(bmp, new Point(rbmp.Width - bmp.Width, rbmp.Height - bmp.Height));
            g.Dispose();
            return rbmp;
        }

        public static Bitmap EnlargeBitmap(Bitmap bmp, float percent)
        {
            Size size = new Size();
            size.Width = (int)(bmp.Width * percent);
            size.Height = (int)(bmp.Height * percent);
            if (size.Width < 1) size.Width = 1;
            if (size.Height < 1) size.Height = 1;
            return EnlargeBitmap(bmp, size);
        }
        public static Bitmap EnlargeBitmap(Bitmap bmp, float percent, System.Drawing.Drawing2D.InterpolationMode mode)
        {
            Size size = new Size();
            size.Width = (int)(bmp.Width * percent);
            size.Height = (int)(bmp.Height * percent);
            if (size.Width < 1) size.Width = 1;
            if (size.Height < 1) size.Height = 1;
            return EnlargeBitmap(bmp, size, size, mode);
        }
        public enum RegularMode
        {
            Over, Inter
        }
        public static Bitmap RegularEnlargeBitmap(Bitmap bmp, Size size, RegularMode rm)
        {
            var a = (float)size.Width / (float)bmp.Width;
            var b = (float)size.Height / (float)bmp.Height;
            float c = 0;
            if (rm == RegularMode.Inter)
            {
                c = a < b ? a : b;
            }
            else if (rm == RegularMode.Over)
            {
                c = a < b ? b : a;
            }
            var bmp2 = EnlargeBitmap(bmp, c);
            return EnlargeBitmap(bmp2, size, bmp2.Size, System.Drawing.Drawing2D.InterpolationMode.Low);
            /*
            var rbmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            TurboArgbPixelFilter.FastFiltering(rbmp, Nuritubusi);
            var g = Graphics.FromImage(rbmp);
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            g.DrawImage(bmp2, 0, 0,bmp2.Width,bmp2.Height);
            return rbmp;
             */
        }

        public static Bitmap EnlargeBitmap(Bitmap bmp, Size AfterSize, Size AfterInSize, System.Drawing.Drawing2D.InterpolationMode col)
        {
            var rbmp = new Bitmap(AfterSize.Width, AfterSize.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            if (!AfterSize.Equals(AfterSize)) TurboArgbPixelFilter32.FastFiltering(rbmp, DrawTextBmp.Nuritubusi);
            var g = Graphics.FromImage(rbmp);
            g.InterpolationMode = col;
            g.DrawImage(bmp, 0, 0, AfterInSize.Width, AfterInSize.Height);
            return rbmp;

        }

        public static Bitmap EnlargeBitmap(Bitmap bmp, Size size)
        {
            return EnlargeBitmap(bmp, size, size, System.Drawing.Drawing2D.InterpolationMode.Low);
        }

    }
}
