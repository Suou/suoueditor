﻿
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BSystem.Collections;


namespace BSystem.Drawing
{

    public class FilterProc
    {
        public Pixel FromPixel { get; set; }
        public Pixel ToPixel { get; set; }
        public void DyeProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            var pix = new Pixel(r, g, b, a);
            if (pix.Equals(FromPixel))
            {
                b = ToPixel.b;
                g = ToPixel.g;
                r = ToPixel.r;
                a = ToPixel.a;
            }
        }
    }
    public static class TurboArgbPixelFilter32
    {
        public static void InverseProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = (byte)~b;
            g = (byte)~g;
            r = (byte)~r;
        }
        public static Color[] GetPixels(this Bitmap bmp)
        {
            var r = new Color[bmp.Width*bmp.Height];
            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            BitmapData bmpData = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppArgb);

            int x = 0;
            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                for (int i = 0; i < dataLength; i += 4)
                {
                    r[x++] = (Color.FromArgb((int)*(pixel++), (int)*(pixel++), (int)*(pixel++), (int)*(pixel++)));
                }
            }

            bmp.UnlockBits(bmpData);
            return r;
        }
        public static Color[][] GetRectanglePixels(this Bitmap bmp)
        {
            var pics = bmp.GetPixels();
            //var r = Collection.ArrayExtensions.Create<Color>(bmp.Width, bmp.Height);
            //return ;
            return pics.To2D(bmp.Width);
        }
        public static Color[] GetBorderPixels(this Bitmap bmp)
        {
            var pics = bmp.GetRectanglePixels();
            List<Color> r = new List<Color>();
            for (int y = 0; y < bmp.Height; y++)
            {
                for (int x = 0; x < bmp.Width; x++)
                {
                    if (y == 0 || y == bmp.Height - 1)
                    {
                        r.Add(pics[y][x]);
                    }
                    else if (x == 0)
                    {
                        r.Add(pics[y][x]);
                        x += bmp.Width - 1;
                        r.Add(pics[y][x]);
                    }                    
                }
            }
            return r.ToArray();
        }
        public static bool CheckFormat32(this Bitmap bmp)
        {
            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException("bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }
            return true;
        }
        /// <summary>
        /// 画像のすべてのピクセルをループして指定した処理をする。
        /// </summary>
        /// <param name="bmp">処理対象の画像。PixelFormat.Format32bppArgbのみ対応。</param>
        /// <param name="proc">ピクセルごとに行う処理。1ピクセルごとに呼び出される。</param>

        #region FastFilter
        public static Bitmap InverseFiltering(Bitmap bmp)
        {
            var bmp2 = (Bitmap)bmp.Clone();
            FastFiltering(bmp2, InverseProc);
            return bmp2;
        }
        public static void FastFiltering(Bitmap bmp, PixelProc32 proc)
        {
            bmp.CheckFormat32();
            BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                for (int i = 0; i < dataLength; i += 4)
                {
                    proc(ref *(pixel++), ref *(pixel++), ref *(pixel++), ref *(pixel++));
                }
            }

            bmp.UnlockBits(bmpData);
        }
        public static void FastFilteringRectangle(Bitmap bmp, PixelProc32 proc, Rectangle rec)
        {
            bmp.CheckFormat32();

            var bmprec = new Rectangle(Point.Empty, bmp.Size);
            rec.Intersect(bmprec);
            if (rec.Size.IsEmpty) return;

            BitmapData bmpData = bmp.LockBits(rec, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;

                for (int y = 0; y < rec.Height; y++)
                {

                    for (int x = 0; x < rec.Width; x++)
                    {
                        proc(ref *(pixel++), ref *(pixel++), ref *(pixel++), ref *(pixel++));
                    }
                    pixel += (bmp.Width - rec.Width) * 4;
                }
            }

            bmp.UnlockBits(bmpData);
        }
        public static Bmpstr FastCopyStruct(Bitmap bmp)
        {

            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            BitmapData bmpData = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppArgb);

            Bmpstr bs = new Bmpstr(bmp.Width, bmp.Height);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                for (int i = 0; i < dataLength; i += 4)
                {
                    Pixel p = new Pixel(*(pixel++), *(pixel++), *(pixel++), *(pixel++));
                    bs.add(p);
                }
            }

            bmp.UnlockBits(bmpData);
            return bs;
        }
        public static Pixel FastGetPixel(Bitmap bmp, Point p)
        {

            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            BitmapData bmpData = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppArgb);

            Pixel gel;

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                pixel += (((p.Y * bmp.Width) + p.X) * 4);
                gel = new Pixel(*(pixel++), *(pixel++), *(pixel++), *(pixel++));
            }

            bmp.UnlockBits(bmpData);
            return gel;
        }
        #endregion

        public static KeyValuePair<Rectangle, Rectangle> ModifyRectangle(Bitmap bmp1, Bitmap bmp2, Point StartLocation1, Point StartLocation2, Size DrawAreaSize)
        {

            var x1 = StartLocation1.X < 0 ? StartLocation1.X : 0;
            var x2 = StartLocation2.X < 0 ? StartLocation2.X : 0;
            var x3 = x1 < x2 ? x1 : x2;

            var y1 = StartLocation1.Y < 0 ? StartLocation1.Y : 0;
            var y2 = StartLocation2.Y < 0 ? StartLocation2.Y : 0;
            var y3 = y1 < y2 ? y1 : y2;

            var p1 = new Point(StartLocation1.X - x3, StartLocation1.Y - y3);
            var p2 = new Point(StartLocation2.X - x3, StartLocation2.Y - y3);
            var sz = new Size(DrawAreaSize.Width + x3, DrawAreaSize.Height + y3);

            var bmprec1 = new Rectangle(new Point(0, 0), bmp1.Size);
            var bmprec2 = new Rectangle(new Point(0, 0), bmp2.Size);
            var rec1 = new Rectangle(p1, sz);
            var rec2 = new Rectangle(p2, sz);
            rec1.Intersect(bmprec1);
            rec2.Intersect(bmprec2);




            var szr = new Size(rec1.Width < rec2.Width ? rec1.Width : rec2.Width, rec1.Height < rec2.Height ? rec1.Height : rec2.Height);

            var rec3 = new Rectangle(rec1.Location, szr);
            var rec4 = new Rectangle(rec2.Location, szr);
            return new KeyValuePair<Rectangle, Rectangle>(rec3, rec4);
        }
        public static KeyValuePair<Rectangle, Rectangle> ModifyRectangle2(Bitmap bmp1, Bitmap bmp2, Rectangle rec1, Rectangle rec2)
        {
            Rectangle rrec1, rrec2;
            if (rec1.Contains(rec2))
            {
                rrec1 = rec2;
                rrec2 = new Rectangle(Point.Empty, rec2.Size);

            }
            else if (rec2.Contains(rec1))
            {
                rrec1 = new Rectangle(Point.Empty, rec1.Size);
                rrec2 = rec1;
            }
            else if (rec1.IntersectsWith(rec2))
            {
                var vrec = Rectangle.Intersect(rec1, rec2);

                var p1 = new Point(vrec.X - rec1.X, vrec.Y - rec1.Y);
                rrec1 = new Rectangle(p1, vrec.Size);

                var p2 = new Point(vrec.X - rec2.X, vrec.Y - rec2.Y);
                rrec2 = new Rectangle(p2, vrec.Size);

            }
            else
            {
                rrec1 = Rectangle.Empty;
                rrec2 = Rectangle.Empty;
            }
            return new KeyValuePair<Rectangle, Rectangle>(rrec1, rrec2);

        }

        #region DrawImage
        public static void DrawImage(Bitmap destBmp, Bitmap srcBmp, Point srcStartLocation, Size DrawAreaSize)
        {
            if (destBmp.Size == srcBmp.Size)
            {
                var a = new Rectangle(srcStartLocation, DrawAreaSize);
                var b = new Rectangle(new Point(0, 0), destBmp.Size);
                var newrec = Rectangle.Intersect(a, b);
                DrawImage2(destBmp, srcBmp, newrec.Location, newrec.Location, newrec.Size);
            }
            else
            {
                throw new Exception();
                //DrawImage2(destBmp, srcBmp, srcStartLocation, srcStartLocation, DrawAreaSize);
            }
        }
        public static void DrawImage(Bitmap destBmp, Bitmap srcBmp, Point destStartLocation, Point srcStartLocation, Size DrawAreaSize)
        {
            var k = ModifyRectangle(destBmp, srcBmp, destStartLocation, srcStartLocation, DrawAreaSize);

            if (k.Key.Size.IsEmpty) return;
            DrawImage2(destBmp, srcBmp, k.Key.Location, k.Value.Location, k.Key.Size);

        }
        public static void DrawImage(Bitmap destBmp, Bitmap srcBmp, Point srcStartLocation)
        {
            var rec = new Rectangle(Point.Empty, destBmp.Size);
            var rec2 = new Rectangle(srcStartLocation, srcBmp.Size);
            var emp = ModifyRectangle2(destBmp, srcBmp, rec, rec2);
            if (emp.Key.Size.IsEmpty || emp.Value.Size.IsEmpty) return;
            DrawImage2(destBmp, srcBmp, emp.Key.Location, emp.Value.Location, emp.Key.Size);
        }
        private static void DrawImage2(Bitmap destBmp, Bitmap srcBmp, Point destStartLocation1, Point srcStartLocation2, Size DrawAreaSize)
        {

            if (destBmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }
            Color ToukaColor = Color.FromArgb(255, 255, 255, 1);
            BitmapData bmpData1 = destBmp.LockBits(new Rectangle(destStartLocation1, DrawAreaSize), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData bmpData2 = srcBmp.LockBits(new Rectangle(srcStartLocation2, DrawAreaSize), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* pixel1 = (byte*)bmpData1.Scan0;
                byte* pixel2 = (byte*)bmpData2.Scan0;

                {
                    for (int y = 0; y < DrawAreaSize.Height; y++)
                    {
                        for (int x = 0; x < DrawAreaSize.Width; x++)
                        {
                            ToukaCopy(ref *(pixel1++), ref *(pixel1++), ref *(pixel1++), ref *(pixel1++), ref  *(pixel2++), ref  *(pixel2++), ref  *(pixel2++), ref  *(pixel2++), ToukaColor);
                            /*
                            for (int z = 0; z < 4; z++)
                            {
                                *(pixel1) = *(pixel2);
                                pixel1++; pixel2++;
                            }
                            */
                        }
                        pixel1 += (destBmp.Width - DrawAreaSize.Width) * 4;
                        pixel2 += (srcBmp.Width - DrawAreaSize.Width) * 4;
                    }
                }
            }
            destBmp.UnlockBits(bmpData1);
            srcBmp.UnlockBits(bmpData2);
        }

        private static void DrawImage(BitmapData bmpData1, Bitmap bmp2, Point StartLocation)
        {

            if (bmpData1.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }


            BitmapData bmpData2 = bmp2.LockBits(new Rectangle(Point.Empty, bmp2.Size), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);


            unsafe
            {
                byte* pixel1 = (byte*)bmpData1.Scan0;
                byte* pixel2 = (byte*)bmpData2.Scan0;

                {
                    int dataLength = bmpData1.Stride * bmpData1.Height;

                    pixel1 += (((StartLocation.Y * bmpData1.Width)) * 4);

                    for (int y = 0; y < bmp2.Height; y++)
                    {
                        pixel1 += StartLocation.X * 4;
                        for (int x = 0; x < bmp2.Width; x++)
                        {
                            pixel2 += 3;
                            if (*(pixel2) == 0)
                            {
                                pixel1 += 4;
                                pixel2++;
                            }
                            else
                            {
                                pixel2 -= 3;
                                for (int z = 0; z < 4; z++)
                                {
                                    *(pixel1) = *(pixel2);
                                    pixel1++; pixel2++;
                                }
                            }
                        }
                        pixel1 += (bmpData1.Width - bmpData2.Width - StartLocation.X) * 4;
                    }
                }
            }
            //bmp1.UnlockBits(bmpData1);
            bmp2.UnlockBits(bmpData2);
        }

        public static Bitmap Cut(this Bitmap bmp1, Rectangle rec)
        {
            var r = new Bitmap(rec.Width, rec.Height, bmp1.PixelFormat);
            DrawImage(r, bmp1, Point.Empty, rec.Location, rec.Size);
            return r;
            //DrawImage(bmp)
        }
        #endregion
        /// <summary>
        /// ImageからPixelFormat32pbbArgbのBitmapを作成して返します。
        /// </summary>
        /// <returns>PixelFormat32pbbArgbのBitmap</returns>
        public static Bitmap GetRegularizedBitmap(Image img)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.DrawImage(img, 0, 0, img.Width, img.Height);
            }

            return bmp;
        }
        public static void ToukaCopy(ref byte b1, ref byte g1, ref byte r1, ref byte a1, ref byte b2, ref byte g2, ref byte r2, ref byte a2, Color c)
        {

            var c2 = Color.FromArgb(a2, r2, g2, b2);
            if (c != c2)
            {
                b1 = b2;
                g1 = g2;
                r1 = r2;
                a1 = a2;
            }
        }
        
    }
    public delegate void PixelProc32(ref byte b, ref byte g, ref byte r, ref byte a);
    


}