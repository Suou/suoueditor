﻿using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BSystem.Drawing;
namespace BSystem.Drawing.Pixel32
{
    public static class SubDrawMethod
    {
        public static IEnumerable<Bitmap> Cuts(this Bitmap map,Size bitmapSize,Size diffSize)
        {
            for (int y = 0; y < map.Height - bitmapSize.Height; y += diffSize.Height)
            {
                for (int x = 0; x < map.Width - bitmapSize.Width; x += diffSize.Width)
                {
                    yield return map.Cut(new Rectangle(x, y, bitmapSize.Width, bitmapSize.Height));
                }
            }
        }
        
    }
}
