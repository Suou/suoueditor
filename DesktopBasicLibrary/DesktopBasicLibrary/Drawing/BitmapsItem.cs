﻿using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BSystem.Drawing;
using BSystem.Drawing.Pixel32;
using System.IO;

namespace BSystem.Drawing
{
    public class BitThipMemory : BitMapsItem
    {
        public BitThipMemory(string filename, Size thipSize, int count, System.Drawing.Imaging.ImageFormat format, System.Drawing.Imaging.PixelFormat format2)
            : base(thipSize,format2)
        {
            this.Info = new FileInfo(filename);
            this.Format = format;
            this.Format2 = format2;
            var bmp = (Bitmap)MainDrawingMethod.LoadFrom(filename,format2);
            this.Maps = bmp.Cuts(thipSize, thipSize).Take(count).ToList();
            var widthCount = bmp.Width / thipSize.Width;
            var heightCount = bmp.Height / thipSize.Height;
            this.CountSize = new Size(widthCount, heightCount);
        }
        public static BitThipMemory Create(string filename, Size thipSize, Size mapCountSize, System.Drawing.Imaging.PixelFormat format, System.Drawing.Imaging.ImageFormat format2)
        {
            var bmp = new Bitmap(thipSize.Width * mapCountSize.Width, thipSize.Height * mapCountSize.Height, format);
            bmp.Save(filename,format2);
            return new BitThipMemory(filename, thipSize, 0, format2,format);
        }
        public System.Drawing.Imaging.ImageFormat Format { get; set; }
        public Size CountSize { get; set; }
        public FileInfo Info { get; set; }
        public bool Lock { get; set; }
        public int MaxCount
        {
            get { return this.CountSize.Width * this.CountSize.Height; }
        }
        public bool IsMax
        {
            get
            {
                return this.Maps.Count == this.MaxCount;
            }
        }
        public override void Add(Bitmap map)
        {
            if (this.Lock || this.IsMax) throw new Exception();
            base.Add(map);
        }
        private void Save()
        {
            using (var bmp = CreateSingleBitmap(this.Maps, this.CountSize.Width, this.CountSize.Height, this.TipSize, this.Format2))
            {
                bmp.Save(this.Info.FullName, this.Format);
            }
        }
        public static BitThipMemory LoadFrom(string path, Size sz,int count,System.Drawing.Imaging.ImageFormat format,System.Drawing.Imaging.PixelFormat format2)
        {
            return new BitThipMemory(path, sz, count, format,format2);
        }
        public void Dispose()
        {
            this.Lock = true;
            this.Save();
            this.Maps.ForEach((v) => v.Dispose());
        }
    }
    public class BitMapsItem
    {
        public BitMapsItem(Size sz,PixelFormat format)
        {
            this.Format2 = format;
            this.TipSize = sz;
            this.Maps = new List<Bitmap>();
        }
        public BitMapsItem(Bitmap map, Size sz, Size diffSz)
        {
            this.Format2 = map.PixelFormat;
            this.Maps = map.Cuts(sz, diffSz).ToList();
        }
        public System.Drawing.Imaging.PixelFormat Format2 { get; set; }
        public Size TipSize { get; set; }
        public List<Bitmap> Maps { get; protected set; }
        public event EventHandler IndexChanged;
        private int _Index = 0;
        public int Index
        {
            get { return this._Index; }
            set
            {
                this._Index = value;
                if (this.IndexChanged != null) this.IndexChanged(this, new EventArgs());
            }
        }
        public virtual void Add(Bitmap map)
        {
            if (map.Size != this.TipSize) throw new Exception();
            this.Maps.Add(map);
        }
        public void Save(string path, ImageFormat format)
        {
            var bmp = CreateSingleBitmap(this.Maps,this.TipSize,this.Format2);
            bmp.Save(path, format);
        }
        public static BitMapsItem LoadFrom(string path, Size sz)
        {
            var bmp = BSystem.Drawing.MainDrawingMethod.LoadFrom(path, PixelFormat.Format32bppArgb);
            return new BitMapsItem(bmp, sz, sz);
        }
        public static Bitmap CreateSingleBitmap(IEnumerable<Bitmap> maps,int width,int height,Size thipSize,System.Drawing.Imaging.PixelFormat format)
        {
            
            int n = width;
            Bitmap r = new Bitmap(thipSize.Width * n, thipSize.Height * height, format);

            int x = 0, y = 0;
            foreach (var bmp in maps)
            {
                var destp = new Point(x * thipSize.Width, y * thipSize.Height);
                BSystem.Drawing.TurboArgbPixelFilter32.DrawImage(r, bmp, destp, Point.Empty, thipSize);
                x++;
                if (x == n) { x = 0; y++; }
            }
            return r;
        }
        public static Bitmap CreateSingleBitmap(IEnumerable<Bitmap> maps,Size thipSize,System.Drawing.Imaging.PixelFormat format)
        {
            double m = System.Math.Sqrt(maps.Count());
            int n = (int)System.Math.Ceiling(m);
            return CreateSingleBitmap(maps, n, n,thipSize,format);
        }
    }
}
