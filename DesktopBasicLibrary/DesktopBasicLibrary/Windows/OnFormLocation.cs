﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
namespace BSystem.Windows
{
    public static class PointExtension
    {
        public static Point GetPointOnForm(this Control c)
        {
            return GetPointOnForm(c, c.PointToScreen(Point.Empty));
        }
        public static Point GetPointOnForm(Control c,Point p)
        {
            if (c.Parent == null)
            {
                throw new Exception();
            }
            else
            {
                //var p2 = c.Parent.PointToClient(p); 
                if (c.Parent is Form)
                {
                    var p3 = c.Parent.PointToClient(p);
                    //var p3 = new Point(p.X - c.Parent.Location.X, p.Y - c.Parent.Location.Y);
                    return p3;
                }
                else
                {
                    return GetPointOnForm(c.Parent, p);
                }
            }
        }
        public static Rectangle OnFormRectangle(this Control c)
        {
            var sz = c.Size;
            var loc = c.GetPointOnForm();
            return new Rectangle(loc, sz);
        }
        /// <summary>
        /// 2つの点の距離を算出します。
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static double GetDistance(Point a, Point b)
        {
            var p = new Point(a.X - b.X, a.Y - b.Y);
            return System.Math.Sqrt(p.X * p.X + p.Y * p.Y);
        }
        /// <summary>
        /// RectangleとPointの距離を算出します。Rectangle内部の点の距離は負で、そうでない場合は正で表示します。
        /// </summary>
        /// <param name="p"></param>
        /// <param name="rec"></param>
        /// <returns></returns>
        public static double GetDistance(Point p, Rectangle rec)
        {
            List<double> r = new List<double>();
            if (rec.X <= p.X && p.X <= rec.Right && rec.Y <= p.Y && p.Y <= rec.Bottom)
            {
                var y = new[] { rec.Y - p.Y, p.Y - rec.Bottom }.Max();
                var x = new[] { rec.X - p.X, p.X - rec.Right }.Max();
                r.Add(System.Math.Max(x,y));
            }
            else if (rec.X <= p.X && p.X <= rec.Right)
            {

                if (rec.Y > p.Y)
                {
                    r.Add(rec.Y - p.Y);
                }
                else if(rec.Bottom < p.Y)
                {
                    r.Add(p.Y - rec.Bottom);
                }

            }
            else if (rec.Y <= p.Y && p.Y <= rec.Bottom)
            {
                if (rec.X > p.X)
                {
                    r.Add(rec.X - p.X);
                }
                else if(rec.Right < p.X)
                {
                    r.Add(p.X - rec.Right);
                }
            }
            

            var leftUp = new Point(rec.X, rec.Y);
            var leftDown = new Point(rec.X, rec.Bottom);
            var rightUp = new Point(rec.Right, rec.Y);
            var rightBottom = new Point(rec.Right, rec.Bottom);

            var p1 = GetDistance(p, leftUp);
            var p2 = GetDistance(p, leftDown);
            var p3 = GetDistance(p, rightUp);
            var p4 = GetDistance(p, rightBottom);
            var r5 = new[] { p1, p2, p3, p4 }.Min();
            r.Add(r5);
            return r.Min();
        }
    }
}
