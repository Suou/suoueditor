﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace BSystem
{
    public class Home:Inject.Console
    {
        public static void Create()
        {
            var v = new Home();
            Inject.Console.Current = v;
        }
        public override void _WriteLine(string text)
        {
            Console.WriteLine(text);
        }
    }
}
