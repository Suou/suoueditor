﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Text;
using SuouBase;
using SuouBase.Files;
using SuouBase.Interface;
namespace SuouBase.Personal
{
    public class PersonalInfo
    {
        public List<KeyValuePair<DateTime, DateTime>> PastRunningTimes { get; set; }
        public List<PersonalStoryInfo> Infos { get; set; }
        public void Add(SuouEditorDataClass bookdata)
        {
            var f = this.Infos.FirstOrDefault((v) => v.Name == bookdata.StoryFile.Name);
            if (f == null)
            {
                throw new NotSupportedException();
            }
            else
            {
                f.Add(bookdata);
            }
        }
    }
    public class PersonalStoryInfo
    {
        public string Name { get; set; }
        public string Path { get; set; }
        public List<PersonalStoryElementInfo> Items { get; set; }

        public void Add(SuouEditorDataClass bookdata)
        {
            var person = new PersonalStoryElementInfo(bookdata);
            this.Items.Add(person);
        }
    }
    public class PersonalStoryElementInfo
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public int ResCount { get; set; }
        public int ComaCount { get; set; }
        public int ByteCount { get; set; }
        public int SerifuCount { get; set; }
        public int SerifuByteCount { get; set; }

        public PersonalStoryElementInfo(SuouEditorDataClass bookdata)
        {
            this.StartTime = DateTime.Now;
            bookdata.Closed += this.Closed;
        }
        private void Closed(object sender, EventArgs e)
        {
            var item = sender as SuouEditorDataClass;
            var book = item.StoryFile;
            this.ResCount = book.OutputPages.Count();
            this.ComaCount = book.DetailBookPages.Count();
            this.ByteCount = book.OutputPages.Sum((v) => v.SText.GetSitarabaByteSize);
            this.EndTime = DateTime.Now;
            item.Closed -= this.Closed;
        }
    }
}
