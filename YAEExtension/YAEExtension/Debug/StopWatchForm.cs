﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Book;
namespace SuouBase
{
    public partial class StopWatchForm : Form
    {
        public StopWatchForm()
        {
            InitializeComponent();
        }
        List<StopWatchItem> Items = new List<StopWatchItem>();

        public void AddItem(StopWatchItem item)
        {
            this.Items.Add(item);
            Action t = (()=>this.listBox1.Items.Add(item));
            //this.listBox1.BeginInvoke(t, null).Pipe(this.listBox1.EndInvoke);
            t();
        }
        public void Clear()
        {
            this.listBox1.Items.Clear();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var n = this.listBox1.SelectedIndex;
            if (n != -1)
            {
                var f = this.Items[n];
                var time = this.Items.Where((v) => v.Name == f.Name).Sum((v) => v.Time);
                this.label1.Text = time.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IDataObject data = Clipboard.GetDataObject();
            if (data.GetDataPresent(typeof(BookPageLapper)))
            {
                var f = (BookPageLapper)data.GetData(typeof(BookPageLapper));
                if (f == null)
                {
                    this.textBox2.Text = "空";
                }
                else
                {
                    this.textBox2.Text = "あり";
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            IDataObject data = Clipboard.GetDataObject();
            if (data.GetDataPresent(typeof(BookPageLapper)))
            {
                var f = (BookPageLapper)data.GetData(typeof(BookPageLapper));
                this.textBox1.Text = f.Text;
            }
        }
        public EditorInfo DLLFunction { get; set; }

        public SectionScene CopyPage { get; set; }
        private void button3_Click(object sender, EventArgs e)
        {
            /*
            if (this.DLLFunction.Info.CopyPages != null)
            {
                this.CopyPage = this.DLLFunction.Info.CopyPages.Pages.First().ToMergeBookPage();

                this.listBox2.Items.Clear();
                this.listBox2.Items.Add("Text");
                for (int i = 0; i < this.CopyPage.Parts.Count; i++)
                {
                    this.listBox2.Items.Add(i.ToString());
                }
            }
            */
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var i = this.listBox2.SelectedIndex;
            if (i == -1) return;
            if (i == 0)
            {
                this.textBox3.Text = this.CopyPage.SText.Text;
            }
            else
            {
                this.textBox3.Text = this.CopyPage.Parts[i - 1].SText.Text;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.instanceViewer1.Item = this.DLLFunction.FileData.SelectData;
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            var result = this.openFileDialog1.ShowDialog();
            if (result == System.Windows.Forms.DialogResult.OK)
            {
                var filename = this.openFileDialog1.FileName;
                var fn = TimeDebug.Watch2(()=>IBookHandler.MergeBookLoad(filename),"FileLoad");
                
            }
        }
        public void AddLog(string name, string text)
        {
            this.CommentLog.Items.Add(text);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var v = (ObsoleteSectionBook)this.DLLFunction.FileData.SelectData.StoryFile;
            v.CreateSerializer().Save(v.Name);
        }
    }

    public class StopWatchItem
    {
        public string Name { get; set; }
        public long Time { get; set; }
        public Stopwatch Watch { get; set; }
        public StopWatchItem(string name, long time)
        {
            this.Name = name;
            this.Time = time;
        }
        public StopWatchItem(string name)
        {
            this.Name = name;
            this.Watch = Stopwatch.StartNew();
        }
        public override string ToString()
        {
            return this.Name + ", " + this.Time;
        }
        public void Stop()
        {
            this.Watch.Stop();
            this.Time = this.Watch.ElapsedMilliseconds;
        }
    }

    public static class TimeDebug
    {
        public static StopWatchForm StopWatchWindow = new StopWatchForm();
        public static EditorInfo DLLFunction
        {
            get { return StopWatchWindow.DLLFunction; }
            set { StopWatchWindow.DLLFunction = value; }
        }
        public static StopWatchItem Start(string name)
        {
            var f = new StopWatchItem(name);
            return f;
        }
        public static void Clear()
        {
#if DEBUG
            StopWatchWindow.Clear();
#endif
        }
        public static void AddComment(string name, string text)
        {
#if DEBUG
            StopWatchWindow.AddLog(name,text);
#endif
        }
        public static void Watch(Action pred,string name)
        {
            Stopwatch sw = Stopwatch.StartNew();
            pred();
            sw.Stop();
            var f = new StopWatchItem(name, sw.ElapsedMilliseconds);
#if DEBUG
            StopWatchWindow.AddItem(f);
#endif
        }
        public static T Watch2<T>(Func<T> pred, string name)
        {
            Stopwatch sw = Stopwatch.StartNew();
            var result = pred();
            sw.Stop();
            var f = new StopWatchItem(name, sw.ElapsedMilliseconds);
#if DEBUG
            StopWatchWindow.AddItem(f);
#endif
            return result;
        }
        public static void Stop(StopWatchItem item)
        {
            item.Stop();
#if DEBUG
            StopWatchWindow.AddItem(item);
#endif
        }
    }
}
