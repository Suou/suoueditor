﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Debug
{
    public partial class DebugPanel : UserControl
    {
        public DebugPanel()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var p = new SuouBase.Book.SectionScene();
            var p2 = new SuouBase.Book.SectionScenePart();
            DataObject data = new DataObject();
            data.SetData(p);
            data.SetData(p2);

            Clipboard.SetDataObject(data, true);
        }
    }
}
