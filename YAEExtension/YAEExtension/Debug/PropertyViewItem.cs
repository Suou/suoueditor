﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Debug
{
    public partial class PropertyViewItem : UserControl
    {
        public PropertyViewItem()
        {
            InitializeComponent();
        }
        public PropertyViewItem(string name,object value):this()
        {
            this.label1.Text = name;
            this.Item = value;
        }
        private object _Item = null;
        //private Type ItemType;
        public object Item
        {
            get { return _Item; }
            set
            {
                _Item = value;
                if (_Item != null)
                {
                    this.textBox1.Text = this.Item.ToString();
                }
            }
        }
    }
}
