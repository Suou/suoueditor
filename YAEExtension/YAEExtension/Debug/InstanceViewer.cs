﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
namespace SuouBase.Debug
{
    public partial class InstanceViewer : UserControl
    {
        public InstanceViewer()
        {
            InitializeComponent();
        }
        private object _Item = null;
        private Type ItemType;
        private PropertyInfo[] Properties { get; set; }
        public object Item
        {
            get { return _Item; }
            set
            {
                _Item = value;
                if (_Item != null)
                {
                    this.groupBox1.Controls.Clear();
                    this.Analyze();
                }
            }
        }
        public void Analyze()
        {
            this.ItemType = this.Item.GetType();
            this.Properties = this.ItemType.GetProperties();
            this.groupBox1.Text = this.ItemType.ToString();
            this.Properties.ToList().ForEach((v) => this.Analyze(v));
            
        }
        private void Analyze(PropertyInfo info)
        {
            var v = info.GetCustomAttributes(typeof(ViewInstanceAttribute), false);
            
            if (v.Count() == 0)
            {
                if (info.PropertyType.IsArray)
                {
                }
                else
                {

                    if (info.Name != "Item") this.Arrange(info.Name, info.GetValue(this.Item, null), false);
                    
                }
            }
            else
            {
                this.Arrange(info.Name, info.GetValue(this.Item, null), true);
            }
        }
        private void Arrange(string name,object ValueItem,bool next)
        {
            Control c = null;
            if (!next)
            {
                c = new PropertyViewItem(name, ValueItem);
            }
            else
            {
                c = new InstanceViewer() { Item = ValueItem };
            }
            this.Add(c);
        }
        private void Add(Control item)
        {
            item.Dock = DockStyle.Top;
            this.groupBox1.Controls.Add(item);
        }
    }
    
}
