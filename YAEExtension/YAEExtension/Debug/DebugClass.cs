﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using SuouBase.Text;
using System.Drawing;
using System.IO;
using SuouBase.Book;
namespace SuouBase.Debug
{
    public class DebugClass
    {
        private static string BMPFolder = "Data/Debug/Image/";
        private static string LogFolder = "Data/Debug/Log/";
        public static IBook SampleIBook()
        {
            var v = IBookHandler.Load("Data/MLTFolder/蘇芳・パヴリチェンコ.mlt");
            return v;
        }
        public static IBook SampleMontageIBook()
        {
            var v = IBookHandler.Load("Data/MLTFolder/yaruo_eye.ast");
            return v;
        }
        public static void Save(Bitmap bmp)
        {
            var p = new DirectoryInfo(BMPFolder);
            if (!p.Exists) p.Create();
            var k = DateTime.Now;
            bmp.Save(BMPFolder + k.Ticks + ".bmp");
        }
        public static void WriteLog(string text)
        {
            var p = new DirectoryInfo(LogFolder);
            if (!p.Exists) p.Create();
            var k = DateTime.Now;
            BSystem.IOLibrary.Save(text, LogFolder + k.Ticks + ".txt");
        }
    }
}
