﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SuouBase.Interface;
using System.Xml.Serialization;
using System.ComponentModel;
using BSystem.Collections;
using SuouBase.Drawing;
using BSystem;
namespace SuouBase.Text.Hukidasi
{
    public class HukidasiParser
    {
        public static HukidasiSaveData ParseVer0(XElement xe)
        {
            HukidasiType type = (HukidasiType)int.Parse(xe.Attribute("Type").Value);
            if (type == HukidasiType.Nothing)
            {
                return HukidasiSaveData.NoneDefault();
            }
            else
            {
                MergeZureOption ZureOption = (MergeZureOption)int.Parse(xe.Attribute("ZureOption").Value);
                bool PrePermitZure = xe.BoolAttributeParse("PrePermitZure", false);
                int centerPreZure = xe.IntAttributeParse("CenterPreZure", 0);
                int upCenterBottomPreZure = xe.IntAttributeParse("UpCenterBottomPreZure", 0);
                bool isTrimSpace = xe.BoolAttributeParse("IsTrimSpace", true);
                int PermitZureLength = xe.IntAttributeParse("PermitZureLength", 0);
                string name = xe.StringAttributeParse("Name", "");
                int[] offset = new int[10];


                var wakus = xe.Element("Wakus");
                var textwakus = wakus.Elements().Select((v) => SuouTextParser.Parse1(v)).ToArray();
                SuouText sampletext = null;
                XElement SampleTextElement = xe.Element("SampleText");
                SpaceLineSet MergeAlgorithm = SpaceLineSet.HukidasiDefaultSet;
                if (SampleTextElement != null)
                {
                    sampletext = SuouTextParser.Parse1(SampleTextElement);
                }

                var p = new HukidasiBaseSaveData()
                {
                    //WakuItems = textwakus,
                    Elements = new WakuElementCollection(textwakus, offset),
                    Options = new WakuOption()
                    {
                        Type = type,
                        //ZureOption = ZureOption,
                        //PermitZureLength = PermitZureLength,
                        MergeAlgorithm = MergeAlgorithm,
                        IsTrimSpace = isTrimSpace,
                        CenterPreZure = centerPreZure,
                        UpCenterBottomPreZure = upCenterBottomPreZure,
                        Name = name
                    },
                    //Offset = offset

                };
                //var hs = new HukidasiSaveData(textwakus, sampletext, type, ZureOption, PermitZureLength, MergeAlgorithm,isTrimSpace) { CenterPreZure = centerPreZure, UpCenterBottomPreZure = upCenterBottomPreZure };
                var hs = new HukidasiSaveData(p);
                if (hs.MergeAlgorithm == null) throw new MyException("");
                return hs;
            }

        }
        public static HukidasiSaveData ParseVer1(XElement xe)
        {
            var elements = WakuElementCollectionParse.Parse1(xe.Element(WakuElementCollection.ParserName));
            var option = WakuOption.Parse(xe.Element(WakuOption.ParserName));
            var arrangement = WakuArrangement.ParseVer1(xe.Element(WakuArrangement.ParserName));
            var f = new HukidasiBaseSaveData()
            {
                Elements = elements,
                Options = option,
                Arrangement = arrangement
            };
            var f2 = new HukidasiSaveData(f);

            return f2;
        }
        public static HukidasiSaveData ParseVer2(XElement xe)
        {
            var elements = WakuElementCollectionParse.Parse1(xe.Element(WakuElementCollection.ParserName));
            var option = WakuOption.Parse(xe.Element(WakuOption.ParserName));
            var arrangement = WakuArrangement.ParseVer2(xe.Element(WakuArrangement.ParserName));
            var f = new HukidasiBaseSaveData()
            {
                Elements = elements,
                Options = option,
                Arrangement = arrangement
            };
            var f2 = new HukidasiSaveData(f);

            return f2;
        }
        public static HukidasiSaveData ParseVer3(XElement xe)
        {
            var p = ParseVer2(xe);
            p.Padding = WakuPadding.Parse(xe.Element(WakuPadding.ParserName));
            return p;
        }
        public static HukidasiSaveData ParseVer4(XElement xe)
        {
            var elements = WakuElementCollectionParse.ParseNew(xe.Element(WakuElementCollection.ParserName));
            var option = WakuOption.Parse(xe.Element(WakuOption.ParserName));
            var arrangement = WakuArrangement.ParseVer2(xe.Element(WakuArrangement.ParserName));
            var padding = WakuPadding.Parse(xe.Element(WakuPadding.ParserName));
            var f = new HukidasiBaseSaveData()
            {
                Elements = elements,
                Options = option,
                Arrangement = arrangement,
                Padding = padding
            };
            var f2 = new HukidasiSaveData(f);

            return f2;
        }

        public static List<HukidasiSaveData> ListParse(XElement xe)
        {
            var version = xe.IntAttributeParse("Version", 0);
            switch (version)
            {
                case 0: return xe.Elements().Select((v) => HukidasiParser.ParseVer0(v)).ToList();
                case 1: return xe.Elements().Select((v) => HukidasiParser.ParseVer1(v)).ToList();
                case 2: return xe.Elements().Select((v) => HukidasiParser.ParseVer2(v)).ToList();
                case 3: return xe.Elements().Select((v) => HukidasiParser.ParseVer3(v)).ToList();
                case 4: return xe.Elements().Select((v) => HukidasiParser.ParseVer4(v)).ToList();
                default: return xe.Elements().Select((v) => HukidasiParser.ParseVer0(v)).ToList();
            }
        }
        public static HukidasiSaveData Parse(XElement xe)
        {
            var version = xe.IntAttributeParse("Version", 0);
            switch (version)
            {
                case 0: return HukidasiParser.ParseVer0(xe);
                case 1: return HukidasiParser.ParseVer1(xe);
                case 2: return HukidasiParser.ParseVer2(xe);
                case 3: return HukidasiParser.ParseVer3(xe);
                case 4: return HukidasiParser.ParseVer4(xe);
                default: return HukidasiParser.ParseVer0(xe);
            }
        }
    }
}
