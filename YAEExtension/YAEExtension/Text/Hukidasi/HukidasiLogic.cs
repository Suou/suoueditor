﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using SuouBase;
using SuouBase.Interface;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using SuouBase.Text.Hukidasi.WakuLogic;
namespace SuouBase.Text
{

    public class ExpansionBlockHukidasi : IHukidasiLogic
    {

        private ITextBlockLogic LeftUp { get { return Blocks[0]; } }
        private ITextBlockLogic CenterUp { get { return Blocks[1]; } }
        private ITextBlockLogic RightUp { get { return Blocks[2]; } }
        private ITextBlockLogic LeftCenter { get { return Blocks[3]; } }
        private ITextBlockLogic RightCenter { get { return Blocks[4]; } }
        private ITextBlockLogic LeftDown { get { return Blocks[5]; } }
        private ITextBlockLogic CenterDown { get { return Blocks[6]; } }
        private ITextBlockLogic RightDown { get { return Blocks[7]; } }
        private ITextBlockLogic L12 { get { return Blocks[8]; } }
        private ITextBlockLogic L14 { get { return Blocks[9]; } }
        private ITextBlockLogic L21 { get { return Blocks[10]; } }
        private ITextBlockLogic L25 { get { return Blocks[11]; } }
        private ITextBlockLogic L41 { get { return Blocks[12]; } }
        private ITextBlockLogic L45 { get { return Blocks[13]; } }
        private ITextBlockLogic L52 { get { return Blocks[14]; } }
        private ITextBlockLogic L54 { get { return Blocks[15]; } }
        private ITextBlockLogic[] Blocks = new ITextBlockLogic[16];
        public HukidasiSaveData SaveData { get; private set; }

        public AltText TextMaker { get; set; }
        public AltText UniTextMaker = AltText.UniDefault();

        public static ExpansionBlockHukidasi Default()
        {
            /*
            string[] s = new string[] { "┏", "┃", "┗", "━", "━", "┓", "┃", "┛" };
            var f = new HukidasiSaveData(s, HukidasiType.Block, HukidasiZureOption.LineRestriction, 5, SpaceLineSet.DefaultSet,false);
            */
            return new ExpansionBlockHukidasi(HukidasiSaveData.FiveDefault());
        }
        
        public ExpansionBlockHukidasi(HukidasiSaveData h)
            : this(h.CreateLogicElements())
        {
            this.SaveData = h;
        }
        private ExpansionBlockHukidasi(LogicElement[] items)
        {
            for (int i = 0; i < 16; i++)
            {
                var a = SuouText.Empty();
                a.Initialize(items[i].Text);
                if (a.Width != a.Lines.Max((t) => a.Width)) throw new Exception();
                this.Blocks[i] = new OneSimpleBlockLogic(items[i],this.UniTextMaker);
            }
        }

        public bool Check
        {
            get
            {
                //if (this.SaveData.SixteenItems.ToList().Exists((v) => v.Text.Length == 0)) return false;
                return true;
            }
        }
        public WidthIterator CreateWidthIterator()
        {
            var v = new WidthIterator(new List<int>() { this.L12.DotWidth, this.L14.DotWidth }, this.CenterUp.DotWidth);
            return v;
        }


        public SuouText CreateWaku(SuouText text, AltText textMaker)
        {
            if (textMaker == null) throw new MyException("");
            this.TextMaker = textMaker;
            var gata = this.CreatePadding(this.TextMaker, text);

            //OneGataBlockLogic TextBlock = new OneGataBlockLogic(text, TextMaker, this.CreateWidthIterator(), gata) { Name = "Text" };
            RectangleTextLogic TextBlock = new RectangleTextLogic(text, this.TextMaker, this.SaveData) { Name = "Text" };
            
            //上、左、右、下
            ThreeBlockLogic UpCenterBlock = new ThreeBlockLogic(L12, CenterUp, L14, ExpandDirection.Yoko,0) { InclimentHeadTail = true, Name = "UpCenter" };
            ThreeBlockLogic LeftCenterBlock = new ThreeBlockLogic(L21, LeftCenter, L41, ExpandDirection.Tate,0) { InclimentHeadTail = true, Name = "LeftCenter" };
            ThreeBlockLogic RightCenterBlock = new ThreeBlockLogic(L25, RightCenter, L45, ExpandDirection.Tate,0) { InclimentHeadTail = true, Name = "RightCenter" };
            ThreeBlockLogic BottomCenterBlock = new ThreeBlockLogic(L52, CenterDown, L54, ExpandDirection.Yoko,0) { InclimentHeadTail = true, Name = "BottomCenter" };
            //上段、中段、下段

            ThreeBlockLogic UpBlock = new ThreeBlockLogic(LeftUp, UpCenterBlock, RightUp, ExpandDirection.Yoko,0) { Name = "Up" };
            ThreeBlockLogic CenterBlock = new ThreeBlockLogic(LeftCenterBlock, TextBlock, RightCenterBlock, ExpandDirection.Yoko,0) { InclimentHeadTail = false, Name = "Center" };
            ThreeBlockLogic BottomBlock = new ThreeBlockLogic(LeftDown, BottomCenterBlock, RightDown, ExpandDirection.Yoko,0) { Name = "Bottom" };
            //全部
            ThreeBlockLogic BlockList = new ThreeBlockLogic(UpBlock, CenterBlock, BottomBlock, ExpandDirection.Tate,this.SaveData.Options.UpCenterBottomPreZure) { Name = "Block" };
            var f = BlockList.CreateSText();

            return f;
        }
    }
    public class BlockHukidasi : IHukidasiLogic
    {
        private ITextBlockLogic LeftUp = null, LeftCenter = null, LeftDown = null, CenterUp = null, CenterDown = null, RightUp = null, RightCenter = null, RightDown = null;
        //private ITextBlockLogic[] Blocks = null;
        public HukidasiSaveData SaveData { get; private set; }

        public AltText TextMaker { get; set; }

        public BlockHukidasi(HukidasiSaveData h)
            : this(h.EightItems.Select((v) => v.Text).ToArray())
        {
            this.SaveData = h;
        }
        private BlockHukidasi(string[] s)
        {
            throw new NotSupportedException();
            /*
            Blocks = new ITextBlockLogic[] { LeftUp, LeftCenter, LeftDown, CenterUp, CenterDown, RightUp, RightCenter, RightDown };

            for (int i = 0; i < 8; i++)
            {
                var a = SuouText.Empty();
                a.Initialize(s[i]);
                if (a.Width != a.Lines.Max((t) => a.Width)) throw new Exception();
                switch (i)
                {
                    case 0:
                        LeftUp = new OneSimpleBlockLogic(a);
                        break;
                    case 3:
                        LeftCenter = new OneSimpleBlockLogic(a);
                        break;

                    case 5:
                        LeftDown = new OneSimpleBlockLogic(a);
                        break;
                    case 1:
                        CenterUp = new OneSimpleBlockLogic(a);
                        break;
                    case 6:
                        CenterDown = new OneSimpleBlockLogic(a);
                        break;
                    case 2:
                        RightUp = new OneSimpleBlockLogic(a);
                        break;
                    case 4:
                        RightCenter = new OneSimpleBlockLogic(a);
                        break;
                    case 7:
                        RightDown = new OneSimpleBlockLogic(a);
                        break;

                }
            }
            */
        }
        public WidthIterator CreateWidthIterator()
        {
            var v = new WidthIterator(new List<int>() { this.CenterUp.DotWidth }, 0);
            return v;
        }
        public bool Check
        {
            get
            {
                if (this.SaveData.EightItems.ToList().Exists((v) => v.Text.Length == 0)) return false;
                return true;
            }
        }

        public SuouText CreateWaku(SuouText text, AltText textMaker)
        {
            this.TextMaker = textMaker;
            var gata = this.CreatePadding(this.TextMaker, text);
            //OneGataBlockLogic TextBlock = new OneGataBlockLogic(text, TextMaker, this.CreateWidthIterator(), gata);
            RectangleTextLogic TextBlock = new RectangleTextLogic(text, TextMaker,this.SaveData) { Name = "Text" };
            
            ThreeBlockLogic UpBlock = new ThreeBlockLogic(LeftUp, CenterUp, RightUp, ExpandDirection.Yoko,0);
            ThreeBlockLogic CenterBlock = new ThreeBlockLogic(LeftCenter, TextBlock, RightCenter, ExpandDirection.Yoko,0);
            ThreeBlockLogic BottomBlock = new ThreeBlockLogic(LeftDown, CenterDown, RightDown, ExpandDirection.Yoko,0);
            ThreeBlockLogic BlockList = new ThreeBlockLogic(UpBlock, CenterBlock, BottomBlock, ExpandDirection.Tate, this.SaveData.Options.UpCenterBottomPreZure);
            return BlockList.CreateSText();

        }
    }
    public class NoHukidasi : IHukidasiLogic
    {
        public virtual SuouText CreateWaku(SuouText text, AltText TextMaker)
        {
            return text;
        }
        public HukidasiSaveData SaveData { get; set; }
        public NoHukidasi(HukidasiSaveData h)
        {
            this.SaveData = h;
        }
        public bool Check { get { return true; } }
    }
    public class NoWakuHukidasi : NoHukidasi
    {
        public NoWakuHukidasi(HukidasiSaveData h):base(h)
        {
        }
        public override SuouText CreateWaku(SuouText text, AltText TextMaker)
        {
            var p = Enumerable.Range(1, 10).Select((v) => new LogicElement(SuouText.Empty(), 0, HukidasiPart.LeftCenter)).ToArray();

            var spaceInter = new SpaceAlignRepresent(this.SaveData.Arrangement.Align);
            TextElement TextBlock = new TextElement(text, this.SaveData.Padding, this.SaveData.Arrangement.Align, TextMaker.SpaceMaker);
            ThreeTateElement LeftElement = new ThreeTateElement(p[0], p[1], p[2], TextMaker, this.SaveData.Arrangement.LeftPathInter);
            ThreeTateElement RightElement = new ThreeTateElement(p[3], p[4], p[5], TextMaker, this.SaveData.Arrangement.RightPathInter);
            CenterElement centerElement = new CenterElement(LeftElement, TextBlock, RightElement, TextMaker.SpaceMaker, this.SaveData, spaceInter);
            return centerElement.SText;
        }
    }


    public class NaiveHukidasi : IHukidasiLogic
    {
        public AltText TextMaker { get; set; }
        public AltLine LineMaker { get; set; }

        public HukidasiSaveData SaveData { get; set; }
        public string LeftUp, LeftCenter, LeftDown, CenterUp, CenterDown, RightUp, RightCenter, RightDown;

        public NaiveHukidasi(HukidasiSaveData h)
            : this(h.EightItems.Select((v) => v.Text).ToArray())
        {
            this.SaveData = h;
        }
        private NaiveHukidasi(string[] s)
        {
            this.Set(s);
        }

        private void Set(string[] s)
        {

            if (s.Length != 8) throw new Exception();
            LeftUp = s[0]; CenterUp = s[3]; RightUp = s[5];
            LeftCenter = s[1]; RightCenter = s[6];
            LeftDown = s[2]; CenterDown = s[4]; RightDown = s[7];
        }
        public SuouText CreateWaku(SuouText text, AltText TextMaker)
        {
            var r = text.Clone();
            int NowY = 0;
            foreach (var v in r)
            {
                var w = new SuouLine(v.Font, v.Color, LeftCenter + v.Sentence + RightCenter);
                r[NowY++] = w;
            }
            return r;
        }
        public bool Check
        {
            get
            {
                if (this.SaveData.EightItems.ToList().Exists((v) => v.Text.Length == 0)) return false;
                return true;
            }
        }
    }
}
