﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using SuouBase;
using SuouBase.Interface;
using System.Diagnostics;
using System.Windows.Forms;
using System.Drawing;
using SuouBase.Book;
namespace SuouBase.Text
{

    public static class HukidasiExtension
    {
        public static IHukidasiLogic Create(HukidasiSaveData h)
        {
            if (h.MergeAlgorithm == null) throw new MyException("");
            switch (h.Type)
            {
                case HukidasiType.Naive:
                    return new NaiveHukidasi(h);
                case HukidasiType.Block:
                    return new BlockHukidasi(h);
                case HukidasiType.Nothing:
                    return new NoHukidasi(h);
                case HukidasiType.FiveBlock:
                    return new SuouBase.Text.Hukidasi.WakuLogic.FiveBlockHukidasi(h);
                case HukidasiType.NoWaku:
                    return new NoWakuHukidasi(h);
                default:
                    throw new Exception();
            }
        }

        public static IBookPage CreateSamplePage(this IHukidasiLogic hu,AltText textmaker,SuouText sampleText)
        {
            if (sampleText == null) return CreateSamplePage(hu, textmaker);
            AstPage p;
            var txt = hu.CreateWaku(sampleText, textmaker);
            
            p = new AstPage() { SText = txt,Name = hu.SaveData.Options.Name };
            return p;
        }
        public static IBookPage CreateSamplePage(this IHukidasiLogic hu, AltText textmaker)
        {
            return CreateSamplePage(hu, textmaker, new SuouText(System.Environment.NewLine + "sample" + System.Environment.NewLine));
        }

        public static GataPadding CreatePadding(this IHukidasiLogic h, AltText at, SuouText text)
        {
            var fcr = new GataPadding(h.SaveData.Padding.TopSpace, h.SaveData.Padding.LeftSpace, h.SaveData.Padding.RightSpace, h.SaveData.Padding.BottomSpace, h.SaveData.Padding.CenterSpace
                , at, text);
            return fcr;
        }
    }
}
