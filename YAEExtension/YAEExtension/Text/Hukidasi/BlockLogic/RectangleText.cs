﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using SuouBase;
using SuouBase.Base;
using System.Diagnostics;

namespace SuouBase.Text
{


    public class RectangleTextLogic : ITextBlockLogic
    {
        public RectangleTextLogic(SuouText Text, AltText TextMaker, HukidasiSaveData savedata)
        {
            if (Text == null) throw new Exception();

            this.SaveData = savedata;
            this.Rectan = new RectangleSuouText(Text, TextMaker,this.SaveData);
            this.TextMaker = TextMaker;
            this.OriginalText = Text.Clone();
            
            
        }
        public bool IsError { get; set; }
        public HukidasiSaveData SaveData { get; set; }
        RectangleSuouText Rectan { get; set; }
        #region Interface
        public string Name { get; set; }
        public Size EditSize { 
            get { return this.Rectan.Size; } 
        } 
        public int DotWidth
        {
            get { return this.EditSize.Width; }
        }
        public int UpperWidth
        {
            get
            {
                return this.Rectan.UpperWidth;
            }
        }
        public int LowerWidth
        {
            get
            {
                return this.Rectan.LowerWidth;
            }
        }
        public int Height { get { return this.EditSize.Height; } }
        public SuouText TextElement
        {
            get
            {

                var v = this.Rectan.SText;

                //var w = this.TextMaker.MakeRectangleText(v, this.EditSize);
                return v;
            }
        }
        public void Incliment(ExpandDirection Dir)
        {
            if (Dir == ExpandDirection.Tate)
            {
                if (!this.TextMaker.SpaceMaker.CanCreateSpace(this.DotWidth)) throw new Exception();
                this.Rectan.AddLine();
            }
            else
            {
                this.Rectan.Increment();
            }
        }
        public SuouText CreateSText()
        {
            return this.TextElement;
        }
        #endregion
        //public ExpandDirection Direction { get; set; }
        private SuouText OriginalText { get; set; }
        
        private AltText TextMaker { get; set; }
        



    }

    public class RectangleSuouText
    {
        public SuouText OriginalText { get; set; }
        List<SuouLineSpacer> Lines { get; set; }
        SortedTripleDictionary<SuouLineSpacer> SortedLines { get; set; }
        public HukidasiSaveData SaveData { get; set; }
        public int Width { get; private set; }
        
        public int Height
        {
            get 
            { 
                return this.GetCenterHeight(this.Lines.Count) + this.SaveData.Padding.TopSpace + this.SaveData.Padding.BottomSpace; 
            }
        }
        public int GetCenterHeight(int textHeight)
        {
            if (textHeight != 0)
            {
                return textHeight + this.SaveData.Padding.CenterSpace * (textHeight - 1);
            }
            else
            {
                return textHeight;
            }
        }
        public Size Size
        {
            get { return new Size(Width, Height); }
        }
        public ISpaceMaker SpaceCreater { get; set; }
        public int UpperWidth { get { return this.SortedLines.MaxKey; } }
        public int LowerWidth { get { return this.SortedLines.MinKey; } }

        public int LeftPadding
        {
            get
            {
                return this.SpaceCreater.CanCreateSpaceNum(this.SaveData.Padding.LeftSpace);
            }
        }
        public int RightPadding
        {
            get
            {
                return this.SpaceCreater.CanCreateSpaceNum(this.SaveData.Padding.RightSpace);
            }
        }
        public RectangleSuouText(SuouText Text, AltText TextMaker,HukidasiSaveData savedata)
        {
            this.SaveData = savedata;

            this.PrePermitZureLength = this.SaveData.Options.CenterPreZure;
            this.OriginalText = Text;
            this.SortedLines = new SortedTripleDictionary<SuouLineSpacer>();
            this.SpaceCreater = TextMaker.SpaceMaker;
            this.Lines = Text.Lines.Select((v) => new SuouLineSpacer(v, this.SpaceCreater,this.LeftPadding,this.RightPadding)).ToList();
            this.Lines.ForEach((v) => this.SortedLines.Add(v));

            this.Increment();
        }
        public int PrePermitZureLength {get;set;}
        public void AddLine()
        {
            var v = this.SpaceCreater.CreateSpace(this.Width);
            var f = new SuouLineSpacer(v, this.SpaceCreater, this.SaveData.Padding.LeftSpace, this.SaveData.Padding.RightSpace);
            this.Lines.Add(f);
        }
        public SuouText SText
        {
            get
            {
                var w = SuouText.Create(this.Lines.Select((v) => v.Sentence));
                var center = this.SpaceCreater.CreateSpace(w.Width);
                var y = w.InsertLines(center, this.SaveData.Padding.CenterSpace).InsertTop(this.Width, this.SaveData.Padding.TopSpace, this.SpaceCreater);
                var x = y.InsertBottom(this.Width, this.SaveData.Padding.BottomSpace, this.SpaceCreater);
                return x;
            }
        }
        public int Increment()
        {
            while (true)
            {
                var v = SortedLines.FirstValue();
                SortedLines.Remove(v);
                v.Increment();
                SortedLines.Add(v);
                if (this.UpperWidth-this.LowerWidth <= this.PrePermitZureLength && this.SpaceCreater.CanCreateSpace(this.UpperWidth))
                {
                    this.Width = SortedLines.MaxKey;
                    return this.Width;
                }
            }
            throw new Exception();
        }
    }
    public class SuouLineSpacer : ISortedDoubleValue
    {
        SuouLine Line { get; set; }
        public int LeftPadding { get; set; }
        public int RightPadding { get; set; }
        private ISpaceMaker SpaceCreater { get; set; }
        public SuouLineSpacer(SuouLine line, ISpaceMaker maker,int leftPadding,int rightPadding)
        {
            this.LeftPadding = leftPadding;
            this.RightPadding = rightPadding;
            this.Line = line;
            this.SpaceCreater = maker;

        }
        public int Width
        {
            get { return this.LeftPadding + this.Line.Width + this.RightPadding; }
        }
        public int Value
        {
            get { return this.Width; }
        }
        public SuouLine Sentence
        {
            get
            {
                return this.SpaceCreater.CreateSpace(this.LeftPadding) + this.Line + this.SpaceCreater.CreateSpace(this.RightPadding);
            }
        }
        public int Increment()
        {
            while (true)
            {
                this.RightPadding++;
                if (this.SpaceCreater.CanCreateSpace(this.RightPadding))
                {
                    return this.RightPadding;
                }
            }
            throw new Exception();
        }
    }
}
