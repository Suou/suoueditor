﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using SuouBase;
using SuouBase.Base;
using System.Diagnostics;

namespace SuouBase.Text
{

    public class OneGataBlockLogic : ITextBlockLogic
    {
        public OneGataBlockLogic(SuouText Text, AltText TextMaker, WidthIterator iter, GataPadding gata)
        {
            if (Text == null) throw new Exception();

            
            this.WidthIterator = iter;
            this.TextMaker = TextMaker;
            this.OriginalText = Text.Clone();
            this.Gata = gata;
            InclimentSpaceWidth = TimeDebug.Watch2(() => WidthIterator.Iterator((i) => i >= NoInclimentTextSize.Width && CanCreateRectangleText(i)), "Incliment");
            //while (!this.TextMaker.SpaceMaker.CanCreateSpace(this.TextElement.Width)) { this.TextElement = this.TextMaker.MakeRectangleText(this.TextElement, false); }


        }
        #region Interface
        public bool IsError { get; set; }
        public string Name { get; set; }
        public int UpperWidth
        {
            get
            {
                return this.DotWidth;
            }
        }
        public int LowerWidth
        {
            get
            {
                return this.DotWidth;
            }
        }
        public Size EditSize { get { return new Size() { Width = this.DotWidth, Height = this.Height }; } }
        public int DotWidth {
            get { return InclimentSpaceWidth; }
        }
        public int Height { get { return this.NoInclimentTextSize.Height + InclimentSpaceHeight; } }
        public SuouText TextElement
        {
            get
            {
                
                var v = this.Gata.CreateText(this.OriginalText, this.DotWidth);

                var w = this.TextMaker.MakeRectangleText(v, this.EditSize);
                if (w.Width != this.DotWidth || w.Count != this.Height)
                {
                    throw new MyException("");
                }
                
                
                return w;
            }
        }
        public void Incliment(ExpandDirection Dir)
        {
            CardinalNumber++;
            if (Dir == ExpandDirection.Tate)
            {
                if (!this.TextMaker.SpaceMaker.CanCreateSpace(this.DotWidth)) throw new Exception();
                InclimentSpaceHeight++;
            }
            else
            {
                InclimentSpaceWidth = this.HabaIterator().First();
            }
        }
        public IEnumerable<int> HabaIterator()
        {
            yield return WidthIterator.Iterator((i) => i >= NoInclimentTextSize.Width && CanCreateRectangleText(i));
        }
        public SuouText CreateSText()
        {
            return this.TextElement;
        }
        #endregion
        private GataPadding Gata { get; set; }
        //public ExpandDirection Direction { get; set; }
        private int CardinalNumber { get; set; }
        private SuouText OriginalText { get; set; }
        private WidthIterator WidthIterator { get; set; }
        private int InclimentSpaceWidth = 0;
        private int InclimentSpaceHeight = 0;
        private Size NoInclimentTextSize
        {
            get
            {
                return new Size(this.Gata.LeftPadding + this.OriginalText.Width + this.Gata.RightPadding, this.Gata.CenterHeight + this.Gata.TopPadding + this.Gata.BottomPadding);
            }
        }
        
        private AltText TextMaker { get; set; }
        private bool CanCreateRectangleText(int n)
        {
            var b = this.TextMaker.CanMakeRectangleText(this.OriginalText, new Size(n - Gata.LeftPadding, this.OriginalText.Count));
            var c = this.TextMaker.SpaceMaker.CanCreateSpace(n);
            return b && c;
        }



        
    }

}
