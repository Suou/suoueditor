﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using SuouBase;
using System.Diagnostics;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
namespace SuouBase.Text
{

    public class GataPadding
    {
        public int LeftPadding;
        public int RightPadding;
        public int TopPadding;
        public int BottomPadding;
        public int CenterPadding;
        AltText SpaceMaker = null;
        public Font Font;
        public Color Color;
        SuouText Text { get; set; }
        public GataPadding(int top, int left, int right, int bottom, int center, AltText spacemaker, SuouText text)
        {
            this.SpaceMaker = spacemaker;
            this.Font = text.Font;
            this.Color = text.Color;
            this.Text = text;
            this.TopPadding = top;
            this.LeftPadding = spacemaker.SpaceMaker.CanCreateSpaceNum(left);
            this.RightPadding = spacemaker.SpaceMaker.CanCreateSpaceNum(right);
            this.BottomPadding = bottom;
            this.CenterPadding = center;
        }

       
        public int CenterHeight
        {
            get
            {
                return this.GetCenterHeight(this.Text.Count);
            }
        }
        public int GetCenterHeight(int textHeight)
        {
            if (textHeight != 0)
            {
                return this.Text.Count + this.CenterPadding * (textHeight - 1);
            }
            else
            {
                return textHeight;
            }
        }
        public Size GetSize(Size textSize)
        {
            var width = this.LeftPadding + textSize.Width + this.RightPadding;
            var height = this.TopPadding + this.GetCenterHeight(textSize.Height) + this.BottomPadding;
            return new Size(width, height);
        }
        private SuouText InsertLine(SuouText text)
        {
            if (CenterPadding != 0)
            {
                List<SuouLine> r = new List<SuouLine>();
                var sp = this.SpaceMaker;

                for (int i = 0; i < text.Count; i++)
                {
                    r.Add(text[i]);
                    if (i != text.Count - 1) this.CenterPadding.ForEach((n) => r.Add(sp.SpaceMaker.CreateSpace(text.Width)));
                }
                return new SuouText(text.Font, text.Color, r.ToArray());
            }
            else
            {
                return text;
            }
        }
        private SuouText CreateUpPaddingText()
        {
            if (TopPadding == 0) return null;
            if (!this.SpaceMaker.CanMakeRectangleText(new Size(CompleteLength, this.TopPadding))) throw new MyException("");
            return this.SpaceMaker.MakeRectangleText(Font, Color, new Size(CompleteLength, this.TopPadding));
        }
        private SuouText CreateLeftPaddingText()
        {
            if (LeftPadding == 0) return null;
            return this.SpaceMaker.MakeRectangleText(Font, Color, new Size(this.LeftPadding, this.Text.Count));
        }
        private int CompleteLength { get; set; }
        //private SuouText CenterText { get; set; }
        private SuouText UpPaddingText { get; set; }
        private SuouText LeftPaddingText { get; set; }
        private SuouText RightPaddingText { get; set; }
        private SuouText BottomPaddingText { get; set; }
        private void PreCreateText(SuouText text)
        {
            if (!this.SpaceMaker.SpaceMaker.CanCreateSpace(CompleteLength))
            {
                throw new MyException("");
            }
            //this.CenterText = this.InsertLine(text);

            this.UpPaddingText = this.CreateUpPaddingText();
            this.LeftPaddingText = this.CreateLeftPaddingText();
            //this.RightPaddingText = this.CreateRightPaddingText();
            //this.BottomPaddingText = this.CreateBottomPaddingText();
        }
        // Join Left -> Center -> Top -> Other
        public SuouText CreateText(SuouText Text,int len)
        {
            this.CompleteLength = len;
            this.PreCreateText(Text);
            var r = this.Text;
            if (LeftPaddingText != null) r = LeftPaddingText + r;
            if (CenterPadding != 0) r = InsertLine(r);
            if (UpPaddingText != null) r = UpPaddingText % r;
            return r;
        }
    }


    public static class GataExtension
    {
        public static SuouText InsertLines(this SuouText text, SuouLine insertLine,int centerPadding)
        {
            if (centerPadding == 0) return text.Clone();
            List<SuouLine> r = new List<SuouLine>();
            

            for (int i = 0; i < text.Count; i++)
            {
                r.Add(text[i]);
                if (i != text.Count - 1) centerPadding.ForEach((n) => r.Add(insertLine.Clone()));
            }
            return new SuouText(text.Font, text.Color, r.ToArray());
        }
        public static SuouText InsertTop(this SuouText text, int topwidth,int topheight,ISpaceMaker spacer)
        {
            if (topheight == 0) return text.Clone();
            var top = SuouText.Create(topwidth, topheight, spacer);
            
            return top % text;
        }
        public static SuouText InsertTop(this SuouText text, int topheight)
        {
            if (topheight == 0) return text.Clone();
            var top = SuouText.Create(topheight);

            return top % text;
        }
        public static SuouText InsertBottom(this SuouText text, int bottomwidth, int bottomheight, ISpaceMaker spacer)
        {
            if (bottomheight == 0) return text.Clone();
            var bottom = SuouText.Create(bottomwidth, bottomheight, spacer);

            return text % bottom;
        }
        public static SuouText InsertBottom(this SuouText text, int count)
        {
            if (count == 0) return text.Clone();
            var bottom = SuouText.Create(count);

            return text % bottom;
        }
    }
}
