﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using SuouBase;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Diagnostics;

namespace SuouBase.Text
{

    public class ThreeBlockLogic : ITextBlockLogic
    {
        public ThreeBlockLogic(ITextBlockLogic LeftBlockLogic, ITextBlockLogic CenterBlockLogic, ITextBlockLogic RightBlockLogic, ExpandDirection InnerDirection,int permitZure)
        {
            this.HeadBlockLogic = LeftBlockLogic;
            this.CenterBlockLogic = CenterBlockLogic;
            this.TailBlockLogic = RightBlockLogic;
            this.InnerDirection = InnerDirection;
            this.PermitZure = permitZure;
            this.InclimentUntilCheckOK();
        }

        private bool _IsError;
        public bool IsError
        {
            get
            {
                var w = this.LogicList.Exists((v) => v.IsError);
                return this._IsError || w;
            }
            set
            {
                this._IsError = value;
            }
        }
        public string Name { get; set; }
        public int PermitZure { get; set; }
        #region Property
        public ITextBlockLogic HeadBlockLogic { get; private set; }
        public ITextBlockLogic CenterBlockLogic { get; private set; }
        public ITextBlockLogic TailBlockLogic { get; private set; }
        public bool InclimentHeadTail { get; set; }
        public BlockJoinRule JoinRule = BlockJoinRule.Strict;
        public int LimitZure = 0;

        public readonly ExpandDirection InnerDirection;
        public int DotWidth
        {
            get
            {
                return this.EditSize.Width;
            }
        }
        public int Height
        {
            get
            {
                return this.EditSize.Height;
            }
        }
        public Size EditSize
        {
            get
            {
                if (InnerDirection == ExpandDirection.Tate)
                {
                    int height = LogicList.Sum((v) => v.Height);
                    if (InclimentHeadTail)
                    {
                        if (CardinalNumber == 0) height = CenterBlockLogic.Height;
                        if (CardinalNumber == 1) height = CenterBlockLogic.Height + HeadBlockLogic.Height;
                    }
                    var width = this.HeadBlockLogic.EditSize.Width;
                    return new Size(width, height);
                }
                else
                {
                    var width = LogicList.Sum((v) => v.DotWidth);
                    if (InclimentHeadTail)
                    {
                        if (CardinalNumber == 0) width = CenterBlockLogic.DotWidth;
                        if (CardinalNumber == 1) width = CenterBlockLogic.DotWidth + HeadBlockLogic.DotWidth;
                    }
                    var height = this.HeadBlockLogic.EditSize.Height;
                    return new Size(width, height);
                }
            }
        }

        public int CardinalNumber { get; private set; }
        #endregion
        public List<ITextBlockLogic> LogicList
        {
            get
            {
                return new List<ITextBlockLogic>() { this.HeadBlockLogic, this.CenterBlockLogic, this.TailBlockLogic };
            }
        }

        #region Incliment
        private bool CheckTateYokoIncliment()
        {
            if (this.IsError) return true;
            var lower = this.LogicList.Min((w) => w.LowerWidth);
            var upper = this.LogicList.Max((w) => w.UpperWidth);
            if (upper - lower <= this.PermitZure) return true;
            return false;
        }
        private void YokoTateIncliment(bool firstCheck)
        {
            if (firstCheck && this.CheckYokoTate()) return;
            do
            {
                this.LogicList.Min2((v) => v.Height).Incliment(ExpandDirection.Tate);
            }
            while (!this.CheckYokoTate());
        }
        private bool CheckYokoTate()
        {
            if (this.IsError) return true;
            return this.LogicList.All((v) => v.Height == this.HeadBlockLogic.Height);
        }
        private void TateYokoIncliment(bool firstCheck)
        {
            if (firstCheck && this.CheckTateYokoIncliment()) return;
            do
            {
                if (this.Name == "Block")
                {
                    var w = this.LogicList.Min2((v) => v.LowerWidth);
                }
                this.LogicList.Min2((v) => v.LowerWidth).Incliment(ExpandDirection.Yoko);
            }
            while (!this.CheckTateYokoIncliment());
        }
        private void OtherIncliment(ExpandDirection Dir)
        {
            if (InclimentHeadTail)
            {
                if (CardinalNumber < 3) return;
                if (CardinalNumber % 2 == 0)
                {
                    HeadBlockLogic.Incliment(Dir);
                }
                else
                {
                    TailBlockLogic.Incliment(Dir);
                }
            }
            else
            {
                this.CenterBlockLogic.Incliment(Dir);
            }
        }

        public void Incliment(ExpandDirection Dir)
        {
            CardinalNumber++;
            if (this.CardinalNumber > 1200)
            {
                this.IsError = true;
                return;
                //throw new MyException("Error");
            }
            if (this.InnerDirection == ExpandDirection.Yoko && Dir == ExpandDirection.Tate)
            {
                this.YokoTateIncliment(false);
            }
            else if (this.InnerDirection == ExpandDirection.Tate && Dir == ExpandDirection.Yoko)
            {
                this.TateYokoIncliment(false);
            }
            else
            {
                this.OtherIncliment(Dir);
            }
        }
        #endregion
        /*
        public bool CheckEachBlockLength()
        {
            bool b = false;
            if (this.InnerDirection == ExpandDirection.Yoko)
            {
                b = LogicList.All((v) => v.Height == HeadBlockLogic.Height);
            }
            else
            {
                b = (LogicList.All((v) => v.DotWidth == HeadBlockLogic.DotWidth));

            }
            return b;

        }
        */
        public void InclimentUntilCheckOK()
        {
            if (this.InnerDirection == ExpandDirection.Yoko)
            {
                this.YokoTateIncliment(true);
                //while (!this.che) { this.LogicList.Min2((v) => v.Height).Incliment(ExpandDirection.Tate); }
            }
            else
            {
                this.TateYokoIncliment(true);
                //while (!this.CheckTateYokoIncliment()) { this.LogicList.Min2((v) => v.DotWidth).Incliment(ExpandDirection.Yoko); }
            }
        }

        public int UpperWidth
        {
            get
            {
                if (this.InnerDirection == ExpandDirection.Tate)
                {
                    return new int[]{this.HeadBlockLogic.UpperWidth, this.CenterBlockLogic.UpperWidth, this.TailBlockLogic.UpperWidth}.Max();
                }
                else
                {
                    return this.DotWidth;
                }
            }
        }
        public int LowerWidth
        {
            get
            {
                if (this.InnerDirection == ExpandDirection.Tate)
                {
                    return new int[] { this.HeadBlockLogic.LowerWidth, this.CenterBlockLogic.LowerWidth, this.TailBlockLogic.LowerWidth }.Min();
                }
                else
                {
                    return this.DotWidth;
                }
            }
        }

        public SuouText CreateSText()
        {
            return this.TextElement;
        }
        public SuouText TextElement
        {
            get
            {
                if (this.IsError)
                {
                    return new SuouText("Error");
                }
                if (InclimentHeadTail)
                {
                    var u = HeadBlockLogic.TextElement;
                    var r = CenterBlockLogic.TextElement;
                    var d = TailBlockLogic.TextElement;
                    if (CardinalNumber > 0)
                    {
                        r = this.InnerDirection == ExpandDirection.Tate ? u.TateJoin(r) : u.YokoJoin(r);
                    }
                    if (CardinalNumber > 1)
                    {
                        r = this.InnerDirection == ExpandDirection.Tate ? r.TateJoin(d) : r.YokoJoin(d);
                    }
                    return r;
                }
                else
                {
                    SuouText r = null;
                    if (this.InnerDirection == ExpandDirection.Tate)
                    {
                        r = HeadBlockLogic.TextElement % CenterBlockLogic.TextElement % TailBlockLogic.TextElement;
                        //r = HeadBlockLogic.TextElement.TateJoin(CenterBlockLogic.TextElement).TateJoin(TailBlockLogic.TextElement);
                    }
                    else
                    {
                        r = HeadBlockLogic.TextElement.YokoJoin(CenterBlockLogic.TextElement).YokoJoin(TailBlockLogic.TextElement);
                    }

                    return r;
                }
            }
        }

    }
}
