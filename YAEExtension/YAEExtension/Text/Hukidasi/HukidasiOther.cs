﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using SuouBase;
using System.Diagnostics;
using BSystem.Collections;
namespace SuouBase.Text
{
    #region Interface
    public interface ITextBlockLogic
    {
        /// <summary>
        /// ブロックをDirの方向に伸ばす
        /// </summary>
        /// <param name="Dir"></param>
        string Name { get; set; }
        void Incliment(ExpandDirection Dir);
        SuouText CreateSText();
        SuouText TextElement { get; }
        Size EditSize { get; }
        int Height { get; }
        int UpperWidth { get; }
        int LowerWidth { get; }
        int DotWidth { get; }
        bool IsError { get; set; }
    }
    public interface IHukidasiLogic
    {
        SuouText CreateWaku(SuouText text, AltText TextMaker);
        HukidasiSaveData SaveData { get; }
        //bool Check { get; }

    }
    #endregion
    #region Block
    public enum JoinDirection
    {
        Up, Left, Right, Down
    }
    public enum ExpandDirection
    {
        Tate, Yoko
    }
    public enum BlockJoinRule
    {
        Strict, Relax
    }

    public class WidthIterator
    {
        IList<int> WidthItem;
        int Width = 0;
        int i = 0;
        public WidthIterator(IList<int> items)
        {
            this.WidthItem = items;
        }
        public WidthIterator(IList<int> items, int w)
            : this(items)
        {
            this.Width = w;
        }
        public int Iterator(Func<int, bool> pred)
        {
            while (true)
            {
                if (i == this.WidthItem.Count) i = 0;
                this.Width += this.WidthItem[i++];
                if (pred(this.Width))
                {

                    return this.Width;
                }
            }
        }
    }
    #endregion
    public enum HukidasiType
    {
        Nothing,Naive,Block,FiveBlock,NoWaku
    }
    public enum MergeZureOption
    {
        Free,LineRestriction,PermitZureLine,Destructive,HighDestructive
    }
    public enum HukidasiPart
    {
        LeftUpCorner, LeftCenterUp, LeftCenter, LeftCenterDown, LeftDownCorner, 
        UpCenterLeft, UpCenter, UpCenterRight, RightUpCorner, 
        RightCenterUp, RightCenter, RightCenterDown, RightDownCorner, 
        DownCenterLeft, DownCenter, DownCenterRight,None
    }
    


    public static class HukidasiPartExtension
    {
        
        public static HukidasiPart[] Parts
        {
            get
            {
                var r = new List<HukidasiPart>();
                r.AddRange(UpperParts);
                r.AddRange(LeftPathParts);
                r.AddRange(RightPathParts);
                r.AddRange(DownerParts);
                var nr = new HukidasiPart[16];
                r.ForEach((v) => nr[HukidasiSaveData.GetIndex(v)] = v);

                return nr;
            }
        }
        public static HukidasiPart[] UpperParts
        {
            get
            {
                return new HukidasiPart[] { HukidasiPart.LeftUpCorner, HukidasiPart.RightUpCorner,
                HukidasiPart.UpCenterLeft, HukidasiPart.UpCenter,
                HukidasiPart.UpCenterRight };
            }
        }
        public static HukidasiPart[] DownerParts
        {
            get
            {
                return new HukidasiPart[] {
                    HukidasiPart.LeftDownCorner, HukidasiPart.RightDownCorner, HukidasiPart.DownCenterLeft, HukidasiPart.DownCenter,
                HukidasiPart.DownCenterRight };
            }
        }
        public static HukidasiPart[] UpCornersParts
        {
            get
            {
                return new HukidasiPart[] {
                HukidasiPart.LeftUpCorner,HukidasiPart.RightUpCorner };
            }
        }
        public static HukidasiPart[] DownCornersParts
        {
            get
            {
                return new HukidasiPart[] {
                HukidasiPart.LeftDownCorner,HukidasiPart.RightDownCorner };
            }
        }
        public static HukidasiPart[] UpperPathParts
        {
            get
            {
                return new HukidasiPart[] {
                HukidasiPart.UpCenterLeft, HukidasiPart.UpCenter,
                HukidasiPart.UpCenterRight };
            }
        }
        public static HukidasiPart[] DownPathParts
        {
            get
            {
                return new HukidasiPart[] {
                    HukidasiPart.DownCenterLeft, HukidasiPart.DownCenter,
                HukidasiPart.DownCenterRight };
            }
        }
        public static HukidasiPart[] LeftPathParts
        {
            get
            {
                return new HukidasiPart[] { HukidasiPart.LeftCenterUp, HukidasiPart.LeftCenter, HukidasiPart.LeftCenterDown };
            }
        }
        public static HukidasiPart[] LeftParts
        {
            get
            {
                return new HukidasiPart[] { HukidasiPart.LeftUpCorner, HukidasiPart.LeftCenterUp, HukidasiPart.LeftCenter, HukidasiPart.LeftCenterDown,HukidasiPart.LeftDownCorner };
            }
        }
        public static HukidasiPart[] RightPathParts
        {
            get
            {
                return new HukidasiPart[] { HukidasiPart.RightCenterUp, HukidasiPart.RightCenter, HukidasiPart.RightCenterDown };
            }
        }


        public static HukidasiPart[] EightParts()
        {
            return new HukidasiPart[] { HukidasiPart.LeftUpCorner, HukidasiPart.LeftCenter, 
                HukidasiPart.LeftDownCorner, HukidasiPart.UpCenter,
                HukidasiPart.RightUpCorner, HukidasiPart.RightCenter,
                HukidasiPart.RightDownCorner, HukidasiPart.DownCenter,
                 };
        }
    }
}
