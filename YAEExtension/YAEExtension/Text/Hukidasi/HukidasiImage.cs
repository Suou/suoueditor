﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase.Text;
using SuouBase.Drawing;
using BSystem.Drawing;
namespace SuouTextBox2.Base
{
    public class HukidasiImage
    {
        public IHukidasiLogic Hukidasi { get; set; }
        public AltText TextMaker { get; set; }
        public HukidasiImage(IHukidasiLogic Hukidasi, AltText tm)
        {
            this.Hukidasi = Hukidasi;
            this.TextMaker = tm;
        }
        public Bitmap CreateImage(Size size, SuouText sampletext)
        {
            var f = Hukidasi.CreateWaku(sampletext, TextMaker);

            var img = BitmapExpansion.CreateBitmapRapid(f,f.YInterval,Color.White);
            var img2 = EnlargeBmp.EnlargeBitmap(img, size);
            return img2;
        }
        public Bitmap CreateImage(SuouText sampletext)
        {
            var f = Hukidasi.CreateWaku(sampletext, TextMaker);
            var img = BitmapExpansion.CreateBitmapRapid(f, f.YInterval, Color.White);
            return img;
        }
    }
}
