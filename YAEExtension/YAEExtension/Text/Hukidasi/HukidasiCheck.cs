﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
using BSystem;
using SuouBase.Base;
namespace SuouBase.Text
{
    public class HukidasiCheck
    {
        public HukidasiSaveData Data { get; set; }
        //public List<string> ErrorMessage { get; set; }
        public MessageDic<HukidasiPart> Dic = new MessageDic<HukidasiPart>();
        private bool[] CheckElements = new bool[16];
        public bool UpDownWidthFlag = false;
        public bool IsNoError
        {
            get { return this.CheckElements.All((v) => v) && UpDownWidthFlag; }
        }
        
        public string GetErrorMessage()
        {
            //var s = String.Join(System.Environment.NewLine, this.ErrorMessage);
            //return s;
            return string.Join(System.Environment.NewLine, this.Dic.AllMessage);
        }
        public string[] GetAllErrorMessage()
        {
            return this.Dic.AllMessage;
        }

        public IEnumerable<KeyValuePair<HukidasiPart, bool>> ErrorIterator()
        {
            var v = this.Data.Type == HukidasiType.FiveBlock ? HukidasiPartExtension.Parts : HukidasiPartExtension.EightParts();
            foreach (var part in v)
            {
                yield return new KeyValuePair<HukidasiPart, bool>(part, this.CheckElements[HukidasiSaveData.GetIndex(part)]);
            }
        }
        public IEnumerable<HukidasiPart> ErrorParts
        {
            get
            {
                return this.Dic.Dic.Keys.ToArray();
            }
        }
        public HukidasiCheck(HukidasiSaveData data)
        {
            this.Data = data;
            //this.ErrorMessage = new List<string>();
            for (int i = 0; i < this.CheckElements.Length; i++) this.CheckElements[i] = true;
            this.ErrorCheck();
        }
        #region check
        public void ErrorCheck()
        {
                switch (this.Data.Type)
                {
                    case HukidasiType.Block: CheckThreeBlock(); break;
                    case HukidasiType.FiveBlock: CheckFiveBlock(); break;
                }
            
        }


        public bool CheckFiveBlock()
        {
            HukidasiPartExtension.Parts.ForEach((v) => this.CheckEmpty(v));
            HukidasiPartExtension.UpperParts.ForEach((v) => this.IsRectangleBlock(v));
            HukidasiPartExtension.DownerParts.ForEach((v) => this.IsRectangleBlock(v));
            //HukidasiPartExtension.Parts.ForEach((v) => this.IsRectangleBlock(v));
            

            this.IsEqualHeightBlocks(HukidasiPart.LeftUpCorner,HukidasiPart.UpCenterLeft,HukidasiPart.UpCenter,HukidasiPart.UpCenterRight, HukidasiPart.RightUpCorner);
            this.IsEqualHeightBlocks(HukidasiPart.LeftDownCorner, HukidasiPart.DownCenterLeft, HukidasiPart.DownCenter, HukidasiPart.DownCenterRight, HukidasiPart.RightDownCorner);
            /*
            if (this.Data.CenterPreZure == 0)
            {
                this.IsEqualWidthBlocks(HukidasiPart.LeftCenterUp, HukidasiPart.LeftCenter, HukidasiPart.LeftCenterDown);
                this.IsEqualWidthBlocks(HukidasiPart.RightCenterUp, HukidasiPart.RightCenter, HukidasiPart.RightCenterDown);
            }
            */

            this.IsEqualHeightBlocks(HukidasiPart.LeftCenterUp, HukidasiPart.RightCenterUp);
            this.IsEqualHeightBlocks(HukidasiPart.LeftCenter, HukidasiPart.RightCenter);
            this.IsEqualHeightBlocks(HukidasiPart.LeftCenterDown, HukidasiPart.RightCenterDown);

            /*
            if (this.Data.UpCenterBottomPreZure == 0)
            {
                this.IsEqualWidthBlocks(HukidasiPart.UpCenterLeft, HukidasiPart.DownCenterLeft);
                this.IsEqualWidthBlocks(HukidasiPart.UpCenter, HukidasiPart.DownCenter);
                this.IsEqualWidthBlocks(HukidasiPart.UpCenterRight, HukidasiPart.DownCenterRight);
            }
            */
            var b = this.CheckPathWidth();
            //this.CheckUpCenterBottomLength();
            return this.IsNoError;
        }

        public bool CheckPathWidth()
        {
            UpDownWidthFlag = true;
            var upn = this.Data.TopPeriodicWidth;
            var downn = this.Data.BottomPeriodicWidth;
            var d = BSystem.MathExtension.Gcd(upn, downn);
            if (d == 1)
            {
                return true;
            }
            else
            {
                if(this.DiffUpDownCornerWidth % d == 0){
                    return true;
                }else{
                    if (this.DiffUpDownCornerWidth <= this.Data.Options.UpCenterBottomPreZure)
                    {
                        return true;
                    }
                    else
                    {
                        this.Dic.Add(HukidasiPart.None, string.Format("エラーです。上端と下端の要素を変更するか、上中下段のズレ許容を{0}以上にしてください。", this.DiffUpDownCornerWidth));
                        UpDownWidthFlag = false;
                        return false;
                    }
                }
            }
        }
        public int DiffUpDownCornerWidth
        {
            get
            {
                var upn = this.Data.TopFixingWidth;
                var downn = this.Data.BottomFixingWidth;
                return System.Math.Abs(upn - downn);
            }
        }
        

        public bool CheckThreeBlock()
        {
            HukidasiPartExtension.EightParts().ForEach((v) => this.CheckEmpty(v));
            HukidasiPartExtension.EightParts().ForEach((v) => this.IsRectangleBlock(v));


            this.IsEqualHeightBlocks(HukidasiPart.LeftUpCorner, HukidasiPart.UpCenter, HukidasiPart.RightUpCorner);
            this.IsEqualHeightBlocks(HukidasiPart.LeftDownCorner, HukidasiPart.DownCenter, HukidasiPart.RightDownCorner);
            this.IsEqualWidthBlocks(HukidasiPart.LeftUpCorner, HukidasiPart.LeftCenter, HukidasiPart.LeftDownCorner);
            this.IsEqualWidthBlocks(HukidasiPart.RightUpCorner, HukidasiPart.RightCenter, HukidasiPart.RightDownCorner);

            this.IsEqualHeightBlocks(HukidasiPart.LeftCenter, HukidasiPart.RightCenter);
            
            this.IsEqualWidthBlocks(HukidasiPart.UpCenter, HukidasiPart.DownCenter);
            
            return this.CheckElements.All((v) => v);
        }

        #endregion
        #region modify
        public void AutoModify(AltText textmaker)
        {
            switch (this.Data.Type)
            {
                case HukidasiType.Block: AutoModifyThreeBlock(textmaker); break;
                case HukidasiType.FiveBlock: AutoModifyFiveBlock(textmaker); break;
            }
        }
        public void AutoModifyThreeBlock(AltText textmaker)
        {
            HukidasiPartExtension.EightParts().ForEach((v) => this.ModifyEmpty(v));
            HukidasiPartExtension.EightParts().ForEach((v) => this.RectanglizeBlock(textmaker, v));

            this.EqualizeHeightBlocks(HukidasiPart.LeftUpCorner, HukidasiPart.UpCenter, HukidasiPart.RightUpCorner);
            this.EqualizeHeightBlocks(HukidasiPart.LeftDownCorner, HukidasiPart.DownCenter, HukidasiPart.RightDownCorner);
            this.EqualizeWidthBlocks(textmaker, HukidasiPart.LeftUpCorner, HukidasiPart.LeftCenter, HukidasiPart.LeftDownCorner);
            this.EqualizeWidthBlocks(textmaker, HukidasiPart.RightUpCorner, HukidasiPart.RightCenter, HukidasiPart.RightDownCorner);

            this.EqualizeHeightBlocks(HukidasiPart.LeftCenter, HukidasiPart.RightCenter);

            this.EqualizeWidthBlocks(textmaker, HukidasiPart.UpCenter, HukidasiPart.DownCenter);
            
        }
        public void AutoModifyFiveBlock(AltText textmaker)
        {
            HukidasiPartExtension.Parts.ForEach((v) => this.ModifyEmpty(v));
            HukidasiPartExtension.Parts.ForEach((v)=> this.RectanglizeBlock(textmaker,v));

            this.EqualizeHeightBlocks(HukidasiPart.LeftUpCorner, HukidasiPart.UpCenterLeft, HukidasiPart.UpCenter, HukidasiPart.UpCenterRight, HukidasiPart.RightUpCorner);
            this.EqualizeHeightBlocks(HukidasiPart.LeftDownCorner, HukidasiPart.DownCenterLeft, HukidasiPart.DownCenter, HukidasiPart.DownCenterRight, HukidasiPart.RightDownCorner);
            this.EqualizeWidthBlocks(textmaker,HukidasiPart.LeftUpCorner, HukidasiPart.LeftCenterUp, HukidasiPart.LeftCenter, HukidasiPart.LeftCenterDown, HukidasiPart.LeftDownCorner);
            this.EqualizeWidthBlocks(textmaker, HukidasiPart.RightUpCorner, HukidasiPart.RightCenterUp, HukidasiPart.RightCenter, HukidasiPart.RightCenterDown, HukidasiPart.RightDownCorner);

            this.EqualizeHeightBlocks(HukidasiPart.LeftCenterUp, HukidasiPart.RightCenterUp);
            this.EqualizeHeightBlocks(HukidasiPart.LeftCenter, HukidasiPart.RightCenter);
            this.EqualizeHeightBlocks(HukidasiPart.LeftCenterDown, HukidasiPart.RightCenterDown);

            this.EqualizeWidthBlocks(textmaker, HukidasiPart.UpCenterLeft, HukidasiPart.DownCenterLeft);
            this.EqualizeWidthBlocks(textmaker, HukidasiPart.UpCenter, HukidasiPart.DownCenter);
            this.EqualizeWidthBlocks(textmaker, HukidasiPart.UpCenterRight, HukidasiPart.DownCenterRight);

        }
        #endregion
        #region EqualCheck
        private bool IsEqualSizeBlocks(params HukidasiPart[] parts)
        {
            var texts = parts.Select((v) => this.Data.GetPart(v));
            var f = SuouText.EqualRectangle(texts);
            if (!f)
            {
                parts.ForEach((v) => CheckElements[HukidasiSaveData.GetIndex(v)] = false);
                parts.ForEach((v) => this.Dic.Add(v, this.InequalMessage(parts)));
                
                //this.ErrorMessage.Add(this.InequalMessage(parts));
                return false;
            }
            return true;
        }
        private bool IsEqualHeightBlocks(params HukidasiPart[] parts)
        {

            var texts = parts.Select((v) => this.Data.GetPart(v));
            var n = texts.First().Count;
            var f = (texts.All((v) => v.Count == n));
            if (!f)
            {
                parts.ForEach((v) => CheckElements[HukidasiSaveData.GetIndex(v)] = false);
                parts.ForEach((v) => this.Dic.Add(v, this.InequalHeightMessage(parts)));
                //this.ErrorMessage.Add(this.InequalHeightMessage(parts));
                return false;
            }
            else
            {
                return true;
            }
        }
        private bool IsEqualWidthBlocks(params HukidasiPart[] parts)
        {

            var texts = parts.Select((v) => this.Data.GetPart(v));
            var b = texts.All((v) => v.IsRectangle());
            var n = texts.First().Width;
            var f = texts.All((v) => v.Width == n);
            if (!f || !b)
            {
                parts.ForEach((v) => CheckElements[HukidasiSaveData.GetIndex(v)] = false);
                parts.ForEach((v) => this.Dic.Add(v, this.InequalWidthMessage(parts)));
                //this.ErrorMessage.Add(this.InequalWidthMessage(parts));
                return false;
            }
            return true;
        }
        private void EqualizeHeightBlocks(params HukidasiPart[] parts)
        {

            var texts = parts.Select((v) => this.Data.GetPart(v));
            if (texts.Any((v) => !v.IsRectangle())) throw new Exception();
            var n = texts.Max((v) => v.Count);
            var newtexts = texts.Select((v) => v.CreateRectangle(n)).ToArray();

            newtexts.Count().ForEach((i) => this.Data[parts[i]] = newtexts[i]);
        }

        private void EqualizeSizeBlocks(AltText textmaker,params HukidasiPart[] parts)
        {
            var texts = parts.Select((v) => this.Data.GetPart(v));
            var f = textmaker.MakeSameSizeRectangleText(texts.ToArray());
            parts.Count().ForEach((i) => this.Data[parts[i]] = f[i]);
        }
        private void EqualizeWidthBlocks(AltText textmaker, params HukidasiPart[] parts)
        {
            var texts = parts.Select((v) => this.Data.GetPart(v));
            var f = textmaker.MakeSameWidthRectangleText(texts.ToArray());
            //int n = 0;
            parts.Count().ForEach((i) => this.Data[parts[i]] = f[i]);
        }
        private bool IsRectangleBlock(HukidasiPart part)
        {
            var block = this.Data.GetPart(part);
            if (!block.IsRectangle())
            {
                this.Dic.Add(part, this.LineWidthInequalMessage(part));
                //this.ErrorMessage.Add(this.LineWidthInequalMessage(part));
                this.CheckElements[HukidasiSaveData.GetIndex(part)] = false;
                return false;
            }
            return true;
        }
        private void RectanglizeBlock(AltText textmaker,HukidasiPart part)
        {
            var block = this.Data.GetPart(part);
            if (!block.IsRectangle())
            {
                this.Data[part] = textmaker.MakeRectangleText(block, true);
            }
        }
        #endregion
        public bool CheckUpCenterBottomLength()
        {
            var f = this.Data.BeraIterator(40);
            if (f.Min() <= this.Data.Options.UpCenterBottomPreZure) return true;

            //var part = HukidasiPart.DownCenter;
            //this.CheckElements[HukidasiSaveData.GetIndex(part)] = false;
            this.Dic.Add(HukidasiPart.None, this.ErrorUpCenterBottomMessage());
            //this.ErrorMessage.Add(this.ErrorEmptyMessage(part));
            return false;
        }
        public bool CheckEmpty(HukidasiPart part)
        {
            var f = this.Data.GetPart(part);
            if (f.Width == 0)
            {
                this.CheckElements[HukidasiSaveData.GetIndex(part)] = false;
                this.Dic.Add(part, this.ErrorEmptyMessage(part));
                //this.ErrorMessage.Add(this.ErrorEmptyMessage(part));
                return false;
            }
            else
            {
                return true;
            }
        }
        public void ModifyEmpty(HukidasiPart part)
        {
            var f = this.Data.GetPart(part);
            if(f.Width == 0)f.Initialize("@");
        }

        #region ErrorMessage
        public string InequalMessage(params HukidasiPart[] parts)
        {
            var anglenames = parts.Select((v) => this.Data.GetName(v));
            var anglesname = String.Join("と", anglenames);
            var msg = anglesname + "の行数と幅が揃いません";
            return msg;
        }
        public string InequalHeightMessage(params HukidasiPart[] parts)
        {
            var anglenames = parts.Select((v) => this.Data.GetName(v));
            var anglesname = String.Join("と", anglenames);
            var msg = anglesname + "の行数が揃いません";
            return msg;
        }
        public string InequalWidthMessage(params HukidasiPart[] parts)
        {
            var anglenames = parts.Select((v) => this.Data.GetName(v));
            var anglesname = String.Join("と", anglenames);
            var msg = anglesname + "の幅が揃いません";
            return msg;
        }
        public string LineWidthInequalMessage(HukidasiPart part)
        {
            var anglename = this.Data.GetName(part);
            var msg = anglename + "の各行の幅が揃いません";
            return msg;
        }
        public string ErrorEmptyMessage(HukidasiPart part)
        {
            var anglename = this.Data.GetName(part);
            var msg = anglename + "の幅が0です";
            return msg;
        }
        public string ErrorUpCenterBottomMessage()
        {
            var msg = "上段・中段・下段の幅が生成時に揃いません";
            return msg;
        }
        #endregion
    }

    
}
