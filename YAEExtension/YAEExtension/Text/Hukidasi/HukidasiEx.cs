﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text
{
    static class HukidasiEx
    {
        public static IEnumerable<int> UpBarIterator(this HukidasiSaveData item, int limit)
        {
            var n = item.LeftUp.Width + item.RightUp.Width + item.UpCenter.Width;
            var i = 0;
            while (i++ < limit)
            {
                n += item.UpCenterLeft.Width;
                yield return n;
                n += item.UpCenterRight.Width;
                yield return n;

            }
        }
        public static IEnumerable<int> BottomBarIterator(this HukidasiSaveData item, int limit)
        {
            var n = item.LeftDown.Width + item.RightDown.Width + item.DownCenter.Width;
            var i = 0;
            while (i++ < limit)
            {
                n += item.DownCenterLeft.Width;
                yield return n;
                n += item.DownCenterRight.Width;
                yield return n;

            }
        }
        public static IEnumerable<int> BeraIterator(this HukidasiSaveData item, int limit)
        {
            var a = item.UpBarIterator(limit);
            var b = item.BottomBarIterator(limit);

            IEnumerable<int> BottomIterator = b;
            int LowerValue = 0;
            int UpperValue = 0;
            var d = b.First();
            foreach (var c in a)
            {
                int skip = 0;
                foreach (var g in BottomIterator)
                {
                    if (g < c)
                    {
                        LowerValue = g;
                    }
                    else
                    {
                        UpperValue = g;
                        break;
                    }
                    skip++;
                }
                BottomIterator = BottomIterator.Skip(skip);
                yield return Math.Min(Math.Abs(c - LowerValue), Math.Abs(UpperValue - c));
            }
        }

        
    }
}
