﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using SuouBase;
using System.Diagnostics;
namespace SuouBase.Text
{
    
    // Block・・・Textの塊を作成する
    
    
    public static class TextBlockMethod
    {
        public static bool CanYokoJoin(this SuouText LeftBlock, SuouText RightBlock)
        {
            if (LeftBlock.Count == RightBlock.Count) return true;
            return false;
        }
        public static bool CanTateJoin(this SuouText LeftBlock, SuouText RightBlock)
        {
            if (LeftBlock.Width == RightBlock.Width) return true;
            return false;
        }

        public static SuouText YokoJoin(this SuouText LeftBlock, SuouText RightBlock)
        {
            if (!LeftBlock.CanYokoJoin(RightBlock))
            {
                var v = LeftBlock + RightBlock;
                WriterExtensions.WriteLine(v.Text, "Error.log");
                throw new MyException("");
            }

            return LeftBlock + RightBlock;
        }
        public static SuouText TateJoin(this SuouText UpBlock, SuouText BottomBlock)
        {

            if (!UpBlock.CanTateJoin(BottomBlock))
            {
                var v = UpBlock % BottomBlock;
                WriterExtensions.WriteLine(v.Text, "Error.log");
                throw new MyException("Error");
            }
            return UpBlock % BottomBlock;
        }
    }

   
}
