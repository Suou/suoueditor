﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
namespace SuouBase.Text.Hukidasi.WakuLogic
{
    class CenterElement:IIncrementElement
    {
        public ThreeTateElement LeftElement { get; set; }
        public SpaceElement LeftSpaceElement { get; set; }
        public TextElement TextElement { get; set; }
        public SpaceElement RightSpaceElement { get; set; }
        public ThreeTateElement RightElement { get; set; }
        private HukidasiSaveData SaveData { get; set; }
        private ISpaceMaker SpaceMaker { get; set; }

        private Inter SpacePinter = new TwoPinter();
        public CenterElement(ThreeTateElement left, TextElement text, ThreeTateElement right, ISpaceMaker spaceMaker, HukidasiSaveData h,Inter spaceInter)
        {
            this.SaveData = h;
            this.SpaceMaker = spaceMaker;
            this.LeftElement = left;
            this.TextElement = text;
            this.RightElement = right;
            this.SpacePinter = spaceInter;
            this.SetupElement();


        }
        private void SetupElement()
        {
            this.LeftElement.CountHeight = this.TextElement.CountHeight;
            this.RightElement.CountHeight = this.TextElement.CountHeight;
            this.LeftSpaceElement = this.CreateLeftSpaceElement();
            this.RightSpaceElement = this.CreateRightSpaceElement();
        }

        public ICenterElementItem[] Items
        {
            get
            {
                return new ICenterElementItem[] { this.LeftElement, this.LeftSpaceElement, this.TextElement, this.RightSpaceElement, this.RightElement };
            }
        }
        private SpaceElement CreateLeftSpaceElement()
        {
            var pairs = this.LeftElement.PaddingOffset.Zip(this.TextElement.LeftPaddingOffset);
            var offsetArray = pairs.Select((v) => v.Key + v.Value).ToArray();
            return new SpaceElement(offsetArray, this.SaveData.Padding.LeftSpace, this.SpaceMaker);

        }
        private SpaceElement CreateRightSpaceElement()
        {
            var pairs = this.RightElement.PaddingOffset.Zip(this.TextElement.RightPaddingOffset);
            var offsetArray = pairs.Select((v) => v.Key + v.Value).ToArray();
            return new SpaceElement(offsetArray, this.SaveData.Padding.RightSpace, this.SpaceMaker);
        }
        public int DotWidth
        {
            get { return this.Items.Sum((v) => v.TopLineWidth); }
        }
        public void IncrementWidth()
        {
            var p = SpacePinter.Get();
            if (p == 0)
            {
                this.LeftSpaceElement.Incliment();
            }
            else
            {
                this.RightSpaceElement.Incliment();
            }
        }
        public int CountHeight
        {
            get { return this.TextElement.CountHeight; }
        }
        public SuouText SText
        {
            get
            {
                return this.LeftElement.SText + this.LeftSpaceElement.SText + this.TextElement.SText + this.RightSpaceElement.SText + this.RightElement.SText;
            }
        }
    }
}
