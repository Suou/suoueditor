﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text.Hukidasi.WakuLogic
{

    class TextElement : ICenterElementItem
    {

        public int TopLineWidth
        {
            get { return this.SText.Lines.First().Width; }
        }
        public TextAlign Align { get; set; }
        public TextElement(SuouText text, WakuPadding pad,TextAlign align,ISpaceMaker spacer)
        {
            this.Align = align;
            if (this.Align == TextAlign.Tate)
            {
                var f = new TategakiElement(text, 16, spacer);
                this.SText = f.SText;
            }
            else
            {
                this.SText = text.InsertEachSpaceLine(pad.CenterSpace).InsertTop(pad.TopSpace).InsertBottom(pad.BottomSpace);
            }
        }
        public TextElement(SuouText text, TextAlign align)
        {
            this.Align = align;
            this.SText = text.Clone();
        }
        public int CountHeight
        {
            get { return this.SText.Count; }
        }
        public int[] LeftPaddingOffset
        {
            get
            {
                switch (this.Align)
                {
                    case TextAlign.Left:
                    case TextAlign.Tate:
                        return Enumerable.Range(0,this.SText.Count).Select((v)=>0).ToArray();
                    case TextAlign.Center:
                        return this.SText.Lines.Select((v) => (this.MaxWidth - v.Width)/2).ToArray();
                    case TextAlign.Right:
                        return this.SText.Lines.Select((v)=>this.MaxWidth - v.Width).ToArray();
                    default:
                        return null;
                }
            }
        }
        public int[] RightPaddingOffset
        {
            get
            {
                switch (this.Align)
                {
                    case TextAlign.Left:
                    case TextAlign.Tate:
                        return this.SText.Lines.Select((v) => this.MaxWidth - v.Width).ToArray();
                    case TextAlign.Center:
                        return this.SText.Lines.Select((v) => (this.MaxWidth - v.Width) / 2).ToArray();
                    case TextAlign.Right:
                        return Enumerable.Range(0, this.SText.Count).Select((v) => 0).ToArray();
                    default:
                        return null;
                }
            }
        }

        public int[] DotWidth
        {
            get
            {
                return this.SText.Lines.Select((v) => v.Width).ToArray();
            }
        }
        public int MaxWidth { get { return this.SText.Width; } }

        public SuouText SText { get; set; }
    }
}
