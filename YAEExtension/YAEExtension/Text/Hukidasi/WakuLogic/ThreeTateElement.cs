﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using BSystem.Collections;
using BSystem;
namespace SuouBase.Text.Hukidasi.WakuLogic
{
    public interface ICenterElementItem
    {
        int TopLineWidth { get; }
        SuouText SText { get; }
    }
    public interface IIncrementElement
    {
        int DotWidth { get; }
        void IncrementWidth();
        SuouText SText { get; }
    }
    class ThreeTateElement:ICenterElementItem
    {

        public LogicElement UpBlock { get; set; }
        public LogicElement CenterBlock { get; set; }
        public LogicElement DownBlock { get; set; }

        private List<LogicElement> _Elements = new List<LogicElement>();
        public List<LogicElement> Elements { get { return this._Elements; } }
        private Inter TateInter = new StandardThreePinter();
        private AltText _SpaceMaker = null;
        private AltText SpaceMaker
        {
            get { return this._SpaceMaker; }
            set
            {
                this._SpaceMaker = value;
            }
        }

        public ThreeTateElement(LogicElement top, LogicElement center, LogicElement bottom,AltText spaceMaker,Inter tateInter)
        {
            this.SpaceMaker = spaceMaker;
            this.UpBlock = top;
            this.CenterBlock = center;
            this.DownBlock = bottom;
            this.TateInter = tateInter;
        }

        public int[] DotWidth
        {
            get
            {
                return this.Elements.SelectMany((v) => v.DotWidthes).ToArray();
                
            }
        }
        public int[] PaddingOffset
        {
            get
            {
                return this.DotWidth.Select((v) => this.MaxDotWidth - v).ToArray();
            }
        }
        public int TopLineWidth
        {
            get { return this.Elements.First().FirstLineWidth; }
        }
        public int CountHeight
        {
            get { return this.Elements.Sum((v)=>v.Text.Count); }
            set
            {
                if (this.CountHeight > value || value < 1) throw new Exception();
                while (this.CountHeight < value) this.InclimentHeight();
            }
        }
        public void InclimentHeight()
        {
            var p = this.TateInter.Get();
            LogicElement r = null;
            switch (System.Math.Abs(p))
            {
                case 1: r = this.UpBlock; break;
                case 2: r = this.CenterBlock; break;
                case 3: r = this.DownBlock; break;
                default: r = this.UpBlock; break;
                
            }
            if (p < 0)
            {
                this.Elements.Insert(0, r);
            }
            else
            {
                this.Elements.Add(r);
            }
            if (this.MaxDotWidth < r.MaxWidth) this.MaxDotWidth = r.MaxWidth;
        }
        public int MaxDotWidth { get; private set; }

        public SuouText SText
        {
            get
            {
                Func<SuouText, SuouText,SuouText> func = (a, b) => a % b;
                var p = this.Elements.Select((v) => v.CreateSText(this.SpaceMaker)).Fold(func);
                return p;
            }
        }
    }


}
