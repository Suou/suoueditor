﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace SuouBase.Text.Hukidasi.WakuLogic
{

    public interface Inter
    {
        int Get();
    }

    public class TwoPinter : Inter
    {
        private bool Flag = false;
        public virtual int Get()
        {
            Flag = !Flag;
            return Flag ? 0 : 1;
        }
    }
    public class StandardThreePinter : TwoPinter
    {
        private bool FirstFlag = true;
        public override int Get()
        {
            if (this.FirstFlag)
            {
                FirstFlag = false;
                return 1;
            }
            else
            {
                return base.Get() * 2;
            }
        }
    }

    [Serializable]
    public class PeriodicRepresent : Inter
    {
        private int Index = 0;
        private bool IsFirstFinished = false;
        public int[] Periodic { get; protected set; }
        public int[] FirstIntegers { get; protected set; }

        public PeriodicRepresent(string s,string fixS)
        {
            var p = PeriodicRepresent.Parse(s);
            var g = PeriodicRepresent.Parse(fixS);
            if (p == null) p = new int[] { 1, 2, 3 };
            if (g == null) g = new int[]{};
            this.Periodic = p;
            this.FirstIntegers = g;
        }

        public PeriodicRepresent(int[] p,int[] fixp)
        {
            this.Periodic = p.ToArray();
            this.FirstIntegers = fixp;
        }
        public PeriodicRepresent()
        {
            this.Periodic = new int[3] { 1, 2, 3 };
            this.FirstIntegers = new int[] { };
        }
        public int[] FixingGet()
        {
            return this.FirstIntegers;
        }
        public int[] PeriodicGet()
        {
            return this.Periodic;
        }
        public static int[] Parse(string s)
        {
            if (s.Length == 0)
            {
                return null;
            }
            else
            {
                try
                {
                    return s.Split(',').Select((v) => int.Parse(v)).ToArray();
                }
                catch (FormatException)
                {
                    return null;
                }
            }
        }
        public static PeriodicRepresent Create(string s,string fixs)
        {
            var p = HukidasiXML.IntArrayParse(s);
            var q = HukidasiXML.IntArrayParse(fixs);
            if (p != null)
            {
                return new PeriodicRepresent(p,q);
            }
            else
            {
                return new PeriodicRepresent();
            }
        }


        public string SerializeString
        {
            get
            {
                return string.Join(",", this.Periodic);
            }
        }
        public string SerializeFixString
        {
            get
            {
                return string.Join(",", this.FirstIntegers);
            }
        }

        public HukidasiPart[] TranslatePeriodic(HukidasiPart[] items)
        {
            //?
            return this.Periodic.Select((v)=>Math.Abs(v)).Where((v)=> v > 0 && v <= 3).Select((v) => items[v-1]).ToArray();
        }
        public HukidasiPart[] TranslateFixing(HukidasiPart[] items)
        {
            //?
            return this.FirstIntegers.Select((v) => Math.Abs(v)).Where((v) => v > 0 && v <= 3).Select((v) => items[v-1]).ToArray();
        }
        public Inter CreaterInstance()
        {
            return new PeriodicRepresent(this.Periodic,this.FirstIntegers);
        }
        public int Get()
        {
            if (this.IsFirstFinished || this.FirstIntegers.Length == 0)
            {
                var n = this.Periodic[Index++];
                if (Index == this.Periodic.Length) Index = 0;
                return n;
            }
            else
            {
                var n = this.FirstIntegers[Index++];
                if (Index == this.FirstIntegers.Length)
                {
                    Index = 0;
                    this.IsFirstFinished = true;
                }
                return n;
            }
        }
        #region Override
        public override int GetHashCode()
        {
            int n = 0;
            foreach(var v in this.Periodic){
                n = n ^ v;
            }
            return n;
        }
        public override bool Equals(object obj)
        {
            if (obj is PeriodicRepresent)
            {
                var v = (PeriodicRepresent)obj;
                if (this.Periodic.Length == v.Periodic.Length)
                {
                    for (int i = 0; i < this.Periodic.Length; i++)
                    {
                        if (this.Periodic[i] != v.Periodic[i]) return false;
                    }
                    return true;
                }
            }
            return false;
        }
        #endregion
    }
    public class SpaceAlignRepresent : PeriodicRepresent
    {
        public SpaceAlignRepresent(TextAlign align):base()
        {
            switch (align)
            {
                case TextAlign.Left: this.Periodic = new int[] { 1 }; break;
                case TextAlign.Center: this.Periodic = new int[] { 0,1 }; break;
                case TextAlign.Right: this.Periodic = new int[] { 0 }; break;
                case TextAlign.Tate: this.Periodic = new int[] { 1 }; break;
            }
        }
        public SpaceAlignRepresent Create(TextAlign align)
        {
            return new SpaceAlignRepresent(align);
        }
    }

}
