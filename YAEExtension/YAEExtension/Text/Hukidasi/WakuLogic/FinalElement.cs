﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;

namespace SuouBase.Text.Hukidasi.WakuLogic
{
    class FinalElement
    {
        FiveYokoElement UpElement { get; set; }
        CenterElement CenterElement { get; set; }
        FiveYokoElement DownElement { get; set; }
        readonly int UpCenterBottomWidthPermitDiff = 0;
        IIncrementElement[] Items { get; set; }
        public FinalElement(FiveYokoElement up, CenterElement center, FiveYokoElement down,int threeWidthDiff)
        {
            this.UpElement = up;
            this.CenterElement = center;
            this.DownElement = down;
            this.Items = new IIncrementElement[] { this.UpElement, this.CenterElement, this.DownElement };
            this.UpCenterBottomWidthPermitDiff = threeWidthDiff;
            this.IncrementUntilOk();
        }
        
        public bool Check()
        {
            return this.DiffElementWidth <= this.UpCenterBottomWidthPermitDiff;
        }
        public int DiffElementWidth
        {
            get
            {
                var n =  this.Items.Max((v) => v.DotWidth) - this.Items.Min((v) => v.DotWidth);
                return n;
            }
        }

        public void Increment()
        {
            IIncrementElement minWidthItem = this.Items.Min2((v) => v.DotWidth);
            minWidthItem.IncrementWidth();
        }
        public void IncrementUntilOk()
        {
            while (!this.Check()) this.Increment();
        }
        public SuouText SText
        {
            get
            {
                return this.UpElement.SText % this.CenterElement.SText % this.DownElement.SText;
            }
        }
    }
}
