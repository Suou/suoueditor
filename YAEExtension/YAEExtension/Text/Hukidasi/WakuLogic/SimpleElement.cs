﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace SuouBase.Text.Hukidasi.WakuLogic
{
    class SimpleElement
    {
        public string Name { get; set; }
        public SimpleElement(LogicElement element, AltText spaceMaker)
        {
            if (element.Text == null) throw new Exception();

            this.OriginalText = element.Text.Clone();
            this.Element = element;
            this.SpaceMaker = spaceMaker;
            //this.TextElement = this.OriginalText;
        }
        private int InclimentWidthTimes = 1;
        private int InclimentHeightTimes = 1;
        private LogicElement Element { get; set; }
        public AltText SpaceMaker { get; set; }
        public bool IsError { get; set; }
        public Size EditSize { get { return new Size() { Width = this.DotWidth, Height = this.Height }; } }
        public int DotWidth { get { return this.OriginalText.Width * InclimentWidthTimes + this.Element.Offset; } }
        public int Height
        {
            get
            {
                var f = this.OriginalText.Count * this.InclimentHeightTimes;
                return f;
            }
        }
        public int UpperWidth
        {
            get
            {
                return this.DotWidth;
            }
        }
        public int LowerWidth
        {
            get
            {
                return this.DotWidth;
            }
        }
        public int CardinalNumber { get; private set; }
        private SuouText OriginalText { get; set; }
        
        public SuouText CreateSText()
        {
            return this.TextElement;
        }
        public void Incliment(ExpandDirection Dir)
        {
            CardinalNumber++;
            if (Dir == ExpandDirection.Tate)
            {
                this.InclimentHeightTimes++;
            }
            else
            {
                this.InclimentWidthTimes++;
            }
        }

        public SuouText TextElement
        {
            get
            {
                var f = this.OriginalText.CreateTimesRectangle(this.InclimentWidthTimes, this.InclimentHeightTimes);
                if (this.Element.Offset != 0)
                {
                    var spaceRec = this.SpaceMaker.MakeRectangleText(SuouEditorConst.AAFont, SuouEditorConst.TextColor, new Size(this.Element.Offset, f.Count));
                    if (this.Element.IsLeft)
                    {
                        return spaceRec + f;
                    }
                    else
                    {
                        return f + spaceRec;
                    }
                }
                else
                {
                    return f;
                }
            }
        }
    }
}
