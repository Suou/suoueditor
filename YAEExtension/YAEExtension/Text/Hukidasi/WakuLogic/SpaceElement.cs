﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text.Hukidasi.WakuLogic
{
    class SpaceElement : ICenterElementItem
    {
        public int[] _Width { get; set; }
        public int Offset { get; set; }
        private ISpaceMaker SpaceMaker { get; set; }
        public SpaceElement(int[] width, int offset, ISpaceMaker spacer)
        {
            this._Width = width;
            this.Offset = offset;
            this.SpaceMaker = spacer;
            if (!this.Check()) this.Incliment();
        }
        public void Incliment()
        {
            do
            {
                this.Offset++;
            } while (!this.Check());
        }
        public bool Check()
        {
            return this.DotWidth.All((v) => this.SpaceMaker.CanCreateSpace(v));
        }
        public int[] DotWidth
        {
            get
            {
                return this._Width.Select((v) => v + this.Offset).ToArray();
            }
        }
        public int TopLineWidth
        {
            get { return this.DotWidth.First(); }
        }
        public SuouText SText
        {
            get
            {
                var p = this.DotWidth.Select((v) => this.SpaceMaker.CreateSpace(v));
                var g = new SuouText(SuouEditorConst.AAFont, SuouEditorConst.TextColor, p);
                return g;
            }
        }
    }
}
