﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text.Hukidasi.WakuLogic
{
    class TategakiElement
    {
        IList<TategakiElementElement> Items = new List<TategakiElementElement>();
        public TategakiElement(SuouText text, int rowWidth, ISpaceMaker spacer)
        {
            var maxHeight = text.Lines.Max((v) => v.Length);
            this.Items = text.Lines.Select((v) => new TategakiElementElement(v, spacer, rowWidth,maxHeight)).ToList();
        }
        public SuouText SText
        {
            get
            {
                SuouText r = null;
                for (int i = 0; i < this.Items.Count; i++)
                {
                    if (r == null)
                    {
                        r = this.Items[i].SText;
                    }
                    else
                    {
                        r = this.Items[i].SText + r;
                    }
                }
                return r;
            }
        }
    }
    class TategakiElementElement
    {
        TextElement Line { get; set; }
        SpaceElement LineSpace { get; set; }
        public TategakiElementElement(SuouLine line, ISpaceMaker spacer,int rowWidth,int height)
        {
            this.Line = new TextElement(line.Taterize(height),TextAlign.Left);
            this.LineSpace = new SpaceElement(this.Line.RightPaddingOffset, rowWidth, spacer);
        }
        public void Setup()
        {
        }
        public SuouText SText
        {
            get
            {
                return this.Line.SText + this.LineSpace.SText;
            }
        }
    }
}
