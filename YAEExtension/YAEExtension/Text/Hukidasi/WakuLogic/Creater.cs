﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using BSystem;
namespace SuouBase.Text.Hukidasi.WakuLogic
{

    public class FiveBlockHukidasi : IHukidasiLogic
    {

        public static SuouText Create(HukidasiSaveData h, SuouText t)
        {
            var v = new FiveBlockHukidasi(h);
            var gv = AltText.Create(h.MergeAlgorithm);
            return v.CreateWaku(t, gv);

        }
        private LogicElement LeftUp { get { return Blocks[0]; } }
        private LogicElement CenterUp { get { return Blocks[1]; } }
        private LogicElement RightUp { get { return Blocks[2]; } }
        private LogicElement LeftCenter { get { return Blocks[3]; } }
        private LogicElement RightCenter { get { return Blocks[4]; } }
        private LogicElement LeftDown { get { return Blocks[5]; } }
        private LogicElement CenterDown { get { return Blocks[6]; } }
        private LogicElement RightDown { get { return Blocks[7]; } }
        private LogicElement L21 { get { return Blocks[8]; } }
        private LogicElement L41 { get { return Blocks[9]; } }
        private LogicElement L12 { get { return Blocks[10]; } }
        private LogicElement L52 { get { return Blocks[11]; } }
        private LogicElement L14 { get { return Blocks[12]; } }
        private LogicElement L54 { get { return Blocks[13]; } }
        private LogicElement L25 { get { return Blocks[14]; } }
        private LogicElement L45 { get { return Blocks[15]; } }
        private LogicElement[] Blocks = new LogicElement[16];
        public HukidasiSaveData SaveData { get; private set; }

        public AltText WakuTextMaker { get; set; }
        public AltText UniTextMaker = AltText.UniDefault();

        public static ExpansionBlockHukidasi Default()
        {
            /*
            string[] s = new string[] { "┏", "┃", "┗", "━", "━", "┓", "┃", "┛" };
            var f = new HukidasiSaveData(s, HukidasiType.Block, HukidasiZureOption.LineRestriction, 5, SpaceLineSet.DefaultSet,false);
            */
            return new ExpansionBlockHukidasi(HukidasiSaveData.FiveDefault());
        }

        public FiveBlockHukidasi(HukidasiSaveData h)
            : this(h.CreateLogicElements())
        {
            this.SaveData = (HukidasiSaveData)h.Clone();
            this.WakuTextMaker = AltText.Create(h.MergeAlgorithm);
            this.Blocks = h.CreateLogicElements();
        }
        private FiveBlockHukidasi(LogicElement[] items)
        {
            for (int i = 0; i < 16; i++)
            {
                var a = SuouText.Empty();
                a.Initialize(items[i].Text);
                if (a.Width != a.Lines.Max((t) => a.Width)) throw new Exception();
                this.Blocks[i] = items[i];
            }
        }

        public bool Check
        {
            get
            {
                return true;
            }
        }

        public SuouText CreateWaku(SuouText text, AltText textMaker)
        {
            if (textMaker == null) throw new MyException("");
            var spaceInter = new SpaceAlignRepresent(this.SaveData.Arrangement.Align);


            FiveYokoElement UpElement = new FiveYokoElement(this.LeftUp, this.L21, this.CenterUp, this.L41, this.RightUp, this.SaveData,this.UniTextMaker,this.SaveData.Arrangement.UpPathInter);
            TextElement TextBlock = new TextElement(text, this.SaveData.Padding,this.SaveData.Arrangement.Align,textMaker.SpaceMaker);
            ThreeTateElement LeftElement = new ThreeTateElement(this.L12, this.LeftCenter, this.L14,this.UniTextMaker,this.SaveData.Arrangement.LeftPathInter);
            ThreeTateElement RightElement = new ThreeTateElement(this.L52, this.RightCenter, this.L54,this.UniTextMaker,this.SaveData.Arrangement.RightPathInter);
            CenterElement centerElement = new CenterElement(LeftElement, TextBlock, RightElement, this.WakuTextMaker.SpaceMaker,this.SaveData,spaceInter);
            FiveYokoElement BottomElement = new FiveYokoElement(this.LeftDown, this.L25, this.CenterDown, this.L45, this.RightDown, this.SaveData,this.UniTextMaker,this.SaveData.Arrangement.DownPathInter);
            FinalElement FinalElement = new FinalElement(UpElement, centerElement, BottomElement,this.SaveData.Options.UpCenterBottomPreZure);
            return FinalElement.SText;
        }
    }
}
