﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
namespace SuouBase.Text.Hukidasi.WakuLogic
{
    class FiveYokoElement:IIncrementElement
    {
        public LogicElement LeftElement { get; set; }
        public LogicElement LeftMiddleElement { get; set; }
        public LogicElement CenterElement { get; set; }
        public LogicElement RightMiddleElement { get; set; }
        public LogicElement RightElement { get; set; }
        public HukidasiSaveData SaveData { get; set; }
        public Inter YokoInter { get; set; }

        private AltText SpaceMaker { get; set; }
        private List<LogicElement> _Elements = new List<LogicElement>();
        public List<LogicElement> Elements { get { return this._Elements; } }
        public IEnumerable<LogicElement> FinalElements
        {
            get 
            {
                yield return this.LeftElement;
                foreach (var v in this.Elements) yield return v;
                yield return this.RightElement;
            }
        }

        public FiveYokoElement(LogicElement left, LogicElement leftmiddle, LogicElement center, LogicElement rightmiddle,
            LogicElement right, HukidasiSaveData h,AltText spaceMaker,Inter yokoInter)
        {
            this.SpaceMaker = spaceMaker;
            this.LeftElement = left;
            this.LeftMiddleElement = leftmiddle;
            this.CenterElement = center;
            this.RightMiddleElement = rightmiddle;
            this.RightElement = right;
            this.SaveData = h;

            this.YokoInter = yokoInter;
            this.DotWidth = this.LeftElement.MaxWidth + this.RightElement.MaxWidth;
        }

        public int DotWidth
        {
            get;
            private set;
        }
        public int CountHeight
        {
            get { return this.LeftElement.Text.Count; }
            
        }
        public void IncrementWidth()
        {
            var p = this.YokoInter.Get();
            LogicElement r = null;
            switch (Math.Abs(p))
            {
                case 1: r = this.LeftMiddleElement; break;
                case 2: r = this.CenterElement; break;
                case 3: r = this.RightMiddleElement; break;
                default: r = this.LeftMiddleElement; break;
            }
            if (p < 0)
            {
                this.Elements.Insert(0, r);
            }
            else
            {
                this.Elements.Add(r);
            }
            this.DotWidth += r.MaxWidth;
        }

        public SuouText SText
        {
            get
            {
                Func<SuouText, SuouText, SuouText> func = (a, b) => a + b;
                var p = this.FinalElements.Select((v) => v.CreateSText(this.SpaceMaker)).Fold(func);
                return p;
            }
        }
    }
}
