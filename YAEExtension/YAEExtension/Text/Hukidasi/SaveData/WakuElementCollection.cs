﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text.Hukidasi.WakuLogic;
using BSystem.Collections;
using BSystem;
using SuouBase.Interface;
using System.Xml.Linq;
namespace SuouBase.Text
{
    [Serializable]
    public class WakuElementCollection:IParse
    {
        private LogicElement[] _Items = null;
        public LogicElement[] Items
        {
            get { return this._Items; }
            set { this._Items = value; }
        }

        public SuouText[] Wakus
        {
            get
            {
                return this.Items.Select((v) => v.Text).ToArray();
            }
            private set
            {
                if (value == null) return;
                var f = value.Select((v) => { return v == null ? SuouText.Empty() : v; }).ToList();
                while (f.Count < 16) f.Add(SuouText.Empty());
                //this._WakuItems = f.ToArray();
            }
        }
        public WakuElementCollection()
        {
        }
        public WakuElementCollection(SuouText[] wakus, int[] offsets10)
        {
            if (offsets10 == null) offsets10 = new int[10];
            if (offsets10.Length != 10) throw new Exception();
            var offsets16 = CreateInternalOffset(offsets10);
            this.Items = HukidasiPartExtension.Parts.Zip(wakus).Zip(offsets16).Select((v)=>new LogicElement(v.Key.Value,v.Value,v.Key.Key)).ToArray();
        }
        private static int[] CreateInternalOffset(int[] offsets10)
        {
            OffsetCheck(offsets10);

            var r = new int[16];
            int i = 0;
            foreach (var p in OffsetNames)
            {
                r[HukidasiSaveData.GetIndex(p)] = offsets10[i++];
            }
            return r;
        }

        //private int[] _Offset = new int[10];
        public int[] Offset
        {
            get
            {
                if (this.Items.Any((v)=>v==null))
                {
                    throw new MyException("");
                }
                else
                {
                    return OffsetNames.Select((v) => this.GetElement(v).Offset).ToArray();
                }
            }
            /*
            set
            {
                this._Offset = value;
                this.OffsetCheck();
            }
            */
        }

        public static HukidasiPart[] OffsetNames
        {
            get
            {
                return new HukidasiPart[]{HukidasiPart.LeftUpCorner,HukidasiPart.LeftCenterUp,HukidasiPart.LeftCenter,
                HukidasiPart.LeftCenterDown,HukidasiPart.LeftDownCorner,
                HukidasiPart.RightUpCorner,HukidasiPart.RightCenterUp,HukidasiPart.RightCenter,
                HukidasiPart.RightCenterDown,HukidasiPart.RightDownCorner};
            }
        }
        /*
        public int LeftUpOffset { get { return this.Offset[0]; } }
        public int LeftCenterUpOffset { get { return this.Offset[1]; } }
        public int LeftCenterOffset { get { return this.Offset[2]; } }
        public int LeftCenterDownOffset { get { return this.Offset[3]; } }
        public int LeftDownOffset { get { return this.Offset[4]; } }
        public int RightUpOffset { get { return this.Offset[5]; } }
        public int RightCenterUpOffset { get { return this.Offset[6]; } }
        public int RightCenterOffset { get { return this.Offset[7]; } }
        public int RightCenterDownOffset { get { return this.Offset[8]; } }
        public int RightDownOffset { get { return this.Offset[9]; } }
        */
        public LogicElement GetElement(HukidasiPart p)
        {
            return this.Items[HukidasiSaveData.GetIndex(p)];
        }
        public SuouText GetText(HukidasiPart p)
        {
            return this.Items[HukidasiSaveData.GetIndex(p)].Text;
        }

        protected static void OffsetCheck(int[] offsets)
        {
            for (int i = 0; i < 10; i++) OffsetCheck(i,offsets);
        }
        protected static void OffsetCheck(int n,int[] offsets)
        {
            if (n >= 0 && n < 5)
            {
                if (offsets[n] < 0)
                {
                    var d = -offsets[n];
                    for (int i = 0; i < 5; i++) offsets[i] += d;
                }
            }
            else
            {
                if (offsets[n] < 0)
                {
                    var d = -offsets[n];
                    for (int i = 5; i < 10; i++) offsets[i] += d;
                }
            }
        }
        
        public int GetElementsWidth(IEnumerable<HukidasiPart> items)
        {
            return items.Select((v) => this.GetElement(v)).Sum((v) => v.MaxWidth);
        }
        
        public int UpCornersWidth
        {
            get
            {
                return this.GetElementsWidth(HukidasiPartExtension.UpCornersParts);
            }
        }
        public int DownCornersWidth
        {
            get
            {
                return this.GetElementsWidth(HukidasiPartExtension.DownCornersParts);
            }
        }
        #region Override
        public override int GetHashCode()
        {
            int n = 0;
            this.Items.ForEach((v) => n ^= v.GetHashCode());
            return n;
        }
        public override bool Equals(object obj)
        {
            if (obj is WakuElementCollection)
            {
                var p = (WakuElementCollection)obj;
                return this.Items.Zip(p.Items).All((v) => v.Key == v.Value);
            }
            return false;
        }
        #endregion

        public static string ParserName = "Elements";
        public XElement CreateSerializer()
        {
            var xe = new XElement(WakuElementCollection.ParserName);
            if (this._Items != null)
            {
                xe.Add(new XAttribute("Offset", string.Join(",", this.Offset)));

                var wakus = new XElement("Wakus");
                this.Wakus.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => wakus.Add(v));
                xe.Add(wakus);
            }
            return xe;
        }
        
    }
}
