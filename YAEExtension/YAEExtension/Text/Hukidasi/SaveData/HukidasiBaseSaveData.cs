﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SuouBase.Interface;
using System.Xml.Serialization;
using System.ComponentModel;
using BSystem.Collections;
using BSystem;
using SuouBase.Drawing;
using SuouBase.Text.Hukidasi.WakuLogic;
namespace SuouBase.Text
{
    public interface IHukidasi
    {
        SuouText CreateWakuText(SuouText text);

        IHukidasi Clone();
        bool IsTrimSpace { get; }
        SpaceLineSet MergeAlgorithm { get; }
        XElement CreateSerializer();

        HukidasiType Type { get; }
        
    }
    [TypeConverter(typeof(PropertyDisplayConverter))]
    [Serializable]
    public class HukidasiBaseSaveData:IHukidasi
    {
        #region Property
        public WakuElementCollection Elements = new WakuElementCollection();
        private WakuOption _Options = null;
        public WakuOption Options
        {
            get {
                if (this._Options == null) this._Options = new WakuOption();
                return this._Options;
            }
            set
            {
                this._Options = value;
            }
        }
        private WakuPadding _Padding = new WakuPadding();
        public WakuPadding Padding
        {
            get { return this._Padding; }
            set { this._Padding = value; }
        }
        public WakuArrangement Arrangement = new WakuArrangement();
        #endregion
        //public SuouText[] _WakuItems { get { return this.Elements.Wakus; } }

        public HukidasiType Type { get { return this.Options.Type; } }
        public SpaceLineSet MergeAlgorithm { get { return new SpaceLineSet(this.Options.MergeAlgorithm); } }
        public bool IsTrimSpace { get { return this.Options.IsTrimSpace; } }

        public HukidasiBaseSaveData()
        {
            this.Options.MergeAlgorithm = this.Options.MergeAlgorithm ?? SpaceLineSet.HukidasiDefaultSet;
        }
        public HukidasiBaseSaveData(HukidasiBaseSaveData item)
            : this(item, true)
        {
        }
        public HukidasiBaseSaveData(HukidasiBaseSaveData item, bool isCopyPadding)
        {
            
            //this._WakuItems = item.WakuItems;
            this.Elements = item.Elements;
            //this.Elements.Offset = item.Elements.Offset;
            this.Arrangement = item.Arrangement;
            this.Options = new WakuOption(item.Options);
            if (isCopyPadding)
            {
                this.Padding = new WakuPadding(item.Padding);
            }
        }
        public SuouText[] WakuItems
        {
            get { return this.Elements.Wakus; }
            
        }
        public int TopPeriodicWidth
        {
            get
            {
                if (this.Arrangement.UpPathType == PathType.Periodic)
                {
                    return this.Elements.GetElementsWidth(this.Arrangement._UpPathPeriodic.TranslatePeriodic(HukidasiPartExtension.UpperPathParts));
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
        public int BottomPeriodicWidth
        {
            get
            {
                if (this.Arrangement.DownPathType == PathType.Periodic)
                {
                    return this.Elements.GetElementsWidth(this.Arrangement._DownPathPeriodic.TranslatePeriodic(HukidasiPartExtension.DownPathParts));
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
        public int TopFixingWidth
        {
            get
            {
                var w = this.Elements.GetElementsWidth(this.Arrangement._UpPathPeriodic.TranslateFixing(HukidasiPartExtension.UpperPathParts));
                return this.Elements.UpCornersWidth + w;
            }
        }
        public int BottomFixingWidth
        {
            get
            {
                var w = this.Elements.GetElementsWidth(this.Arrangement._DownPathPeriodic.TranslateFixing(HukidasiPartExtension.DownPathParts));
                return this.Elements.DownCornersWidth + w;
            }
        }
        public int[] TopPeriodicValues
        {
            get
            {
                if (this.Arrangement.UpPathType == PathType.Periodic)
                {
                    return this.Arrangement._UpPathPeriodic.TranslatePeriodic(HukidasiPartExtension.UpperPathParts).Select((v)=>this.Elements.GetElement(v).MaxWidth).ToArray();
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
        public int[] BottomPeriodicValues
        {
            get
            {
                if (this.Arrangement.DownPathType == PathType.Periodic)
                {
                    return this.Arrangement._DownPathPeriodic.TranslatePeriodic(HukidasiPartExtension.DownPathParts).Select((v) => this.Elements.GetElement(v).MaxWidth).ToArray();
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }

        public override int GetHashCode()
        {
            return this.Elements.GetHashCode() ^ this.Padding.GetHashCode() ^ this.Options.GetHashCode() ^ this.Arrangement.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            if (obj is HukidasiBaseSaveData)
            {
                var p = (HukidasiBaseSaveData)obj;
                return this.Elements == p.Elements && this.Options == p.Options && this.Arrangement == p.Arrangement && this.Padding.Equals(p.Padding);
            }
            return false;
        }

        public virtual SuouText CreateWakuText(SuouText text)
        {
            throw new Exception();
        }




        #region XML
        public static string ParseName { get { return "HukidasiSaveData"; } }

        public XAttribute OffsetAttribute
        {
            get
            {
                var value = string.Join(",", this.Elements.Offset);
                return new XAttribute("Offset", value);
            }
        }
        private static int Version = 4;
        public XElement CreateSerializer()
        {
            var xe = new XElement(ParseName);
            xe.Add(new XAttribute("Version", Version));
            xe.Add(this.Elements.CreateSerializer());
            xe.Add(this.Options.CreateSerializer());
            xe.Add(this.Arrangement.CreateSerializer());
            xe.Add(this.Padding.CreateSerializer());
            return xe;
        }
        public static XElement CreateXElement(IList<HukidasiSaveData> items)
        {
            var xe = new XElement("HukidasiSaveDataItems", "");
            xe.Add(new XAttribute("Version", Version));
            items.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => xe.Add(v));
            return xe;
        }
        #endregion

        public IHukidasi Clone()
        {
            return new HukidasiSaveData(this);
        }
    }

    public enum PathType
    {
        Periodic,NonPeriodic
    }
    public enum TextAlign
    {
        Left,Center,Right,Tate
    }
}
