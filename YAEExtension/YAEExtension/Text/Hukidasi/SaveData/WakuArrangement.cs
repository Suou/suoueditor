﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text.Hukidasi.WakuLogic;
using BSystem.Collections;
using BSystem;
using SuouBase.Interface;
using System.Xml.Linq;

namespace SuouBase.Text
{
    [Serializable]
    public class WakuArrangement : IParse
    {
        #region Property
        public PathType UpPathType = PathType.Periodic;
        public PathType LeftPathType = PathType.Periodic;
        public PathType RightPathType = PathType.Periodic;
        public PathType DownPathType = PathType.Periodic;
        public PeriodicRepresent _UpPathPeriodic = new PeriodicRepresent();
        public PeriodicRepresent _LeftPathPeriodic = new PeriodicRepresent();
        public PeriodicRepresent _RightPathPeriodic = new PeriodicRepresent();
        public PeriodicRepresent _DownPathPeriodic = new PeriodicRepresent();
        public TextAlign Align = TextAlign.Left;
        #endregion
        #region Property2
        private PathType[] Types
        {
            get
            {
                return new PathType[] { this.UpPathType, this.LeftPathType, this.RightPathType, this.DownPathType };
            }
        }
        private Inter[] PeriodicRepresents
        {
            get
            {
                return new Inter[] { this.UpPathInter, this.LeftPathInter, this.RightPathInter, this.DownPathInter };
            }
        }

        public Inter UpPathInter
        {
            get
            {
                if (this.UpPathType == PathType.Periodic)
                {
                    return this._UpPathPeriodic.CreaterInstance();
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
        public Inter LeftPathInter
        {
            get
            {
                if (this.LeftPathType == PathType.Periodic)
                {
                    return this._LeftPathPeriodic.CreaterInstance();
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
        public Inter RightPathInter
        {
            get
            {
                if (this.RightPathType == PathType.Periodic)
                {
                    return this._RightPathPeriodic.CreaterInstance();
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
        public Inter DownPathInter
        {
            get
            {
                if (this.DownPathType == PathType.Periodic)
                {
                    return this._DownPathPeriodic.CreaterInstance();
                }
                else
                {
                    throw new NotSupportedException();
                }
            }
        }
        #endregion
        #region Override
        public override int GetHashCode()
        {
            var n = 0;
            this.Types.ForEach((v) => n = n ^ v.GetHashCode());
            this.PeriodicRepresents.ForEach((v) => n = n ^ v.GetHashCode());
            n ^= this.Align.GetHashCode();
            return n;
        }
        public override bool Equals(object obj)
        {
            if (obj is WakuArrangement)
            {
                var item = (WakuArrangement)obj;
                var p = this.Types.Zip(item.Types).All((v) => v.Key == v.Value);
                var g = this.PeriodicRepresents.Zip(item.PeriodicRepresents).All((v) => v.Key == v.Value);
                var ty = this.Align == item.Align;
                return p && g && ty;
            }
            return false;
        }
        #endregion
        public static string ParserName = "Arrangement";
        public XElement CreateSerializer()
        {
            var p = new XElement(ParserName);
            p.Add(new XAttribute("UpPathType", this.UpPathType));
            p.Add(new XAttribute("LeftPathType", this.LeftPathType));
            p.Add(new XAttribute("RightPathType", this.RightPathType));
            p.Add(new XAttribute("BottomPathType", this.RightPathType));

            p.Add(new XAttribute("UpPathPeriodic", this._UpPathPeriodic.SerializeString));
            p.Add(new XAttribute("LeftPathPeriodic", this._LeftPathPeriodic.SerializeString));
            p.Add(new XAttribute("RightPathPeriodic", this._RightPathPeriodic.SerializeString));
            p.Add(new XAttribute("BottomPathPeriodic", this._DownPathPeriodic.SerializeString));

            p.Add(new XAttribute("UpPathFix", this._UpPathPeriodic.SerializeFixString));
            p.Add(new XAttribute("LeftPathFix", this._LeftPathPeriodic.SerializeFixString));
            p.Add(new XAttribute("RightPathFix", this._RightPathPeriodic.SerializeFixString));
            p.Add(new XAttribute("BottomPathFix", this._DownPathPeriodic.SerializeFixString));

            p.Add(new XAttribute("Align", this.Align));
            return p;
        }
        public static WakuArrangement ParseVer2(XElement xe)
        {

            var upPathType = xe.EnumAttributeParse<PathType>("UpPathType", PathType.Periodic);
            var leftPathType = xe.EnumAttributeParse<PathType>("LeftPathType", PathType.Periodic);
            var rightPathType = xe.EnumAttributeParse<PathType>("RightPathType", PathType.Periodic);
            var bottomPathType = xe.EnumAttributeParse<PathType>("BottomPathType", PathType.Periodic);
            var upString = xe.StringAttributeParse("UpPathPeriodic", null);
            var leftString = xe.StringAttributeParse("LeftPathPeriodic", null);
            var rightString = xe.StringAttributeParse("RightPathPeriodic", null);
            var bottomString = xe.StringAttributeParse("BottomPathPeriodic", null);


            var upFixString = xe.StringAttributeParse("UpPathFix", null);
            var leftFixString = xe.StringAttributeParse("LeftPathFix", null);
            var rightFixString = xe.StringAttributeParse("RightPathFix", null);
            var bottomFixString = xe.StringAttributeParse("BottomPathFix", null);

            var up = PeriodicRepresent.Create(upString,upFixString);
            var left = PeriodicRepresent.Create(leftString,leftFixString);
            var right = PeriodicRepresent.Create(rightString,rightFixString);
            var bottom = PeriodicRepresent.Create(bottomString,bottomFixString);

            var align = xe.EnumAttributeParse<TextAlign>("Align", TextAlign.Left);

            var f = new WakuArrangement()
            {
                UpPathType = upPathType,
                LeftPathType = leftPathType,
                RightPathType = rightPathType,
                DownPathType = bottomPathType,
                _UpPathPeriodic = up,
                _LeftPathPeriodic = left,
                _RightPathPeriodic = right,
                _DownPathPeriodic = bottom,
                Align = align
            };
            return f;
        }
        public static WakuArrangement ParseVer1(XElement xe)
        {
            var upPathType = xe.EnumAttributeParse<PathType>("UpPathType", PathType.Periodic);
            var leftPathType = xe.EnumAttributeParse<PathType>("LeftPathType", PathType.Periodic);
            var rightPathType = xe.EnumAttributeParse<PathType>("RightPathType", PathType.Periodic);
            var bottomPathType = xe.EnumAttributeParse<PathType>("BottomPathType", PathType.Periodic);
            var upString = xe.StringAttributeParse("UpPathPeriodic", "").Pipe(HukidasiXML.IntArrayParse).Select((v)=>v+1).ToArray();
            var leftString = xe.StringAttributeParse("LeftPathPeriodic", "").Pipe(HukidasiXML.IntArrayParse).Select((v) => v + 1).ToArray();
            var rightString = xe.StringAttributeParse("RightPathPeriodic", "").Pipe(HukidasiXML.IntArrayParse).Select((v) => v + 1).ToArray();
            var bottomString = xe.StringAttributeParse("BottomPathPeriodic", "").Pipe(HukidasiXML.IntArrayParse).Select((v) => v + 1).ToArray();

            var up = new PeriodicRepresent(upString,new int[]{});
            var left = new PeriodicRepresent(leftString, new int[] { });
            var right = new PeriodicRepresent(rightString, new int[] { });
            var bottom =  new PeriodicRepresent(bottomString,new int[]{});

            var align = xe.EnumAttributeParse<TextAlign>("Align", TextAlign.Left);

            var f = new WakuArrangement()
            {
                UpPathType = upPathType,
                LeftPathType = leftPathType,
                RightPathType = rightPathType,
                DownPathType = bottomPathType,
                _UpPathPeriodic = up,
                _LeftPathPeriodic = left,
                _RightPathPeriodic = right,
                _DownPathPeriodic = bottom,
                Align = align
            };
            return f;
        }
    }
}
