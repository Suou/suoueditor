﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
namespace SuouBase.Text
{
    [Serializable]
    public class LogicElement
    {
        public SuouText Text { get; private set; }
        public int Offset { get; private set; }
        public bool IsLeft { get; private set; }
        public HukidasiPart Part { get; private set; }
        /*
        public LogicElement(SuouText text, int offset, bool isleft)
        {
            this.Text = text;
            this.Offset = offset;
            this.IsLeft = isleft;
        }
        */
        public LogicElement(SuouText text, int offset, HukidasiPart part)
        {
            this.Text = text;
            this.Offset = offset;
            this.Part = part;
            if (HukidasiPartExtension.LeftParts.Any((v)=>v==part)) this.IsLeft = true;
        }
        //public SuouText TextElement { get { return this.Text; } }
        public SuouText CreateSText(AltText spacer)
        {
            if (spacer == null) throw new MyException("");
            if (this.Offset == 0) return this.Text;
            
            var space = spacer.MakeRectangleText(Text.Font, Text.Color, new Size(this.Offset, this.Text.Count));
            return this.IsLeft ? space + this.Text : this.Text + space;
        }
        public int[] DotWidthes
        {
            get
            {
                return this.Text.Lines.Select((v) => v.Width + Offset).ToArray();
            }
        }
        public int MaxWidth { get { return this.Text.Width + Offset; } }
        public int FirstLineWidth
        {
            get { return this.Text.Lines.First().Width + Offset; }
        }
        public override int GetHashCode()
        {
            return this.Text.GetHashCode() ^ this.Offset;
        }
        public override bool Equals(object obj)
        {
            if (obj is LogicElement)
            {
                var p = (LogicElement)obj;
                return this.Text == p.Text && this.Offset == p.Offset;
            }
            return false;
        
        }
    }
}
