﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SuouBase.Interface;
using System.Xml.Serialization;
using System.ComponentModel;
using BSystem.Collections;
using SuouBase.Drawing;
using SuouBase.Text.Hukidasi.WakuLogic;
using SuouBase.Text.Hukidasi;
namespace SuouBase.Text
{
    [ReadOnly(true)]
    [Serializable]
    public class HukidasiSaveData:HukidasiBaseSaveData,IParse,IXmlSerializable
    {
        #region BaseProperty

        [System.Xml.Serialization.XmlIgnore]
        private IHukidasiLogic _Logic = null;
        public IHukidasiLogic Logic
        {
            get
            {
                this._Logic = this._Logic ?? HukidasiExtension.Create(this);
                return this._Logic;
            }
        }

        #endregion
        #region Property

        public SuouText LeftUp
        {
            get { return this.WakuItems[0]; }
            set { this.WakuItems[0] = value; }
        }
        public SuouText UpCenter
        {
            get { return this.WakuItems[1]; }
            set { this.WakuItems[1] = value; }
        }
        public SuouText RightUp
        {
            get { return this.WakuItems[2]; }
            set { this.WakuItems[2] = value; }
        }
        public SuouText LeftCenter
        {
            get { return this.WakuItems[3]; }
            set { this.WakuItems[3] = value; }
        }
        public SuouText RightCenter
        {
            get { return this.WakuItems[4]; }
            set { this.WakuItems[4] = value; }
        }
        public SuouText LeftDown
        {
            get { return this.WakuItems[5]; }
            set { this.WakuItems[5] = value; }
        }
        public SuouText DownCenter
        {
            get { return this.WakuItems[6]; }
            set { this.WakuItems[6] = value; }
        }
        public SuouText RightDown
        {
            get { return this.WakuItems[7]; }
            set { this.WakuItems[7] = value; }
        }
        public SuouText UpCenterLeft
        {
            get { return this.WakuItems[8]; }
            set { this.WakuItems[8] = value; }
        }
        public SuouText UpCenterRight
        {
            get { return this.WakuItems[9]; }
            set { this.WakuItems[9] = value; }
        }
        public SuouText LeftCenterUp
        {
            get { return this.WakuItems[10]; }
            set { this.WakuItems[10] = value; }
        }
        public SuouText RightCenterUp
        {
            get { return this.WakuItems[11]; }
            set { this.WakuItems[11] = value; }
        }
        public SuouText LeftCenterDown
        {
            get { return this.WakuItems[12]; }
            set { this.WakuItems[12] = value; }
        }
        public SuouText RightCenterDown
        {
            get { return this.WakuItems[13]; }
            set { this.WakuItems[13] = value; }
        }
        public SuouText DownCenterLeft
        {
            get { return this.WakuItems[14]; }
            set { this.WakuItems[14] = value; }
        }
        
        public SuouText DownCenterRight
        {
            get { return this.WakuItems[15]; }
            set { this.WakuItems[15] = value; }
        }
        

        public SuouText this[HukidasiPart part]
        {
            get
            {
                return this.GetPart(part);
            }
            set
            {
                this.WakuItems[HukidasiSaveData.GetIndex(part)] = value;
            }
        }
        public SuouText[] EightItems
        {
            get
            {
                var f = this.WakuItems.Take(8).ToArray();
                return f;
            }
        }
        public SuouText[] SixteenItems
        {
            get
            {
                var f = WakuItems.Take(16).ToArray();
                if (f.Contains(null)) throw new MyException(System.Environment.StackTrace);
                return f;
            }
        }
        public SuouText[] LeftBar
        {
            get
            {
                if (this.Type == HukidasiType.FiveBlock)
                {
                    return new SuouText[] { LeftCenterUp, LeftCenter, LeftCenterDown };
                }
                else
                {
                    return new SuouText[] { LeftCenter };
                }
            }
        }
        public SuouText[] RightBar
        {
            get
            {
                if (this.Type == HukidasiType.FiveBlock)
                {
                    return new SuouText[] { RightCenterUp, RightCenter, RightCenterDown };
                }
                else
                {
                    return new SuouText[] { RightCenter };
                }
            }
        }
        public SuouText[] Items
        {
            get
            {
                if (this.Type == HukidasiType.FiveBlock)
                {
                    return this.SixteenItems;
                }
                else
                {
                    return this.EightItems;
                }
            }
        }
        #endregion

        public static HukidasiPart[] Zyunban
        {
            get 
            {
                return new HukidasiPart[]{HukidasiPart.LeftUpCorner,HukidasiPart.UpCenter,HukidasiPart.RightUpCorner,HukidasiPart.LeftCenter,HukidasiPart.RightCenter,
                HukidasiPart.LeftDownCorner,HukidasiPart.DownCenter,HukidasiPart.RightDownCorner,HukidasiPart.UpCenterLeft,HukidasiPart.UpCenterRight,HukidasiPart.LeftCenterUp,
                HukidasiPart.RightCenterUp,HukidasiPart.LeftCenterDown,HukidasiPart.RightCenterDown,HukidasiPart.DownCenterLeft,HukidasiPart.DownCenterRight};
            }
        }
        
        #region Property2
        public SuouText GetPart(HukidasiPart part)
        {
            switch (part)
            {
                case HukidasiPart.LeftUpCorner: return this.LeftUp;
                case HukidasiPart.LeftCenterUp: return this.LeftCenterUp;
                case HukidasiPart.LeftCenter: return this.LeftCenter;
                case HukidasiPart.LeftCenterDown: return this.LeftCenterDown;
                case HukidasiPart.LeftDownCorner: return this.LeftDown;
                case HukidasiPart.UpCenterLeft: return this.UpCenterLeft;
                case HukidasiPart.UpCenter: return this.UpCenter;
                case HukidasiPart.UpCenterRight: return this.UpCenterRight;
                case HukidasiPart.RightUpCorner: return this.RightUp;
                case HukidasiPart.RightCenterUp: return this.RightCenterUp;
                case HukidasiPart.RightCenter: return this.RightCenter;
                case HukidasiPart.RightCenterDown: return this.RightCenterDown;
                case HukidasiPart.RightDownCorner: return this.RightDown;
                case HukidasiPart.DownCenterLeft: return this.DownCenterLeft;
                case HukidasiPart.DownCenter: return this.DownCenter;
                case HukidasiPart.DownCenterRight: return this.DownCenterRight;
            }
            return this.LeftUp;
        }


        public static int GetIndex(HukidasiPart part)
        {
            switch (part)
            {
                case HukidasiPart.LeftUpCorner: return 0;
                case HukidasiPart.LeftCenterUp: return 10;
                case HukidasiPart.LeftCenter: return 3;
                case HukidasiPart.LeftCenterDown: return 12;
                case HukidasiPart.LeftDownCorner: return 5;
                case HukidasiPart.UpCenterLeft: return 8;
                case HukidasiPart.UpCenter: return 1;
                case HukidasiPart.UpCenterRight: return 9;
                case HukidasiPart.RightUpCorner: return 2;
                case HukidasiPart.RightCenterUp: return 11;
                case HukidasiPart.RightCenter: return 4;
                case HukidasiPart.RightCenterDown: return 13;
                case HukidasiPart.RightDownCorner: return 7;
                case HukidasiPart.DownCenterLeft: return 14;
                case HukidasiPart.DownCenter: return 6;
                case HukidasiPart.DownCenterRight: return 15;
            }
            return 0;
        }
        public string GetName(HukidasiPart part)
        {
            switch (part)
            {
                case HukidasiPart.LeftUpCorner: return "(1,2)マス";
                case HukidasiPart.LeftCenterUp: return "(1,2)マス";
                case HukidasiPart.LeftCenter: return "(1,3)マス";
                case HukidasiPart.LeftCenterDown: return "(1,4)マス";
                case HukidasiPart.LeftDownCorner: return "(1,5)マス";
                case HukidasiPart.UpCenterLeft: return "(2,1)マス";
                case HukidasiPart.UpCenter: return "(3,1)マス";
                case HukidasiPart.UpCenterRight: return "(4,1)マス";
                case HukidasiPart.RightUpCorner: return "(5,1)マス";
                case HukidasiPart.RightCenterUp: return "(5,2)マス";
                case HukidasiPart.RightCenter: return "(5,3)マス";
                case HukidasiPart.RightCenterDown: return "(5,4)マス";
                case HukidasiPart.RightDownCorner: return "(5,5)マス";
                case HukidasiPart.DownCenterLeft: return "(2,5)マス";
                case HukidasiPart.DownCenter: return "(3,5)マス";
                case HukidasiPart.DownCenterRight: return "(4,5)マス";
            }
            return "(-,-)マス";
        }
        #endregion
        #region Constructor

        public HukidasiSaveData():base()
        {
        }
        public HukidasiSaveData(HukidasiBaseSaveData item):base(item)
        {
        }
        #endregion
        public void DoEqualRectangle(SuouText a, SuouText b, AltText textmaker)
        {
            textmaker.MakeRectangleText(a, b);
        }
        #region Default
        public static HukidasiSaveData FiveDefault()
        {
            var s = new string[] { "╋", "━", "╋", "┃", "┃", "╋", "━", "╋", "━", "━", "┃", "┃", "┃", "┃", "━", "━" };
            var s2 = s.Select((v) => new SuouText(v)).ToArray();
            var p = new HukidasiBaseSaveData()
            {
                //WakuItems = s2,
                Elements = new WakuElementCollection(s2,null),
                Options = WakuOption.FiveDefault()
            };
            return new HukidasiSaveData(p);
        }
        public static HukidasiSaveData NoneDefault()
        {
            var p = new HukidasiBaseSaveData()
            {
                //WakuItems = null,
                Options = new WakuOption(){Type = HukidasiType.Nothing,MergeAlgorithm = SpaceLineSet.DefaultSet}
            };
            return new HukidasiSaveData(p);
        }
        public static HukidasiSaveData NoWakuDefault()
        {
            var p = new HukidasiBaseSaveData()
            {
                Options = new WakuOption() { Type = HukidasiType.NoWaku, MergeAlgorithm = SpaceLineSet.DefaultSet }                
            };
            return new HukidasiSaveData(p);
        }
        public static HukidasiSaveData ThreeDefault()
        {
            var s = new string[] { "┏", "┃", "┗", "━", "━", "┓", "┃", "┛" };
            var s2 = s.Select((v) => new SuouText(v)).ToArray();
            var p = new HukidasiBaseSaveData()
            {
                Elements = new WakuElementCollection(s2,null),
                Options = new WakuOption()
                {
                    Type = HukidasiType.Block,
                    MergeAlgorithm = SpaceLineSet.DefaultSet,
                    IsTrimSpace = true
                }
            };
            return new HukidasiSaveData(p);
        }
        #endregion

        public HukidasiCheck CreateCheckData()
        {
            return new HukidasiCheck(this);
        }
        public bool ErrorCheck()
        {
            var f = this.CreateCheckData();
            return f.IsNoError;
        }

        public LogicElement[] CreateLogicElements()
        {
            return HukidasiSaveData.Zyunban.Select((v) =>
            {
                return this.Elements.GetElement(v);
            }).ToArray();
        }


        public HukidasiSaveData ResetPadding()
        {
            var p = new HukidasiBaseSaveData(this,false);
            return new HukidasiSaveData(p);
            
        }
        public HukidasiSaveData PaddingClearClone()
        {
            var v = new HukidasiSaveData(this);
            v.Padding = new WakuPadding();
            return v;
        }
        public override SuouText CreateWakuText(SuouText Text)
        {
            var f = HukidasiExtension.Create(this);
            if (this.MergeAlgorithm == null) throw new MyException("");
            var v = AltText.Create(this.MergeAlgorithm, Text.Font, Text.Color);
            return f.CreateWaku(Text, v);
        }
        
        #region IXMLSerialize
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            var p = this.CreateSerializer();
            p.WriteTo(writer);
        }
        public void ReadXml(System.Xml.XmlReader reader)
        {
            
            //while (reader.NodeType != System.Xml.XmlNodeType.Element)
            //    reader.Read();
            var p = XElement.Parse(reader.ReadInnerXml(), LoadOptions.None);
            var item = HukidasiParser.Parse(p);
            //var ser = new XmlSerializer(typeof(HukidasiBaseSaveData));
            //var item = (HukidasiBaseSaveData)ser.Deserialize(reader);

            base.Elements = item.Elements;
            base.Arrangement = item.Arrangement;
            base.Options = item.Options;
            base.Padding = item.Padding;

        }
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }
        #endregion
        
        public static List<HukidasiSaveData> Load(string filename)
        {
            try
            {
                var file = XElement.Load(filename);
                return HukidasiParser.ListParse(file);
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static void Save(IList<HukidasiSaveData> items, string filename)
        {
            HukidasiSaveData.CreateXElement(items).Save(filename);
        }
        public TextThumbImage ToTextThumbImage (AltText textmaker,SuouText sampleText)
        {
            if (textmaker == null) throw new MyException("");
            var Ihukidasi = HukidasiExtension.Create(this);
            var v = Ihukidasi.CreateSamplePage(textmaker,sampleText);
            var f = new TextThumbImage(v, 0);
            return f;
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public HukidasiSaveData ChengeClone(SpaceLineSet set)
        {
            var f = (HukidasiBaseSaveData)this.Clone();
            f.Options.MergeAlgorithm = set;
            return (HukidasiSaveData)f;
        }
        public HukidasiSaveData ChengeClone(MergeZureOption option)
        {
            var f = (HukidasiBaseSaveData)this.Clone();
            //f.Options.ZureOption = option;
            return (HukidasiSaveData)f;
        }
    }

    public class HukidasiList
    {
        public List<HukidasiBaseSaveData> Items { get; set; }
        public HukidasiList()
        {
        }
        public HukidasiList(IEnumerable<HukidasiSaveData> items)
        {
            this.Items = items.Select((v)=>new HukidasiBaseSaveData(v)).ToList();
        }
    }
}
