﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SuouBase.Interface;
using System.Xml.Serialization;
using System.ComponentModel;
using BSystem.Collections;
using SuouBase.Drawing;
using BSystem;
namespace SuouBase.Text
{


    public class HukidasiXML
    {
        
        public static int[] OffsetParse(XElement x, string name)
        {
            if (x == null) throw new MyException("");
            if (x.Attribute(name) != null)
            {
                var item = x.Attribute(name).Value;
                return item.Split(',').Select((v) => int.Parse(v)).ToArray();
            }
            else
            {
                return new int[10];
            }
        }
        public static int[] IntArrayParse(string name)
        {
            if (name != null)
            {
                if (name.Length == 0) return new int[] { };
                return name.Split(',').Select((v) => int.Parse(v)).ToArray();
            }
            else
            {
                return null;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="xe"></param>
        /// <returns></returns>

    }
    public class WakuElementCollectionParse
    {
        public static WakuElementCollection Parse1(XElement xe)
        {
            if (!xe.HasElements) return new WakuElementCollection();
            var offset = HukidasiXML.OffsetParse(xe, "Offset");

            var wakus = xe.Element("Wakus");
            var textwakus = wakus.Elements().Select((v) => SuouTextParser.Parse1(v)).ToArray();
            var pb = new WakuElementCollection(textwakus, offset);
            return pb;
        }
        public static WakuElementCollection ParseNew(XElement xe)
        {
            if (!xe.HasElements) return new WakuElementCollection();
            var offset = HukidasiXML.OffsetParse(xe, "Offset");

            var wakus = xe.Element("Wakus");
            var textwakus = wakus.Elements().Select((v) => SuouTextParser.Parse2(v)).ToArray();
            var pb = new WakuElementCollection(textwakus, offset);
            return pb;
        }
    }
}
