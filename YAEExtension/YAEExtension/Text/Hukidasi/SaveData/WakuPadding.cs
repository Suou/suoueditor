﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text.Hukidasi.WakuLogic;
using BSystem.Collections;
using BSystem;
using SuouBase.Interface;
using System.Xml.Linq;
namespace SuouBase.Text
{

    [Serializable]
    public class WakuPadding
    {
        public int _TopSpace;
        public int _LeftSpace;
        public int _RightSpace;
        public int _BottomSpace;
        public int _CenterSpace;


        public int TopSpace
        {
            get { return this._TopSpace; }
            set { if (value >= 0) this._TopSpace = value; }
        }
        public int LeftSpace
        {
            get { return this._LeftSpace; }
            set { if (value >= 0) this._LeftSpace = value; }
        }
        public int RightSpace
        {
            get { return this._RightSpace; }
            set { if (value >= 0) this._RightSpace = value; }
        }
        public int BottomSpace
        {
            get { return this._BottomSpace; }
            set { if (value >= 0) this._BottomSpace = value; }
        }
        public int CenterSpace
        {
            get { return this._CenterSpace; }
            set { if (value >= 0) this._CenterSpace = value; }
        }
        public WakuPadding()
        {
        }
        public WakuPadding(WakuPadding item)
        {
            this._TopSpace = item._TopSpace;
            this._LeftSpace = item._LeftSpace;
            this._RightSpace = item._RightSpace;
            this._BottomSpace = item._BottomSpace;
            this._CenterSpace = item._CenterSpace;
        }
        private int[] Values
        {
            get
            {
                return new int[] { this.TopSpace, this.LeftSpace, this.RightSpace, this.BottomSpace, this.CenterSpace };
            }
        }
        public override int GetHashCode()
        {
            int n = 0;
            this.Values.ForEach((v) => n ^= v);
            return n;
        }
        public override bool Equals(object obj)
        {
            if (obj is WakuPadding)
            {
                var p = (WakuPadding)obj;
                return this.Values.Zip(p.Values).All((v) => v.Key == v.Value);
            }
            return false;
        }

        public static string ParserName = "Padding";
        public XElement CreateSerializer()
        {
            var f = new XElement(ParserName);
            f.Add(new XAttribute("Top", this.TopSpace));
            f.Add(new XAttribute("Left", this.LeftSpace));
            f.Add(new XAttribute("Right", this.RightSpace));
            f.Add(new XAttribute("Bottom", this.BottomSpace));
            f.Add(new XAttribute("Center", this.CenterSpace));
            //f.Add(new XAttribute("MergeAlgorithm", this.UpCenterBottomPreZure));
            return f;
        }
        public static WakuPadding Parse(XElement xe)
        {
            var top = (short)xe.IntAttributeParse("Top", 0);
            var left = (short)xe.IntAttributeParse("Left", 0);
            var right = (short)xe.IntAttributeParse("Right", 0);
            var bottom = (short)xe.IntAttributeParse("Bottom", 0);
            var center = (short)xe.IntAttributeParse("Center", 0);

            return new WakuPadding() { TopSpace = top, LeftSpace = left, RightSpace = right, BottomSpace = bottom, CenterSpace = center };
        }
    }
}
