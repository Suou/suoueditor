﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text.Hukidasi.WakuLogic;
using BSystem.Collections;
using BSystem;
using SuouBase.Interface;
using System.Xml.Linq;
namespace SuouBase.Text
{
    [Serializable]
    public class WakuOption : IParse
    {
        #region Property
        public HukidasiType Type { get; set; }
        public int UpCenterBottomPreZure { get; set; }
        public int CenterPreZure { get; set; }
        public bool IsTrimSpace { get; set; } = true;
        //public SuouText SampleText { get; set; }
        public string Name { get; set; }
        private SpaceLineSet _MergeAlgorithm = null;
        public SpaceLineSet MergeAlgorithm
        {
            get
            {
                if (this._MergeAlgorithm == null) this._MergeAlgorithm = SpaceLineSet.HukidasiDefaultSet;
                return this._MergeAlgorithm;
            }
            set
            {
                this._MergeAlgorithm = value;
            }
        }
        private object[] ObjectItems
        {
            get
            {
                return new object[] { this.Type, this.UpCenterBottomPreZure, this.CenterPreZure, this.IsTrimSpace, this.MergeAlgorithm, this.Name };
            }
        }
        #endregion
        #region Constructor
        public WakuOption()
        {
        }
        public WakuOption(WakuOption item)
        {

            this.Type = item.Type;
            //this.ZureOption = item.ZureOption;
            //this.SampleText = item.SampleText;
            this.MergeAlgorithm = item.MergeAlgorithm;
            //this.PermitZureLength = item.PermitZureLength;
            this.UpCenterBottomPreZure = item.UpCenterBottomPreZure;
            this.CenterPreZure = item.CenterPreZure;
            this.IsTrimSpace = item.IsTrimSpace;
            this.Name = item.Name;
        }
        #endregion
        public static WakuOption FiveDefault()
        {
            var v = new WakuOption()
            {
                Type = HukidasiType.FiveBlock,
                //ZureOption = MergeZureOption.LineRestriction,
                //PermitZureLength = 0,
                MergeAlgorithm = SpaceLineSet.DefaultSet,
                IsTrimSpace = true
            };
            return v;
        }
        #region Override
        public override int GetHashCode()
        {
            int n = 0;
            this.ObjectItems.Where((v) => v != null).ForEach((v) => n ^= v.GetHashCode());
            return n;
        }
        public override bool Equals(object obj)
        {
            if (obj is WakuOption)
            {
                WakuOption p = (WakuOption)obj;
                return this.ObjectItems.Where((v) => v != null).Zip(p.ObjectItems).All((v) => v.Key == v.Value);
            }
            return false;
        }
        #endregion
        public static string ParserName = "Option";
        public XElement CreateSerializer()
        {
            var f = new XElement(ParserName);
            if (this.Name != null) f.Add(new XAttribute("Name", this.Name));
            //f.Add(new XAttribute("ZureOption", this.ZureOption));
            //f.Add(new XAttribute("PermitZureLength", this.PermitZureLength));
            f.Add(new XAttribute("UpCenterBottomPreZure", this.UpCenterBottomPreZure));
            f.Add(new XAttribute("CenterPreZure", this.CenterPreZure));
            f.Add(new XAttribute("IsTrimSpace", this.IsTrimSpace));
            f.Add(new XAttribute("Type", this.Type));
            f.Add(this.MergeAlgorithm.CreateSerializer());
            //f.Add(new XAttribute("MergeAlgorithm", this.UpCenterBottomPreZure));
            return f;
        }
        public static WakuOption Parse(XElement xe)
        {
            var name = xe.StringAttributeParse("Name", null);
            var zureoption = xe.EnumAttributeParse<MergeZureOption>("ZureOption", MergeZureOption.Free);
            var permitZureLength = xe.IntAttributeParse("PermitZureLength", 0);
            var upCenterBottomPreZure = xe.IntAttributeParse("UpCenterBottomPreZure", 0);
            var centerPreZure = xe.IntAttributeParse("CenterPreZure", 0);
            var isTrimSpace = xe.BoolAttributeParse("IsTrimSpace", true);
            var type = xe.EnumAttributeParse<HukidasiType>("Type", HukidasiType.FiveBlock);
            var mergeAlgorithm = SpaceLineSet.Parse(xe.Element(SpaceLineSet.ParserName));
            var f = new WakuOption()
            {
                Name = name,
                //ZureOption = zureoption,
                //PermitZureLength = permitZureLength,
                UpCenterBottomPreZure = upCenterBottomPreZure,
                CenterPreZure = centerPreZure,
                IsTrimSpace = isTrimSpace,
                MergeAlgorithm = mergeAlgorithm,
                Type = type
            };
            return f;
        }
    }
}
