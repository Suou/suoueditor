﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using BSystem.Drawing;

namespace SuouBase.Drawing
{

    public static class SuouTextFontExtension
    {
        public static LineDotIndexes WordDotIndexes(this Font f, string word)
        {
            var dn = f.DotIndexes(word);
            var width = word.Length == 0 ? 0 : (int)dn.Last();
            var height = f.Height;
            var ldi = new LineDotIndexes(new Size(width, height), dn);
            return ldi;
        }
    }
    public class LineDotIndexes
    {
        public Size Size { get; private set; }
        public List<int> DotIndexes;
        public Dictionary<int, int> DotIndexDic;
        public int Height { get { return this.Size.Height; } }
        public int Width { get { return this.Size.Width; } }
        public bool IsEmpty
        {
            get
            {
                return this.DotIndexes.Count() == 0;
            }
        }
        public bool ContainDotIndex(int dotindex)
        {
            return DotIndexDic.ContainsKey((char)dotindex);
        }
        private int FindDicIndex(int dotindex)
        {
            if (this.ContainDotIndex(dotindex))
            {
                return this.DotIndexDic[dotindex];
            }
            else
            {
                return -1;
            }
        }
        public int FindIndex(int DotIndex)
        {
            if (DotIndex == 0) return 0;
            var n = this.FindDicIndex(DotIndex);
            if (n == -1)
            {
                return -1;
            }
            else
            {
                return n + 1;
            }
        }
        public int FindNearIndex(int dotindex,BinaryOption option)
        {
            var v = this.FindIndex(dotindex);
            if (v != -1) return v;
            if (this.ContainDotIndex(dotindex))
            {
                return this.DotIndexDic[dotindex];
            }
            else
            {
                return this.DotIndexes.BinaryNearSearch(dotindex, option);
            }
        }
        public LineDotIndexes(Size size, int[] dotIndexes)
        {
            this.Size = size;
            this.DotIndexes = new List<int>(dotIndexes);
            this.MakeDictionary();
        }
        public LineDotIndexes(Size size, List<int> dotIndexes,Dictionary<int,int> dotDic)
        {
            this.Size = size;
            this.DotIndexes = new List<int>(dotIndexes);
            this.DotIndexDic = new Dictionary<int,int>(dotDic);
        }
        public void MakeDictionary()
        {
            this.DotIndexDic = new Dictionary<int, int>();
            for (int i = 0; i < this.DotIndexes.Count(); i++)
            {
                DotIndexDic[DotIndexes[i]] = i;
            }
        }
        public static LineDotIndexes operator +(LineDotIndexes a, LineDotIndexes b)
        {
            if (a == null || b == null) throw new MyException("");
            var width = a.Size.Width + b.Size.Width;
            var height = a.Height > b.Height ? a.Height : b.Height;
            var cd = new List<int>(a.DotIndexes);
            var dic = new Dictionary<int, int>(a.DotIndexDic);
            var Geta = a.IsEmpty ? 0 : a.DotIndexes.Last();
            var len = cd.Count();
            var f = b.DotIndexes.Select((t) => (int)(t + Geta)).ToList();
            cd.AddRange(f);

            for (int i = 0; i < f.Count(); i++)
            {
                dic[f[i]] = i + len;
            }

            var r = new LineDotIndexes(new Size(width, height), cd,dic);
            return r;
        }
        public static LineDotIndexes Empty(Font f)
        {
            return new LineDotIndexes(new Size(0,f.Height), new int[0]);
        }
        public LineDotIndexes Copy()
        {
            return new LineDotIndexes(this.Size, this.DotIndexes,this.DotIndexDic);
        }
        public void WriteLineItem()
        {
            //this.DotIndexDic.Select((v)=> new KeyValuePair<int,int>(v.Key,v.Value)).WriteLine();
            //this.DotIndexes.Select((v) => (int)v).WriteLine();
            
        }
    }
}
