﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using BSystem.Collections;
using BSystem;
namespace SuouBase.Text
{
    public class AltLine
    {
        public ISpaceMaker SpaceMaker;

        public AltLine(ISpaceMaker am)
        {
            SpaceMaker = am;
        }
        #region WidthRightSpace
        public bool CanMakeLineWidthRightSpace(SuouLine text, int Width)
        {
            var p = text.SpaceSplit(SpaceMaker);
            var TextLine = p.Left + p.Center;
            var SpaceWidth = Width - TextLine.Width;
            var v = SpaceMaker.CanCreateSpace(SpaceWidth);
            return v;
        }
        public SuouLine MakeLineWidthRightSpace(SuouLine text, int Width)
        {
            if (text.Width - Width == 0) return text.Clone();
            var p = text.SpaceSplit(SpaceMaker);
            var TextLine = p.Left + p.Center;
            var SpaceWidth = Width - TextLine.Width;
            var RightSpace = SpaceMaker.CreateSpace(SpaceWidth);

            return TextLine + RightSpace;
        }
        #endregion
        #region SpaceReplaceStrictly
        public bool CanSpaceReplaceSublineStrictly(SuouLine text, int ReplaceStartPos, int ReplaceLength)
        {
            var b2 = text.ContainDotIndex(ReplaceStartPos);
            var b3 = text.ContainDotIndex(ReplaceStartPos + ReplaceLength);
            var b = SpaceMaker.CanCreateSpace(ReplaceLength);

            if (!b || !b2 || !b3) return false;
            return true;
        }
        public SuouLine SpaceReplaceSublineStrictly(SuouLine text, int ReplaceStartPos, int ReplaceWidth)
        {
            var r = SuouLine.Empty(text);
            if (!CanSpaceReplaceSublineStrictly(text, ReplaceStartPos, ReplaceWidth)) throw new Exception();

            var ReplaceStartIndex = text.DotIndexList.FindIndex(ReplaceStartPos);
            if (ReplaceStartIndex == -1) throw new Exception();

            var ReplaceEndLength = text.DotIndexList.FindIndex(ReplaceStartPos + ReplaceWidth);
            var ReplaceLength = ReplaceEndLength - ReplaceStartIndex;
            var LeftCenterRight = text.TripleSplit(ReplaceStartIndex, ReplaceLength);
            var Space = SpaceMaker.CreateSpace(ReplaceWidth);

            r = LeftCenterRight.Left + Space + LeftCenterRight.Right;
            return r;
        }
        #endregion
        #region Increment
        public int NearestCanIncrementSpaceLine(SuouLine text, int index, int incrementlength)
        {
            var width = text.MeasureWidth(index);
            var f = text.CenterSpaceDotSplit(this.SpaceMaker,width);
            if (f.Center.Width + incrementlength < 0) return 0;
            if (incrementlength >= 0)
            {
                return incrementlength.FindIndexWidthCountUp((i) => this.CanIncrementSpaceLine(text, width, i));
            }
            else
            {
                return incrementlength.FindIndexWidthCountDown((i) => this.CanIncrementSpaceLine(text, width, i));
            }
        }
        public bool CanIncrementSpaceLine(SuouLine text, int x, int incrementlength)
        {
            if (!text.IsInnerDotRange(x)) return false;
            var splitlines = text.CenterSpaceDotSplit(this.SpaceMaker, x);
            var spacelength = splitlines.Center.Width + incrementlength;
            return this.SpaceMaker.CanCreateSpace(spacelength);
            
        }
        public ThreeSplitLine IncrementSpaceLine(SuouLine text, int index, int incrementlength)
        {
            var width = text.MeasureWidth(index);
            if (!text.IsInnerDotRange(width)) throw new MyException("");
            var ThreeSplitLine = text.CenterSpaceDotSplit(this.SpaceMaker, width);
            var spacelength = ThreeSplitLine.Center.Width + incrementlength;
            if (!this.SpaceMaker.CanCreateSpace(spacelength)) throw new MyException("");
            var Result = new ThreeSplitLine(ThreeSplitLine.Left, this.SpaceMaker.CreateSpace(spacelength), ThreeSplitLine.Right);
            return Result;
        }
        #endregion
        #region Strictly
        public bool CanMergeStringStrictly(SuouLine DownString, SuouLine UpString, int StartDotX)
        {
            var TrimedDownString = DownString.SpaceSplit(this.SpaceMaker);
            if (TrimedDownString.Right.Width != 0) return CanMergeStringStrictly(TrimedDownString.Left + TrimedDownString.Center, UpString, StartDotX);
            

            var ThreeSpaceTrimedUpString = UpString.SpaceSplit(this.SpaceMaker);
            var NewStartDotX = ThreeSpaceTrimedUpString.Left.Width + StartDotX;
            var Len = ThreeSpaceTrimedUpString.Center.Width;

            if (DownString.Width < NewStartDotX)
            {
                var SpaceWitdh = NewStartDotX - DownString.Width;
                var CanCreateSpace = this.SpaceMaker.CanCreateSpace(SpaceWitdh);
                return CanCreateSpace;
            }
            else if (DownString.Width < NewStartDotX + Len)
            {
                var CanSplit = DownString.CanDotSplit(NewStartDotX);
                return CanSplit;
            }
            else
            {
                var CanSplit = DownString.CanTripleDotSplit(NewStartDotX, Len);
                return CanSplit;
            }

        }
        public SuouLine MergeStringStrictly(SuouLine DownString, SuouLine UpString, int StartDotX)
        {
            var TrimedDownString = DownString.SpaceSplit(this.SpaceMaker);
            if (TrimedDownString.Right.Width != 0) return MergeStringStrictly(TrimedDownString.Left + TrimedDownString.Center, UpString, StartDotX);


            var ThreeSpaceTrimedUpString = UpString.SpaceSplit(this.SpaceMaker);

            //NewStartDotXは空白削除を考慮しただけなので合成位置はずれていない
            var NewStartDotX = ThreeSpaceTrimedUpString.Left.Width + StartDotX;
            var Len = ThreeSpaceTrimedUpString.Center.Width;

            if (DownString.Width < NewStartDotX)
            {
                var SpaceWitdh = NewStartDotX - DownString.Width;
                var LeftDownString = DownString + this.SpaceMaker.CreateSpace(SpaceWitdh);
                return LeftDownString + ThreeSpaceTrimedUpString.Center;
            }
            else if (DownString.Width < NewStartDotX + Len)
            {
                var LeftDownString = DownString.DotSplit(NewStartDotX).Left;
                return LeftDownString + ThreeSpaceTrimedUpString.Center;
            }
            else
            {
                var LeftCenterRightDownString = DownString.TripleDotSplit(NewStartDotX, Len);
                return LeftCenterRightDownString.Left + ThreeSpaceTrimedUpString.Center + LeftCenterRightDownString.Right;
            }


        }
        #endregion
        #region MergeCarelessly
        public SuouLine MergeStringCarelessly(SuouLine DownString, SuouLine UpString, int StartDotX)
        {
            if (StartDotX < 0)
            {
                if (UpString.Length == 0)
                {
                    return MergeStringCarelessly(DownString, UpString, 0);
                }
                else
                {
                    //多少パフォーマンス悪い
                    var NewUpString = UpString.TrimLeft();
                    var TrimWidth = UpString.Width - NewUpString.Width;
                    return MergeStringCarelessly(DownString, NewUpString, StartDotX + TrimWidth);
                }
            }
            else
            {
                var ReplaceWidth = UpString.Width;
                var DownStringLeftCenterRight = DownString.TripleSplitIncompletetly(this.SpaceMaker, StartDotX, ReplaceWidth, SplitWordOption.NoBangWord, SplitWidthOption.Relax);
                ThreeSplitLine line = new ThreeSplitLine(DownStringLeftCenterRight.Left, UpString, DownStringLeftCenterRight.Right);
                return line.SpaceJoin(this.SpaceMaker);
            }
        }
        #endregion
        #region SmallBang
        public SuouLine SmallBangMergeString(SuouLine DownString, SuouLine UpString, int StartDotX)
        {
            var width = UpString.Width;
            if (DownString.Width < StartDotX)
            {
                var n = StartDotX - DownString.Width;
                return DownString + this.SpaceMaker.CreateSpace(n) + UpString;
            }
            else
            {
                //var w1 = DownString.Width;
                var triple = DownString.TripleDotSplitInfo(this.SpaceMaker, StartDotX, StartDotX + width);
                //var w2 = triple.Width;
                //if (w1 != w2) throw new Exception();
                triple.CenterInfo = UpString.SpaceSplitInfo(this.SpaceMaker);
                return triple.StrictSpaceJoin(this.SpaceMaker);
                /*
                var triple = DownString.TripleDotSplit(this.SpaceMaker, StartDotX, StartDotX + width, true);
                triple.Center = UpString;
                return triple.StrictSpaceJoin(this.SpaceMaker);
                */
            }
        }
        public bool CanSmallBangMergeString(SuouLine DownString, SuouLine UpString, int StartDotX)
        {
            var width = UpString.Width;
            if (DownString.Width < StartDotX)
            {
                var n = StartDotX - DownString.Width;
                return this.SpaceMaker.CanCreateSpace(n);
            }
            else
            {
                var triple = DownString.TripleDotSplitInfo(this.SpaceMaker, StartDotX, StartDotX + width);
                triple.CenterInfo = UpString.SpaceSplitInfo(this.SpaceMaker);
                return triple.CanStrictSpaceJoin(this.SpaceMaker);
                /*
                var v = DownString.CanTripleDotSplit(this.SpaceMaker, StartDotX, StartDotX + width, true);
                if (!v) return v;

                var triple = DownString.TripleDotSplit(this.SpaceMaker, StartDotX, StartDotX + width, true);
                triple.Center = UpString;
                return triple.CanStrictSpaceJoin(this.SpaceMaker);
                */
            }
        }
        #endregion
        #region BigBang
        public SuouLine BigBangMergeString(SuouLine DownString, SuouLine UpString, int StartDotX)
        {
            var width = UpString.Width;
            if (DownString.Width < StartDotX)
            {
                var n = StartDotX - DownString.Width;
                var left = DownString.SpaceSplitInfo(this.SpaceMaker);
                left.RightWidth += n;
                var center = UpString.SpaceSplitInfo(this.SpaceMaker);
                var right = EdgeSpaceLineInfo.CreateEmpty(DownString.Font, DownString.Color);
                var triple = new ThreeVirtualSplitLine(left, center, right);
                var rst = triple.CenterCarelessBang(this.SpaceMaker);
                return rst;
            }
            else
            {
                var triple = DownString.TripleDotSplitInfo(this.SpaceMaker, StartDotX, StartDotX + width);
                triple.CenterInfo = UpString.SpaceSplitInfo(this.SpaceMaker);
                return triple.CenterCarelessBang(this.SpaceMaker);
            }
        }
        #endregion
        #region SubLine
        public void Subline(SuouLine text, int StartDotIndex, int Width,LineMerge LM,int y)
        {
            var r = text.TripleSplitIncompletetly(this.SpaceMaker, StartDotIndex, Width, SplitWordOption.NoBangWord, SplitWidthOption.Relax);
            var SubLine = r.Center;
            var Location = new EditPoint(StartDotIndex,y);
            if(SubLine.Width > 0)LM.Add(SubLine, Location,false);
        }
        public void Subline(SuouLine text, int StartDotIndex, LineMerge LM,int y)
        {
            var len = text.Width - StartDotIndex;
            this.Subline(text, StartDotIndex, len, LM,y);
        }
        #endregion
    }
}
