﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace SuouBase.Text
{
    public abstract class BasicLineMerge
    {
        public EditPoint Location { get; set; }
        public Size TextSize { get; protected set; }

        public abstract int Bottom { get; }
        public abstract SuouText Create();

        public abstract IEnumerable<KeyValuePair<int, StringLineData[]>> Iterate();
        
        public IEnumerable<SuouLine> GetLineEnumerator()
        {
            foreach (var v in this.Iterate())
            {
                foreach (var w in v.Value)
                {
                    yield return w.Line;
                }
            }
        }
        public IEnumerable<StringPlaneData> GetPlaneEnumerator()
        {
            foreach (var v in this.Iterate())
            {
                foreach (var w in v.Value)
                {
                    yield return new StringPlaneData(w.Line, new EditPoint(this.Location.X + w.X, this.Location.Y + v.Key));
                }
            }
        }
        public bool IsSimple { get; protected set; }

        public static BasicLineMerge Create(SuouText text, ISpaceMaker maker)
        {
            if(maker != null)
            {
                if (text.CheckAllSimpleLine())
                {
                    return new SimpleLineMerge(text, maker);
                }
                else
                {
                    return new LineMerge(text, maker);
                }
            }
            else
            {
                if (text.CheckAllSimpleLine())
                {
                    return new SimpleLineMerge(text);
                }
                else
                {
                    return new LineMerge(text);
                }
            }

        }
        /*
        public static BasicLineMerge Create(SuouText text)
        {

        }
        */
    }

    public class SimpleLineMerge:BasicLineMerge
    {
        private StringLineData[] Planes { get; set; }
        public SimpleLineMerge()
        {
            this.IsSimple = true;
            this.Location = new EditPoint(0, 0);
        }
        public SimpleLineMerge(SuouText text)
            : this()
        {
            this.TextSize = text.Size;
            this.Planes = new StringLineData[text.Count];
            int NowY = 0;
            foreach (var line in text)
            {
                if (line.Words.Count == 1)
                {
                    if (!line.Words[0].IsTransparent)
                    {
                        this.Planes[NowY] = new StringLineData(line, 0);
                        this._Bottom = NowY;
                    }
                }
                else if (line.Words.Count == 0)
                {
                }
                else
                {
                    throw new Exception();
                }
                
                NowY++;
            }

        }
        public SimpleLineMerge(SuouText text,ISpaceMaker spacer)
            : this()
        {
            this.TextSize = text.Size;
            this.Planes = new StringLineData[text.Count];

            int NowY = 0;
            foreach (var line in text)
            {
                if (line.Words.Count == 1)
                {
                    if (!line.Words[0].IsTransparent)
                    {
                        var trimed = line.SpaceSplit(spacer);
                        this.Planes[NowY] = new StringLineData(trimed.Center, trimed.Left.Width);
                        this._Bottom = NowY;
                    }
                    
                }
                else if (line.Words.Count == 0)
                {
                }
                else
                {
                    throw new Exception();
                }

                NowY++;
            }
            
        }
        private int _Bottom;
        public override int Bottom
        {
            get { return _Bottom + this.Location.Y; }
        }
        public override IEnumerable<KeyValuePair<int, StringLineData[]>> Iterate()
        {
            int t = 0;
            foreach (var v in this.Planes)
            {
                if (v != null)
                {

                    yield return new KeyValuePair<int, StringLineData[]>(t, new[] { v });
                }
                t++;
            }
        }
        public override SuouText Create()
        {
            var r = new SuouText();
            int y = 0;
            foreach (var v in this.Planes)
            {
                if (v != null)
                {
                    r[y] += v.Line;
                }
                y++;
            }
            return r;
        }
    }

    public class LineMerge : BasicLineMerge
    {
        private Dictionary<int,List<StringLineData>> Lines { get; set; }

        public LineMerge()
        {
            this.IsSimple = false;
            this.Location = new EditPoint(0, 0);
            Lines = new Dictionary<int, List<StringLineData>>();
        }
        public LineMerge(SuouText text):this()
        {
            this.TextSize = text.Size;

            int NowY = 0;
            foreach (var line in text)
            {
                var x = 0;
                foreach (var w in line.Words)
                {
                    if (!w.IsTransparent)
                    {
                        this.Add(w, new EditPoint(x, NowY));
                    }
                    x += w.Width;
                }
                NowY++;
            }

        }
        /// <summary>
        /// spacerで両端の空白を取り除いたインスタンスを設定します。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="spacer"></param>
        public LineMerge(SuouText text,ISpaceMaker spacer)
            : this()
        {
            this.MakeLineMerge(text, spacer);
            
        }
        /// <summary>
        /// spacerでテキストから両端の空白を取り除いた結果をインスタンスにせっとします。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="spacer"></param>
        private void MakeLineMerge(SuouText text,ISpaceMaker spacer)
        {
            this.TextSize = text.Size;

            int NowY = 0;
            var trimLines = text.Lines.Select((v) => v.SpaceSplit(spacer));
            var mintrimlen = trimLines.Min((v) => v.Left.Width);

            foreach (var line in trimLines)
            {
                var x = line.Left.Width - mintrimlen;
                foreach (var w in line.Center.Words)
                {
                    if (!w.IsTransparent) this.Add(w, new EditPoint(x, NowY));
                    x += w.Width;
                }
                NowY++;
            }
        }

        public IEnumerator<KeyValuePair<int,StringLineData[]>> GetEnumerator()
        {
            foreach (var v in Lines)
            {
                yield return new KeyValuePair<int,StringLineData[]>(v.Key,v.Value.ToArray());
            }
        }
        public override IEnumerable<KeyValuePair<int, StringLineData[]>> Iterate()
        {
            foreach (var v in Lines)
            {
                yield return new KeyValuePair<int, StringLineData[]>(v.Key, v.Value.ToArray());
            }
        }


        public void Add(SuouLine Line, EditPoint Location,bool IsZeroLineOmit = true)
        {
            if (IsZeroLineOmit && Line.Width == 0) return;
            var v = new StringLineData(Line,Location.X);
            if (Lines.ContainsKey(Location.Y))
            {
                this.Lines[Location.Y].Add(v);
            }
            else
            {
                this.Lines[Location.Y] = new List<StringLineData>() { v };
            }
        }
        public void Add(SuouWord word, EditPoint Location, bool IsZeroLineOmit = true)
        {
            if (IsZeroLineOmit && word.Width == 0) return;
            this.Add(new SuouLine(word), Location,IsZeroLineOmit);
        }
        public override int Bottom
        {
            get
            {
                var n = this.Lines.Keys;
                var m = 0;
                foreach (var c in n) { if (c > m) m = c; }
                return m + Location.Y;
            }
        }
        public override SuouText Create()
        {
            var r = new SuouText();
            foreach (var v in this.Lines)
            {
                var y = v.Key;
                v.Value.Sort((a, b) => a.X - b.X);
                foreach (var w in v.Value) r[y] += w.Line;
            }
            return r;
        }
    }
    public class StringLineData
    {
        public SuouLine Line;
        public int X;

        public StringLineData()
        {
        }

        public StringLineData(SuouLine str, int x)
        {
            this.Line = str;
            this.X = x;
        }
    }
    public class StringPlaneData
    {
        public SuouLine Line;
        public EditPoint Location;
        public int Y { get { return Location.Y; } }
        public int X { get { return Location.X; } }
        public StringPlaneData(SuouLine str, EditPoint p)
        {
            this.Line = str;
            this.Location = p;
        }
    }
}
