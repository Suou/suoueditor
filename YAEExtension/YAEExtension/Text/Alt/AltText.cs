﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using SuouBase;
using System.Drawing;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.ComponentModel;
namespace SuouBase.Text
{
    [ReadOnly(true)]
    public class AltText:AltLine
    {
        /*
        public AltSpace SpaceMaker;
        public AltLine LineMaker = new AltLine();
        */
        public AltText(ISpaceMaker am)
            : base(am)
        {
        }
        public static AltText Create(SpaceLineSet sl)
        {
            var v = new SpaceMaker(sl,SuouBase.SuouEditorConst.AAFont,SuouBase.SuouEditorConst.TextColor);
            var v2 = new AltText(v);
            return v2;
        }

        public static AltText Default()
        {
            var f = SuouBase.Text.SpaceMaker.Default();
            var e = new AltText(f);
            return e;
        }
        public static AltText Create(SpaceLineSet set,Font f,Color c)
        {
            if (set == null) throw new MyException("合成設定が存在しません");
            var g = SpaceMakerDic.Create(set,f,c);
            var r = new AltText(g);
            return r;
        }
        public static AltText Create(ISpaceMaker sp)
        {
            return new AltText(sp);
        }
        public static AltText CleanDefault()
        {
            var f = SuouBase.Text.SpaceMaker.CleanDefault();
            var e = new AltText(f);
            return e;
        }
        public static AltText UniDefault()
        {
            var f = SuouBase.Text.SpaceMaker.UniDefault();
            var e = new AltText(f);
            return e;
        }
        
        public static SuouText LeftPaste(SuouText Text,SuouText Layer,int StartLocationY){
            var r = Text.Clone();
            int NowY = StartLocationY;
            foreach (var line in Layer.Lines)
            {
                if(NowY>=0)r[NowY] += line;
                NowY++;
            }
            return r;
        }
        #region Rectangle
        public bool CanMakeRectangleText(SuouText Text, Size EditSize)
        {
            if (Text.Count > EditSize.Height) return false;
            SuouText r = Text.Clone();
            if (!this.CanMakeLineWidthRightSpace(r, EditSize.Width)) return false;
            return true;
        }
        public bool CanMakeRectangleText(Size EditSize)
        {
            return this.SpaceMaker.CanCreateSpace(EditSize.Width);
        }
        public SuouText MakeRectangleText(Font f,Color c,Size EditSize)
        {
            var tf = EditSize.Height.Select((v) => this.SpaceMaker.CreateSpace(EditSize.Width));
            var text = new SuouText(f, c, tf.ToArray());
            return text;
        }
        public SuouText MakeRectangleText(SuouText Text, Size EditSize)
        {
            SuouText r = Text.Clone();
            r = this.MakeLineWidthRightSpace(r, EditSize.Width);
            var spaceHeight = EditSize.Height - Text.Count;
            if (spaceHeight > 0)
            {
                var spacetext = this.MakeRectangleText(Text.Font, Text.Color, new Size(EditSize.Width, spaceHeight));
                r = r % spacetext;
            }
            else if (spaceHeight < 0)
            {
                throw new MyException("");
            }
            return r;
        }
        public SuouText MakeRectangleText(SuouText Text, bool MinWidthRectangle)
        {
            var width = Text.Width + (MinWidthRectangle ? 0 : 1);
            while (!this.CanMakeLineWidthRightSpace(Text, width)) width++;
            return this.MakeLineWidthRightSpace(Text, width);
        }
        public void MakeRectangleText(SuouText a,SuouText b)
        {
            int w = System.Math.Max(a.Width, b.Width);
            int h = System.Math.Max(a.Count, b.Count);
            for (int t = 0; ; t++)
            {
                var size = new Size(w+t,h);
                var fa = this.CanMakeRectangleText(a, size);
                var fb = this.CanMakeRectangleText(b, size);
                if (fa && fb)
                {
                    this.MakeRectangleText(a, size);
                    this.MakeRectangleText(b, size);
                    break;
                }
            }
        }
        public SuouText[] MakeSameWidthRectangleText(SuouText[] Text)
        {
            var rText = Text.Select((v) => v.Clone()).ToArray();
            var width = rText.Max((v) => v.Width);
            while (!rText.All((v) => this.CanMakeRectangleText(v, new Size(width, v.Count))))
            {
                width++;
            }
            rText = rText.Select((v) => this.MakeRectangleText(v, new Size(width, v.Count))).ToArray();
            return rText;
        }
        public SuouText[] MakeSameSizeRectangleText(SuouText[] Text)
        {
            var rText = Text.Select((v) => v.Clone()).ToArray();
            var width = rText.Max((v) => v.Width);
            var height = rText.Max((v) => v.Count);
            while (!rText.All((v) => this.CanMakeRectangleText(v, new Size(width, height))))
            {
                width++;
            }
            rText = rText.Select((v) => this.MakeRectangleText(v, new Size(width, height))).ToArray();
            return rText;
        }

        public bool IsRectangle(SuouText Text)
        {
            int w = Text[0].Width;
            return Text.Lines.All((v) => v.Width == w);
        }
        #endregion
        #region RightSpace
        public bool CanMakeLineWidthRightSpace(SuouText Text, int TextWidth)
        {
            var r = Text.Clone();
            var b =  r.Lines.All((t) => base.CanMakeLineWidthRightSpace(t, TextWidth));
            return b;
        }
        public SuouText MakeLineWidthRightSpace(SuouText Text, int TextWidth)
        {
            var r = SuouText.Empty(Text);
            
            for (int i = 0; i < Text.Count; i++)
            {
                r.Replace(i,(base.MakeLineWidthRightSpace(Text[i], TextWidth)));
            }
            return r;
        }
        #endregion

        public SuouText MakeSpaceLinesNear(SuouText Text, int TextLength)
        {
            throw new NotImplementedException();
        }
        public SuouText MakeSpaceLinesNear(SuouText Text)
        {
            throw new NotImplementedException();
        }
        #region SmallBang
        public bool CanSmallBangMergeText(SuouText DownText, SuouLine UpString, int StartDotX,int y)
        {
            if (y < 0) return true;
            return this.CanSmallBangMergeString(DownText[y], UpString, StartDotX);
        }
        public void SmallBangMergeText(SuouText DownText, SuouLine UpString, int StartDotX, int y)
        {
            if(y >= 0)DownText[y] = this.SmallBangMergeString(DownText[y], UpString, StartDotX);
        }
        public bool CanSmallBangMergeText(SuouText DownText, SimpleLineMerge UpTextData, int diff)
        {
            foreach (var v in UpTextData.Iterate())
            {
                foreach (var w in v.Value)
                {
                    Point p = new Point(UpTextData.Location.X + diff + w.X, UpTextData.Location.Y + v.Key);
                    if (!CanSmallBangMergeText(DownText, w.Line, p.X, p.Y)) return false;
                }
            }
            return true;
        }
        public bool CanSmallBangMergeText(SuouText DownText, LineMerge UpTextData,int diff)
        {
            var dt = DownText.Clone();
            foreach (var v in UpTextData.Iterate())
            {
                foreach (var w in v.Value)
                {
                    Point p = new Point(UpTextData.Location.X + diff + w.X, UpTextData.Location.Y + v.Key);
                    if (CanSmallBangMergeText(dt, w.Line, p.X, p.Y))
                    {
                        this.SmallBangMergeText(dt, w.Line, p.X, p.Y);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public SuouText SmallBangMergeText(SuouText DownText, BasicLineMerge UpTextData, int diff)
        {
            var dt = DownText.Clone();
            foreach (var v in UpTextData.Iterate())
            {
                foreach (var w in v.Value)
                {
                    Point p = new Point(UpTextData.Location.X + diff + w.X, UpTextData.Location.Y + v.Key);
                    //var w1 = UpTextData.Location.X + w.X + w.Line.Width + diff;
                    this.SmallBangMergeText(dt, w.Line, p.X, p.Y);
                    //var w2 = dt.Width;
                    //if (w1 != w2) throw new Exception();
                }
            }
            return dt;
        }
        #endregion
        #region BigBang
        public void BigBangMergeText(SuouText DownText, BasicLineMerge UpString)
        {
            foreach (var v in UpString.GetPlaneEnumerator())
            {
                DownText[v.Y] = this.BigBangMergeString(DownText[v.Y], v.Line, v.X);
            }
        }
        #endregion
        #region Copy
        /// <summary>
        /// 選択文字列を除いたTextの全ての文字列を
        /// StringPlaneDataに変換してリストにして返します
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="DotLineStructList"></param>
        /// <returns></returns>
        public LineMerge XORPartialCopy(SuouText Text, InverseDataCollection IDC)
        {
            var r = new LineMerge();
            for (int i = 0; i < Text.Count; i++)
            {
                //その行に反転選択が存在していたら
                if (IDC.Items.ContainsKey(i))
                {
                    int x = 0;
                    foreach (DotBand dls in IDC.Items[i].Key)
                    {
                        this.Subline(Text[i], x, dls.BeginX - x,r,i);
                        x = dls.EndX + 1;
                    }
                    this.Subline(Text[i], x,r,i);
                    r.Add(SuouLine.Empty(Text.Font,Text.Color), new EditPoint(0, i),false);
                }
                else
                {
                    r.Add(Text[i], new EditPoint(0, i),false);
                }
            }
            return r;
        }
        public SuouText NaiveCopy(SuouText Text, InverseDataCollection IDC)
        {
            throw new NotImplementedException();
        }
        public SuouText SimpleCopy(SuouText Text, InverseDataLapper IDL)
        {
            SuouText rlist = SuouText.Empty(Text);
            //var copyText = Text.Clone();

            int min = IDL.Item.MinInt;
            foreach (var dls in IDL.Item.Items)
            {
                foreach (var v in dls.Value.Key)
                {
                    if(Text.Count <= v.Location.Y) continue;
                    var bond = v.Modify(Text[v.Location.Y]);
                    if (!bond.HasValue) continue;
                    var str = Text[v.Location.Y].SubDotLineIncompletely(this.SpaceMaker,bond.Value.Location.X, bond.Value.Width,SplitWordOption.NoBangWord,SplitWidthOption.Relax);
                    if (str.Sentence != null)
                    {
                        int n = v.Location.Y - min;
                        rlist[n] += str;
                    }
                }

                var b = dls.Value.Value;
                var y = dls.Key - min+1;
                if(b){while (rlist.Count <= y) rlist.Add(SuouLine.Empty(Text.Font,Text.Color));}
                
            }


            return rlist;
        }
        public bool CanSpaceReplace(SuouText Text, InverseDataLapper IDL)
        {
            //var data = XORPartialCopy(Text, IDC);

            //var copyText = data.Create(Text.PrintSet, Text.Count);
            var copyText = Text.Clone();
            return IDL.Item.GetDotBands().All(
                (band) =>{
                    if (copyText.Count <= band.Location.Y) return true;
                    return this.CanSpaceReplaceSublineStrictly(copyText[band.Location.Y], band.BeginX, band.Width);
                }
                    );
        }
        public SuouText SpaceReplace(SuouText Text, InverseDataCollection IDC)
        {
            var copyText = Text.Clone();
            foreach (var band in IDC.GetDotBands())
            {
                if (copyText.Count <= band.Location.Y) continue;
                copyText[band.Location.Y] = (this.SpaceReplaceSublineStrictly(copyText[band.Location.Y], band.BeginX, band.Width));
                
            }
            return copyText;
        }
        public SuouText ReplaceArea(SuouText Text, InverseDataCollection IDC, string s)
        {
            var simple = new SimpleSpaceMaker(s);
            var copyText = Text.Clone();
            IDC.GetDotBands().Where((v) => !copyText.IsOutOfIndex(v.Location.Y)).ForEach((v) => ReplaceLineArea(copyText, v, simple));
            return copyText;
        }
        private void ReplaceLineArea(SuouText Text, DotBand band,SimpleSpaceMaker item)
        {
            var line = Text[band.Location.Y];
            var triple = line.TripleDotSplit(band.BeginX,band.Width);
            Text[band.Location.Y] = triple.Left + item.CreateSpaceCarelessly(triple.Center.Width) + triple.Right;
        }

        #endregion

        #region Other
        public SuouText Delete(SuouText Text, InverseDataLapper IDL)
        {
            var data = XORPartialCopy(Text, IDL.Item);
            var r = data.Create();
            IDL.Item.Paragraphs.Reverse().ToList().ForEach((n) => { r.DeleteParagraph(n + 1); });
            return r;
        }
        /// <summary>
        /// AAEditのAlt+→←機能
        /// nドット左右に寄せる
        /// </summary>
        /// <param name="n"></param>
        /// <param name="Text"></param>
        /// <param name="SP"></param>
        /// <returns></returns>
        public KeyValuePair<SuouText, StringPoint> AltOperator(int n, SuouText Text, StringPoint SP)
        {
            throw new NotImplementedException();

        }
        /// <summary>
        /// Text[p.y][p.x]から長さlenの文字列を抜き出します
        /// </summary>
        /// <param name="Text"></param>
        /// <param name="p"></param>
        /// <param name="len"></param>
        /// <returns></returns>
        public SuouLine SubString(SuouText Text, EditPoint p, int len)
        {
            if (Text.Count <= p.Y) return null;
            return Text[p.Y].DotSublineStrictly(p.X, len);
        }
        #endregion

        #region Merge

        public void MergeEachLineCarelessly(SuouText DownText, BasicLineMerge UpTextData)
        {
            foreach (var Line in UpTextData.GetPlaneEnumerator())
            {
                if (Line.Location.Y < 0) continue;
                var line = base.MergeStringCarelessly(DownText[Line.Y], Line.Line, Line.X);
                DownText[Line.Y] = line;
            }
        }
        public void MergeTextCarelessly(SuouText DownText, BasicLineMerge UpTextData)
        {
            var ep = UpTextData.Location;
            foreach (var x in IntExtension.PlusMinusCountUp())
            {
                if (x > 1000) throw new Exception();
                UpTextData.Location = new EditPoint(ep.X + x,ep.Y);
                if (CanMergeStrictly(DownText, UpTextData))
                {
                    MergeStrictly(DownText, UpTextData);
                    UpTextData.Location = ep;
                    break;
                }
            }
        }
        public void MergeTextCarelessly(SuouText DownText, BasicLineMerge UpTextData,int PermitZure)
        {
            var ep = UpTextData.Location;
            foreach (var x in IntExtension.PlusMinusCountUp())
            {
                if (x > 1000) throw new Exception();
                UpTextData.Location = new EditPoint(ep.X + x, ep.Y);
                if (CanMergeTextWithZure(DownText, UpTextData,PermitZure))
                {
                    MergeTextWithZure(DownText, UpTextData,PermitZure);
                    UpTextData.Location = ep;
                    break;
                }
            }
        }
        public bool CanMergeTextWithZure(SuouText DownText, BasicLineMerge UpTextData,int PermitZure)
        {
            var CloneDownText = DownText.Clone();
            var ep = UpTextData.Location;
            var diff = IntExtension.PlusMinusIntArray(PermitZure);
            
            return UpTextData.Iterate().ToList().All((b) =>
            {
                var y = UpTextData.Location.Y + b.Key;
                if (y < 0) return true;
                var by = b.Value.All((c) =>
                {
                    var x = UpTextData.Location.X + c.X;

                    if (this.CanMergeStringStrictly(CloneDownText[y], c.Line, x))
                    {
                        CloneDownText[y] = (this.MergeStringStrictly(CloneDownText[y], c.Line, x));
                        return true;
                    }
                    else
                    {
                        var z = diff.FirstOrDefault((n) => this.CanMergeStringStrictly(CloneDownText[y], c.Line, x + n));
                        if (z == 0)
                        {
                            return false;
                        }
                        else
                        {
                            CloneDownText[y] = (this.MergeStringStrictly(CloneDownText[y], c.Line, x + z));
                            return true;
                        }
                    }
                }
                    );

                return by;
            }
                );
        }
        public void MergeTextWithZure(SuouText DownText, BasicLineMerge UpTextData, int PermitZure)
        {
            var ep = UpTextData.Location;
            var diff = IntExtension.PlusMinusIntArray(PermitZure);

            UpTextData.Iterate().ToList().ForEach((b) =>
            {
                var y = UpTextData.Location.Y + b.Key;
                if (y >= 0)
                {
                    b.Value.ForEach((c) =>
                    {
                        var x = UpTextData.Location.X + c.X;
                        var z = diff.FirstOrDefault((n) => this.CanMergeStringStrictly(DownText[y], c.Line, x + n));
                        DownText[y] = (this.MergeStringStrictly(DownText[y], c.Line, x + z));
                    }

                        );
                }

            }
                );
        }
        public bool CanMergeStrictly(SuouText DownText, BasicLineMerge UpTextData)
        {
            var CloneDownText = DownText.Clone();
            /*
            if (CloneDownText.Count < UpTextData.Bottom)
            {
                throw new MyException("");
            }
            */
            return UpTextData.Iterate().ToList().All((b) => CheckCanMergeStrictly(CloneDownText, UpTextData.Location, b));
                
        }
        private bool CheckCanMergeStrictly(SuouText DownText, EditPoint StartPoint,KeyValuePair<int,StringLineData[]> b)
        {
            var y = StartPoint.Y + b.Key;
            if (y < 0) return true;
            var by = b.Value.All((c) =>
            {
                var x = StartPoint.X + c.X;
                var z = this.CanMergeStringStrictly(DownText[y], c.Line, x);
                if (z)
                {
                    DownText[y] = (this.MergeStringStrictly(DownText[y], c.Line, x));
                    return true;
                }
                else
                {
                    return false;
                }
            }
                );

            return by;
        }
        public void MergeStrictly(SuouText DownText, BasicLineMerge UpTextData)
        {
            //if (DownText.Count < UpTextData.Bottom) throw new Exception();
            UpTextData.Iterate().ToList().ForEach((b) =>
            {
                var y = UpTextData.Location.Y + b.Key;
                if (y >= 0)
                {
                    b.Value.ForEach((c) =>
                    {
                        var x = UpTextData.Location.X + c.X;
                        DownText[y] = this.MergeStringStrictly(DownText[y], c.Line, x);
                    }
                        );
                }
            }
                );
        }
        public IEnumerable<EditPoint> LineEachLocation(EditPoint ep, int count)
        {
            for (int i = 0; i < count; i++)
            {
                yield return new EditPoint(ep.X + i, ep.Y);
            }
        }

        #endregion
    }
}
