﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text
{

    public class InverseDataLapper
    {
        public event InverseMethodEventHandler InverseChanged;
        private InverseDataCollection _Item;
        public InverseDataCollection Item
        {
            get { return this._Item; }
            set
            {
                this._Item = value;
                this.OnInverseChanged();
            }
        }
        public void Clear()
        {
            this.Item = new InverseDataCollection();
        }
        public bool IsEmpty
        {
            get
            {
                return this.Item.IsEmpty;
            }
        }
        public EditPoint GetFrontLocation(SuouText text)
        {
            return this.Item.FrontLocation(text);
        }

        private void OnInverseChanged()
        {
            if (InverseChanged != null) InverseChanged(this, new InverseMethodEventArg(this.Item));
        }
        public void MergeXOR(DotBand d)
        {
            var v = this.Item.Copy();
            v.MergeXOR(d);
            this.Item = v;

        }
        public bool CreateNewLine(EditPoint e, int ChoiceLength)
        {
            var vp = new InverseDataCollection();

            var ChoiceStartPoint = e.X - ChoiceLength;
            var len = ChoiceStartPoint < 0 ? e.X : ChoiceLength;
            ChoiceStartPoint = ChoiceStartPoint < 0 ? 0 : ChoiceStartPoint;
            if (e.IsPlus && len > 0)
            {
                var v = new DotBand(ChoiceStartPoint, e.Y, len);

                //var v = new DotBand(e.EditLocation.X - len,e.EditLocation.Y, len);
                vp.MergeXOR(v);
                this.Item = vp;
                return true;
            }
            return false;
        }
    }
}
