﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using SuouBase;

namespace SuouBase.Text
{
    public class InverseDataCollection
    {
        public Dictionary<int, KeyValuePair<DotBand[], bool>> Items = new Dictionary<int, KeyValuePair<DotBand[], bool>>();

        public IEnumerable<Band> BandIterator(SuouText st)
        {
            foreach (var v in this.Items.Values)
            {
                foreach (var w in v.Key)
                {
                    yield return w.ToBand(st);
                }
            }
        }
        public IEnumerable<Band> NormalBandIterator(SuouText st)
        {
            foreach (var v in this.Items.Values)
            {
                foreach (var w in v.Key)
                {
                    if (st.ContainDotLocation(w.Location))
                    {
                        yield return w.ToBand(st);
                    }
                }
            }
        }
        #region Merge
        public void MergeXOR(KeyValuePair<DotBand[], bool> id,int y)
        {
            if (this.Items.ContainsKey(y))
            {
                this.Items[y] = DotBandMerge.MergeXOR(this.Items[y], id,y);
            }
            else
            {
                this.Items[y] = id;
            }
        }
        public void MergeParagraphXOR(int y)
        {
            if (this.Items.ContainsKey(y))
            {
                var b = !this.Items[y].Value;
                this.Items[y] = new KeyValuePair<DotBand[],bool>(this.Items[y].Key,b);
            }
            else
            {
                this.Items[y] = new KeyValuePair<DotBand[],bool>(new DotBand[]{},true);
            }
        }
        public void MergeParagraphXOR(int[] ys)
        {
            foreach (var n in ys) this.MergeParagraphXOR(n);
        }
        public void MergeOR(KeyValuePair<DotBand[], bool> id, int y)
        {
            if (this.Items.ContainsKey(y))
            {
                this.Items[y] = DotBandMerge.MergeOR(this.Items[y], id, y);
            }
            else
            {
                this.Items[y] = id;
            }
        }
        private void MergeAND(KeyValuePair<DotBand[], bool> id, int y)
        {
            if (this.Items.ContainsKey(y))
            {
                this.Items[y] = DotBandMerge.MergeAND(this.Items[y], id, y);
            }
        }
        public void MergeXOR(DotBand d)
        {
            int y = d.Location.Y;
            if (this.Items.ContainsKey(y))
            {
                if (this.Items.Keys != null)
                {
                    var a = DotBandMerge.MergeXOR(this.Items[y].Key, d, y);
                    this.Items[y] = new KeyValuePair<DotBand[], bool>(a, this.Items[y].Value);
                }
                else
                {
                    var a = new DotBand[] { d };
                    this.Items[y] = new KeyValuePair<DotBand[], bool>(a, this.Items[y].Value);
                }
            }
            else
            {
                var a = new DotBand[] { d };
                this.Items[y] = new KeyValuePair<DotBand[], bool>(a, false);
            }
        }

        public void MergeXOR(InverseDataCollection a)
        {
            foreach (var t in a.Items)
            {
                this.MergeXOR(t.Value, t.Key);
            }
        }
        public void MergeAND(InverseDataCollection b)
        {
            var aY = this.LocationYs;
            var bY = b.LocationYs;
            var deleteY = aY.Except(bY).ToArray();
            foreach (int y in deleteY) this.Items.Remove(y);
            foreach (var t in b.Items)
            {
                this.MergeAND(t.Value, t.Key);
            }
        }
        public void MergeOR(InverseDataCollection a)
        {
            foreach (var t in a.Items)
            {
                this.MergeOR(t.Value, t.Key);
            }
        }
        #endregion
        #region StaticMerge
        public static InverseDataCollection MergeXOR(InverseDataCollection a, InverseDataCollection b)
        {
            var r = new InverseDataCollection();
            r.MergeXOR(a);
            r.MergeXOR(b);
            return r;
        }
        public static InverseDataCollection MergeOR(InverseDataCollection a, InverseDataCollection b)
        {
            var r = new InverseDataCollection();
            r.MergeOR(a);
            r.MergeOR(b);
            return r;
        }
        public static InverseDataCollection MergeAND(InverseDataCollection a, InverseDataCollection b)
        {
            var r = a.Copy();
            r.MergeAND(b);
            return r;
        }
        #endregion
        #region NonBandMethod
        
        public DotBand[] GetDotBands()
        {
            var r = this.Items.Values.SelectMany((v) => v.Key);
            return r.ToArray();
        }
        public InverseDataCollection Copy()
        {
            var r = new InverseDataCollection();
            foreach (var t in this.Items)
            {
                r.MergeXOR(t.Value, t.Key);
            }
            return r;
        }
        public int MaxY
        {
            get
            {
                if (this.Items.Keys.Count == 0) return -1;
                int y = this.Items.Keys.Max();
                return y;
            }
        }
        public int[] LocationYs
        {
            get
            {
                var v = this.Items.Keys.ToArray();
                var rs = v.Where((t) => this.Items[t].Key.Length > 0 || this.Items[t].Value);
                var ars = rs.ToList();
                ars.Sort();
                return ars.ToArray();
            }
        }
        public bool IsEmpty
        {
            get
            {
                return this.Items.Values.All((t) => { return DotBandMerge.BandEmpty(t.Key) && !t.Value; });
            }
        }
        public EditPoint FrontLocation(SuouText text)
        {
                var y = this.Items.Keys.Min();
                if (text.Count <= y) return text.LastLocation;
                int x = text[y].Width;
                if(this.Items[y].Key.Count() > 0)x = this.Items[y].Key.Min((t) => t.Location.X);
                
                return new EditPoint(x, y);
            
        }
        public override string ToString()
        {
            string s = "IDC/";
            foreach (var v in this.Items.Values)
            {
                foreach (var w in v.Key)
                {
                    s += "{" + "X:" + w.Location.X + "/Y:" + w.Location.Y + "/Wid:" + w.Width + "}";
                }
                s += v.Value;
            }
            return s;
        }
        public bool EqualLength(InverseDataCollection idc)
        {
            if (this.Items.Count != idc.Items.Count) return false;
            foreach (var t in idc.Items)
            {
                if (!this.Items.ContainsKey(t.Key)) return false;
                if (!DotBandMerge.EqualLength(this.Items[t.Key], t.Value)) return false;
            }
            return true;
        }
        public InverseDataCollection Sabun(InverseDataCollection idc)
        {
            var r = new InverseDataCollection();
            r.MergeXOR(this);
            r.MergeXOR(idc);
            return r;
        }
        public int[] Paragraphs
        {
            get
            {
                var r = new List<int>();
                foreach (var v in this.Items)
                {
                    if (v.Value.Value) r.Add(v.Key);
                }
                r.Sort();
                return r.ToArray();
            }
        }
        public int DotBandCount
        {
            get
            {
                int i = 0;
                foreach (var v in this.Items.Values)
                {
                    i += v.Key.Count();
                }
                return i;
            }
        }
        public int MinInt
        {
            get
            {
                if (this.Items.Count == 0) return -1;
                return this.Items.Keys.Min((t) => t);
            }
        }
        
        #endregion
        #region BandMethod
        public void Clear()
        {
            this.Items.Clear();
        }
        public void LineChoice(int nth, SuouText tc)
        {
            if (nth >= tc.Count || nth < 0) throw new Exception();
            if (this.Items.ContainsKey(nth)) this.Items.Remove(nth);
            
            bool b = tc.Count-1 != nth;
            if (tc[nth].Width == 0)
            {
                this.Items[nth] = new KeyValuePair<DotBand[], bool>(new DotBand[] {}, b);
            }
            else
            {
                var v = new DotBand(0, nth, tc[nth].Width);
                this.Items[nth] = new KeyValuePair<DotBand[], bool>(new DotBand[] { v }, b);
            }
        }
        public void AllChoice(SuouText tc)
        {
            for (int i = 0; i < tc.Count; i++)
            {
                LineChoice(i, tc);
            }
        }
        #endregion
        #region Operator
        public static InverseDataCollection operator &(InverseDataCollection a, InverseDataCollection b)
        {
            return InverseDataCollection.MergeAND(a,b);
        }
        public static InverseDataCollection operator |(InverseDataCollection a, InverseDataCollection b)
        {
            return InverseDataCollection.MergeOR(a, b);
        }
        public static InverseDataCollection operator -(InverseDataCollection a, InverseDataCollection b)
        {
            var c = InverseDataCollection.MergeXOR(a, b);
            return c & a;
        }
        public static InverseDataCollection operator ^(InverseDataCollection a, InverseDataCollection b)
        {
            var c = InverseDataCollection.MergeXOR(a, b);
            return c;
        }
        #endregion        
    }


}
