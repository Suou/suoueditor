﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Numerics;
using System.Windows.Forms;
using SuouBase;


namespace SuouBase.Text
{


    public class ParagraphList
    {
        public List<int> y = new List<int>();

        public IEnumerator<int> GetEnumerator()
        {
            for (int i = 0; i < y.Count; i++)
                yield return y[i];
        }

        public int this[int i]
        {
            get { return this.y[i]; }
        }

        public bool IsEmpty
        {
            get
            {
                return this.y.Count == 0;
            }
        }
        /*
        public Rectangle[] ToRectangle(PrintSetting PS)
        {
            List<Rectangle> r = new List<Rectangle>();
            foreach (int i in y)
            {
                Rectangle Rec = new Rectangle(DotSize.MeasureLength(PS.GetText()[i-1]), (i-1) * PS.YLineSize, DotSize.MeasureLength("↓"), PS.YLineSize);
                Rec.X += PS.BasePaddingC.Width - PS.HsbC.Value;
                Rec.Y += PS.BasePaddingC.Height - (PS.VsbC.Value * PS.YLineSize);
                r.Add(Rec);
            }
            return r.ToArray();
        }
        */
        public void Add(int i)
        {
            if (!y.Contains(i))
            {
                y.Add(i);
                y.Sort();
                y.Reverse();
            }
            else
            {
                this.Delete(i);
            }
        }
        public void AddRange(ParagraphList pl)
        {
            foreach (int y in pl)
            {
                this.Add(y);
            }
        }
        public void Delete(int i)
        {
            y.RemoveAll(n => n==i);
        }
        public void Clear()
        {
            y.Clear();
        }
        public ParagraphList Copy()
        {
            var p = new ParagraphList();
            p.AddRange(this);
            return p;
        }
    }

    

    public interface InverseData{
    }

    public struct DotParagraph:InverseData
    {
        public int Y;
        public DotParagraph(int y)
        {
            this.Y = y;
        }
    }
    public struct Band
    {
        public StringPoint Location;
        public int Length;
        public Band(StringPoint location,int length)
        {
            this.Location = location;
            this.Length = length;
        }
        public int StartX
        {
            get { return this.Location.X; }
        }
        public int EndX
        {
            get { return this.Location.X + Length; }
        }
        public int Y
        {
            get { return this.Location.Y; }
        }
    }
    public class PBand
    {
        public int X;
        public int Length;
        public PBand(int x,int length)
        {
            this.X = x;
            this.Length = length;
        }
        public int StartX
        {
            get { return this.X; }
        }
        public int EndX
        {
            get { return this.X + Length; }
        }
    }
    public class MatchBand:PBand
    {
        public int StartWidth { get; set; }
        public int Y { get; set; }
        public int MatchPoint;
        public MatchBand(int x,int length,int matchpoint):base(x,length)
        {
            this.MatchPoint = matchpoint;
        }
        public EditPoint EditLocation
        {
            get { return new EditPoint(this.StartWidth, this.Y); }
            set
            {
                this.StartWidth = value.X;
                this.Y = value.Y;
            }
        }
        public bool Overlap(MatchBand v)
        {
            if (v == null) return false;
            if (this.Y != v.Y) return false;
            if (this.X <= v.X && v.X <= this.EndX) return true;
            if (v.X <= this.X && this.X <= v.EndX) return true;
            return false;
        }

    }
    public struct DotBand : IComparable,InverseData
    {
        public readonly EditPoint Location;
        public readonly int Width;


        public int EndX
        {
            get { return this.Location.X + Width-1; }
        }
        public int BeginX
        {
            get { return this.Location.X; }
        }
        public EditPoint EndLocation
        {
            get
            {
                var a = this.Location;
                a.X += this.Width;
                return a;
            }
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public DotBand(int x, int y, int Length)
        {
            if (Length == 0) throw new Exception();
            var p = new EditPoint();

            if (x < 0)
            {
                Length += x;
                x = 0;
            }

            p.X = x;
            p.Y = y;
            this.Location = p;
            this.Width = Length;
        }
        public DotBand? Modify(SuouLine line)
        {
            if (line.Width < this.BeginX) return null;
            if (this.EndX > line.Width)
            {
                return new DotBand(this.BeginX,this.Location.Y, line.Width - this.BeginX);
                
            }
            else
            {
                return this;
            }
        }

        public DotBand(EditPoint p, int Length)
        {
            if (Length == 0) throw new Exception();
            if (p.X < 0)
            {
                Length += p.X;
                p.X = 0;
            }

            this.Location = p;
            this.Width = Length;
        }
        
        public BigInteger ToBigInteger()
        {
            BigInteger v = new BigInteger(0);
            
            var upper = BigInteger.Pow(2, this.EndX + 1) - 1;
            var lower = BigInteger.Pow(2, this.BeginX) - 1;
            v = upper - lower;

            return v;
        }
        
        public string ToDigitalString()
        {
            StringBuilder s = new StringBuilder("");
            for (int i = 0; i <= this.EndX; i++)
            {
                if (i >= this.BeginX)
                {
                    s.Append("1");
                }
                else
                {
                    s.Append("0");
                }
            }
            return s.ToString();
        }
        public Band ToBand(SuouText st)
        {
            var x1 = st[this.Location.Y].TruncateFromWidthToLength(this.BeginX, NearestOption.LeftNearest).Index;
            var x2 = st[this.Location.Y].TruncateFromWidthToLength(this.EndX, NearestOption.RightNearest).Index;
            var f = new Band(new StringPoint(x1, this.Location.Y), x2 - x1);
            return f;
        }
        
        
        /*
        public Rectangle ToRectangle(PrintSetting PS)
        {
            Rectangle Rec = new Rectangle(this.Location.X, this.Location.Y * PS.YLineSize, this.Length, PS.YLineSize);
            Rec.X += PS.BasePaddingC.Width - PS.HsbC.Value;
            Rec.Y += PS.BasePaddingC.Height - (PS.VsbC.Value * PS.YLineSize);
            return Rec;
        }
        */
        public override string ToString()
        {
            return "DotBand { Position = " + this.Location + ", Length = " + this.Width+" }";
        }

        public int CompareTo(object obj)
        {
            DotBand dls1 = this;
            DotBand dls2 = (DotBand)obj;

            if (dls1.Location < dls2.Location)
            {
                return -1;
            }
            else if (dls1.Location > dls2.Location)
            {
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public static bool operator ==(DotBand a, DotBand b)
        {
            return (a.Location == b.Location && a.Width == b.Width);
        }
        public static bool operator !=(DotBand a, DotBand b)
        {
            return !(a==b);
        }
        
    }

}
