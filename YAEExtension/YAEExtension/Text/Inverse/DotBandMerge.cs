﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Numerics;
using SuouBase;
using BSystem;
using BSystem.Text;
namespace SuouBase.Text
{

    public class DotBandMerge
    {
        public static bool BandEmpty(DotBand[] a)
        {
            int n = 0;
            if (a == null) return true;
            foreach (var d in a)
            {
                n += d.Width;
            }
            return !(n > 0);
        }
        public static int BandLength(DotBand[] a)
        {
            int n = 0;
            if (a == null) return 0;
            foreach (var d in a)
            {
                n += d.Width;
            }
            return n;
        }


        public static bool EqualLength(KeyValuePair<DotBand[], bool> a, KeyValuePair<DotBand[], bool> b)
        {
            if (a.Value != b.Value) return false;
            if (a.Key != null && b.Key != null)
            {
                if (a.Key.Count() != b.Key.Count()) return false;
                int n = a.Key.Count();
                for (int i = 0; i < n; i++)
                {
                    if (a.Key[i] != b.Key[i]) return false;
                }
                return true;
            }
            else
            {
                return a.Key == b.Key;
            }
        }

        public static KeyValuePair<DotBand[], bool> MergeXOR(KeyValuePair<DotBand[], bool> a, KeyValuePair<DotBand[], bool> b, int y)
        {
            DotBand[] c = a.Key.Concat(b.Key).ToArray();
            var d = DotBandMerge.MergeXOR(c, y);
            bool Paragraph = (a.Value || b.Value) && (!a.Value || !b.Value);
            return new KeyValuePair<DotBand[], bool>(d, Paragraph);
        }
        public static KeyValuePair<DotBand[], bool> MergeOR(KeyValuePair<DotBand[], bool> a, KeyValuePair<DotBand[], bool> b, int y)
        {
            DotBand[] c = a.Key.Concat(b.Key).ToArray();
            var d = DotBandMerge.MergeOR(c, y);
            bool Paragraph = (a.Value || b.Value);
            return new KeyValuePair<DotBand[], bool>(d, Paragraph);
        }
        public static KeyValuePair<DotBand[], bool> MergeAND(KeyValuePair<DotBand[], bool> a, KeyValuePair<DotBand[], bool> b, int y)
        {
            var d = DotBandMerge.MergeAND(a.Key, b.Key, y);
            bool Paragraph = (a.Value && b.Value);
            return new KeyValuePair<DotBand[], bool>(d, Paragraph);
        }
        public static DotBand[] MergeXOR(DotBand[] a, DotBand b, int y)
        {
            var c = a.ToList();
            c.Add(b);
            var d = DotBandMerge.MergeXOR(c.ToArray(), y);
            return d;
        }

        public static DotBand[] ConvertDotBand(string bits, int y)
        {
            List<DotBand> rlist = new List<DotBand>();
            bool creating = false;
            int len = -1;
            int beginx = -1;
            int i = 0;
            while (i < bits.Length)
            {

                while (bits[i] == '1')
                {
                    if (!creating)
                    {
                        creating = true;
                        len = 1;
                        beginx = i;
                    }
                    else
                    {
                        len++;
                    }
                    i++;
                    if (i >= bits.Length) break;
                }
                if (i >= bits.Length) break;
                while (bits[i] == '0')
                {
                    if (creating)
                    {
                        rlist.Add(new DotBand(beginx, y, len));
                        creating = false;
                        len = 0;
                    }
                    i++;
                    if (i >= bits.Length) break;
                }
            }
            if (creating)
            {
                rlist.Add(new DotBand(beginx, y, len));
            }
            return rlist.ToArray();

        }
        /*
        public static DotBand[] ConvertDotBand(string bits, int y)
        {
            List<DotBand> rlist = new List<DotBand>();
            bool creating = false;
            int len = -1;
            int beginx = -1;


            var v = new DotBand();
            for (int i = 0; i < bits.Length; i++)
            {
                
                if (bits[i] == '1')
                {

                    if (b)
                    {
                        v = new DotBand(v.Location, v.Length + 1);
                    }
                    else
                    {
                        v = new DotBand(i, y, 1);
                        b = true;
                    }
                }
                else
                {
                    if (b)
                    {
                        b = false;
                        rlist.Add(v);
                        v = new DotBand(v.Location, 0);
                    }
                }
            }
            if (v.Length != 0) rlist.Add(v);
            return rlist.ToArray();
        }
        */
        public static DotBand[] MergeXOR(DotBand[] a, int y)
        {
            BigInteger c = new BigInteger(0);
            foreach (DotBand d in a)
            {
                var dbi = d.ToBigInteger();
                c = c ^ dbi;
            }
            var bitstring = DotBandMerge.ToBinaryString(c);

            var v = DotBandMerge.ConvertDotBand(bitstring, y);

            return v;
        }

        public static DotBand[] MergeXOR(DotBand[] a, DotBand[] b, int y)
        {
            if (a == null || b == null) throw new ArgumentNullException();
            var ab = (DotBand[])a.Concat(b);
            return MergeXOR(ab, y);
        }
        public static DotBand[] MergeOR(DotBand[] a, int y)
        {

            BigInteger c = new BigInteger(0);
            foreach (DotBand d in a)
            {
                c = c | d.ToBigInteger();
            }
            var v = DotBandMerge.ConvertDotBand(DotBandMerge.ToBinaryString(c), y);

            return v;
        }
        public static DotBand[] MergeAND(DotBand[] a, DotBand[] b, int y)
        {
            if (a == null || b == null) throw new ArgumentNullException();
            BigInteger cbit = new BigInteger(0);
            foreach (DotBand d in a)
            {
                cbit = cbit ^ d.ToBigInteger();
            }
            BigInteger bbit = new BigInteger(0);
            foreach (DotBand d in b)
            {
                bbit = bbit ^ d.ToBigInteger();
            }
            var rbit = cbit & bbit;
            var v = DotBandMerge.ConvertDotBand(DotBandMerge.ToBinaryString(rbit), y);

            return v;
        }
        public static string ToBinaryString(BigInteger a)
        {
            StringBuilder s = new StringBuilder("");
            var c = a.ToByteArray();
            for (int i = 0; i < c.Length; i++)
            {
                var v = Convert.ToString(c[i], 2);
                v = v.Reverse();
                if (i != c.Length - 1)
                {
                    while (v.Length < 8) v += "0";
                }
                s.Append(v);
            }
            return s.ToString();
        }
        /*
        public static Dictionary<int, List<DotBand>> ToDictionary(DotBand[] a)
        {
            var dic = new Dictionary<int, List<DotBand>>();
            foreach (DotBand b in a)
            {
                if (!dic.ContainsKey(b.Location.Y))
                {
                    dic[b.Location.Y] = new List<DotBand>();
                }
                dic[b.Location.Y].Add(b);
            }
            return dic;
        }
        */
    }
}
