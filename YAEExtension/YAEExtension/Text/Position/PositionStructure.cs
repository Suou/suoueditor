﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase;

namespace SuouBase.Text
{
    

    public struct StringPoint
    {
        private Point Position;

        public static StringPoint Empty
        {
            get { return new StringPoint(0, 0); }
        }
        public Point Location
        {
            get { return Position; }
        }
        public int X
        {
            get
            {
                return Position.X;
            }
            set
            {
                Position.X = value;
            }
        }
        public int Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                Position.Y = value;
            }
        }

        public StringPoint(int x,int y)
        {
            this.Position = new Point(x, y);
        }
        public override string ToString()
        {
            string s = "StringPoint/";
            s += "Position = {" + this.X + "/" + this.Y + "}/";

            return s;
        }
    }
    [Serializable]
    public struct EditPoint
    {
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public bool IsPlus
        {
            get 
            {
                if (this.X >= 0 && this.Y >= 0) return true;
                return false;
            }
        }
        private Point Position;
        public int X
        {
            get
            {
                return Position.X;
            }
            set
            {
                Position.X = value;
            }
        }
        public int Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                Position.Y = value;
            }
        }
        public EditPoint(int x,int y)
        {
            this.Position = new Point(x, y);
        }
        public EditPoint(Point p)
        {
            this.Position = new Point(p.X, p.Y);
        }
        public override string ToString()
        {
            string s = "EditPoint/";
            s += "Pos = {" + this.X + "/" + this.Y + "}";

            return s;
        }
        /*
        public ExEditPoint ToExEditPoint(DisplaySetting ds,TextComposition tc)
        {
            return new ExEditPoint(this.X, this.Y, ds, tc);
        }
        */
        public Point ToPoint()
        {
            return new Point(this.X, this.Y);
        }
        #region Operator

        public static bool operator ==(EditPoint a, EditPoint b)
        {
            return !(a != b);
        }
        public static bool operator !=(EditPoint a, EditPoint b)
        {
            return !(a.X == b.X && a.Y == b.Y);
        }

        public static bool operator <(EditPoint a, EditPoint b)
        {
            //a<b
            if (a.Y > b.Y)
            {
                return false;
            }
            else if (a.Y == b.Y)
            {
                if (a.X < b.X)
                {
                    return true;
                }
                else//(a.X >= b.X)
                {
                    return false;
                }
            }
            else //(a.Y < b.Y)
            {
                return true;
            }
        }
        public static bool operator <=(EditPoint a, EditPoint b)
        {
            //a<b
            if (a.Y > b.Y)
            {
                return false;
            }
            else if (a.Y == b.Y)
            {
                if (a.X <= b.X)
                {
                    return true;
                }
                else//(a.X >= b.X)
                {
                    return false;
                }
            }
            else //(a.Y < b.Y)
            {
                return true;
            }
        }
        public static bool operator >(EditPoint a, EditPoint b)
        {
            return !(a <= b);
        }
        public static bool operator >=(EditPoint a, EditPoint b)
        {
            return !(a < b);
        }
        public static EditPoint operator +(EditPoint a, EditPoint b)
        {
            return new EditPoint(a.X + b.X, a.Y + b.Y);
        }

        #endregion
    }
    public struct DotPoint
    {
        public Point Position;
        public int X
        {
            get
            {
                return Position.X;
            }
            set
            {
                Position.X = value;
            }
        }
        public int Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                Position.Y = value;
            }
        }
        public DotPoint(int x,int y)
        {
            this.Position = new Point(x, y);
        }
        public TextAreaPoint ToTextAreaPoint(Rectangle displayRectangle)
        {
            var x = this.X - displayRectangle.Location.X;
            var y = this.Y - displayRectangle.Location.Y;
            return new TextAreaPoint() { X=x,Y=y};
        }
        /*
        public DisplayPoint ToDisplayPoint(DisplaySetting DisplaySet)
        {
            var x = this.X - DisplaySet.TextAreaMapLocation.X + DisplaySet.Padding.Left;
            var y = this.Y - DisplaySet.TextAreaMapLocation.Y + DisplaySet.Padding.Top;
            return new DisplayPoint(){X=x,Y=y};
        }
        */
    }
    public struct TextAreaPoint
    {
        private Point Position;
        public int X
        {
            get
            {
                return Position.X;
            }
            set
            {
                Position.X = value;
            }
        }
        public int Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                Position.Y = value;
            }
        }
    }
    public struct DisplayPoint
    {
        private Point Position;
        public int X
        {
            get
            {
                return Position.X;
            }
            set
            {
                Position.X = value;
            }
        }
        public int Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                Position.Y = value;
            }
        }
    }


    /// <summary>
    /// 定義：StringPointとは
    /// 二次元テキスト上の位置
    /// </summary>
    /// 
}
