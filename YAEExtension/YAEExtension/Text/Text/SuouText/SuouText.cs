﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase.Text;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using System.Xml.Linq;
using SuouBase.Interface;
using WebExtension;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Xml;
using System.ComponentModel;
using SuouBase.Text.Search;
namespace SuouBase.Text
{
    [Serializable]
    [ReadOnly(true)]
    public partial class SuouText:IParse,IXmlSerializable
    {
        #region BaseProperty
        private List<SuouLine> _Lines;
        public List<SuouLine> Lines
        {
            get { return _Lines; }
            private set
            {
                if (value == null) throw new ArgumentNullException();
                _Lines = value;
            }
        }
        public virtual int YInterval { get; set; }
        private char _Color;
        private char _Font;
        public virtual Color Color
        {
            get { return FontColorDictionary.GetColor(this._Color); }
            set
            {
                this._Color = FontColorDictionary.RegisterColor(value);
            }
        }
        public virtual Font Font
        {
            get { return FontColorDictionary.GetFont(this._Font); }
            set
            {
                this._Font = FontColorDictionary.RegisterFont(value);
            }
        }
        public string[] LinesText
        {
            get
            {
                return this.Lines.Select((v) => v.Sentence).ToArray();
            }
        }
        public string Text
        {
            get
            {
                StringBuilder r = new StringBuilder("");
                int n = 0;

                if (Lines == null) throw new MyException("");
                while (n < Lines.Count)
                {
                    r.Append(Lines[n++].Sentence);
                    if (n != Lines.Count) r.Append(System.Environment.NewLine);
                }
                return r.ToString();
            }
            set
            {
                if (value == null) throw new MyException("");
                this.Initialize(value);
            }
        }
        #endregion
        #region Property
        [NonSerialized]
        private SuouLine _MaxWidthLine;
        private SuouLine MaxWidthLine
        {
            get
            {
                if (_MaxWidthLine == null) this.ReSize();
                return _MaxWidthLine;
            }
            set
            {
                this._MaxWidthLine = value;
            }
        }
        [NonSerialized]
        private int _Height = -1;
        public int Height
        {
            get
            {
                if (this.Count == 0) return 0;
                if (_Height == -1) { this.ReHeight(); }
                return _Height;
            }
            protected set
            {
                _Height = value;
            }
        }
        public IEnumerator<SuouLine> GetEnumerator()
        {
            for (int i = 0; i < Lines.Count(); i++)
                yield return Lines[i];
        }
        public virtual SuouLine this[int i]
        {
            get
            {
                if (i < 0)
                {
                    throw new MyException("負の引数です");
                }
                else if (i < this.Count)
                {

                    return this.Lines[i];
                }
                else
                {
                    return SuouLine.Empty(this.Font, this.Color);
                }
            }
            set
            {
                if (i < 0)
                {
                    throw new MyException("負の引数です");
                }
                else if (i < this.Count)
                {
                    this.Replace(i, value);
                }
                else
                {
                    this.Replace(i, value);
                }

            }
        }
        #endregion
        #region ReadProperty
        public StringPoint TailLocation
        {
            get 
            {
                var y = this.Count - 1;
                var x = this[y].Length;
                return new StringPoint(x, y);
            }
        }
        public EditPoint LastLocation
        {
            get
            {
                var y = this.Count-1;
                var x = this[y].Width;
                var r = new EditPoint(x,y);
                return r;
            }
        }
        public virtual void MakeBottomLine(int n)
        {
            while (n >= this.Count) this.AddLine(SuouLine.Empty(this.Font,this.Color));
        }
        public bool IsComputeWidth
        {
            get
            {
                return this._MaxWidthLine != null;
            }
        }
        public Size Size
        {
            get
            {
                return new Size(Width, Height);
            }
        }
        public int Width
        {
            get
            {
                if (this.Count == 0) return 0;
                return MaxWidthLine.Width;
            }
        }
        public int Length
        {
            get { return this.Lines.Sum((v) => v.Length); }
        }
        #endregion
        #region Operarion

        protected virtual void ReplaceResize(SuouLine preline,SuouLine newline)
        {
            if (preline == MaxWidthLine)
            {
                if (newline.Width > preline.Width)
                {
                    MaxWidthLine = newline;
                }
                else
                {
                    MaxWidthLine = null;
                }
            }
            else
            {
                if (newline.Width > MaxWidthLine.Width) MaxWidthLine = newline;
            }
        }
        protected virtual void InsertResize(SuouLine newline)
        {
            if (this.IsComputeWidth)
            {
                if (newline.Width > MaxWidthLine.Width) MaxWidthLine = newline;
            }
            this.Height += newline.Height + this.YInterval;
        }
        protected virtual void AddResize(SuouLine newline)
        {
            if (this.IsComputeWidth)
            {
                if (newline.Width > MaxWidthLine.Width) MaxWidthLine = newline;
            }
            this.Height += newline.Height + this.YInterval;
        }
        protected virtual void DeleteResize(SuouLine deleteline)
        {
            if (deleteline == MaxWidthLine) MaxWidthLine = null;
            this.Height -= (deleteline.Height + this.YInterval);
        }

        protected virtual void InsertLine(int n, SuouLine line)
        {
            this.Lines.Insert(n, line);
            InsertResize(line);
        }
        public bool IsOutOfIndex(int n)
        {
            return n < 0 || n >= this.Count;
        }
        public void DeleteLine(int n)
        {
            var line = this[n];
            this.Lines.RemoveAt(n);
            DeleteResize(line);
        }
        protected virtual void InsertRangeLine(int n, IEnumerable<SuouLine> lines)
        {
            this.Lines.InsertRange(n, lines);
            lines.ToList().ForEach((v) => InsertResize(v));
        }
        protected virtual void AddLine(SuouLine line)
        {
            this.Lines.Add(line);
            InsertResize(line);
        }

        public bool ContainDotLocation(EditPoint p)
        {
            var a = p.Y >= 0 && p.Y < this.Count;
            if (a)
            {
                var len = this[p.Y].Width;
                if (p.X >= 0 && p.X < len)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        public void Decode()
        {
            this.Text = this.Text.DecodeToSjis();
            
        }
        public bool CheckHasTransparentItem()
        {
            foreach (var line in this.Lines)
            {
                foreach (var w in line.Words)
                {
                    if (w.IsTransparent) return true;
                }
            }
            return false;
        }
        /// <summary>
        /// 各行のワードが高々一つからなるときにのみTrueを返します。
        /// </summary>
        /// <returns></returns>
        public bool CheckAllSimpleLine()
        {
            var rst = this.Lines.All((v) => v.Words.Count <= 1);
            
            return rst;
        }

        #region Insert

        private StringPoint InsertParagraph(StringPoint InsertLocation)
        {
            var LeftRight = Lines[InsertLocation.Y].Split(InsertLocation.X);
            this[InsertLocation.Y] = LeftRight.Left;
            this.InsertLine(InsertLocation.Y + 1, LeftRight.Right);
            return new StringPoint(0,InsertLocation.Y + 1);
        }
        public StringPoint Insert(StringPoint InsertLocation, SuouText text)
        {
            
            var InsertText = text.Clone();
            var SplitLine = this.Lines[InsertLocation.Y].Split(InsertLocation.X);

            InsertText.Insert(StringPoint.Empty, SplitLine.Left);
            InsertText.Insert(InsertText.TailLocation, SplitLine.Right);
            this.Lines.RemoveAt(InsertLocation.Y);
            this.InsertRangeLine(InsertLocation.Y, InsertText.Lines);
            
            var x = InsertText.Count == 1 ? text[0].Length + SplitLine.Left.Length : text.LastLine.Length;
            var y = InsertLocation.Y + text.Count - 1;
            return new StringPoint(x, y);

        }

        public Size LooseSize
        {
            get
            {
                return new Size(SuouEditorConst.LooseWidth,this.Height);
            }
        }
        public SuouLine LastLine
        {
            get { return this.Lines[this.Count - 1].Clone(); }
        }

        public StringPoint Insert(StringPoint InsertLocation, SuouLine sentence)
        {
            this[InsertLocation.Y] = this[InsertLocation.Y].Insert(InsertLocation.X, sentence);
            return new StringPoint(InsertLocation.X + sentence.Length, InsertLocation.Y);
        }
        public virtual StringPoint Replace(int y, SuouLine sentence)
        {
            var preline = this[y];
            this.MakeBottomLine(y);
            this.Lines[y] = sentence;
            this.ReplaceResize(preline, sentence);
            return new StringPoint(sentence.Length, y);
        }
        
        public virtual void ReSize(){
            if (this.Lines.Count != 0)
            {
                var maxwidthline = this.Lines.Max2((v) => v.Width);
                if (maxwidthline == null) throw new MyException("");
                this.MaxWidthLine = maxwidthline;
                this.Height = this.Lines.Sum((v) => v.Height + this.YInterval);
            }
            else
            {
                this.Height = 0;
            }
        }
        public void ReHeight()
        {
            if (this.Lines.Count != 0)
            {
                this.Height = this.Lines.Sum((v) => v.Height + this.YInterval);
            }
            else
            {
                this.Height = 0;
            }
        }
        
        #endregion

        #region Delete

        public void DeleteParagraph(int n)
        {
            this[n - 1] += this[n];
            DeleteLine(n);
        }
        public void Delete(Band d)
        {
            this.Delete(d.Location, d.Length);
        }
        public void Delete(StringPoint DeleteLocation, int len)
        {
            if (len < 0)
            {
                this.Delete(new StringPoint(DeleteLocation.X + len, DeleteLocation.Y), -len);
            }
            else
            {
                this[DeleteLocation.Y] = this[DeleteLocation.Y].Delete(DeleteLocation.X, len);
            }
            //NeedResize = true;
        }

        public void DeleteALL()
        {
            this.Initialize("");
        }
        public bool IsMaxWidthLine(SuouLine line)
        {
            return this.MaxWidthLine == line;
        }

        private void ReplaceIterator(Func<int, SuouLine> func)
        {
            for (int i = 0; i < this.Count; i++)
            {
                var v = func(i);
                if (v != null) this[i] = v;
            }
        }
        private void Replace(char tag,Func<SuouLine,SuouLine> func)
        {
            Func<int, SuouLine> t = (i) => this.Replace(tag, i, func);
            this.ReplaceIterator(t);
        }
        public void Replace(char tag, BasicLineMerge lm,ISpaceMaker sm)
        {
            var lines = lm.GetLineEnumerator().ToList();
            int m = 0;
            Func<SuouLine, SuouLine> t = (line) =>
            {
                    if (m < lines.Count)
                    {
                        return lines[m++].Tagize(tag);
                    }
                    else
                    {
                        return sm.CreateSpaceCarelessly(line.Width).Tagize(tag);
                    }
                };
            this.Replace(tag, t);
        }
        private SuouLine Replace(char tag,int i,Func<SuouLine,SuouLine> func)
        {
            var tripleLine = this[i].TripleTagSplit(tag);
            if (tripleLine.Center.Length != 0)
            {
                tripleLine.Center = func(tripleLine.Center);
                return tripleLine.Join();
            }
            return null;

        }

        public MatchedReplaceItems Search(LineMergeCollection lmc,char tag,ISpaceMaker spaceMaker)
        {
            var r = new MatchedReplaceItems(this);
            var p = lmc.Select((v) => this.Find(v,spaceMaker)).FirstOrDefault((v) => v.IsMatch);
            if (p == null)
            {
                return r;
            }
            else
            {
                p.CreateReplacer(lmc,tag,spaceMaker).ForEach((v)=>r.Add(v));
                return r;
            }
        }
        public MatchedTextItems Find(BasicLineMerge lm, ISpaceMaker spaceMaker)
        {
            List<MatchedTextLocation> r = new List<MatchedTextLocation>();
            var v = lm.GetPlaneEnumerator().First();
            var w = this.Find(v.Line,spaceMaker);
            foreach (var p in w)
            {
                lm.Location = p.EditLocation;
                var contains = lm.GetPlaneEnumerator().Select((plane => this.Find(plane,spaceMaker)));
                if (contains.All((b) => b != null))
                {
                    var data = new MatchedTextLocation(contains.ToArray());
                    r.Add(data);
                }
            }
            var pr = new MatchedTextItems(lm, r.ToArray(), this);
            return pr;
        }
        private MatchBand Find(StringPlaneData line,ISpaceMaker sm)
        {
            int y = line.Y;
            var pn = this[y].Sentence.FindWithoutSpace(line.Line.Sentence, sm);
            //var n = this[y].Sentence.IndexOf(line.Line.Sentence);
            if (pn.X < 0)
            {
                return null;
            }
            else
            {
                pn.Y = y;
                return pn;
            }
        }
        
        private MatchBand[] Find(SuouLine line,ISpaceMaker sm)
        {
            var word = line.Sentence;
            List<MatchBand> r = new List<MatchBand>();
            for(int y =0;y<this.Count;y++)
            {
                var pn = this[y].Sentence.FindWithoutSpace(word,sm);
                if (pn.X >= 0)
                {
                    pn.EditLocation = new EditPoint(this[y].Subline(0,pn.X).Width, y);
                    //var v = new DotBand(new EditPoint(pn.X,y),pn.Length);
                    r.Add(pn);
                }
            }
            return r.ToArray();
        }
        
        

        #endregion

        #region Constructor
        /// <summary>
        /// コピー
        /// </summary>
        /// <param name="tfc"></param>
        public SuouText(SuouText tfc)
        {
            this._Font = tfc._Font;
            this._Color = tfc._Color;
            this.YInterval = tfc.YInterval;
            this.Lines = new List<SuouLine>();
            this.Initialize(tfc);
            
        }
        public SuouText():this(SuouEditorConst.AAFont,Color.Black)
        {
            if (this.Lines == null) throw new MyException("");
        }
        public SuouText(List<SuouLine> lines)
            : this(SuouEditorConst.AAFont, Color.Black)
        {
            this.Lines = lines;
        }
        public SuouText(string s)
            : this(SuouEditorConst.AAFont, Color.Black,s,SuouEditorConst.AAFontYInterval)
        {
        }
        public SuouText(Font f, Color c)
            : this(f, c, "", SuouEditorConst.AAFontYInterval)
        {
            
        }
        public SuouText(Font f, Color c, IEnumerable<SuouLine> lines):this(f,c,lines,SuouEditorConst.AAFontYInterval)
        {
        }
        public SuouText(Font f, Color c, IEnumerable<SuouLine> lines, int yinterval)
        {
            this.Lines = new List<SuouLine>();
            this.Font = f;
            this.Color = c;
            this.YInterval = yinterval;
            this.Initialize(lines);
        }
        public SuouText(Font f, Color c,string s,int yinterval)
        {
            if (f == null) throw new MyException("font is null");
            this.Lines = new List<SuouLine>();
            this.Font = f;
            this.Color = c;
            this.YInterval = yinterval;
            this.Initialize(s);

        }
        #endregion

        #region BangMethod
        public void ClearSize()
        {
            this.MaxWidthLine = null;
            this.Height = -1;
        }
        public virtual void Initialize(string text)
        {
            this.Lines.Clear();
            this.ClearSize();
            if (text != null)
            {
                var p = text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
                p.ForEach((v) => this.AddLine(new SuouLine(this.Font, this.Color, v)));
            }
            //ReSize();
        }
        public virtual void Initialize(IEnumerable<SuouLine> lines)
        {
            this.Lines.Clear();
            this.ClearSize();
            if (lines != null)
            {
                lines.ForEach((v) => this.AddLine(v));
            }
            //ReSize();
        }
        public virtual void Initialize(SuouText text)
        {
            if (text.Count == 0) throw new Exception();
            this.Lines.Clear();
            this.ClearSize();
            text.Lines.ForEach(
                (v) => 
                {
                    var line = v.Clone();
                    this.AddLine(line);
                }
                );
            //ReSize();
        }
        public void Add(SuouLine lc)
        {
            this.AddLine(lc);
        }
        public void Add(string text)
        {
            var v = new SuouLine(this.Font,this.Color,text);
            this.AddLine(v);
        }
        public void AddRange(SuouLine[] lcs)
        {
            lcs.ForEach((v) => this.AddLine(v));
        }
        #endregion

        #region NoBangMethod
        public string ConvertString()
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < Lines.Count(); i++)
            {
                s.Append(Lines[i].Sentence);
                if (i == Lines.Count() - 1) s.Append(System.Environment.NewLine);
            }
            return s.ToString();
        }
        public SuouText Clone()
        {
            return new SuouText(this);
        }
        public int Count
        {
            get { return this.Lines.Count; }
        }

        public static SuouText Empty()
        {
            var r = new SuouText();
            return r;
        }
        public static SuouText Empty(SuouText text)
        {
            var r = new SuouText(text.Font,text.Color);
            return r;
        }
        public static SuouText Empty(int height)
        {
            var r = new SuouText();
            for (int i = 0; i < height; i++)
            {
                r[i] = SuouLine.Empty(r.Font,r.Color);
            }
            return r;
        }
        /*
        public static SuouText BaseEmpty(SuouText text)
        {
            var r = new SuouText(text.Font, text.Color,null);
            return r;
        }
        */
        public override string ToString()
        {
            return this.Text;
            
        }
        #endregion

        #region OtherMethod
        public List<string> ToListString()
        {
            var r = new List<string>();
            foreach (var v in this.Lines) r.Add(v.ToString());
            return r;
        }
        public int GetSitarabaByteSize
        {
            get
            {
                var x = this.Lines.Sum((v) => v.Sentence.GetSitarabaByte());
                x += (this.Lines.Count - 1) * 4;
                return x;
            }
        }
        public bool IsRectangle()
        {
            var n = this.Lines.First().Width;
            return this.Lines.All((v) => v.Width == n);
        }
        public bool IsEqualBlock(SuouText b)
        {
            if (!this.IsRectangle() || !b.IsRectangle()) return false;
            return this.Count == b.Count && this.Width == b.Width;
        }
        public Bitmap ToImage(int yspace, Color Background)
        {
            return BitmapExpansion.CreateBitmapRapid(this, yspace, Background);
        }
        public Size GetSize(int yspace)
        {
            var h = this.Lines.Sum((v) => v.Height) + yspace * this.Count;
            return new Size(this.Width, h);
        }
        public Size GetDrawStringSize(int yspace)
        {
            var h = this.Lines.Sum((v) => v.Height) + yspace * this.Count;
            return new Size(this.Width+this.Width/20, h);
        }
        public int LooseWidth
        {
            get { return SuouEditorConst.LooseWidth; }
        }
        public Size GetLooseSize(int yspace)
        {
            var h = this.Lines.Sum((v) => v.Height) + yspace * this.Count;
            return new Size(this.LooseWidth, h);
        }
        public static bool EqualRectangle(IEnumerable<SuouText> texts)
        {
            var f = texts.All((v)=>v.IsRectangle());
            var basetext = texts.First();
            var size = new Size(basetext.Width,basetext.Count);
            var g = texts.All((v)=>v.Width == size.Width && v.Count == size.Height);
            return f && g;
        }
        public SuouText CreateTimesRectangle(int WidthTimes,int HeightTimes)
        {
            if (HeightTimes == 0) throw new MyException("");
            var fEmpty = SuouText.Empty(this);
            var f = WidthTimes.Fold((v) => v += this, fEmpty.Clone());
            var g = (HeightTimes-1).Fold((v) => v % f, f);
            return g;
        }
        public SuouText CreateRectangle(int Height)
        {
            if (!this.IsRectangle()) throw new Exception();
            var f = this.Clone();
            while (f.Count < Height) f.Add(f[0]);
            return f;
        }
        public static SuouText operator +(SuouText a, SuouText b)
        {
            var r = SuouText.Empty(a);
            int n = System.Math.Max(a.Count, b.Count);
            n.ForEach((i) => r[i] = a[i] + b[i]);
            return r;
        }
        public static SuouText operator %(SuouText a, SuouText b)
        {
            var r = a.Clone();
            b.Count.ForEach((i) => r.Add(b[i]));
            return r;
        }

        
        public string NotifyPageSizeMessage()
        {
            var s = this.GetSitarabaByteSize.ToString() + "Byte(したらば)";

            return s;

        }
        public string CreateLightInformation()
        {
            var s = string.Format("{0}*{1},{2}", this.Width, this.Height, this.NotifyPageSizeMessage());
            return s;
        }

        public IEnumerable<SuouWord> WordIterator()
        {
            foreach (var v in this.Lines)
            {
                foreach (var w in v.Words)
                {
                    yield return w;
                }
            }
        }

        public SuouText EdgeSpaceTransparentize(ISpaceMaker spacemaker)
        {
            var f = this.Clone();
            for (int i = 0; i < f.Count; i++)
            {
                f[i] = f[i].EdgeSpaceTransparentize(spacemaker);
            }
            return f;
        }
        public void NoTagize()
        {
            for (int i = 0; i < this.Count; i++)
            {
                this[i] = this[i].Tagize((char)0);
            }
        }
        public BasicLineMerge ToLineMerge(ISpaceMaker spaceMaker,bool isTrimSpace)
        {
            //var NewText = spaceTransparent ? this.EdgeSpaceTransparentize(spaceMaker) : this.Clone();
            var NewText = this.Clone();
            System.Console.WriteLine(isTrimSpace);
            if (!isTrimSpace)
            {
                var f = BasicLineMerge.Create(NewText, null);
                return f;
            }
            else
            {
                var f = BasicLineMerge.Create(NewText,spaceMaker);
                return f;
            }
        }
        #endregion
        #region Static
        public static SuouText Create(IEnumerable<SuouLine> lines)
        {
            var v = new SuouText(SuouBase.SuouEditorConst.AAFont, SuouBase.SuouEditorConst.TextColor, lines);
            return v;
        }
        public static SuouText Create(int width, int height, ISpaceMaker spacer)
        {
            var lines = new List<SuouLine>();
            var line = spacer.CreateSpace(width);
            height.ForEach((w) => lines.Add(line.Clone()));

            var v = new SuouText(SuouBase.SuouEditorConst.AAFont, SuouBase.SuouEditorConst.TextColor, lines);
            return v;
        }
        public static SuouText Create(int count)
        {
            var lines = new List<SuouLine>();
            count.ForEach((w) => lines.Add(new SuouLine()));

            var v = new SuouText(SuouBase.SuouEditorConst.AAFont, SuouBase.SuouEditorConst.TextColor, lines);
            return v;
        }

        #endregion
        #region XML
        public static string ParserName
        {
            get
            {
                return "SuouText";
            }
        }
        
        /*
        private XElement CreateXElement()
        {
            if (this.IsNaive)
            {
                return this.CreateEasyXElement();
            }
            else
            {
                return this.CreateSerializer();
            }
        }
        */
        private XElement CreateStandardSerializer()
        {
            XElement xe = new XElement(ParserName);
            xe.Add(new XAttribute("IsNaive", this.IsNaive));
            xe.Add(XMLParse.GetXMLFromColor(this.Color));
            xe.Add(XMLParse.GetXMLFromFont(this.Font));
            var lines = new XElement("Lines");
            Console.WriteLine("CreateSerializer");

            this.Lines.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => lines.Add(v));
            xe.Add(lines);
            return xe;

        }

        public XElement CreateSerializer()
        {
            if (this.IsNaive)
            {
                return this.CreateEasyXElement();
            }
            else
            {
                return this.CreateStandardSerializer();
            }

        }


        private XElement CreateEasyXElement()
        {
            XElement xe = new XElement(ParserName);

            xe.Add(new XAttribute("IsNaive", this.IsNaive));
            //this.Lines.Select((v) => new XElement("SuouLine", v.Sentence)).ForEach((v)=>xe.Add(v));
            //this.Lines.Select((v) => v.CreateEasyXElement()).ForEach((v) => xe.Add(v));
            xe.Add(this.XText);
            return xe;
        }
        public XCData XText
        {
            get
            {
                
                return new XCData(this.Text);
            }
        }
        

        public bool IsNaive
        {
            get
            {
                return this.Lines.All((v) => v.IsNaive);
            }
        }
        #endregion
        #region IXMLSerialize
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            this.CreateSerializer().WriteTo(writer);
        }
        public void ReadXml(System.Xml.XmlReader reader)
        {

            var v = XElement.Load(reader);
            var p = SuouTextParser.Parse2(v.Element("SuouText"));
            this.Lines = p.Lines;
        }
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }
        #endregion

        private int GetHIndex(int y,int yspace)
        {
            if (y < 0) return 0;
            int t = 0;
            int h = 0;
            foreach (var v in this.Lines)
            {
                if (h <= y && y < h + v.Height + yspace)
                {
                    return t;
                }
                h += v.Height;
                t++;
            }
            return t;
        }
        public EditPoint TranslatePoint(Point p,int yspace)
        {
            return new EditPoint(p.X, this.GetHIndex(p.Y, yspace));
        }
        public Point TranslateEditPoint(EditPoint p,int yspace)
        {
            int h = 0;
            int t = 0;
            foreach(var v in this.Lines)
            {
                if (t >= p.Y) break;
                h += this.Lines[t++].Height;
                h += yspace;
            }
            return new Point(p.X, h);
        }
    }

    public enum EncodeType
    {
        UTF8, SJIS10, SJIS16
    }
}
