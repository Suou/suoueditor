﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase.Text;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using System.Xml.Linq;
using SuouBase.Interface;
using WebExtension;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Xml;
using System.ComponentModel;
using SuouBase.Text.Search;
namespace SuouBase.Text
{
    class SuouTextParser
    {
        public static SuouText Parse1(XElement xe)
        {
            if (xe == null) return SuouText.Empty();

            var p = xe.AttributeBoolValue("IsNaive");
            if (!p)
            {
                return NormalParse(xe);
            }
            else
            {
                return EasyParse1(xe);
            }
        }
        public static SuouText Parse2(XElement xe)
        {
            if (xe == null) return SuouText.Empty();

            var isNative = xe.AttributeBoolValue("IsNaive");
            SuouText txt = null;
            if (!isNative)
            {
                txt = NormalParse(xe);
            }
            else
            {
                txt = EasyParse2(xe);
            }
            return txt;
        }

        private static SuouText NormalParse(XElement xe)
        {
            var fontElement = xe.Element("Font");
            if(fontElement == null)
            {
                throw new Exception("Something wrong.");
            }
            Font f = XMLParse.GetFont(fontElement);
            Color c = XMLParse.GetColor(xe.Element("Color"));
            var lines = xe.Element("Lines").Elements().Select((v) => SuouLine.Parse(v)).ToArray();
            var st = new SuouText(f, c, lines);
            return st;

        }
        private static SuouText EasyParse1(XElement xe)
        {
            var lines = ParseXCdata1((XCData)xe.FirstNode).ToList();
            
            var p = new SuouText(lines);
            return p;
        }
        private static SuouText EasyParse2(XElement xe)
        {
            var lines = ParseXCdata2((XCData)xe.FirstNode).ToList();
            var p = new SuouText(lines);
            return p;
        }
        private static IEnumerable<SuouLine> ParseXCdata1(XCData x)
        {
            var lines = x.Value.LineSprit().Skip(1);
            return lines.Select((v) => new SuouLine(v));
        }
        private static IEnumerable<SuouLine> ParseXCdata2(XCData x)
        {
            var lines = x.Value.LineSprit();
            return lines.Select((v) => new SuouLine(v));
        }
    }
}
