﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using BSystem.Collections;
using BSystem.Drawing;
namespace SuouBase.Text
{
    public partial class SuouText
    {
        public uint Hash
        {
            get
            {
                uint p = 0;
                this.Lines.ForEach((v) => p = p ^ v.Hash);
                return p;
            }
        }
    }
    public static class SuouTextExtension
    {
        public static Size TextSize(this SuouText f, int yinterval)
        {
            return f.Font.TextSize(f.ToListString(), yinterval);
        }
        public static SuouText MoveLeft(this SuouText text,ISpaceMaker spacer)
        {
            
            var p = text.Lines.Select((v) => v.SpaceSplit(spacer, SpaceSplitOption.LeftSpace));
            var widthList = p.Select((v) => v.Left.Width == 0 ? -1 : v.Left.Width).ToArray();
            var min = widthList.Where((v)=> v != -1).Min((v)=>v);
            var widthList2 = widthList.Select((v) => v == -1 ? -1 : v - min).ToArray();
            while (true)
            {
                if (widthList2.All((v) => v == -1 ? true : spacer.CanCreateSpace(v)))
                {
                    break;
                }
                else
                {
                    widthList2 = widthList2.Select((v) => v == -1 ? -1 : v + 1).ToArray();
                }
            }
            widthList2 = widthList2.Select((v) => v == -1 ? 0 : v).ToArray();
            var g = p.Zip(widthList2, (a, b) => spacer.CreateSpace(b) + a.CenterRight);
            var r = new SuouText(text.Font, text.Color, g);
            return r;
        }
        public static SuouText DeleteRight(this SuouText text, ISpaceMaker spacer)
        {
            var v = text.Clone();
            for (int i = 0; i < v.Count; i++)
            {
                v[i] = v[i].SpaceSplit(spacer, SpaceSplitOption.RightSpace).LeftCenter;
            }
            return v;
        }
        public static SuouText MergeRectangle(this SuouText text, SuouText bText, AltText spacer)
        {
            var min = text.Count < bText.Count ? text : bText;
            var max = text.Count < bText.Count ? bText : text;
            min = spacer.MakeRectangleText(min, new Size(min.Width, max.Count));
            var left = max == text ? max:min;
            var right = max == text ? min : max;
            return left + right;
        }
        public static SuouText InsertEachSpaceLine(this SuouText text,int len)
        {
            if (len == 0) return text.Clone();
            var v = SuouText.Empty();
            for (int i = 0; i < text.Count; i++)
            {
                v.Add(text[i]);
                if(i != text.Count-1)v %= CreateEmptyLines(text, len);
            }
            return v;
        }
        private static SuouText CreateEmptyLines(SuouText text, int len)
        {
            var v = new SuouText(text.Font,text.Color);
            for (int i = 1; i < len; i++)
            {
                v.Add(SuouLine.Empty(text.Font,text.Color));
            }
            return v;
        }
        
        public static SuouText Taterize(this SuouLine text,int height)
        {
            var s = text.Sentence.Select((v) => v);
            var s2 = string.Join(System.Environment.NewLine, s);
            var s3 = new SuouText(s2);

            Enumerable.Range(0, height - text.Length).ForEach((v) => s3.Add(SuouLine.Empty(s3.Font,s3.Color)));
            return s3;
        }
    }
}
