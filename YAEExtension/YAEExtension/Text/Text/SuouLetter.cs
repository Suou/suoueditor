﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using BSystem.Drawing;
namespace SuouBase.Text
{
    public struct SuouMiniLetter
    {
        public char Letter;
        public Color Color { get; set; }
        public Color BackColor { get; set; }
        public bool IsTransparent { get; set; }

        public char Height { get; set; }

        public char Width { get; set; }
        
        public int Length
        {
            get { return 1; }
        }
        /*
        public SuouMiniLetter()
        {

            this.Letter = (char)0;
            this.Height = (char)0;
            this.Width = (char)0;
            this.Color = Color.Empty;
            this.BackColor = Color.Empty;
            this.IsTransparent = false;
        }
        */
        public SuouMiniLetter(char w,Font f,Color c,Color Backc,bool Istransparent):this()
        {
            Letter = w;
            Height = (char)f.Height;
            Width = (char)f.Width(w);
            Color = c;
            BackColor = Backc;
            IsTransparent = Istransparent;
        }
    }

    [Serializable]
    public class SuouLetter
    {

        #region Property
        public char Letter;

        public Color Color { get; set; }
        public Font Font { get; set; }
        public Color BackColor { get; set; }
        public bool IsTransparent { get; set; }
        public int Height
        {
            get { return Size.Height; }
        }
        public int Width
        {
            get { return Size.Width; }
        }
        public int Length
        {
            get { return 1; }
        }
        public Size Size
        {
            get
            {
                return this.Font.WordSize(this.Letter);
            }
        }
        #endregion
        
        public SuouLetter()
        {

        }
        public SuouLetter(char w):this(w,SuouEditorConst.AAFont,SuouEditorConst.TextColor,Color.White,false)
        {

        }
        public SuouLetter(char w,Font f,Color c,Color Backc,bool Istransparent)
        {
            this.Letter = w;
            this.Font = f;
            this.Color = c;
            this.BackColor = Backc;
            this.IsTransparent = Istransparent;
        }
        public SuouWord ToWord()
        {
            var w = new SuouWord(this.Font, this.Color, this.Letter.ToString(),this.BackColor,this.IsTransparent,(char)0);
            return w;
        }
        public SuouWord CreateWord(int len)
        {
            StringBuilder sb = new StringBuilder("");
            len.ForEach((v) => sb.Append(Letter));
            return new SuouWord(this.Font,this.Color, sb.ToString(),this.BackColor,this.IsTransparent,(char)0);
        }
        public override bool Equals(object obj)
        {
            if (!(obj is SuouLetter)) return false;
            var objm = (SuouLetter)obj;
            var a = this.Font.Name == objm.Font.Name && this.Font.SizeInPoints == objm.Font.SizeInPoints;
            var b = this.Color == objm.Color;
            var c = this.Letter == objm.Letter;
            var d = this.IsTransparent == objm.IsTransparent && this.BackColor == objm.BackColor;
            return a && b && c && d;
        }
        public override int GetHashCode()
        {
            return this.Font.GetSuperHashCode() ^ this.Color.GetHashCode() ^ this.Letter ^ this.BackColor.GetHashCode() ^ this.IsTransparent.GetHashCode();
        }
        public SuouLetter Clone()
        {
            return new SuouLetter(this.Letter, (Font)this.Font.Clone(), this.Color, this.BackColor, this.IsTransparent);
        }
    }
}
