﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase.Text;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using System.Xml.Linq;

namespace SuouBase.Text
{
    class FontColorDictionary
    {
        static Dictionary<char, Font> FontDic = new Dictionary<char, Font>() {};
        static Dictionary<char, Color> ColorDic = new Dictionary<char, Color>() { };
        static char FontChar = (char)1;
        static char ColorChar = (char)1;
        public static char RegisterFont(Font f)
        {
            if (FontDic.ContainsValue(f))
            {
                foreach (var v in FontDic)
                {
                    if (v.Value.Equals(f)) return v.Key; 
                }
                throw new MyException("");
            }
            else
            {
                FontDic[FontChar] = f;
                return FontChar++;
            }
        }
        public static Font GetFont(char c)
        {
            if (c == 0) return SuouEditorConst.KoteiAAFont;
            if (!FontDic.ContainsKey(c)) FontDic[c] = SuouEditorConst.AAFont;
            return FontDic[c];
        }
        public static char RegisterColor(Color c)
        {
            if (ColorDic.ContainsValue(c))
            {
                foreach (var v in ColorDic)
                {
                    if (v.Value == c) return v.Key;
                }
                throw new MyException("");
            }
            else
            {
                ColorDic[ColorChar] = c;
                return ColorChar++;
            }
        }
        public static Color GetColor(char c)
        {
            if (c == 0) return SuouEditorConst.TextColor;
            if (!ColorDic.ContainsKey(c)) ColorDic[c] = SuouEditorConst.TextColor;
            return ColorDic[c];
        }

    }
}
