﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text
{
    public class SubLineDictionary
    {
        public Dictionary<KeyValuePair<int, int>, SuouLine> Dic = new Dictionary<KeyValuePair<int, int>, SuouLine>();
        public Dictionary<int, ThreeSplitLine> SpaceDic = new Dictionary<int, ThreeSplitLine>();

        public SubLineDictionary()
        {
            Dic = new Dictionary<KeyValuePair<int, int>, SuouLine>();
        }
        public SubLineDictionary(Dictionary<KeyValuePair<int, int>, SuouLine> dic, Dictionary<int, ThreeSplitLine> spaceDic)
        {
            Dic = new Dictionary<KeyValuePair<int, int>, SuouLine>(dic);
            this.SpaceDic = new Dictionary<int, ThreeSplitLine>(spaceDic);
        }
 

        public bool ContainSubLine(int Index,int Length)
        {
            var kv = new KeyValuePair<int, int>(Index, Length);
            return this.Dic.ContainsKey(kv);
        }
        public SuouLine GetSubLine(int Index, int Length)
        {
            var kv = new KeyValuePair<int, int>(Index, Length);
            return this.Dic[kv].Clone();
        }
        public void Register(int Index, int Length, SuouLine line)
        {
            var kv = new KeyValuePair<int, int>(Index, Length);
            this.Dic[kv] = line.Clone();
        }
        public SubLineDictionary Copy()
        {
            return new SubLineDictionary(this.Dic,this.SpaceDic);
        }
        public int Count
        {
            get
            {
                return this.Dic.Count;
            }
        }
        public bool ContainSpace(int Length)
        {
            return this.SpaceDic.ContainsKey(Length);
        }
        public ThreeSplitLine GetSpaceSplit(int LineLength)
        {
            return this.SpaceDic[LineLength].Copy();
        }
        public void Register(int Length, ThreeSplitLine Lines)
        {
            this.SpaceDic[Length] = Lines.Copy();
        }
    }
}
