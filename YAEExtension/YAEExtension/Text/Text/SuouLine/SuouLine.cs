﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using SuouBase;
using SuouBase.Text;
using System.Xml.Linq;
using System.Diagnostics;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using SuouBase.Interface;
using SuouBase.Drawing;
namespace SuouBase.Text
{

    [Serializable]
    public partial class SuouLine:IParse
    {
        #region BaseProperty
        private char _Color;
        private char _Font;

        public List<SuouWord> Words
        {
            get { return _Words; }

            protected set
            {
                if (value == null) throw new ArgumentNullException();
                _Words.Clear();
                foreach (SuouWord wc in value) Words.Add(wc);
                //_Words = value;
                //ReMakeText();
            }

        }
        //readonly private List<SuouWord> _Words;
        readonly private List<SuouWord> _Words = new List<SuouWord>();
        #endregion
        #region Property
        public Color Color
        {
            get { return FontColorDictionary.GetColor(this._Color); }
            set
            {
                this._Color = FontColorDictionary.RegisterColor(value);
            }
        }
        public Font Font
        {
            get { return FontColorDictionary.GetFont(this._Font); }
            set
            {
                this._Font = FontColorDictionary.RegisterFont(value);
            }
        }
        /*
        [NonSerialized]
        private SubLineDictionary _SubLineDic;
        public SubLineDictionary SubLineDic
        {
            get 
            {
                if (this._SubLineDic == null) this._SubLineDic = new SubLineDictionary();
                return _SubLineDic; 
            }
            private set { _SubLineDic = value; }
        }
        */
        public SuouWord this[int i]
        {
            private set { this.Words[i] = value; }
            get { return this.Words[i]; }
        }
        
        [NonSerialized]
        private LineDotIndexes _DotIndexList;
        public LineDotIndexes DotIndexList
        {
            get
            {
                if (_DotIndexList == null) SetDotIndexes();
                return _DotIndexList;
            }
            private set
            {
                _DotIndexList = value;
            }
        }
        
        [Obsolete]//遅い
        public SuouLetter GetLetter(int n)
        {
            int i = 0;
            return this.LetterIterator().First((v) => (i++) == n);
        }
        public int GetLetterWidth(int index)
        {
            int t = 0;
            foreach (var v in this.Words)
            {
                if (t <= index && index < t + v.Length)
                {
                    return v.GetLetterWidth(index - t);
                }
                t += v.Length;
            }
            throw new Exception();
        }
        
        
        #endregion
        #region Iterator
        public IEnumerable<SuouMiniLetter> MiniPreIterator()
        {
            foreach (var v in this.Words)
            {
                foreach (var c in v.Word)
                {
                    yield return new SuouMiniLetter(c, v.Font, v.Color, v.BackColor, v.IsTransparent);
                }
            }
        }
        public IEnumerable<SuouMiniLetter> MiniSufIterator()
        {
            for (int i = this.Words.Count - 1; i >= 0; i--)
            {
                for (int j = this.Words[i].Length - 1; j >= 0; j--)
                {
                    yield return new SuouMiniLetter(this.Words[i].Word[j], this.Words[i].Font, this.Words[i].Color, this.Words[i].BackColor, this.Words[i].IsTransparent);
                }
            }
        }
        public IEnumerable<SuouLetter> LetterIterator()
        {
            foreach (var v in this.Words)
            {
                foreach (var letter in v)
                {
                    yield return letter;
                }
            }
        }
        #endregion
        #region ReadProperty
        public bool IsComputeSize
        {
            get
            {
                return this._DotIndexList != null;
            }
        }
        public int LooseWidth
        {
            get { return SuouEditorConst.LooseWidth; }
        }
        public Size LooseSize
        {
            get
            {
                return new Size(this.LooseWidth, this.Height);
            }
        }
        public int Count
        {
            get
            {
                return this.Words.Count;
            }
        }
        public int CharCount
        {
            get
            {
                return this.Words.Sum((v)=>v.Length);
            }
        }
        public int Length
        {
            get { return this.CharCount; }
        }
        public Size LineSize
        {
            get
            {
                return DotIndexList.Size;
            }
        }


        public int Width
        {
            get
            {
                return DotIndexList.Size.Width;
            }
            //protected set { DotIndexList.Size = new Size(value,this.Height); }
        }
       

        public int Height
        {
            get
            {
                if (!this.IsComputeSize)
                {
                    if (this.Words.Count == 0)
                    {
                        return this.Font.Height;
                    }
                    else
                    {
                        return this.Words.Max((v) => v.Height);
                    }
                }
                else
                {
                    return DotIndexList.Size.Height;
                }
            }
        }
        #endregion
        #region Construct

        public SuouLine(SuouWord w)
        {
            
            this.Font = w.Font;
            this.Color = w.Color;
            this.Words = new List<SuouWord>() { w };
            
            //this.SubLineDic = new SubLineDictionary();
            //SetSize();
            if (this.Font == null) throw new ArgumentNullException();
            if (this.Words == null) throw new NullReferenceException();
            //if (this.DotIndexList == null) throw new NullReferenceException();
        }
        
        public SuouLine(SuouLine line)
        {
            List<SuouWord> f = new List<SuouWord>();
            foreach (var v in line.Words) f.Add(v.Clone());


            this._Font = line._Font;
            this._Color = line._Color;
            this._Words = f;
            //this.SubLineDic = new SubLineDictionary();
            //SetSize();
            //if (this.Font == null) throw new ArgumentNullException();
            //if (this.Words == null) throw new NullReferenceException();
            //if (this.DotIndexList == null) throw new NullReferenceException();
        }
        
        public SuouLine(Font font, Color c, string text):this(FontColorDictionary.RegisterFont(font),FontColorDictionary.RegisterColor(c),text)
        {
        
        }
        public SuouLine(char font, char c, string text)
        {
            this._Words = new List<SuouWord>();
            this._Font = font;
            this._Color = c;
            this.Sentence = text;
            
            //this.SubLineDic = new SubLineDictionary();
        }
        public SuouLine(Font font, Color c, List<SuouWord> f)
        {
            this.Font = font;
            this.Color = c;
            this._Words = f;
            //this.SubLineDic = new SubLineDictionary();
            //SetSize();
            if (this.Font == null) throw new ArgumentNullException();
            if (this.Words == null) throw new NullReferenceException();
            //if (this.DotIndexList == null) throw new NullReferenceException();
        }
        public SuouLine():this(SuouEditorConst.AAFont,Color.Black,"")
        {
        }
        public SuouLine(string sentense)
            : this(SuouEditorConst.AAFontChar, SuouEditorConst.TextColorChar, sentense)
        {
        }
        #endregion
        #region Method
        protected virtual void Add(SuouWord tf)
        {
            if (Words.Count == 0)
            {
                Words.Add(tf);
            }
            else
            {
                var v = Words[Words.Count - 1];
                if (v.EqualClass(tf))
                {
                    var nv = new SuouWord(v.Font, v.Color, v.Word + tf.Word,v.BackColor,v.IsTransparent,v.Tag);
                    Words[Words.Count - 1] = nv;
                }
                else
                {
                    Words.Add(tf);
                }
            }
            
            if(this._DotIndexList != null)SetSizeCheck(tf);
        }
        protected virtual void Add(SuouLine tf)
        {
            if (Words.Count == 0)
            {
                this.Words = tf.Words;
            }
            else
            {
                var v = Words[Words.Count - 1];
                if (tf.Words.Count == 0)
                {
                }
                else if (v.EqualClass(tf.Words[0]))
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(v.Word);
                    sb.Append(tf.Words[0].Word);
                    var nv = new SuouWord(v.Font, v.Color, sb.ToString(),v.BackColor,v.IsTransparent,v.Tag);
                    Words[Words.Count - 1] = nv;
                    Words.AddRange(tf.Words.Skip(1).ToArray());
                }
                else
                {
                    Words.AddRange(tf.Words);
                }
            }

            if (this._DotIndexList != null) SetSizeCheck(tf);
        }
        
        public virtual SuouLine TrimLeft()
        {
            if (this.Length == 0) throw new Exception("長さが0です。これ以上削れません");
            var f = this.Subline(1);
            /*
            var f = this.Clone();
            if (f.Words.First().Length == 0)
            {
                f.Words.RemoveAt(0);    
            }
            f.SetSize();
            */
            return f;
        }
        
        public virtual void Clear()
        {
            Words.Clear();
            this._DotIndexList = null;
            //SetSize();
        }    
        private void Append(string s)
        {
            var w = new SuouWord(this.Font, this.Color, s);
            this.Add(w);
            //Sentence += s;
        }
        #endregion
        #region NoBangMethod
        public SuouLine Insert(int strn, SuouWord tf)
        {
            var LeftRight = this.Split(strn);
            var NewLine = LeftRight.Left + tf + LeftRight.Right;
            return NewLine;
        }
        public SuouLine Insert(int strn, SuouLine tf)
        {
            var LeftRight = this.Split(strn);
            var NewLine = LeftRight.Left + tf + LeftRight.Right;
            return NewLine;
        }
        
        public SuouLine Delete(int Index, int Length)
        {
            var thpdata = this.TripleSplit(Index, Length);
            var NewLine = thpdata.Left + thpdata.Right;
            return NewLine;
        }
        
        public SuouLine Reverse()
        {
            List<SuouWord> ws = new List<SuouWord>();
            foreach (var v in Words)
            {
                var s = v.Word.Reverse();
                var r = new SuouWord(v.Font, v.Color, s);
                ws.Add(r);
            }
            ws.Reverse();
            var rr = new SuouLine(this.Font, this.Color, ws);
            return rr;
        }
        /*
        public SuouLine ToSuouLine()
        {
            List<SuouWord> Words = new List<SuouWord>();
            foreach(var v in this.Words) Words.Add(v.ToSuouWord());
            var r = new SuouLine() { Color = this.Color, Font = this.Font, Words = Words };
            return r;
        }
        */
        #endregion

        #region XML

        private XElement CreateStandardSerializer()
        {
            XElement xe = new XElement("SuouLine");
            xe.Add(XMLParse.GetXMLFromColor(this.Color));
            xe.Add(XMLParse.GetXMLFromFont(this.Font));
            var words = new XElement("Words");
            this.Words.Where((v) => v.Word.Length > 0).Select((v) => v.CreateSerializer()).ToList().ForEach((v) => words.Add(v));
            xe.Add(words);
            return xe;
        }

        public XElement CreateSerializer()
        {
            if (this.IsNaive)
            {
                return this.CreateEasyXElement();
            }
            else
            {
                return this.CreateStandardSerializer();
            }
        }
        private XElement CreateEasyXElement()
        {
            XElement xe = new XElement("SuouLine");
            xe.Add(new XAttribute("Value", this.Sentence));
            return xe;
        }
        public static SuouLine Parse(XElement xe)
        {
            var valueAttr = xe.Attribute("Value");
            
            if (valueAttr == null)
            {
                Font f = XMLParse.GetFont(xe.Element("Font"));
                Color c = XMLParse.GetColor(xe.Element("Color"));
                var words = xe.Element("Words").Elements().Select((v) => SuouWord.Parse(v)).ToList();
                var sl = new SuouLine(f, c, words);
                return sl;
            }
            else
            {
                var str = valueAttr.Value;
                var sl = new SuouLine(str);
                return sl;

            }
        }
        public bool IsNaive
        {
            get
            {
                if (this.Words.Count != 1) return false;
                var v = this.Words[0];
                return (!v.IsTransparent && v.Tag == 0);
            }
        }
        public static SuouLine EasyParse(XElement xe)
        {
            var v = xe.Attribute("Value");
            if (v == null)
            {
                return new SuouLine();
            }
            else
            {
                return new SuouLine(v.Value);
            }
        }
        #endregion
        public bool IsInnerDotRange(int x)
        {
            return x >= 0 && x <= this.Width;
        }
        public SuouLine PaintTransparent()
        {
            var words = this.Words.Select((v) => new SuouWord(v.Font, v.Color, v.Word, v.BackColor, true,v.Tag));
            var newLine = new SuouLine(this.Font,this.Color,words.ToList());
            return newLine;
        }
        public SuouLine Tagize(char c)
        {
            var words = this.Words.Select((v) => new SuouWord(v.Font, v.Color, v.Word, v.BackColor, v.IsTransparent,c));
            var newLine = new SuouLine(this.Font, this.Color, words.ToList());

            
            

            return newLine;
        }
        public SuouLine PaintTransparent(int StartIndex,int EndIndex)
        {
            Func<SuouLine, SuouLine> t = (line) => line.PaintTransparent();
            return this.Choice(StartIndex, EndIndex-StartIndex, t);
        }
        public SuouLine Tagize(int StartIndex, int EndIndex,char c)
        {
            Func<SuouLine, SuouLine> t = (line) => line.Tagize(c);
            var r = this.Choice(StartIndex, EndIndex - StartIndex, t);

            return r;
        }
        public ThreeSplitLine Split(Func<SuouWord, bool> func)
        {
            var words = this.Clone().Words;
            var first = words.TakeWhile((v) => !func(v));
            var firstlen = first.Sum((v)=>v.Length);
            words = words.SkipWhile((v) => !func(v)).ToList();
            var second = words.TakeWhile((v) => func(v));
            var secondlen = words.Sum((v)=>v.Length);
            return this.TripleSplit(firstlen, secondlen);
        }
        public ThreeSplitLine TripleTagSplit(char tag)
        {
            Func<SuouWord, bool> func = (v) => v.Tag == tag;
            return this.Split(func);
        }

        public SuouLine PaintTransparent(int StartIndex, ISpaceMaker SpaceMaker)
        {
            Func<SuouLine, SuouLine> t = (line) => line.PaintTransparent();
            var f = this.Choice(StartIndex, SpaceMaker, t);
            return f;
        }
        public SuouLine Spacize(int startIndex, int length, ISpaceMaker SpaceMaker)
        {
            var width = this.Subline(startIndex, length).Width;
            var tripleline = this.TripleSplit(startIndex,length);
            tripleline.Center = SpaceMaker.CreateSpaceCarelessly(width);
            return tripleline.Join();
        }
    }

    
    
}
