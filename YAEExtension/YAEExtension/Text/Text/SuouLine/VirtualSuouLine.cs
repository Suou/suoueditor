﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace SuouBase.Text
{
    public class TwoVirtualSplitLine
    {
        public int EdgeLeftWidth { get; set; }
        public VirtualSuouLine Left { get; set; }
        public int CenterWidth { get; set; }
        public VirtualSuouLine Right { get; set; }
        public int EdgeRightWidth { get; set; }

        public TwoVirtualSplitLine(EdgeSpaceLineInfo left, EdgeSpaceLineInfo right)
        {
            this.EdgeLeftWidth = left.LeftWidth;
            this.Left = left.Center;
            this.CenterWidth = left.RightWidth + right.LeftWidth;
            this.Right = right.Center;
            this.EdgeRightWidth = right.RightWidth;

        }
        public TwoVirtualSplitLine(System.Drawing.Font font, System.Drawing.Color color)
        {
            this.Left = VirtualSuouLine.CreateEmpty(font, color);
            this.Right = VirtualSuouLine.CreateEmpty(font, color);
        }
        /*
        public virtual bool IsEmpty
        {
            get
            {
                return this.Left.IsEmpty && this.Right.IsEmpty;
            }
        }
        */
    }
    public class ThreeVirtualSplitLine
    {
        /*
        public override bool IsEmpty
        {
            get
            {
                return base.IsEmpty && this.Center.IsEmpty == 0;
            }
        }
        */
        public EdgeSpaceLineInfo LeftInfo { get; set; }
        public EdgeSpaceLineInfo CenterInfo { get; set; }
        public EdgeSpaceLineInfo RightInfo { get; set; }
        public int EdgeLeftWidth { get { return this.LeftInfo.LeftWidth; } }
        public VirtualSuouLine Left { get { return this.LeftInfo.Center; } }
        public int LeftCenterSpaceWidth { get { return this.LeftInfo.RightWidth + this.CenterInfo.LeftWidth; } }
        public VirtualSuouLine Center { get { return this.CenterInfo.Center; } }
        public int CenterRightSpaceWidth { get { return this.CenterInfo.RightWidth + this.RightInfo.LeftWidth; } }
        public VirtualSuouLine Right { get { return this.RightInfo.Center; } }
        public int EdgeRightWidth { get { return this.RightInfo.RightWidth; } }
        public int Width
        {
            get { return this.LeftInfo.Width + this.CenterInfo.Width + this.RightInfo.Width; }
        }
        public ThreeVirtualSplitLine(EdgeSpaceLineInfo left, EdgeSpaceLineInfo Center, EdgeSpaceLineInfo right)
        {
            this.LeftInfo = left;
            this.CenterInfo = Center;
            this.RightInfo = right;
        }
        public SuouLine CarelessSpaceJoin(ISpaceMaker spacer)
        {
            var edgeLeftSpaceLine = spacer.CreateSpaceCarelessly(this.EdgeLeftWidth);
            var leftLine = this.Left.Create();
            var leftCenterSpaceLine = spacer.CreateSpaceCarelessly(this.LeftCenterSpaceWidth);
            var centerLine = this.Center.Create();
            var centerRightSpaceLine = spacer.CreateSpaceCarelessly(this.CenterRightSpaceWidth);
            var rightLine = this.Right.Create();
            var rightEdgeSpaceLine = spacer.CreateSpaceCarelessly(this.EdgeRightWidth);
            return SuouLine.Concat(edgeLeftSpaceLine, leftLine, leftCenterSpaceLine, centerLine, centerRightSpaceLine, rightLine, rightEdgeSpaceLine);
        }
        public SuouLine StrictSpaceJoin(ISpaceMaker spacer)
        {
            var edgeLeftSpaceLine = spacer.CreateSpace(this.EdgeLeftWidth);
            var leftLine = this.Left.Create();
            var leftCenterSpaceLine = spacer.CreateSpace(this.LeftCenterSpaceWidth);
            var centerLine = this.Center.Create();
            var centerRightSpaceLine = spacer.CreateSpace(this.CenterRightSpaceWidth);
            var rightLine = this.Right.Create();
            var rightEdgeSpaceLine = spacer.CreateSpace(this.EdgeRightWidth);
            return SuouLine.Concat(edgeLeftSpaceLine, leftLine, leftCenterSpaceLine, centerLine, centerRightSpaceLine, rightLine, rightEdgeSpaceLine);
        }
        public bool CanStrictSpaceJoin(ISpaceMaker spacer)
        {
            return spacer.CanCreateSpace(this.EdgeLeftWidth) 
                && spacer.CanCreateSpace(this.LeftCenterSpaceWidth)
                && spacer.CanCreateSpace(CenterRightSpaceWidth)
                && spacer.CanCreateSpace(EdgeRightWidth);
        }
        public SuouLine CenterCarelessBang(ISpaceMaker spacer)
        {
            //var fstW = this.Width;
            this.Normalize();
            while (!spacer.CanCreateSpace(this.LeftCenterSpaceWidth)) this.LeftInfo.TrimOneSuffix(SpaceSplitOption.LeftSpace);
            this.LeftInfo.LeftWidth = spacer.CreateSpaceCarelessly(this.LeftInfo.LeftWidth).Width;
            
            while (!spacer.CanCreateSpace(this.CenterRightSpaceWidth)) this.RightInfo.TrimOnePrefix(SpaceSplitOption.RightSpace);
            if (this.RightInfo.Center.Length == 0 && this.CenterRightSpaceWidth == 0) this.RightInfo.RightWidth = 0;
            SuouLine rst;
            if (this.CanStrictSpaceJoin(spacer))
            {
                rst = this.StrictSpaceJoin(spacer);
            }
            else
            {
                rst = this.CarelessSpaceJoin(spacer);
            }
            //var rst = this.StrictSpaceJoin(spacer);
            //var lstW = rst.Width;
            //if (fstW != lstW) throw new Exception();
            return rst;
        }
        private void Normalize()
        {
            this.LeftInfo.RightWidth += this.CenterInfo.LeftWidth;
            this.CenterInfo.LeftWidth = 0;
            this.RightInfo.LeftWidth += this.CenterInfo.RightWidth;
            this.CenterInfo.RightWidth = 0;
            this.LeftInfo.EmptyCheck(SpaceSplitOption.LeftSpace);
            this.RightInfo.EmptyCheck(SpaceSplitOption.RightSpace);
        }
    }
    public class EdgeSpaceLineInfo
    {
        public int LeftWidth { get; set; }
        public int RightWidth { get; set; }
        public VirtualSuouLine Center { get; private set; }

        public int Width
        {
            get { return this.LeftWidth + this.Center.GetWidth() + this.RightWidth; }
        }
        public bool IsEmpty
        {
            get
            {
                return this.LeftWidth == 0 && this.Center.Length == 0 && this.RightWidth == 0;
            }
        }
        public EdgeSpaceLineInfo(int left, VirtualSuouLine center,int right)
        {
            this.LeftWidth = left;
            this.Center = center;
            this.RightWidth = right;
        }
        public EdgeSpaceLineInfo(int left, SuouLine center, int right)
        {
            this.LeftWidth = left;
            this.Center = new VirtualSuouLine(center);
            this.RightWidth = right;
        }
        public static EdgeSpaceLineInfo CreateEmpty(System.Drawing.Font font, System.Drawing.Color color)
        {
            var empty = SuouLine.Empty(font,color);
            var v = new VirtualSuouLine(empty, 0, 0); 
            return new EdgeSpaceLineInfo(0, v, 0);
        }
        public void TrimOnePrefix(SpaceSplitOption option)
        {
            var p = this.Center.TrimOnePrefix();
            this.LeftWidth += p.Value;
            this.Center = p.Key;
            this.EmptyCheck(option);
        }
        public void EmptyCheck(SpaceSplitOption option)
        {
            if (this.Center.Length == 0)
            {
                switch (option)
                {
                    case SpaceSplitOption.LeftSpace:
                    case SpaceSplitOption.None:
                        this.LeftWidth += this.RightWidth;
                        this.RightWidth = 0;
                        break;
                    case SpaceSplitOption.CenterSpace:
                        throw new Exception();
                    case SpaceSplitOption.RightSpace:
                        this.RightWidth += this.LeftWidth;
                        this.LeftWidth = 0;
                        break;

                }
            }
        }
        public void TrimOneSuffix(SpaceSplitOption option)
        {
            var p = this.Center.TrimOneSuffix();
            this.RightWidth += p.Value;
            this.Center = p.Key;

            this.EmptyCheck(option);
        }
    }
    public struct VirtualSuouLine
    {
        private int _Index;
        private int _Length;
        private SuouLine Line;
        private int _Width;
        public int Length { get { return this._Length; } }
        public int Width
        {
            get
            {
                if (this._Width == -1) this._Width = this.Create().Width;
                return this._Width;
            }
        }
        public VirtualSuouLine(SuouLine line, int index, int length)
        {
            _Index = index;
            _Length = length;
            Line = line;
            _Width = -1;
        }
        public VirtualSuouLine(SuouLine line)
        {
            _Index = 0;
            _Length = line.Length;
            Line = line;
            _Width = line.Width;
        }
        private VirtualSuouLine(SuouLine line, int index, int length,int width)
        {
            _Index = 0;
            _Length = length;
            Line = line;
            _Width = width;

        }
        public SuouLine Create()
        {
            if (this._Length == 0)
            {
                return SuouLine.Empty(this.Line.Font, this.Line.Color);
            }
            else
            {
                return this.Line.Subline(this._Index, this._Length);
            }
        }
        public KeyValuePair<VirtualSuouLine,int> TrimOnePrefix()
        {
            if (this.Length == 0) throw new Exception();
            var cwidth = this.Line.GetLetterWidth(this._Index);
            var r = new VirtualSuouLine(this.Line, this._Index + 1, this.Length - 1, this.Width - cwidth);
            return new KeyValuePair<VirtualSuouLine, int>(r, cwidth);
        }
        public KeyValuePair<VirtualSuouLine,int> TrimOneSuffix()
        {
            if (this.Length == 0) throw new Exception();
            var cwidth = this.Line.GetLetterWidth(this._Index + this.Length - 1);
            var r = new VirtualSuouLine(this.Line, this._Index, this.Length - 1, this.Width - cwidth);
            return new KeyValuePair<VirtualSuouLine, int>(r, cwidth);
        }

        public int GetWidth()
        {
            return this.Width;
        }
        public static VirtualSuouLine CreateEmpty(System.Drawing.Font font, System.Drawing.Color color)
        {
            var line = SuouLine.Empty(font, color);
            return new VirtualSuouLine(line, 0, 0);
        }
    }
}
