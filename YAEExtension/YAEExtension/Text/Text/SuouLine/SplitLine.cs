﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text
{
    public class TwoSplitLine
    {
        public SuouLine Left { get; set; }
        public SuouLine Right { get; set; }
        public TwoSplitLine(SuouLine left, SuouLine right)
        {
            this.Left = left;
            this.Right = right;

        }
        public TwoSplitLine(System.Drawing.Font font,System.Drawing.Color color)
        {
            this.Left = SuouLine.Empty(font, color);
            this.Right = SuouLine.Empty(font, color);
        }
        public virtual bool IsEmpty
        {
            get
            {
                return this.Left.Length == 0 && this.Right.Length == 0;
            }
        }
    }
    public class ThreeSplitLine : TwoSplitLine
    {
        public override bool IsEmpty
        {
            get
            {
                return base.IsEmpty && this.Center.Length == 0;
            }
        }
        public SuouLine Center { get; set; }
        public SuouLine LeftCenter
        {
            get
            {
                return this.Left + this.Center;
            }
        }
        public SuouLine CenterRight
        {
            get
            {
                return this.Center + this.Right;
            }
        }
        public ThreeSplitLine(SuouLine left, SuouLine Center, SuouLine right)
            : base(left, right)
        {
            this.Center = Center;
        }
        public ThreeSplitLine(System.Drawing.Font font,System.Drawing.Color color)
            : base(font,color)
        {
            this.Center = SuouLine.Empty(font, color);
        }
        public override string ToString()
        {
            var s = "";
            s += "Left = {" + this.Left.Sentence + "}" + System.Environment.NewLine;
            s += "Center = {" + this.Center.Sentence + "}" + System.Environment.NewLine;
            s += "Right = {" + this.Right.Sentence + "}";
            return s;
        }
        public SuouLine Join()
        {
            return this.Left + this.Center + this.Right;
        }
        public SuouLine SpaceJoin(ISpaceMaker spacer)
        {
            var left = this.Left.SpaceSplit(spacer, SpaceSplitOption.RightSpace);
            var center = this.Center.SpaceSplit(spacer, SpaceSplitOption.None);
            var right = this.Right.SpaceSplit(spacer, SpaceSplitOption.LeftSpace);

            var leftSpaceLen = left.Right.Width + center.Left.Width;
            var rightSpaceLen = center.Right.Width + right.Left.Width;
            return left.LeftCenter + spacer.CreateSpaceCarelessly(leftSpaceLen) + center.Center + spacer.CreateSpaceCarelessly(rightSpaceLen) + right.CenterRight;
        }
        public SuouLine StrictSpaceJoin(ISpaceMaker spacer)
        {
            var left = this.Left.SpaceSplit(spacer, SpaceSplitOption.RightSpace);
            var center = this.Center.SpaceSplit(spacer, SpaceSplitOption.None);
            var right = this.Right.SpaceSplit(spacer, SpaceSplitOption.LeftSpace);

            var leftSpaceLen = left.Right.Width + center.Left.Width;
            var rightSpaceLen = center.Right.Width + right.Left.Width;

            if (!(spacer.CanCreateSpace(leftSpaceLen) && spacer.CanCreateSpace(rightSpaceLen))) throw new MyException("");
            var leftSpace = spacer.CreateSpace(leftSpaceLen);
            var rightSpace = spacer.CreateSpace(rightSpaceLen);

            return left.LeftCenter + leftSpace + center.Center + rightSpace + right.CenterRight;
        }
        public bool CanStrictSpaceJoin(ISpaceMaker spacer)
        {
            var left = this.Left.SpaceSplit(spacer, SpaceSplitOption.RightSpace);
            var center = this.Center.SpaceSplit(spacer, SpaceSplitOption.None);
            var right = this.Right.SpaceSplit(spacer, SpaceSplitOption.LeftSpace);

            var leftSpaceLen = left.Right.Width + center.Left.Width;
            var rightSpaceLen = center.Right.Width + right.Left.Width;

            if (!(spacer.CanCreateSpace(leftSpaceLen) && spacer.CanCreateSpace(rightSpaceLen))) return false;
            return true;
        }
        public ThreeSplitLine Copy()
        {
            var r = new ThreeSplitLine(Left.Clone(), Center.Clone(), Right.Clone());
            return r;
        }
    }


    public enum SplitWordOption
    {
        BangWord, NoBangWord
    }
    public enum SplitWidthOption
    {
        Relax, Fix
    }
    public enum TripleSplitOption
    {
        LeftAddSpace, CenterAddSpace, RightAddSpace
    }
    public enum NearestOption
    {
        LeftNearest, RightNearest, None
    }
    /// <summary>
    /// 空白分割時に空白部分しかない場合、どの部分に置くかを決定します
    /// </summary>
    public enum SpaceSplitOption
    {
        LeftSpace, CenterSpace, RightSpace, None
    }
}
