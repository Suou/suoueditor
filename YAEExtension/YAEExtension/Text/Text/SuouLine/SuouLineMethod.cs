﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using SuouBase;
using SuouBase.Text;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using BSystem.Drawing;
using SuouBase.Drawing;
namespace SuouBase.Text
{
    public partial class SuouLine
    {

        #region Method2
        public TwoSplitLine Split(int SplitPosition)
        {
            if (SplitPosition < 0)
            {
                throw new IndexOutOfRangeException(SplitPosition.ToString());
            }
            else if (SplitPosition > this.Length)
            {
                throw new IndexOutOfRangeException(SplitPosition.ToString()+"/"+this.Length);
            }
            TwoSplitLine r = new TwoSplitLine(this.Font,this.Color);

            var LeftLength = SplitPosition;

            r.Left = this.Subline(0, LeftLength);
            r.Right = this.Subline(LeftLength);

            if (r.Left.Length != LeftLength)
            {
                throw new MyException("");
            }
            return r;
        }
        public ThreeSplitLine CenterSpaceSplit(ISpaceMaker SpaceItem, int Index)
        {
            var TwoSplitLine = this.Split(Index);
            var LeftLine = TwoSplitLine.Left.SpaceSplit(SpaceItem, SpaceSplitOption.RightSpace);
            var RightLine = TwoSplitLine.Right.SpaceSplit(SpaceItem, SpaceSplitOption.LeftSpace);
            return new ThreeSplitLine(LeftLine.Left + LeftLine.Center, LeftLine.Right + RightLine.Left, RightLine.Center + RightLine.Right);
        }
        public ThreeSplitLine CenterSpaceDotSplit(ISpaceMaker SpaceItem,int DotIndex)
        {
            var x = this.DotIndexList.FindIndex(DotIndex);
            if (x == -1) throw new MyException("");
            return this.CenterSpaceSplit(SpaceItem,x);
        }
        /*
        public ThreeSplitLine DotSplit(int x1, int x2, AltSpace SpaceItem)
        {
            var left = this.SubDotLineIncompletely(SpaceItem, 0, x1, SplitWordOption.BangWord, SplitWidthOption.Fix);
            var center = this.SubDotLineIncompletely(SpaceItem, x1, x2 - x1, SplitWordOption.BangWord, SplitWidthOption.Fix);
            return null;
        }
        */
        /*
        public ThreeSplitLine SpaceSplit(int SplitPosition, AltSpace SpaceItem)
        {
            var Double = this.Split(SplitPosition);
            var LeftTriple = Double.Left.SpaceSplit(SpaceItem);
            var RightTriple = Double.Right.SpaceSplit(SpaceItem);
            var CenterSpace = LeftTriple.Right + RightTriple.Left;
            return new ThreeSplitLine(LeftTriple.Left + LeftTriple.Center, CenterSpace, RightTriple.Center + RightTriple.Right);
        }
        */
        /// <summary>
        /// 左端空白文字列、中央文字列、右端空白文字列に分割します。
        /// 入力文字列が空白文字列だったとき、空白文字列が入る場所はoptionによって決定します。
        /// </summary>
        /// <param name="SpaceItem"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public ThreeSplitLine SpaceSplit(ISpaceMaker SpaceItem,SpaceSplitOption option)
        {
            var v = this.SpaceSplit(SpaceItem);
            if (v.Center.Length > 0) return v;
            switch (option)
            {
                case SpaceSplitOption.LeftSpace:
                    return v;
                case SpaceSplitOption.CenterSpace:
                    return new ThreeSplitLine(v.Center, v.Left, v.Right);
                case SpaceSplitOption.RightSpace:
                    return new ThreeSplitLine(v.Right, v.Center, v.Left);
                default:
                    return v;
            }
        }
        public ThreeSplitLine SpaceSplit(int index,ISpaceMaker SpaceItem, SpaceSplitOption option)
        {
            var a = this.Subline(0, index).SpaceSplit(SpaceItem, option);
            var left = a.LeftCenter;
            var b = this.Subline(index).SpaceSplit(SpaceItem, option);
            //var right = b.lef
            return null;
        }
        /// <summary>
        /// 左端空白文字列、中央文字列、右端空白文字列に分割します。
        /// 入力文字列が全て空白文字列ならば、左端空白文字列、中央空、右端空が返されます。
        /// </summary>
        /// <param name="SpaceItem"></param>
        /// <returns></returns>
        public ThreeSplitLine SpaceSplit(ISpaceMaker SpaceItem)
        {
            var r = new ThreeSplitLine(this.Font,this.Color);
            r.Left = this;
            
            var leftIndex = this.Sentence.FindIndex((n) => !SpaceItem.IsSpace(n));
            if (leftIndex != -1)
            {
                var v = this.Split(leftIndex);
                r.Left = v.Left;
                r.Center = v.Right;
            }
            var rightIndex = (r.Center.Length-1) - r.Center.MiniSufIterator().FindIndex((n) => !SpaceItem.IsSpace(n.Letter));
            if (rightIndex != r.Center.Length)
            {
                var v = r.Center.Split(rightIndex + 1);
                r.Center = v.Left;
                r.Right = v.Right;
            }

            
            return r;
        }
        public EdgeSpaceLineInfo SpaceSplitInfo(ISpaceMaker SpaceItem)
        {
            var p = this.SpaceSplit(SpaceItem);
            var line = new VirtualSuouLine(p.Center, 0, p.Center.Length);
            return new EdgeSpaceLineInfo(p.Left.Width, line, p.Right.Width);
        }
        public bool CanSpaceDotSplit(int SplitDotPosition)
        {
            var n = this.DotIndexList.FindNearIndex(SplitDotPosition, BinaryOption.RangeNear);
            if (n < 0) return false;
            return true;
        }
        public ThreeSplitLine SpaceDotSplit(int SplitDotPosition,  ISpaceMaker SpaceItem)
        {
            var Index = this.DotIndexList.FindNearIndex(SplitDotPosition, BinaryOption.RangeNear);
            if (Index < 0) throw new Exception();
            return CenterSpaceSplit(SpaceItem,Index);
        }



        public bool CanDotSplit(int SplitPosition)
        {
            var x = this.DotIndexList.FindIndex(SplitPosition);
            if (x == -1) return false;
            return true;
        }
        public TwoSplitLine DotSplit(int SplitPosition)
        {
            var x = this.DotIndexList.FindIndex(SplitPosition);
            if (x == -1) throw new Exception();
            return this.Split(x);
        }
        public bool CanTripleDotSplit(int CenterStartPosition, int CenterLength)
        {
            var x = this.DotIndexList.FindIndex(CenterStartPosition);
            var x2 = this.DotIndexList.FindIndex(CenterStartPosition + CenterLength);

            if (x == -1 || x2 == -1) return false;
            return true;
        }
        public ThreeSplitLine TripleDotSplit(int CenterStartPosition, int CenterLength)
        {
            var x = this.DotIndexList.FindIndex(CenterStartPosition);
            var x2 = this.DotIndexList.FindIndex(CenterStartPosition + CenterLength);

            if (x == -1 || x2 == -1) throw new Exception();
            return this.TripleSplit(x, x2 - x);
        }
        public bool CanTripleDotSplit(int CenterStartPosition, int CenterLength,ISpaceMaker SpaceMaker ,TripleSplitOption Option)
        {
            throw new Exception();
            /*
            var SpaceTrimedLine = this.SpaceSplit(SpaceMaker);
            var RightSpaceTrimedLine = SpaceTrimedLine.Left + SpaceTrimedLine.Center;
            if (SpaceTrimedLine.Right.RealWidth != 0) return RightSpaceTrimedLine.CanTripleDotSplit(CenterStartPosition, CenterLength, SpaceMaker, Option);

            if (RightSpaceTrimedLine.RealWidth < CenterStartPosition)
            {
                var LeftSpaceLen = CenterStartPosition - RightSpaceTrimedLine.RealWidth;
                bool CanLeftSpace = SpaceMaker.CanCreateSpace(LeftSpaceLen);
                return CanLeftSpace;
            }
            else if (this.RealWidth < CenterStartPosition + CenterLength)
            {
            }
            else
            {
            }
            var x = this.TranslateDotToIndexStrictly(CenterStartPosition);
            var x2 = this.TranslateDotToIndexStrictly(CenterStartPosition + CenterLength);

            if (x == -1 || x2 == -1) throw new Exception();
            return this.TripleSplit(x, x2 - x);
            */
        }
        public ThreeSplitLine TripleDotSplit(int CenterStartPosition, int CenterLength,TripleSplitOption Option)
        {
            throw new Exception();
            /*
            if (this.RealWidth < CenterStartPosition)
            {
            }
            else if (this.RealWidth < CenterStartPosition + CenterLength)
            {
            }
            else
            {
            }
            var x = this.TranslateDotToIndexStrictly(CenterStartPosition);
            var x2 = this.TranslateDotToIndexStrictly(CenterStartPosition + CenterLength);

            if (x == -1 || x2 == -1) throw new Exception();
            return this.TripleSplit(x, x2 - x);
            */
        }
        public ThreeSplitLine TripleSplit(int CenterStartPosition, int CenterLength)
        {
            if (CenterStartPosition < 0) throw new IndexOutOfRangeException(CenterStartPosition.ToString());
            if (CenterLength < 0) return this.TripleSplit(CenterStartPosition + CenterLength, -CenterLength);
            var RightStartPosition = CenterLength;
            var LeftLength = CenterStartPosition;

            var r = new ThreeSplitLine(this.Font,this.Color);
            var LeftRight = this.Split(CenterStartPosition);
            var CenterRight = LeftRight.Right.Split(RightStartPosition);

            r.Left = LeftRight.Left;
            r.Center = CenterRight.Left;
            r.Right = CenterRight.Right;

            if (r.Left.Length != LeftLength) throw new Exception();
            if (r.Center.Length != CenterLength) throw new Exception();
            return r;
        }

        


        /// <summary>
        /// 後で直す
        /// </summary>
        /// <param name="Index"></param>
        /// <param name="Length"></param>
        /// <returns></returns>
        public SuouLine Subline(int Index, int Length)
        {
            int LastIndex = Index + Length;
            var r = SuouLine.Empty(this.Font, this.Color);
            if (Length == 0) return r;  
            if (Index < 0 || Index >= this.Length) throw new Exception("部分文字列の開始位置が元の文字列の範囲を超えています");
            if (Length < 0) throw new Exception("部分文字列の長さが負です");
            if (LastIndex > this.Length)
            {
                throw new MyException("部分文字列が境界を越えています");
            }
            /*
            if (this.SubLineDic.ContainSubLine(Index, Length))
            {
                var mr = this.SubLineDic.GetSubLine(Index, Length);

                if (Length != mr.Length) throw new MyException("");
                return mr;
            }
            */
            else
            {
                //int n = 0;

                
                
                //this.LetterIterator().Skip(Index).Take(Length).ToList().ForEach((v) => r += v);
                
                int NowIndex = 0;

                Func<SuouWord, bool> LeftRangeOutFunc = (w) => { if (NowIndex + w.Length < Index) { NowIndex += w.Length; return true; } else { return false; } };
                Func<SuouWord, bool> OverlapFunc = (w) => { if (NowIndex < Index && LastIndex <= NowIndex + w.Length) { r += w.SubWord(Index - NowIndex, Length); NowIndex += w.Length; return true; } else { return false; } };
                Func<SuouWord, bool> LeftOverlapFunc = (w) => { if (NowIndex < Index && NowIndex + w.Length <= LastIndex) { r += w.Split(Index - NowIndex).Right; NowIndex += w.Length; return true; } else { return false; } };
                Func<SuouWord, bool> RangeFunc = (w) => { if (Index <= NowIndex && NowIndex + w.Length <= LastIndex) { r += w; NowIndex += w.Length; return true; } else { return false; } };
                Func<SuouWord, bool> RightOverlapFunc = (w) => { if (NowIndex <= LastIndex && LastIndex < NowIndex + w.Length) { r += w.Split(LastIndex - NowIndex).Left; NowIndex += w.Length; return true; } else { return false; } };

                this.Words.SkipWhile(LeftRangeOutFunc).SkipWhile(OverlapFunc).SkipWhile(LeftOverlapFunc).SkipWhile(RangeFunc).SkipWhile(RightOverlapFunc).Do();
                
                
                //this.SubLineDic.Register(Index, Length, r);
                if (Length != r.Length)
                {
                    throw new MyException("");
                }
                return r;
            }
        }
        public SuouLine Subline(int Index)
        {
            int len = this.Length - Index;
            return this.Subline(Index, len);
        }

        


        public virtual SuouLine Clone()
        {
            return new SuouLine(this);
        }
        public string Sentence
        {
            get
            {
                StringBuilder sb = new StringBuilder("");
                foreach (SuouWord tf in Words) sb.Append(tf.Word);
                return sb.ToString();
            }

             
            private set
            {
                //var v = new SuouWord(this.Font, this.Color, value);
                var v = new SuouWord(this._Font, this._Color, value);
                this.Clear();
                this.Add(v);
            }

        }

        public static SuouLine Empty(Font f, Color c)
        {
            var r = SuouWord.Empty(f);
            return new SuouLine(r);

        }
        public static SuouLine Empty(SuouLine lc)
        {
            var r = SuouLine.Empty(lc.Font, lc.Color);
            return r;

        }
        public SuouLetter LastChar()
        {
            return this.Words.Last().Last();
        }
        public SuouLetter FirstChar()
        {
            return this.Words.First().First();
        }
        private void ViolateCheck()
        {

        }
        protected virtual void SetSizeCheck(SuouWord wc)
        {
            this.DotIndexList += wc.DotIndexList;
            
        }
        protected virtual void SetSizeCheck(SuouLine wc)
        {
            this.DotIndexList += wc.DotIndexList;
        }
        private void SetDotIndexes()
        {
            var r = LineDotIndexes.Empty(this.Font);
            foreach (var v in this.Words)
            {
                if (v.DotIndexList == null) throw new MyException("");
                r += v.DotIndexList;
            }
            this._DotIndexList = r;
        }
        protected virtual void SetSize()
        {
            SetDotIndexes();
            //this.SubLineDic = new SubLineDictionary();
            
        }
        /*
        private void InitializeSize()
        {
            RealWidth = -1;
            Height = -1;
        }
        */
        public int MeasureWidth(int len)
        {
            
            if (len > this.Length || len < 0) throw new Exception();
            if (len == 0) return 0;
            var n = len;
            var width = 0;
            foreach (var w in Words)
            {
                if (n > w.CharCount)
                {
                    width += w.Width;
                    n -= w.CharCount;
                }
                else
                {
                    width += w.MeasureWidth(n);
                    n = 0;
                    return width;
                }
            }
            return width;
            
        }
        /// <summary>
        /// 端数は切り捨て
        /// </summary>
        /// <param name="dotindex"></param>
        /// <returns></returns>
        public DiffIndex TruncateFromWidthToLength(int dotindex,NearestOption option)
        {
            if (dotindex < 0) return new DiffIndex(-1,dotindex);
            var bandIndex = this.LetterIterator().FoldFindIndex((c) => c.Width, (n) => n > dotindex);
            if (bandIndex == -1)
            {
                return new DiffIndex(this.Length, dotindex - this.Width);
            }
            var wWidth = this.MeasureWidth(bandIndex);
            if (wWidth == dotindex) return new DiffIndex(bandIndex,0);
            var diff = wWidth - dotindex;
            switch (option)
            {
                case NearestOption.LeftNearest:
                    return new DiffIndex(bandIndex,diff);
                case NearestOption.RightNearest:
                    wWidth = this.MeasureWidth(bandIndex + 1);
                    diff = wWidth - dotindex;
                    return new DiffIndex(bandIndex + 1, diff);
                case NearestOption.None:
                    var rightdiff = this.MeasureWidth(bandIndex + 1) - dotindex;
                    var leftdiff = dotindex - this.MeasureWidth(bandIndex);
                    if (System.Math.Abs(rightdiff) > System.Math.Abs(leftdiff))
                    {
                        return new DiffIndex(bandIndex,-leftdiff);
                    }
                    else
                    {
                        return new DiffIndex(bandIndex + 1, rightdiff);
                    }
            }
            return new DiffIndex(-1,0);
            /*
            foreach (var w in Words)
            {
                if (n >= w.Width)
                {
                    r += w.CharCount;
                    n -= w.Width;
                }
                else
                {
                    var rm = w.Font.TruncateFromWidthToLength(w.Word, n);
                    r += rm;
                    return r;
                }
            }
            return r;
            */
        }
        public int CharWidth(int nth)
        {
            if (nth == 0) return this.DotIndexList.DotIndexes[0];
            return this.DotIndexList.DotIndexes[nth] - this.DotIndexList.DotIndexes[nth - 1];
        }
        [Obsolete]
        public string SubDotString(int beginDotX, int width)
        {
            string str = "";
            int n = 0;
            int EndDotPos = beginDotX + width;
            foreach (var w in Words)
            {
                foreach (var c in w.Word)
                {
                    if (n + w.Font.Width(c) > EndDotPos) return str;
                    if (n >= beginDotX && n <= EndDotPos) str += c;
                    n += w.Font.Width(c);
                }
            }
            return str;
        }
        private void ToConvert()
        {
            string s = "";
            foreach (SuouWord tf in Words)
            {
                s += tf.Word;
            }
            Sentence = s;
        }
        #endregion
        #region StaticOperate

        public static SuouLine Concat(params SuouLine[] lines)
        {
            var font = lines.First().Font;
            var color = lines.First().Color;
            var p = SuouLine.Empty(font, color);
            foreach (var v in lines)
            {
                p.Add(v);
            }
            return p;
        }
        public static SuouLine Merge(SuouLine a, SuouLine b)
        {

            var ar = a.Clone();
            var br = b.Clone();
            ar.Add(br);

            return ar;
        }

        public static SuouLine[] Create(string[] ws, Font f, Color c)
        {
            var r = new SuouLine[ws.Length];
            for (int i = 0; i < ws.Length; i++)
            {
                var fs = new SuouWord(f, c, ws[i]);
                var v = new SuouLine(fs);
                r[i] = v;
            }
            return r;
        }
        #endregion
        #region Operator
        public static SuouLine operator +(SuouLine lc, SuouWord wc)
        {
            var r = lc.Clone();
            r.Add(wc.Clone());
            return r;
        }
        public static SuouLine operator +(SuouLine lc, SuouLetter letter)
        {
            var r = lc.Clone();
            r.Add(letter.ToWord());
            return r;
        }
        public static SuouLine operator +(SuouWord wc, SuouLine lc)
        {
            var w = wc.ToLineComposition();
            var r = SuouLine.Merge(w, lc.Clone());
            return r;
        }
        public static SuouLine operator +(SuouLine lc1, SuouLine lc2)
        {
            if (lc1 == null || lc2 == null) throw new MyException(System.Environment.StackTrace);
            var a = lc1.Clone();
            var b = lc2.Clone();
            a.Add(b);
            return a;
        }
        #endregion
        #region ToMethod
        public override string ToString()
        {
            return this.Sentence;
        }
        /*
        public TextLineComposition ToTextLine(WordComposition EndWord)
        {
            var r = new TextLineComposition(this,EndWord);
            return r;
        }
        */
        #endregion
    }
    public struct DiffIndex
    {
        private int _Index;
        private int _DotDiff;
        public int Index { get { return this._Index; } }
        public int DotDiff { get { return this._DotDiff; } }

        public DiffIndex(int index, int dotDiff)
        {
            this._DotDiff = dotDiff;
            this._Index = index;
        }
    }
    
}
