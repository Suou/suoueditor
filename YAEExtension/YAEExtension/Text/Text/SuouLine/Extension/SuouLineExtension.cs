﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using System.Drawing.Imaging;
using SuouBase;
using SuouBase.Text;
using System.Xml.Linq;
using System.Diagnostics;

namespace SuouBase.Text
{
    public static class SuouLineExtension
    {
        public static SuouLine Choice(this SuouLine item, int StartIndex, int Length, Func<SuouLine, SuouLine> Func)
        {
            var v = item.TripleSplit(StartIndex, Length);
            v.Center = Func(v.Center);

            var line = v.Join();
            return line;
        }
        public static SuouLine Choice(this SuouLine item, int StartDotIndex, ISpaceMaker SpaceMaker, Func<SuouLine, SuouLine> Func)
        {
            if (item.CanSpaceDotSplit(StartDotIndex))
            {
                var v = item.SpaceDotSplit(StartDotIndex, SpaceMaker);

                v.Center = Func(v.Center);
                return v.Join();
            }
            else
            {
                return null;
            }
        }
        #region TripleSplit
        public static ThreeSplitLine TripleSplitIncompletetly(this SuouLine item, ISpaceMaker Spacer, int CenterStartPosition, int CenterLength, SplitWordOption WordOption, SplitWidthOption WidthOption)
        {
            var r = new ThreeSplitLine(item.Font,item.Color);

            r.Left = item.SubDotLineIncompletely(Spacer, 0, CenterStartPosition, WordOption, WidthOption);
            r.Center = item.SubDotLineIncompletely(Spacer, CenterStartPosition, CenterLength, WordOption, WidthOption);
            r.Right = item.SubDotLineIncompletely(Spacer, CenterStartPosition + CenterLength, WordOption, WidthOption);

            return r;
        }
        public static ThreeSplitLine TripleSplit(this SuouLine item, ISpaceMaker Spacer, int CenterStartPosition, int CenterLength)
        {
            var r = new ThreeSplitLine(item.Font, item.Color);

            r.Left = item.SubDotLineIncompletely(Spacer, 0, CenterStartPosition, SplitWordOption.BangWord, SplitWidthOption.Fix);
            r.Center = item.SubDotLineIncompletely(Spacer, CenterStartPosition, CenterLength, SplitWordOption.BangWord, SplitWidthOption.Fix);
            r.Right = item.SubDotLineIncompletely(Spacer, CenterStartPosition + CenterLength, SplitWordOption.BangWord, SplitWidthOption.Fix);

            return r;
        }

        #endregion

        #region SubLine
        public static SuouLine SubLineCarelessWidthNoBangWord(this SuouLine item, ISpaceMaker Spacer, int StartIndex, int SubLength, int HeadSpaceWidth, int TailSpaceWidth)
        {

            var HeadSpace = Spacer.CreateSpaceCarelessly(HeadSpaceWidth);
            var nDiff = HeadSpace.Width - HeadSpaceWidth;
            var NewTailSpaceWidth = nDiff + TailSpaceWidth;
            if (NewTailSpaceWidth < 0) NewTailSpaceWidth = 0;
            var TailSpace = Spacer.CreateSpaceCarelessly(NewTailSpaceWidth);
            var r = item.Subline(StartIndex, SubLength);
            var Result = HeadSpace + r + TailSpace;

            return Result;
        }
        public static SuouLine SubLineBangWordStrictWidth(this SuouLine item, ISpaceMaker Spacer, int StartIndex, int SubLength, int HeadSpaceWidth, int TailSpaceWidth)
        {
            var a = Spacer.CanCreateSpace(HeadSpaceWidth);
            var b = Spacer.CanCreateSpace(TailSpaceWidth);
            if (!a)
            {
                int n = item.CharWidth(StartIndex);
                return item.SubLineBangWordStrictWidth(Spacer, StartIndex + 1, SubLength - 1, HeadSpaceWidth + n, TailSpaceWidth);
            }

            if (!b)
            {
                var LastIndex = StartIndex + SubLength - 1;
                int n = item.CharWidth(LastIndex);
                return item.SubLineBangWordStrictWidth(Spacer, StartIndex, SubLength - 1, HeadSpaceWidth, TailSpaceWidth + n);
            }

            var r = item.Subline(StartIndex, SubLength);

            var HeadSpace = Spacer.CreateSpace(HeadSpaceWidth);
            var TailSpace = Spacer.CreateSpace(TailSpaceWidth);


            var newr = HeadSpace + r + TailSpace;

            return newr;
        }
        #endregion

    }
}
