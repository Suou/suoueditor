﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using BSystem;
namespace SuouBase.Text
{
    public partial class SuouLine
    {
        public SuouLine SubDotLineBangLineFixWidthFixPos(ISpaceMaker Spacer, int StartDotIndex, int Width)
        {
            if (StartDotIndex >= this.Width) return Spacer.CreateSpace(Width);
            var EndDotIndex = StartDotIndex + Width;
            var x1 = this.TruncateFromWidthToLength(StartDotIndex, NearestOption.RightNearest).Index;
            var x2 = this.TruncateFromWidthToLength(StartDotIndex + Width, NearestOption.LeftNearest).Index;
            var NewX1 = x1.FindIndexWidthCountUp((i) =>
            {
                if (i >= this.Length) return true;
                var d1 = this.MeasureWidth(i);
                return Spacer.CanCreateSpace(d1 - StartDotIndex);
            }
            );
            var NewX2 = x2.FindIndexWidthCountDown((i) =>
            {
                if (i < 0) return true;
                var d1 = this.MeasureWidth(i);
                return Spacer.CanCreateSpace(EndDotIndex - d1);
            }
            );
            if (NewX1 >= this.Length && Spacer.CanCreateSpace(Width)) return Spacer.CreateSpace(Width);
            if (NewX1 >= this.Length || NewX2 < 0 || NewX1 > NewX2) throw new MyException("");
            var LeftSpace = Spacer.CreateSpace(this.MeasureWidth(NewX1) - StartDotIndex);
            var RightSpace = Spacer.CreateSpace(EndDotIndex - this.MeasureWidth(NewX2));
            var CenterLine = this.Subline(NewX1, NewX2 - NewX1);

            return LeftSpace + CenterLine + RightSpace;
        }

        private EdgeSpaceLineInfo SubDotLineInfoBangFixWidthFixPos(int StartDotIndex, int Width)
        {
            if (Width <= 0)
            {
                return EdgeSpaceLineInfo.CreateEmpty(this.Font, this.Color);
            }
            else
            {
                EdgeSpaceLineInfo rst = null;
                if (StartDotIndex < 0)
                {
                    Width += StartDotIndex;
                    StartDotIndex = 0;
                }
                else if (StartDotIndex > this.Width)
                {
                    var p = VirtualSuouLine.CreateEmpty(this.Font, this.Color);
                    return new EdgeSpaceLineInfo(0, p, Width);
                }

                var index = this.TruncateFromWidthToLength(StartDotIndex, NearestOption.RightNearest);
                if (StartDotIndex + Width > this.Width)
                {
                    var diff = StartDotIndex + Width - this.Width;
                    Width = this.Width - StartDotIndex;

                    var virtualCenter = new VirtualSuouLine(this, index.Index, this.Length - index.Index);
                    rst = new EdgeSpaceLineInfo(index.DotDiff, virtualCenter, diff);
                }
                else
                {
                    var endIndex = this.TruncateFromWidthToLength(StartDotIndex + Width, NearestOption.LeftNearest);
                    if (endIndex.Index >= index.Index)
                    {
                        var virtualCenter = new VirtualSuouLine(this, index.Index, endIndex.Index - index.Index);
                        rst = new EdgeSpaceLineInfo(index.DotDiff, virtualCenter, -endIndex.DotDiff);
                    }
                    else
                    {
                        var virtualCenter = VirtualSuouLine.CreateEmpty(this.Font, this.Color);
                        rst = new EdgeSpaceLineInfo(Width, virtualCenter, 0);
                    }
                    if (rst.Width != Width) throw new Exception();
                }
                return rst;
            }

        }
        public EdgeSpaceLineInfo SubDotLineInfoBangFixWidthFixPos2(ISpaceMaker Spacer,int StartDotIndex, int Width)
        {
            var p = this.SubDotLineInfoBangFixWidthFixPos(StartDotIndex, Width);
            var line = p.Center.Create();
            var splitResult = line.SpaceSplit(Spacer, SpaceSplitOption.LeftSpace);
            EdgeSpaceLineInfo rst = null;
            if (splitResult.Center.Length == 0)
            {
                rst = new EdgeSpaceLineInfo(p.LeftWidth + splitResult.Left.Width + p.RightWidth, VirtualSuouLine.CreateEmpty(this.Font, this.Color), 0);
            }
            else
            {
                var virtualCenter = new VirtualSuouLine(splitResult.Center, 0, splitResult.Center.Length);
                rst = new EdgeSpaceLineInfo(p.LeftWidth + splitResult.Left.Width, virtualCenter, p.RightWidth + splitResult.Right.Width);
            }
            if (rst.Width != Width) throw new Exception();
            return rst;
        }


        public bool CanSubDotLineBangLineFixWidthFixPos(ISpaceMaker Spacer, int StartDotIndex, int Width)
        {
            if (StartDotIndex >= this.Width) return Spacer.CanCreateSpace(Width);
            
            var EndDotIndex = StartDotIndex + Width;
            var x1 = this.TruncateFromWidthToLength(StartDotIndex, NearestOption.RightNearest).Index;
            var x2 = this.TruncateFromWidthToLength(StartDotIndex + Width, NearestOption.LeftNearest).Index;
            var NewX1 = x1.FindIndexWidthCountUp((i) =>
            {
                if (i >= this.Length) return true;
                var d1 = this.MeasureWidth(i);
                return Spacer.CanCreateSpace(d1 - StartDotIndex);
            }
            );
            var NewX2 = x2.FindIndexWidthCountDown((i) =>
            {
                if (i < 0) return true;
                var d1 = this.MeasureWidth(i);
                return Spacer.CanCreateSpace(EndDotIndex - d1);
            }
            );
            if (NewX1 >= this.Length && Spacer.CanCreateSpace(Width)) return true;
            if (NewX1 >= this.Length || NewX2 < 0 || NewX1 > NewX2) { return false; }
            
            return true;
        }

        public ThreeSplitLine TripleDotSplit(ISpaceMaker Spacer,int x1,int x2,bool permitcenternull){
            var left = this.SubDotLineBangLineFixWidthFixPos(Spacer, 0, x1);
            var rightlen = (this.Width - x2 >= 0) ? this.Width - x2 : 0; 
            var right = this.SubDotLineBangLineFixWidthFixPos(Spacer, x2, rightlen);
            SuouLine center = null;
            if (this.CanSubDotLineBangLineFixWidthFixPos(Spacer, x1, x2 - x1))
            {
                center = this.SubDotLineBangLineFixWidthFixPos(Spacer, x1, x2 - x1);
            }
            else if (!permitcenternull)
            {
                throw new MyException("");
            }
            return new ThreeSplitLine(left,center,right);
        }
        public ThreeVirtualSplitLine TripleDotSplitInfo(ISpaceMaker Spacer, int x1, int x2)
        {
            var left = this.SubDotLineInfoBangFixWidthFixPos2(Spacer, 0, x1);
            if (left.Width != x1) throw new Exception();
            var rightlen = (this.Width - x2 >= 0) ? this.Width - x2 : 0;
            var right = this.SubDotLineInfoBangFixWidthFixPos2(Spacer, x2, rightlen);
            EdgeSpaceLineInfo center = this.SubDotLineInfoBangFixWidthFixPos2(Spacer, x1, x2 - x1);
            
            return new ThreeVirtualSplitLine(left, center, right);
        }
        public bool CanTripleDotSplit(ISpaceMaker Spacer, int x1, int x2, bool permitcenternull)
        {
            if(!this.CanSubDotLineBangLineFixWidthFixPos(Spacer, 0, x1))return false;
            var rightlen = (this.Width - x2 >= 0) ? this.Width - x2 : 0; 
            if(!this.CanSubDotLineBangLineFixWidthFixPos(Spacer, x2, rightlen)) return false;
            if (this.CanSubDotLineBangLineFixWidthFixPos(Spacer, x1, x2 - x1))
            {
            }
            else if (!permitcenternull)
            {
                return false;
            }
            return true;
        }
        public SuouLine EdgeSpaceTransparentize(ISpaceMaker Spacer)
        {
            var f = this.SpaceSplit(Spacer);
            f.Left = f.Left.PaintTransparent();
            f.Right = f.Right.PaintTransparent();
            return f.Join();
        }
    }
}
