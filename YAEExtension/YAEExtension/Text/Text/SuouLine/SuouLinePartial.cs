﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using BSystem.Text;
namespace SuouBase.Text
{
    public partial class SuouLine
    {
        #region Satisfy
        public bool CanSatisfyMinWidth(ISpaceMaker Spacer, int width)
        {
            var r = this.Clone();
            var SpaceWidth = width - r.Width;
            return Spacer.CanCreateSpace(SpaceWidth);
        }
        public SuouLine SatisfyMinWidth(ISpaceMaker Spacer, int width)
        {
            var r = this.Clone();
            if (r.Width >= width) return r;
            var SpaceWidth = width - r.Width;
            var Space = Spacer.CreateSpaceCarelesslyNotShort(SpaceWidth);
            return r + Space;
        }
        #endregion


        #region SubDotLine
        public SuouLine SubDotLineIncompletely(ISpaceMaker Spacer, int StartDotIndex, int Width, int HeadSpaceWidth, int TailSpaceWidth, SplitWordOption WordOption, SplitWidthOption WidthOption)
        {
            if (Width == 0)
            {
                StartDotIndex = 0;
            }
            var a = this.ContainDotIndex(StartDotIndex);
            var b = this.ContainDotIndex(StartDotIndex + Width);
            if (!a)
            {
                return SubDotLineIncompletely(Spacer, StartDotIndex + 1, Width - 1, HeadSpaceWidth + 1, TailSpaceWidth, WordOption, WidthOption);
            }

            if (!b)
            {
                return SubDotLineIncompletely(Spacer, StartDotIndex, Width - 1, HeadSpaceWidth, TailSpaceWidth + 1, WordOption, WidthOption);
            }

            var StartIndex = this.DotIndexList.FindIndex(StartDotIndex);
            var EndIndex = this.DotIndexList.FindIndex(StartDotIndex + Width);
            var Length = EndIndex - StartIndex;

            SuouLine r;
            if (WordOption == SplitWordOption.BangWord && WidthOption == SplitWidthOption.Fix)
            {
                r = this.SubLineBangWordStrictWidth(Spacer, StartIndex, Length, HeadSpaceWidth, TailSpaceWidth);
            }
            else if (WordOption == SplitWordOption.NoBangWord && WidthOption == SplitWidthOption.Relax)
            {
                r = this.SubLineCarelessWidthNoBangWord(Spacer, StartIndex, Length, HeadSpaceWidth, TailSpaceWidth);
            }
            else
            {
                throw new Exception();
            }
            return r;

        }
        /// <summary>
        /// StartDotIndexから始まる幅Widthの文字列を返します。
        /// </summary>
        /// <param name="Spacer"></param>
        /// <param name="StartDotIndex"></param>
        /// <param name="Width"></param>
        /// <param name="WordOption"></param>
        /// <param name="WidthOption"></param>
        /// <returns></returns>
        public SuouLine SubDotLineIncompletely(ISpaceMaker Spacer, int StartDotIndex, int Width, SplitWordOption WordOption, SplitWidthOption WidthOption)
        {
            SuouLine r;
            if (StartDotIndex < 0)
            {
                Width -= -(StartDotIndex);
                StartDotIndex = 0;
            }

            if (StartDotIndex > this.Width)
            {
                if (Width >= 0)
                {
                    if (WidthOption == SplitWidthOption.Relax)
                    {
                        r = Spacer.CreateSpaceCarelessly(Width);
                    }
                    else
                    {
                        if (Spacer.CanCreateSpace(Width))
                        {
                            r = Spacer.CreateSpace(Width);
                        }
                        else
                        {
                            throw new Exception("長さが適する部分文字列を作ることができません");
                        }
                    }
                }
                else
                {
                    if (WidthOption == SplitWidthOption.Relax)
                    {
                        r = SuouLine.Empty(this);
                    }
                    else
                    {
                        throw new Exception("長さが適する部分文字列を作ることができません");
                    }
                }
            }
            else
            {
                if (Width < 0)
                {
                    throw new Exception();
                }
                else
                {
                    var EndDotIndex = StartDotIndex + Width;
                    var ModifyWidth = Width;
                    var SpaceWidth = 0;
                    if (EndDotIndex > this.Width)
                    {
                        SpaceWidth = EndDotIndex - this.Width;
                        ModifyWidth = this.Width - StartDotIndex;
                    }
                    r = this.SubDotLineIncompletely(Spacer, StartDotIndex, ModifyWidth, 0, SpaceWidth, WordOption, WidthOption);

                }
            }
            return r;
        }
        public SuouLine SubDotLineIncompletely(ISpaceMaker Spacer, int StartDotIndex, SplitWordOption WordOption, SplitWidthOption WidthOption)
        {
            var Width = this.Width - StartDotIndex;
            if (this.Width < StartDotIndex) return Spacer.CreateSpaceCarelessly(0);
            var r = SubDotLineIncompletely(Spacer, StartDotIndex, Width, WordOption, WidthOption);
            return r;
        }

        public SuouLine DotSublineStrictly(int DotIndex)
        {
            var r = SuouLine.Empty(this.Font, this.Color);
            var index = this.DotIndexList.FindIndex(DotIndex);
            if (index == -1) throw new Exception("適切な部分文字列開始位置が見つかりません");
            return this.Subline(index);

        }
        public SuouLine DotSublineStrictly(int DotIndex, int Width)
        {
            var r = SuouLine.Empty(this.Font, this.Color);
            var index = this.DotIndexList.FindIndex(DotIndex);
            var lastIndex = this.DotIndexList.FindIndex(DotIndex + Width);

            if (index != -1 && lastIndex != -1)
            {
                return Subline(index, lastIndex - index);
            }
            throw new Exception();
        }
        #endregion
        #region ContainIndex
        public bool ContainDotIndex(int DotStartIndex)
        {
            var b = this.DotIndexList.FindIndex(DotStartIndex) != -1;
            return b;
        }
        public bool ContainSubDotline(int DotIndex, int Width)
        {
            var index = this.DotIndexList.FindIndex(DotIndex);
            var lastIndex = this.DotIndexList.FindIndex(DotIndex + Width);

            var b = (index != -1 && lastIndex != -1);
            return b;


        }
        #endregion
        
        public uint Hash
        {
            get
            {
                return this.Sentence.GetLineHash();
            }
        }
    }
}
