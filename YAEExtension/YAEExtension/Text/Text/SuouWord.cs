﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase.Text;
using System.Diagnostics;
using SuouBase;
using System.Xml.Linq;
using SuouBase.Interface;
using SuouBase.Drawing;
using BSystem.Drawing;
namespace SuouBase.Text
{
    [Serializable]
    public class SuouWord:IParse
    {
        #region BaseProperty
        private StringBuilder _Word;
        private char _Color;
        private char _BackColor;
        private char _Font;
        public Color Color
        {
            get { return FontColorDictionary.GetColor(this._Color); }
            set
            {
                this._Color = FontColorDictionary.RegisterColor(value);
            }
        }
        public Color BackColor
        {
            get { return FontColorDictionary.GetColor(this._BackColor); }
            set
            {
                this._BackColor = FontColorDictionary.RegisterColor(value);
            }
        }
        public Font Font
        {
            get { return FontColorDictionary.GetFont(this._Font); }
            set
            {
                this._Font = FontColorDictionary.RegisterFont(value);
            }
        }
        public bool IsTransparent { get; private set; }
        public char Tag { get; private set; }

        #endregion
        #region ReadProperty
        public bool HasTag
        {
            get
            {
                return this.Tag > 0;
            }
        }
        public bool IsComputeSize
        {
            get
            {
                return this._DotIndexList != null;
            }
        }
        public int Height
        {
            get { return this.Font.Height; }
        }
        public int Width
        {
            get { return Size.Width; }
        }
        public int CharCount
        {
            get { return this.Word.Length; }
        }
        public int Length
        {
            get { return this.Word.Length; }
        }
        public Size Size
        {
            get
            {
                return DotIndexList.Size;
            }
        }
        #endregion
        #region Property
        public string Word
        {
            get { return _Word.ToString(); }
            private set { _Word = new StringBuilder(value); }
        }
        public StringBuilder BWord
        {
            get { return _Word; }
            private set { _Word = value; }
        }


        
        
        
        public SuouLetter this[int i]
        {
            get
            {
                if (Word.Length <= i)
                {
                    throw new MyException("Error");
                }
                return new SuouLetter(Word[i],this.Font, this.Color,this.BackColor,this.IsTransparent);
            }
        }
        public SuouLetter Last()
        {
            var c = this[this.Length - 1];
            return c;
        }
        public SuouLetter First()
        {
            var c = this[0];
            return c;
        }
        
        [NonSerialized]
        private LineDotIndexes _DotIndexList;
        public LineDotIndexes DotIndexList
        {
            get 
            {
                if (_DotIndexList == null) this._DotIndexList = this.Font.WordDotIndexes(this.Word);
                return _DotIndexList; 
            }
            private set { _DotIndexList = value; }
        }
        

        public IEnumerator<SuouLetter> GetEnumerator()
        {
            for (int i = 0; i < Word.Length; i++)
                yield return this[i];
        }
        public int GetLetterWidth(int index)
        {
            char c = this.Word[index];
            return this.Font.Width(c);
        }
        #endregion

        #region Constructor
        
        public SuouWord(string w)
            :this(SuouEditorConst.AAFontChar,SuouEditorConst.TextColorChar,w)
        {
        }
        
        public SuouWord(Font f, Color c, string w)
            :this(f, c, w, null,Color.White,false,(char)0)
        {
            
        }
        public SuouWord(char f, char c, string w)
        {
            this._Font = f;
            this._Color = c;
            this.Word = w;
        }
        public SuouWord(Font f, Color c, string w,Color backc,bool transparent,char tag)
            : this(f, c, w, null,backc,transparent,tag)
        {

        }
        
        public SuouWord(SuouWord w)
            : this(w._Font, w._Color, w._Word,w._DotIndexList,w._BackColor,w.IsTransparent,w.Tag)
        {
        }
        
        /*
        public SuouWord(Font f, Color c, string w,LineDotIndexes lineSize,Color backc,bool transparent,char tag)
            :this(f,c,w,lineSize,backc,transparent,(char)0)
        {

        }
        */
        public SuouWord(Font f, Color c, string w, LineDotIndexes lineSize, Color backc, bool transparent,char tag)
        {

            if (f == null) throw new ArgumentNullException();
            if (w == null) throw new ArgumentNullException();
            this.Font = f;
            this.Color = c;
            this.BackColor = backc;
            this.IsTransparent = transparent;
            //this.Type = t;

            this.Word = w;
            this.DotIndexList = lineSize;
            this.Tag = tag;
        }
        private SuouWord(char f, char c, StringBuilder w, LineDotIndexes lineSize, char backc, bool transparent,char tag)
        {
            this._Font = f;
            this._Color = c;
            this._BackColor = backc;
            this.IsTransparent = transparent;
            //this.Type = t;

            this.BWord = w;
            this.DotIndexList = lineSize;
            this.Tag = tag;
        }
        #endregion
        #region BangMethod1
        /*
        public WordComposition TrimLeft()
        {
            var c = this.Word.First();
            this.Word =this.Word.Substring(1);
            var w = new WordComposition(this.Font, this.Color, c.ToString());
            this.DotIndexList = this.Font.WordDotIndexes(this.Word);
            return w;
        }
        public WordComposition SubString(int StartIndex,int Length)
        {
            var SubWord = this.Word.Substring(StartIndex, Length);
            return new WordComposition(this.Font, this.Color, SubWord);
        }
        public WordComposition SubString(int StartIndex)
        {
            return this.SubString(StartIndex, this.Length - StartIndex);
        }
        */
        public SuouWord[] Insert(int n, SuouWord tf)
        {
            if (this.EqualClass(tf))
            {
                string text = this.Word;
                
                this.Word = Word.Insert(n, tf.Word);
                this.DotIndexList = this.Font.WordDotIndexes(this.Word);
                return null;
            }
            else
            {  
                var LeftRight = this.Split(n);
                if (LeftRight == null) throw new Exception();
                var r = new SuouWord[] { LeftRight.Left, tf, LeftRight.Right };

                return r;
            }
        }
        public SplitWordComposition Split(int n)
        {
            var w1 = this.Word.Substring(0, n);
            var w2 = this.Word.Substring(n);
            SuouWord a = null;
            SuouWord b = null;
            
            if (w2.Length == 0)
            {
                a = new SuouWord((Font)this.Font.Clone(), this.Color, w1, this.DotIndexList,this.BackColor,this.IsTransparent,this.Tag);
                b = new SuouWord((Font)this.Font.Clone(), this.Color, w2, this.Font.WordDotIndexes(""),this.BackColor,this.IsTransparent,this.Tag);
            }
            else if (w1.Length == 0)
            {
                a = new SuouWord((Font)this.Font.Clone(), this.Color, w1, this.Font.WordDotIndexes(""),this.BackColor,this.IsTransparent,this.Tag);
                b = new SuouWord((Font)this.Font.Clone(), this.Color, w2, this.DotIndexList,this.BackColor,this.IsTransparent,this.Tag);
            }
            else
            {
                a = new SuouWord((Font)this.Font.Clone(), this.Color, w1,this.BackColor,this.IsTransparent,this.Tag);
                b = new SuouWord((Font)this.Font.Clone(), this.Color, w2,this.BackColor,this.IsTransparent,this.Tag);
            }
            return new SplitWordComposition(a, b);
        }
        public SuouWord SubWord(int index, int Len)
        {
            return this.Split(index).Right.Split(Len).Left;
        }
        [Obsolete]
        /// <summary>
        /// RightWord.Width > RigthWordMinWidth を満たすもっとも短いRigthWordと残りのLeftWordを返します
        /// </summary>
        /// <param name="RigthWordMinWidth"></param>
        /// <returns></returns>
        public SplitWordComposition NearSplit(int RigthWordMinWidth)
        {
            throw new MissingMethodException();
#pragma warning disable
            if (this.Width == 0)
            {
                var left = new SuouWord(this.Font, this.Color, "");
            }
            else if (this.Width < RigthWordMinWidth)
            {
            }
            int nth = this.Length;
            return null;
#pragma warning restore
        }
        #endregion

        #region Method2
        public SuouWord CreateWord(int n)
        {
            string s = "";
            for (int i = 0; i < n; i++)
            {
                s += this.Word;
            }
            var v = new SuouWord(this.Font, this.Color, s);
            return v;
        }
        public SuouWord Clone()
        {

            var w = new SuouWord(this);

            return w;
        }

        public int MeasureWidth(int len)
        {
            string s = Word.Substring(0, len);
            return this.Font.Width(s);
        }



        public SuouLine ToLineComposition()
        {
            var r = new SuouLine(this.Clone());
            return r;
        }
        public static SuouWord Empty(Font f)
        {
            var r = new SuouWord(f, Color.Black, "");
            return r;
        }

        public bool EqualClass(SuouWord tf)
        {
            return this.Font.Equals(tf.Font) && this.Color == tf.Color && this.IsTransparent == tf.IsTransparent && this.Tag == tf.Tag;
        }
        #endregion

        #region XML
        public XElement CreateSerializer()
        {            
            XElement doc = new XElement("SuouWord");
            doc.Add(XMLParse.GetXMLFromColor(this.Color));
            doc.Add(XMLParse.GetXMLFromFont(this.Font));
            doc.Add(new XElement("Word", this.Word));
            doc.Add(new XElement("BackColor", this.BackColor.Name));
            doc.Add(new XElement("IsTransparent", this.IsTransparent));
            doc.Add(new XElement("Tag", (int)this.Tag));
            
            
            return doc;
        }
        public static SuouWord Parse(XElement xe)
        {
            string w = xe.Element("Word").Value;
            Font f = XMLParse.GetFont(xe.Element("Font"));
            Color c = XMLParse.GetColor(xe.Element("Color"));
            Color backc = XMLParse.GetColor(xe.Element("BackColor"));
            bool Istransparent = bool.Parse(xe.Element("IsTransparent").Value);

            var sw = new SuouWord(f, c, w, backc, Istransparent,(char)0);
            return sw;
        }
        #endregion
    }

    public class SplitWordComposition
    {
        public SuouWord Left;
        public SuouWord Right;
        public SplitWordComposition(SuouWord left, SuouWord right)
        {
            this.Left = left;
            this.Right = right;
        }
    }
    public class LineException : MyException
    {
        public LineException(string message, SuouLine line)
            : base(message)
        {
            this.Line = line;
        }
        public SuouLine Line { get; set; }
    }
    public class LineDotIndexException : LineException
    {
        public LineDotIndexException(string message, SuouLine line, int DotIndex, int Width)
            : base(message, line)
        {
            this.DotIndex = DotIndex;
            this.Width = Width;
        }
        public int DotIndex { get; set; }
        public int Width { get; set; }
    }
    public class LineIndexException : LineException
    {
        public LineIndexException(string message, SuouLine line, int Index, int Length)
            : base(message, line)
        {
            this.Index = Index;
            this.Length = Length;
        }
        public int Index { get; set; }
        public int Length { get; set; }
    }
}
