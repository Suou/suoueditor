﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SuouBase.Book;
namespace SuouBase.Text
{
    public class IBookSearchReport
    {
        public IBook Book { get; private set; }
        public IBookPage[] Report { get; private set; }

        public IBookSearchReport(IBook book, IBookPage[] reports)
        {
            this.Book = book;
            this.Report = reports;
        }
    }
    public static class IBookSearch
    {
        public static bool Search(this IBookPage book, string text)
        {
            var p = book.SText.Text.IndexOf(text);
            return p != -1;
        }
        public static IBookSearchReport Search(this IBook book, string text)
        {
            var r = book.BookPages.Where((v) => v.Search(text)).ToArray();
            if(r.Length == 0) return null;
            return new IBookSearchReport(book, r.ToArray());
        }
        public static bool SearchLine(this IBookPage page, string line)
        {
            return page.SText.Lines.Any((v) => v.Sentence == line);
        }
        public static bool SearchLine(this IBookPage page, string[] lines)
        {
            return lines.Any((v) => SearchLine(page,v));
        }
        public static IBookSearchReport SearchLine(this IBook book,string[] lines){
            var p = book.BookPages.Where((v) => v.SearchLine(lines)).ToArray();
            if (p.Length == 0) return null;
            return new IBookSearchReport(book, p);
        }

    }
}
