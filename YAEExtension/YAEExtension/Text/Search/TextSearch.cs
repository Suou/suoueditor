﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Text;
using System.IO;
using SuouBase.Book;
namespace SuouBase.Text.Search
{
    public static class TextSearchExtnsion
    {
        public static BookSearchResult TextSearch(this IBook book,string text)
        {
            var p = book.BookPages.Where((v) => v.SText.Text.IndexOf(text) != -1).Select((v)=>new TextSearchResult(book,v));
            return new BookSearchResult(book, p.ToArray());
        }
    }
    public class BookSearchResult
    {
        public FileInfo IBookInfo { get; set; }
        public TextSearchResult[] Pages { get; set; }
        public BookSearchResult(IBook book, TextSearchResult[] page)
        {
            this.IBookInfo = new FileInfo(book.FullFilePath);
            this.Pages = page;
        }
    }
    public class TextSearchResult
    {
        public FileInfo IBookInfo { get; set; }
        public IBookPage Page { get; set; }
        public TextSearchResult(IBook book, IBookPage page)
        {
            this.IBookInfo = new FileInfo(book.FullFilePath);
            this.Page = page;
        }
    }
}
