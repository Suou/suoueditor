﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase;
using SuouBase.Text;
using BSystem.Text.Encoding;
using SuouBase.Interface;
using SuouBase.Files;
using System.Security.Permissions;
using SuouBase.Controls.Task;
using SuouBase.Book;
using BSystem.Text.Japanese;
namespace SuouBase.Text.Search
{
    public class Search3
    {
        public static IEnumerable<KeyValuePair<string, string>> CreateFirstLinesList(IEnumerable<IBook> books)
        {
            var p = books.Select((v) => new KeyValuePair<string,string>(v.FullFilePath,CreateSearchText(v)));
            foreach (var w in p) yield return w;
        }
        private static string CreateSearchText(IBook book)
        {
            return book.BookPages.Select((w) => w.SText.Lines.First().Sentence).Where((v) => IsAlmostJapaneseOrAlphabet(v)).JoinString("|");
        }
        private static bool IsAlmostJapaneseOrAlphabet(string s)
        {
            var len = (float)s.Length;
            var p = (float)s.Count((c) => c.IsJapaneseOrAlphabet() && !c.IsSpace());
            return p / len > 0.8F;
        }
    }
    public class SearchBooks
    {
        public string CacheFolderPath { get; set; }
        public SearchBooks(string path)
        {
            this.CacheFolderPath = path;
        }
        private string SelectedTreeCachePath(SuouFileTree tree)
        {
            if (this.CacheFolderPath == null) return null;
            var name = tree.FullPath;
            var path = this.CacheFolderPath + "/" + name.GetBHashCode() + ".che";
            return path;            
        }
        public SerializableDictionary<string,string> CreateCache(SuouFileTree tree)
        {
            //Action act = ()=> SuouBase.Text.Search.Search3.CreateFirstLinesList(tree.FileIterator());
            
            var list = SuouBase.Text.Search.Search3.CreateFirstLinesList(tree.FileIterator());
            var task = new SuperTaskDialogTask<KeyValuePair<string, string>>("キャッシュを作成しています", list, "キャッシュを作成しました");

            var countTask = SuperTaskDialogTask<string>.CreateIterateTask("ファイル数を調べています", tree.FileNodeIterator().Select((v) => v.Name), "ファイル数を調べました");
            var rst = TaskDialog.Create("キャッシュファイルの作成",countTask,task);
            if (rst == System.Windows.Forms.DialogResult.OK)
            {
                var sd = new SerializableDictionary<string, string>(task.Result);
                if (this.CacheFolderPath != null)
                {
                    BSystem.XML.NonBinarySerializer.Save(this.SelectedTreeCachePath(tree), sd);
                }
                return sd;
            }
            else
            {
                throw new Exception();
            }
        }
        public SerializableDictionary<string,string> LoadCache(SuouFileTree tree)
        {
            var path = this.SelectedTreeCachePath(tree);
            var item = BSystem.XML.NonBinarySerializer.Load<SerializableDictionary<string, string>>(path);
            if (item != null)
            {
                return item;
            }
            else
            {
                return this.CreateCache(tree);
            }
        }
        public KeyValuePair<string, string>[] Search(string searchWord, SuouFileTree tree)
        {
            var dic = this.LoadCache(tree);
            return dic.Where((v) => v.Value.IndexOf(searchWord) != -1).ToArray();
        }
    }
}
