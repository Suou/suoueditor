﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Text;
using BSystem.Text;
using SuouBase.Book;
namespace SuouBase.Text
{
    /// <summary>
    /// 集合辞書を提供します。
    /// </summary>
    /// <typeparam name="A"></typeparam>
    /// <typeparam name="B"></typeparam>
    [Serializable]
    public class MapDictionary<A, B> : Dictionary<A, List<B>>
    {
        public void AddItem(A key, B value)
        {
            if (this.ContainsKey(key))
            {
                this[key].Add(value);
            }
            else
            {
                this[key] = new List<B>();
                this[key].Add(value);
            }
        }
        public IEnumerable<KeyValuePair<A, B>> AllKeyValues
        {
            get
            {
                foreach (var pair in this)
                {
                    foreach (var value in pair.Value)
                    {
                        yield return new KeyValuePair<A, B>(pair.Key, value);
                    }
                }
            }
        }
    }
    /// <summary>
    /// 指定したIBookPageの各行のハッシュ値の配列を提供します。
    /// </summary>
    public class HashBookPage
    {
        bool IsCreateHash = false;
        //private uint[] _HashLines = null;
        private uint _Hash;

        public uint[] HashLines { get; set; }

        public uint Hash
        {
            get
            {
                if (!this.IsCreateHash) throw new Exception();
                return this._Hash;
            }
            set
            {
                this._Hash = value;
                this.IsCreateHash = true;
            }
        }
        public HashBookPage()
        {
        }
        public static HashBookPage CreateHashLinePage(IBookPage page,bool createHashLines,bool createHash)
        {
            if (createHash)
            {
                return new HashBookPage()
                {
                    HashLines = createHashLines ? page.SText.Lines.Select((v) => v.Hash).ToArray() : null,
                    Hash = page.SText.Hash
                };
            }
            else
            {
                return new HashBookPage()
                {
                    HashLines = createHashLines ? page.SText.Lines.Select((v) => v.Hash).ToArray() : null,
                };

            } 
            
        }
        public static HashBookPage CreateHashPage(IBookPage page)
        {
            var r = new HashBookPage() { Hash = page.SText.Hash };
            return r;
        }
    }

    /// <summary>
    /// 指定したIBookのHashBookPageの配列を提供します。
    /// </summary>
    public class HashBook
    {
        public string FilePath { get; set; }
        public List<HashBookPage> Pages { get; set; }
        public int GetCount()
        {
            return this.Pages.Sum((v) => v.HashLines.Length);
        }
        public HashBook()
        {
            this.Pages = new List<HashBookPage>();
        }
        public static HashBook CreateBook(string filename)
        {

            var book = new HashBook();
            book.FilePath = filename;
            var f = IBookHandler.Load(filename);
            for (int i = 0; i < f.Count; i++)
            {
                book.Pages.Add(HashBookPage.CreateHashLinePage(f[i],true,false));
            }
            return book;
        }
        public IEnumerable<uint> GetHashes()
        {
            return this.Pages.SelectMany((v) => v.HashLines);
        }
        public IEnumerable<LineHashItem> GetHashPairs(ushort bookNumber)
        {
            for (ushort i = 0; i < this.Pages.Count; i++)
            {
                foreach (var hash in this.Pages[i].HashLines)
                {
                    yield return new LineHashItem(hash,bookNumber,i);
                }

            }

        }
    }
}
