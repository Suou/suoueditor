﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Text;
using System.Windows.Forms;
using BSystem;
using BSystem.Collections;
using BSystem.Structure;

namespace SuouBase.Text
{
    public class LineHashItemsOnMainMemory : IReadOnlyArray<LineHashItem>
    {
        LineHashItem[] Items { get; set; }
        public LineHashItem this[long i]
        {
            get
            {
                return this.Items[(int)i];
            }
        }
        public long Length
        {
            get { return this.Items.Length; }
        }
        protected DiskSortedMemory<LineHashItem> Streamer { get; set; }
        public LineHashItemsOnMainMemory(FileInfo fi, DirectoryInfo di)
            : this(new DiskSortedMemory<LineHashItem>(fi, di))
        {
        }
        public LineHashItemsOnMainMemory(DiskSortedMemory<LineHashItem> streamer)
        {
            throw new Exception();
            //this.Streamer = streamer;
            //this.Items = this.Streamer[0, (int)this.Streamer.Length];
        }
    }

    public struct LineHashItem : IndexMemoryItem, IComparer<LineHashItem>
    {
        public byte[] ToBytes()
        {
            List<byte> r = new List<byte>();
            r.AddRange(BitConverter.GetBytes(this.Hash));
            r.AddRange(BitConverter.GetBytes(this.FileNamePointer));
            r.AddRange(BitConverter.GetBytes(this.PagePointer));
            return r.ToArray();
        }
        public int ByteLength { get { return 8; } }
        public uint HashValue { get { return this.Hash; } }
        public BSystem.IO.IKumikomiMemoryItem<uint> CreateItem(byte[] byteitem)
        {
            uint a = BitConverter.ToUInt32(byteitem, 0);
            ushort b = BitConverter.ToUInt16(byteitem, 4);
            ushort c = BitConverter.ToUInt16(byteitem, 6);
            return new LineHashItem(a, b, c);
        }
        public int Compare(LineHashItem a, LineHashItem b)
        {
            return a.HashValue.CompareTo(b.HashValue);
        }

        public uint Hash;
        public ushort FileNamePointer;
        public ushort PagePointer;
        public LineHashItem(uint a, ushort b, ushort c)
        {
            Hash = a;
            FileNamePointer = b;
            this.PagePointer = c;
        }
        public string GetValueCode()
        {
            List<byte> r = new List<byte>();
            r.AddRange(BitConverter.GetBytes(this.FileNamePointer));
            r.AddRange(BitConverter.GetBytes(this.PagePointer));
            return BitConverter.ToString(r.ToArray());
        }
        public static LineHashItem CreateEmptyPointerItem(uint hash)
        {
            return new LineHashItem(hash, 0, 0);
        }
        public BSystem.IO.IKumikomiMemoryItem<uint> CreateItem(uint item)
        {
            throw new NotSupportedException();
        }
        public uint Value
        {
            get
            {
                throw new NotSupportedException();
            }
        }
    }
    
    public class LineHashItemPool : List<LineHashItem>
    {
        public List<string> BookFileNames { get; set; }
        private object LockObject = new object();
        private DiskSortedMemory<LineHashItem> Streamer { get; set; }
        public bool OnMemory { get; set; }

        public LineHashItemPool(DiskSortedMemory<LineHashItem> stream, List<string> bookFileNames)
        {
            this.Streamer = stream;
            this.BookFileNames = bookFileNames;
        }
        public void Add(HashBook book)
        {
            lock (LockObject)
            {
                char n = (char)this.BookFileNames.Count;
                this.BookFileNames.Add(book.FilePath);
                foreach (var pair in book.GetHashPairs(n))
                {
                    base.Add(pair);
                }

                if (this.Count > 100000 && !this.OnMemory) this.Clean();
            }
        }
        public void Clean()
        {
            throw new Exception();
            
            //if (this.OnMemory) this.Sort((a,b)=>a.Compare(a,b));
            ///this.ForEach((v) => this.Streamer.Append(v));
            //this.Clear();
            
        }
        public void Finish()
        {
            this.Clean();
        }

    }
}
