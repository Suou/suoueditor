﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Text;
using System.Windows.Forms;
using BSystem;
using BSystem.Collections;
using BSystem.Structure;
using SuouBase.Book;
namespace SuouBase.Text
{
    /// <summary>
    /// 指定されているフォルダに含まれているAAの行検索を行うクラス
    /// </summary>
    public class LineHashSearch
    {
        protected ILongArray<LineHashItem> Streamer { get; set; }
        public LineHashSubData Subdata { get; set; }
        public SuouBase.Files.BookDictionary BookDic = new Files.BookDictionary();
        public LineHashSearch(LineHashSubData subData, ILongArray<LineHashItem> streamer)
        {
            this.Subdata = subData;
            this.Streamer = streamer;
        }
        #region Search
        /*
        public string[] Search(uint hash)
        {
            List<string> r = new List<string>();
            foreach (var v in this.Streamer)
            {
                if (v.Hash == hash) r.Add(this.Subdata.BookLinearDataItems[v.FileNamePointer].FullFilePath);
            }
            return r.ToArray();
        }
        */
        public LineHashItem[] BinarySearch(uint hash)
        {
            throw new Exception();
            //var r = this.Streamer.BinarySearch(LineHashItem.CreateEmptyPointerItem(hash)).ToList();
            //return r.GroupBy((v) => v.GetValueCode()).Select((v) => v.First()).ToArray();

        }
        public LineHashSearchSubResult[] BinarySearch(IBookPage page,float lowerBound = 0)
        {
            var hashpage = HashBookPage.CreateHashLinePage(page, true, true);
            return this.BinarySearch(page, hashpage,lowerBound);
        }
        public LineHashSearchSubResult[] BinarySearch(IBookPage page, HashBookPage hashPage)
        {
            return this.BinarySearch(page, hashPage, 0);
        }
        public LineHashSearchSubResult[] BinarySearch(IBookPage page,HashBookPage hashPage,float lowerBound)
        {
            var sourceLinearData = new BookPageLinearData(page);
            var f = hashPage.HashLines.SelectMany((v) => this.BinarySearch(v)).GroupBy((v) => v.GetValueCode());
            var r = f.Select((v) => LineHashSearchSubResult.Create(v, sourceLinearData, this.Subdata)).Where((v)=>v.MatchPercent > lowerBound).ToArray();
            Array.Sort(r, (a, b) =>
            {
                if (a.MatchPercent > b.MatchPercent)
                {
                    return -1;
                }
                else if (a.MatchPercent < b.MatchPercent)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            });
            return r;
        }


        public LineHashSearchResult BinarySearchBook(IBookPage page,float lowerBound = 0)
        {
            var pages = BinarySearch(page,lowerBound).ToList();
            return new LineHashSearchResult(pages, this.BookDic);
        }
        public LineHashSearchResult BinarySearchBook(IBookPage page,HashBookPage hashpage)
        {
            var pages = BinarySearch(page,hashpage).ToList();
            return new LineHashSearchResult(pages, this.BookDic);
        }
        /*
        public string[] Search(HashBookPage page)
        {
            var f = page.HashLines.SelectMany((v) => this.Search(v));
            return f.ToArray();
        }
        */
        #endregion
        public void Close()
        {
            
        }
    }

    /// <summary>
    /// 行検索クラスを作成するためのアイテム
    /// </summary>
    [Serializable]
    public abstract class AbstractIndexCreaterWithLineHashSearch
    {
        protected DiskSortedMemory<LineHashItem> Streamer { get; set; }
        public LineHashSubData Sub { get; protected set; }
        #region BaseProperty
        
        public LineHashItemPool HashDic { get; set; }
        public DirectoryInfo IndexDirectoryInfo { get; protected set; }
        public string Name { get; set; }
        public FileInfo IndexInfoFileInfo
        {
            get
            {
                return new FileInfo(this.IndexDirectoryInfo.FullName + "/" + this.Name + "_list.obj");
            }
        }
        public FileInfo IndexInfo
        {
            get
            {
                return new FileInfo(this.IndexDirectoryInfo.FullName + "/" + this.Name + "_index.obj");
            }
        }
        public DirectoryInfo TemporaryFolderInfo
        {
            get
            {
                return new DirectoryInfo(this.IndexDirectoryInfo.FullName + "/" + "temp");
            }
        }

        public bool IsCreatedIndex { get { return this.IndexInfo.Exists; } }
        public bool IsCreatedFileList { get { return this.IndexInfoFileInfo.Exists; } }
        public bool OnMainMemory { get; set; }

        private LineHashItemsOnMainMemory MainMemory { get; set; }
        private ILongArray<LineHashItem> LongItem
        {
            get
            {
                throw new NotSupportedException();
                /*
                if (this.OnMainMemory)
                {
                    this.MainMemory = this.MainMemory ?? new LineHashItemsOnMainMemory(this.Streamer);
                    return this.MainMemory;
                }
                else
                {
                    return this.Streamer;
                }
                */
            }
        }
        private LineHashSearch _Searcher = null;
        public LineHashSearch Searcher
        {
            get
            {
                if (this.IsCreated && this._Searcher == null) this._Searcher = new LineHashSearch(this.Sub, this.LongItem);
                if (!this.IsCreated && this._Searcher == null) throw new Exception();
                return this._Searcher;
            }
        }

        #endregion

        #region Constructor
        protected AbstractIndexCreaterWithLineHashSearch()
        {
        }
        #endregion
        #region Method
        public abstract IEnumerable<IBook> BookIterator();
        
        public void PrepareSearch()
        {
            this.Sub = BSystem.XML.NonBinarySerializer.Load<LineHashSubData>(this.IndexInfoFileInfo.FullName) ?? new LineHashSubData();
            //if (this.BookFileNames == null) this.BookFileNames = new List<string>();
            this.Streamer = DiskSortedMemory<LineHashItem>.Create(this.IndexInfo, this.TemporaryFolderInfo);
            this.HashDic = new LineHashItemPool(this.Streamer, this.Sub.FileNames) { OnMemory = this.OnMainMemory};
        }
        
        public void Delete()
        {
            if (this.IsCreated)
            {
                this.Streamer.Delete();
                this.IndexInfoFileInfo.Delete();
                this.Sub.FileNames = new List<string>();
                this.Streamer = null;
                this.HashDic = null;
            }
        }
        public bool IsCreated
        {
            get
            {
                return this.IsCreatedFileList && this.IsCreatedIndex;
            }
        }
        public long GetLength()
        {
            return this.Streamer.Length;
        }
        public int SortingTimes
        {
            get
            {
                return (int)this.Streamer.Divide;
            }
        }
        public static AbstractIndexCreaterWithLineHashSearch Load(string filename)
        {
            var obj = BSystem.XML.BinarySerialize.LoadFromBinaryFile<AbstractIndexCreaterWithLineHashSearch>(filename);
            return obj;
        }
        public static void EmptyMethod()
        {
        }
        private void CreateIndex(Action t)
        {
            t = t ?? EmptyMethod;
            this.BookIterator()
                .ForEach2((v)=> this.Sub.BookLinearDataItems.Add(new BookLinearData(v)))
                .ForEach2((v) => t())
                .Select((v) => HashBook.CreateBook(v.FullFilePath))
                .ForEach((v) => this.HashDic.Add(v));

            this.HashDic.Finish();
        }
        public abstract int IndexLength { get; }
        

        public void CreateAndSaveIndex(Action t)
        {
            var p = t ?? EmptyMethod;
            this.Create1(p);
            this.Create2();
            this.Create3(p);
            
        }
        public void Create1(Action t)
        {
            this.PrepareSearch();
            this.CreateIndex(t);
        }

        public void Create2()
        {
            BSystem.XML.NonBinarySerializer.Save<LineHashSubData>(this.IndexInfoFileInfo.FullName, this.Sub);
        }

        public void Create3(Action t)
        {
            if(!this.OnMainMemory)this.Streamer.Sort(t);
            
        }

        #endregion
        public bool Dispose()
        {
            this.Streamer.Close();
            return true;
        }
    }

    public class IndexCreaterWithLineHashSearchInFile : AbstractIndexCreaterWithLineHashSearch
    {
        public FileInfo SearchFileInfo { get; set; }
        public IndexCreaterWithLineHashSearchInFile(FileInfo booksInfo, DirectoryInfo booksIndexFolderInfo, string name)
        {
            this.SearchFileInfo = booksInfo;
            this.Name = name;
            this.IndexDirectoryInfo = booksIndexFolderInfo;
            this.Sub = new LineHashSubData();
            if (this.IsCreated) this.PrepareSearch();
        }
        public override IEnumerable<IBook> BookIterator()
        {
            var v = IBookHandler.Load(this.SearchFileInfo.FullName);
            yield return v;
        }
        public override int IndexLength { get { return 1; } }
        
    }
    public class IndexCreaterWithLineHashSearchInDirectory : AbstractIndexCreaterWithLineHashSearch
    {
        public DirectoryInfo BooksFolderInfo { get; protected set; }
        public IndexCreaterWithLineHashSearchInDirectory(DirectoryInfo booksFolderInfo, DirectoryInfo booksIndexFolderInfo, string name)
        {
            this.Name = name;
            this.BooksFolderInfo = booksFolderInfo;
            this.IndexDirectoryInfo = booksIndexFolderInfo;
            this.Sub = new LineHashSubData();
            if (this.IsCreated) this.PrepareSearch();
        }
        public override IEnumerable<IBook> BookIterator()
        {
            return this.BooksFolderInfo.AAFileIteratorAsync();
        }
        public override int IndexLength
        {
            get
            {
                return this.BooksFolderInfo.AAFileNameIterator().Count();
            }
        }
    }

}
