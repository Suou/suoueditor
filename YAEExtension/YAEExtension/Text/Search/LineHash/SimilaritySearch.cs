﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using SuouBase.Book;
namespace SuouBase.Text
{
    public class SimilaritySearch
    {
        LineHashSearch Searcher { get; set; }
        HashSet<uint> PageHashSet = new HashSet<uint>();
        List<IBook> Items = new List<IBook>();
        public float MatchThreshold { get; set; }
        public System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        public System.Diagnostics.Stopwatch sw1 = new System.Diagnostics.Stopwatch();
        public int SearchCount = 0;
        public SimilaritySearch(LineHashSearch searcher)
        {
            this.Searcher = searcher;

            
        }
        public void Search(IEnumerable<IBookPage> pages,float matchThreshold)
        {
            sw1.Start();
            this.MatchThreshold = matchThreshold;
            pages
                
                .Select((v) => new KeyValuePair<IBookPage,HashBookPage>(v,HashBookPage.CreateHashLinePage(v, true, true)))
                .Where((v) => !this.PageHashSet.Contains(v.Value.Hash))
                
                .Select((v) => this.Searcher.BinarySearchBook(v.Key,v.Value))
                
                .Where((v) => v.IsHit)
                .ForEach2((v) => sw.Start())
                .ForEach2((v) => this.Register(v))
                .ForEach((v) =>
                {
                    sw.Stop();
                    sw.Reset();
                    this.SearchCount++
                        ;
                })
                ;
            sw1.Stop();
        }
        public void Register(LineHashSearchResult result)
        {
            
            var book = result.CreateBook(this.MatchThreshold);
            if (book.Count > 1)
            {
                this.Items.Add(book);
            }
            book.BookPages.ForEach((v) => this.PageHashSet.Add(v.SText.Hash));
            
        }
        public void Save(DirectoryInfo info)
        {
            int i = 0;
            if (!info.Exists) info.Create();
            this.Items.ForEach((v) => v.Save(info.FullName+"/"+ string.Format("{0}.ast", i++), false));
        }
    }
}
