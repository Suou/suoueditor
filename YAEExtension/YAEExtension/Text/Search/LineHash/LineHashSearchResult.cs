﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Files;
using SuouBase.Book;
namespace SuouBase.Text
{
    public class LineHashSearchResult
    {
        public string[] FileNameList { get; set; }
        public List<LineHashSearchSubResult> Results { get; set; }
        private BookDictionary Books { get; set; }
        
        public bool IsHit
        {
            get { return this.FileNameList.Length != 0; }
        }
        
        public LineHashSearchResult(List<LineHashSearchSubResult> items,BookDictionary books)
        {
            this.Books = books;
            this.Results = items;
            this.FileNameList = items.Select((v) =>v.FileNameList[v.MatchInfo.FileNamePointer]).Distinct().ToArray();
            //var pages = items.Select((v) => v.CreatePage(books));
        }
        public IBook CreateBook()
        {
            var pages = this.Results.Select((v) => v.CreatePage(this.Books));
            return EditingFile.Create(pages);
        }
        public IBook CreateBook(float matchThreshold)
        {
            var pages = this.Results.Where((v)=>v.MatchPercent > matchThreshold).Select((v) => v.CreatePage(this.Books));
            return EditingFile.CreateReadOnly(pages);
        }
        public LineHashSearchSubResult TopHitData
        {
            get {
                if (this.IsHit)
                {
                    return this.Results.Max2((v) => (int)(v.MatchPercent * 10000));
                }
                else
                {
                    return null;
                }
            }
        }
    }
    public struct HashData
    {
        public ushort FileNamePointer { get; set; }
        public ushort PageNumberPointer { get; set; }
    }
    public class LineHashSearchSubResult
    {
        public HashData MatchInfo { get; set; }
        public IList<string> FileNameList { get; set; }
        public BookPageLinearData MatchData { get; set; }
        public BookPageLinearData SourceData { get; set; }
        public int MatchPoint { get; set; }
        public string FileName
        {
            get
            {
                return this.FileNameList[this.MatchInfo.FileNamePointer];
            }
        }
        public float MatchPercent
        {
            get
            {
                return (float)this.MatchPoint / (float)((this.SourceData.Count + this.MatchData.Count)/2);
            }
        }
        public static LineHashSearchSubResult Create(IGrouping<string,LineHashItem> source,BookPageLinearData sourceData,LineHashSubData data)
        {
            var p = source.First();
            var hashdata = new HashData(){FileNamePointer = p.FileNamePointer,PageNumberPointer = p.PagePointer};
            var matchdata = data.BookLinearDataItems[p.FileNamePointer][p.PagePointer];
            var files = data.FileNames;
            var v = new LineHashSearchSubResult()
            {
                MatchInfo = hashdata,
                FileNameList = files,
                SourceData = sourceData,
                MatchData = matchdata,
                MatchPoint = source.Count()
            };
            return v;
        }
        public AstPage CreatePage(BookDictionary books)
        {
            var text = books[this.FileName][this.MatchInfo.PageNumberPointer].SText;
            var name = this.MatchPercent.ToString();
            return new AstPage(name, text.Text);
        }
    }
}
