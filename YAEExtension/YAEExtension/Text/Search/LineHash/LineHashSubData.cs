﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Book;

namespace SuouBase.Text
{
    [Serializable]
    public class LineHashSubData
    {
        public List<string> FileNames { get; set; }
        public List<BookLinearData> BookLinearDataItems { get; set; }

        public LineHashSubData()
        {
            this.FileNames = new List<string>();
            this.BookLinearDataItems = new List<BookLinearData>();
        }
    }
    [Serializable]
    public class BookLinearData : List<BookPageLinearData>
    {
        public string FullFilePath { get; set; }
        public BookLinearData()
            : base()
        {
        }
        public BookLinearData(IBook book)
            : base()
        {
            this.FullFilePath = book.FullFilePath;
            book.BookPages.Select((v) => new BookPageLinearData(v)).ForEach((v) => this.Add(v));
        }
    }
    [Serializable]
    public class BookPageLinearData
    {
        public int Bytes { get; set; }
        public int Count { get; set; }
        public BookPageLinearData()
        {
        }
        public BookPageLinearData(IBookPage page)
        {
            //this.Bytes = page.Text.GetSitarabaByteSize;
            this.Count = page.SText.Count;
        }
    }
}
