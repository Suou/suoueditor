﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using BSystem.Collections;
using SuouBase.Text;
using SuouBase.Text.Search;
using SuouBase.Book;
namespace SuouBase.Text.Search
{
    class MatchItemCandidateCollection
    {
        public List<MatchedReplaceItem> Items = new List<MatchedReplaceItem>();
    }
    public class MatchedTextLocation
    {
        public List<MatchBand> Items { get; set; }
        public MatchedTextLocation(MatchBand[] items)
        {
            this.Items = items.ToList();
        }
        public int MatchPoint
        {
            get { return this.Items.Sum((v) => v.MatchPoint); }
        }
        public int Top
        {
            get
            {
                return this.Items.Min((v) => v.Y);
            }
        }
        public int Bottom
        {
            get
            {
                return this.Items.Min((v) => v.Y);
            }
        }
        public MatchBand GetMatchBand(int y)
        {
            return this.Items.FirstOrDefault((v) => v.Y == y);
        }
        public bool Overlap(MatchedTextLocation item)
        {
            if (item.Bottom < this.Top) return false;
            if (this.Bottom < item.Bottom) return false;
            return this.Items.Exists((v) => this.Overlap(item,v));
        }
        private bool Overlap(MatchedTextLocation item, MatchBand band)
        {
            var vband = item.GetMatchBand(band.Y);
            return band.Overlap(vband);
        }
    }
    public class LineMergeCollection:List<BasicLineMerge>
    {
        public IBook ParentItem { get; private set; }
        public LineMergeCollection(IBook item)
        {
            this.ParentItem = item;
        }
    }
    public class MatchedReplaceItem
    {
        public LineMergeCollection MergeItems { get; private set; }
        public BasicLineMerge SelectedItem{get;set;}
        public MatchedTextLocation MatchItems { get; private set; }
        public SuouText Text { get; private set; }
        public int SelectedIndex
        {
            get
            {
                return this.MergeItems.FindIndex((v) => v == this.SelectedItem);
            }
        }
        public ISpaceMaker SpaceMaker { get; private set; }
        public char Tag { get; set; }
        public void Replace(int n)
        {
            this.Text.Replace(Tag, this.MergeItems[n], this.SpaceMaker);
        }
        public MatchedReplaceItem(SuouText text, LineMergeCollection mergeitem, char tag,BasicLineMerge selectedItem,ISpaceMaker spaceMaker,MatchedTextLocation items)
        {
            this.Text = text;
            this.MergeItems = mergeitem;
            this.Tag = tag;
            this.SelectedItem = selectedItem;
            this.SpaceMaker = spaceMaker;
            this.MatchItems = items;
        }
        public void TagizeText()
        {
            foreach (var w in this.MatchItems.Items)
            {
                this.Text[w.Y] = this.Text[w.Y].Tagize(w.StartX, w.EndX, this.Tag);
            }
        }
    }
    public class MatchedReplaceItems : List<MatchedReplaceItem>
    {
        public void SuperAdd(MatchedReplaceItem item)
        {
            bool ok = true;
            var r = new List<MatchedReplaceItem>();
            foreach (var v in this)
            {
                if (v.MatchItems.Overlap(item.MatchItems))
                {
                    if (v.MatchItems.MatchPoint <= item.MatchItems.MatchPoint)
                    {
                        ok = false;
                        break;
                    }
                    else
                    {
                        r.Add(v);
                    }
                }
            }
            r.ForEach((v) => this.Remove(v));
            if (ok) this.Add(item);
        }
        public SuouText Text { get; private set; }
        public ISpaceMaker SpaceMaker { get; set; }
        public char MaxTag
        {
            get
            {
                if (this.Count != 0)
                {
                    return this.Max((v) => v.Tag);
                }
                else
                {
                    return (char)1;
                }
            }
        }
        public MatchedReplaceItems(SuouText text)
        {
            this.Text = text;
        }
        public static MatchedReplaceItems Create(SuouText text,LineMergeCollection lmc,ISpaceMaker spacemaker, char tag = (char)1)
        {
            var v = text.Search(lmc, tag,spacemaker);
            v.SpaceMaker = spacemaker;
            return v;
        }
        public static MatchedReplaceItems Create(SuouText text, IBook book, ISpaceMaker spacemaker,char tag = (char)1)
        {
            var p = book.CreateLineMergeCollection(spacemaker);
            return Create(text, p, spacemaker,tag);
        }
        public void InputTag()
        {
            this.ForEach((v) => v.TagizeText());
        }
        public static MatchedReplaceItems Create(SuouText text, IBookCollection books, ISpaceMaker spacemaker)
        {
            char tag = (char)1;
            var r = new MatchedReplaceItems(text);
            foreach (var v in books.Items)
            {
                var w = MatchedReplaceItems.Create(text, v, spacemaker,tag);
                foreach (var item in w)
                {
                    r.SuperAdd(item);
                }
                tag = (char)(r.MaxTag + 1);
            }
            r.InputTag();
            r.Sort((a,b) => b.MatchItems.Top - a.MatchItems.Top);
            return r;
        }
    }

    public class MatchedTextItems
    {
        public BasicLineMerge LineMergeItem { get; private set; }
        public SuouText Text { get; private set; }
        public MatchedTextLocation[] Items { get; private set; }
        public MatchedTextItems(BasicLineMerge a,MatchedTextLocation[] b,SuouText c)
        {
            this.LineMergeItem = a;
            this.Items = b;
            this.Text = c;
        }
        public bool IsMatch
        {
            get { return this.Items.Length != 0; }
        }
        public MatchBand GetBand(int n, int y)
        {
            var p = this.Items[n];
            //var v = new Band(p.Items[y], this.LineMergeItem.GetLineEnumerator().ElementAt(y).Length);
            return p.Items[y];
        }
        public IEnumerable<MatchBand> GetBands(int n)
        {
            for (int i = 0; i < this.Items[n].Items.Count; i++)
            {
                yield return this.GetBand(n, i);
            }
        }
        public SuouText GetTransparentizeText(int n)
        {
            var v = this.Text.Clone();
            foreach (var w in this.GetBands(n))
            {
                v[w.Y] = v[w.Y].PaintTransparent(w.StartX, w.EndX);
            }
            return v;
        }
        public void GetTagizeText(int n,char tag)
        {
            this.GetTagizeText(n, tag, this.Text.Clone());
        }
        private void GetTagizeText(int n, char tag,SuouText text)
        {
            foreach (var w in this.GetBands(n))
            {
                text[w.Y] = text[w.Y].Tagize(w.StartX, w.EndX, tag);
            }
        }
        public SuouText GetTagizeText(char tag)
        {
            for (int i = 0; i < this.Items.Length; i++)
            {
                this.GetTagizeText(i, tag++, this.Text);
            }
            return this.Text;
        }
        public MatchedReplaceItem[] CreateReplacer(LineMergeCollection lmc,char tag,ISpaceMaker spaceMaker)
        {
            var r = new List<MatchedReplaceItem>();
            //var v = this.GetTagizeText(tag);
            for (int i = 0; i < this.Items.Length; i++)
            {
                var w = new MatchedReplaceItem(this.Text,lmc,(char)(tag+i),this.LineMergeItem,spaceMaker,this.Items[i]);
                r.Add(w);
            }
            return r.ToArray();
        }
        public SuouText Spacize(int n,ISpaceMaker sm)
        {
            var v = this.Text.Clone();
            foreach (var w in this.GetBands(n))
            {
                v[w.Y] = v[w.Y].Spacize(w.StartX, w.Length, sm);
            }
            return v;
        }
        public SuouText Replace(int n, SuouText layer, AltLine al)
        {
            
            var text = this.Spacize(n,al.SpaceMaker);
            var bands = this.GetBands(n);
            var pairs = bands.Zip(layer.Lines);
            foreach (var w in pairs)
            {
                text[w.Key.Y] = text[w.Key.Y].Spacize(w.Key.StartX, w.Key.Length, al.SpaceMaker);
                var width = text[w.Key.Y].Subline(0, w.Key.StartX).Width;
                text[w.Key.Y] = al.MergeStringCarelessly(text[w.Key.Y], w.Value, width);
            }
            return text;
        }
    }
    
}
