﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase;
using SuouBase.Text;
using System.Diagnostics;
namespace SuouBase.Text
{
    class SpaceItemSequencer
    {
        public SpaceItemSequencer(List<SpaceEDItem> items)
        {
            this.Items = items.Where((v) => !v.Item.HankakuSpace).Select((v) => v.Copy()).ToList();
            var w = items.First((v) => v.Item.HankakuSpace);
            if (w != null) this.HankakuItem = w.Copy();
        }
        private List<SpaceEDItem> Items { get; set; }
        private SpaceEDItem HankakuItem { get; set; }
        private bool ContainHankaku
        {
            get
            {
                return this.HankakuItem != null;
            }
        }

        private bool PermitFirstHankaku { get; set; }

        private IEnumerable<SpaceItem> Create()
        {
            bool IsFirst = true;
            var hankakuStack = this.HankakuStack();
            var normalStack = this.NormalItemStack();
            while (hankakuStack.Count > 0 || normalStack.Count > 0)
            {
                if (!IsFirst && hankakuStack.Count > 0) yield return hankakuStack.Pop();
                if (normalStack.Count > 0) yield return normalStack.Pop();
                IsFirst = false;
            }
        }
        private IEnumerable<SpaceItem> Create2(bool IsFirst)
        {
            bool IsHankaku = false;
            bool IsLastHankaku = false;
            foreach (var v in this.Items)
            {
                if (IsFirst && !this.PermitFirstHankaku && v.Item.HankakuSpace)
                {
                    continue;
                }
                else
                {
                    v.Num--;
                    yield return v.Item;
                    if (IsFirst) IsFirst = false;
                    if (v.Item.HankakuSpace)
                    {
                        IsHankaku = true;
                        IsLastHankaku = true;
                    }
                    else
                    {
                        IsLastHankaku = false;
                    }
                    if (IsHankaku && !IsLastHankaku) break;

                }
            }

        }
        private Stack<SpaceItem> HankakuStack()
        {
            var r = new Stack<SpaceItem>();
            if (this.HankakuItem != null)
            {
                while (this.HankakuItem.Num > 0)
                {
                    this.HankakuItem.Num--;
                    r.Push(this.HankakuItem.Item);
                }
            }
            return r;
        }
        private Stack<SpaceItem> NormalItemStack()
        {
            //var r = new Stack<SpaceItem>();
            var r = new List<SpaceItem>();
            while (this.Items.Count > 0)
            {
                this.Items = this.Items.Where((v) => v.Num > 0).ToList();
                this.Items.Sort((a, b) => (a.Item.IsSpace ? 0 : 1) - (b.Item.IsSpace ? 0 : 1));
                foreach (var v in this.Items)
                {
                    for (int i = 0; i < v.Num; i++)
                    {
                        r.Add(v.Item);
                    }
                    v.Num = 0;
                }
                this.Items.Clear();
            }
            //r.Reverse();
            return new Stack<SpaceItem>(r);

        }

        #region Create
        public static SuouLine Create(List<SpaceEDItem> items, Color c, Font f)
        {
            var v = new SpaceItemSequencer(items);
            var p = v.Create();
            var r = SuouLine.Empty(f, c);
            foreach (var w in p)
            {
                r += w.SuouWord;
            }
            return r;
        }
        public static string LightCreate(List<SpaceEDItem> items)
        {
            var v = new SpaceItemSequencer(items);
            var p = v.Create();
            StringBuilder s = new StringBuilder();
            foreach (var w in p)
            {
                s.Append(w.SuouWord.Word);
            }
            return s.ToString();
        }
        #endregion
    }
}
