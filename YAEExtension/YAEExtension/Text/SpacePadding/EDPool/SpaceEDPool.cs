﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase;
using SuouBase.Text;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Diagnostics;

namespace SuouBase.Text
{
    class SpaceEDItem
    {
        public SpaceItem Item { get; set; }
        public int Num { get; set; }
        public SpaceEDItem Copy()
        {
            return new SpaceEDItem(this);
        }
        public SpaceEDItem(SpaceItem item)
        {
            this.Item = item;
            this.Num = 1;
        }
        public SpaceEDItem(SpaceEDItem item)
        {
            this.Item = item.Item;
            this.Num = item.Num;
        }
        public static SpaceEDItem Empty(SpaceItem item)
        {
            var v = new SpaceEDItem(item);
            v.Num = 0;
            return v;
        }
    }
    class SpaceEDSet:List<SpaceEDItem>
    {
        public List<SpaceEDItem> Items { get; set; }
        public SpaceLineSet Setting { get; private set; }
        public int LimitDirty
        {
            get { return this.Setting.PermitDirtyLength; }
        }
        public SpacePriorityType Type
        {
            get { return this.Setting.PriorityType; }
        }
        public SpaceEDSet(SpaceLineSet set)
        {
            if (set.Items == null) throw new MyException("");

            this.Items = set.Items.Select((v) => SpaceEDItem.Empty(v)).ToList();
            this.Setting = set;
        }
        public SpaceEDSet(SpaceEDSet item)
        {
            this.Items = item.Items.Select((v) => v.Copy()).ToList();
            this.Setting = item.Setting;
        }
        public static SpaceEDSet Empty(SpaceLineSet set)
        {
            return new SpaceEDSet(set);
        }
        public void Add(SpaceItem item)
        {
            var v = this.Items.FindIndex((w) => w.Item == item);
            if (v == -1)
            {
                this.Items.Add(new SpaceEDItem(item));
            }
            else
            {
                this.Items[v].Num++;
            }
        }
        public SpaceEDSet Copy()
        {
            return new SpaceEDSet(this);
        }
        public int Length
        {
            get
            {
                return this.Items.Sum((v) => v.Num);
            }
        }
        public int Point
        {
            get
            {
                if (!this.Check()) return int.MaxValue;
                if (this.Type == SpacePriorityType.Clean)
                {
                    return this.Dirty;
                }
                else if (this.Type == SpacePriorityType.EucByte)
                {
                    return this.EucByte;
                }
                return this.Dirty;
            }
        }
        public int EucByte
        {
            get
            {
                return this.Items.Sum((v) => v.Item.Word.LenB_EUC() * v.Num);
            }
        }
        public static SpaceEDSet operator +(SpaceEDSet a, SpaceItem b)
        {
            var f = a.Copy();
            f.Add(b);
            return f;
        }
        public bool Check()
        {

            var s = SpaceItemSequencer.LightCreate(this.Items);
            if (this.LimitDirty != -1)
            {
                if (this.LimitDirty < this.CountDirty(s)) return false;
            }
            if (this.ContainRenzokuHankaku(s)) return false;
            if (this.ContainFirstHankaku(s)) return false;
            return true;
        }
        public SuouLine Create(Font f, Color c)
        {
            return SpaceItemSequencer.Create(this.Items, c, f);
        }
        public int CountRenzokuDirty(string s)
        {
            List<int> r = new List<int>();
            int n = 0;
            foreach (var c in s)
            {
                if (c.IsSpace())
                {
                    r.Add(n);
                    n = 0;
                }
                else
                {
                    n++;
                }
            }
            r.Add(n);
            return r.Max();
        }
        public int CountDirty(string s)
        {
            return s.Count((v) => !v.IsSpace());
        }
        public int Dirty
        {
            get
            {
                return this.Items.Sum((v) => v.Item.IsSpace ? 0 : v.Num);
            }
        }
        public bool ContainRenzokuHankaku(string s)
        {
            for (int i = 0; i < s.Length - 1; i++)
            {
                if (s[i] == s[i + 1] && s[i] == ' ') return true;
            }
            return false;
        }
        public bool ContainFirstHankaku(string s)
        {
            if (s.Length == 0) return false;
            return s[0] == ' ';
        }

    }
    class SpaceEditDistancePool : Dictionary<int, SpaceEDSet>
    {
        private SpaceLineSet Item { get; set; }

        public SpaceEditDistancePool(SpaceLineSet item)
        {
            this.Item = item;
            //this.Dic = new Dictionary<int, NumberSpaceSet>();
            this[0] = SpaceEDSet.Empty(this.Item);
        }
        private SpaceEDSet Compute(int length, SpaceItem item)
        {
            var AfterLength = length - item.Width;
            var r = this.Compute(AfterLength);
            if (r == null)
            {
                return null;
            }
            else
            {
                return r + item;
            }
        }

        private SpaceEDSet Compute(int length)
        {
            if (this.ContainsKey(length))
            {
                return this[length];
            }
            else if (length < 0)
            {
                return null;
            }
            else
            {
                var Candidates = this.Item.Items.Select((v) => this.Compute(length, v)).Where((v) => v != null);
                SpaceEDSet r = null;
                if (Candidates.Count() != 0)
                {
                    r = Candidates.Min2((v) => v.Point);
                }
                this[length] = r;
                return r;
            }

        }

        public SuouLine Create(int length, Font f, Color c)
        {
            var r = this.Compute(length);

            if (r != null && r.Check())
            {
                return r.Create(f, c);
            }
            else
            {
                return null;
            }
        }
        public bool CanCreate(int length)
        {
            var r = this.Compute(length);
            return r != null && r.Check();
        }
    }
}
