﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase;
using SuouBase.Text;
using System.Diagnostics;
using BSystem.Collections;
using BSystem.Drawing;
namespace SuouBase.Text
{
    /// <summary>
    /// 定義
    /// Create......与えられた数字の引数だけを使って文字列結果を返すこと
    /// Can...Create...与えられた数字の引数を使って文字列を生成できるか調べる
    /// GetSubString...文字列から部分文字列を取得する。修正または空白の追加は行わない。
    /// Strictrly...与えられた長さと開始位置を厳密に守る。
    /// Carelessly...与えられた長さと開始位置の要求にもっとも近い答えを返す。
    /// Add.........与えられた文字列に何らかの文字列を追加して返す
    /// </summary>
    public interface ISpaceMaker
    {
        SuouLine CreateSpace(int len);
        SuouLine CreateSpaceCarelessly(int len);
        SuouLine CreateSpaceCarelesslyNotShort(int len);
        bool CanCreateSpace(int len);
        bool IsSpace(char c);
        int CanCreateSpaceNum(int n);
    }
    public static class SpaceMakerDic
    {
        private static Dictionary<int, SpaceMaker> Dic = new Dictionary<int, SpaceMaker>();
        public static SpaceMaker Create(SpaceLineSet set, Font f, Color c)
        {
            if (set == null) throw new MyException("合成設定が存在しません");
            if (f == null) throw new MyException("フォントが存在しません");
            var n = set.GetHashCode() ^ f.GetSuperHashCode() ^ c.GetHashCode();
            if (!Dic.ContainsKey(n))
            {
                Dic[n] = new SpaceMaker(set, f, c);
            }
            return Dic[n];
        }
    }
    public enum SpacePriorityType
    {
        Clean, EucByte
    }
    class AltStaticSpace
    {
        public Dictionary<SpaceLineSet, ISpaceMaker> SpaceMakingDic = new Dictionary<SpaceLineSet, ISpaceMaker>();
    }
    public enum SpaceLineSetType
    {
        Default, Uni, Uni2, HukidasiDefault, CompleteClean, Other
    }
    public static class StringExtension
    {
        public static MatchBand FindWithoutSpace(this string text, string pattern, ISpaceMaker sm)
        {
            return PatternMatchWithoutSpace.Match(text, pattern, sm);
        }
    }
    public class PatternMatchWithoutSpace
    {
        public string Text { get; set; }
        public string MatchText { get; set; }
        public string Pattern { get; set; }
        public string TextWithoutSpace { get; set; }
        public string PatternWithoutSpace { get; set; }
        public ISpaceMaker SpaceCreater { get; set; }
        public Font Font { get; set; }

        public PatternMatchWithoutSpace(string text, string pattern, ISpaceMaker sm)
        {
            this.SpaceCreater = sm;
            this.Text = text;
            this.Pattern = pattern;
            this.TextWithoutSpace = this.WithoutSpace(text);
            this.PatternWithoutSpace = this.WithoutSpace(pattern);
            this.Font = SuouEditorConst.AAFont;
        }
        public static MatchBand Match(string text, string pattern, ISpaceMaker sm)
        {
            var v = new PatternMatchWithoutSpace(text, pattern, sm);

            var n = v.Find();
            return n;
        }
        public MatchBand Find()
        {
            var n = this.TextWithoutSpace.IndexOf(this.PatternWithoutSpace);
            var pos = this.Translate(n);
            var len = this.MeasureLength(pos);
            int point = -1;
            if (pos != -1)
            {
                this.MatchText = this.Text.Substring(pos, len);
                point = this.ComputeMatchPoint();
            }

            return new MatchBand(pos, len, point);
        }
        public int[] GetSpaceWidthes(string line)
        {
            var r = new List<int>();
            int t = 0;
            foreach (var c in line)
            {
                if (this.SpaceCreater.IsSpace(c))
                {
                    t += this.Font.Width(c);
                }
                else
                {
                    r.Add(t);
                    t = 0;
                }
            }
            return r.ToArray();
        }
        public int ComputeMatchPoint()
        {
            var a = this.GetSpaceWidthes(this.MatchText);
            var b = this.GetSpaceWidthes(this.Pattern);
            return a.Zip(b).Sum((v) => Math.Abs(v.Key - v.Value));
        }
        public int Translate(int n)
        {
            int t = 0;
            if (n == -1) return n;
            for (int i = 0; i < this.Text.Length; i++)
            {
                if (!this.SpaceCreater.IsSpace(this.Text[i]))
                {
                    if (t == n) return i;
                    t++;
                }
            }
            return -1;
        }
        public int MeasureLength(int n)
        {
            int t = 0, p = 0;

            if (n == -1) return -1;
            for (int i = n; i < this.Text.Length; i++)
            {
                t++;
                if (!this.SpaceCreater.IsSpace(this.Text[i]))
                {
                    p++;
                    if (p >= this.PatternWithoutSpace.Length) break;
                }
            }
            return t;
        }
        public string WithoutSpace(string s)
        {
            StringBuilder sb = new StringBuilder("");
            foreach (var c in s)
            {
                if (!this.SpaceCreater.IsSpace(c)) sb.Append(c);
            }
            return sb.ToString();
        }
    }
    public static class SpaceChars
    {
        public static char Uni1 = ' ';
        public static char Uni2 = ' ';
        public static char Uni3 = ' ';
        public static char Uni4 = ' ';
        public static char Uni5 = ' ';
        public static char[] UniSpaces = new char[] { Uni1, Uni2, Uni3, Uni4, Uni5 };
        public static char SJIS11 = '　';
        public static char SJIS5 = ' ';
        public static char ZenkakuSpace = '　';
        public static char HankakuSpace = ' ';

        public static char PiriodSpace = '.';

        public static char[] Spaces = new char[] { SJIS11, SJIS5, Uni1, Uni2, Uni3, Uni4, Uni5 };
        public static char[] AASpaces = new char[] { PiriodSpace,SJIS11, SJIS5, Uni1, Uni2, Uni3, Uni4, Uni5 };
    }
}