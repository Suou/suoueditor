﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Text
{
    public class SimpleSpaceMaker:ISpaceMaker
    {
        public SuouWord Item { get; private set; }
        public SimpleSpaceMaker(string s)
        {
            this.Item = new SuouWord(s);
        }
        public SuouLine CreateSpace(int len)
        {
            var v = this.CreateSpaceCarelessly(len);
            if (v.Width != len) throw new MyException("");
            return v;
        }
        public SuouLine CreateSpaceCarelessly(int len)
        {
            var v = SuouLine.Empty(this.Item.ToLineComposition());
            foreach (var w in this.Iterator())
            {
                v += w;
                if (v.Width >= len) break;
            }
            return v;
        }
        public SuouLine CreateSpaceCarelesslyNotShort(int len)
        {
            return this.CreateSpaceCarelessly(len);
        }
        public bool CanCreateSpace(int len)
        {
            var v = this.CreateSpaceCarelessly(len);
            return v.Width == len;
        }
        public bool IsSpace(char c)
        {
            return false;
        }
        public int CanCreateSpaceNum(int n)
        {
            return -1;
        }

        private IEnumerable<SuouLetter> Iterator()
        {
            while (true)
            {
                foreach (var c in this.Item)
                {
                    yield return c;
                }
            }
        }
    }
}
