﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Xml.Linq;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Xml;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Windows.Forms.VisualStyles;
using System.Runtime.CompilerServices;
using System.Security.Policy;

namespace SuouBase.Text
{
    [Serializable]
    public class SpaceLineSetXML
    {
        #region BaseProperty
        public List<SpaceItem> _Items = new List<SpaceItem>();
        
        public string Name { get; set; }
        public int PermitDirtyLength { get; set; }
        public int MinCaliculate { get; set; }
        public int _PriorityType = (int)SpacePriorityType.Clean;

        public SpaceLineSetXML()
        {
        }
        public SpaceLineSetXML(SpaceLineSetXML item)
        {
            this._Items = item._Items;
            this.Name = item.Name;
            this.PermitDirtyLength = item.PermitDirtyLength;
            this.MinCaliculate = item.MinCaliculate;
            this._PriorityType = item._PriorityType;
        }

        #endregion
    }
    [Serializable]
    [ImmutableObject(true)]
    public class SpaceLineSet:SpaceLineSetXML,IParse
    {
        public IList<SpaceItem> Items
        {
            get { return base._Items; }
        }

        public new int PermitDirtyLength{
            get { return base.PermitDirtyLength; }
        }
        public new int MinCaliculate
        {
            get { return base.MinCaliculate; }
        }
        public SpacePriorityType PriorityType
        {
            get { return (SpacePriorityType)base._PriorityType; }
        }
        public SpaceLineSet():base()
        {
        }

        public bool isPerfect
        {
            get
            {
                return this._Items.Exists((v) => v.Width == 1);
            }
        }
        public bool isUnicodeSet
        {
            get
            {
                return this.Name == SpaceLineSet.UniDefaultSetName;
            }
        }

        public SpaceLineSet(int minCaliculate,int permitRenzokuDirty,SpacePriorityType type,IList<SpaceItem> items,string name)
        {
            if (items == null) throw new ArgumentNullException();
            base.MinCaliculate = minCaliculate;
            base.PermitDirtyLength = permitRenzokuDirty;
            base._PriorityType = (int)type;
            items.ForEach((v) => base._Items.Add(v));
            base.Name = name;
        }
        public SpaceLineSet(SpaceLineSetXML item):base(item)
        {
        }
        public SpaceLineSet(int minCaliculate, int permitRenzokuDirty, SpacePriorityType type, IList<SpaceItem> items)
            :this(minCaliculate,permitRenzokuDirty,type,items,"")
        {
        }
        
        #region Default

        static SpaceLineSet()
        {
            //throw new MyException("Error");
            _DefaultSet = SpaceLineSet.Default();
            _UniDefaultSet = SpaceLineSet.UniDefault();
            _UniDefaultSet2 = SpaceLineSet.UniDefault2();
            _CompleteCleanDefaultSet = SpaceLineSet.CompleteCleanDefault();
            _HukidasiDefaultSet = SpaceLineSet.HukidasiDefault();

        }

        private readonly static SpaceLineSet _DefaultSet;
        private readonly static SpaceLineSet _UniDefaultSet;
        private readonly static SpaceLineSet _UniDefaultSet2;
        private readonly static SpaceLineSet _CompleteCleanDefaultSet;
        private readonly static SpaceLineSet _HukidasiDefaultSet;

        public static SpaceLineSet DefaultSet { get { return SpaceLineSet._DefaultSet; } }
        public static SpaceLineSet UniDefaultSet { get { return SpaceLineSet._UniDefaultSet; } }
        public static SpaceLineSet UniDefaultSet2 { get { return SpaceLineSet._UniDefaultSet2; } }
        public static SpaceLineSet CompleteCleanDefaultSet { get { return SpaceLineSet._CompleteCleanDefaultSet; } }
        public static SpaceLineSet HukidasiDefaultSet { get { return SpaceLineSet._HukidasiDefaultSet; } }

        public static string UniDefaultSetName
        {
            get
            {
                return "ユニコ空白型";
            }
        }

        private static SpaceLineSet Default()
        {
            var aAlt = new SpaceItem('　');
            var bAlt = new SpaceItem(' ');
            var cAlt = new SpaceItem('.');
            var f = new List<SpaceItem>() { aAlt, bAlt, cAlt };
            var fs = new SpaceLineSet(300,2,SpacePriorityType.Clean,f);

            
            fs.Name = "標準空白型";

            return fs;
        }
        private static SpaceLineSet UniDefault()
        {
            var aAlt = new SpaceItem(SpaceChars.Uni1);
            var bAlt = new SpaceItem(SpaceChars.Uni2);
            var UniAAlt = new SpaceItem(SpaceChars.Uni3);
            var UniBAlt = new SpaceItem(SpaceChars.Uni4);
            var UniCAlt = new SpaceItem(SpaceChars.ZenkakuSpace);
            var UniDAlt = new SpaceItem(SpaceChars.HankakuSpace);
            var f = new List<SpaceItem>() { aAlt, bAlt, UniAAlt, UniBAlt, UniCAlt, UniDAlt};
            var fs = new SpaceLineSet(200,-1,SpacePriorityType.EucByte,f);
            
            fs.Name = UniDefaultSetName;
            return fs;
        }
        private static SpaceLineSet UniDefault2()
        {
            var aAlt = new SpaceItem(SpaceChars.Uni1);
            var bAlt = new SpaceItem(SpaceChars.Uni2);
            var UniAAlt = new SpaceItem(SpaceChars.Uni3);
            var UniBAlt = new SpaceItem(SpaceChars.Uni4);
            var UniCAlt = new SpaceItem(SpaceChars.ZenkakuSpace);
            var UniDAlt = new SpaceItem(SpaceChars.HankakuSpace);
            var e = new SpaceItem('.');
            var f = new List<SpaceItem>() { aAlt, bAlt, UniAAlt, UniBAlt, UniCAlt, UniDAlt,e };
            var fs = new SpaceLineSet(200, 0, SpacePriorityType.EucByte, f);

            fs.Name = "ピリオド＋ユニコ空白型";
            return fs;
        }
        private static SpaceLineSet CompleteCleanDefault()
        {
            
            var aAlt = new SpaceItem(SpaceChars.ZenkakuSpace);
            var bAlt = new SpaceItem(SpaceChars.HankakuSpace);
            var f = new List<SpaceItem>() { aAlt, bAlt };
            var fs = new SpaceLineSet(300,-1,SpacePriorityType.Clean,f);
            
            fs.Name = "完全空白型";
            return fs;
        }
        private static SpaceLineSet HukidasiDefault()
        {
            var aAlt = new SpaceItem(SpaceChars.ZenkakuSpace);
            var bAlt = new SpaceItem(SpaceChars.HankakuSpace);
            var cAlt = new SpaceItem('.');
            
            var f = new List<SpaceItem>() { aAlt, bAlt,cAlt };
            var fs = new SpaceLineSet(300, 1, SpacePriorityType.Clean, f);

            fs.Name = "フキダシ内部用空白型";
            return fs;
        }
        #endregion

        public SpaceLineSetType Type
        {
            get
            {
                var n = this.GetHashCode();
                if (n == SpaceLineSet.DefaultSet.GetHashCode()) return SpaceLineSetType.Default;
                if (n == SpaceLineSet.UniDefaultSet.GetHashCode()) return SpaceLineSetType.Uni;
                if (n == SpaceLineSet.UniDefaultSet2.GetHashCode()) return SpaceLineSetType.Uni2;
                if (n == SpaceLineSet.CompleteCleanDefaultSet.GetHashCode()) return SpaceLineSetType.CompleteClean;
                if (n == SpaceLineSet.HukidasiDefaultSet.GetHashCode()) return SpaceLineSetType.HukidasiDefault;
                return SpaceLineSetType.Other;
            }
        }
        public SpaceItem MostWideChar
        {
            get { return this.Items.Max2((v) => v.Width); }
        }
        public bool ContainsSpace
        {
            get
            {
                return this.Items.Any((v) => v.IsSpace);
            }
        }

        public bool Check()
        {
            if (this.Items.Count == 0) return false;
            if (this.Items.All((v) => v.HankakuSpace)) return false;

            return true;
        }
        public override bool Equals(object obj)
        {
            if (!(obj is SpaceLineSet)) return false;
            var objm = (SpaceLineSet)obj;
            if (this.Items.Count != objm.Items.Count) return false;
            for (int i = 0; i < this.Items.Count; i++)
            {
                if (this.Items[i] != objm.Items[i]) return false;
            }
            return true;
        }
        public override int GetHashCode()
        {
            int n = 0;
            this.Items.ForEach((v) => n = n ^ v.GetHashCode());
            n = n ^ this.PriorityType.GetHashCode();
            return n;
        }
        public SpaceLineSet Clone()
        {
            var f = this.Items.Select((v) => v.Clone()).ToList();
            var r = new SpaceLineSet(this.MinCaliculate,this.PermitDirtyLength,this.PriorityType,this.Items);
            r.Name = this.Name;

            return r;
        }
        /// <summary>
        /// downTextの上にLayerTextを合成した結果を返します。
        /// </summary>
        /// <param name="downText"></param>
        /// <param name="layerText"></param>
        /// <param name="location"></param>
        /// <param name="h"></param>
        /// <param name="zureLength"></param>
        /// <returns></returns>
        public SuouText MergeText(SuouText downText, BasicLineMerge layerText, EditPoint location, MergeZureOption h, int zureLength)
        {
            var copyDownText = downText.Clone();
            var merger = AltText.Create(this, copyDownText.Font, copyDownText.Color);
            layerText.Location = location;
            switch (h)
            {
                case MergeZureOption.Free:
                    merger.MergeEachLineCarelessly(copyDownText, layerText);
                    break;
                case MergeZureOption.LineRestriction:
                    merger.MergeTextCarelessly(copyDownText, layerText);
                    break;
                case MergeZureOption.PermitZureLine:
                    merger.MergeTextCarelessly(copyDownText, layerText, zureLength);
                    break;
                case MergeZureOption.Destructive:
                    this.DestructiveMerge(copyDownText, layerText, location, merger);
                    break;
                case MergeZureOption.HighDestructive:
                    merger.BigBangMergeText(copyDownText, layerText);
                    break;
            }
            return copyDownText;
        }
        private static void PerfectMergeLine(StringLineData DownString, StringLineData UpString, ISpaceMaker unico)
        {
            if(DownString.X < UpString.X)
            {
                var x = DownString.X;
                UpString.X -= DownString.X;
                DownString.X = 0;
                var ReplaceWidth = UpString.Line.Width;

                var DownStringLeftCenterRight = DownString.Line.TripleSplit(unico, UpString.X, ReplaceWidth);
                ThreeSplitLine line = new ThreeSplitLine(DownStringLeftCenterRight.Left, UpString.Line, DownStringLeftCenterRight.Right);
                DownString.Line = line.SpaceJoin(unico);
                DownString.X = x;
            }
            else
            {
                var x = UpString.X;
                if(UpString.X < 0)
                {

                }
                else
                {
                    DownString.X -= UpString.X;
                    UpString.X = 0;
                    var upStringEnd = UpString.Line.Width;
                    var downStringEnd = DownString.X + DownString.Line.Width;
                    if (downStringEnd <= upStringEnd)
                    {
                        DownString.Line = UpString.Line;
                        DownString.X = x;
                    }
                    else if (upStringEnd <= DownString.X)
                    {
                        var width = DownString.X - UpString.Line.Width;
                        var centerSpace = unico.CreateSpace(width);
                        var split = new ThreeSplitLine(UpString.Line, centerSpace, DownString.Line);
                        DownString.Line = split.SpaceJoin(unico);
                        DownString.X += x;
                    }
                    else
                    {
                        var trimWidth = UpString.Line.Width - DownString.X;
                        var DownStringLeftCenterRight = DownString.Line.TripleSplit(unico, 0, trimWidth);
                        var centerSpace = unico.CreateSpace(0);
                        var split = new ThreeSplitLine(UpString.Line, centerSpace, DownStringLeftCenterRight.Right);
                        DownString.Line = split.SpaceJoin(unico);
                        DownString.X = x;
                    }
                }

            }
        }

        private static StringLineData mergeLines(KeyValuePair<int, StringLineData>[] lines, ISpaceMaker unico)
        {
            var tmp = lines.ToList();
            tmp.Sort((a, b) => a.Key - b.Key);
            var fst = tmp.First().Value;
            for(var i = 1; i < tmp.Count; i++)
            {
                PerfectMergeLine(fst, tmp[i].Value, unico);
            }
            return fst;

        }

        private static StringLineData[][] ToSortedStringLineArrays(BasicLineMerge[] layerTexts, ISpaceMaker unico)
        {
            List<List<KeyValuePair<int, StringLineData>>> output = new List<List<KeyValuePair<int, StringLineData>>>();
            //Dictionary<int, List<Tuple<int, StringLineData>>> dic;
            for(var i = 0; i < layerTexts.Length; i++)
            {
                layerTexts[i].GetPlaneEnumerator().ForEach((v) =>
                {
                    //var y = v.Key + layerTexts[i].Location.Y;
                    if(v.Y >= 0)
                    {
                        while (output.Count <= v.Y)
                        {
                            output.Add(new List<KeyValuePair<int, StringLineData>>());
                        }
                        output[v.Y].Add(new KeyValuePair<int, StringLineData>(i, new StringLineData(v.Line, v.X)));

                    }

                    //v.ForEach((w) =>
                    //{
                    //});

                });
            }
            StringLineData[][] output2 = new StringLineData[output.Count][];

            List<KeyValuePair<int, StringLineData>> tmp = new List<KeyValuePair<int, StringLineData>>();
            List<StringLineData> lineTmp = new List<StringLineData>();
            for (var i = 0; i < output.Count; i++)
            {
                output[i].Sort((a, b) => a.Value.X - b.Value.X);

                for (var j = 0; j < output[i].Count; j++)
                {
                    if (tmp.Count > 0)
                    {
                        var max = tmp.Select((v) => {
                            return v.Value.X + v.Value.Line.Width;
                        }).Max();
                        if(max < output[i][j].Value.X)
                        {
                            var line = mergeLines(tmp.ToArray(), unico);
                            tmp.Clear();
                            lineTmp.Add(line);
                        }
                    }
                    tmp.Add(output[i][j]);

                }
                if(tmp.Count > 0)
                {
                    var line = mergeLines(tmp.ToArray(), unico);
                    tmp.Clear();
                    lineTmp.Add(line);
                }
                output2[i] = lineTmp.ToArray();
                lineTmp.Clear();
            }
            return output2;       

        }
        
        public static void MergeText(SuouText downText, BasicLineMerge[] layerTexts, ISpaceMaker unico)
        {
            var arr = ToSortedStringLineArrays(layerTexts, unico);
            for(var y = 0; y < arr.Length; y++)
            {
                if(arr[y].Length > 0)
                {

                    while (downText.Count < y)
                    {
                        downText.Add(SuouLine.Empty(downText.Font, downText.Color));
                    }
                    var line = downText[y];
                    var lineData = new StringLineData(line, 0);
                    for (var x = 0; x < arr[y].Length; x++)
                    {
                        PerfectMergeLine(lineData, arr[y][x], unico);
                    }
                    downText[y] = lineData.Line;

                }

            }

        }
        


        public void DestructiveMerge(SuouText CopyDownText, BasicLineMerge LayerText, EditPoint location,AltText merger)
        {
            bool b = false;

            if (LayerText.IsSimple)
            {
                var mlayer = (SimpleLineMerge)LayerText;
                for (int diff = 0; diff < 16; diff++)
                {
                    if (merger.CanSmallBangMergeText(CopyDownText, mlayer, diff))
                    {
                        b = true;
                        CopyDownText.Initialize(merger.SmallBangMergeText(CopyDownText, LayerText, diff));
                        break;
                    }

                }
                if (!b)
                {
                    merger.BigBangMergeText(CopyDownText, LayerText);
                }
            }
            else
            {
                var mlayer = (LineMerge)LayerText;
                for (int diff = 0; diff < 16; diff++)
                {
                    if (merger.CanSmallBangMergeText(CopyDownText, mlayer, diff))
                    {
                        b = true;
                        CopyDownText.Initialize(merger.SmallBangMergeText(CopyDownText, LayerText, diff));
                        break;
                    }

                }
                if (!b)
                {
                    merger.BigBangMergeText(CopyDownText, LayerText);
                }
            }
        }
        #region XML
        public static string ParserName { get { return "SpaceLineSet"; } }
        public XElement CreateSerializer()
        {
            var xe = new XElement(ParserName);
            xe.Add(new XAttribute("Type", (int)this.Type));
            if (this.Type == SpaceLineSetType.Other)
            {
                xe.Add(new XAttribute("PriorityType", (int)this.PriorityType));
                xe.Add(new XAttribute("Name", this.Name));
                xe.Add(new XAttribute("PermitDirtyLength", this.PermitDirtyLength));
                xe.Add(new XAttribute("MinCaliculate", this.MinCaliculate));

                var items = new XElement("Items");
                this.Items.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => items.Add(v));
                xe.Add(items);
            }
            return xe;
        }
        private static SpaceLineSet SubParse(XElement parser)
        {
            var priorityTyep = (SpacePriorityType)int.Parse((parser.Attribute("PriorityType").Value));
            var name = parser.Attribute("Name").Value;
            var pdl = int.Parse((parser.Attribute("PermitDirtyLength").Value));
            var mincc = int.Parse((parser.Attribute("MinCaliculate").Value));
            var items = SpaceItem.ListParse(parser.Element("Items"));

            var r = new SpaceLineSet(mincc, pdl, priorityTyep, items) { Name = name };
            return r;
        }
        public static SpaceLineSet Parse(XElement parser)
        {
            var type = (SpaceLineSetType)int.Parse((parser.Attribute("Type").Value));

            switch (type)
            {
                case SpaceLineSetType.Default: return SpaceLineSet.DefaultSet;
                case SpaceLineSetType.Uni: return SpaceLineSet.UniDefaultSet;
                case SpaceLineSetType.Uni2: return SpaceLineSet.UniDefaultSet2;
                case SpaceLineSetType.CompleteClean: return SpaceLineSet.CompleteCleanDefaultSet;
                case SpaceLineSetType.HukidasiDefault: return SpaceLineSet.HukidasiDefaultSet;
                default : return SpaceLineSet.SubParse(parser);
            }
        }
        public static XElement CreateNormalXElement(IList<SpaceLineSet> items)
        {
            var xe = new XElement("SpaceLineSetList");
            items.ForEach((v) => xe.Add(v.CreateSerializer()));
            return xe;
        }
        public static IList<SpaceLineSet> ListParse(XElement parser)
        {
            var items = parser.Elements(ParserName);
            return items.Select((v) => SpaceLineSet.Parse(v)).ToList();
        }
        


        #endregion
        #region IXMLSerialize
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            var baseItem = new SpaceLineSetXML(this);
            var ser = new XmlSerializer(typeof(SpaceLineSetXML));
            ser.Serialize(writer, baseItem);
        }
        public void ReadXml(System.Xml.XmlReader reader)
        {

            var ser = new XmlSerializer(typeof(SpaceLineSetXML));
            var item = (SpaceLineSetXML)ser.Deserialize(reader);

            base._Items = item._Items;
            base._PriorityType = item._PriorityType;
            base.MinCaliculate = item.MinCaliculate;
            base.Name = item.Name;
            base.PermitDirtyLength = item.PermitDirtyLength;
        }
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }
        #endregion
    }
    [Serializable]
    [ImmutableObject(true)]
    public class SpaceItem:IXmlSerializable,IParse
    {
        public SpaceItem()
        {
        }
        public SpaceItem(char w)
        {
            this.SuouWord = new SuouWord(w.ToString());
        }
        public SpaceItem(SpaceItem w)
        {
            this.SuouWord = w.SuouWord.Clone();
        }
        public SuouWord SuouWord { get; private set; }
        public string Word { get { return SuouWord.Word; } }


        public int Width { get { return SuouWord.Width; } }
        
        public bool IsSpace
        {
            get
            {
                return this.SuouWord.Word.All((v) => AATextLaw.IsSpace(v));
            }
        }
        public bool HankakuSpace
        {
            get
            {
                return this.Word[0] == ' ';
            }
        }
        /*
        public SpaceItem(SuouLetter wc)
        {
            this.SuouLetter = wc;
        }
        */
        public SpaceItem Clone()
        {
            return new SpaceItem(this);
        }
        public override bool Equals(object obj)
        {
            if (!(obj is SpaceItem)) return false;
            var objm = (SpaceItem)obj;
            var a = this.SuouWord == objm.SuouWord;
            return a;
        }
        public override int GetHashCode()
        {
            return this.SuouWord.GetHashCode();
        }
        public override string ToString()
        {
            return this.SuouWord.Word.ToString();
        }

        #region XML
        public XElement CreateSerializer()
        {
            var xe = new XElement("SpaceItem");
            xe.Value = this.SuouWord.Word;
            //xe.Add(this.SuouWord.CreateNormalXElement());
            return xe;
        }
        public static SpaceItem Parse(XElement parser)
        {
            var c = parser.Value[0];

            return new SpaceItem(c);
        }
        public static List<SpaceItem> ListParse(XElement parser)
        {
            var f = parser.Elements().Select((v) => SpaceItem.Parse(v)).ToList();
            return f;
        }
        #endregion

        #region IXMLSerialize
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            var p = new XElement("SpaceLineSet");
            p.Add(new XAttribute("Word", this.Word));
            
            p.WriteTo(writer);
            //writer.WriteAttributeString("Word", this.Word);
        }
        public void ReadXml(System.Xml.XmlReader reader)
        {
            
            var v = XElement.Parse(reader.ReadInnerXml());
            this.SuouWord = new SuouWord(v.Attribute("Word").Value);
        }
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }
        #endregion
    }
}
