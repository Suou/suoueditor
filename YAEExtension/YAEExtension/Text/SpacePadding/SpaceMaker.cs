﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase;
using SuouBase.Text;
using System.Diagnostics;
using BSystem;
namespace SuouBase.Text
{
    

    public class SpaceMaker : ISpaceMaker
    {
        public readonly SpaceLineSet Seting;
        protected Font Font;
        protected Color Color;

        private SpaceEditDistancePool Creater { get; set; }
        public Dictionary<int, SuouLine> SpaceCash = new Dictionary<int, SuouLine>();
        public Dictionary<int, int> CarelessCash = new Dictionary<int, int>();

        public SpaceMaker(SpaceLineSet setting, Font f, Color c)
        {
            this.MaxCaliculate = int.MaxValue;
            this.Seting = setting;
            this.Font = f;
            this.Color = c;

            this.Creater = new SpaceEditDistancePool(this.Seting);
            this.PreCreate(this.Seting.MinCaliculate);
            this.PreCreate();

        }
        public bool IsSpace(char c)
        {
            var v = SpaceChars.AASpaces.Contains(c);
            return v;
        }
        public int MaxCaliculate { get; set; }
        public int Periodicity
        {
            get { return this.Seting.MostWideChar.Width; }
        }

        public void PreCreate(int len)
        {
            this.CanCreateSpace(len);
            this.MaxCaliculate = len;
        }
        public void PreCreate()
        {
            int t=0;
            int p = 0;
            while (t < this.Periodicity)
            {
                if (this.Creater.CanCreate(p++))
                {
                    t++;
                }
                else
                {
                    t = 0;
                }
            }
            if (this.MaxCaliculate < p - 1) this.MaxCaliculate = p - 1;
        }
        #region Default
        public static SpaceMaker Default()
        {            
            return SpaceMakerDic.Create(SpaceLineSet.DefaultSet, SuouEditorConst.AAFont, Color.Black);
        }
        public static SpaceMaker UniDefault()
        {
            return SpaceMakerDic.Create(SpaceLineSet.UniDefaultSet, SuouEditorConst.AAFont, Color.Black);
        }
        public static SpaceMaker CleanDefault()
        {
            return SpaceMakerDic.Create(SpaceLineSet.CompleteCleanDefaultSet, SuouEditorConst.AAFont, Color.Black);
        }
        #endregion
        public int CanCreateSpaceNum(int n)
        {
            return n.FindIndexWidthCountUp((i) => this.CanCreateSpace(i));
        }
        public bool CanCreateSpace(int n)
        {
            if (n < 0)
            {
                return false;
            }
            else if (n > this.MaxCaliculate)
            {
                return true;
            }
            else if(SpaceCash.ContainsKey(n) || this.Creater.CanCreate(n))
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public SuouLine CreateSpace(int n)
        {
            SuouLine Result = null;
            if (n > this.MaxCaliculate)
            {
                Result = this.NaiveCashCreateSpace(n);
            }
            else if (SpaceCash.ContainsKey(n))
            {
                Result = SpaceCash[n].Clone();
            }
            else if (this.Creater.CanCreate(n))
            {
                this.SpaceCash[n] = this.Creater.Create(n, Font, Color);
                Result = SpaceCash[n].Clone();
            }
            else
            {
                throw new MyException("空白作成の不自然な失敗");
            }

            if (Result != null && Result.Width != n) { throw new MyException("空白作成の不自然な失敗"); }
            if (Result == null) { throw new MyException("空白作成の失敗"+n); }
            return Result;
        }
        private SuouLine NaiveCashCreateSpace(int n)
        {
            var len = n - (this.MaxCaliculate + 1);
            var a = len / this.Seting.MostWideChar.Width;
            var b = len % this.Seting.MostWideChar.Width;
            a += 1;
            
            var c = (this.MaxCaliculate + 1) - (this.Seting.MostWideChar.Width - b);
            var f = this.CreateSpace(c).Clone();
            var w = new SuouWord(this.Seting.MostWideChar.SuouWord.CreateWord(a));
            f += w;
            return f;

        }

        public SuouLine CreateSpaceCarelessly(int n)
        {
            if (CarelessCash.ContainsKey(n)) return this.CreateSpace(CarelessCash[n]);
            var t = IntExtension.PlusMinusCountUp().First((x) => CanCreateSpace(n + x));
            var line = this.CreateSpace(n + t);
            CarelessCash[n] = n + t;
            return this.CreateSpace(n + t);
        }
        public SuouLine CreateSpaceCarelesslyNotShort(int n)
        {
            if (CarelessCash.ContainsKey(n)) return this.CreateSpace(CarelessCash[n]);
            var t = IntExtension.PlusCountUp().First((x) => CanCreateSpace(n + x));
            var line = this.CreateSpace(n + t);
            CarelessCash[n] = n + t;
            return this.CreateSpace(n + t);
        }

    }

    
}
