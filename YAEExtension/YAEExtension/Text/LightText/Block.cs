﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using BSystem.Collections;
namespace SuouBase.Text
{
    public class RangeString
    {
        public Point Location { get; set; }
        public string Sentence { get; private set; }
    }
    public interface Range1DString
    {
        int X { get; set; }
        string Sentence { get; set; }
    }
    public interface Range1D
    {
        int X { get; set; }
        
        int LeftX { get;}
        int RightX { get;}
        int Length { get; set; }
    }
    public class RangeList{
        private List<Range> Items = new List<Range>();
        public IList<int> Overlap(Range r){
            return this.Items.WhereIndex((v)=>v.Overlap2D(r)).ToList();
        }
    }
    public struct Range:Range1D
    {
        public Point Location { get; set; }
        public int Length { get; set; }
        public int X
        {
            get { return this.Location.X; }
            set { this.Location = new Point(value, this.Location.Y); }
        }
        public int RightX{
            get{
                return this.X + this.Length;
            }
        }
        public int LeftX{
            get{
                return this.X;
            }
        }
        public int Y
        {
            get { return this.Location.Y; }
            set { this.Location = new Point(this.Location.X, value); }
        }
        public bool Overlap1D(Range1D p){
            return (p.LeftX <= this.LeftX && this.LeftX <= p.RightX) || (this.LeftX <= p.LeftX && p.LeftX <= this.RightX);
        }
        public bool Overlap2D(Range p){
            return (this.Y == p.Y && this.Overlap1D(p));
        }
    }

    public class BlockString
    {
        public Dictionary<int, List<Range1DString>> Lines { get; private set; }

        public Point Location { get; set; }
        public Range TopRange { get; set; }
        public Range BottomRange { get; set; }

        public BlockString()
        {
            this.Lines = new Dictionary<int, List<Range1DString>>();
        }
 
    }
    public class BlockStringRow
    {
        public List<BlockString> Items = new List<BlockString>();
        public IEnumerable<BlockString> TopOverlap(Range p)
        {
            return this.Items.Select((v) => v.TopRange).WhereIndex((v) => v.Overlap2D(p)).Select((v) => this.Items[v]).ToList();
        }
        public IEnumerable<BlockString> BottomOverlap(Range p)
        {
            return this.Items.Select((v) => v.BottomRange).WhereIndex((v) => v.Overlap2D(p)).Select((v) => this.Items[v]).ToList();
        }
    }
    public class BlockBlockString
    {
        public Dictionary<int, BlockStringRow> Dic = new Dictionary<int, BlockStringRow>();
        /*
        public IEnumerable<BlockString> Rindo(BlockString item)
        {
            
        }
        public IList<BlockString> Take(Range p)
        {
            List<BlockString> rlist = new List<BlockString>();
            if (this.Dic.ContainsKey(p.Y)) return rlist;

        }
        */
    }
}
