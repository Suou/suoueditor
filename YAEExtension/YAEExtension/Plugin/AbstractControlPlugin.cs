﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using SuouBase.Interface;
namespace SuouBase.Plugin
{
    public abstract class AbstractControlPlugin<BaseControl> : IComponentPlugin where BaseControl : ContainerControl
    {

        public AbstractControlPlugin()
        {
        }

        public EditorInfo DLLFunction { get; set; }
        public virtual Form Form
        {
            get
            {
                return null;
            }
        }
        public BaseControl _Ctrl { get; set; }
        public virtual ContainerControl Control
        {
            get
            {
                return this._Ctrl;
            }
        }
        public virtual SettingPage[] SettingPage
        {
            get
            {
                return null;
            }
        }
        public virtual Size CompactSize
        {
            get
            {
                return new Size(100, 100);
            }
        }
        public virtual string Name
        {
            get
            {
                return this._Ctrl.Name;
            }
        }
        public virtual bool DisposeCheck()
        {
            return true;
        }
    }
}
