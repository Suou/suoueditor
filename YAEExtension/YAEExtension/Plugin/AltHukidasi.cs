﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using SuouBase.Text;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Linq;
namespace SuouBase.Plugin
{
    [Serializable]
    public class MergeAssistCollection:IPluginBase
    {

        public ObservableCollection<HukidasiSaveData> HukidasiSaveDataList { get; set; }
        public List<SpaceLineSet> AltSettingSaveDataList { get; set; }

        //public static string SaveHFileName = SuouEditorConst.SettingDirectory + "/MergeAssistH.stn";

        public SettingPage[] SettingPage { get; set; }
        public static string HukidasiDataInitializeFileName = SuouEditorConst.GetDataFile("hukidasi.hkd");
        public static string HukidasiDataFileName = SuouEditorConst.UserDirectory +"/hukidasi.hkd";
        public MergeAssistCollection()
        {
            HukidasiSaveDataList = new ObservableCollection<HukidasiSaveData>() { };

            this.HukidasiSaveDataList.CollectionChanged += this.Check;
            var hukidasiItems = HukidasiSaveData.Load(HukidasiDataFileName) ?? HukidasiSaveData.Load(HukidasiDataInitializeFileName);
            if (hukidasiItems != null)
            {
                hukidasiItems.ForEach((v) => this.HukidasiSaveDataList.Add(v));
            }
            AltSettingSaveDataList = new List<SpaceLineSet>(){ SpaceLineSet.DefaultSet,SpaceLineSet.CompleteCleanDefaultSet,SpaceLineSet.HukidasiDefaultSet, SpaceLineSet.UniDefaultSet,SpaceLineSet.UniDefaultSet2 };
        }
        private void Check(object sender,EventArgs e)
        {
            if (HukidasiSaveDataList.Any((v) => v.MergeAlgorithm == null))
            {
                throw new MyException("");
            }
        }
        public void Save(string filenameH)
        {
            HukidasiSaveData.Save(this.HukidasiSaveDataList, filenameH);
            //BinarySerialize.SaveToBinaryFile(this, filenameH);
        }

        public static MergeAssistCollection Load(string filename)
        {
            return new MergeAssistCollection();
            /*
            var f = HukidasiSaveData.Load(filename);
            if (f == null)
            {
                return new MergeAssistCollection();
            }
            var ff = (MergeAssistCollection)f;
            return ff;
            */
        }
        public bool DisposeCheck()
        {
            this.Save(HukidasiDataFileName);
            return true;
        }
    }

    
}
