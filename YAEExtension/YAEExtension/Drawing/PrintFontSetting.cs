﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using SuouBase.Text;
using SuouTextBox2.Base;

using System.Diagnostics;

namespace SuouBase.Drawing
{
    public class PrintFontSetting
    {
        private Font _Font = SuouEditorConst.AAFont;
        public Font Font { get { return _Font; } set { this._Font = value; } }
        public event EventHandler SettingChanged;
        public int YLineSpace = SuouEditorConst.AAFontYInterval;

        private Color _TextColor = Color.Black;
        private Color _BackGround = Color.FromArgb(250,250,250);

        private Color _ViolateColor = Color.Red;
        private Color _TransparentColor = Color.LightPink;
        private Color _TagColor = Color.LightGreen;
        private Color _SpaceColor = Color.FromArgb(200, 200, 250);
        private Color _ParagraphColor = Color.Green;
        private Color _EndColor = Color.Green;

        private Color _PaddingColor = Color.FromArgb(250,250,250);
        private bool _EnableSpaceLine = true;
        public bool IsPriority { get; set; }

        public void DoDefaultBackColor()
        {
            this.BackGround = Color.FromArgb(250, 250, 250);
        }
        public void DoUnusedBackColor()
        {
            this.BackGround = Color.DarkGray;
        }
        private void InvokeSettingChanged(){
            if (this.SettingChanged != null) this.SettingChanged(this, new EventArgs());
        }
        public bool EnableSpaceLine
        {
            get { return this._EnableSpaceLine; }
            set {
                this._EnableSpaceLine = value;
                this.InvokeSettingChanged();
            }
        
        }
        public Color PaddingColor
        {
            get { return _PaddingColor; }
            set { _PaddingColor = value; }
        }
        public Color TransparentColor
        {
            get { return this._TransparentColor; }
            set { this._TransparentColor = value; }
        }
        public Color TagColor
        {
            get { return this._TagColor; }
            set { this._TagColor = value; }
        }
        public Color BackGround
        {
            get { return _BackGround; }
            set
            {
                _BackGround = value;
                this.InvokeSettingChanged();
            }
        }
        public Color TextColor
        {
            get { return _TextColor; }
            set
            {
                _TextColor = value;
                this.InvokeSettingChanged();
            }
        }
        public Color ViolateColor
        {
            get { return _ViolateColor; }
            set
            {
                _ViolateColor = value;
            }
        }
        public Color SpaceColor
        {
            get { return _SpaceColor; }
            set
            {
                _SpaceColor = value;
            }
        }
        public Color SpaceUniColor
        {
            get;
            set;
        }
        public Color ParagraphColor
        {
            get { return _ParagraphColor; }
            set
            {
                _ParagraphColor = value;
                MakeForm();
            }
        }
        public Color EndColor
        {
            get { return _EndColor; }
            set
            {
                _EndColor = value;
                MakeForm();
            }
        }
        public SuouWord ParagraphForm;
        public SuouWord EndForm;
        private bool _IsDrawSpace = true;
        public bool IsDrawSpace
        {
            get { return this._IsDrawSpace; }
            set
            {
                this._IsDrawSpace = value;
                this.InvokeSettingChanged();
            }
        }

        public PrintFontSetting()
        {
            this.SpaceUniColor = Color.Blue;
            MakeForm();
        }
        public void MakeForm()
        {
            ParagraphForm = new SuouWord(Font, ParagraphColor, "↓");
            EndForm = new SuouWord(Font, EndColor, "[EOF]");
        }

        
        
    }
}
