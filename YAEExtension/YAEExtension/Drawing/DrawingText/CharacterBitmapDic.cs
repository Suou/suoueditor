﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;

namespace SuouBase.Drawing
{
    public class CharacterBitmapDic
    {
        public Font Font;
        public Dictionary<char, Bitmap> Items = new Dictionary<char, Bitmap>();
        public CharacterBitmapDic(Font f)
        {
            this.Font = f;
        }
        public Bitmap GetBitmap(char c)
        {
            Bitmap r = null;
            var b = this.Items.TryGetValue(c, out r);
            if (!b)
            {
                var bmp = BSystem.Drawing.DrawTextBmp.CreateCharBitmap(c, Font);
                r = BSystem.Drawing.Byte.BinaryBmp.CreateBinaryBitmap(bmp);
                this.Items[c] = r;
            }
            return r;
        }
        public Bitmap[] GetBitmaps(string s)
        {
            return s.ToCharArray().Select((v) => this.GetBitmap(v)).ToArray();
        }
    }
}
