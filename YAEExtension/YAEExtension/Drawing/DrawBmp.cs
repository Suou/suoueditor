﻿
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using BSystem.Drawing;
using BSystem.Collections;
namespace SuouBase.Drawing
{

    public class BitmapExpansion
    {
        public static Bitmap CreateBitmapRapid(SuouText text, int ySpaceSize, Color BackColor)
        {
            var slist = text.ToListString();
            return DrawTextBmp.CreateBitmapRapid(slist, text.Font, ySpaceSize, text.Color, BackColor);
        }
        /// <summary>
        /// NTBコントロールからこのコントロールが重なっている部分を切り出して
        /// このコントロールの背景に設定します
        /// </summary>
        /// <param name="bmp"></param>
        /// <param name="p"></param>
        public static Bitmap BackGroundCopy(Bitmap bmp, Point p,Size size)
        {
            if (bmp == null) throw new ArgumentNullException();
            // 切り抜く大きさのBitmapを準備
            Bitmap bmp2 = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            // これを使って Graphicsを生成
            using (Graphics gc = Graphics.FromImage(bmp2))
            {
                Rectangle rcDraw = new Rectangle(p.X, p.Y, size.Width, size.Height);
                gc.DrawImage(bmp, 0, 0, rcDraw, GraphicsUnit.Pixel);
            }

            return bmp2;
        }
    }
    public static class GraphicsExtension
    {
        public static void DrawRaughOverride(this Graphics g, SuouText text, int ySpace, Point Location)
        {
            Point p = Location;
            int basex = p.X - (int)(text.Font.SizeInPoints * (96 / 72) * 0.17);
            var size = text.TextSize(ySpace);
            for (int y = 0; y < text.Lines.Count; y++)
            {
                p.X = basex;
                g.DrawString(text.Lines[y].ToString(), text.Font, Brushes.Black, p);
                p.Y += text.Font.Height + ySpace;
            }
        }
        public static void DrawText(this Graphics g, SuouText text,int ySpace,Point Location,Color BackColor)
        {
            g.Clear(BackColor);
            g.DrawRaughOverride(text, ySpace, Location);
        }
        public static void DrawText(this Graphics g, SuouText text, int ySpace, Point Location)
        {
            g.DrawRaughOverride(text, ySpace, Location);
        }
        public static void TextRenderOverride(this Graphics g, SuouText text, int ySpace, Point Location)
        {
            Point p = Location;
            var size = text.TextSize(ySpace);
            //g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;
            for (int y = 0; y < text.Lines.Count; y++)
            {
                //描画
                TextRenderer.DrawText(g, text.Lines[y].ToString(), text.Font, p, text.Color, TextFormatFlags.NoPadding | TextFormatFlags.NoPrefix);
                p.Y += text.Font.Height + ySpace;
            }
        }
        public static void TextRender(this Graphics g, SuouText text, int ySpace, Point Location, Color BackColor)
        {
            g.Clear(BackColor);
            g.TextRenderOverride(text, ySpace, Location);
        }
        public static void DrawText(this Graphics g, SuouText text, int ySpace, Size PictureSize, Size TextSize, Color BackColor, Color FontColor, Point offsetPoint)
        {
            DrawText(g, text, ySpace, PictureSize,TextSize, BackColor, FontColor, offsetPoint, false);
        }
        public static void DrawText(this Graphics g, SuouText text, int ySpace, Size PictureSize,Size TextSize,Color BackColor,Color FontColor,Point offsetPoint,bool isProportion)
        {
            Point p = offsetPoint;
            g.Clear(BackColor);
            bool b = false;
            SizeF Proportion = new SizeF(1, 1);

            /* 精度は高いが遅い
             * if (PictureSize.Width != TextSize.Width && PictureSize.Height != TextSize.Height)
            {
                var textSizeWidth = TextSize.Width == 0 ? 1f : TextSize.Width;
                var line = text.Lines.Max2((v) => v.Width);
                textSizeWidth = g.MeasureString(line.Sentence, text.Font).Width;
                b = true;
                Proportion = new SizeF(((float)PictureSize.Width) / (float)(textSizeWidth), (float)PictureSize.Height / (float)TextSize.Height);
                g.ScaleTransform(Proportion.Width, Proportion.Height, System.Drawing.Drawing2D.MatrixOrder.Append);
            }
             */
            //+32fは対症療法的
            if (PictureSize.Width != TextSize.Width && PictureSize.Height != TextSize.Height)
            {
                var textSizeWidth = TextSize.Width == 0 ? 1f : TextSize.Width;
                b = true;
                Proportion = new SizeF(((float)PictureSize.Width) / (float)(textSizeWidth + 32f), (float)PictureSize.Height / (float)TextSize.Height);
                if (isProportion)
                {
                    var m = Math.Min(Proportion.Width,Proportion.Height);
                    Proportion = new SizeF(m, m);
                }
                g.ScaleTransform(Proportion.Width, Proportion.Height, System.Drawing.Drawing2D.MatrixOrder.Append);
            }
            g.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SystemDefault;
            g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            //g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            g.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;
            StringFormat flag = new StringFormat();
            flag.HotkeyPrefix = System.Drawing.Text.HotkeyPrefix.None;
            flag.FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.FitBlackBox;
            flag.Trimming = StringTrimming.None;
            var size = text.TextSize(ySpace);


            if (b)
            {
                var propox = (float)(text.Font.SizeInPoints * (96 / 72) * 0.17);
                float basefx = p.X - propox;
                PointF startLocation = new PointF(basefx, p.Y);
                for (int y = 0; y < text.Lines.Count; y++)
                {
                    g.DrawString(text.Lines[y].ToString(), text.Font, new SolidBrush(FontColor), startLocation,flag);
                    startLocation.Y += (text.Font.Height + ySpace);
                }
            }
            else
            {
                //int baseIntx = (int)p.X;
                //p.X = baseIntx;
                for (int y = 0; y < text.Lines.Count; y++)
                {
                    TextRenderer.DrawText(g, text.Lines[y].ToString(), text.Font, p, FontColor, TextFormatFlags.NoPadding | TextFormatFlags.NoPrefix);
                    p.Y += (text.Font.Height + ySpace);
                }
            }
            
        }
        public static void DrawText(this Graphics g, SuouText text, int ySpace, Size PictureSize, Size TextSize, Color BackColor)
        {
            DrawText(g, text, ySpace, PictureSize, TextSize, BackColor, text.Color,Point.Empty);
        }
        public static void TextRender(this Graphics g, SuouText text, int ySpace, Size PictureSize, Color BackColor,TextRenderOption option)
        {
            if (option == TextRenderOption.None)
            {
                var bmp = BitmapExpansion.CreateBitmapRapid(text, ySpace, BackColor);
                var rbmp = EnlargeBmp.EnlargeBitmap(bmp, PictureSize);
                g.DrawImage(rbmp, Point.Empty);
            }
            else
            {
                var bmp = BitmapExpansion.CreateBitmapRapid(text, ySpace, BackColor);
                var textsize = text.GetSize(ySpace);
                var d1 = (float)PictureSize.Width / (float)textsize.Width;
                var d2 = (float)PictureSize.Height / (float)textsize.Height;
                var d = d1 < d2 ? d1 : d2;
                var rbmp = EnlargeBmp.EnlargeBitmap(bmp, d);
                g.Clear(BackColor);
                g.DrawImage(rbmp, Point.Empty);
            }
        }
        public static Bitmap CreateBitmap(SuouText text,PrintFontSetting pfs)
        {
            Size size = text.TextSize(pfs.YLineSpace);
            if (size.Width == 0) size.Width = 1;
            if (size.Height == 0) size.Height = 1;
            Bitmap bmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            var g = Graphics.FromImage(bmp);
            g.SuouTextRender(text, new Point(0, 0), pfs);
            g.Dispose();
            return bmp;
        }
        public static void TextRender(this Graphics g, SuouText text, Size PictureSize, TextRenderOption option, PrintFontSetting pfs)
        {
            var bmp = CreateBitmap(text, pfs);
            var textsize = text.GetSize(pfs.YLineSpace);
            if (option == TextRenderOption.None)
            {
                var rbmp = EnlargeBmp.EnlargeBitmap(bmp, PictureSize);
                g.DrawImage(rbmp, Point.Empty);
            }
            else
            {
                var d1 = (float)PictureSize.Width / (float)textsize.Width;
                var d2 = (float)PictureSize.Height / (float)textsize.Height;
                var d = d1 < d2 ? d1 : d2;
                var rbmp = EnlargeBmp.EnlargeBitmap(bmp, d);
                g.Clear(pfs.BackGround);
                g.DrawImage(rbmp, Point.Empty);
            }
        }
        private static void SuouTextRender(this Graphics g, SuouText Text, Point Location, PrintFontSetting pfs)
        {
            foreach (var line in Text)
            {
                SuouLineRender(g, line, Location, pfs);
                Location.Y += line.Height + pfs.YLineSpace;
            }
        }
        private static void SuouLineRender(this Graphics g, SuouLine line,Point Location, PrintFontSetting pfs)
        {
            
            foreach (var word in line.Words)
            {
                SuouWordRender(g, word, Location, pfs);
                Location.X += word.Width;
            }
        }
        private static void SuouWordRender(this Graphics g, SuouWord word, Point Location, PrintFontSetting pfs)
        {
            var rec = new Rectangle(Location,word.Size);
            if (word.HasTag)
            {
                g.FillRectangle(new SolidBrush(pfs.TagColor), rec);
            }
            else if (word.IsTransparent)
            {
                g.FillRectangle(new SolidBrush(pfs.TransparentColor), rec);
            }
            TextRenderer.DrawText(g, word.Word, word.Font, Location, pfs.TextColor, TextFormatFlags.NoPadding | TextFormatFlags.NoPrefix);
        }
    }

}
