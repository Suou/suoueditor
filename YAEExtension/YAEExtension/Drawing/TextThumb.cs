﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Book;
namespace SuouBase.Drawing
{
    public class TextThumbImage
    {
        public event BookPageContentChangeEventHandler PropertyChanged;
        public IBookPage Page { get; set; }
        public int YSpace { get; set; }
        public Padding Padding { get;set;}
        public ThumbnailMode _Mode = ThumbnailMode.Raw;
        public ThumbnailMode Mode
        {
            get
            {
                if (this.IsSmallerRawView && this.TextSize.Width < 250 && this.TextSize.Height < 100) return ThumbnailMode.Raw;
                return _Mode;
            }
            set
            {
                _Mode = value;
            }

        }
        public bool IsSmallerRawView { get; set; }
        public bool IsLoose { get; set; }
        public Size MinimumSize = new Size(16, 16);
        private float _Percent;
        private Size _PictureSize;
        private Size? _PercentPictureSize;
        public float Percent
        {
            get { return _Percent; }
            set
            {
                this._PercentPictureSize = null;
                _Percent = value;
                this.Mode = ThumbnailMode.Percent;
            }
        }
        private Size TextSizeWidthPadding
        {
            get
            {
                return new Size(this.Padding.Left + this.TextSize.Width + this.Padding.Right, this.Padding.Top + this.TextSize.Height + this.Padding.Bottom);
            }
        }
        public Size PercentPictureSize
        {
            get
            {
                if (!this._PercentPictureSize.HasValue)
                this._PercentPictureSize = this.GetPercentSize(this.TextSize, this.Percent);
                return this._PercentPictureSize.Value;
            }
        }
        public Size PictureSize
        {
            get
            {
                Size r = Size.Empty;
                switch (Mode)
                {
                    case ThumbnailMode.Raw: r = this.TextSizeWidthPadding; break;
                    case ThumbnailMode.Size: r = this._PictureSize; break;
                    case ThumbnailMode.Percent: r = this.PercentPictureSize; break;
                }
                if (r.Width < this.MinimumSize.Width) r.Width = this.MinimumSize.Width;
                if (r.Height < this.MinimumSize.Height) r.Height = this.MinimumSize.Height;
                
                return r;
            }
            set
            {
                this._PictureSize = value;
                this.Mode = ThumbnailMode.Size;
            }
        }
        public Rectangle PictureDisplayRectangle
        {
            get { return new Rectangle(Point.Empty, this.PictureSize); }
        }
        private Size? _TextSize = null;
        public Size TextSize
        {
            get
            {
                if (_TextSize == null)
                {
                    if (this.IsLoose)
                    {
                        var v = this.Page.GetLooseSize(this.YSpace);
                        if (v.Width <= 0) v.Width = 1;
                        if (v.Height <= 0) v.Height = 1;
                        return v;
                    }
                    else
                    {
                        var v = this.Page.SText.GetDrawStringSize(this.YSpace);
                        if (v.Width <= 0) v.Width = 1;
                        if (v.Height <= 0) v.Height = 1;
                        this._TextSize = v;
                    }
                }
                return _TextSize.Value;
            }
        }
        private Color _ControlBackColor;
        public Color ControlBackColor
        {
            get
            {
                return _ControlBackColor;
            }
            set
            {
                _ControlBackColor = value;
                //if (PropertyChanged != null) PropertyChanged(this, new EventArgs());
            }
        }
        public bool ProhibitCategory { get; set; }
        public string CategoryName
        {
            get
            {
                switch (this.PureType)
                {
                    case TextPictureType.Picture:
                        return null;
                    case TextPictureType.Category:
                        return this.Page.Name;
                    case TextPictureType.PictureCategory:
                        return this.Page.SText[0].Sentence;
                }
                return null;
            }
        }
        public TextPictureType Type
        {
            get
            {
                if (this.ProhibitCategory) return TextPictureType.Picture;
                return this.PureType;
            }
        }
        public TextPictureType PureType
        {
            get
            {
                if (this.Page is SectionScene)
                {
                    if (this.Page.Name.Length >= 1) return TextPictureType.Category;
                }
                else if (this.Page is ObsoleteChapterBookPage)
                {

                }
                else
                {
                    var p = (AstPage)this.Page;
                    if (p.TextRowCount == 1) return TextPictureType.PictureCategory;
                }

                {
                    return TextPictureType.Picture;
                }
            }
        }


        public bool IsCategory
        {
            get { return this.Type == TextPictureType.Category || this.Type == TextPictureType.PictureCategory; }
        }
        private void PageText_Changed(object sender, BookPageContentChangeEventArgs e)
        {
            this._TextSize = null;
            this._PercentPictureSize = null;
            if (PropertyChanged != null) PropertyChanged(this, e);
        }

        public TextThumbImage(IBookPage page, int YSpace)
        {
            this.Page = page;
            this.YSpace = YSpace;
            this.ControlBackColor = Color.White;
            this.Page.EditStateChanged += this.PageText_Changed;
        }
        public static TextThumbImage[] CreateTextBitmaps(IBook ef, int yinterval)
        {
            List<TextThumbImage> sparray = new List<TextThumbImage>();
            foreach (IBookPage ap in ef)
            {

                var stt = new TextThumbImage(ap, yinterval);
                sparray.Add(stt);

            }
            return sparray.ToArray();
        }
        private Size GetPercentSize(Size f, float per)
        {
            return new SizeF(f.Width * per, f.Height * per).ToSize();
        }
        public IBookPage Decode()
        {
            var v = this.Page.IBookPageCopy();
            SuouText p = v.SText;
            p.Decode();
            //v.SText = p;
            return v;
        }
        public void DrawText(Graphics g, Color textColor, Color backGround)
        {
            var picsize = this.PictureSize;
            if (this.Mode == ThumbnailMode.Raw) picsize = this.TextSize;

            g.DrawText(this.DecodeText(), this.YSpace, picsize, this.TextSize, backGround,textColor, new Point(this.Padding.Left,this.Padding.Top));
        }
        public Bitmap CreateImage()
        {
            var bmp = new Bitmap(this.PictureSize.Width, this.PictureSize.Height);
            var g = Graphics.FromImage(bmp);
            this.DrawText(g, Color.Black, Color.White);
            g.Dispose();
            return bmp;
        }
        public Bitmap CreatePlainImage()
        {
            var bmp = new Bitmap(this.PictureSize.Width, this.PictureSize.Height);
            var g = Graphics.FromImage(bmp);
            g.DrawText(this.Page.SText, 2, Point.Empty);
            g.Dispose();
            return bmp;
        }

        public Bitmap CreateBitmap16()
        {
            var bmp = new Bitmap(this.PictureSize.Width, this.PictureSize.Height,System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            var g = Graphics.FromImage(bmp);
            this.DrawText(g, Color.Black, Color.White);
            g.Dispose();
            return bmp;
        }
        public Bitmap CreateBitmap2()
        {
            return BSystem.Drawing.Byte.TurboFilter16.ToBitmap2(this.CreateBitmap16());
        }

        public SuouText DecodeText()
        {
            var v = this.Page.SText;
            v.Decode();
            return v;
        }
        
    }
    public enum ThumbnailMode
    {
        Size, Percent, Raw,WidthSizeHeightPer
    }
    public enum TextPictureType
    {
        Picture,Category,PictureCategory
    }
}
