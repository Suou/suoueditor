﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace SuouBase.Api
{
    public static class WINAPI
    {
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, int msg, int wParam, int lParam);

        public const int WM_SETREDRAW = 0x000B;
        public const int Win32False = 0;
        public const int Win32True = 1;

        public static void StopControlDraw(IntPtr handle){
            SendMessage(handle, WM_SETREDRAW, Win32False, 0);
        }
        public static void StartControlDraw(IntPtr handle)
        {
            SendMessage(handle, WM_SETREDRAW, Win32True, 0);
        }

        [DllImport("user32.dll")]
        public static extern UInt32 GetWindowLong(IntPtr hWnd, Int32 nIndex);

        [DllImport("user32.dll")]
        public static extern UInt32 SetWindowLong(IntPtr hWnd, Int32 nIndex, UInt32 dwNewLong);
    }
}
