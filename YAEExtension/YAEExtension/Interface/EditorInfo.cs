﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Files;
using SuouBase.Plugin;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using SuouBase.Book;
namespace SuouBase.Interface
{
    /// <summary>
    /// プラグインに渡すクラスです
    /// </summary>
    public class EditorInfo
    {
        private const string SettingSupervisorFileName = "Asetting.stn";
        public EditorInfo(SuouEditorDataCollection filedata, ISupervisorForm MainForm)
        {
            this.Info = new SmallInfo();
            this.FileData = filedata;
            this.MasterForm = MainForm;
            _PluginList = new List<IComponentPluginMetaData>() { };
            this.Common = new SmallPluginDelegate();
            this.Setting = BSystem.XML.NonBinarySerializer.Load<BaseSetting>(SuouEditorConst.GetSettingFile(SettingSupervisorFileName)) ?? new BaseSetting();
            this.Info.EditFunc = this.EditFunc;
            
        }
        public event EventHandler StartSettingEnded;
        public void FinishStartSetting()
        {
            if (this.StartSettingEnded != null) this.StartSettingEnded(this, new EventArgs());
        }

        #region Property
        /// <summary>
        /// 蘇芳エディタの基本情報を収納したクラス
        /// </summary>
        public SuouEditorDataCollection FileData { get; private set; }

        public ISupervisorForm MasterForm { get; set; }
        public IList<IComponentPluginMetaData> PluginList
        {
            get { return this._PluginList; }
        }
        private List<IComponentPluginMetaData> _PluginList = new List<IComponentPluginMetaData>();

        public EditorDisplay DisplayType { get; set; }
        public SmallInfo Info { get; private set; }
        public IBook SelectedStoryFile
        {
            get
            {
                if (this.FileData.SelectData == null) return null;
                return this.FileData.SelectData.StoryFile;
            }
        }
        public MergeAssistCollection AltHukidasiPlugin = MergeAssistCollection.Load(MergeAssistCollection.HukidasiDataFileName);

        public Func<IHukidasiSelecter> HukidasiSelecterForm { get; set; }
        public BaseSetting Setting { get; set; }
        public SmallPluginDelegate Common;
        private IFrameworkPlugin _Framework = null;
        public IFrameworkPlugin Framework
        {
            get { return _Framework; }
            set
            {
                _Framework = value;
                if (value != null) MasterForm.SetFramework(Framework);
            }
        }
        public bool LoadPlugin(Func<IFrameworkPluginMetaData, EditorInfo, IFrameworkPlugin> loaderFramework
            , Func<EditorInfo, BaseSetting, IList<IComponentPluginMetaData>> loaderPlugins)
        {
            if (this.Setting == null)
            {
                this.Setting = new BaseSetting();
                return false;
            }
            else
            {
                this.Framework = loaderFramework(this.Setting.FrameSetting, this);
                if (this.Framework == null) return false;
                var items = loaderPlugins(this, this.Setting);
                items.ForEach((v) => this.AddPluginList(v));
                
                return true;
            }
        }
        public void LoadFileData()
        {
            foreach (string s in this.Setting.FileTabList)
            {
                this.FileData.AddFilePath(s);
            }
        }
        
        public void SaveSetting()
        {
            this.Setting.FileTabList = this.FileData.Select((v) => v.StoryFile.FullFilePath).ToList();
            this.Setting.PluginSetting = this.PluginList.ToList();
            BSystem.XML.NonBinarySerializer.Save(SuouEditorConst.GetSettingFile(SettingSupervisorFileName), this.Setting);
        }
        #endregion
        #region Plugin
        public IBookPage SelectedPage
        {
            get
            {
                if (this.TextBoxPlugin != null)
                {
                    return this.TextBoxPlugin.Page;
                }
                return null;
            }
        }
        
        private ITextEditClass _TextBoxPlugin;
        /// <summary>
        /// 蘇芳エディタの指定しているAAファイルのページを編集・描画するプラグイン
        /// </summary>
        /// 
        public ITextEditClass TextBoxPlugin
        {
            get { return _TextBoxPlugin; }
            set
            {
                _TextBoxPlugin = value;
            }
        }

       // private IStoryPageListClass _oStoryList;

        /// <summary>
        /// 蘇芳エディタの指定しているAAファイルのページを表示するプラグイン
        /// </summary>
        public IStoryPageListClass StoryPageViewPlugin
        {
            get
            {
                var p = this.PluginList.Where((v) => v.Plugin is IStoryPageListClass).FirstOrDefault((v)=>v.Position != -1);
                return p == null ? null : (IStoryPageListClass)p.Plugin;
            }
            /*
            set
            {
                _oStoryList = value;
                //TabItemReDraw();
            }
            */
        }
        /// <summary>
        /// AAファイルの閲覧・ページコピーを目的としたプラグイン
        /// </summary>
        public ITemplatePageListClass TemplatePageViewPlugin { get; set; }
        private ILocalTemplateClass _LocalTemplatePlugin;
        public ILocalTemplateClass LocalTemplatePlugin
        {
            get { return _LocalTemplatePlugin; }
            set
            {
                _LocalTemplatePlugin = value;
                //TabItemReDraw();
            }
        }
        private IPreviewClass _PreviewPlugin;
        public IPreviewClass PreviewPlugin
        {
            get { return _PreviewPlugin; }
            set
            {
                _PreviewPlugin = value;
                //TabItemReDraw();
            }
        }

        //public List<SuouBase.Controls.DockingManagerPanel> DockingPanels = new List<Controls.DockingManagerPanel>();
        private IGlobalTemplateClass _oGlobalTemplate = null;
        /// <summary>
        /// 膨大なAAファイルが収納されたフォルダを閲覧、管理、操作するプラグイン
        /// </summary>
        public IGlobalTemplateClass GlobalTemplatePlugin
        {
            get { return _oGlobalTemplate; }
            set
            {
                _oGlobalTemplate = value;
            }
        }
        private IRegisterTemplateClass _oRegisterTemplate = null;
        public IRegisterTemplateClass RegisterTemplatePlugin
        {
            get { return _oRegisterTemplate; }
            set
            {
                _oRegisterTemplate = value;
            }
        }
        #endregion
        #region Method

        public void ReplaceEditor(IComponentPluginMetaData after)
        {
            var book = this.StoryPageViewPlugin.Item;
            var before = this.GetPluginMetaData(this.StoryPageViewPlugin);
            if (before == after) return;
            this.StoryPageViewPlugin.Item = null;
            this.Framework.Replace(before, after);
            this.StoryPageViewPlugin.Item = book;

            //this.StoryPageViewPlugin = (IStoryPageListClass)after.Plugin;
        }

        public void Remove(IComponentPluginMetaData pd)
        {
            pd.Plugin.DisposeCheck();
            this.PluginList.Remove(pd);
            this.Framework.Remove(pd);
        }
        public void AddPluginList(IComponentPluginMetaData item){
            this._PluginList.Add(item);
            this.Framework.SetPlugin(item);
        }

        public IComponentPluginMetaData GetPluginMetaData(IComponentPlugin cp)
        {
            return this.PluginList.FirstOrDefault((v) => v.Plugin == cp);
        }
        
        public bool MasterFormContains(ContainerControl c)
        {
            return c.ParentForm == this.MasterForm.Form;
        }
        public bool DisposeCheck()
        {
            //this.Setting.PluginSetting = new List<IComponentPluginMetaData>();
            //this.PluginList.ForEach((v) => this.Setting.PluginSetting.Add(v));
            //this.Setting.PluginSetting = this.PluginList;
            
            this.Setting.MainFormShortCutKeyDic = SuouBase.Controls.ShortCutKeyDictionary.Create(this.MasterForm.Form.MainMenuStrip);
            this.Setting.IsNormalEnd = true;
            this.SaveSetting();

            bool b = false;
            b = Framework.DisposeCheck();
            if (!b)
            {
                return false;
            }
            var b1 = this.PluginList.Any((v) => !v.Plugin.DisposeCheck());
            if (b1) return false;
            /*
            foreach (IComponentPluginMetaData pb in PluginList)
            {
                b = pb.Plugin.DisposeCheck();
                if (!b)
                {
                    return false;
                    
                }
            }
            */
            b = AltHukidasiPlugin.DisposeCheck();
            if (!b)
            {
                return false;
            }
            return true;
        }
        #endregion
        private void EditFunc(IBookPage page)
        {
            if (this.TextBoxPlugin.Page != null)
            {
                this.Info.EditBack(this.TextBoxPlugin.Page);
                this.TextBoxPlugin.Page = null;
            }
            var copy = page.IBookPageCopy();
            this.TextBoxPlugin.Page = copy;
        }
    }

    public enum EditorDisplay
    {
        TextEdit,PageViewer
    }
    /// <summary>
    /// プラグイン操作用クラス
    /// </summary>
    public class SmallPluginDelegate
    {
        //public IFrameworkPlugin MainCtrl;

        public Action<SuouText> Montage { get; set; }
        public Func<SuouText,SuouText> ShowMontageDialog { get; set; }

        public SmallPluginDelegate()
        {
            //this.RegisterPlugin = func;
        }
    }
    public class SmallInfo
    {
        public SmallInfo()
        {
            this.IBookDictionary = new BookDictionary();
            ofd = GeneralMethod.OpenDialogBox();
        }
        public OpenFileDialog ofd { get; set; }
        public IBook OpenBookShowDialog()
        {
            var rst = ofd.ShowDialog();
            if (rst == DialogResult.OK)
            {
                return IBookHandler.Load(ofd.FileName);
            }
            else
            {
                return null;
            }
        }
        public bool IsClipPage
        {
            get
            {
                IDataObject dataObject = Clipboard.GetDataObject();
                if (dataObject != null && dataObject.GetDataPresent(typeof(List<IBookPage>)))
                {
                    return true;
                }
                else if (dataObject.GetDataPresent(DataFormats.Text))
                {
                    return true;
                }
                return false;

            }
        }
        public event Action<object, IBookPage> PageCopied;
        public void InvokePageCopied(IBookPage page)
        {
            if (this.PageCopied != null)
            {
                this.PageCopied(this, page);
            }
        }
        public IBookPage DecodeCopy(IBookPage page)
        {
            if (page is AstPage)
            {
                var pg = page as AstPage;
                return pg.DecodeCopy();
            }
            else
            {
                return page.IBookPageCopy();
            }
        }
        public void Copy(IEnumerable<IBookPage> pages,EncodeType enctype)
        {
            DataObject data = new DataObject();
            var npages = pages.Select((v) => DecodeCopy(v).ToSectionPage2(null)).ToList();
            var text = pages.Select((v) => WebExtension.HTTPEncode.DecodeToSjis(v.Text).GetEncodeText(enctype)).JoinLineString();

            data.SetData(npages);
            data.SetData(text);
            
            Clipboard.SetDataObject(data, true);
           

            foreach (var page in pages)
            {
                this.InvokePageCopied(page);
            }
        }
        public void Copy(IBookPage page, EncodeType type)
        {
            this.Copy(new List<SectionPage>() { page.ToSectionPage2(null) }, type);
        }
        public void Copy(string text, EncodeType type)
        {
            var page = new AstPage("", text);
            this.Copy(new List<SectionPage>() { page.ToSectionPage2(null) }, type);
        }
        public IEnumerable<IBookPage> GetCopy()
        {
            
            IDataObject dataObject = Clipboard.GetDataObject();
            
            if (dataObject != null && dataObject.GetDataPresent(typeof(List<SectionPage>)))
            {
                List<SectionPage> person = (List<SectionPage>)dataObject.GetData(typeof(List<SectionPage>));
                if (person != null)
                {
                    foreach (var v in person)
                    {
                        var page = v.IBookPageCopy();
                        yield return page;
                    }
                }
                else
                {
                    throw new Exception();
                    /*
                    var text = (string)dataObject.GetData(DataFormats.Text);
                    var p = new AstPage("", text);
                    yield return p;
                    */
                }
                
            }
            else if (dataObject.GetDataPresent(DataFormats.Text))
            {                
                var text = (string)dataObject.GetData(DataFormats.Text);
                var p = new AstPage("",text);
                yield return p;
            }
            
        }
        public Action<IBookPage> EditFunc { get; set; }
        private Action<IBookPage,IBookPage> EditCallBack { get; set; }
        private IBookPage EditingPage { get; set; }
        public void EditBack(IBookPage page)
        {
            if (this.EditCallBack != null && this.EditingPage != null && page != null)
            {
                this.EditCallBack(this.EditingPage,page);
                this.EditingPage = null;
            }
        }
        public void Edit(IBookPage page,Action<IBookPage,IBookPage> callback)
        {
            if (this.EditFunc != null && page != null)
            {
                this.EditFunc(page);
                this.EditCallBack = callback;
                this.EditingPage = page;
            }
        }
        public BookDictionary IBookDictionary { get; private set; }

    }
}
