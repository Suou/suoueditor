﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using SuouBase;
using SuouBase.Interface;
using Microsoft.VisualBasic;
using SuouBase.Text;
using SuouBase.Book;

namespace SuouBase.Interface
{
    public class AbstractSimplePlugin : IComponentPlugin
    {
        public Form Form { set; get; }
        public ContainerControl Control { get; set; }
        
        public Size CompactSize
        {
            get
            {
                return this.Control.Size;
            }
        }
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }

        public bool DisposeCheck()
        {
            return true;
        }
        public SettingPage[] SettingPage { get; set; }
    }
}
