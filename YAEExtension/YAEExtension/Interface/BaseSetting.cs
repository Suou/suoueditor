﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using SuouBase.Controls;
namespace SuouBase.Interface
{
    [Serializable]
    public class BaseSetting
    {
        private bool _EnableBackup = true;
        public bool EnableBackup
        {
            get { return this._EnableBackup; }
            set { this._EnableBackup = value; }
        }
        private double _BackUpInterval;
        public double BackUpInterval
        {
            get
            {
                return this._BackUpInterval;
            }
            set { this._BackUpInterval = value; }
        }
        /*
        private string _StoryFileFolder = System.IO.Directory.GetCurrentDirectory() + "/" + "user";
        public string StoryFileFolder
        {
            get { return this._StoryFileFolder; }
            set { this._StoryFileFolder = value; }
        }
        */

        public ShortCutKeyDictionary MainFormShortCutKeyDic { get; set; }

        private List<string> _FileTabList = new List<string>();
        public List<string> FileTabList
        {
            get 
            {
                return this._FileTabList; 
            }
            set {
                this._FileTabList = value; 
            }
        }

        private RecentFileList _RecentUsingFileList = new RecentFileList();
        public RecentFileList RecentUsingFileList
        {
            get {
                if (this._RecentUsingFileList == null) this._RecentUsingFileList = new RecentFileList();
                return _RecentUsingFileList; 
            }
            set { this._RecentUsingFileList = value; }
        }

        private List<IComponentPluginMetaData> _PluginSetting = new List<IComponentPluginMetaData>();
        public List<IComponentPluginMetaData> PluginSetting
        {
            get { return this._PluginSetting; }
            set { this._PluginSetting = value; }
        }
        public IFrameworkPluginMetaData FrameSetting { get; set; }

        private bool _IsNormalEnd = true;
        public bool IsNormalEnd
        {
            get { return this._IsNormalEnd; }
            set { this._IsNormalEnd = value; }
        }

        public BaseSetting()
        {
        }
    }

    [Serializable]
    public class RecentFileList : List<string>
    {
        private int _MaxCount = 8;
        public int MaxCount
        {
            get { return this._MaxCount; }
            set { this._MaxCount = value; }
        }
        public void LimitAdd(string filepath)
        {
            var v = new FileInfo(filepath);
            if (v.Exists)
            {
                this.Remove(v.FullName);
                this.Insert(0, v.FullName);
            }
            if (this.Count > this.MaxCount)
            {
                this.RemoveAt(this.Count - 1);
            }
        }
        public void LimitAdd(SuouBase.Book.IBook ib)
        {
            if (ib.FullFilePath == null) return;
            this.Add(ib.FullFilePath);
        }
    }
}
