﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Controls;
using System.Xml.Linq;
using System.Xml;
namespace SuouBase.Interface
{
    public interface IParse
    {
        XElement CreateSerializer();
    }
    public static class IParseExtension
    {
        public static void SaveXML(this IParse item,string path)
        {
            var xml = item.CreateSerializer();
            //var dec = new XDeclaration("1.0", "UTF-8", "yes");
            //var xmlDoc = new XDocument(xml);
            
            //xmlDoc.Declaration = dec;
            //xmlDoc.Add(xml);
            
            string[] separator = { "\r\n" };
            using (var xtw = new XmlTextWriter(path, Encoding.UTF8))
            {
                xtw.Formatting = Formatting.Indented;
                xml.Save(xtw);
                //xmlDoc.Save(xtw);
            }
        }
    }

    public interface IListPanelItem
    {
        Control Control { get; }
        IListPanelItem Empty();


        event EventHandler ItemDeleting;
    }
    public interface IJoinTop
    {
        bool JoinTop { get;}
    }
    public interface ITabPlugin
    {
        string Name { get; }
        void TabClick();
    }
    /*
    public interface IDropDownItem
    {
        ContextMenuStrip[] ContextItems { get; }
    }
    */

    [Serializable]
    public abstract class BaseSaveData
    {
        public void Save(string filename)
        {
            BSystem.XML.BinarySerialize.SaveToBinaryFile(this, filename);
        }
        public static object Load(string filename)
        {
            return BSystem.XML.BinarySerialize.LoadFromBinaryFile<BaseSaveData>(filename);
        }
    }
}
