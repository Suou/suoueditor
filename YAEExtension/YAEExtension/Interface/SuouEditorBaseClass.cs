﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using SuouBase;
using SuouBase.Interface;
using Microsoft.VisualBasic;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouBase.Interface
{

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
    public class IComponentPluginAttribute : Attribute
    {
        public string Name { get; private set; }
        public PluginType Type { get; private set; }
        public string Description { get; private set; }
        public Bitmap Image { get; private set; }

        public IComponentPluginAttribute(string name,PluginType type)
        {
            this.Name = name;
            this.Type = type;
        }
    }
    public class CompleteSet
    {
        public IFrameworkPluginMetaData Frame;
        public List<IComponentPluginMetaData> PluginSet;

        public CompleteSet()
        {
            PluginSet = new List<IComponentPluginMetaData>();
        }

    }

    public enum PluginType
    {
        None,Control,Form,Framework
    }

    public interface IPluginBase
    {
        bool DisposeCheck();
        SettingPage[] SettingPage { get; }
    }

    public interface SettingPage
    {
        Control Ctrl { get; }
        void OK();
        void Cancel();
        string PageName { get; }
    }

    public interface IComponentPlugin :IPluginBase
    {
        Form Form { get; }
        ContainerControl Control { get; }
        Size CompactSize { get; }
        string Name { get; }
    }
    public interface NotifyControl
    {
        event EventHandler FocusNotifiedEventHandler;
        void FocusNotify();
    }
    public interface IStoryViewer
    {
        event Action<object, SectionScene> ItemLeftClicked;
        SmallInfo Info { get; set; }
    }

    public interface IStoryPageListClass:IComponentPlugin
    {
        IBook Item { get; set;}
        void Paste(IBookPage ap);
        void FocusPage(int index);
        void Call();
        ToolStripItemCollection ToolStripItems { get; }
        void CheckToolStripItems();
        event BookPageContentChangeEventHandler ViewerPageSelected;
    }
    
    public interface IChapterEditorPlugin : IStoryPageListClass
    {
    }
    
    public abstract class StoryPageListClass: IStoryPageListClass
    {
        public abstract IBook Item { get; set; }
        private IStoryViewer _Viewer = null;
        public IStoryViewer Viewer
        {
            get { return this._Viewer; }
            set
            {
                this._Viewer = value;
                this.Viewer.ItemLeftClicked += this.Viewer_PageLeftClicked;

            }
        }

        public EditorInfo Info { get; set; }
        public StoryPageListClass()
        {

        }
        public StoryPageListClass(EditorInfo info)
        {
            this.Info = info;
            info.FileData.TabChanged += this.SuperVisor_PageChanged;
            
        }

        public abstract Form Form { get; }
        public abstract ContainerControl Control { get; }
        public virtual Size CompactSize
        {
            get { return new Size(250, 250); }
        }
        public abstract string Name { get; }
        public abstract void Paste(IBookPage ap);
        public abstract void FocusPage(int index);
        public abstract void Call();

        public abstract ToolStripItemCollection ToolStripItems { get; set; }
        public abstract void CheckToolStripItems();
        public event BookPageContentChangeEventHandler ViewerPageSelected;


        public abstract bool DisposeCheck();
        public abstract SettingPage[] SettingPage { get; }

        private void SuperVisor_PageChanged(object sender, SuouEditorDataClass data)
        {
            if (this.Control.Parent == null) return;
            if (data == null)
            {
                this.Item = null;
            }
            else
            {
                this.Item = data.StoryFile;
            }
        }
        private void Viewer_PageLeftClicked(object sender, SectionScene page)
        {
            this.SelectPage(page);
        }
        protected void SelectPage(SectionScene page)
        {
            if (this.Info != null && this.Info.TextBoxPlugin != null)
            {
                this.Info.TextBoxPlugin.Page = page;
            }
        }
    }

    public interface ITemplatePageListClass:IComponentPlugin
    {
        IBook Item { get; set; }
        IBookPage SelectPage(Form form, string text);
    }
    public interface IAASelectClass : IComponentPlugin
    {
        void NotifyDropPageItem(IBookPage page);
    }

    public interface ITextEditClass : IComponentPlugin
    {
        IBookPage Page { get; set; }
        string Text { get; set; }
        string LayerText { get; set; }
        string CopyText { get; }
        string LayerCopyText { get; }
        void InsertText(string text);
        void InsertLayerText(string text);
        void AddText(string text);
        void AddLayerText(string text);
        //void MakeNameDictionary(Dictionary<string, List<ToolStripItem>> dic);
        //ToolStripItemCollection ToolStripItems { get; }
        event EventHandler InnerTextChanged;
        event EventHandler PageChanged;
        Color BackColor { get; set; }
    }

    public interface IHukidasiClass : IComponentPlugin
    {
        event HukidasiEventHandler HukidasiSelected;
    }
    public interface IHukidasiSelecter
    {
        void Dispose();
        DialogResult ShowDialog();
        HukidasiSaveData SelectedSaveData { get;}
    }
    public interface IAAViewerClass : IComponentPlugin
    {
        IBook Item { get; }
        event BookPageContentChangeEventHandler ItemSelected;
    }
    public interface IGlobalTemplateClass:IComponentPlugin
    {

        FolderDataCollection Item { get; set; }
        //event TreeViewEventHandler NodeSelected;
        //event TreeViewEventHandler NodeViewed;
    }
    public interface IRegisterTemplateClass : IComponentPlugin
    {

        FolderDataCollection Item { get; set; }
        void AddNode(TreeNode tn);
    }
    public interface IPreviewClass : IComponentPlugin
    {
        void SetItem(IBook ibook);
        void SetPageItem(IBookPage page);
        
    }
    public interface ILocalTemplateClass : IComponentPlugin
    {
        //event TreeViewEventHandler NodeSelected;
        //TemplateDataCollection Item { set; }
        void OpenReadViewerFile(string storyfilename);
        void AddNode(TreeNode tn);
    }

    public interface AAViewClass
    {
        Form Form { get; }
        void View(AstPage ap);
    }

    public interface ISupervisorData
    {
        event EditingFileEventHandler TabSelectedIndexChanged;
    }

    public interface ISupervisorForm
    {
        SettingPage[] SettingPage { get; }
        Form Form { get; }
        void SetFramework(IFrameworkPlugin mw);
        void NotifyPageSize(string text);
        void NotifyCaretInformation(string text);
        void NotifyCaretCharInformation(string text);
        
        void ViewPageSize(IBookPage page);
    }
    public interface INotifyStripForm
    {
        void NotifyMessage(string text);
    }
    public enum FormOrControl
    {
        Form,Control
    }

    [Serializable]
    public class IFrameworkPluginMetaData : PluginMetaData
    {
        [NonSerialized]
        [System.Xml.Serialization.XmlIgnore]
        public IFrameworkPlugin Plugin = null;
        public int Position { get; set; }

        public IFrameworkPluginMetaData()
        {
        }

        public IFrameworkPluginMetaData(string dLLName, string nameSpace, string className, string pluginName)
        {
            DLLName = dLLName;
            NameSpace = nameSpace;
            ClassName = className;
            PluginName = pluginName;
            Tag = null;
            this.Position = 0;
        }
        public IFrameworkPluginMetaData(PluginMetaData cps, int position, object tag)
        {
            DLLName = cps.DLLName;
            NameSpace = cps.NameSpace;
            ClassName = cps.ClassName;
            PluginName = cps.PluginName;
            Tag = tag;
            Position = position;
        }
    }
    [Serializable]
    public class IComponentPluginMetaData : PluginMetaData
    {
        [NonSerialized]
        [System.Xml.Serialization.XmlIgnore]
        public IComponentPlugin Plugin = null;

        public int Position { get; set; }

        public IComponentPluginMetaData()
        {
        }

        public IComponentPluginMetaData(string dLLName, string nameSpace, string className, string pluginName)
        {
            DLLName = dLLName;
            NameSpace = nameSpace;
            ClassName = className;
            PluginName = pluginName;
            Tag = null;
            this.Position = 0;
        }
        public IComponentPluginMetaData(PluginMetaData cps, int position, object tag)
        {
            DLLName = cps.DLLName;
            NameSpace = cps.NameSpace;
            ClassName = cps.ClassName;
            PluginName = cps.PluginName;
            Tag = tag;
            Position = position;
        }
    }

    [Serializable()]
    public class PluginMetaData
    {


        public string DLLName { get; set; }
        public string NameSpace { get; set; }
        public string ClassName { get; set; }
        public string PluginName { get; set; }
        public object Tag { get; set; }

        public PluginMetaData()
        {
        }


        public PluginMetaData(string dLLName, string nameSpace, string className, string pluginName)
        {
            DLLName = dLLName;
            NameSpace = nameSpace;
            ClassName = className;
            PluginName = pluginName;
            Tag = null;
        }

        public bool Check()
        {
            string[] s = {DLLName,NameSpace,ClassName,PluginName};
            foreach (string ss in s)
            {
                if (ss == null) return false;
            }
            return true;
        }
    }

}
