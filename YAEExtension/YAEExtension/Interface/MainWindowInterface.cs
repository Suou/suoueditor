﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Interface
{
    /*
    public interface IDockingManagerPlugin
    {
        SuouBase.Controls.DockingManagerPanel Panel { get; set; }
    }
    */
    public interface DoubleDockingPanelPlugin
    {
        ContainerControl SubControl { get; }
    }
    public interface IFrameworkPlugin : IPluginBase
    {
        ContainerControl Ctrl { get; }
        void SetPlugin(IComponentPluginMetaData cpmd);

        void Replace(IComponentPluginMetaData before, IComponentPluginMetaData after);
        void Remove(IComponentPluginMetaData cpmd);
        bool CanSetPlugin(IComponentPlugin pb, int nth);
        //bool InitializePluginList();
        //List<IComponentPluginMetaData> PluginDic { get; }
        bool Focus(IComponentPlugin pb);
        void NotifyFinishAddPluginList();
        void Clear();
    }
    [Serializable]
    public class SuouFrameTagData
    {
        public bool IsOmit { get; set; }

    }
}
