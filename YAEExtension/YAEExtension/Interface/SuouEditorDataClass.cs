﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using SuouBase;
using SuouBase.Text;
using Microsoft.VisualBasic;
using System.Xml.Serialization;
using SuouBase.Book;
using BSystem.Collections;
namespace SuouBase.Interface
{
    public class Menubar
    {
        public MenuStrip MenuStrip { get; set; }
        public ToolStripMenuItem FileToolStrip { get; set; }
        public ToolStripMenuItem EditToolStrip { get; set; }
        public ToolStripMenuItem PageToolStrip { get; set; }
        public ToolStripMenuItem ToolToolStrip { get; set; }
        public ToolStripMenuItem ShowToolStrip { get; set; }
        public ToolStripMenuItem OtherToolStrip { get; set; }
    }

    public class SuouEditorDataCollection:SuperObservableCollection<SuouEditorDataClass>
    {
        #region Property
        //public List<SuouEditorDataClass> suouEditorDataList = new List<SuouEditorDataClass>();
        public Menubar Menubar { get; set; }
        public AstPage[] CopyPages { get; set; }
        public event EventHandler SaveChanged;
        TabControl _tab;
        TabControl Tab
        {
            get
            {
                return _tab;
            }
            set
            {
                this._tab = value;
                this._tab.SelectedIndexChanged += tab_SelectedIndexChanged;
                
                
            }
        }
        public List<string> FilePathList
        {
            get
            {
                List<string> r = new List<string>();
                foreach (SuouEditorDataClass sed in this)
                    r.Add(sed.StoryFile.FullFilePath);
                return r;
            }
        }
        public SuouEditorDataClass SelectData
        {
            get
            {
                var tp = this.Tab.SelectedTab;
                if (tp == null) return null;
                var v = ((SuouEditorDataClass)tp.Tag);
                return v;
            }
        }
        public event Action<object,SuouEditorDataClass> TabChanged;
        public event BookContentChangeEventHandler TabSizeChanged;
        #endregion

        #region EventMethod
        public void DataClass_DataChanged(object sender, EventArgs e)
        {
            var f = (SuouEditorDataClass)sender;
            var tabpage = this.GetTabPage(f);
            if (tabpage != null)
            {
                tabpage.Text = f.Title;
            }
        }
        private void tab_SelectedIndexChanged(object sender, EventArgs e)
        {
            var f = this.SelectData;
            if (f == null)
            {
                if (TabChanged != null) TabChanged(this, null);
            }
            else
            {
                if (TabChanged != null) TabChanged(sender, f);
            }
        }

        #endregion
        /*
        #region GetEnumerator
        public IEnumerator<SuouEditorDataClass> GetEnumerator()
        {
            for (int i = 0; i < this.Count; i++)
                yield return suouEditorDataList[i];
        }

        #endregion
        */

        private SuouEditorDataCollection()
        {
            this.CollectionChanged += this.Item_CollectionChanged;
        }
        public void AddFilePath(string path)
        {
            var book = IBookHandler.TryLoad(path);
            if (book != null)
            {
                var p = new SuouEditorDataClass(book);
                this.Add(p);
            }
        }
        private void Item_CollectionChanged(object sender, SuperObservableCollectionEventArgs<SuouEditorDataClass> e)
        {
            switch (e.Action)
            {
                case SuperObservableCollectionAction.Add:
                    foreach (var v in e.AddedItems)
                    {
                        this.AppendTab(v);
                    }
                    break;
                case SuperObservableCollectionAction.Remove:
                    foreach (var v in e.RemovedItems)
                    {
                        v.DataChanged -= this.DataClass_DataChanged;
                        var tp = this.Tab.TabPages.Cast<TabPage>().First((w) => w.Tag == v);
                        this.Tab.TabPages.Remove(tp);
                    }
                    break;
                default: throw new Exception();
            }
        }
        public SuouEditorDataCollection(TabControl tab,Menubar mbar):this()
        {
            this.Tab = tab;

            this.Menubar = mbar;
        }

        #region Method

        public void StartEnd()
        {
            this.ForEach((v) => v.StartEnd());
        }

        public TabPage IsDuplicate(FileInfo fi)
        {
            foreach (TabPage tp in Tab.TabPages)
            {
                var p = (SuouEditorDataClass)tp.Tag;
                if (fi.FullName == p.StoryFile.FullFilePath)
                {
                    return tp;
                }
            }
            return null;
        }
        /*
        public void Add(string filePath)
        {
            if (filePath == null) return;
            TabPage tp;
            var p = new FileInfo(filePath);
            if (!p.Exists)
            {
                ErrorMessage.Show(ErrorMessage.Warning.NotFoundFile, filePath);
                return;
            }
            if ((tp = IsDuplicate(p)) != null)//重複チェック
            {
                Tab.SelectTab(tp);
                return;
            }

            var book = IBookHander.Load(filePath).ToChapterBook2();
            var suou = new SuouEditorDataClass(book);
            this.Add(suou);
        }
        
        public void Add(IBook ib)
        {
            var suou = new SuouEditorDataClass(ib);
            this.Add(suou);
        }
        
        private void Add(SuouEditorDataClass suou)
        {
            var v = new TabPage();
            this.Add(v, suou);
        }
         */
        private void AppendTab(SuouEditorDataClass sedc)
        {
            var tp = new TabPage();
            tp.Tag = sedc;
            tp.Text = sedc.Title;
            sedc.DataChanged += this.DataClass_DataChanged;
            ///BottleNeck
            
            {
                if (this.Tab.TabPages.Count != 0)
                {
                    this.Tab.TabPages.Add(tp);
                    this.Tab.SelectedTab = tp;
                }
                else
                {
                    this.Tab.TabPages.Add(tp);
                    this.Tab.SelectedIndex = -1;
                    this.Tab.SelectedTab = tp;
                }
            }
            this.InvokeTabSizeChanged(sedc);
        }
        
        private string GetNewName(SuouBase.AAFileExtension type)
        {
            return SuouEditorConst.NewFileName(type);
            
        }

        public void AddNewFile()
        {
            //var v = new SuouEditorDataClass(new MergeBook() { Name = NewName});
            var v = new SuouEditorDataClass(new ObsoleteChapterBook() { Name = this.GetNewName(AAFileExtension.Gst) });
            
            this.Add(v);
        }
        public void AddNewFile(SuouBase.AAFileExtension ext)
        {
            IBook f = null;
            switch (ext)
            {
                case AAFileExtension.Sst:
                    f = new Book.SectionNBook();
                    break;
                case AAFileExtension.Gst:
                    f = new Book.Chapter.ChapterBook2();
                    break;
                case AAFileExtension.Ast:
                    f = new EditingFile();
                    break;
                case AAFileExtension.Mlt:
                    f = new EditingFile();
                    break;
            }
            f.Name = this.GetNewName(ext);
            var v = new SuouEditorDataClass(f);
            this.Add(v);
        }
        public bool AutoBackUp()
        {
            if (this.SelectData != null)
            {
                this.SelectData.BackupSave();
                this.SelectData.BackupCheck();
            }
            return true;
        }

        public bool SaveWithName()
        {
            var b = this.SelectData.SaveWithName();
            this.Tab.SelectedTab.Text = this.SelectData.Title;
            if (this.SaveChanged != null) this.SaveChanged(this.SelectData.StoryFile, new EventArgs());
            return b;
        }

        public bool SaveOverRide()
        {
            return this.SelectData.SaveOverRide();
        }

        public void NewStoryPage()
        {
            this.SelectData.StoryFile.Insert(this.SelectData.StoryFile.Count, new SectionScene());
        }
        public TabPage GetTabPage(SuouEditorDataClass f)
        {
            for (int i = 0; i < this.Tab.TabCount; i++)
            {
                if (this.Tab.TabPages[i].Tag == f)
                {
                    return this.Tab.TabPages[i];
                }
            }
            return null;
        }
        public bool CloseAllTab(SaveFileDialog sfd)
        {
            while (SelectedClose(sfd))
            {
            }

            if (this.Tab.SelectedTab == null)
            {
                return true;
            }
            else return false;

        }

        private void InvokeTabSizeChanged(SuouEditorDataClass sedc)
        {
            if (this.TabSizeChanged != null)
            {
                var f = new BookContentChangeEventArgs(sedc.StoryFile, BookContentChangeType.Other);
                this.TabSizeChanged(this, f);
            }
        }

        public bool SelectedClose(SaveFileDialog sfd)
        {
            var tp = this.Tab.SelectedTab;
            if (tp == null) return false;
            var v = ((SuouEditorDataClass)tp.Tag);
            if (v.Close())
            {
                //this.Tab.TabPages.Remove(tp);
                this.Remove(v);
                this.InvokeTabSizeChanged(v);
                return true;
            }
            else
            {
                return false;
            }

        }
        #endregion
    }
    public class SuouEditorDataClass
    {
        [ViewInstance]
        public IBook StoryFile { get; set; }
        public string Title { get; set; }
        public bool IsBackup { get; set; }

        public event EventHandler DataChanged;
        public event EventHandler Closed;
        public bool IsChanged
        {
            get { return this.StoryFile.IsEdit; }
            set { this.StoryFile.IsEdit = value; }
        }
        public SuouEditorDataClass(IBook storyfile)
        {
            this.StoryFile = storyfile;
            this.StoryFile.ContentChanged += this.EditingFile_Changed;
            this.StoryFile.IsEdit = false;
            ReTitle();
            //ReadSettingFile(this.StoryFile.Name);
        }

        public void StartEnd()
        {
            this.StoryFile.IsEdit = false;
            ReTitle();
        }

        private SuouEditorDataClass(string filePath)
        {
            StoryFile = IBookHandler.Load(filePath);
            this.StoryFile.ContentChanged += this.EditingFile_Changed;
            this.StoryFile.IsEdit = false;
            ReTitle();
        }
        public SuouEditorDataClass(SuouBase.Book.Chapter.ChapterBook2 book)
        {
            StoryFile = book;
            this.StoryFile.ContentChanged += this.EditingFile_Changed;
            this.StoryFile.IsEdit = false;
            ReTitle();
        }
        public void ReTitle()
        {
            this.Title = this.StoryFile.Name + (this.StoryFile.IsEdit ? "*" : "");
            if (DataChanged != null) DataChanged(this, new EventArgs());
        }
        public void EditingFile_Changed(object sender, BookContentChangeEventArgs e)
        {
            this.IsBackup = true;
            ReTitle();
        }

        public string BackUpFolderName
        {
            get
            {
                var name = this.StoryFile.Name;
                return SuouEditorConst.GetBackupFolderPath(name);
            }
        }

        public string BackUpName
        {
            get
            {
                var name = this.StoryFile.Name;
                var dirname = Path.GetFileNameWithoutExtension(name);
                var f = new DirectoryInfo(BackUpFolderName);
                
                var filename = BackUpFolderName + "/" + TimeStamp() +"_" + name;
                return filename;
            }
        }

        public void BackupSave()
        {
            if (!this.StoryFile.IsEdit || !this.IsBackup) return;
            var f = new DirectoryInfo(BackUpFolderName);
            if (!f.Exists) f.Create();
            this.Save(BackUpName, false, false, false);
            this.IsBackup = false;
        }
        public void BackupCheck()
        {
            var info = new DirectoryInfo(this.BackUpFolderName);
            if (!info.Exists) return;
            var p = new SuouBase.Base.BackupCollection(this.BackUpFolderName,System.IO.Path.GetExtension(this.StoryFile.Name));
            p.Remove(10, 2);
        }


        public static string TimeStamp()
        {
            string r = "";
            var s = System.DateTime.Now;
            r += s.ToFileTime();
            return r;
        }
       
        public bool CloseWarning()
        {
            DialogResult result = this.StoryFile.IsExistFile ? GeneralMethod.OverWriteKakuninBox(this.Title) : GeneralMethod.SaveKakuninBox(this.Title);

            switch (result)
            {
                case DialogResult.Yes:
                    return SaveOverRide();
                case DialogResult.No:
                    //IsEdit = false;
                    return true;
                case DialogResult.Cancel:
                    return false;
                default:
                    return false;
            }
        }
        public bool SaveOverRide()
        {
            if (!this.StoryFile.IsExistFile) SaveWithName();
            var name = this.StoryFile.FullFilePath;
            if (this.StoryFile.IsExistFile)
            {
                var f = new FileInfo(this.StoryFile.FullFilePath);
                if (f.Exists)
                {
                    this.Save(this.StoryFile.FullFilePath, true, true, true);
                    return true;
                }
                else
                {
                    return SaveWithName();
                }
            }
            else
            {
                return false;
            }
        }

        public void Save(string filename,bool OffEdit,bool ChangeName,bool ChangeFullFilePath)
        {
            var fi =new FileInfo(filename);
            var ext = fi.Extension;
            if (ext == SuouEditorConst.SectionBookExtension || ext == SuouEditorConst.ChapterBookExtension)
            {
                if (this.StoryFile is ObsoleteSectionBook || this.StoryFile is ObsoleteChapterBook || this.StoryFile is SuouBase.Book.Chapter.ChapterBook2)
                {
                    this.StoryFile.Save(filename, OffEdit);
                }
                else if (this.StoryFile is EditingFile)
                {
                    ((EditingFile)this.StoryFile).ToMergeBook().Save(filename, OffEdit);
                }
                else
                {
                    throw new Exception();
                }
            }
            else if (ext == SuouEditorConst.AstExtension)
            {
                EditingFile.WriteFile.WriteStoryFile(fi, this.StoryFile);
            }
            else if (ext == SuouEditorConst.MltExtension)
            {
                EditingFile.WriteFile.WriteStoryFile(fi, this.StoryFile);   
            }
            else if (ext == SuouEditorConst.TestExtension)
            {
                ((ObsoleteSectionBook)this.StoryFile).SaveXML(fi.FullName);
            }
            else
            {
                MessageBox.Show("保存できません");
            }
            if (ChangeName) this.StoryFile.Name = fi.Name;
            if (ChangeFullFilePath) this.StoryFile.FullFilePath = fi.FullName;
            if (OffEdit) this.StoryFile.IsEdit = false;
            //ReTitle();
            ReTitle();
        }
        public void PutFile(string filename)
        {
            this.Save(filename, false, false, false);
        }

        public bool SaveWithName()
        {
            string filename = GeneralMethod.SaveDialogBox(this.StoryFile);

            if (filename != null)
            {
                var fi = new FileInfo(filename);
                var ext = fi.Extension;
                if (ext == SuouEditorConst.SectionBookExtension || ext == SuouEditorConst.ChapterBookExtension)
                {
                    this.Save(fi.FullName,true,true,true);
                }
                else if(!(this.StoryFile is EditingFile)){
                    if (ext == SuouEditorConst.AstExtension)
                    {
                        MessageBox.Show("レイヤーの情報が失われますがよろしいですか？(元データに影響は与えません)");
                        this.Save(fi.FullName, true, true, true);
                    }
                    else if (ext == SuouEditorConst.MltExtension)
                    {
                        MessageBox.Show("レイヤーの情報が失われますがよろしいですか？(元データに影響は与えません)");
                        this.Save(fi.FullName, true, true, true);
                    }
                    else
                    {
                        MessageBox.Show("拡張子無しで保存できません");
                        return false;
                    }
                }
                else if (ext == SuouEditorConst.AstExtension || ext == SuouEditorConst.MltExtension)
                {
                    this.Save(fi.FullName, true, true, true);
                }
                else
                {
                    MessageBox.Show("拡張子無しで保存できません");
                    return false;
                }
                
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool Close()
        {
            var b = false;
            if (this.StoryFile.IsEdit)
            {
                b = this.CloseWarning();
            }
            else
            {
                b = true;
            }
            if (b && this.Closed != null) this.Closed(this, new EventArgs());
            return b;
        }

        private bool SaveNoFormat(string filename)
        {
            this.StoryFile.Save(filename,true);
            return true;
        }
    }
    public class FolderDataCollection
    {
        private List<FolderData> localTemplateDataList = new List<FolderData>();

        public FolderData SelectTemlateData { get; set; }
        public List<FolderData> Data { get { return localTemplateDataList; } }

        public FolderDataCollection()
        {
        }
        public FolderDataCollection(string folderpath):this()
        {
            this.Add(folderpath);
        }

        public IEnumerator<FolderData> GetEnumerator()
        {
            for (int i = 0; i < localTemplateDataList.Count; i++)
                yield return localTemplateDataList[i];
        }
        public bool Add(string FolderPath)
        {
            var v = new FolderData(FolderPath);
            this.localTemplateDataList.Add(v);
            return true;
        }

        public bool Add(FolderData ltdc)
        {
            if (!localTemplateDataList.Contains(ltdc))
            {
                this.localTemplateDataList.Add(ltdc);
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Save(string filename)
        {
            LocalTemplateXML.SaveNowTabPage(filename, this);
        }
        public bool Remove(FolderData tdc)
        {
            return this.localTemplateDataList.Remove(tdc);
        }

        public List<TempStruct> ToTempStruct()
        {
            List<TempStruct> tp = new List<TempStruct>();
            foreach (FolderData tdc in localTemplateDataList)
                tp.Add(new TempStruct(tdc));
            return tp;
        }
        /*
        public void ListUp()
        {
            this.Data.ToList().ForEach((v) => v.ListUp());
        }
        */
        
    }

    [Serializable()]
    public struct TempStruct
    {
        public string Title;
        public string FolderPath;

        public TempStruct(FolderData tdc)
        {
            this.Title = tdc.Title;
            this.FolderPath = tdc.FolderPath;
        }
    }
    public class FolderData
    {
        public string Title { get; set; }
        public string FolderPath { get; set; }

        [XmlIgnore]
        public TreeNodeCollection TemplateData { get; set; }

        public FolderData()
        {
            var tnc = (new TreeNode()).Nodes;
            this.Title = "";
            this.TemplateData = tnc;
        }
        
        public FolderData(string title, TreeNodeCollection localTemplateData)
        {
            this.Title = title;
            this.TemplateData = localTemplateData;
        }

        public FolderData(string title, TreeNode tn)
        {
            var p = (TreeNode)tn.Clone();
            var tnc = (new TreeNode()).Nodes;
            tnc.Add(p);
            this.Title = title;
            this.TemplateData = tnc;
        }

        public FolderData(string title, string DirName)
        {
            this.Title = title;
            this.FolderPath = DirName;
            this.Load(DirName);
        }

        public FolderData(string DirName)
        {
            this.Load(DirName);
            this.Title = new FileInfo(DirName).Name;
            FolderPath = DirName;
        }
        /*
        public void ListUp()
        {
            this.TemplateData.ListUP();
        }
        */
        #region
        public void Load(string DirName)
        {

            ParallelLoad(DirName);
        }

        private void ParallelLoad(string DirName)
        {
            var tv = AAFileTreeClass.MakeTree(DirName);
            TemplateData = tv.Nodes;
        }
        #endregion
    }
}
