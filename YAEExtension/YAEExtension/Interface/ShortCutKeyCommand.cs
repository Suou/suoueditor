﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
namespace SuouBase.Interface
{
    public class ShortCutCommandCollection
    {
        public virtual string Name { get; set; }
        public virtual ShortCutCommand[] Items { get; set; }
    }
    [Serializable]
    public class ShortCutCommand:ShortCutKey
    {
        public string Name { get; set; }        
        public ShortCutCommand(string Name)
        {
            this.Name = Name;
        }
        public ShortCutCommand(string Name,Keys key)
        {
            this.Name = Name;
            this.RawKey = key;
        }
        public ShortCutCommand()
        {
        }
    }
    public static class KeysExtension
    {
        public static bool HasKey(this Keys me, Keys key)
        {
            return (me & key) == key;
        }
        public static Keys Minus(this Keys me, Keys key)
        {
            return (me ^ key) & me;
        }
    }
    [Serializable]
    public class ShortCutKey
    {
        public bool IsCtrl
        {
            get { return this.RawKey.HasKey(Keys.Control); }
        }
        public bool IsAlt
        {
            get { return this.RawKey.HasKey(Keys.Alt); }
        }
        public bool IsShift
        {
            get { return this.RawKey.HasKey(Keys.Shift); }
        }
        public Keys RawKey { get; set; }
        public Keys NaiveKey
        {
            get
            {
                return this.RawKey.Minus(Keys.Control | Keys.Alt | Keys.Shift);
            }
        }
        public ShortCutKey(Keys key)
        {
            this.RawKey = key;
        }
        
        public static bool operator ==(ShortCutKey a,Keys b){
            return a.RawKey == b;
        }
        public static bool operator ==(ShortCutKey a, ShortCutKey b)
        {
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }
            return a.RawKey == b.RawKey;

        }
        public static bool operator !=(ShortCutKey a, ShortCutKey b)
        {
            return !(a == b);
        }
        public static bool operator !=(ShortCutKey a, Keys b)
        {
            return !(a == b);

        }
        public override string ToString()
        {
            var s = "";
            List<string> r = new List<string>();
            if (IsCtrl) r.Add(Keys.Control.ToString());
            if (IsAlt) r.Add(Keys.Alt.ToString());
            if (IsShift) r.Add(Keys.Shift.ToString());
            r.Add(NaiveKey.ToString());

            for (int i = 0; i < r.Count(); i++)
            {
                s += r[i];
                if (i != r.Count() - 1) s += "+";
            }
            return s;
        }
        /*
        public ShortCutKey(Keys key)
        {
            KeyBoardKey = key;
        }
        */
        public ShortCutKey()
        {
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public static Keys[] NormalKey = new Keys[] { Keys.A, Keys.B, Keys.C, Keys.D, Keys.E, Keys.F, Keys.G, Keys.H, Keys.I, Keys.J, Keys.K, Keys.L,
        Keys.M,Keys.N,Keys.O,Keys.P,Keys.Q,Keys.R,Keys.S,Keys.T,Keys.U,Keys.V,Keys.W,Keys.X,Keys.Y,Keys.Z,
        Keys.F1,Keys.F2,Keys.F3,Keys.F4,Keys.F5,Keys.F6,Keys.F7,Keys.F8,Keys.F9,Keys.F10,Keys.F11,Keys.F12};

        public static ShortCutKey[] GetAllCommand()
        {
            var r = new List<ShortCutKey>();
            bool[] b = new bool[] { true, false };
            r.Add(new ShortCutKey(Keys.None));
            foreach (var Key in NormalKey)
            {
                var k = Key;
                foreach (var isAlt in b)
                {
                    k = Key;
                    if (isAlt) k = k | Keys.Alt;
                    if (k != Key) r.Add(new ShortCutKey(k));

                }
                /*
                foreach (var isShift in b)
                {
                    k = Key;
                    if (isShift) k = k | Keys.Shift;
                    if (k != Key) r.Add(new ShortCutKey(k));

                }
                */
                foreach (var isCtrl in b)
                {
                    k = Key;
                    if (isCtrl) k = k | Keys.Control;
                    if (k != Key) r.Add(new ShortCutKey(k));
                }
            }
            
            return r.ToArray();
        }
    }
}
