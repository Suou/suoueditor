﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Drawing;
using System.IO;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using SuouBase;
using SuouBase.Interface;
using Microsoft.VisualBasic;
using System.Xml.Serialization;
namespace SuouBase
{

    public class LocalTemplateXML
    {
        public void ReadSettingFile(string FileName)
        {
            var f = AAFileTreeClass.ReadFileTreeSetting(FileName);
            /*
            foreach (TemplateDataClass tnc in f)
            {
                var f2 = NewTabPage();
                f2.AddFileTreeNode(tnc.TemplateData);
                f2.Text = tnc.Title;
            }
             */
        }

        public static void SaveNowTabPage(string FileName, FolderDataCollection tdc)
        {
            var d = new DirectoryInfo(SuouEditorConst.FileTreeDirectoryName);
            if (!d.Exists) d.Create();


            XmlTextWriter xtw = null;
            string fname = "";
            if (Path.GetExtension(FileName) == ".vwf")
            {
                fname = FileName;
            }
            else
            {
                fname = SuouEditorConst.GetFileTreeFile(FileName);
            }
            xtw = new XmlTextWriter(fname, Encoding.Default);
            xtw.Formatting = Formatting.Indented;
            string[] separator = { "\r\n" };

            xtw.WriteStartDocument();//XML宣言
            xtw.WriteStartElement("FileTreeSetting");//開始タグ
            {

                foreach (FolderData tdclass in tdc)
                {
                    xtw.WriteStartElement("File");
                    {
                        SaveXML(xtw, tdclass);
                    }
                    xtw.WriteEndElement();
                }
            }
            xtw.WriteEndElement();//htmlタグを閉じる
            xtw.WriteEndDocument();
            xtw.Close();
        }


        private static void SaveXML(XmlTextWriter xtw, FolderData tdc)
        {
            xtw.WriteAttributeString("Name", tdc.Title);
            SaveFileTree(xtw, tdc.TemplateData);
        }

        private static void SaveFileTree(XmlTextWriter xtw, TreeNodeCollection tnc)
        {
            SaveXMLRecursive(xtw, tnc);
        }
        private static void SaveXMLRecursive(XmlTextWriter xtw, TreeNodeCollection TNC)
        {

            foreach (TreeNode TN in TNC)
            {
                if (TN.Tag is FolderNodeClass)
                {
                    xtw.WriteStartElement("Folder");
                    {
                        xtw.WriteAttributeString("Name", TN.Text);
                        xtw.WriteAttributeString("Path", ((FolderNodeClass)TN.Tag).FullName);
                        SaveXMLRecursive(xtw, TN.Nodes);
                    }
                    xtw.WriteEndElement();
                }
                else
                {
                    xtw.WriteStartElement("File");
                    {
                        xtw.WriteAttributeString("Name", TN.Text);
                        xtw.WriteElementString("Path", ((FileNodeClass)TN.Tag).FullName);
                    }
                    xtw.WriteEndElement();
                }
            }
        }
    }

    public class AAEditorXml
    {
        public static void MakeNameDictionary(Dictionary<string, List<ToolStripItem>> Dic,ToolStripItemCollection TSIC)
        {
            foreach (ToolStripItem vv in TSIC)
            {
                if (vv is ToolStripMenuItem)
                {
                    RecursiveMakeNameDictionary(Dic, (ToolStripMenuItem)vv);
                }
            }

        }

        public static void RecursiveMakeNameDictionary(Dictionary<string, List<ToolStripItem>> Dic, ToolStripMenuItem TTMI)
        {
            if (Dic.ContainsKey(TTMI.Text))
            {
                Dic[TTMI.Text].Add(TTMI);
            }
            else
            {
                Dic[TTMI.Text] = new List<ToolStripItem> { TTMI };
            }
            if (TTMI.DropDownItems != null)
            {
                foreach (ToolStripItem vv in TTMI.DropDownItems)
                {
                    if (vv is ToolStripMenuItem)
                    {
                        RecursiveMakeNameDictionary(Dic, (ToolStripMenuItem)vv);
                    }
                }
            }
        }
    }
}
