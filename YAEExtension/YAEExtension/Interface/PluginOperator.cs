﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Reflection;
using System.IO;

namespace SuouBase.Interface
{
    public class PluginOperator
    {
        public static IComponentPluginAttribute GetAttribute(IComponentPlugin pb)
        {
            var t = pb.GetType();
            var at = t.GetCustomAttributes(typeof(IComponentPluginAttribute), false);
            if (at.Length == 0) return null;
            return (IComponentPluginAttribute)at[0];
            
        }

        public static bool CanControlize(IComponentPlugin pb)
        {
            var att = PluginOperator.GetAttribute(pb);
            return (att.Type & PluginType.Control) == PluginType.Control;
        }
        public static bool CanFormize(IComponentPlugin pb)
        {
            var att = PluginOperator.GetAttribute(pb);
            return (att.Type & PluginType.Form) == PluginType.Form;
        }

        public static IComponentPluginMetaData GetIComponentPluginMetaData(IComponentPlugin pb, int nth)
        {
            var pcs = new IComponentPluginMetaData();
            
            Type t = pb.GetType();
            var fi = new FileInfo(t.Module.Name);
            var atts = t.GetCustomAttributes(typeof(IComponentPluginAttribute), false);
            var att = (atts.Length > 0) ? (IComponentPluginAttribute)atts[0] : null;

            pcs.NameSpace = t.Namespace;
            pcs.ClassName = t.Name;
            pcs.DLLName = Path.GetFileNameWithoutExtension(fi.Name);
            pcs.PluginName = (att != null) ? att.Name : "None";
            pcs.Position = nth;
            return pcs;
        }
        public static IFrameworkPluginMetaData GetIFrameworkPluginMetaData(IFrameworkPlugin pb)
        {
            var pcs = new IFrameworkPluginMetaData();

            Type t = pb.GetType();
            var fi = new FileInfo(t.Module.Name);
            var atts = t.GetCustomAttributes(typeof(IComponentPluginAttribute), false);
            var att = (atts.Length > 0) ? (IComponentPluginAttribute)atts[0] : null;

            pcs.NameSpace = t.Namespace;
            pcs.ClassName = t.Name;
            pcs.DLLName = Path.GetFileNameWithoutExtension(fi.Name);
            pcs.PluginName = (att != null) ? att.Name : "None";
            return pcs;
        }

        public static void CallSet(EditorInfo DLLFunction,CompleteSet cs)
        {

            DLLFunction.Setting.FrameSetting = cs.Frame;
            CallFramework(DLLFunction, cs.Frame);
            foreach (IComponentPluginMetaData cp in cs.PluginSet)
            {
                Call(DLLFunction, cp);
            }
        }

        public static void CallFramework(EditorInfo DLLFunction, IFrameworkPluginMetaData cp)
        {
            cp.Plugin = CallFramework(cp.DLLName, cp.NameSpace, cp.ClassName, DLLFunction, cp.PluginName);
        }
        public static IFrameworkPlugin GetCallFramework(EditorInfo DLLFunction, IFrameworkPluginMetaData cp)
        {
            return CallFramework(cp.DLLName, cp.NameSpace, cp.ClassName, DLLFunction, cp.PluginName);
        }
        public static IFrameworkPlugin CallFramework(string DLLName, string NameSpace, string Class, EditorInfo DLLFunction,string PluginName)
        {
         #pragma warning disable
            var lib = Assembly.LoadWithPartialName(DLLName);
         #pragma warning restore

            var type = lib.GetType(NameSpace + "." + Class);
            var d = (IFrameworkPlugin)Activator.CreateInstance(type, DLLFunction);
            if (d == null) throw new Exception("PluginFrameworkError");
            if (d is IFrameworkPlugin) DLLFunction.Framework = (IFrameworkPlugin)d;
            return d;
        }
        public static void Call(EditorInfo DLLFunction, IComponentPluginMetaData cp)
        {
            cp.Plugin = Call(cp.DLLName, cp.NameSpace, cp.ClassName, DLLFunction, cp.Position, cp.PluginName);
            DLLFunction.PluginList.Add(cp);
            DLLFunction.Framework.SetPlugin(cp);
            
        }
        public static void DirectCall(EditorInfo DLLFunction, IComponentPluginMetaData cp)
        {
            DLLFunction.PluginList.Add(cp);
            DLLFunction.Framework.SetPlugin(cp);
        }
        public static IComponentPlugin Call(string DLLName, string NameSpace, string Class, EditorInfo DLLFunction, int Pos, string PluginName)
        {

            #pragma warning disable
            var lib = Assembly.LoadWithPartialName(DLLName);
            #pragma warning restore

            IComponentPlugin d = null;



            
            try
            {
                var type = lib.GetType(NameSpace + "." + Class);
                d  = (IComponentPlugin)Activator.CreateInstance(type,DLLFunction);
                if (d == null) throw new Exception("PluginError");

               
                
            }
            catch (Exception e)
            {
                MessageBox.Show("DLLCallError\n" + NameSpace + "." + Class + "にて例外が発生しました\n" + e.Message);

                //System.Environment.Exit(-1);
                throw e;
            }
            finally { }
            
            return d;
        }
            
    }
}
