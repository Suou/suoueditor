﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Text;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Collections.Specialized;
using SuouBase.Book;
namespace SuouBase
{
    public delegate void AstEventHandler(object sender, AstEventArgs e);
    public delegate void EditingFileEventHandler(object sender, EditingFileEventArgs e);
    public delegate void FolderNodeEventHandler(object sender, FolderNodeEventArgs e);
    public delegate void FileNodeEventHandler(object sender, FileNodeEventArgs e);

    public delegate void BookContentChangeEventHandler(object sender, BookContentChangeEventArgs e);
    public delegate void BookPageContentChangeEventHandler(object sender, BookPageContentChangeEventArgs e);
    public delegate void SectionBookPagePartContentChangeEventHandler(object sender, SectionBookPagePartContentChangeEventArgs e);

    public class BookPageContentChangeEventArgs : EventArgs
    {
        public BookContentChangeEventArgs Tag { get; private set; }
        public PageContentChangeType Type { get; private set; }
        public IBookPage Page { get; private set; }
        public BookPageContentChangeEventArgs(IBookPage fnc,PageContentChangeType type)
        {
            this.Page = fnc;
            this.Type = type;
        }
        public BookPageContentChangeEventArgs(IBookPage fnc, BookContentChangeEventArgs e)
        {
            this.Page = fnc;
            this.Type = PageContentChangeType.ChapterPageInnerType;
            this.Tag = e;
        }
    }
    public class SectionBookPagePartContentChangeEventArgs : EventArgs
    {
        public MergeContentChangeType Type { get; private set; }
        public AbstractSectionScenePart PagePart { get; private set; }
        public SectionBookPagePartContentChangeEventArgs(AbstractSectionScenePart fnc, MergeContentChangeType type)
        {
            this.PagePart = fnc;
            this.Type = type;
        }
    }
    public class BookContentChangeEventArgs : EventArgs
    {
        public static BookContentChangeEventArgs Create(IBook fnc, BookPageContentChangeEventArgs e)
        {
            var p = new BookContentChangeEventArgs(fnc, e);
            return p;
        }
        public static BookContentChangeEventArgs Create(IBook fnc, SuperObservableCollectionAction e, int n)
        {
            var p = new BookContentChangeEventArgs(fnc, BookContentChangeType.Page,e, n);
            return p;
        }
        public static BookContentChangeEventArgs Create(IBook fnc, SuperObservableCollectionAction e, int newIndex, int oldIndex)
        {
            var p = new BookContentChangeEventArgs(fnc, BookContentChangeType.Page, e, newIndex,oldIndex);
            return p;
        }
        /*
        public static BookContentChangeEventArgs Create(IBook fnc, BookContentChangeType type,int n)
        {
            var p = new BookContentChangeEventArgs(fnc, type, n);
            return p;
        }
        */
        public static BookContentChangeEventArgs Create(IBook fnc, BookContentChangeType type, SuperObservableCollectionAction act, int n)
        {
            var p = new BookContentChangeEventArgs(fnc, type,act, n);
            return p;
        }
        public IBook Book { get; set; }
        private int _ChangeIndex = -1;
        public int ChangeIndex
        {
            get
            {
                if (this._ChangeIndex == -1 && this.PageEventArgs != null)
                {
                    this._ChangeIndex = this.Book.BookPages.FindIndex((v) => this.PageEventArgs.Page == v);
                }
                return this._ChangeIndex;
            }
        }
        public int OldIndex { get; private set; }
        public BookPageContentChangeEventArgs PageEventArgs { get; set; }
        public BookContentChangeType Type { get; private set; }
        public SuperObservableCollectionAction PagesAction { get; set; }
        public BookContentChangeEventArgs(IBook fnc,BookContentChangeType type)
        {
            this.Book = fnc;
            this.Type = type;
        }
        public BookContentChangeEventArgs(IBook fnc, BookContentChangeType type,int i)
        {
            this.Book = fnc;
            this.Type = type;
            this._ChangeIndex = i;
        }
        public BookContentChangeEventArgs(IBook fnc, BookContentChangeType type, SuperObservableCollectionAction act, int n)
        {
            this.Book = fnc;
            this.Type = type;
            this._ChangeIndex = n;
            this.PagesAction = act;
        }
        public BookContentChangeEventArgs(IBook fnc, BookContentChangeType type, SuperObservableCollectionAction act, int n, int oldIndex)
        {
            this.Book = fnc;
            this.Type = type;
            this._ChangeIndex = n;
            this.OldIndex = oldIndex;
            this.PagesAction = act;
        }
        public BookContentChangeEventArgs(IBook fnc, BookPageContentChangeEventArgs e)
        {
            this.Book = fnc;
            this.Type = BookContentChangeType.PageContent;
            this._ChangeIndex = -1;
            this.PageEventArgs = e;
        }
    }

    public class ViewAstPage
    {
        public string[] Text;

        public ViewAstPage(IBookPage ap)
        {
            Text = ap.SText.Text.ToHTMLString().ToParagraphList().ToArray();
        }
    }

    public class FolderNodeEventArgs : EventArgs
    {
        public FolderNodeClass FolderNode;
        public FolderNodeEventArgs(FolderNodeClass fnc)
        {
            this.FolderNode = fnc;
        }
    }

    public class FileNodeEventArgs : EventArgs
    {
        public FileNodeClass FileNode;
        public FileNodeEventArgs(FileNodeClass fnc)
        {
            this.FileNode = fnc;
        }
    }

    public class EditingFileEventArgs : EventArgs
    {
        public EditingFile File { get; set; }
        

        public EditingFileEventArgs(EditingFile ef)
            : base()
        {
            this.File = ef;
        }
    }

    public class AstEventArgs : EventArgs
    {
        public AstPage[] Pages{get;private set;}

        public AstPage Page
        {
            get
            {
                return Pages[0];
            }
        }
        public bool IsPage
        {
            get
            {
                return Pages.Length >= 1;
            }
        }

        public AstEventArgs(AstPage[] ap):base()
        {
            this.Pages = ap;
        }
        public AstEventArgs(AstPage page)
            : base()
        {
            this.Pages = new AstPage[1];
            this.Pages[0] = page;
        }
    }
}
