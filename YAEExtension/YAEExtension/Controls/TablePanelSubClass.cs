﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Controls
{
    public class RowLine
    {
        public Control ParentCtrl { get; set; }
        public List<Control> Items { get; set; }
        public Point Location { get; set; }
        public Point LastLocation
        {
            get
            {
                var p = new Point(this.Location.X + this.Width, this.Location.Y);
                return p;
            }
        }
        public int Width
        {
            get
            {
                if (this.Items.Count == 0) return 0;
                return this.Items.Max((v) => v.Location.X+v.Width);
            }
        }
        public int Height
        {
            get
            {
                if (this.Items.Count == 0) return 0;
                return this.Items.Max((v) => v.Height);
            }
        }
        public int Bottom
        {
            get { return this.Location.Y + this.Height; }
        }
        public RowLine(Control parent, Point location)
        {
            this.Items = new List<Control>();
            this.ParentCtrl = parent;
            this.Location = location;
        }


        public void Add(Control ctrl)
        {
            ctrl.Location = this.LastLocation;
            this.Items.Add(ctrl);
        }
        public bool CanPut(Control ctrl)
        {
            if (this.LastLocation.X + ctrl.Width < ParentCtrl.Width)
            {
                return true;
            }
            return false;
        }
    }
    public class RowCollection
    {
        public Control ParentControl { get; set; }
        public List<RowLine> Item { get; set; }
        public Point LastLocation
        {
            get
            {
                if (this.Item.Count == 0) return new Point(0, 0);
                return new Point(this.Item.Last().Location.X, this.Item.Last().Bottom);
            }
        }

        public RowCollection(Control parent)
        {
            this.Item = new List<RowLine>();
            this.ParentControl = parent;
            this.AddNewRow();

        }
        private void AddNewRow()
        {
            var v = new RowLine(this.ParentControl, this.LastLocation);
            this.Item.Add(v);
        }
        private RowLine LastLine
        {
            get
            {
                return this.Item.Last();
            }
        }

        private void AddSub(Control ctrl)
        {
            if (this.Item.Count == 0)
            {
                this.AddNewRow();
            }
            else
            {
                if (!this.LastLine.CanPut(ctrl)) this.AddNewRow();
            }
            this.LastLine.Add(ctrl);
        }
        public void Add(Control ctrl)
        {
            this.ParentControl.Controls.Add(ctrl);
            this.AddSub(ctrl);
        }
        public void ReArrange()
        {
            var v = this.Item.SelectMany((p) => p.Items).ToList();
            
            this.Item.Clear();
            v.ForEach((p) => this.AddSub(p));
        }
    }
}
