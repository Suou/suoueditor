﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Base;
using SuouBase.Interface;
namespace SuouBase.Controls
{
    public partial class ProcessCmdTextBox : TextBox
    {
        public ProcessCmdTextBox()
        {
            InitializeComponent();
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool b = false;
            if (keyData.HasKey(Keys.Control) || keyData.HasKey(Keys.Alt))
            {
                b = false;
            }
            else
            {
                b = base.ProcessCmdKey(ref msg, keyData);
            }
            return b;
        }
    }
}
