﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
namespace SuouBase.Controls
{
    public partial class SentenceLocationBox : UserControl
    {
        public SentenceLocationBox()
        {
            InitializeComponent();
        }
        public event EventHandler SelectedChanged;
        public TextAlign Align
        {
            get
            {
                if (this.comboBox1.SelectedIndex == 0)
                {
                    return TextAlign.Left;
                }
                else if (this.comboBox1.SelectedIndex == 1)
                {
                    return TextAlign.Center;
                }
                else if (this.comboBox1.SelectedIndex == 2)
                {
                    return TextAlign.Right;
                }
                else if (this.comboBox1.SelectedIndex == 3)
                {
                    return TextAlign.Tate;
                }
                else
                {
                    return TextAlign.Left;
                }
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedChanged != null) this.SelectedChanged(sender, e);
        }
    }
}
