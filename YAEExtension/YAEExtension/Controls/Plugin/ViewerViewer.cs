﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Plugin;
using SuouBase.Interface;
namespace SuouBase.Controls.Plugin
{
    public partial class ViewerViewer : Form
    {
        public ViewerViewer()
        {
            InitializeComponent();
        }
        public ViewerViewer(EditorInfo info):this()
        {
            this.Info = info;
        }
        
        private EditorInfo _Info = null;
        public EditorInfo Info
        {
            get { return this._Info; }
            set
            {
                this._Info = value;
                if (value != null) this.Run();
            }
        }
        private void Run()
        {
            this.listBox1.Items.Clear();
            foreach (var v in this.Info.PluginList)
            {
                if (v.Plugin is IStoryPageListClass)
                {
                    this.listBox1.Items.Add(v);
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var item = (IComponentPluginMetaData)this.listBox1.SelectedItem;
            if (item != null)
            {
                this.Info.ReplaceEditor(item);
                this.Close();
            }
        }
    }
}
