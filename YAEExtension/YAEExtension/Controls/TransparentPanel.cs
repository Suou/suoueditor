﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Controls
{
    public partial class TransparentPanel : UserControl
    {
        public TransparentPanel()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.BackColor = Color.Transparent; // 透明
            this.BackColor = Color.FromArgb(100, 255, 255, 255); // 半透明
        }
        protected override void OnPaintBackground(System.Windows.Forms.PaintEventArgs pevent)
        {
            pevent.Graphics.Clear(this.Parent.BackColor);
            // 親コントロールとの間のコントロールを親側から描画
            for (int i = this.Parent.Controls.Count - 1; i >= 0; i--)
            {
                Control c = this.Parent.Controls[i];
                if (c == this)
                {
                    break;
                }
                if (this.Bounds.IntersectsWith(c.Bounds) == false || c.Visible == false)
                {
                    continue;
                }
                this.DrawBackControl(c, pevent);
            }
        }

        private void DrawBackControl(Control c, System.Windows.Forms.PaintEventArgs pevent)
        {
            using (Bitmap bmp = new Bitmap(c.Width, c.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
            {
                c.DrawToBitmap(bmp, new Rectangle(0, 0, c.Width, c.Height));

                int offsetX = (c.Left - this.Left) - (int)Math.Floor((double)(this.Bounds.Width - this.ClientRectangle.Width) / 2.0);
                int offsetY = (c.Top - this.Top) - (int)Math.Floor((double)(this.Bounds.Height - this.ClientRectangle.Height) / 2.0);
                pevent.Graphics.DrawImage(bmp, offsetX, offsetY, c.Width, c.Height);
            }
        }

        private void TransparentPanel_SizeChanged(object sender, EventArgs e)
        {
            //this.Refresh();
        }

        private void TransparentPanel_LocationChanged(object sender, EventArgs e)
        {
            this.Refresh();

        }
    }
}
