﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Book;
/*
namespace SuouBase.Controls
{
    public static class SectionPageContext
    {
        public static void CreatePageToolMenuItem(SectionScenePart page,ContextMenuStrip parent)
        {
            var r = new ToolStripMenuItem() { Text = "ページの詳細設定" };
            r.DropDownItems.Add(CreateA(page));
            r.DropDownItems.Add(CreateB(page));
            parent.Items.Add(r);
        }
        public static ToolStripMenuItem CreateB(SectionScenePart page)
        {
            var r = new ToolStripMenuItem() { Text = "合成方法" };
            var lrmPage = HukidasiContext.PermitLineMerge;
            var p = new ToolStripMenuItem[] { HukidasiContext.DestructiveMerge, HukidasiContext.FreeMerge, HukidasiContext.LineRestrictionMerge, lrmPage,HukidasiContext.HighDestructiveMerge };

            var numeric = new BSystem.Controls.ToolStripNumericUpDown() { AutoSize = false, Height = 32};
            
            lrmPage.DropDownItems.Add(numeric);
            lrmPage.DropDownOpening += (a, b) =>
            {
                numeric.Value = page.PermitZureLength;
            };
            numeric.ValueChanged += (a, b) =>
            {
                var n = (int)numeric.Value;
                if (n != page.PermitZureLength) page.PermitZureLength = n;
            };


            p.ForEach((v) => r.DropDownItems.Add(v));
            p.ForEach((v) => v.MouseDown += (a, b) => page.MergeOption = (MergeZureOption)v.Tag);
            r.Tag = page;
            r.DropDownOpening += (a, b) =>
            {
                var tools = a as ToolStripMenuItem;
                var spage = tools.Tag as SectionScenePart;
                foreach (ToolStripMenuItem item in tools.DropDownItems)
                {
                    var set = (MergeZureOption)item.Tag;
                    item.Checked = spage.MergeOption == set;
                }
            };
            
            return r;
        }
        public static ToolStripMenuItem CreateA(SectionScenePart page)
        {
            var r = new ToolStripMenuItem() { Text = "内部合成アルゴリズム" };
            var p = new ToolStripMenuItem[] { HukidasiContext.UniInAlgoItem, HukidasiContext.DefaultInAlgoItem, HukidasiContext.HukidasiDefaultInAlgoItem, HukidasiContext.CompleteCleanInAlgoItem };
            p.ForEach((v) => r.DropDownItems.Add(v));
            p.ForEach((v) => v.Click += (a, b) => page.Merger = ((SpaceLineSet)v.Tag));
            r.Tag = page;
            r.DropDownOpening += (a, b) =>
            {
                var tools = a as ToolStripMenuItem;
                var spage = tools.Tag as SectionScenePart;
                foreach (ToolStripMenuItem item in tools.DropDownItems)
                {
                    var set = item.Tag as SpaceLineSet;
                    item.Checked = spage.Merger == set;
                }
            };
            return r;
        }

    }
}
*/