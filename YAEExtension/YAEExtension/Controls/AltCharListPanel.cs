﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;

namespace SuouBase.Controls
{
    public partial class AltCharListPanel : UserControl
    {
        public AltCharListPanel()
        {
            InitializeComponent();
            this.AddButton.Click += this.AddButton_Clicked;
        }
        public Color BorderColor
        {
            get { return this.groupBox1.BorderColor; }
            set { this.groupBox1.BorderColor = value; }
        }
        private List<IListPanelItem> _Items;
        public List<IListPanelItem> Items
        {
            get { return _Items; }
            set
            {
                _Items = value;
                if (Items != null)
                {
                    foreach (var v in this.Items)
                    {
                        v.ItemDeleting += this.ItemDeleting;
                    }
                }
                CreateList();
            }
        }
        public IListPanelItem EmptyCreateItem { get; set; }
        public Button AddButton = new Button() { Text = "追加" };
        private bool _IsAddButton = true;
        public bool IsAddButton
        {
            get { return _IsAddButton; }
            set
            {
                _IsAddButton = value;
                if (_IsAddButton)
                {
                    this.AddButton.Visible = true;
                }
                else
                {
                    this.AddButton.Visible = false;
                }
            }
        }
        
        public void CreateList()
        {
            this.groupBox1.Controls.Clear();
            if (this.Items == null) return;
            foreach (var v in this.Items)
            {
                this.groupBox1.Controls.Add(v.Control);
            }
            this.groupBox1.Controls.Add(this.AddButton);
            Formize();
        }

        public void Formize()
        {
            Point position = new Point(14,this.groupBox1.Font.Height);
            if (this.Items.Count != 0)
            {
                foreach (var v in this.Items)
                {
                    v.Control.Location = position;
                    position.Y += v.Control.Size.Height;
                }
                this.AddButton.Location = position;
                position.Y = this.AddButton.Location.Y + this.AddButton.Height + 10;
                var width = new int[] { this.Items.Max((v) => v.Control.Size.Width), this.AddButton.Right, 100 }.Max();
                var height = new int[] { position.Y, 50 }.Max();
                this.Size = new Size(width, height);
            }
        }
        public void Delete(IListPanelItem Item)
        {
            this.Items.Remove(Item);
            CreateList();
        }
        public string GroupBoxName
        {
            get { return this.groupBox1.Text; }
            set { this.groupBox1.Text = value; }
        }
        private void ItemDeleting(object sender, EventArgs e)
        {
            var f = (IListPanelItem)sender;
            this.Delete(f);
        }
        private void AddButton_Clicked(object sender, EventArgs e)
        {
            if (EmptyCreateItem != null && this.Items != null)
            {
                var f = EmptyCreateItem.Empty();
                this.Items.Add(f);
                this.Items = this.Items;
            }
        }
    }
}
