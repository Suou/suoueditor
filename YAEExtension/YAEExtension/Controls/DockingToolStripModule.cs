﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
namespace SuouBase.Controls
{
    public class DockingToolStripModule
    {
        public Control Item { get; set; }
        public DockingToolStripModule(Control c)
        {
            this.Item = c;
            this.Item.ContextMenuStrip.Opening += this.ContextMenuStrip_DropOpening;
            this.Item.ContextMenuStrip.Items.Add(this.AddDockingButton);
        }
        public ToolStripItem AddDockingButton = new ToolStripMenuItem() { Text = "add" };
        public ToolStripItem RemoveDockingButton = new ToolStripMenuItem() { Text = "remove" };


        private void ContextMenuStrip_DropOpening(object sender, EventArgs e)
        {
        }
    }
}
