﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Files;
namespace SuouBase.Controls
{
    public class BookFolderOpenDialog
    {
        FolderBrowserDialog Dialog = new FolderBrowserDialog();

        public BookFolderOpenDialog()
        {
        }

        public SuouFileTree ShowDialog()
        {
            var rst = this.Dialog.ShowDialog();
            if (rst == DialogResult.OK)
            {
                return this.SelectedFileTree;
            }
            else
            {
                return null;
            }
        }
        public SuouFileTree SelectedFileTree
        {
            get
            {
                var filepath = new System.IO.DirectoryInfo(this.Dialog.SelectedPath);
                return new SuouFileTree(filepath);
            }
        }
    }
}
