﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Controls
{
    public partial class Rubberband : UserControl
    {
        public Rubberband()
        {
            InitializeComponent();
        }
        public int BorderWidth = 1;
        public void SetRegion()
        {

            //this.SetBounds(this.Left, this.Top, 301, 301,BoundsSpecified.Size);
            System.Drawing.Drawing2D.GraphicsPath path =
                new System.Drawing.Drawing2D.GraphicsPath();
            //丸を描く
            path.AddRectangle(new Rectangle(0, 0, this.Size.Width, this.Size.Height));
            //真ん中を丸くくりぬく
            path.AddRectangle(new Rectangle(BorderWidth, BorderWidth, this.Size.Width - (BorderWidth*2), this.Size.Height - (BorderWidth*2)));
            this.Region = new Region(path);
        }

        private void Rubberband_SizeChanged(object sender, EventArgs e)
        {
            this.SetRegion();
        }
    }
}
