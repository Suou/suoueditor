﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
namespace SuouBase.Controls
{
    public partial class CountDownTimer : UserControl
    {
        public CountDownTimer()
        {
            InitializeComponent();
        }
        private long _StartTime;
        public long StartTime
        {
            get { return this._StartTime; }
            set
            {
                this._StartTime = value;
                this.Start();
            }
        }
        public void Start()
        {
            this.IsCountStop = false;
            this.CountTimer.Restart();
            this.CountCheck();
        }
        public void Stop()
        {
            this.CountTimer.Stop();
            this.IsCountStop = true;
            this.textBox1.Text = "0";
        }
        public bool IsCountStop
        {
            get;
            set;
        }
        public bool IsZero
        {
            get
            {
                var n = this.StartTime - this.CountTimer.ElapsedMilliseconds;
                return n < 0;
            }
        }
        public TimeSpan NowTime
        {
            get
            {
                var n = this.StartTime - this.CountTimer.ElapsedMilliseconds;
                return TimeSpan.FromMilliseconds(n);
            }
        }
        public Stopwatch CountTimer = new Stopwatch();
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.CountCheck();
        }
        private void CountCheck()
        {
            if (this.IsZero) this.Stop();
            if (!this.IsCountStop)
            {
                this.textBox1.Text = this.NowTime.ToString();
            }
        
        }
    }
}
