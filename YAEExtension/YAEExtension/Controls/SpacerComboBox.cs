﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
namespace SuouBase.Controls
{
    public partial class SpacerComboBox : UserControl
    {
        public SpacerComboBox()
        {
            InitializeComponent();
        }
        public bool ChengeNowPageSetting { get; set; }
        [Browsable(false)]
        [ReadOnly(true)]
        public SpaceLineSet SelectedSpaceSet
        {
            get
            {
                var n = this.comboBox1.SelectedIndex;
                if (n == -1)
                {
                    return SpaceLineSet.DefaultSet;
                }
                else if (n == 0)
                {
                    return SpaceLineSet.DefaultSet;
                }
                else if (n == 1)
                {
                    return SpaceLineSet.HukidasiDefaultSet;
                }
                else if (n == 2)
                {
                    return SpaceLineSet.UniDefaultSet;
                }
                else if (n == 3)
                {
                    return SpaceLineSet.CompleteCleanDefaultSet;
                }
                return SpaceLineSet.HukidasiDefaultSet;
            }
            set
            {
                if (value != null)
                {
                    if (value.Type == SpaceLineSetType.Default)
                    {
                        this.comboBox1.SelectedIndex = 0;
                    }
                    else if (value.Type == SpaceLineSetType.HukidasiDefault)
                    {
                        this.comboBox1.SelectedIndex = 1;
                    }
                    else if (value.Type == SpaceLineSetType.Uni)
                    {
                        this.comboBox1.SelectedIndex = 2;
                    }
                    else if (value.Type == SpaceLineSetType.CompleteClean)
                    {
                        this.comboBox1.SelectedIndex = 3;
                    }
                    else
                    {
                    }
                }
            }
        }

        public event EventHandler SelectedChanged;

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (SelectedChanged != null) this.SelectedChanged(this, new EventArgs());
        }
    }
}
