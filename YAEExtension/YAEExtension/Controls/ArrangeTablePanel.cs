﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Controls
{
    public partial class ArrangeTablePanel : UserControl
    {
        public ArrangeTablePanel()
        {
            InitializeComponent();
            this.TableItem = new RowCollection(this);
            this.OldSize = this.Size;
        }
        public RowCollection TableItem { get; set; }
        public void ClearItem()
        {
            this.Controls.Clear();
            this.TableItem = new RowCollection(this);
        }
        public void AddControl(Control ctrl)
        {
            this.TableItem.Add(ctrl);
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            var v = new Button() { Text = "aaa" };
            this.TableItem.Add(v);
        }
        private Size OldSize { get; set; }
        private void ArrangeTablePanel_SizeChanged(object sender, EventArgs e)
        {
            if (this.OldSize.Width != this.Size.Width)
            {
                this.TableItem.ReArrange();
            }
            this.OldSize = this.Size;
        }
    }
}
