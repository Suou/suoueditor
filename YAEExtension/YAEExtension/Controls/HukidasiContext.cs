﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouBase.Controls
{
    public class HukidasiContext:ToolStripMenuItem
    {
        public HukidasiContext():base()
        {
            this.Text = "枠の詳細設定";
        }
        public HukidasiContext(SectionScenePart part):this()
        {
            this.Page = part;
            //var b = part.Waku.Type != HukidasiType.Nothing;
            this.DropDownItems.Add(this.CreateA());
            //this.DropDownItems.Add(this.CreateB());
            this.DropDownItems.Add(this.CreateResetPadding());
            this.DropDownItems.Add(this.CreateWakuChoice());
            this.DropDownItems.Add(this.CreateUpCenterBottomZureControl());
            this.DropDownItems.Add(this.CreateCenterZureControl());
            var menuItem = this.CreateTrimSpaceMenuItem();
            this.DropDownItems.Add(menuItem);

            this.DropDownOpening += (a, b) =>
            {
                menuItem.Checked = part.Waku.IsTrimSpace;
            };


        }
        private BSystem.Controls.ToolStripNumericUpDown UpCenterBottomUpDown { get; set; }
        private BSystem.Controls.ToolStripNumericUpDown CenterUpDown { get; set; }

        public HukidasiSaveData Waku
        {
            get { return (HukidasiSaveData)this.Page.Waku; }
        }
        public ToolStripMenuItem CreateUpCenterBottomZureControl()
        {
            var r = new ToolStripMenuItem() { Text = "上中下のズレ許容度" };
            this.UpCenterBottomUpDown = new BSystem.Controls.ToolStripNumericUpDown();
            this.UpCenterBottomUpDown.AutoSize = false;
            this.UpCenterBottomUpDown.Height = 32;
            r.DropDownItems.Add(this.UpCenterBottomUpDown);
            this.DropDownItems.Add(r);
            this.UpCenterBottomUpDown.ValueChanged += this.UpCenterBottomNumericValue_Changed;
            r.DropDownOpening += (a, b) => this.UpCenterBottomUpDown.Value = this.Waku.Options.UpCenterBottomPreZure;
            return r;
        }
        public ToolStripMenuItem CreateCenterZureControl()
        {
            var r = new ToolStripMenuItem() { Text = "中のズレ許容度" };
            this.CenterUpDown = new BSystem.Controls.ToolStripNumericUpDown();
            this.CenterUpDown.AutoSize = false;
            this.CenterUpDown.Height = 32;
            r.DropDownItems.Add(this.CenterUpDown);
            this.DropDownItems.Add(r);
            this.CenterUpDown.ValueChanged += this.CenterNumericValue_Changed;
            r.DropDownOpening += (a, b) => this.CenterUpDown.Value = this.Waku.Options.CenterPreZure;
            return r;
        }
        
        private void UpCenterBottomNumericValue_Changed(object sender, EventArgs e)
        {
            var p = (BSystem.Controls.ToolStripNumericUpDown)sender;
            var value = (int)p.Value;
            var waku = (HukidasiSaveData)this.Waku.Clone();
            waku.Options.UpCenterBottomPreZure = value;
            this.Page.Waku = waku;
        }
        private void CenterNumericValue_Changed(object sender, EventArgs e)
        {
            var p = (BSystem.Controls.ToolStripNumericUpDown)sender;
            var value = (int)p.Value;
            var waku = (HukidasiSaveData)this.Waku.Clone();
            waku.Options.CenterPreZure = value;
            this.Page.Waku = waku;
        }
        public SectionScenePart Page { get; set; }
        public HukidasiSaveData Hukidasi
        {
            get { return (HukidasiSaveData)this.Page.Waku; }
        }
        public static ToolStripMenuItem UniInAlgoItem{
            get
            {
                return new ToolStripMenuItem() { Text = "ユニコード", Tag = SpaceLineSet.UniDefaultSet };
            }
        }
        public static ToolStripMenuItem DefaultInAlgoItem
        {
            get { return new ToolStripMenuItem() { Text = "デフォルト", Tag = SpaceLineSet.DefaultSet }; }
        }

        public static ToolStripMenuItem HukidasiDefaultInAlgoItem
        {
            get { return new ToolStripMenuItem() { Text = "フキダシ用空白", Tag = SpaceLineSet.HukidasiDefaultSet }; }
        }
        public static ToolStripMenuItem CompleteCleanInAlgoItem
        {
            get
            {
                return new ToolStripMenuItem() { Text = "非ユニコード空白のみ", Tag = SpaceLineSet.CompleteCleanDefaultSet };
            }
        }

        public static ToolStripMenuItem DestructiveMerge
        {
            get
            {
                return new ToolStripMenuItem() { Text = "破壊的合成", Tag = MergeZureOption.Destructive };
            }
        }
        public static ToolStripMenuItem HighDestructiveMerge
        {
            get
            {
                return new ToolStripMenuItem() { Text = "強い破壊的合成", Tag = MergeZureOption.HighDestructive };
            }
        }
        public static ToolStripMenuItem FreeMerge
        {
            get { return new ToolStripMenuItem() { Text = "制限無し", Tag = MergeZureOption.Free }; }
        }
        public static ToolStripMenuItem LineRestrictionMerge
        {
            get { return new ToolStripMenuItem() { Text = "制限あり", Tag = MergeZureOption.LineRestriction }; }
        }
        public static ToolStripMenuItem PermitLineMerge
        {
            get { return new ToolStripMenuItem() { Text = "行のずれを許す", Tag = MergeZureOption.PermitZureLine }; }
        }


        private ToolStripMenuItem NoWakuItem = new ToolStripMenuItem() { Text = "枠なし" };

        public ToolStripMenuItem CreateWakuChoice()
        {
            var r = new ToolStripMenuItem() { Text = "変更" };
            r.DropDownItems.Add(this.NoWakuItem);
            this.NoWakuItem.Click += (a, b) => this.Page.Waku = HukidasiSaveData.NoneDefault();
            return r;
        }

        public ToolStripMenuItem CreateResetPadding()
        {
            var r = new ToolStripMenuItem() { Text = "パディングのリセット" };
            r.Click += (a, b) => this.Page.Waku = this.Hukidasi.ResetPadding();
            return r;
        }
        public ToolStripMenuItem CreateTrimSpaceMenuItem()
        {
            var r = new ToolStripMenuItem() { Text = "左の空白を透明化する" };

            r.Click += (a, b) =>
            {
                HukidasiBaseSaveData copy = (HukidasiBaseSaveData)this.Hukidasi.Clone();
                copy.Options.IsTrimSpace = !this.Hukidasi.Options.IsTrimSpace;
                this.Page.Waku = copy;
                //this.Hukidasi.Options.IsTrimSpace = !this.Hukidasi.Options.IsTrimSpace;
                //this.Page.ContentChanged
            };
            //this.Page.Waku = this.Hukidasi.ResetPadding();
            return r;
        }

        /*
        public ToolStripMenuItem CreateB()
        {
            var r = new ToolStripMenuItem() { Text = "合成方法" };
            var p = new ToolStripMenuItem[] { DestructiveMerge, FreeMerge, LineRestrictionMerge, PermitLineMerge };
            p.ForEach((v) => r.DropDownItems.Add(v));
            p.ForEach((v) => v.Click += (a, b) => this.Page.Waku = this.Hukidasi.ChengeClone((MergeZureOption)v.Tag));

            r.DropDownOpening += (a, b) =>
            {
                p.ForEach((v) => v.Checked = false);
                p.FirstOrDefault((v) => (MergeZureOption)v.Tag == this.Hukidasi.Options.ZureOption).VoidNullPipe((v)=>v.Checked = true);
            };
            return r;
        }
        */
        public ToolStripMenuItem CreateA()
        {
            var r = new ToolStripMenuItem() { Text = "内部合成アルゴリズム" };
            var p = new ToolStripMenuItem[] { UniInAlgoItem, DefaultInAlgoItem, HukidasiDefaultInAlgoItem, CompleteCleanInAlgoItem };
            p.ForEach((v) => r.DropDownItems.Add(v));
            p.ForEach((v) => v.Click += (a, b) => this.Page.Waku = this.Hukidasi.ChengeClone((SpaceLineSet)v.Tag));
            
            r.DropDownOpening += (a, b) =>
            {
                p.ForEach((v) => v.Checked = false);
                p.FirstOrDefault((v) => (SpaceLineSet)v.Tag == this.Hukidasi.Options.MergeAlgorithm).VoidNullPipe((v) => v.Checked = true);
            };
            return r;
        }
    }
}
