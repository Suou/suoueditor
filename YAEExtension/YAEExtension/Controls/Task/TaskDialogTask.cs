﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Base;
using BSystem.Collections;
namespace SuouBase.Controls.Task
{

    public class TaskDialogTask
    {
        public TaskDialogTask(string startmsg, Action<LapperProgressBar> task, string endmsg)
        {
            this.StartMessage = startmsg;
            this.Task = task;
            this.EndMessage = endmsg;
        }
        public TaskDialogTask(string startmsg, string endmsg)
        {
            this.StartMessage = startmsg;
            this.EndMessage = endmsg;
        }
        public static TaskDialogTask Create(string startmsg, Action task, string endmsg)
        {
            Action<LapperProgressBar> p = (bar) => task();
            return new TaskDialogTask(startmsg, p, endmsg);
        }
        public static TaskDialogTask Create(string startmsg, Func<int> task, string endmsg)
        {
            Action<LapperProgressBar> p = (bar) => 
            {
                bar.ProgressMaximum = task(); 
            };
            return new TaskDialogTask(startmsg, p, endmsg);
        }
        public string StartMessage { get; set; }
        public virtual Action<LapperProgressBar> Task { get; set; }
        public string EndMessage { get; set; }
    }
    public class SuperTaskDialogTask<T> : TaskDialogTask
    {
        public SuperTaskDialogTask(string startmsg, IEnumerable<T> task, string endmsg)
            : base(startmsg, endmsg)
        {
            this.TaskItems = task;
            this.Task = this.CreateSuperTask();
        }
        public static SuperTaskDialogTask<T> Create(string startmsg, IEnumerable<T> task, string endmsg)
        {
            return new SuperTaskDialogTask<T>(startmsg, task, endmsg);
        }
        public static SuperTaskDialogTask<T> CreateIterateTask(string startmsg, IEnumerable<T> task, string endmsg)
        {
            var r = new SuperTaskDialogTask<T>(startmsg, task, endmsg);
            r.Task = r.CreateIterateTask();
            return r;
        }
        public IEnumerable<T> TaskItems { get; private set; }

        public List<T> Result { get; private set; }
        private Action<LapperProgressBar> CreateSuperTask()
        {
            Action<LapperProgressBar> r = (bar) =>
            {
                bar.ProgressValue = 0;
                this.Result = this.TaskItems.ForEach2((v) => bar.ProgressValue++)
                    .ForEach2((v) => bar.Message = this.StartMessage + string.Format("{0}/{1}", bar.ProgressValue, bar.ProgressMaximum)).ToList();
            };
            return r;
        }
        private Action<LapperProgressBar> CreateIterateTask()
        {
            Action<LapperProgressBar> r = (bar) =>
            {
                bar.ProgressValue = 0;
                this.Result = this.TaskItems.ForEach2((v) => bar.Message = string.Format("{0}/{1}",this.StartMessage, v.ToString())).ToList();
                bar.ProgressMaximum = this.Result.Count;
            };
            return r;
        }

    }
    public class LapperProgressBar
    {
        public Form Form { get; set; }
        public ProgressBar Bar { get; set; }
        public ListBox Box { get; set; }
        public int Number { get; set; }
        public int ProgressMaximum
        {
            get
            {
                return Bar.Maximum;
            }
            set
            {
                this.Form.Do(() => Bar.Maximum = value);
            }
        }
        public int ProgressValue
        {
            get
            {
                return Bar.Value;
            }
            set
            {
                this.Form.Do(() => Bar.Value = value);
            }
        }
        public string Message
        {
            get { return (string)this.Box.Items[this.Number]; }
            set { this.Form.Do(() => this.Box.Items[this.Number] = value); }
        }
    }
}
