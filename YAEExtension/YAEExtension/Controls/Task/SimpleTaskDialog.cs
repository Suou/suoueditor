﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Base;
using BSystem.Collections;
namespace SuouBase.Controls.Task
{
    public partial class SimpleTaskDialog : Form
    {
        public SimpleTaskDialog()
        {
            InitializeComponent();
        }
        public SimpleTaskDialog(List<TaskDialogTask> tasks):this()
        {
            this.Tasks = tasks;
            this.Tasks.ForEach((v) => this.listBox1.Items.Add(""));
            this.TaskBar = new LapperProgressBar() { Form = this,Bar = this.progressBar1,Box = this.listBox1};
            
        }
        public SimpleTaskDialog(TaskDialogTask task,int max)
            : this(new List<TaskDialogTask>() { task})
        {
            this.progressBar1.Maximum = max;
        }
        private List<TaskDialogTask> Tasks { get; set; }
        private LapperProgressBar TaskBar { get; set; }
        public bool IsFinished { get; set; }
        private Action Lap(TaskDialogTask task,int i)
        {
            Action t = () =>
            {
                this.Do(()=>this.listBox1.Items[i] = task.StartMessage);
                task.Task(this.TaskBar);
                this.TaskBar.Number++;
                this.Do(()=>this.listBox1.Items[i] = task.EndMessage);
            };
            return t;
        }
        private List<Action> BoxTasks
        {
            get
            {
                int i = 0;
                return this.Tasks.Select((v)=>this.Lap(v,i++)).ToList();
            }
        }
        private void TaskDialog_Shown(object sender, EventArgs e)
        {
            var p = this.BoxTasks;
            var task = SuouBase.Base.AsyncLinQ.StartDo(p.First());
            for (int i = 1; i < this.Tasks.Count; i++)
            {
                task = task.Do(p[i]);
            }
            var lasttask = this.BoxedDo(() => { this.IsFinished = true; this.DialogResult = System.Windows.Forms.DialogResult.OK; });
            task.Do(lasttask).EndDo();
            //this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void TaskDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing&&!this.IsFinished)
            {
                MessageBox.Show("途中終了は出来ません(未実装)");
                e.Cancel = true;
            }
        }
    }
}
