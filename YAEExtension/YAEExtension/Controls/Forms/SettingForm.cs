﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Controls.Forms
{
    public partial class SettingForm : Form
    {
        public Control MainControl { get; set; }
        public SettingForm()
        {
            InitializeComponent();
        }
        public SettingForm(Control c, string title):this()
        {
            this.Text = title;
            this.MainControl = c;
            this.Controls.Add(c);
            c.Location = Point.Empty;
            this.panel1.Width = c.Width;
            this.panel1.Location = new Point(0, c.Height);
            //this.Size = new Size(c.Width, this.panel1.Bottom);
        }
        public static DialogResult Create(Control c, string title)
        {
            var f = new SettingForm(c, title);
            return f.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
        }
    }
}
