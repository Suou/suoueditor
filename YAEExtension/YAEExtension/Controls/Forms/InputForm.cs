﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouBase.Controls
{
    public partial class InputForm : Form
    {
        public string Title
        {
            get { return this.Text; }
            set { this.Text = value; }
        }
        public string Message
        {
            get { return this.textBox1.Text; }
            set { this.textBox1.Text = value; }
        }
        public InputForm()
        {
            InitializeComponent();
        }
        public InputForm(string title, string msg):this()
        {
            this.Title = title;
            this.Message = msg;
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                this.OKButton.PerformClick();
            }
        }
        public static string CreateNameBox(string title,string msg)
        {
            var v = new InputForm(title, msg);
            var rst = v.ShowDialog();
            if (rst == DialogResult.OK)
            {
                return v.textBox1.Text;
            }
            else
            {
                return null;
            }
        }
        public static string CreateNameBox(string title, string msg,Form form,FormStartPosition position)
        {
            var v = new InputForm(title, msg);
            v.StartPosition = position;
            var rst = v.ShowDialog(form);
            if (rst == DialogResult.OK)
            {
                return v.textBox1.Text;
            }
            else
            {
                return null;
            }
        }
    }
}
