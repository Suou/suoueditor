﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;

namespace SuouBase.Controls
{
    public partial class ShortCutRegisterItem : UserControl
    {
        private ShortCutCommand _Item;
        public ShortCutCommand Item
        {
            get { return _Item; }
            set
            {
                _Item = value;
                if (Item != null)
                {
                    for (int i = 0; i < this.comboBox1.Items.Count; i++)
                    {
                        if (((ShortCutKey)this.comboBox1.Items[i]) == Item)
                        {
                            this.comboBox1.SelectedIndex = i;
                            break;
                        }
                    }
                }
            }
        }
        public ShortCutRegisterItem()
        {
            InitializeComponent();
            this.comboBox1.Items.AddRange((object[])ShortCutKey.GetAllCommand());
        }
        public ShortCutRegisterItem(ShortCutCommand Item):this()
        {
            this.Item = Item;
            this.label1.Text = Item.Name;


            
        }
        public void Determine()
        {
            if (this.comboBox1.SelectedItem != null)
            {
                var p = (ShortCutKey)this.comboBox1.SelectedItem;
                this.Item.RawKey = p.RawKey;
            }
        }
    }
}
