﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;

namespace SuouBase.Controls
{
    public partial class ShortCutRegister : UserControl,SettingPage
    {
        public MenuStrip MenuStrip { get; set; }
        public ShortCutRegisterItem[] ControlItems = new ShortCutRegisterItem[]{};
        public ShortCutCommandCollection _Items;
        public ShortCutCommandCollection Items
        {
            get { return _Items; }
            set
            {
                if (value == null) return;
                _Items = value;
            }
        }
        public void Clear()
        {
            this.groupBox1.Controls.Clear();
        }
        private ToolStripMenuItem[] MenuKeyItems { get; set; }

        public ShortCutRegister()
        {
            InitializeComponent();
        }
        
        public ShortCutRegister(ShortCutCommandCollection Items):this()
        {
            this.Set(Items);
        }
        private void Set(ShortCutCommandCollection command)
        {
            this.Items = command;
            Clear();
            this.ControlItems = new ShortCutRegisterItem[this.Items.Items.Length];
            
            int height = 0;
            this.groupBox1.Text = this.Items.Name;
            for (int i = 0; i < Items.Items.Length; i++)
            {
                ControlItems[i] = new ShortCutRegisterItem(Items.Items[i]);
                this.groupBox1.Controls.Add(ControlItems[i]);
                ControlItems[i].Visible = true;
                ControlItems[i].Dock = DockStyle.Top;
                ControlItems[i].BringToFront();
                height += ControlItems[i].Height + 6;
            }
            this.Size = new Size(this.Width, height);
        }
        public ShortCutRegister(MenuStrip menu)
            : this()
        {
            this.Items = Items;
            var items = MenuStripIterator.Iterator(menu.Items);
            var keyItems = items.Select((v) =>
            {
                var p = new ShortCutCommand();
                p.Name = v.Text;
                p.RawKey = v.ShortcutKeys;
                return p;
            }).ToArray();
            this.MenuStrip = menu;
            this.MenuKeyItems = items.ToArray();
            this.Set(new ShortCutCommandCollection() { Items = keyItems ,Name = menu.Name});
        }

        public void Determine()
        {
            foreach (var v in ControlItems)
            {
                v.Determine();
            }
            if (this.MenuKeyItems == null) return;
            ShortCutKeyDictionary.SetShortCutKey(this.MenuStrip, this.MenuKeyItems, this.Items);
        }

        public Control Ctrl
        {
            get { return this; }
        }
        public void OK()
        {
            this.Determine();
        }
        public void Cancel()
        {
        }
        public string PageName
        {
            get { return this.groupBox1.Text; }
            set { this.groupBox1.Text = value; }
        }
    }
    public static class MenuStripIterator
    {
        public static IEnumerable<ToolStripMenuItem> Iterator(ToolStripMenuItem item)
        {
            foreach (var v in item.DropDownItems)
            {
                if (v is ToolStripMenuItem)
                {
                    var subitem = (ToolStripMenuItem)v;
                    yield return subitem;
                    foreach (var w in Iterator(subitem))
                    {
                        yield return w;
                    }
                }
            }
        }
        public static IEnumerable<ToolStripMenuItem> Iterator(ToolStripItemCollection items)
        {
            foreach (var v in items)
            {
                if (v is ToolStripMenuItem)
                {
                    var subitem = (ToolStripMenuItem)v;
                    yield return subitem;
                    foreach (var w in Iterator(subitem))
                    {
                        yield return w;
                    }
                }
            }
        }
    }
}
