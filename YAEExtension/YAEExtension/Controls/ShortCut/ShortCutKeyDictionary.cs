﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
namespace SuouBase.Controls
{
    [Serializable]
    public class ShortCutKeyDictionary:SerializableDictionary<string,Keys>
    {
        public ShortCutKeyDictionary()
        {
        }
        public static ShortCutKeyDictionary Create(MenuStrip menu)
        {
            var p = new ShortCutKeyDictionary();
            MenuStripIterator.Iterator(menu.Items).ForEach((v) => p[v.Name] = v.ShortcutKeys);
            return p;
        }
        public static void SetShortCutKey(MenuStrip menu, ToolStripMenuItem[] items, SuouBase.Interface.ShortCutCommandCollection keyItems)
        {
            var p = new ShortCutKeyDictionary();
            for (int i = 0; i < items.Length; i++)
            {
                p[items[i].Name] = keyItems.Items[i].RawKey;
            }
            p.SetShortCutKey(menu);
        }
        public void SetShortCutKey(MenuStrip menu)
        {
            foreach (var v in MenuStripIterator.Iterator(menu.Items))
            {
                if (this.ContainsKey(v.Name))
                {
                    v.ShortcutKeys = this[v.Name];
                    if (v.ShortcutKeys != System.Windows.Forms.Keys.None)
                    {
                        var p = new SuouBase.Interface.ShortCutCommand(v.Name,v.ShortcutKeys);
                        v.ShortcutKeyDisplayString = p.ToString();
                    }
                    
                }
            }

        }
    }
}
