﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Book;

namespace SuouBase.Controls
{
    public partial class IBookEViewer : UserControl
    {
        public IBookEViewer()
        {
            InitializeComponent();
        }
        public EditorInfo DLLFunction { get; set; }
        public event EventHandler ItemSelected;
        private IBook _Item;
        /*
        public ListViewItem this[int i]
        {
            set { this.listView1.Items[i] = value; }
            get { return this.listView1.Items[i]; }
        }
        */
        public IBookPage TopNoCheckedPage
        {
            get
            {
                var n = this.TopNoCheckedIndex;
                if (n == -1) return null;
                return this.Item.OutputPages.Skip(n).First();
            }
        }
        public ListViewItem TopNoCheckItem
        {
            get
            {
                var n = this.TopNoCheckedIndex;
                if (n == -1) return null;
                return this.listView1.Items[n];
            }
        }
        public int TopNoCheckedIndex
        {
            get
            {
                int i = 0;
                foreach (ListViewItem v in this.listView1.Items)
                {
                    if (v.SubItems[3].Text == "未投下") return i;
                    i++;
                }
                return -1;
            }
        }
        public void CheckTouka(ListViewItem item)
        {
            item.SubItems[3].Text = "投下済み";
        }
        public int SelectedIndex
        {
            get
            {
                var v = this.listView1.SelectedIndices;
                if (v.Count == 0) return -1;
                return v[0];
            }
        }
        public IBookPage SelectedPage
        {
            get
            {
                int i = this.SelectedIndex;
                if (i == -1)
                {
                    return null;
                }
                else
                {
                    return this.Item.OutputPages.Skip(i).First();
                }
            }
        }
        public IBook Item
        {
            get { return this._Item; }
            set
            {
                this._Item = value;
                if (this._Item != null)
                {
                    this.ListUp();
                }
            }
        }
        private void ListUp()
        {
            //int i = 0;
            this.listView1.Items.Clear();
            foreach (var v in this.Item.CreateViewItems())
            {
                this.listView1.Items.Add(v);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.DLLFunction != null && this.SelectedIndex != -1)
            {
                this.DLLFunction.StoryPageViewPlugin.FocusPage(this.SelectedIndex);
            }
            if (this.ItemSelected != null) this.ItemSelected(this, e);
        }
    }
    public static class ViewerExtension
    {
        public static ListViewItem CreateViewItem(this IBookPage ap,int i)
        {
            var size = ap.SText.GetSitarabaByteSize;
            var s = new string[] { i.ToString(), ap.Name, size.ToString(),"未投下",ap.SText.Width.ToString() };
            var f = new ListViewItem(s);
            
            if (size < 4096)
            {
            }
            else
            {
                f.BackColor = Color.Red;
            }
            return f;
        }
        public static IEnumerable<ListViewItem> CreateViewItems(this IBook ib)
        {
            int i = 1;
            if (ib is ObsoleteChapterBook)
            {
                var sib = (ObsoleteChapterBook)ib;
                Color b = Color.LightBlue;
                foreach (var page in sib.Pages)
                {
                    foreach (var subpage in page.Items.OutputPages)
                    {
                        var p = subpage.CreateViewItem(i++);
                        if(p.BackColor != Color.Red)p.BackColor = b;
                        yield return p;
                    }
                    b = b == Color.LightBlue ? Color.LightGreen : Color.LightBlue;
                    
                }
            }
            else
            {
                foreach (var page in ib.OutputPages)
                {
                    yield return page.CreateViewItem(i++);
                }
            }
            
        }
    }
}
