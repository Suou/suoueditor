﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase;
using SuouBase.Drawing;
using WebExtension;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using BSystem.Drawing;
using SuouBase.Book;

namespace SuouBase.Controls
{
    public partial class BookPagePlainPictureBox : PictureBox
    {
        public BookPagePlainPictureBox()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.Changer = new AutoSizeChanger(this);
        }
        public EditPoint TranslatePoint(Point p)
        {
            return this.Page.SText.TranslatePoint(p,2);
        }
        public Point TranslateEditPoint(EditPoint p)
        {
            return this.Page.SText.TranslateEditPoint(p,2);
        }
        private AutoSizeChanger Changer { get; set; }
        public event EventHandler PictureSeted;

        public void UpdateImage()
        {
            this.Loading();
        }
        public bool EnablePaste { get; set; }
        public bool EnableCopy { get; set; }
        public ShrinkType AutoSizeType { get; set; }
        private TextThumbImage _Item;
        public SuouText SText
        {
            get
            {
                if (this.Item == null) return null;
                return this.Item.Page.SText;
            }
            set
            {
                if (value != null)
                {
                    var page = new SectionScene(value);
                    this.Item = new TextThumbImage(page, 2);
                }
            }
        }
        public IBookPage Page
        {
            get
            {
                if (this.Item == null) return null;
                return this.Item.Page;
            }
            set
            {
                if (value != null)
                {
                    this.Item = new TextThumbImage(value, 2);
                }
            }
        }
        public TextThumbImage Item
        {
            get
            {
                return _Item;
            }
            set
            {
                if (value != null)
                {
                    _Item = value;
                    this.aaToolTip1.TextItem = value;
                    this.Loading();
                }
            }
        }
        public PrintFontSetting PrintSet = new PrintFontSetting();
        public Size PictureSize
        {
            get
            {
                if (this.Item == null) return this.Size;
                if (this.AutoSizeType == ShrinkType.TateYoko)
                {
                    return this.SText.Size;
                }
                else if (this.AutoSizeType == ShrinkType.Tate)
                {
                    return new Size(this.Width, this.SText.Size.Height);
                }
                else
                {
                    return this.Size;
                }
            }
        }
        public Size PageSize
        {
            get
            {
                if (this.Item == null)
                {
                    return Size.Empty;
                }
                else
                {
                    return this.Item.TextSize;
                }
            }
        }
        public void Loading()
        {
            
            if (this.Item == null) return;
            this.Item.PictureSize = this.PictureSize;
            //this.Refresh();
            this.Image = this.Item.CreatePlainImage();
            
            if (this.PictureSeted != null) this.PictureSeted(this, null);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Drag();
            }
        }
        private void Drag()
        {
            if (this.Item == null) return;
            //ドラッグ&ドロップ処理を開始する
            DragDropEffects dde =
                this.DoDragDrop(this.Item, DragDropEffects.All);

        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Item != null)
            {
                var v = this.SText.Text;
                Clipboard.SetText(v);
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Paste();
        }
        public void Paste()
        {
            var v = Clipboard.GetText().CheckNewLine().DecodeToSjis();
            this.SText = new SuouText(v);
        }
        public bool CanPaste
        {
            get
            {
                string s = Clipboard.GetText();
                return s.Length > 0;
            }
        }
        public bool CanCopy
        {
            get
            {
                return this.Item != null;
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            /*
            this.copyToolStripMenuItem.Visible = this.EnableCopy;
            this.pasteToolStripMenuItem.Visible = this.EnablePaste;
            this.copyToolStripMenuItem.Enabled = this.CanCopy;
            this.pasteToolStripMenuItem.Enabled = this.CanPaste;
            */
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            /*
            if (this.Item == null)
            {
                this.contextMenuStrip1.Show(Control.MousePosition);
            }
            else
            {
                this.aaToolTip1.ToolTipShow();
            }
            */
        }

        private void BookPagePictureBox_Paint(object sender, PaintEventArgs e)
        {
            /*
            if (this.Page != null)
            {
                e.Graphics.DrawText(this.Page.SText, 2, Point.Empty);
            }
            */
        }

        private void BookPagePlainPictureBox_SizeChanged(object sender, EventArgs e)
        {
            this.Loading();
        }
    }
}
