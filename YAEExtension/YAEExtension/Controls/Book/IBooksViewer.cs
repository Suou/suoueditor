﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using System.IO;
namespace SuouBase.Controls
{
    public partial class IBooksViewer : UserControl
    {
        public IBooksViewer()
        {
            InitializeComponent();
            this.Item = new IBookCollection();
        }
        public event EventHandler SelectedItemChanged;
        private IBookCollection _Item = null;
        public IBookCollection Item
        {
            get { return this._Item; }
            set
            {
                this._Item = value;
                if (this._Item != null)
                {
                    this.ReadingItem();
                }
            }
        }
        public void ReadingItem()
        {
            foreach (var v in this.Item.ItemPathList)
            {
                var name = Path.GetFileName(v); 
                this.listBox1.Items.Add(name);
            }
        }
        public string SelectedItemPath
        {
            get
            {
                var n = this.listBox1.SelectedIndex;
                if (n != -1)
                {
                    var v = this.Item.ItemPathList[n];
                    return v;
                }
                else
                {
                    return null;
                }
            }
        }
        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedItemChanged != null) this.SelectedItemChanged(this, e);
        }

        private void listBox1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var strs = (string[])e.Data.GetData(DataFormats.FileDrop);
                this.Item.ItemPathList.AddRange(strs);
                this.listBox1.Items.AddRange(strs);

            }
        }

        private void listBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Move;
                
            }
        }

    }
}
