﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Windows.Forms;
using System.Drawing;
namespace SuouBase.Controls
{
    public class MouseDragMoveChanger:DragEventer
    {
        public MouseDragMoveChanger(Control ctrl):base(ctrl)
        {
            this.AbsoluteLocation = true;
        }
        public event Action<object, Point> Moved;
        protected override void ContentPanel_MouseMove(object sender, MouseEventArgs e)
        {
            base.ContentPanel_MouseMove(sender, e);
            if (this.IsDragging && e.Button == MouseButtons.Left)
            {
                if (this.DistanceBetweenStartAndEnd > 5)
                {
                    if (this.Moved != null)
                    {
                        this.Moved(this, this.ChangePoint);
                    }
                }
            }
        }
        private Point ChangePoint
        {
            get
            {
                return new Point(this.NowPoint.X - this.DragStartLocation.X, this.NowPoint.Y - this.DragStartLocation.Y);
            }
        }
        public void ResetStartLocation()
        {
            this.IsResetFlag = true;
        }
    }
    public class DragEventer
    {
        public DragEventer(Control ctrl)
        {
            this.Ctrl = ctrl;
            this.Ctrl.MouseMove += this.ContentPanel_MouseMove;
            this.Ctrl.MouseDown += this.ContentPanel_MouseDown;
            this.Ctrl.MouseUp += this.ContentPanel_MouseUp;
        }
        public event EventHandler DragStarted;
        public event EventHandler DragEnded;
        public bool AbsoluteLocation { get; set; }
        protected Control Ctrl { get; set; }
        protected Point NowPoint { get; set; }
        private bool _IsDragging;
        public virtual bool IsDragging
        {
            get { return this._IsDragging; }
            protected set
            {
                this._IsDragging = value;
                if (this._IsDragging)
                {
                    if (this.DragStarted != null) this.DragStarted(this, new EventArgs());
                }
                else
                {
                    if (this.DragEnded != null) this.DragEnded(this, new EventArgs());
                }
            }
        }
        protected Point DragStartLocation { get; set; }
        protected Point DragEndLocation { get; set; }
        protected bool IsResetFlag { get; set; }
        protected double DistanceBetweenStartAndEnd
        {
            get
            {
                return BSystem.MathExtension.Distance(this.DragStartLocation.X, this.DragStartLocation.Y, this.NowPoint.X, this.NowPoint.Y);
            }
        }

        private void ContentPanel_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.IsDragging = true;
                this.DragStartLocation = this.GetNowLocation(e);
                this.DragEndLocation = this.DragStartLocation;
            }
        }
        private Point GetNowLocation(MouseEventArgs e)
        {
            if (this.AbsoluteLocation)
            {
                return Control.MousePosition;
            }
            else
            {
                return e.Location;
            }
        }

        private void ContentPanel_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && this.IsDragging)
            {
                this.IsDragging = false;
                this.DragStartLocation = this.GetNowLocation(e);
            }
        }
        protected virtual void ContentPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.IsResetFlag)
            {
                this.DragStartLocation = this.GetNowLocation(e);
                this.NowPoint = this.DragStartLocation;
                this.IsResetFlag = false;
            }
            if (this.IsDragging && e.Button == MouseButtons.Left)
            {
                this.DragEndLocation = this.GetNowLocation(e);
            }
            this.NowPoint = this.GetNowLocation(e);


        }
    }
}
