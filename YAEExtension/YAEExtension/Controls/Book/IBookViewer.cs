﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouBase.Controls
{
    public partial class IBookViewer : UserControl
    {
        public IBookViewer()
        {
            InitializeComponent();
            
        }
        public event EventHandler ItemSelected;
        private IBook _Item;
        public IBookPage SelectedPage
        {
            get
            {
                int i = this.listBox1.SelectedIndex;
                if (i == -1)
                {
                    return null;
                }
                else
                {
                    return this.Item[i];
                }
            }
        }
        public IBook Item
        {
            get { return this._Item; }
            set
            {
                this._Item = value;
                if (this._Item != null)
                {
                    this.ListUp();
                }
            }
        }
        private void ListUp()
        {
            int i=0;
            this.listBox1.Items.Clear();
            foreach (var v in this.Item)
            {
                this.listBox1.Items.Add(i++ + " " + v.Name);
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ItemSelected != null) this.ItemSelected(this, e);
        }
    }
}
