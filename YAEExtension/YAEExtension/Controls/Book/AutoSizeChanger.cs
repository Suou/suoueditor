﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Windows.Forms;
using System.Drawing;
namespace SuouBase.Controls
{
    public class AutoSizeChanger
    {
        public AutoSizeChanger(Control ctrl)
        {
            this.Ctrl = ctrl;
            ctrl.ParentChanged += this.Ctrl_ParentChanged;
        }
        public Control Ctrl { get; set; }
        private void Ctrl_ParentChanged(object sender,EventArgs e)
        {
            if(this.Ctrl.Parent != null)this.Ctrl.Parent.SizeChanged += this.Parent_SizeChanged;
        }
        private void Parent_SizeChanged(object sender,EventArgs e)
        {
            if(this.Ctrl.Width < this.Ctrl.Parent.Width) this.Ctrl.Width = this.Ctrl.Parent.Width;
            if (this.Ctrl.Height < this.Ctrl.Parent.Height) this.Ctrl.Height = this.Ctrl.Parent.Height;
        }
    }
}
