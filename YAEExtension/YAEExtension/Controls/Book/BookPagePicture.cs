﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase;
using SuouBase.Drawing;
using WebExtension;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using BSystem.Drawing;
using SuouBase.Book;
namespace SuouBase.Controls
{
    public partial class BookPagePicture : UserControl
    {
        public BookPagePicture()
        {
            InitializeComponent();
        }
        public event EventHandler PictureSeted;

        public bool EnablePaste { get; set; }
        public bool EnableCopy { get; set; }
        public ShrinkType AutoSizeType { get; set; }
        private TextThumbImage _Item;
        public SuouText SText
        {
            get
            {
                if (this.Item == null) return null;
                return this.Item.Page.SText;
            }
            set
            {
                if (value != null)
                {
                    var page = new SectionScene(value);
                    this.Item = new TextThumbImage(page, 2);
                }
            }
        }
        public IBookPage Page
        {
            get
            {
                if (this.Item == null) return null;
                return this.Item.Page;
            }
            set
            {
                if (value != null)
                {
                    this.Item = new TextThumbImage(value, 2);
                }
            }
        }
        public TextThumbImage Item
        {
            get
            {
                return _Item;
            }
            set
            {
                if (value != null)
                {
                    _Item = value;
                    this.aaToolTip1.TextItem = value;
                    this.Loading();
                }
            }
        }
        public PrintFontSetting PrintSet = new PrintFontSetting();
        public Size PictureSize
        {
            get
            {
                if (this.Item == null) return this.pictureBox1.Size;
                if (this.AutoSizeType == ShrinkType.TateYoko)
                {
                    return this.SText.Size;
                }
                else if (this.AutoSizeType == ShrinkType.Tate)
                {
                    return new Size(this.pictureBox1.Width, this.SText.Size.Height);
                }
                else
                {
                    return this.pictureBox1.Size;
                }
            }
        }
        public void Loading()
        {
            if (this.Item == null) return;
            this.Item.PictureSize = this.PictureSize;
            var bmp = new Bitmap(this.Item.PictureSize.Width, this.Item.PictureSize.Height);
            var g = Graphics.FromImage(bmp);
            
            BufferedGraphicsContext currentContext;
            BufferedGraphics myBuffer;
            currentContext = BufferedGraphicsManager.Current;
            myBuffer = currentContext.Allocate(g, this.DisplayRectangle);
            //myBuffer.Graphics.DrawText(this.Item.Page.Text, this.Item.YSpace, this.Item.PictureSize, this.Item.TextSize, Color.White);
            //myBuffer.Graphics.TextRender(this.Item.Page.Text, this.Item.YSpace, this.Item.PictureSize, Color.White,TextRenderOption.RightProportion);
            myBuffer.Graphics.TextRender(this.Item.Page.SText, this.Item.PictureSize, TextRenderOption.RightProportion,this.PrintSet);
            
            myBuffer.Render();
            myBuffer.Graphics.Dispose();
            g.Dispose();
            this.pictureBox1.Image = bmp;
            
            this.Size = this.PictureSize;
            if (this.PictureSeted != null) this.PictureSeted(this, null);
        }

        private void BookPagePicture_SizeChanged(object sender, EventArgs e)
        {
            //this.Loading();
        }

        private void pictureBox1_DragLeave(object sender, EventArgs e)
        {
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.Drag();
            }
        }
        private void Drag()
        {
            if (this.Item == null) return;
            //ドラッグ&ドロップ処理を開始する
            DragDropEffects dde =
                this.DoDragDrop(this.Item, DragDropEffects.All);

        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Item != null)
            {
                var v = this.SText.Text;
                Clipboard.SetText(v);
            }
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Paste();
        }
        public void Paste()
        {
            var v = Clipboard.GetText().CheckNewLine().DecodeToSjis();
            this.SText = new SuouText(v);
        }
        public bool CanPaste
        {
            get
            {
                string s = Clipboard.GetText();
                return s.Length > 0;
            }
        }
        public bool CanCopy
        {
            get
            {
                return this.Item != null;
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            this.copyToolStripMenuItem.Visible = this.EnableCopy;
            this.pasteToolStripMenuItem.Visible = this.EnablePaste;
            this.copyToolStripMenuItem.Enabled = this.CanCopy;
            this.pasteToolStripMenuItem.Enabled = this.CanPaste;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (this.Item == null)
            {
                this.contextMenuStrip1.Show(Control.MousePosition);
            }
            else
            {
                this.aaToolTip1.ToolTipShow();
            }
        }
        
    }
    public enum ShrinkType
    {
        None,TateYoko,Tate
    }
}
