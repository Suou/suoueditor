﻿namespace SuouBase.Controls
{
    partial class BookPagePlainPictureBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.aaToolTip1 = new SuouBase.Controls.AAToolTip();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // aaToolTip1
            // 
            this.aaToolTip1.AutoPopDelay = 8000;
            this.aaToolTip1.InitialDelay = 100;
            this.aaToolTip1.IsSensitivePopup = false;
            this.aaToolTip1.OwnerDraw = true;
            this.aaToolTip1.ParentControl = null;
            this.aaToolTip1.ReshowDelay = 100;
            // 
            // BookPagePlainPictureBox
            // 
            this.SizeChanged += new System.EventHandler(this.BookPagePlainPictureBox_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.BookPagePictureBox_Paint);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AAToolTip aaToolTip1;
    }
}
