﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
namespace SuouBase.Controls
{
    public partial class ListControl : UserControl
    {
        public ListControl()
        {
            InitializeComponent();
            this.InitializeSet();
            
        }

        public Color BorderColor
        {
            get { return this.groupBox1.BorderColor; }
            set { this.groupBox1.BorderColor = value; }
        }
        protected void InitializeSet()
        {
            this.Items = new ObservableCollection<ListControlItem>();
            this.Items.CollectionChanged += this.Items_Changed;
            this.CreateList();

        }
        public ObservableCollection<ListControlItem> Items { get; private set; }
        public void CreateList()
        {
            this.groupBox1.Controls.Clear();
            foreach (var v in this.Items)
            {
                this.groupBox1.Controls.Add(v);
            }
            Formize();
        }

        private void Items_Changed(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case System.Collections.Specialized.NotifyCollectionChangedAction.Add:
                    if (e.NewStartingIndex == this.Items.Count - 1)
                    {
                        this.Add(e.NewItems[0] as ListControlItem);
                    }
                    else
                    {
                    }
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Move: break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Remove:
                    foreach (ListControlItem v in e.OldItems)
                    {
                        v.SizeChanged -= this.Item_SizeChanged;
                    }
                    this.CreateList();
                    break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Replace: break;
                case System.Collections.Specialized.NotifyCollectionChangedAction.Reset:
                    this.CreateList();
                    break;
            }
        }

        private void Item_SizeChanged(object sender,EventArgs e)
        {
            this.Formize();
        }

        private Point LastLocation { get; set; }
        private void Add(ListControlItem v)
        {
            this.groupBox1.Controls.Add(v);
            v.Dock = DockStyle.None;
            v.SizeChanged += this.Item_SizeChanged;
            if (this.LastLocation == Point.Empty) this.LastLocation = new Point(5, 12);
            v.Location = this.LastLocation;
            this.LastLocation = new Point(this.LastLocation.X,this.LastLocation.Y + v.Height);

            var width = new int[] { this.Items.Max((w) => w.Size.Width), 100 }.Max() + 10;
            var height = new int[] { this.LastLocation.Y + 3, 50 }.Max();
            this.Size = new Size(width, height);
            this.Visible = true;
        }
        public int GetItemsMaxWidth()
        {
            if (this.Items.Count != 0)
            {
                var width = new int[] { this.Items.Max((v) => v.Size.Width), 100 }.Max() + 10;
                return width;
            }
            else
            {
                return 0;
            }
        }
        public void Formize()
        {
            this.LastLocation = new Point(5, 12);
            if (this.Items.Count != 0)
            {
                
                foreach (var v in this.Items)
                {
                    v.Location = this.LastLocation;
                    this.LastLocation = new Point(this.LastLocation.X, this.LastLocation.Y + v.Height);
                }
                var width = this.GetItemsMaxWidth();
                var height = new int[] { this.LastLocation.Y + 3, 50 }.Max();
                this.Size = new Size(width,height);
                this.Visible = true;
            }
            else
            {
                this.Visible = false;
                this.Size = new Size(0, 0);
            }
        }
        public string GroupBoxName
        {
            get { return this.groupBox1.Text; }
            set { this.groupBox1.Text = value; }
        }
    }
}
