﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase;
using SuouBase.Plugin;
using SuouBase.Interface;
namespace SuouBase.Controls
{
    public class AAToolTip:ToolTip
    {
        //private IContainer components;

        Point StartPoint { get; set; }
        [ReadOnly(true)]
        [Browsable(false)]
        public TextThumbImage TextItem { get; set; }
        public bool IsSensitivePopup { get; set; }
        public Size PopUpSize
        {
            get {
                if (this.TextItem.Mode == ThumbnailMode.Raw)
                {
                    var p = this.TextItem.Decode();
                    var pw = new TextThumbImage(p, 0);
                    pw.Padding = new Padding(5, 5, 5, 5);
                    return pw.PictureSize;
                }
                else
                {
                    return this.TextItem.PictureSize;
                }
            }
        }

        public Timer TimerWatch = null;
        private Control _ParentControl = null;
        public Control ParentControl
        {
            get { return this._ParentControl; }
            set
            {
                if (value != null)
                {
                    this._ParentControl = value;
                    this.ParentControl.MouseMove += this.ParentControl_MouseMove;
                }
            }
        }
        //public Stopwatch ToolTipOpeningTime = new Stopwatch();
        public AAToolTip():base()
        {
            this.InitializeComponent();
        }

        #region Method

        private void InitializeComponent()
        {
            // 
            // AAToolTip
            // 
            this.AutoPopDelay = 8000;
            this.InitialDelay = 100;
            this.OwnerDraw = true;
            this.ReshowDelay = 100;
            this.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.AAToolTip_Draw);
            this.Popup += new System.Windows.Forms.PopupEventHandler(this.AAToolTip_Popup);

        }
        
        public Timer CreateTimer()
        {
            var r = new Timer() { Enabled = true,Interval = this.AutoPopDelay};
            r.Tick += this.timer1_Tick;
            return r;
        }
        public void ToolTipShow(Point p)
        {
            
            this.InitializeTimer();
            this.TimerWatch = this.CreateTimer();
            this.TimerWatch.Start();

            //ToolTipOpeningTime.Start();
            this.Show(" ", this.ParentControl, new Point(p.X, p.Y));
        }
        public void ToolTipShow(Point p,BSystem.RectCornerDirection d)
        {
            var r = BSystem.IntRectangle.GetTopLeftCorner(BSystem.PointConverter.ToBasicPoint(p),BSystem.PointConverter.ToBasicPoint(this.PopUpSize), d);
            this.ToolTipShow(BSystem.PointConverter.ToDesktopPoint(r));
        }
        public void ToolTipShow()
        {
            this.ToolTipShow(BSystem.RectCornerDirection.TopLeftCorner);
        }
        public void ToolTipShow(BSystem.RectCornerDirection d)
        {
            if (this.ParentControl == null) return;
            var p = this.ParentControl.PointToClient(Control.MousePosition);
            var p2 = BSystem.PointConverter.ToBasicPoint(p);
            var sz = new BSystem.BSize(this.PopUpSize.Width, this.PopUpSize.Height);
            var p3 = BSystem.IntRectangle.GetTopLeftCorner(p2, sz, d);
            var p4 = BSystem.PointConverter.ToDesktopPoint(p3);
            this.StartPoint = p;
            this.ToolTipShow(p4);
        }
        private void InitializeTimer()
        {
            this.Tag = null;
            if (this.TimerWatch != null)
            {
                this.TimerWatch.Stop();
                this.TimerWatch.Tick -= this.timer1_Tick;
                this.TimerWatch = null;
            }
            
        }
        public void ToolTipHide()
        {
            if (this.ParentControl == null || this.TimerWatch == null) return;
            //throw new Exception();
            if (this.Active)
            {
                this.Hide(this.ParentControl);
            }
            this.InitializeTimer();
            //ToolTipOpeningTime.Reset();
        }
        #endregion
        #region EventMethod
        private void ParentControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (BSystem.Windows.PointExtension.GetDistance(this.StartPoint, e.Location) > 10 && this.IsSensitivePopup)
            {
                this.ToolTipHide();
            }
        }
        private void AAToolTip_Draw(object sender, DrawToolTipEventArgs e)
        {

            if (this.TextItem != null)
            {
                this.TextItem.DrawText(e.Graphics,Color.Black,Color.White);
                //e.Graphics.TextRender(this.TextItem.Decode().Text, 2, Point.Empty, Color.White);
            }
        }

        private void AAToolTip_Popup(object sender, PopupEventArgs e)
        {
            if (this.TextItem != null)
            {
                e.ToolTipSize = this.PopUpSize;
                //e.ToolTipSize = this.TextItem.Decode().Text.GetSize(SuouBase.SuouEditorConst.AAFontYInterval);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.ToolTipHide();
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            if(this.ParentControl != null)this.ParentControl.MouseMove -= this.ParentControl_MouseMove;
            this.Draw -= new System.Windows.Forms.DrawToolTipEventHandler(this.AAToolTip_Draw);
            this.Popup -= new System.Windows.Forms.PopupEventHandler(this.AAToolTip_Popup);
            this.TextItem = null;
            this.ParentControl = null;
            base.Dispose(disposing);
        }
    }
}
