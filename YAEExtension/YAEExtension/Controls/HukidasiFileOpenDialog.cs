﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Files;
using SuouBase.Text;
using System.Collections.ObjectModel;
using System.IO;
namespace SuouBase.Controls
{
    public class HukidasiFileOpenDialog
    {
        OpenFileDialog Dialog = new OpenFileDialog();

        public HukidasiFileOpenDialog()
        {
            this.Dialog.Filter = "フキダシファイル(*.hkd2)|*.hkd2|すべてのファイル(*.*)|*.*";
        }
        public FileInfo Info
        {
            get
            {
                return new FileInfo(this.Dialog.FileName);
            }
        }
        public IList<HukidasiSaveData> ShowDialog()
        {
            var rst = this.Dialog.ShowDialog();
            if (rst == DialogResult.OK)
            {
                return this.SelectedFileTree;
            }
            else
            {
                return null;
            }
        }
        public IList<HukidasiSaveData> OpenFile(string path)
        {

            var loadItems = HukidasiSaveData.Load(path);
            if (loadItems != null)
            {
                var items = new List<HukidasiSaveData>(loadItems);
                return items;

            }
            else
            {
                return null;
            }
        }
        public IList<HukidasiSaveData> SelectedFileTree
        {
            get
            {
                return this.OpenFile(this.Dialog.FileName);
            }
        }
    }
}
