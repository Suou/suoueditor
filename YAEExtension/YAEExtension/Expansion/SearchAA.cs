﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Threading.Tasks;
using SuouBase;
using SuouBase.Book;

namespace WindowsFormsApplication1
{
    public class SearchAA
    {
        //TreeNodeCollection Data;
        string FileName;

        public SearchAA()
        {
            FileName = "testr.xml";
        }

        public void MethodA(TreeNodeCollection TNC)
        {
            XmlTextWriter xtw = xtw = new XmlTextWriter(FileName, Encoding.GetEncoding("Shift-Jis"));

            xtw.Formatting = Formatting.Indented;

            xtw.WriteStartDocument();//XML宣言
            xtw.WriteStartElement("Data");
            foreach (TreeNode tn in TNC)
            {
                RecursiveNode(tn,xtw);
            }
            xtw.WriteEndElement();
            xtw.WriteEndDocument();
            xtw.Close();
        }

        public void SaveXML(string Path)
        {
            

        }

        public void ReadXML(string xmlFile)
        {
            //XmlDocumentインタンスを作成し、XMLファイルを読み込む
            XmlDocument xmlr = new XmlDocument();
            try
            {
                xmlr.Load(xmlFile);
            }
            catch
            {
                ErrorMessage.Show(ErrorMessage.Warning.NotFoundFile, xmlFile);
                return;
            }

            string s = "Setting/" + "Files/File";
            

            XmlNodeList xl = xmlr.SelectNodes(s);
            
        }
        private void RecursiveRead(XmlNode xn)
        {
            switch (xn.Name)
            {
                case "Folder": break;
                case "File": break;
            }
        }

        private void RecursiveNode(TreeNode tnc, XmlTextWriter xtw)
        {
            if (tnc.Tag is FolderNodeClass)
            {
                var f = tnc.Tag as FolderNodeClass;
                xtw.WriteStartElement("Folder", f.Name);
                foreach (TreeNode tn in tnc.Nodes)
                {
                    RecursiveNode(tn,xtw);
                }
                xtw.WriteEndElement();
            }
            else if (tnc.Tag is FileNodeClass)
            {
                var f = (tnc.Tag as FileNodeClass);
                /*
                if (f.IsFile)
                {
                    EditingFile ef = f.File;
                    this.WriteFile(xtw, ef);
                }
                else
                {
                }
                */
            }
        }
        
        private void WriteFile(XmlTextWriter xtw,EditingFile ef)
        {
            xtw.WriteStartElement("File");
            xtw.WriteAttributeString("FileName", ef.FullFilePath);
            foreach (AstPage ap in ef.Pages)
            {
                xtw.WriteElementString("Text", ap.toString());
            }
            xtw.WriteEndElement();

        }
    }
}
