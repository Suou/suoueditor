﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Windows.Forms;
using SuouBase;

namespace SuouBase.Expansion
{
    public partial class CopyPasteForm : Form
    {
        OpenFileDialog ofd = new OpenFileDialog();
        //public ControlTabForm pointer;
        public CopyPasteForm()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
        }

        public void SetShowFunc(SetPageDelegate f)
        {
            //cutPasteControl1.SetShowFuncs(f);
        }

        public void AddViewerFile(string FileName)
        {
            //cutPasteControl1.AddViewerFile(FileName);
        }

        public void AddViewerFile(TreeNode tn)
        {
            //cutPasteControl1.AddViewerFile(tn);
        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.cutPasteControl1.CutFromViewerPage();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.cutPasteControl1.PasteIntoViewerPage();
        }

        private void CopyPasteForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.cutPasteControl1.AllCloseViewerFile();
            this.Hide();
            e.Cancel = true;
        }

        private void newFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //cutPasteControl1.AddNewFile();

        }

        private void saveWithNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //cutPasteControl1.SaveWithName();
        }

        private void CopyPasteForm_KeyDown(object sender, KeyEventArgs e)
        {

            //pointer.OperationObject.FocusMode(e.KeyCode);

            
        }

        private void newPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //cutPasteControl1.AddNewPage();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            string filename = MyClass.OpenDialogBox(ofd);
            if (filename != null)
            {
                cutPasteControl1.AddViewerFile(filename);
            }
             */
        }

        private void notContainMatchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.cutPasteControl1.NotContainsSearch();
        }

        public void MakeNameDictionary(Dictionary<string,List<ToolStripItem>> Dic)
        {
            SuouBase.AAEditorXml.MakeNameDictionary(Dic, this.menuStrip1.Items);
        }

    }
}
