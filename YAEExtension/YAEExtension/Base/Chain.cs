﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace SuouBase.Base
{
    public class ChainMethod
    {
        public List<Func<object, object>> Funcs { get; set; }
        public ChainMethod()
        {
        }
        public ChainMethod Chain(Func<object,object> func)
        {
            this.Funcs.Add(func);
            return this;
        }
    }
}
