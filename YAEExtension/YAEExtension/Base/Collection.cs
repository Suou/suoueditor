﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase.Base
{
    public interface ISortedDoubleValue
    {
        int Value { get; }
    }

    public class SortedTripleDictionary<VALUE> : SortedDoubleDictionary<int, VALUE> where VALUE : ISortedDoubleValue
    {
        public int MaxKey { get; private set; }
        public int MinKey { get; private set; }

        public void Add(VALUE value)
        {
            this.Add(value.Value, value);
            if (this.Count == 1)
            {
                this.MaxKey = value.Value;
                this.MinKey = value.Value;
            }
            else
            {
                if (value.Value > this.MaxKey) this.MaxKey = value.Value;
                if (value.Value < this.MinKey) this.MinKey = value.Value;
            }
        }
        public void Remove(VALUE value)
        {
            base.Remove(value.Value,value);
            if (!this.ContainsKey(value.Value))
            {
                if (this.Count != 0)
                {
                    for (int i = this.MinKey; i <= this.MaxKey; i++)
                    {
                        if (this.ContainsKey(i))
                        {
                            this.MinKey = i;
                            break;
                        }
                    }
                }
            }
        }
    }
    public class SortedDoubleDictionary<KEY, VALUE> : SortedDictionary<KEY, List<VALUE>>
    {
        public new IEnumerable<VALUE> Values
        {
            get
            {
                foreach (var v in base.Values)
                {
                    foreach (var w in v)
                    {
                        yield return w;
                    }
                }
            }
        }
        public void Add(KEY key, VALUE value)
        {
            if (this.ContainsKey(key))
            {
                base[key].Add(value);
            }
            else
            {
                base[key] = new List<VALUE>() { value };
            }
        }
        public void Remove(KEY key, VALUE value)
        {
            base[key].Remove(value);
            if (base[key].Count == 0) base.Remove(key);
        }
        public KeyValuePair<KEY, VALUE> FirstItem()
        {
            var v = this.First();
            return new KeyValuePair<KEY, VALUE>(v.Key, v.Value.First());
        }
        public VALUE FirstValue()
        {
            var v = this.First();
            return v.Value.First();
        }
    }
    /*
    public class ListDictionary<KEY, VALUE> : Dictionary<KEY, List<VALUE>>
    {
        public void Add(KEY key, VALUE value)
        {
            if (this.ContainsKey(key))
            {
                base[key].Add(value);
            }
            else
            {
                base[key] = new List<VALUE>() { value };
            }
        }
        public void Remove(KEY key, VALUE value)
        {
            base[key].Remove(value);
            if (base[key].Count == 0) base.Remove(key);
        }
        public IEnumerable<VALUE> OneValues
        {
            get
            {
                return this.Values.Select((v) => v.First());
            }
        }
    }
    */
}
