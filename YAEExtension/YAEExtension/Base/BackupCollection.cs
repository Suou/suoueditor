﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
namespace SuouBase.Base
{
    public class BackupFile:IComparable<BackupFile>
    {
        public DateTime Date { get; set; }
        public string FullPath { get; set; }
        public string Extension
        {
            get
            {
                return Path.GetExtension(this.FullPath);
            }
        }
        public string Name
        {
            get
            {
                var p = Path.GetFileNameWithoutExtension(this.FullPath);
                return p.Split('_')[1];
            }
        }

        public BackupFile()
        {
            
        }
        public static BackupFile Parse(string fullPath)
        {
            var p = Path.GetFileName(fullPath);
            var result = p.Split('_');
            if (result.Length != 2) return null;
            var left = result[0];
            DateTime time;
            long fileTime;
            var b = long.TryParse(left, out fileTime);
            if (b)
            {
                time = DateTime.FromFileTime(fileTime);
                return new BackupFile() { Date = time, FullPath = fullPath };
            }
            else
            {
                return null;
            }
        }
        public FileInfo Info
        {
            get
            {
                return new FileInfo(this.FullPath);
            }
        }
        public int CompareTo(BackupFile b)
        {
            return this.Date.CompareTo(b.Date);
        }
        public DateTime YYHHDD
        {
            get
            {
                return new DateTime(this.Date.Year,this.Date.Month,this.Date.Day);
            }
        }
    }
    public class BackupCollection : Dictionary<DateTime,List<BackupFile>>
    {
        public string BackUpFolderPath { get; set; }
        public string Extension { get; set; }
        public DirectoryInfo Info
        {
            get { return new DirectoryInfo(this.BackUpFolderPath); }
        }
        public string FileName
        {
            get
            {
                return Path.GetFileName(this.BackUpFolderPath);
            }
        }

        public BackupCollection(string backUpFolderPath,string extension)
        {
            this.BackUpFolderPath = backUpFolderPath;
            this.Extension = extension;
            var list = GetFolderItems(new DirectoryInfo(backUpFolderPath), extension);
            foreach (var v in list)
            {
                if (!this.ContainsKey(v.YYHHDD)) this[v.YYHHDD] = new List<BackupFile>();
                this[v.YYHHDD].Add(v);
            }
        }

        public static FileInfo GetLastBackupFileInfo(string backUpFolderPath, string extension)
        {
            var p = new BackupCollection(backUpFolderPath, extension);
            var p1 = p.LastBackup;
            if (p1 == null) return null;
            return p1.Info;
        }
        public static List<BackupFile> GetFolderItems(DirectoryInfo info,string extension)
        {
            if (!info.Exists) return new List<BackupFile>();
            var p = info.GetFiles().Select((v)=>BackupFile.Parse(v.FullName))
                .Where((v)=>v!= null).Where((v)=>v.Name == info.Name && v.Extension == extension).ToList();
            p.Sort((a, b) => a.Date.CompareTo(b.Date));
            return p;
        }
        public DateTime? LastBackupDay
        {
            get
            {
                var p = this.ToList();
                if (p.Count == 0) return null;
                p.Sort((a, b) => a.Key.CompareTo(b.Key));
                return p.Last().Key;
                
            }
        }
        public IEnumerable<DateTime> OldDays
        {
            get
            {
                var p = this.Keys.ToArray();
                Array.Sort(p);
                for (int i = 0; i < p.Length - 1; i++)
                {
                    yield return p[i];
                }
            }
        }

        public BackupFile LastBackup
        {
            get
            {
                if (!this.LastBackupDay.HasValue) return null;
                return this[this.LastBackupDay.Value].Last();
            }
        }

        public void BackUpRemove(DateTime time, int num)
        {
            var items = this[time];
            while (items.Count > num)
            {
                items[0].Info.Delete();
                items.RemoveAt(0);
            }

        }
        public void Remove(int lastDayNum, int otherNum)
        {
            foreach (var v in this.OldDays)
            {
                this.BackUpRemove(v, otherNum);
            }
            if(this.LastBackupDay.HasValue) this.BackUpRemove(this.LastBackupDay.Value, lastDayNum);
        }
    }
}
