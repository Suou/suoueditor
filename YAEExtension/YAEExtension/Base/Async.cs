﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Windows.Forms;
namespace SuouBase.Base
{
    public static class ControlAsync{
        public static void Do(this Control item,Action t)
        {
            item.EndInvoke(item.BeginInvoke(t, null));
        }
        public static void Do(this Form item, Action t)
        {
            item.EndInvoke(item.BeginInvoke(t, null));
        }
        public static Action BoxedDo(this Form item, Action t)
        {
            return ()=>item.EndInvoke(item.BeginInvoke(t, null));
        }
    }
    public class AsyncLinQ
    {
        public Action ActionItem { get; set; }
        public AsyncLinQ NextMethod { get; set; }
        private bool LastFlag { get; set; }
        public AsyncLinQ Do(Action func)
        {
            this.ActionItem = func;
            this.NextMethod = new AsyncLinQ();

            return this.NextMethod;
        }
        private void PrivateDo()
        {
            AsyncCallback callback = new AsyncCallback(this.some_Completed);
            this.ActionItem.BeginInvoke(callback, null);
        }
        public void EndDo()
        {
            this.ActionItem = () => { };
            this.NextMethod = new AsyncLinQ();
            this.LastFlag = true;
            //this.ActionItem.BeginInvoke(null, null);
        }

        public static AsyncLinQ StartDo(Action func)
        {
            var r = new AsyncLinQ();
            r.ActionItem = func;
            r.NextMethod = new AsyncLinQ();

            r.PrivateDo();

            return r.NextMethod;
        }

        public void some_Completed(IAsyncResult async)
        {
            //int i = 0;
            while (this.NextMethod.ActionItem == null && !this.LastFlag) {  };
            if(!this.LastFlag)this.NextMethod.PrivateDo();
        }
    }
}
