﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using SuouBase.Interface;
using SuouBase.Text;

namespace SuouBase
{

    public static class SuouEditorConst
    {
        private static Font _AAFont = new Font("ＭＳ Ｐゴシック", 12);

        public static Font KoteiAAFont
        {
            get
            {
                return _AAFont;
            }
        }
        public static Font AAFont
        {
            get
            {
                return _AAFont;
            }
        }
        public static char AAFontChar
        {
            get
            {
                return (char)0;
            }
        }
        public static char TextColorChar
        {
            get
            {
                return (char)0;
            }
        }
        public static Color TextColor
        {
            get
            {
                return Color.Black;
            }
        }
        public static int AAFontYInterval
        {
            get { return 2; }
        }
        public static int LooseWidth
        {
            get { return 1000; }
        }
        

        

        public static bool IsFolderCreated = false;
        //public static string PluginDirectory ="Lib/Plugin";
        public static string FileTreeDirectoryName = "User/PathTree";
        public static string AutoBookDirectoryName = "User/AutoBook";
        public static string PrivateBookDirectoryName = "User/PrivateBook";
        public static string IndexDirectoryName = "User/Index";
        public static string FileTreeExtension = ".vwf";
        public static string SettingDirectory = "Setting";
        public static string DataDirectory = "Data";
        public static string BackUpDirectory = "BackUp";
        public static string UserDirectory = "User";

        public static string TestExtension = ".tst";
        public static string SectionBookExtension = ".sec";
        public static string NewSectionBookExtension = ".sec2";
        public static string ChapterBookExtension = ".cpt";
        public static string AstExtension = ".ast";
        public static string MltExtension = ".mlt";
        public static string GetBackupFolderPath(string filename)
        {
            var dirname = Path.GetFileNameWithoutExtension(filename);
            var dir = SuouEditorConst.BackUpDirectory + "/" + dirname;
            return dir;
        }
        private static string ExtFilter1
        {
            get
            {
                var g = String.Join(";",BookFileExtensions.Select((v) => "*" + v));
                return "ファイル(" + g + ")";
            }
        }
        private static string ExtFilter2
        {
            get
            {
                var g = String.Join(";", BookFileExtensions.Select((v) => "*" + v));
                return "|" + g + "|";
            }
        }
        public static string ExtFilter
        {
            get
            {
                return ExtFilter1 + ExtFilter2 + "すべてのファイル(*.*)|*.*";
            }
        }
        private static int NewFileNumber = 1;
        public static string NewFileName(AAFileExtension ext)
        {
            return "NewFile" + NewFileNumber++ + GetName(ext);
        }
        public static string GetName(AAFileExtension ext)
        {
            switch (ext)
            {
                case AAFileExtension.Ast:
                    return AstExtension;
                case AAFileExtension.Mlt:
                    return MltExtension;
                case AAFileExtension.Sst:
                    return SectionBookExtension;
                case AAFileExtension.Gst:
                    return ChapterBookExtension;
            }
            return "";
        }

        public static string[] BookFileExtensions = new string[] { SectionBookExtension, AstExtension, MltExtension,ChapterBookExtension };
        public static bool IsBookFile(this FileInfo f)
        {
            var s = f.Extension;
            return BookFileExtensions.Contains(s);
        }
        
        public static string GetFileTreeFile(string StoryFileName)
        {
            //CheckFolder();
            string f = SuouEditorConst.FileTreeDirectoryName + "/" + StoryFileName + SuouEditorConst.FileTreeExtension;
            return f;
        }
        
        public static string GetDataFile(string FileName)
        {
            //CheckFolder();
            string f = SuouEditorConst.DataDirectory + "/" + FileName;
            return f;
        }
        public static string GetUserFile(string FileName)
        {
            //CheckFolder();
            string f = SuouEditorConst.UserDirectory + "/" + FileName;
            return f;
        }
        public static DirectoryInfo SettingInfo = new DirectoryInfo("Setting");
        public static string GetSettingFile(string FileName)
        {
            if (!SettingInfo.Exists) SettingInfo.Create();
            string f = SettingDirectory + "/" + FileName;
            return f;
        }
        public static void CheckFolder()
        {
            if (IsFolderCreated) return;
            List<DirectoryInfo> r = new List<DirectoryInfo>();
            r.Add(new DirectoryInfo(FileTreeDirectoryName));
            r.Add(new DirectoryInfo(SettingDirectory));
            r.Add(new DirectoryInfo(DataDirectory));
            r.Add(new DirectoryInfo(UserDirectory));
            r.Add(new DirectoryInfo(IndexDirectoryName));
            r.Add(new DirectoryInfo(PrivateBookDirectoryName));
            r.Add(new DirectoryInfo(AutoBookDirectoryName));
            foreach (DirectoryInfo p in r)
            {
                if (!p.Exists) p.Create();
            }
            IsFolderCreated = true;
        }

    }
    public static class SuouEditorConst2
    {
        public static HukidasiSaveData DefaultHukidasi;
        public static SpaceLineSet DefaultMergeAlgorithm;

        static SuouEditorConst2()
        {
            DefaultHukidasi = HukidasiSaveData.NoneDefault();
            DefaultMergeAlgorithm = SpaceLineSet.DefaultSet;
        }
    }
}
