﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouBase
{
    [AttributeUsage(AttributeTargets.Property)]
    public class ViewInstanceAttribute : Attribute
    {
        public ViewInstanceAttribute() { }
    }



    public class ErrorMessage
    {
        private static List<string> ErrorLog = new List<string>();
        public static void AddLog(string str)
        {
            ErrorLog.Insert(0, str);
            if (LogWindow != null && _pko)
            {
               LogWindow.Text = String.Join(System.Environment.NewLine, ErrorLog);
            }
        }
        public MessageMode Mode = MessageMode.TextLog;
        public static TextBox _LogWindow;
        public static TextBox LogWindow { get { return _LogWindow; } 
            set {
                _LogWindow = value;
                if (_LogWindow != null) _LogWindow.Text = String.Join(System.Environment.NewLine, ErrorLog);
            }
        }

        public enum MessageMode
        {
            MessageBox,Writer,TextLog
        }

        public enum Watching{
            SuouTextBoxPageChange,Draw_SuouTextBox,Text_SuouTextBox
        }
        public enum Misc
        {
            TimeStamp
        }

        public enum Warning
        {
            NotFoundEditFile,NotFoundEditPage,NotFoundWebHTMLFile,
            LoadFileException,SaveFileException,NotFoundFile
            
        }
        private static int Pop = 0;
        private static Dictionary<string, bool> WatchHash = new Dictionary<string, bool>();
        private static Dictionary<string, Stopwatch> WatchHash2 = new Dictionary<string, Stopwatch>();
        private static bool _pko = true;
        public static bool pko 
        {
            get { return _pko; }
            set
            {
                _pko = value;
                if (_pko)
                {
                    if (LogWindow != null && pko)
                    {
                        LogWindow.Text = String.Join(System.Environment.NewLine, ErrorLog);
                    }
                }
            }
        }
        

        public static void Start(string s,bool b = false)
        {
            WatchHash2[s] = Stopwatch.StartNew();
            WatchHash[s] = b;
            if (WatchHash[s])
            {
                Pop++;
            }
        }
        public static void MiddleStop(string s, bool b = false)
        {
            string message = "";
            for (int i = 0; i < Pop; i++) message += " ";
            message += s + " is " + WatchHash2[s].ElapsedMilliseconds;
            AddLog(message);
        }
        public static void Stop(string s,params object[] o)
        {
            WatchHash2[s].Stop();
            if (WatchHash[s])
            {
                Pop--;
            }
            string message = "";
            for (int i = 0; i < Pop; i++) message += " ";
                message += s + " is " + WatchHash2[s].ElapsedMilliseconds;
                foreach (object ob in o)
                {
                    message += ob.ToString();
                }
            AddLog(message);
        }
        public static void AddLog(Misc m)
        {
        }
        public static void AddLog(Warning w, params object[] o)
        {
            string message = ErrorMessage.GetMessage(w, o);
            AddLog(message);
        }

        public static string GetMessage(Warning w, params object[] o)
        {
            string message = "";
            switch (w)
            {
                case Warning.NotFoundEditFile:
                    message = "エラー：ファイルが見つかりません";
                    break;
                case Warning.NotFoundEditPage:
                    message = "エラー：ページが見つかりません";
                    break;
                case Warning.NotFoundWebHTMLFile:
                    message = "エラー：WebBrowser.htmlが見つかりません";
                    break;
                case Warning.LoadFileException:
                    if (o.Count() == 1)
                    {
                        string path = (string)o.First();
                        message = "エラー：" + path + "の読み込みでエラーが発生しました";
                    }
                    else
                    {
                        message = "エラー：ファイルの読み込みでエラーが発生しました";
                    }
                    break;
                case Warning.SaveFileException:
                    if (o.Count() == 1)
                    {
                        string path = (string)o.First();
                        message = "エラー：" + path + "の保存でエラーが発生しました";
                    }
                    else
                    {
                        message = "エラー：ファイルの保存でエラーが発生しました";
                    }
                    break;
                case Warning.NotFoundFile:
                    if (o.Count() == 1)
                    {
                        string path = (string)o.First();
                        message = "エラー：" + path + "が見つかりません";
                    }
                    else
                    {
                        message = "エラー：ファイルが見つかりません";
                    }
                    break;



            }
            return message;
        }

        public static void Show(Warning w,params object[] o)
        {
            string message = ErrorMessage.GetMessage(w, o);
            ErrorMessage.AddLog(message);
            MessageBox.Show(message,"エラー",MessageBoxButtons.OK,MessageBoxIcon.Error);
        }
    }
    public enum AAFileExtension
    {
        Ast,Mlt,Sst,Gst,Tst
    }
    /*
    public class BinarySerialize
    {
        /// <summary>
        /// オブジェクトの内容をファイルから読み込み復元する
        /// </summary>
        /// <param name="path">読み込むファイル名</param>
        /// <returns>復元されたオブジェクト</returns>
        public static object LoadFromBinaryFile(string path)
        {
            var fi = new FileInfo(path);
            if (!fi.Exists) return null;

            FileStream fs = new FileStream(path,
                FileMode.Open,
                FileAccess.Read);
            object obj = null;
            BinaryFormatter f = new BinaryFormatter();
            try
            {
                //読み込んで逆シリアル化する
                obj = f.Deserialize(fs);

            }
            catch(Exception e)
            {
                #if DEBUG
                ErrorMessage.Show(ErrorMessage.Warning.LoadFileException,path);
                MessageBox.Show(e.Message);
                #else
                #endif
            }
            finally
            {
                fs.Close();   
            }
            return obj;
        }
        /// <summary>
        /// オブジェクトの内容をファイルに保存する
        /// </summary>
        /// <param name="obj">保存するオブジェクト</param>
        /// <param name="path">保存先のファイル名</param>
        public static bool SaveToBinaryFile(object obj, string path)
        {
            FileStream fs = new FileStream(path,
                FileMode.Create,
                FileAccess.Write);
            BinaryFormatter bf = new BinaryFormatter();
            bool b = false;
            //シリアル化して書き込む
            try
            {
                bf.Serialize(fs, obj);
                b = true;
            }
            catch
            {
                //ErrorMessage.Show(ErrorMessage.Warning.SaveFileException, path);
                //MessageBox.Show(e.Message);
                throw new MyException("BInaryError");
            }
            finally
            {
                fs.Close();
            }
            return b;
        }
    }
    */
    public class GeneralMethod
    {
        public static Point Shift(Point p, Point OriginalPoint)
        {
            var r = new Point(p.X - OriginalPoint.X, p.Y - OriginalPoint.Y);
            return r;
        }

        public static TabPage CreateTabPage(Control ctrl)
        {
            if (ctrl == null) throw new Exception();
            var p = new TabPage();
            ctrl.Dock = DockStyle.Fill;
            p.Controls.Add(ctrl);
            p.Text = ctrl.Text;
            return p;
        }

        private static string CurrentDirectory;

        public static string GetXmlStoryFile(EditingFile ef)
        {
            string Title = ef.Name;
            return SuouEditorConst.GetFileTreeFile(Title);
        }
 

        private static string GetCurrentDirectory()
        {
            if (CurrentDirectory == null)
            {
                CurrentDirectory = System.IO.Directory.GetCurrentDirectory()+"/";
            }
            return CurrentDirectory;
        }

        public static DialogResult OverWriteKakuninBox(string Title)
        {
            DialogResult result = MessageBox.Show("ファイルを上書きしますか？",
                Title+"を上書きしようとしています",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button1);
            return result;
        }

        public static DialogResult SaveKakuninBox(string Title)
        {
            DialogResult result = MessageBox.Show("ファイルを保存しますか？",
                Title+"を閉じようとしています",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);
            return result;
        }

        public static string OutPutSaveDialogBox(string name, string filter = "ファイル(*.ast;*.mlt;*.sst3)|*.ast;*.mlt;*.sst3|すべてのファイル(*.*)|*.*")
        {

            //SaveFileDialogクラスのインスタンスを作成
            SaveFileDialog sfd = IBookSaveDialog;

            //はじめのファイル名を指定する
            sfd.FileName = name;
            //ダイアログを表示する
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                return sfd.FileName;
            }
            return null;

        }
        public static SaveFileDialog IBookSaveDialog = new SaveFileDialog();
        public static void SetIbookSaveDialog(string initialfolder)
        {
            //SaveFileDialogクラスのインスタンスを作成

            SaveFileDialog sfd = IBookSaveDialog;

            //はじめのファイル名を指定する
            //はじめに表示されるフォルダを指定する
            sfd.InitialDirectory = initialfolder;
            //[ファイルの種類]に表示される選択肢を指定する
            sfd.Filter =
                "ファイル(*.ast;*.mlt)|*.ast;*.mlt|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに
            //「すべてのファイル」が選択されているようにする
            sfd.FilterIndex = 2;
            //タイトルを設定する
            sfd.Title = "保存先のファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            sfd.RestoreDirectory = true;
            //既に存在するファイル名を指定したとき警告する
            //デフォルトでTrueなので指定する必要はない
            sfd.OverwritePrompt = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            sfd.CheckPathExists = true;
        }
        public static string SaveDialogBox(IBook ef)
        {

            var sfd = IBookSaveDialog;
            sfd.FileName = ef.Name;

            //ダイアログを表示する
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                return sfd.FileName;
            }
            return null;

        }
        public static SaveFileDialog SaveDialogBox()
        {

            //SaveFileDialogクラスのインスタンスを作成
            SaveFileDialog sfd = new SaveFileDialog();

            //はじめに表示されるフォルダを指定する
            //sfd.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            sfd.Filter =
                "ファイル(*.ast;*.mlt)|*.ast;*.mlt|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに
            //「すべてのファイル」が選択されているようにする
            sfd.FilterIndex = 2;
            //タイトルを設定する
            sfd.Title = "保存先のファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            sfd.RestoreDirectory = true;
            //既に存在するファイル名を指定したとき警告する
            //デフォルトでTrueなので指定する必要はない
            sfd.OverwritePrompt = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            sfd.CheckPathExists = true;

            return sfd;
        }

        public static OpenFileDialog OpenDialogBox()
        {

            //SaveFileDialogクラスのインスタンスを作成
            OpenFileDialog ofd = new OpenFileDialog();

            //はじめに表示されるフォルダを指定する
            //sfd.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            ofd.Filter =
                "ファイル(*.ast;*.mlt;*.txt)|*.ast;*.mlt;*.txt|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに
            //「すべてのファイル」が選択されているようにする
            ofd.FilterIndex = 1;
            //タイトルを設定する
            ofd.Title = "開くファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckPathExists = true;

            return ofd;
        }

        public static OpenFileDialog CreateOpenDialogBox(string title,string filter)
        {

            //SaveFileDialogクラスのインスタンスを作成
            OpenFileDialog ofd = new OpenFileDialog();

            //はじめに表示されるフォルダを指定する
            //sfd.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            ofd.Filter = filter;
            //[ファイルの種類]ではじめに
            //「すべてのファイル」が選択されているようにする
            ofd.FilterIndex = 1;
            //タイトルを設定する
            ofd.Title = title;
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckPathExists = true;

            return ofd;
        }

        public static OpenFileDialog OpenViewerDialogBox()
        {

            //SaveFileDialogクラスのインスタンスを作成
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = SuouEditorConst.FileTreeDirectoryName;
            //はじめに表示されるフォルダを指定する
            //sfd.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            ofd.Filter =
                "ファイル(*.vwf)|*.vwf|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに
            //「すべてのファイル」が選択されているようにする
            ofd.FilterIndex = 1;
            //タイトルを設定する
            ofd.Title = "テンプレートファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckPathExists = true;

            return ofd;
        }
    }

    public class MenuStripMethod
    {
        public static void CopyStripList(ToolStripItemCollection a, ToolStripItemCollection b)
        {
            foreach (var v in b)
            {
                a.Add(CopyStrip((ToolStripItem)v));
            }
        }
        [Obsolete]
        public static ContextMenuStrip MergeStripList(ContextMenuStrip a, ContextMenuStrip b)
        {
            var r = new ContextMenuStrip();
            CopyStripList(r.Items, a.Items);
            CopyStripList(r.Items, b.Items);
            
            return r;
        }
        public static ToolStripItem CopyStrip(ToolStripItem tsi)
        {
            if (tsi is ToolStripMenuItem)
            {
                var ftsi = (ToolStripMenuItem)tsi;
                var f = new ToolStripMenuItem();
                f.Text = tsi.Text;
                f.Enabled = tsi.Enabled;
                f.Click += ((object sender, EventArgs e) => { tsi.PerformClick(); });
                f.ShortcutKeys = ftsi.ShortcutKeys;
                return f;
            }
            else if (tsi is ToolStripSeparator)
            {
                var f = new ToolStripSeparator();
                return f;
            }
            return null;

        }
    }
}
