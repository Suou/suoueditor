﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase
{
    public class MyException:Exception
    {
        private string _StackTrace;
        public MyException(string message):base(message)
        {
            _StackTrace = System.Environment.StackTrace;
        }
        public MyException(string message,Exception ex)
            : base(message,ex)
        {
            _StackTrace = System.Environment.StackTrace;
        }
        public override string StackTrace
        {
            get
            {
                return _StackTrace;
            }
        }
    }
    public class NotSupportedVertionException : Exception
    {
    }
    
}
