﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
namespace SuouBase.Base
{
    public static class WindowsExtension
    {
        public static IEnumerable<ToolStripMenuItem> GetMenuItems(this ContextMenuStrip cms)
        {
            List<ToolStripMenuItem> r = new List<ToolStripMenuItem>();
            foreach (var v in cms.Items)
            {
                if (v is ToolStripMenuItem) r.Add((ToolStripMenuItem)v);
            }
            return r;
        }
    }
}
