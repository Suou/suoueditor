﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
using BSystem.Text;
namespace SuouBase.Base
{
    public class MessageDic<Key>
    {
        public BSystem.Collections.ListDictionary<Key, string> Dic = new ListDictionary<Key, string>();
        public string this[Key name]
        {
            get
            {
                return string.Join(System.Environment.NewLine, this.Dic[name]);
            }
        }
        public void Add(Key name, string msg)
        {
            this.Dic[name].Add(msg);
        }
        public string[] AllMessage
        {
            get 
            {
                var p = this.Dic.Values.SelectMany((v) => v);
                var p2 = p.Distinct();
                return p2.ToArray(); 
            }
        }
    }
}
