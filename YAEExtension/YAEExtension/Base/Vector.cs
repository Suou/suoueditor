﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;

namespace SuouBase.Vector2D
{

    public class Vector2D
    {
        public int X { get; set; }
        public int Y { get; set; }

        public double Length { get; set; }

        public Point Start { get; set; }
        public Point End { get; set; }

        public Vector2D(Point a, Point b)
        {
            this.Start = a;
            this.End = b;

            X = b.X - a.X;
            Y = b.Y - a.Y;


            this.Length = Math.Sqrt(X * X + Y * Y);
        }

        public Point MinGetPoint(Point p)
        {
            Vector2D b = new Vector2D(this.Start, p);
            Vector2D a = this;

            var d = Vector2D.Naiseki(a, b) / a.Length;

            if (d < 0)
            {
                return this.Start;
            }
            else if (d > a.Length)
            {
                var g = new Vector2D(this.End, p);
                return this.End;
            }
            else
            {
                return this.GetPoint(d);
            }
        }

        private Point GetPoint(double d)
        {
            var propo = d / this.Length;
            var p = new Point((int)(this.Start.X + (this.X * propo)), (int)(this.Start.Y + (this.Y * propo)));
            return p;
        }
        /*
        private int Length(Point a)
        {
            return (int)Math.Sqrt(x * x + y * y);
        }
        */
        public static double Arg(Vector2D a, Vector2D b)
        {
            double ablen = a.Length * b.Length;
            double ab = a.X * b.X + a.Y * b.Y;
            return ab / ablen;
        }
        public static double Naiseki(Vector2D a, Vector2D b)
        {
            double ab = a.X * b.X + a.Y * b.Y;
            return ab;
        }

        public double MinDistance(Point p)
        {
            Vector2D b = new Vector2D(this.Start, p);
            Vector2D a = this;



            var d = Vector2D.Naiseki(a, b) / a.Length;

            if (d < 0)
            {
                return b.Length;
            }
            else if (d > a.Length)
            {
                var g = new Vector2D(this.End, p);
                return g.Length;
            }
            else
            {
                var g = b.Length * b.Length - d * d;
                var gg = Math.Sqrt(g);
                return gg;
            }
        }

        public static int MeasureMinDistance(Rectangle rec, Point p)
        {
            var UL = new Point(rec.X, rec.Y);
            var UR = new Point(rec.X + rec.Width, rec.Y);
            var DL = new Point(rec.X, rec.Y + rec.Height);
            var DR = new Point(rec.X + rec.Width, rec.Y + rec.Height);

            var a = new Vector2D(UL, UR);
            var b = new Vector2D(UL, DL);
            var c = new Vector2D(UR, DR);
            var d = new Vector2D(DL, DR);

            var A = a.MinDistance(p);
            var B = b.MinDistance(p);
            var C = c.MinDistance(p);
            var D = d.MinDistance(p);

            var E = A < B ? A : B;
            var F = C < D ? C : D;

            return E < F ? (int)E : (int)F;
        }
        public static Point GetMinPoint(Rectangle rec, Point p)
        {
            var UL = new Point(rec.X, rec.Y);
            var UR = new Point(rec.X + rec.Width, rec.Y);
            var DL = new Point(rec.X, rec.Y + rec.Height);
            var DR = new Point(rec.X + rec.Width, rec.Y + rec.Height);

            var a = new Vector2D(UL, UR);
            var b = new Vector2D(UL, DL);
            var c = new Vector2D(UR, DR);
            var d = new Vector2D(DL, DR);

            var A = a.MinDistance(p);
            var B = b.MinDistance(p);
            var C = c.MinDistance(p);
            var D = d.MinDistance(p);

            var E = A < B ? A : B;
            var F = C < D ? C : D;

            var G = E < F ? E : F;

            Vector2D f;

            if (A == G) { f = a; } else if (B == G) { f = b; } else if (C == G) { f = c; } else { f = d; }

            return f.MinGetPoint(p);
        }

    }
}
