﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SuouBase
{
    public static class StringExtensions2
    {
        public static IEnumerable<string> Qgram(this string s, int len)
        {
            for (int i = 0; i < s.Length - len; i++)
            {
                yield return s.Substring(i, len);
            }
        }
    }
}
