﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
namespace SuouBase
{
    public delegate void HukidasiEventHandler(object sender,HukidasiEventArgs e);
    public class HukidasiEventArgs : EventArgs
    {
        public HukidasiSaveData SaveData { get; set; }
        public HukidasiEventArgs(HukidasiSaveData sd):base()
        {
            this.SaveData = sd;
        }

    }

    public class InverseMethodEventArg : EventArgs
    {
        public InverseDataCollection InverseData;
        public InverseMethodEventArg(InverseDataCollection id)
            : base()
        {
            this.InverseData = id.Copy();
        }
    }
    public delegate void InverseMethodEventHandler(object sender, InverseMethodEventArg e);
}
