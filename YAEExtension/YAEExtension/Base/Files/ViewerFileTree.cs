﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using System.Runtime.InteropServices;
using SuouBase.Interface;
using System.Xml;
using BSystem.Collections;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouBase
{
    #region NodeClass
    public class ViewerNodeClass
    {

    }

    public class FolderNodeClass : ViewerNodeClass
    {
        public string Name;
        public string FullName;

        public FolderNodeClass(string Name, string Path)
        {
            this.Name = Name;
            this.FullName = Path;
        }

        public FolderNodeClass()
        {
            this.Name = "";
            this.FullName = "";
        }
    }

    public class FileNodeClass : ViewerNodeClass
    {
        //public EditingFile File;

        public string Name;
        public string FullName;
        /*
        public FileNodeClass(EditingFile ef)
        {
            this.Name = ef.Name;
            this.FullName = ef.FullFilePath;
            this.File = ef;
        }
        */

        public FileNodeClass(string Name, string Path)
        {
            this.Name = Name;
            this.FullName = Path;

        }
        /*
        public bool IsFile
        {
            get
            {
                if (File == null)
                {
                    FileLoad();
                }

                return File.IsExist;
            }
        }
        */
        /*
        private void FileLoad()
        {
            this.File = AddFile(this.FullName);
        }
        */

        private EditingFile AddFile(string filename)
        {

            var page = new EditingFile(filename, 0);
            if (page != null)
            {
                return page;
            }
            else
            {
                return null;
            }

        }
    }
    #endregion

    public class TextInfo
    {
        public FileInfo Info { get; private set; }
        public string Text { get; private set; }

        public TextInfo(FileInfo info)
        {
            this.Info = info; 
            StreamReader sr = new StreamReader(info.FullName, Encoding.GetEncoding("Shift_JIS"));
            
            this.Text = sr.ReadToEnd();
            sr.Close();
        }
    }
    /*
    public static class FolderSuperVisor
    {
        
        
    }
    */

    public static class AAFileTreeClass
    {
        /*
        public static KeyValuePair<TreeNodeCollection,string>[] ReadFileTreeSetting(string xmlFile)
        {
            //XmlDocumentインタンスを作成し、XMLファイルを読み込む
            XmlDocument xmlr = new XmlDocument();
            xmlr.Load(xmlFile);

            List<KeyValuePair<TreeNodeCollection, string>> tnclist = new List<KeyValuePair<TreeNodeCollection, string>>();

            string s = "FileTreeSetting";
            XmlNode xn = xmlr.SelectSingleNode(s);
            XmlNodeList xnl = xn.SelectNodes("File");

            foreach (XmlNode xn1 in xnl)
            {
                var tn = new TreeNode();
                string Text = ((XmlElement)xn1).GetAttribute("Name");
                ReadXMLRecursive(xn1.ChildNodes, tn);
                tnclist.Add(new KeyValuePair<TreeNodeCollection,string>(tn.Nodes,Text));
            }

            return tnclist.ToArray();
        }
        */
        public static FolderDataCollection ReadFileTreeSetting(string xmlFile)
        {
            //XmlDocumentインタンスを作成し、XMLファイルを読み込む
            try
            {
                XmlDocument xmlr = new XmlDocument();
                xmlr.Load(xmlFile);

                List<KeyValuePair<TreeNodeCollection, string>> tnclist = new List<KeyValuePair<TreeNodeCollection, string>>();

                string s = "FileTreeSetting";
                XmlNode xn = xmlr.SelectSingleNode(s);
                XmlNodeList xnl = xn.SelectNodes("File");

                var localdatacollection = new SuouBase.Interface.FolderDataCollection();

                foreach (XmlNode xn1 in xnl)
                {
                    var tn = new TreeNode();
                    string Text = ((XmlElement)xn1).GetAttribute("Name");
                    ReadXMLRecursive(xn1.ChildNodes, tn);
                    var v = new SuouBase.Interface.FolderData(Text, tn.Nodes);
                    localdatacollection.Add(v);
                }

                return localdatacollection;
            }
            catch
            {
                ErrorMessage.Show(ErrorMessage.Warning.LoadFileException, xmlFile);
                throw new Exception();
                //TemplateDataCollection v = new TemplateDataCollection();
                //return v;
            }
        }
        /*
        private static void ReadFileTreeSetting2(XmlNode xml,TreeNode tnc1)
        {
            foreach (XmlNode xmlr in xml.SelectNodes("File"))
            {
                ReadXMLRecursive(xmlr.ChildNodes, tnc1);
            }
        }
        */
        private static void ReadXMLRecursive(XmlNodeList xl, TreeNode tn)
        {
            foreach (XmlElement xn in xl)
            {
                switch (xn.Name)
                {
                    case "Folder":
                        tn.Nodes.Add(AAFileTreeClass.MakeFolderNode(xn.GetAttribute("Path"), xn.GetAttribute("Name")));
                        ReadXMLRecursive(xn.ChildNodes, tn.LastNode);
                        break;
                    case "File":
                        var xns = xn.SelectSingleNode("Path");
                        if (xns != null)
                        {
                            string filepath = xns.InnerText;
                            tn.Nodes.Add(AAFileTreeClass.MakeFileNode(filepath, xn.GetAttribute("Name")));
                        }
                        else
                        {
                            EditingFile ef = new EditingFile();
                            var FileName = xn.Attributes["FileName"].InnerText;
                            //ef.FileTitle = FileName;
                            ef.FullFilePath = FileName;

                            var xnt = xn.SelectNodes("Text");
                            foreach (XmlNode xn2 in xnt)
                            {
                                ef.Pages.Add(new AstPage());
                            }
                        }
                        break;
                }
            }
        }

        public static void ReadCacheEF(TreeNode tn,XmlNode xn)
        {

        }

        /*
        public static void MakeTreeList(TreeNodeCollection tnc, List<EditingFile> efl)
        {
            foreach (TreeNode tn in tnc)
            {
                if (tn.Tag is SuouBase.FileNodeClass)
                {
                    var pic = tn.Tag as SuouBase.FileNodeClass;
                    
                    if (pic.IsFile)
                    {
                        efl.Add(pic.File);
                    }
                }
                else if (tn.Tag is SuouBase.FolderNodeClass)
                {
                    MakeTreeList(tn.Nodes, efl);
                }
                else
                {
                }
            }
        }
        */


        public static TreeNode MakeFileNode(string FullName, string Name)
        {
            TreeNode tn = new TreeNode();
            tn.Name = FullName;
            tn.Text = Name;
            tn.Tag = new FileNodeClass(Name, FullName);

            return tn;
        }

        public static TreeNode MakeFolderNode(string FullName, string Name)
        {
            TreeNode tn = new TreeNode();
            tn.Name = FullName;
            tn.Text = Name;
            tn.Tag = new FolderNodeClass(Name, FullName);

            return tn;
        }


        public static TreeNode MakeTree(string FullName)
        {
            TreeNode tn = new TreeNode();
            RecursiveMakeTree(tn, FullName);
            return tn;
        }

        private static void RecursiveMakeTree(TreeNode tnc, string startdir)
        {
            foreach (string dir in Directory.GetDirectories(startdir))
            {
                // 親ディレクトリ名 (フォルダ名) を取得する
                var DI = new System.IO.DirectoryInfo(dir);
                tnc.Nodes.Add(MakeFolderNode(DI.FullName, DI.Name));
                RecursiveMakeTree(tnc.LastNode, dir);
            }
            foreach (string file in Directory.GetFiles(startdir))
            {
                var DI = new System.IO.DirectoryInfo(file);
                var ex = DI.Extension;
                switch(ex)
                {
                    case ".txt":
                    case ".mlt":
                    case ".ast":
                    case ".sst2": 
                    tnc.Nodes.Add(MakeFileNode(DI.FullName, DI.Name));
                    break;
                }
            }
        }

        public static TreeNode MakeTreeList(string startdir)
        {
            
            TreeNode tv = new TreeNode();
            
            var DIS = new System.IO.DirectoryInfo(startdir);
            if (DIS.Exists)
            {
                RecursiveMakeTree(tv, startdir);
            }
            return tv;
        }
        

        public static void TreeConc(TreeNodeCollection tv, TreeNode tn)
        {
            foreach (TreeNode tk in tn.Nodes)
            {
                tv.Add(tk);
            }
        }

        public static void SaveFileTree(XmlTextWriter xtw,TreeNodeCollection tnc)
        {
            SaveXMLRecursive(xtw, tnc);
        }
        private static void SaveXMLRecursive(XmlTextWriter xtw, TreeNodeCollection TNC)
        {
            foreach (TreeNode TN in TNC)
            {
                if (TN.Tag is FolderNodeClass)
                {
                    xtw.WriteStartElement("Folder");
                    {
                        xtw.WriteAttributeString("Name", TN.Text);
                        xtw.WriteAttributeString("Path", TN.FullPath);
                        SaveXMLRecursive(xtw, TN.Nodes);
                    }
                    xtw.WriteEndElement();
                }
                else
                {
                    xtw.WriteStartElement("File");
                    {
                        xtw.WriteAttributeString("Name", TN.Text);
                        xtw.WriteElementString("Path", ((FileNodeClass)TN.Tag).FullName);
                    }
                    xtw.WriteEndElement();
                }
            }
        }

    }

    public delegate void Rush(TreeNodeCollection tv, TreeNode tn);

}
