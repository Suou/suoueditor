﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using System.Windows.Forms;
using System.IO;
using SuouBase.Text;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;
using BSystem;
using System.Collections.ObjectModel;
using BSystem.Collections;
using SuouBase.Book;
namespace SuouBase.Files
{
    [Serializable]
    public class SuouFileTreeCollection : ObservableCollection<SuouFileTree>
    {
        public SuouFileTreeCollection():base()
        {
        }
        public void Check()
        {
            
            for (int i = this.Items.Count - 1; i >= 0; i--)
            {
                if (this[i].Info != null && !this[i].Info.Exists) this.RemoveAt(i);
            }
        }
        private int _GroupNum = 4;
        public int GroupNum
        {
            get { return this._GroupNum; }
            set { 
                this._GroupNum = value;

                foreach (var v in this.Items)
                {
                    v.GroupNum = value;
                }
            }
        }
    }
    [Serializable]
    public class SuouFileTree:IXmlSerializable
    {
        public string Name { get; set; }
        public string FullPath { get; set; }
        public Dictionary<string, FileNode> NodeDic { get; private set; }
        private FileNode[] _Nodes = null;
        public FileNode[] Nodes
        {
            get
            {
                if (this._Nodes == null) this.Load();
                return this._Nodes;
            }
        }
        public bool IsCustom
        {
            get
            {
                return this.Info == null;
            }
        }
        public DirectoryInfo Info { get; private set; }

        private SuouFileTree()
        {
            NodeDic = new Dictionary<string, FileNode>();
            this.Name = "";
            this.FullPath = "";

        }

        public SuouFileTree(DirectoryInfo di):this()
        {
            this.SetInfo(di);
            //this.Load(di);
            
        }
        public SuouFileTree(string[] paths, string name)
            : this()
        {
            this._Nodes = paths.Select((v) => new FileNode(v)).ToArray();
            this.Load();
            this.Name = name;
            //this.Load(di);

        }
        public static SuouFileTree Create(string[] paths,string name)
        {
            return new SuouFileTree(paths, name);
        }
        private void SetInfo(DirectoryInfo di)
        {
            this.Info = di;
            this.FullPath = di.FullName;
            this.Name = this.Info.Name;
        }
        private void Load()
        {
            if (this.Info != null)
            {
                if (!this.Info.Exists)
                {
                    this._Nodes = new FileNode[0];
                    return;
                }
                var r = new List<FileNode>();
                foreach (var v in this.Info.GetFiles())
                {
                    if (SuouEditorConst.BookFileExtensions.Contains(v.Extension))
                    {
                        var w = new FileNode(1, v);

                        r.Add(w);
                    }
                }
                foreach (var v in this.Info.GetDirectories())
                {
                    var w = new FileNode(1, v, this.NodeDic);
                    r.Add(w);
                }
                foreach (var w in r) this.NodeDic[w.HashCode] = w;
                this._Nodes = r.ToArray();
            }
            else if (this._Nodes != null)
            {
                this.NodeDic = new Dictionary<string, FileNode>();
                foreach (var v in this.Nodes)
                {
                    this.NodeDic[v.HashCode] = v;
                }
            }
        }
        private int _GroupNum = 4;
        public int GroupNum
        {
            get { return this._GroupNum; }
            set { this._GroupNum = value; }
        }
        public FileNode[][] GroupNodes
        {
            
            get
            {
                var r = Nodes;
                List<FileNode[]> rs = new List<FileNode[]>();
                while (r.Count() > 0)
                {
                    rs.Add(r.Take(GroupNum).ToArray());
                    r = r.Skip(GroupNum).ToArray();
                }
                return rs.ToArray();
            }
        }

        public IEnumerable<FileNode> FileNodeIterator()
        {
            return this.AllNodeIterator().Where((v) => v.IsFile);
        }
        public IEnumerable<FileNode> FolderNodeIterator()
        {
            return this.AllNodeIterator().Where((v) => !v.IsFile);
        }
        public IEnumerable<FileNode> AllNodeIterator()
        {
            foreach (var v in this.Nodes)
            {
                foreach (var w in v.AllFileNodeIterator())
                {
                    yield return w;
                }
            }
        }

        public IEnumerable<IBook> FileIterator()
        {
            foreach (var v in this.FileNodeIterator())
            {
                yield return IBookHandler.Load(v.FullPath);
            }
        }
        public IEnumerable<FilePointer> TagFileIterator()
        {
            List<AATag> r = new List<AATag>();
            foreach (var v in this.Nodes)
            {
                foreach (var w in v.TagFileIterator(r.ToList(),0))
                {
                    yield return w;
                }
            }
        }
        #region IXMLSerialize
        public XElement CreateXElement()
        {
            var v = new XElement("SuouFileTree");
            v.Add(new XAttribute("IsCustom", this.IsCustom));
            if (this.IsCustom)
            {
            }
            else
            {
                v.Add(new XAttribute("Path", this.FullPath));
            }
            return v;
        }
        public void Parse(XElement parser)
        {
            bool b = parser.BoolAttributeParse("IsCustom",false);
            if (b)
            {
            }
            else
            {
                var info = new DirectoryInfo(parser.Attribute("Path").Value);
                this.SetInfo(info);
            }
        }
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            this.CreateXElement().WriteTo(writer);
        }
        public void ReadXml(System.Xml.XmlReader reader)
        {

            var v = XElement.Parse(reader.ReadInnerXml(),LoadOptions.None);
            this.Parse(v);
            //this.Lines = p.Lines;
        }
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }
        #endregion
    }
    public class FileNode
    {
        

        public bool IsFile { get; set; }
        public FileNode[] Nodes { get; set; }
        public string Name { get; set; }
        public string FullPath { get; set; }
        public string HashCode
        {
            get;
            private set;
        }
        public int Number { get; set; }
        public bool IsRowTail
        {
            get
            {
                return Number % 5 == 0;
            }
        }
        public FileNode(FileInfo f)
        {
            this.IsFile = true;
            this.Nodes = null;
            this.Name = f.Name;
            this.FullPath = f.FullName;
            this.HashCode = "s" + this.GetHashCode().ToString();
        }
        public FileNode(DirectoryInfo f)
        {
            this.IsFile = false;
            this.Nodes = CreateFileNodes(0,f,new Dictionary<string,FileNode>());
            this.Name = f.Name;
            this.FullPath = f.FullName;
            this.HashCode = "s" + this.GetHashCode().ToString();
        }
        public FileNode(string path)
        {
            var p = new FileInfo(path);
            var q = new DirectoryInfo(path);
            if (p.Exists)
            {
                this.IsFile = true;
                this.Nodes = null;
                this.Name = p.Name;
                this.FullPath = p.FullName;
                this.HashCode = "s" + this.GetHashCode().ToString();
            }
            else if (q.Exists)
            {
                this.IsFile = false;
                this.Nodes = CreateFileNodes(0, q, new Dictionary<string, FileNode>());
                this.Name = q.Name;
                this.FullPath = q.FullName;
                this.HashCode = "s" + this.GetHashCode().ToString();
            }
            else
            {
                throw new Exception();
            }
        }
        public FileNode(TreeNode tn,int num,Dictionary<string,FileNode> dic)
        {
            this.Name = tn.Text;
            this.FullPath = tn.Name;
            this.Number = num;
            this.Nodes = CreateFileNodes(this.Number, tn, dic);
            this.IsFile = this.Nodes == null;
            /*
            if (tn.Nodes.Count > 0)
            {
                var tnclist = new List<FileNode>();
                var t = 0;
                foreach (TreeNode tnc in tn.Nodes)
                {
                    tnclist.Add(new FileNode(tnc,t++,dic));
                }
                this.Nodes = tnclist.ToArray();
                IsFile = false;
            }
            else
            {
                IsFile = true;
            }
            */
            this.HashCode = "s" + this.GetHashCode().ToString();
            dic[this.HashCode] = this;
        }
        public FileNode(int n, DirectoryInfo di, Dictionary<string, FileNode> dic)
        {
            this.IsFile = false;
            this.Number = n;
            this.Name = di.Name;
            this.FullPath = di.FullName;
            this.Nodes = CreateFileNodes(this.Number, di,dic);

            this.HashCode = "s" + this.GetHashCode().ToString();
            
            
        }
        public FileNode(int n, FileInfo fi)
        {
            this.IsFile = true;
            this.Number = n;
            this.Name = fi.Name;
            this.FullPath = fi.FullName;


            this.HashCode = "s" + this.GetHashCode().ToString();
        }
        public static object BItem = new object();
        public static FileNode[] CreateFileNodes(int number,DirectoryInfo di, Dictionary<string, FileNode> dic)
        {
            var r = new List<FileNode>();
            foreach (var v in di.GetFiles())
            {
                if (SuouEditorConst.BookFileExtensions.Contains(v.Extension))
                {
                    var w = new FileNode(number + 1, v);
                    r.Add(w);
                }
            }
            foreach (var v in di.GetDirectories())
            {
                var w = new FileNode(number + 1, v,dic);
                r.Add(w);
            }
            lock (BItem)
            {
                foreach (var w in r) dic[w.HashCode] = w;
            }
            return r.ToArray();
        }
        public static FileNode[] CreateFileNodes(int number, TreeNode tn, Dictionary<string, FileNode> dic)
        {
            if (tn.Nodes.Count > 0)
            {
                var tnclist = new List<FileNode>();
                var t = 0;
                foreach (TreeNode tnc in tn.Nodes)
                {
                    tnclist.Add(new FileNode(tnc, t++, dic));
                }
                return tnclist.ToArray();
            }
            else
            {
                return null;
            }
        }
        public void ListUp()
        {
            if (!this.IsFile) this.Nodes.ToList().ForEach((v) => v.ListUp());
        }

        public IEnumerable<FileNode> FileNodeIterator()
        {
            return this.AllFileNodeIterator().Where((v) => v.IsFile);
        }
        public IEnumerable<FileNode> AllFileNodeIterator()
        {
            yield return this;
            if (!this.IsFile)
            {
                foreach (var w in this.Nodes)
                {
                    foreach(var v in w.AllFileNodeIterator()) yield return v;
                }
            }
        }


        public IEnumerable<FilePointer> TagFileIterator(List<AATag> tags,int treelevel)
        {
            var tag = new AATag(){Name = this.Name,TreeLevel = treelevel++};
            tags.Add(tag);
            if (this.IsFile)
            {
                var v = new FilePointer(this.FullPath,tags.ToArray(),treelevel);
                yield return v;
            }
            else
            {
                foreach (var v in this.Nodes)
                {
                    foreach (var w in v.TagFileIterator(tags.ToList(),treelevel))
                    {
                        yield return w;
                    }
                }
            }
        }
        public static FileNode Create(string path)
        {
            var p = new DirectoryInfo(path);
            if(p.Exists){
                return new FileNode(p);
            }else{
                var r = new FileInfo(path);
                if (r.Exists && r.IsAAFile())
                {
                    return new FileNode(new FileInfo(path));
                }
                else
                {
                    return null;
                }
            }

        }
    }

    [Serializable]
    public class PathCollection
    {
        public PathCollection()
        {
            this.Items = new ObservableCollection<string>();
        }
        public ObservableCollection<string> Items { get; set; }
        public string Name { get; set; }


        public void Delete(FileNode f)
        {
            this.Items.Remove(f.FullPath);
            //if (this.FileItemDeleted != null) this.FileItemDeleted(f, new EventArgs());
            //this.ItemChangeCheck();
        }
    }
    [Serializable]
    public class PathCollectionCollection:ObservableCollection<PathCollection>
    {

        public static string SonotaAAFolderPath = "Data/HukuTemp/その他汎用AA";
        public static string SamplePersonFilePath1 = "Data/HukuTemp/やる夫";
        public static string SamplePersonFilePath2 = "Data/HukuTemp/やらない夫";
        public static string SamplePersonFilePath3 = "Data/HukuTemp/蘇芳・パヴリチェンコ.mlt";
        public static string OtherName = "その他";
        public static PathCollectionCollection CreateDefaultPathCollections()
        {
            var p = new PathCollectionCollection();

            var r = new DirectoryInfo(SonotaAAFolderPath);
            if(r.Exists)r.GetDirectories().Select((v) => FileNode.Create(v.FullName)).Where((v) => v != null).ForEach((v) => p.Add(v, OtherName));


            var pr = new string[] { SamplePersonFilePath1, SamplePersonFilePath2, SamplePersonFilePath3 };
            pr.Select((v) => FileNode.Create(v)).Where((v) => v != null).ForEach((v) => p.PersonAdd(v));
            return p;
        }

        public string Name { get; set; }
        public void TrimNoContent()
        {
            for (int i = this.Count - 1; i >= 0; i--)
            {
                if (this[i].Items.Count == 0) this.RemoveAt(i);
            }
        }
        public event EventHandler PathChanged;
        protected override void OnCollectionChanged(System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            foreach (PathCollection v in e.NewItems)
            {
                v.Items.CollectionChanged += (obj, w) => { if (PathChanged != null) this.PathChanged(obj, w); };
            }
            base.OnCollectionChanged(e);

            if (this.PathChanged != null) this.PathChanged(this, e);
        }
        public void Add(string fullpath, string name)
        {
            var m = this.FirstOrDefault((v) => v.Name == name);
            if (m == null)
            {
                var v = new PathCollection() { Name = name };
                v.Items.Add(fullpath);
                this.Add(v);
            }
            else
            {
                m.Items.Add(fullpath);
            }
        }

        public void Add(FileNode f)
        {
            //
            if (this.Count != 0)
            {
                this.First().Items.Add(f.FullPath);
            }
            else
            {
                var v = new PathCollection();
                v.Name = f.Name;
                v.Items.Add(f.FullPath);
                this.Add(v);
            }
        }
        public void Add(FileNode f, string name)
        {
            var m = this.FirstOrDefault((v) => v.Name == name);
            if (m == null)
            {
                var v = new PathCollection() { Name = name };
                v.Items.Add(f.FullPath);
                this.Add(v);
            }
            else
            {
                m.Items.Add(f.FullPath);
            }
        }
        public void PersonAdd(FileNode f)
        {
            this.Add(f, "人物");
        }
    }
    
}
