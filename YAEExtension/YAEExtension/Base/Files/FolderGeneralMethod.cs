﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Drawing;

namespace SuouBase
{
    public class FolderGeneralMethod
    {
        /// <summary>
        /// フォルダのサイズを取得する
        /// </summary>
        /// <param name="dirInfo">サイズを取得するフォルダ</param>
        /// <returns>フォルダのサイズ（バイト）</returns>
        public static long GetDirectorySize(DirectoryInfo dirInfo,bool ContainSubFolder)
        {
            long size = 0;

            //フォルダ内の全ファイルの合計サイズを計算する
            foreach (FileInfo fi in dirInfo.GetFiles())
            {

                size += fi.Length;
            }

            //サブフォルダのサイズを合計していく
            if (ContainSubFolder)
            {
                foreach (DirectoryInfo di in dirInfo.GetDirectories())
                    size += GetDirectorySize(di, ContainSubFolder); 
            }

            //結果を返す
            return size;
        }
        public static long DeleteOldFiles(DirectoryInfo di, long DeleteByte, string Kakutyousi)
        {
            long DeletedByte = 0;
            var f = di.GetFiles();
            var f2 = f.ToList();
            f2.Sort((FileInfo a, FileInfo b) => { return DateTime.Compare(a.LastAccessTime, b.LastAccessTime); });

            foreach (var d in f2)
            {
                if (d.Extension != Kakutyousi) continue;
                long Dbyte = d.Length;
                try
                {
                    d.Delete();
                    DeletedByte += Dbyte;
                    if (DeleteByte - DeletedByte <= 0) break;
                }
                catch
                {
                    throw new MyException("");
                }
            }
            return DeletedByte;
        }
        public static Point GetControlLocation(Control ctrl, Point p)
        {
            if (ctrl.Parent == null)
            {
                return p;
            }
            else
            {
                p = new Point(p.X + ctrl.Location.X, p.Y + ctrl.Location.Y);
                return GetControlLocation(ctrl.Parent, p);
            }
        }
        public static Point GetControlLocation(Control ctrl)
        {
            return GetControlLocation(ctrl, Point.Empty);
        }
    }
}
