﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouBase.Files
{
    public class BookDictionary
    {
        public IBook this[string filepath]
        {
            get { 
                return this.GetBook(filepath); }
            set { this.BookDic[filepath] = value; }
        }
        private Dictionary<string, IBook> BookDic = new Dictionary<string, IBook>();

        public IBook GetBook(string filename)
        {
            if (BookDic.ContainsKey(filename)) return BookDic[filename];
            var book = IBookHandler.Load(filename);
            BookDic[filename] = book;
            return book;
        }
        public void Remove(string filename)
        {
            this.BookDic.Remove(filename);
        }
        public IBookPage RandomPick()
        {
            if (this.BookDic.Count == 0) return null;
            var f = this.BookDic.Values.ToList();
            var rand = new Random();
            var n = rand.Next(f.Count);
            var selectedBook = f[n];
            if(selectedBook.Count == 0) return null;
            var m = rand.Next(selectedBook.Count);
            return selectedBook[m];
        }
    }
}
