﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.IO;
using BSystem.Collections;
using SuouBase.Book;
namespace SuouBase.Files
{
    public class AATagCollection
    {
        public List<FilePointer> FilePointerList = new List<FilePointer>();
        public List<PagePointer> PagePointerList = new List<PagePointer>();
        public List<AATag> TagList = new List<AATag>();
        public List<AATag> FileTagList
        {
            get
            {
                var r = new List<AATag>();
                r = this.FilePointerList.SelectMany((v) => v.Tags).ToList();
                return r;
            }
        }
        public IList<AATag> GetTag(int treelevel)
        {
            return this.FileTagList.Where((v) => v.TreeLevel == treelevel).Distinct().ToList();
        }

        public AATagCollection(DirectoryInfo di)
        {
            this.FileAndPageLoad(di);
            this.TagLoad();
        }
        public void FileAndPageLoad(DirectoryInfo di)
        {
            var r = new SuouBase.Files.SuouFileTree(di);
            //r.Show();
            this.FilePointerList = r.TagFileIterator().ToList();
            foreach (var p in this.FilePointerList)
            {
                foreach (var eg in IBookTagHandler.GetTagPage(p))
                {
                    this.PagePointerList.Add(eg);
                }
            }
        }
        public void TagLoad()
        {
            var p = this.PagePointerList.SelectMany((v) => v.Tags).ToList();            
            var w = p.Distinct().ToList();
            this.TagList = w;
        }
    }
    public class AATag
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public int TreeLevel { get; set; }
        public AATag[] Children { get; set; }
    }

    public class FilePointer
    {
        public string FilePath { get; set; }
        public int TreeLevel { get; set; }
        public AATag[] Tags { get; set; }

        public FilePointer(string filePath, AATag[] tags,int level)
        {
            this.FilePath = filePath;
            this.Tags = tags;
            this.TreeLevel = level;
        }

        public PagePointer ToFilePointer(int number, AATag[] AddTags)
        {
            var v = this.Tags.ToList();
            AddTags.ForEach((w) => v.Add(w));
            var r = new PagePointer(this.FilePath, v.ToArray(), number,TreeLevel);
            return r;
        }
        public bool ContainTag(AATag tag)
        {
            return this.Tags.Contains(tag);
        }
    }
    public class PagePointer:FilePointer
    {
        public int PageNumber { get; set; }
        public PagePointer(string filePath, AATag[] tags, int number,int level)
            : base(filePath, tags,level)
        {
            this.PageNumber = number;
        }
    }
    public enum TagType
    {
        FolderNameTag,FileNameTag,RangePagesTag,PageTag
    }

    public static class IBookTagHandler
    {
        public static PagePointer[] GetTagPage(FilePointer fp)
        {
            var book = IBookHandler.Load(fp.FilePath);
            var r = new List<PagePointer>();
            int t = 0;
            if (book is EditingFile)
            {
                return book.BookPages.Select((v) => IBookTagHandler.GetTagPage((AstPage)v, fp, t++)).Where((v) => v != null).ToArray();
            }
            else
            {
            }
            return r.ToArray();
        }
        public static PagePointer GetTagPage(AstPage page,FilePointer fp,int pageNumber)
        {
            if (page.TextRowCount == 1)
            {
                var tex = page.SText.Text;
                var tag = new AATag() { Name = tex };
                var tags = new AATag[] { tag };
                var pp = fp.ToFilePointer(pageNumber, tags);
                return pp;
            }
            else
            {
                return null;
            }
        }
    }
}
