﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Diagnostics;
namespace SuouBase.Base
{
    public class TimeWatch :IDisposable
    {
        Stopwatch SW;
        public TimeWatch()
        {
            this.SW = new Stopwatch();
            this.SW.Start();
        }
        public void Dispose()
        {
            this.SW.Stop();
            
        }
    }
}
