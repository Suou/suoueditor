﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Drawing;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
namespace SuouBase
{
    public class XMLParse
    {
        public static XElement GetXMLFromFont(Font f)
        {
            XElement xe = new XElement("Font");
            xe.Add(new XAttribute("Name", f.Name));
            xe.Add(new XAttribute("Size", f.SizeInPoints));
            return xe;
        }
        public static XElement GetXMLFromColor(Color c)
        {
            XElement xe = new XElement("Color", c.Name);
            return xe;
        }
        public static Color GetColor(XElement xe)
        {
            Color c = Color.FromName(xe.Value);
            return c;
        }
        public static Font GetFont(XElement xe)
        {
            if(xe == null)
            {
                throw new Exception("Something wrong");
            }
            string name = xe.Attribute("Name").Value;
            float f = float.Parse(xe.Attribute("Size").Value);
            var font = new Font(name, f);
            return font;
        }
        
    }

    public class XMLGeneralSerialize
    {
        public static void WriteXML(string path, Type t, object obj)
        {
            using (TextWriter w = new StreamWriter(path))
            {
                XmlSerializer xs = new XmlSerializer(t);
                xs.Serialize(w, obj);
            }
        }
        public static object ReadXML(string path, Type t)
        {
            using (XmlReader r = new XmlTextReader(new FileStream(path, FileMode.Open)))
            {
                XmlSerializer xs = new XmlSerializer(t);
                return xs.Deserialize(r);
            }
        }

        public static void WriteBinary(string path, object obj)
        {
            FileInfo fi = new FileInfo(path);
            DirectoryInfo di = new DirectoryInfo(fi.DirectoryName);
            if (!di.Exists) di.Create();

            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(fs, obj);
            }
        }
        public static object ReadBinary(string path)
        {
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryFormatter f = new BinaryFormatter();
            //読み込んで逆シリアル化する
            object obj = f.Deserialize(fs);

            fs.Close();

            return obj;

        }

    }
}
