﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouBase
{
    class AsyncBook
    {
        bool IsComplete { get; set; }
        string Path {get;set;}
        IAsyncResult Result { get; set; }
        Action<string, AsyncBook> Func = AsyncBook.Load;
        private IBook _Book = null;
        public IBook Book
        {
            get
            {
                if (!this.Result.IsCompleted)
                {
                    this.Result.AsyncWaitHandle.WaitOne();
                }
                
                return _Book;
            }
        }

        public AsyncBook(string path)
        {
            this.Result = this.Func.BeginInvoke(path,this,null,null);
            
        }

        public static void Load(string path,AsyncBook b)
        {
            b._Book = IBookHandler.Load(path);
        }
    }
}
