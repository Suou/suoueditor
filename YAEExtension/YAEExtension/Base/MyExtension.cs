﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase.Text;
using System.IO;
using System.Drawing;
using WebExtension;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
namespace SuouBase
{
    public static class MessageBoxExtension
    {
        public static DialogResult CreateYesNoMessageBox(string title,string text)
        {
            DialogResult result = MessageBox.Show(text,title,MessageBoxButtons.YesNoCancel,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button2);
            return result;
        }
    }
    public static class ClipBoardExtensions
    {
        public static bool HasClipBoardData
        {
            get
            {
                IDataObject data = Clipboard.GetDataObject();
                if (data.GetDataPresent(DataFormats.Text))
                {
                    return true;
                }
                return false;
            }
        }
        public static SuouText ClipText
        {
            get
            {
                IDataObject data = Clipboard.GetDataObject();

                if (data.GetDataPresent(DataFormats.Text))
                {
                    var s = (string)data.GetData(DataFormats.Text);
                    if (s != null)
                    {
                        var v = new SuouText(s);
                        return v;
                    }
                }
                return null;
            }
        }
        public static bool CanPaste
        {
            get
            {
                string s = Clipboard.GetText();
                return s.Length > 0;
            }
        }
        public static string GetEncodeText(this string text ,EncodeType type)
        {
            var tp = text;
            //var tp = TextProcessor.SimpleCopy(TextOperator.Text, IDC);
            Clipboard.Clear();
            if (tp.Length <= 0) return "";
            if (type == EncodeType.UTF8)
            {
                return tp;
            }
            else if (type == EncodeType.SJIS10)
            {
                return tp.ToSjis10String();
            }
            else if (type == EncodeType.SJIS16)
            {
                return (tp.ToSjis16String());
            }
            return "";
        }
    }
    
    public static class WriterExtensions
    {
        public static void WriteLine(string text,string filename)
        {
            Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
            StreamWriter writer = new StreamWriter(filename, true, sjisEnc);
            writer.WriteLine(text);
            writer.Close();
        }
    }
    public static class TreeNodeExtensions
    {
        /*
        public static void ListUP(this TreeNodeCollection tnf)
        {
            foreach (TreeNode node in tnf)
            {
                
                if (node.Nodes != null) node.Nodes.ListUP();
            }
        }
        */
    }
    public static class DebugExtension
    {
        public static void StopWatch(Action pred,string msg)
        {
            Stopwatch sw = Stopwatch.StartNew();
            pred();
            sw.Stop();
        }
    }
    public class BlockHukidasiException : MyException
    {
        public BlockHukidasi Hukidasi { get; set; }
        public BlockHukidasiException(string message, BlockHukidasi b)
            : base(message)
        {
            Hukidasi = b;
        }
    }
    /*
    public static class ArrayExtensions
    {
        public static bool IsInclude(this int[] arr, int[] parr)
        {
            foreach (int n in parr)
            {
                if (Array.Exists(arr, delegate(int m) { return m == n; })) return true;
            }
            return false;
        }
        

        /// <summary>
        /// arrの各要素からn引く
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static int[] EachMinus(this int[] arr, int n)
        {
            int[] iarr = new int[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                iarr[i] = n - arr[i];
            }
            return iarr;
        }

    }
    public static class LongExtension
    {
        public static IEnumerable<long> PlusCountUp(this long n)
        {
            for (long i = n; ; ++i)
            {
                yield return i;
            }
        }
        public static IEnumerable<long> MinusCountUp(this long n)
        {
            for (long i = n; ; --i)
            {
                yield return i;
            }
        }
    }
    */
    /*
    public static class IntExtension
    {
        public static int Truncate(this int n,int m)
        {
            return (n / m) * m;
        }
        public static int FindIndexWidthCountUp(this int n, Func<int, bool> pred)
        {
            for (int i = n; ; i++)
            {
                if (pred(i)) return i;
            }
        }
        public static int FindIndexWidthCountDown(this int n, Func<int, bool> pred)
        {
            for (int i = n; ; i--)
            {
                if (pred(i)) return i;
            }
        }

        public static IEnumerable<int> PlusMinusCountUp()
        {
            for (int i = 0; ; ++i)
            {
                yield return i;
                if (i != 0)
                {
                    yield return -i;
                }
            }
        }
        public static IEnumerable<int> PlusCountUp()
        {
            for (int i = 0; ; ++i)
            {
                yield return i;
            }
        }
        public static int FirstMatchInclimentNumber(this int n, Func<int,bool> pred)
        {
            for (int i = n; ; ++i)
            {
                if (pred(i)) return i;
            }
        }
        public static int[] IntArray(int a, int b)
        {
            var r = new int[a-b+1];
            for (int i = 0; i < b - a + 1; i++) r[i] = a + i;
            return r;
        }
        public static int[] PlusMinusIntArray(int diff)
        {
            return PlusMinusCountUp().Take(diff * 2 + 1).ToArray();
        }
        public static void ForEach(this int n,Action<int> pred)
        {
            for (int i = 0; i < n; i++)
            {
                pred(i);
            }
        }
        public static IEnumerable<T> Select<T>(this int n, Func<int,T> pred)
        {
            for (int i = 0; i < n; i++)
            {
                yield return pred(i);
            }
        }
        public static T Fold<T>(this int n, Func<T, T> pred,T First)
        {
            T r = First;
            for (int i = 0; i < n; i++)
            {
                r = pred(r);
            }
            return r;
        }
    }
    */
    /*
    public static class LinqExtension
    {
        public static int FindIndex<T>(this IEnumerable<T> items, Func<T, bool> pred)
        {
            int n = 0;
            foreach (var v in items)
            {
                if (pred(v)) return n;
                n++;
            }
            return -1;
        }
        public static void ForEach<T>(this IEnumerable<T> arr, Action<T> pred)
        {
            foreach (var v in arr) { pred(v); }
            
        }
        public static IEnumerable<T> ForEach2<T>(this IEnumerable<T> arr, Action<T> pred)
        {
            foreach (var v in arr) { pred(v); yield return v; }

        }
        public static IEnumerable<T> Skips<T>(this IEnumerable<T> items,int MaxSkipNumber, Action<T> pred)
        {
            var len = items.Count();
            if (len < MaxSkipNumber) MaxSkipNumber = len;
            items.Take(MaxSkipNumber).ToList().ForEach((v) => pred(v));
            return items.Skip(MaxSkipNumber);
        }
        public static void Do<T>(this IEnumerable<T> items)
        {
            items.Count();
            //return items;
        }
        public static int FoldFindIndex<T>(this IEnumerable<T> items, Func<T, int> pred, Func<int, bool> pred2)
        {
            int n = 0;
            int sum = 0;
            foreach (T v in items)
            {
                sum += pred(v);
                if (pred2(sum)) return n;
                n++;
            }
            return -1;
        }
        public static IEnumerable<KeyValuePair<A, B>> Zip<A, B>(this IEnumerable<A> itemA, IEnumerable<B> itemB)
        {
            var aIter = new Stack<A>(itemA);
            var bIter = new Stack<B>(itemB);
            while (aIter.Count != 0 && bIter.Count != 0)
            {
                yield return new KeyValuePair<A, B>(aIter.Pop(), bIter.Pop());
            }
            
        }
        public static IEnumerable<IEnumerable<A>> ReverseSelectMeny<A>(this IEnumerable<A> itemA,Func<A,bool> func)
        {
            var tmp = new List<A>();
            foreach (var v in itemA)
            {
                if (func(v) && tmp.Count > 0)
                {
                    yield return tmp;
                    tmp = new List<A>();
                }
                tmp.Add(v);
            }
            if (tmp.Count > 0) yield return tmp;

        }
    }
    */
    public static class AATextLaw
    {
        public static int LenB_EUC(this string s)
        {
            return s.Sum((c) => c.LenB_EUC());
        }
        public static int GetSitarabaByte(this string s)
        {
            return s.Sum((c) => c.ToSitarabaCode().LenB_EUC());
        }
        
        public static string ToSitarabaCode(this char c)
        {
            var s = "";
            if (c == '"')
            {
                s = "&quot;";
            }
            else if (c == '<')
            {
                s = "&lt;";
            }
            else if (c == '>')
            {
                s = "&gt;";
            }
            else if (c == '&')
            {
                s = "amp;";
            }
            else
            {
                s = HTTPEncode.ToEncodeSJIS10(c);
            }
            return s;
        }
        public static bool IsSpace(this char c)
        {
            return SuouBase.Text.SpaceChars.Spaces.Contains(c);
        }
        public static bool IsUniSpace(this char c)
        {
            return SuouBase.Text.SpaceChars.UniSpaces.Contains(c);
        }
        
        public static bool IsViolate(bool continuity, bool front, char c, bool BackChar)
        {
            if (c == ' ')
            {
                if (continuity || front || BackChar) return true;
            }
            return false;
        }
        public static bool IsViolate(bool first, char? FrontChar, char Char, char? BackChar)
        {
            bool a = Char == ' ';
            bool b = FrontChar == ' ' || BackChar == ' ';
            return a && (b || first);
        }
        public static bool IsViolate(char FrontChar, char Char)
        {
            bool a = Char == ' ';
            bool b = FrontChar == ' ';
            return a && (b);
        }
        public static bool IsViolate(bool first, char Char)
        {
            bool a = Char == ' ';
            return a && (first);
        }
        
    }
    public interface ITwoDpred<T>
    {
        void Clear();
        Func<T, bool> Pred { get; }
    }
    public static class QueueExtension
    {
        public static IEnumerable<T> PopTake<T>(this Queue<T> Items, Func<T, bool> pred)
        {
            List<T> r = new List<T>();
            while (Items.Count != 0)
            {
                var v = Items.Peek();
                if (!pred(v)) break;
                r.Add(Items.Dequeue());
            }
            return r;
        }
        public static IEnumerable<IEnumerable<T>> PopTakeIterator<T>(this Queue<T> Items, ITwoDpred<T> predClass)
        {
            while (Items.Count != 0)
            {
                predClass.Clear();
                var v = Items.PopTake(predClass.Pred);
                yield return v;
            }
        }
    }
    public class TerribleException : Exception
    {
        public TerribleException()
            : base()
        {
        }

        /*
        public override string ToString()
        {
            return s;
        }
         */
    }
    public static class BinaryExtension
    {
        public static int BinaryNearSearch(this List<int> arr, int n, BinaryOption option)
        {
            var a = arr.BinarySearch(n);
            if (a >= 0) return a;
            var b = -(a) - 2;
            bool bRange = false;
            switch (option)
            {
                case BinaryOption.RangeRearNear:
                case BinaryOption.RangeFrontNear:
                case BinaryOption.RangeNear:
                    bRange = true; break;
                default: bRange = false; break;
            }

            if (a == -1)
            {
                if (bRange) { return a; } else { return 0; }
            }
            if (b == arr.Count - 1)
            {
                if (bRange) { return a; } else { return b; }
            }

            var LeftNearValue = arr[b];
            var RightNearValue = arr[b + 1];
            if (option == BinaryOption.FrontNear || option == BinaryOption.RangeFrontNear) return b;
            if (option == BinaryOption.RangeRearNear || option == BinaryOption.RangeRearNear) return b + 1;

            var LeftNearDistance = n - arr[b];
            var RightNearDistance = arr[b + 1] - n;
            if (LeftNearDistance <= RightNearDistance)
            {
                return b;
            }
            else
            {
                return b + 1;
            }
            /*
            if (option == BinaryOption.Near || option == BinaryOption.RangeNear)
            {
                if (a == -1 && option == BinaryOption.RangeNear) return a;

            }

            return b;
            */
        }
    }
    public enum BinaryOption
    {
        Near, FrontNear, RearNear, RangeNear, RangeFrontNear, RangeRearNear
    }
    public static class GetHash
    {
        public static string Create(string s)
        {
            //文字列をbyte型配列に変換する
            byte[] data = System.Text.Encoding.UTF8.GetBytes(s);

            //MD5CryptoServiceProviderオブジェクトを作成
            System.Security.Cryptography.MD5CryptoServiceProvider md5 =
                new System.Security.Cryptography.MD5CryptoServiceProvider();
            //または、次のようにもできる
            //System.Security.Cryptography.MD5 md5 =
            //    System.Security.Cryptography.MD5.Create();

            //ハッシュ値を計算する
            byte[] bs = md5.ComputeHash(data);

            //byte型配列を16進数の文字列に変換
            System.Text.StringBuilder result = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                result.Append(b.ToString("x2"));
            }
            //ここの部分は次のようにもできる
            //string result = BitConverter.ToString(bs).ToLower().Replace("-","");

            //結果を表示
            return result.ToString();
        }
    }

    public static class PathExtension
    {
        public static IEnumerable<string> GetSuffixPath(FileInfo fi,string directryname)
        {
            var p = fi.FullName.Split(Path.DirectorySeparatorChar);
            var n = p.FindIndex((v) => v == directryname);
            if (n == -1) return null;
            
            return p.Skip(n + 1).ToArray();
        }
    }
    
}
