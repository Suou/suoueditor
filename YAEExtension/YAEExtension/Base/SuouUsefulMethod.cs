﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace SuouBase.Base
{
    public class SuouUsefulMethod
    {
        public static Rectangle CreateNoDragRectangle(Point p)
        {
            Rectangle moveRect = new Rectangle(
           p.X - SystemInformation.DragSize.Width / 2,
           p.Y - SystemInformation.DragSize.Height / 2,
           SystemInformation.DragSize.Width,
           SystemInformation.DragSize.Height);
            return moveRect;
        }
    }
}
