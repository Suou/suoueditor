﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using SuouBase.Interface;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using SuouBase.Drawing;
using SuouBase.Base;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Xml;
using SuouBase.Text.Search;
using SuouBase.Book;
namespace SuouBase.Text
{

    public class MergeBookCollection : IBookCollection
    {
        public MergeBookCollection()
        {
            this.SizeDic = new ListDictionary<uint, IBookPage>();
        }
        public ListDictionary<uint, IBookPage> SizeDic { get; set; }
        public void ExamineBook()
        {
            this.Items.SelectMany((v) => v.BookPages).ForEach((v) => this.SizeDic.Add(v.SText.Text.GetLineHash(), v));
        }
        public void Add(IBook item)
        {
            this.Items.Add(item);
            item.BookPages.ForEach((v) => this.SizeDic.Add(v.SText.Text.GetLineHash(), v));
        }
        public IList<IBookPage> GetSubun(IBook item)
        {
            return item.BookPages.Where((v) => !this.SizeDic.ContainsKey(v.SText.Text.GetLineHash())).ToList();
        }

        public IBook CreateBook()
        {
            this.ExamineBook();
            var f = new EditingFile();
            var pages = this.SizeDic.OneValues;
            pages.ForEach((v) => f.Add(v));
            return f;
        }
        public static IBook CreateSabunBook(IBook aBook, IBook bBook)
        {
            var v = new MergeBookCollection();
            v.Add(aBook);
            var p = v.GetSubun(bBook);
            var r = new EditingFile(p);
            return r;

        }

    }

    public class IBookCollection
    {
        public IBookCollection()
        {
            this.ItemPathList = new List<string>();
        }
        private List<IBook> _Items = null;
        public List<IBook> Items
        {
            get
            {
                if (this._Items == null)
                {
                    var p = this.ItemPathList.Select((v) => IBookHandler.Load(v)).Where((v)=>v!= null).ToList();
                    this._Items = p;
                }
                return this._Items;
            }
        }
        public static IBookCollection Load(string folderpath)
        {
            var r = new IBookCollection();
            r.Name = folderpath;
            var v = new DirectoryInfo(folderpath);
            var files = IBookFileIterator.AAFileNameIterator(v);
            foreach (var file in files)
            {
                r.ItemPathList.Add(file);
            }
            return r;
        }
        public string Name { get; set; }
        public List<string> ItemPathList { get; private set; }
        public IBook Create()
        {
            var v = new EditingFile();
            this.Items.ForEach((w) => w.BookPages.ToList().ForEach(q => v.Add(q.IBookPageCopy())));
            return v;
        }
    }
    public class BookPageLapper
    {
        private IBookPage[] _Pages = null;
        public IBookPage[] Pages
        {
            get { return this._Pages.Select((v) => v.IBookPageCopy()).ToArray(); }
            set { this._Pages = value; }
        }
        public BookPageLapper(IBookPage p)
        {
            if (p == null) throw new MyException("");
            this.Pages = new IBookPage[] { p };
        }
        public BookPageLapper(IEnumerable<IBookPage> p)
        {
            if (p == null) throw new MyException("");
            this.Pages = p.ToArray();
        }
        public DataObject ToClipBoardData()
        {
            var f = new DataObject();
            f.SetData(this.Text);
            f.SetData(this);
            return f;
        }
        public string Text
        {
            get
            {
                var f = this.Pages.Select((v) => v.SText.Text).Aggregate((a, b) => a + b);
                return f;
            }
        }
    }
    public interface ISuouWaku
    {
        SuouText CreateWaku(SuouText Text);
    }
}
