﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using SuouBase.Book;
namespace SuouBase.Text
{
    class ObsoleteSectionBookParser
    {
        public static ObsoleteSectionBook Parse(XElement parser)
        {
            var version = parser.AttributeIntValue("Version");
            switch (version)
            {
                case 0: return Parse0(parser);
                case 1: return Parse1(parser);
                case 2: return Parse2(parser);
                default: return Parse0(parser);
            }
        }
        private static ObsoleteSectionBook Parse0(XElement parser)
        {
            if (parser == null) throw new MyException("");
            var nameAtt = parser.Attribute("Name");
            var name = nameAtt == null ? "NoName" : nameAtt.Value;
            var pages = new SuperObservableCollection<SectionScene>(SectionScene.ListParse0(parser.Element("Pages")));
            var p = new ObsoleteSectionBook() { Name = name, Pages = pages };
            p.IsEdit = false;
            return p;
        }
        private static ObsoleteSectionBook Parse1(XElement parser)
        {
            if (parser == null) throw new MyException("");
            var nameAtt = parser.Attribute("Name");
            var name = nameAtt == null ? "NoName" : nameAtt.Value;
            var pages = new SuperObservableCollection<SectionScene>(SectionScene.ListParse1(parser.Element("Pages")));
            var mk = parser.Element(SuouBookPageModel.ClassName);
            var model = mk == null ? new SuouBookPageModel() : SuouBookPageModel.Parse(mk);
            var p = new ObsoleteSectionBook() { Name = name, Pages = pages,Model = model };
            p.IsEdit = false;
            return p;
        }
        private static ObsoleteSectionBook Parse2(XElement parser)
        {
            if (parser == null) throw new MyException("");
            var nameAtt = parser.Attribute("Name");
            var name = nameAtt == null ? "NoName" : nameAtt.Value;
            var pages = new SuperObservableCollection<SectionScene>(SectionScene.ListParse2(parser.Element("Pages")));
            var mk = parser.Element(SuouBookPageModel.ClassName);
            var model = mk == null ? new SuouBookPageModel() : SuouBookPageModel.Parse(mk);
            var p = new ObsoleteSectionBook() { Name = name, Pages = pages, Model = model };
            p.IsEdit = false;
            return p;
        }
    }
}
