﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using SuouBase.Book;
namespace SuouBase.Book
{

    [Serializable]
    public class SectionScene : AbstractSectionScenePart,IBookPage,IParse,IXmlSerializable
    {
        //public int Number { get; set; }
        #region BaseProperty
        private ObservableCollection<SectionScenePart> _Parts;
        private SuouText _Text { get; set; }
        private bool _JoinTop = false;
        //private string _Name = null;
        public int _JointSpaceLine = 3;
        //private bool _IsChanged = false;
        private bool IsEditing { get; set; }
        #endregion
        #region Property
        public ObservableCollection<SectionScenePart> Parts
        {
            get { return _Parts; }
            private set
            {
                _Parts = new ObservableCollection<SectionScenePart>();
                this.IsEditing = true;
                foreach (var v in value)
                {
                    this.AddPage(v);
                }
                this.IsEditing = false;

                _Parts.CollectionChanged += this.PagePart_ItemChanged;
                this.PageParts_CountChanged(this,new EventArgs());
            }
        }
        /*
        public void AddSerihu(string text)
        {
            var p = SectionScenePart.CreateSerifuPart(this.Parts.Count,this.Merger);
            p.SText = new SuouText(text);
            this.AddPage(p);
        }
        */
        public SectionScenePart BackGround
        {
            get { return this.Parts.First(); }
        }
        public override SuouText SText
        {
            get
            {
                if (this._Text == null)
                {
                    Action f = () => this.RefreshMerge();
                    BSystem.Diagnostics.ConsoleStopWatch(f, "RefreshMerge");

                    //this.RefreshMerge();
                }
                return _Text; 
            }
            set
            {
                _Text = value;
            }
        }
        public bool JoinTop
        {
            get { return _JoinTop; }
            set
            {
                _JoinTop = value;
                this.InvokeContentChangeEvent(PageContentChangeType.JoinTop);
            }
        }
        public int JoinSpaceLine
        {
            get { return _JointSpaceLine; }
            set
            {
                _JointSpaceLine = value;
                this.InvokeContentChangeEvent(PageContentChangeType.JoinSpaceLine);
            }
        }
        /*
        public string Name
        {
            get { return this._Name; }
            set
            {
                if (this._Name != value)
                {
                    this._Name = value;
                    this.InvokeContentChangeEvent(PageContentChangeType.Name);
                }
            }
        }
        */
        public bool IsChanged
        {
            get { return this._Text == null; }
        }
        public void RefreshText()
        {
            if (this.IsChanged)
            {
                this.RefreshMerge();
            }
        }
        public SuouText GetSTextWithoutInvokeEvent()
        {
            if (this._Text != null)
            {
                return this.SText;
            }
            else
            {
                return this.Merge();
            }
        }
    
        #endregion
        #region Event
        //[field: NonSerialized]
        //public event EventHandler TextChanged;
        [field: NonSerialized]
        public event BookPageContentChangeEventHandler EditStateChanged;

        //private bool IsNotifyChanged { get; set; }
        private void PagePart_ItemChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (e.NewItems != null)
            {
                foreach (SectionScenePart v in e.NewItems)
                {
                    v.ContentChanged += this.PagePart_ContentChanged;
                } 
            }
            if (e.OldItems != null)
            {
                foreach (SectionScenePart v in e.OldItems)
                {
                    v.ContentChanged -= this.PagePart_ContentChanged;
                }
            }
            if (!this.IsEditing)
            {
                //if (this.IsNotifyChanged) 
                    this.PageParts_CountChanged(sender, e);
            }
        }
        private void InvokeContentChangeEvent(PageContentChangeType type)
        {
            if (EditStateChanged != null)
            {
                EditStateChanged(this, new BookPageContentChangeEventArgs(this, type));
            }
        }
        private void PageParts_CountChanged(object sender, EventArgs e)
        {       
            this.InvokeContentChangeEvent(PageContentChangeType.TextPartsCount);
            this.UpdateNullText(true);
        }
        private void PagePart_ContentChanged(object sender, EventArgs e)
        {
            BSystem.Debug.ColorConsole.IncrementTabCount();
            var sw = System.Diagnostics.Stopwatch.StartNew();
            this.InvokeContentChangeEvent(PageContentChangeType.TextPart);
            this.UpdateNullText(false);
            sw.Stop();

            if(sw.ElapsedMilliseconds > 100)BSystem.Debug.ColorConsole.WriteLine($"PagePart_ContentChanged: {sw.ElapsedMilliseconds}");
            BSystem.Debug.ColorConsole.DecrementTabCount();

        }
        #endregion
        #region Constructor
        public SectionScene()
        {
            Parts = new ObservableCollection<SectionScenePart>() { new SectionScenePart() { Name = "背景" } };
            this.Parts.CollectionChanged += this.PagePart_ItemChanged;
            Name = "";
            this._Text = new SuouText();
            if (this.Parts.Count == 0) throw new MyException("");
        }
        public SectionScene(AstPage ap)
            : this()
        {
            
            this.Name = ap.Name;

            var a = ap.SText;//コピーされている


            this.Parts.First().SText = a.Clone();

            this._Text = a;
            this.JoinSpaceLine = 0;
            this.JoinTop = false;
            if (this.Parts.Count == 0) throw new MyException("");
        }
        public SectionScene(SectionScene ap)
            : this()
        {
            this.Name = ap.Name;
            this.JoinTop = ap.JoinTop;
            this.JoinSpaceLine = ap.JoinSpaceLine;
            this.Parts.Clear();

            ap.Parts.ToList().ForEach((v) => this.AddPage(v.Copy()));
            this._Text = ap.SText.Clone();
            if (this.Parts.Count == 0) throw new MyException("");
        }
        public SectionScene(IBookPage ap)
            : this()
        {
            this.Name = ap.Name;
            var a = ap.SText.Clone();
            var b = ap.SText.Clone();
            this.Parts.First().SText = a;
            this._Text = b;
            if (this.Parts.Count == 0) throw new MyException("");
        }
        public SectionScene(SuouText st)
            : this()
        {
            this.Initialize(st);
        }
        #endregion
        #region Method
        public void Initialize(SuouText st)
        {

            Parts = new ObservableCollection<SectionScenePart>() { new SectionScenePart() { Name = "背景", SText = st.Clone() } };
            this.SText = this.Merge();
        }
        public Size GetLooseSize(int ySpace)
        {
            return this.SText.GetLooseSize(ySpace);
        }
        public SuouText CreateOuterText()
        {
            var w = SuouText.Empty(this.SText);
            if (this.JoinSpaceLine == 0)
            {
                w = this.SText.Clone();
            }
            else
            {
                (this.JoinSpaceLine - 1).ForEach((v) => w.Add(""));
                w %= this.SText;
            }
            return w;
        }
        private void AddPage(SectionScenePart p)
        {
            this._Parts.Add(p);
            p.ContentChanged += this.PagePart_ContentChanged;
        }
        public static SectionScene Copy(IBookPage ap)
        {
            if (ap is AstPage)
            {
                var f = (AstPage)ap;
                return new SectionScene(f);
            }
            else if (ap is SectionScene)
            {
                var f = (SectionScene)ap;
                return new SectionScene(f);
            }
            return null;
        }
        public static SectionScene Load(string filename)
        {
            var f = BSystem.XML.BinarySerialize.LoadFromBinaryFile<SectionScene>(filename);
            if (f is SectionScene)
            {
                return (SectionScene)f;
            }
            return null;
        }
        public void Save(string filename)
        {
            try
            {
                BSystem.XML.BinarySerialize.SaveToBinaryFile(this, filename);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        public SectionScene Copy()
        {
            return new SectionScene(this);
        }
        public IBookPage IBookPageCopy()
        {
            return this.Copy();
        }
        public void Refresh()
        {
            foreach (var v in Parts)
            {
                v.ContentChanged += this.PagePart_ContentChanged;
            }
            Parts.CollectionChanged += this.PagePart_ItemChanged;
        }
        public void RefreshMerge()
        {
            this.SText = this.Merge();
        }
        private SuouText Merge()
        {
            var downtext = this.Parts.First().SText.Clone();
            var layerParts = this.Parts.Skip(1).Where((v) => v.Enable).ToList();
            bool isAllUnicodeSpace = layerParts.All((v) => v.Merger.isUnicodeSet);
            if (isAllUnicodeSpace && layerParts.Count > 0)
            {
                var unico = layerParts[0].CreateAltSpace();
                var layers = layerParts.Select((v) =>
                {
                    var w = v.WakuTextInfo;
                    w.Location = v.EditLocation;
                    return w;
                }).ToArray();
                SpaceLineSet.MergeText(downtext, layers, unico);
            }
            else
            {
                this.Parts.Skip(1).ToList().ForEach((v) => downtext = v.Enable ? v.Merge(downtext) : downtext);
            }
            if (this.BackGround.Waku.Type != HukidasiType.Nothing)
            {
                downtext = this.BackGround.Waku.CreateWakuText(downtext);
            }

            return downtext;
        }
        #endregion
        #region Other
        public static explicit operator AstPage(SectionScene mp)
        {
            var r = new AstPage();
            r.Name = mp.Name;
            r.SText = mp.SText.Clone();
            return r;
        }
        public SectionPage ToSectionPage2(SuouBookPageModel model)
        {
            var copy = this.Copy();
            copy.JoinSpaceLine = 0;
            if(model != null)copy.JoinSpaceLine = model.TopAppendSpaceCount;
            var r = new SectionPage();
            r.Name = this.Name;
            r.Items.Add(copy);
            return r;
        }
        #endregion

        #region XML
        public static string ClassName = "SectionBookPage";
        public static string ClassName2 = "SectionScene";
        public XElement CreateSerializer()
        {
            var xe = new XElement(ClassName);
            xe.Add(new XAttribute("JoinTop", this.JoinTop));
            xe.Add(new XAttribute("JoinSpaceLine", this.JoinSpaceLine));
            xe.Add(new XAttribute("Name", this.Name));
            if (this.Parts.Count != 1)
            {
                xe.Add(this.SText.CreateSerializer());
            }

            var items = new XElement("Parts");
            this.Parts.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => items.Add(v));
            xe.Add(items);

            return xe;
        }
        public static void Parse0(XElement parser, SectionScene item)
        {
            item.JoinTop = parser.AttributeBoolValue("JoinTop");
            item.JoinSpaceLine = parser.AttributeIntValue("JoinSpaceLine");
            item.Name = parser.Attribute("Name").Value;
            var parts = SectionScenePart.ListParse0(parser.Element("Parts"));
            var g = new ObservableCollection<SectionScenePart>(parts);
            item.Parts = g;
            if (parts.Count == 1)
            {
                item.SText = parts[0].SText;
            }
            else
            {
                item.SText = SuouTextParser.Parse1(parser.Element(SuouText.ParserName));
            }
            
        }
        public static void Parse1(XElement parser, SectionScene item)
        {
            item.JoinTop = parser.AttributeBoolValue("JoinTop");
            item.JoinSpaceLine = parser.AttributeIntValue("JoinSpaceLine");
            item.Name = parser.Attribute("Name").Value;
            
            var parts = SectionScenePart.ListParse1(parser.Element("Parts"));

            var g = new ObservableCollection<SectionScenePart>(parts);
            item.Parts = g;
            if (parts.Count == 1)
            {
                item.SText = parts[0].SText;
            }
            else
            {
                item.SText = SuouTextParser.Parse2(parser.Element(SuouText.ParserName));
            }

        }
        public static void ParseNew(XElement parser, SectionScene item)
        {
            item.JoinTop = parser.AttributeBoolValue("JoinTop");
            item.JoinSpaceLine = parser.AttributeIntValue("JoinSpaceLine");
            item.Name = parser.Attribute("Name").Value;
            var parts = SectionScenePart.ListParse2(parser.Element("Parts"));

            var g = new ObservableCollection<SectionScenePart>(parts);
            item.Parts = g;
            if (parts.Count == 1)
            {
                item.SText = parts[0].SText;
            }
            else
            {
                item.SText = SuouTextParser.Parse2(parser.Element(SuouText.ParserName));
            }
        }

        public static SectionScene Parse0(XElement parser)
        {
            var p = new SectionScene();
            Parse0(parser, p);
            return p;
        }
        public static List<SectionScene> ListParse0(XElement xe)
        {
            var f = xe.Elements().Select((v) => SectionScene.Parse0(v)).ToList();
            return f;
        }
        public static SectionScene Parse1(XElement parser)
        {
            var p = new SectionScene();
            Parse1(parser, p);
            return p;
        }
        public static SectionScene Parse2(XElement parser)
        {
            var p = new SectionScene();
            ParseNew(parser, p);
            return p;
        }
        public static List<SectionScene> ListParse1(XElement xe)
        {
            var f = xe.Elements().Select((v) => SectionScene.Parse1(v)).ToList();
            return f;
        }
        public static List<SectionScene> ListParse2(XElement xe)
        {
            var f = xe.Elements().Select((v) => SectionScene.Parse2(v)).ToList();
            return f;
        }
        #endregion
        public void WriteXml(System.Xml.XmlWriter writer)
        {
            this.CreateSerializer().WriteTo(writer);
            
        }
        public void ReadXml(System.Xml.XmlReader reader)
        {
            var v = XElement.Load(reader);
            Parse0(v.Element("SuouText"),this);   
        }
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }

        public string Text
        {
            get
            {
                return this.SText.Text;
            }
        }
        public void Dispose()
        {
            this.Parts.CollectionChanged -= this.PagePart_ItemChanged;
        }

        public void Replace(SectionScene page)
        {
            this.IsEditing = true;
            var count = this.Parts.Count;
            
            foreach (var v in page.Parts)
            {
                this.Parts.Add(v.Copy());
            }
            for (int i = 0; i < count; i++)
            {
                this.Parts.RemoveAt(0);
            }
            this.IsEditing = false;
            this.PageParts_CountChanged(this, new EventArgs());
            this.UpdateNullText(true);
        }

        private void UpdateNullText(bool isUpdate)
        {
            this._Text = null;
            if(isUpdate)this.InvokeContentChangeEvent(PageContentChangeType.Text);
        }
        public SectionScenePart AddSerihuPart(string name,string text)
        {
            
            var r = SectionScenePart.CreateSerifuPart(name, this.Merger);
            r.SText = new SuouText(text);
            var x = this.BackGround.SText.Width+33;
            if (x > 800) x = 800;
            var y = this.BackGround.SText.Count / 2;
            r.EditLocation = new EditPoint(x, y);
            this.AddPage(r);
            return r;
        }
    }
}
