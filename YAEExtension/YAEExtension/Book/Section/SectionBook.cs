﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using SuouBase.Book;
namespace SuouBase.Book
{
    
    [Serializable]
    public class ObsoleteSectionBook:BaseBook<SectionScene>,IBook,IParse
    {
        #region BaseProperty
        #endregion
        #region Property
        private SuouBookPageModel _Model = new SuouBookPageModel();
        public SuouBookPageModel Model
        {
            get { return this._Model; }
            set
            {
                if (value != null)
                {
                    this._Model = value;
                }
            }
        }

        public override IEnumerable<IBookPage> OutputPages
        {
            get
            {
                var v = ObsoleteSectionBook.GetClusters(this.Pages, this.Model).Select((p) => p.Page);
                foreach (var f in v)
                {
                    yield return f;
                }
            }
        }

        public IEnumerable<IBookPageCluster> Clusters
        {
            get
            {
                return ObsoleteSectionBook.GetClusters(this.Pages, this.Model);
            }
        }
        public override IEnumerable<IBookPage> BookPages
        {
            get
            {
                return this.Pages;
            }
            set
            {
                if (value != null)
                {
                    var f = value.Select((v) => (SectionScene)v).ToList();
                    this.Pages = new SuperObservableCollection<SectionScene>(f);
                }
            }
        }
        public override IEnumerable<IBookPage> DetailBookPages
        {
            get
            {
                return this.Pages;
            }
        }
        public override IBookPage VirtualSelectedPage
        {
            get
            {
                if (this.VirtualSelectedIndex == -1) return null;
                return this.OutputPages.Skip(this.VirtualSelectedIndex).FirstOrDefault();
            }
        }
        public int VirtualSelectedIndex
        {
            get
            {
                var p = this.SelectedPageIndex;
                return ObsoleteSectionBook.TranslateVirtualIndex(this.Pages, p,this.Model);
            }
        }
        public override IEnumerable<IBookPage> VirtualSelectedGrupePages
        {
            get { return this.OutputPages; }
        }

        #endregion
        #region ShowProperty
        public override IBookPage this[int i]
        {
            get
            {
                return this.Pages[i];
            }
            set
            {
                this.Pages[i] = (SectionScene)value;
            }
        }
        public void ResetEvent()
        {
            this.Pages = this.Pages;
            this.Pages.ForEach((v) => v.Refresh());
        }
        #endregion
        #region Constructor
        public ObsoleteSectionBook()
        {
            this.Name = SuouEditorConst.NewFileName(AAFileExtension.Sst);
            this.Pages = new SuperObservableCollection<SectionScene>() { new SectionScene() };
        }
        public ObsoleteSectionBook(EditingFile ef)
        {
            this.Name = ef.Name;
            this.Pages = new SuperObservableCollection<SectionScene>();
            foreach (var v in ef.Pages)
            {
                this.Pages.Add(new SectionScene(v));
            }
        }
        #endregion
        #region Method
        public override void Save(string filename,bool OffIsEdit) 
        {
            if (IODirectory.CheckExistDirectory(filename))
            {
                this.SaveXML(filename);
                base.Save(filename, OffIsEdit);
            }
            else
            {
                throw new MyException("Error");
            }
        }
        public static ObsoleteSectionBook XMLLoad(string filename)
        {
            ObsoleteSectionBook r = null;
            try
            {
                var x = XElement.Load(filename,LoadOptions.PreserveWhitespace);
                r = ObsoleteSectionBookParser.Parse(x);
                r.Pages = r.Pages;
                foreach (var w in r.Pages) w.Refresh();
            }
            catch (Exception)
            {
                r = new ObsoleteSectionBook() { };
            }
            finally
            {
                var f = new FileInfo(filename);
                r.Name = f.Name;
                r.IsEdit = false;
                r.FullFilePath = f.FullName;
            }
            return r;
        }
        #endregion
        #region Other
        public static SectionScene MergePage(SectionScene UpPage, SectionScene DownPage)
        {
            var r = new SectionScene();
            var text = UpPage.SText.Clone();
            for (int i = 0; i < DownPage.JoinSpaceLine; i++) text.Lines.Add(new SuouLine());
            DownPage.SText.Lines.ForEach((v) => text.Lines.Add(v.Clone()));
            
            r.Parts.Clear();
            var c = new SectionScenePart(HukidasiSaveData.NoneDefault(),SpaceLineSet.DefaultSet){SText = text.Clone()};
            r.Parts.Add(c);
            //r.SText = text.Clone();

            r.JoinSpaceLine = UpPage.JoinSpaceLine;
            r.JoinTop = UpPage.JoinTop;
            r.Name = UpPage.Name;

            return r;
        }
        public KeyValuePair<ObsoleteSectionBook, ObsoleteSectionBook> Split(int n)
        {
            var a = new ObsoleteSectionBook();
            a.Pages.Clear();
            var b = new ObsoleteSectionBook();
            b.Pages.Clear();
            this.Pages.Take(n).ForEach((v) => a.Pages.Add(v.Copy()));
            this.Pages.Skip(n).ForEach((v) => b.Pages.Add(v.Copy()));
            a.Name = this.Name;
            b.Name = this.Name;
            return new KeyValuePair<ObsoleteSectionBook, ObsoleteSectionBook>(a, b);
        }
        public AstPage[] CreatePages()
        {
            return this.Clusters.Select((v) => v.Page).ToArray();
        }
        public static AstPage Create(IEnumerable<SectionScene> items)
        {
            if (items.Count() == 0) return new AstPage();
            var name = items.First().Name;
            var tex = items.First().CreateOuterText();

            foreach (var v in items.Skip(1))
            {
                tex %= v.CreateOuterText();
            }
            var w = new AstPage(name,tex.Text);
            return w;
        }
        
        #endregion
        
        /*
        public override void Add(IBookPage p)
        {
            this.Add(p.ToMergeBookPage());
        }
        public override void Insert(int n, IBookPage Page)
        {
            this.Insert(n,Page.ToMergeBookPage());
        }
        */
        #region XML
        public static string ClassName = "SectionBook";
        public XElement CreateSerializer()
        {
            var xe = new XElement(ClassName);
            
            xe.Add(new XAttribute("Name", this.Name));
            xe.Add(this.Model.CreateSerializer());
            xe.Add(new XAttribute("Version", 2));
            var items = new XElement("Pages");
            this.Pages.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => items.Add(v));
            xe.Add(items);

            return xe;
        }
        
        #endregion
        
        #region Iterator
        public static int TranslateVirtualIndex(IEnumerable<SectionScene> ims,int n,SuouBookPageModel model)
        {
            int i = 0;
            var p = ObsoleteSectionBook.GetClusters(ims,model);
            var gp = p.Select(v => v.Pages.Count());
            foreach (var v in gp)
            {
                n -= v;
                if (n < 0) return i;
                i++;
            }
            return i;
        }
        public static IEnumerable<IBookPageCluster> GetClusters(IEnumerable<SectionScene> ims,SuouBookPageModel model)
        {

            List<SectionScene[]> r = new List<SectionScene[]>();
            var tmp = new List<SectionScene>();
            foreach (var v in ims)
            {
                if (v.JoinTop)
                {
                    tmp.Add(v);
                }
                else
                {
                    if (tmp.Count != 0) r.Add(tmp.ToArray());
                    tmp.Clear();
                    tmp.Add(v);
                }
            }
            r.Add(tmp.ToArray());
            return r.Select((p) => new IBookPageCluster(p,model));
        }
        #endregion
        public override SectionScene CreateNewBookPage()
        {
            var r = new SectionScene { JoinSpaceLine = this.Model.TopAppendSpaceCount };
            r.Parts.First().Merger = this.Model.DefaultMergeSet;
            return r;
        }
        public SectionNBook ToSectionNBook()
        {
            return SectionNBook.Translate(this);
        }
    
    }
    

    
}
