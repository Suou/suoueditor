﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using SuouBase.Book;

namespace SuouBase.Text
{
    public class IJoinTopExtension
    {
        public static IEnumerable<IJoinTop[]> Clasterize(IList<IJoinTop> items)
        {
            List<IJoinTop[]> r = new List<IJoinTop[]>();
            var tmp = new List<IJoinTop>();
            foreach (var v in items)
            {
                if (v.JoinTop)
                {
                    tmp.Add(v);
                }
                else
                {
                    if (tmp.Count != 0) r.Add(tmp.ToArray());
                    tmp.Clear();
                    tmp.Add(v);
                }
            }
            r.Add(tmp.ToArray());
            return r;
        }
        public static IEnumerable<IJoinTop> GetCluster(IList<IJoinTop> ims, int index)
        {
            List<IJoinTop> r = new List<IJoinTop>();
            int n = index;
            int m = index + 1;
            while (n >= 0)
            {
                if (!ims[n].JoinTop) break;
                n--;
            }
            while (m < ims.Count)
            {
                if (!ims[m].JoinTop) break;
                m++;
            }
            for (int i = n; i < m; i++)
            {
                r.Add(ims[i]);
            }
            return r;
        }
        public static int ClusterIndexOf(IEnumerable<IJoinTop> ims, int index)
        {
            return ims.Take(index).Count((v) => !v.JoinTop);
        }


    }
    public class IBookPageCluster
    {
        public IEnumerable<IBookPage> Pages { get; private set; }
        private SuouBookPageModel Model { get; set; }
        private IEnumerable<SectionScene> MPages
        {
            get
            {
                return (this.Pages.Select((v) => (SectionScene)v));
            }
        }
        public IBookPageCluster(IEnumerable<IBookPage> ims,SuouBookPageModel model)
        {
            this.Pages = ims;
            this.Model = model;
        }
        private void Text_Changed(object sender, BookPageContentChangeEventArgs e)
        {
            if (e.Type == PageContentChangeType.JoinTop)
            {
            }
            else
            {
            }
        }
        public AstPage Page
        {
            get
            {
                if (this.Pages.Count() == 0) return null;
                if (this.Pages.First() is SectionScene)
                {
                    return ObsoleteSectionBook.Create(this.Pages.Select((v) => (SectionScene)v));
                }
                else
                {
                    return this.Pages.First().ToAstPage();
                }
            }
        }
        public int Size
        {
            get
            {
                if (this.Pages.Count() == 0) return 0;
                if (this.Pages.First() is SectionScene)
                {

                    var p = this.MPages.Sum((v) => v.SText.GetSitarabaByteSize + (v.JoinSpaceLine * 4));
                    p += (this.Pages.Count() - 1) + 4;
                    return p;
                }
                else
                {
                    return this.Pages.First().SText.GetSitarabaByteSize;
                }
            }
        }
        public int Width
        {
            get
            {
                return this.Pages.Max((v) => v.SText.Width);
            }
        }
        public int Count
        {
            get
            {
                return this.Pages.Sum((v) => v.SText.Count);
            }
        }

        public bool IsInvalid
        {
            get
            {
                if (this.Size >= Model.MaxSize || this.Width > Model.MaxPageWidth || this.Count > Model.MaxPageCount)
                {
                    return true;
                }
                return false;
            }
        }
    }


    public enum MergePartsType
    {
        Comment, Other
    }
    public enum MergeContentChangeType
    {
        TextDestructive, NoTextDestructive,EditLocation
    }
    public enum PageContentChangeType
    {
        Name, Text, TextPart, JoinTop, JoinSpaceLine, Other,TextPartsCount,ChapterPageInnerType
    }
    public enum BookContentChangeType
    {
        Page,PageContent,Other,Save
        //PageName, PageText, PageTextPart, PageJoinTop, PageJoinSpaceLine, PageCount, PageInsert, SetPages, PageChanged, Other
    }
    public static class BookContentChangeTypeExtension
    {
        /*
        public static BookContentChangeType Translate(this PageContentChangeType type)
        {
            switch (type)
            {
                case PageContentChangeType.JoinSpaceLine: return BookContentChangeType.PageJoinSpaceLine;
                case PageContentChangeType.JoinTop: return BookContentChangeType.PageJoinTop;
                case PageContentChangeType.Name: return BookContentChangeType.PageName;
                case PageContentChangeType.Text: return BookContentChangeType.PageText;
                case PageContentChangeType.TextPart: return BookContentChangeType.PageTextPart;
                case PageContentChangeType.Other: return BookContentChangeType.Other;
            }
            return BookContentChangeType.Other;
        }
        */
    }
}
