﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem;
using SuouBase.Text.Hukidasi;
namespace SuouBase.Book
{
    [Serializable]
    public abstract class AbstractSectionScenePart
    {
        #region BaseProperty
        [NonSerialized]
        [XmlIgnore]
        private BasicLineMerge _WakuTextInfo = null;
        private IHukidasi _Waku;
        private string _Name;
        private Point _EditLocation;
        private bool _Enable = true;
        private MergePartsType _Type;
        private SpaceLineSet _Merger;
        private MergeZureOption _MergeOption = MergeZureOption.Destructive;
        private SuouText _WakuSText;
        private int _PermitZureLength = 16;
        public abstract SuouText SText { get; set; }
        #endregion

        public SuouText WakuSText
        {
            get
            {
                if (this._WakuSText == null) this._WakuSText = this.CreateWakuText();
                return this._WakuSText;
            }
        }
        public BasicLineMerge WakuTextInfo
        {
            get
            {
                if (_WakuTextInfo == null)
                {
                    //var v = this.CreateAltSpace();
                    this._WakuTextInfo = this.CreateLineMerge();
                }
                _WakuTextInfo.Location = this.EditLocation;
                return _WakuTextInfo;
            }
        }

        public MergePartsType Type
        {
            get { return _Type; }
            set
            {
                _Type = value;
                /*
                if (value == MergePartsType.Comment)
                {
                    //this.Waku.Options.IsTrimSpace = false;
                    this.EdgeSpaceTransparent = false;
                }
                else
                {
                }
                */
                NotifyContentChanged(MergeContentChangeType.TextDestructive);
            }
        }
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                NotifyContentChanged(MergeContentChangeType.NoTextDestructive);
            }
        }
        public EditPoint EditLocation
        {
            get { return new EditPoint(this._EditLocation.X, this._EditLocation.Y); }
            set { _EditLocation = value.ToPoint(); NotifyContentChanged(MergeContentChangeType.EditLocation); }
        }
        /// <summary>
        /// 値がTrueのときのみ合成されます。
        /// </summary>
        public bool Enable
        {
            get { return _Enable; }
            set
            {
                if (_Enable != value)
                {
                    _Enable = value;
                    if (EnableChanged != null) EnableChanged(this, new EventArgs());
                    NotifyContentChanged(MergeContentChangeType.NoTextDestructive);
                }
                else
                {
                    _Enable = value;
                }

            }
        }

        public SpaceLineSet Merger
        {
            get
            {
                if (this._Merger == null) this._Merger = SpaceLineSet.DefaultSet;
                return this._Merger;
            }
            set
            {
                this._Merger = value;
                NotifyContentChanged(MergeContentChangeType.TextDestructive);
                if (this._Merger == null) throw new MyException("");
            }
        }
        public MergeZureOption MergeOption
        {
            get
            {
                return this._MergeOption;
            }
            set
            {
                this._MergeOption = value;
                NotifyContentChanged(MergeContentChangeType.TextDestructive);
            }
        }
        public int PermitZureLength
        {
            get { return this._PermitZureLength; }
            set
            {
                this._PermitZureLength = value;
                NotifyContentChanged(MergeContentChangeType.TextDestructive);
            }
        }


        #region EventProperty
        [field: NonSerialized]
        public event SectionBookPagePartContentChangeEventHandler ContentChanged;
        [field: NonSerialized]
        public event EventHandler EnableChanged;
        #endregion


        #region EventMethod
        protected void NotifyContentChanged(MergeContentChangeType type)
        {
            
            if (type == MergeContentChangeType.TextDestructive)
            {
                this._WakuTextInfo = null;
                this._WakuSText = null;
            }
            if (ContentChanged != null) { ContentChanged(this, new SectionBookPagePartContentChangeEventArgs(this, type)); }
            
        }
        #endregion

        public IHukidasi Waku
        {
            get
            {
                if (_Waku == null) this._Waku = HukidasiSaveData.NoneDefault();
                return this._Waku;
            }
            set
            {
                this._Waku = value;
                NotifyContentChanged(MergeContentChangeType.TextDestructive);
                if (this._Waku.MergeAlgorithm == null) throw new MyException("");
            }
        }

        public SuouText Merge(SuouText DownText)
        {
            var r = this.Merger.MergeText(DownText, this.WakuTextInfo, this.EditLocation, this.MergeOption, this.PermitZureLength);
            return r;
        }
        public SuouText CreateWakuText()
        {
            return this.Waku.CreateWakuText(this.SText);
        }
        public SpaceMaker CreateAltSpace()
        {
            var v = SpaceMakerDic.Create(this.Merger, this.SText.Font, this.SText.Color);

            return v;
        }
        public AltText CreateAltText()
        {
            return new AltText(this.CreateAltSpace());
        }
        public BasicLineMerge CreateLineMerge()
        {
            return this.WakuSText.ToLineMerge(this.CreateAltSpace(), this.Waku.IsTrimSpace);
        }
    }
}
