﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem;
using SuouBase.Text.Hukidasi;
namespace SuouBase.Book
{
    public class MergeOption
    {
        private SpaceLineSet _Merger;
        private int _PermitZureLength = 16;
        private MergeZureOption _Option = MergeZureOption.Destructive;
        public event EventHandler ContentChanged;
        public SpaceLineSet Merger
        {
            get
            {
                if (this._Merger == null) this._Merger = SpaceLineSet.DefaultSet;
                return this._Merger;
            }
            set
            {
                this._Merger = value;
                if (this.ContentChanged != null) this.ContentChanged(this, new EventArgs());
                if (this._Merger == null) throw new MyException("");
            }
        }
        public MergeZureOption Option
        {
            get
            {
                return this._Option;
            }
            set
            {
                this._Option = value;
                if (this.ContentChanged != null) this.ContentChanged(this, new EventArgs());
            }
        }
        public int PermitZureLength
        {
            get { return this._PermitZureLength; }
            set
            {
                this._PermitZureLength = value;
                if (this.ContentChanged != null) this.ContentChanged(this, new EventArgs());
            }
        }
    }
}
