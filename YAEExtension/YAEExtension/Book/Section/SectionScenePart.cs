﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem;
using SuouBase.Text.Hukidasi;
namespace SuouBase.Book
{
    [Serializable]
    public class SectionScenePart:AbstractSectionScenePart,IParse
    {
        
        #region BaseProperty
        private SuouText _Text = new SuouText();
        #endregion
        #region Property

        public override SuouText SText
        {
            get { return _Text; }
            set
            {
                if (value == null) throw new Exception();
                _Text = value;
                NotifyContentChanged(MergeContentChangeType.TextDestructive);
            }
        }
        #endregion
        public static SectionScenePart FirstMergeBookPagePart()
        {
            var f = new SectionScenePart(HukidasiSaveData.NoneDefault(), SpaceLineSet.DefaultSet);
            return f;
        }
        #region Constructor
        public static SectionScenePart CreateSerifuPart(string name, SpaceLineSet lineset)
        {
            var r = new SectionScenePart(SuouEditorConst2.DefaultHukidasi, lineset) { Type = MergePartsType.Comment, Name = name,MergeOption = MergeZureOption.Free };
            var p = HukidasiSaveData.NoneDefault();
            p.Options.IsTrimSpace = false;
            r.Waku = p;
            return r;
        }
        

        public SectionScenePart(IHukidasi v, SpaceLineSet mergeAlgorithm)
        {
            //this.EdgeSpaceTransparent = true;
            if (v == null || v.MergeAlgorithm == null) throw new MyException("");
            _Text = new SuouText();
            this.Type = MergePartsType.Other;
            Name = "";
            Waku = v.Clone();
            this.Merger = mergeAlgorithm.Clone();
            if (this.Waku.MergeAlgorithm == null) throw new MyException("合成アルゴリズムが存在しません");
            if (this.Merger == null) throw new MyException("合成アルゴリズムが存在しません");
        }
        public SectionScenePart()
        {
            //this.EdgeSpaceTransparent = true;
            _Text = new SuouText();
            this.Type = MergePartsType.Other;
            Name = "";
        }
        #endregion
        #region Method
        public SectionScenePart Copy()
        {
            var p = new SectionScenePart(this.Waku.Clone(), this.Merger.Clone());
            p.Name = this.Name;
            p._Text = this.SText.Clone();
            p.Type = this.Type;
            p.MergeOption = this.MergeOption;

            p.EditLocation = this.EditLocation;
            p.Enable = this.Enable;

            return p;
        }
        #endregion


        #region XML
        private static string ClassName = "SectionBookPagePart";
        public XElement CreateSerializer()
        {
            var xe = new XElement(ClassName);
            xe.Add(new XAttribute("Type", (int)this.Type));
            xe.Add(new XAttribute("Name", this.Name));
            //xe.Add(new XAttribute("EdgeSpaceTransparent", this.EdgeSpaceTransparent));
            xe.Add(new XAttribute("Enable", this.Enable));
            xe.Add(new XAttribute("MergeOption", (int)this.MergeOption));
            xe.Add(new XAttribute("PermitZureLength", this.PermitZureLength));

            xe.Add(this.CreatePointXElement(this.EditLocation.ToPoint()));
            //xe.Add(this.Text.CreateXElement());
            xe.Add(this.SText.CreateSerializer());
            xe.Add(this.Merger.CreateSerializer());
            xe.Add(this.Waku.CreateSerializer());
            return xe;
        }
        public static SectionScenePart Parse0(XElement parser)
        {
            var name = parser.Attribute("Name").Value;
            var type = (MergePartsType)parser.AttributeIntValue("Type");
            //var est = parser.AttributeBoolValue("EdgeSpaceTransparent");
            var enable = parser.AttributeBoolValue("Enable");
            var point = PointParse(parser.Element("Point"));

            var text = SuouTextParser.Parse1(parser.Element(SuouText.ParserName));
            var merger = SpaceLineSet.Parse(parser.Element(SpaceLineSet.ParserName));
            var waku = HukidasiParser.Parse(parser.Element(HukidasiSaveData.ParseName));
            var mp = new SectionScenePart(waku, merger) { Name = name,Type = type,Enable = enable,EditLocation = new EditPoint(point.X,point.Y),_Text = text};
            return mp;
        }
        public static SectionScenePart Parse1(XElement parser)
        {
            var name = parser.Attribute("Name").Value;
            var type = (MergePartsType)parser.AttributeIntValue("Type");
            //var est = parser.AttributeBoolValue("EdgeSpaceTransparent");
            var enable = parser.AttributeBoolValue("Enable");
            var point = PointParse(parser.Element("Point"));

            var text = SuouTextParser.Parse2(parser.Element(SuouText.ParserName));
            var merger = SpaceLineSet.Parse(parser.Element(SpaceLineSet.ParserName));
            var waku = HukidasiParser.Parse(parser.Element(HukidasiSaveData.ParseName));
            var mp = new SectionScenePart(waku, merger) { Name = name, Type = type, Enable = enable, EditLocation = new EditPoint(point.X, point.Y), _Text = text };
            return mp;
        }
        public static SectionScenePart ParseNew(XElement parser)
        {
            var name = parser.Attribute("Name").Value;
            var type = (MergePartsType)parser.AttributeIntValue("Type");
            var est = parser.AttributeBoolValue("EdgeSpaceTransparent");
            var enable = parser.AttributeBoolValue("Enable");
            var point = PointParse(parser.Element("Point"));
            var zureop = (SuouBase.Text.MergeZureOption)parser.AttributeIntValue("MergeOption");
            var zurelength = parser.AttributeIntValue("PermitZureLength");

            var text = SuouTextParser.Parse2(parser.Element(SuouText.ParserName));
            var merger = SpaceLineSet.Parse(parser.Element(SpaceLineSet.ParserName));
            var waku = HukidasiParser.Parse(parser.Element(HukidasiSaveData.ParseName));
            var mp = new SectionScenePart(waku, merger) { Name = name, Type = type, Enable = enable, EditLocation = new EditPoint(point.X, point.Y), _Text = text, PermitZureLength = zurelength, MergeOption = zureop };
            return mp;
        }

        public XElement CreatePointXElement(Point p)
        {
            var xe = new XElement("Point");
            xe.Add(new XAttribute("X", p.X));
            xe.Add(new XAttribute("Y", p.Y));
            return xe;
        }
        public static Point PointParse(XElement parser)
        {
            var x = parser.AttributeIntValue("X");
            var y = parser.AttributeIntValue("Y");
            return new Point(x, y);
        }
        public static List<SectionScenePart> ListParse0(XElement xe)
        {
            var f = xe.Elements().Select((v) => SectionScenePart.Parse0(v)).ToList();
            return f;
        }
        public static List<SectionScenePart> ListParse1(XElement xe)
        {
            var f = xe.Elements().Select((v) => SectionScenePart.Parse1(v)).ToList();
            return f;
        }
        public static List<SectionScenePart> ListParse2(XElement xe)
        {
            var f = xe.Elements().Select((v) => SectionScenePart.ParseNew(v)).ToList();
            return f;
        }

        #endregion
    }
}
