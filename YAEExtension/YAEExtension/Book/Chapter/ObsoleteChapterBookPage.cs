﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using SuouBase.Interface;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Collections.Specialized;
using SuouBase.Book;
using SuouBase.Text;
namespace SuouBase.Book
{

    [Serializable]
    public class ObsoleteChapterBookPage : IBookPage, IParse
    {
        public static ObsoleteChapterBookPage Merge(ObsoleteChapterBookPage up, ObsoleteChapterBookPage down)
        {
            var r = (ObsoleteChapterBookPage)up.IBookPageCopy();
            down.Items.Pages.ForEach((v) => r.Items.Add(v.Copy()));
            return r;
        }

        public int Number { get; set; }
        #region Name
        private static string XMLName = "ChapterBookPage";
        #endregion
        #region Property
        public ObsoleteSectionBook Items { get; private set; }
        //private string _Name = "";
        public string Name
        {
            get { return this.Items.Name; }
            set
            {
                if (value != null)
                {
                    this.Items.Name = value;
                    this.LightContentChenged(this, new BookPageContentChangeEventArgs(this,PageContentChangeType.Name));
                }
            }
        }
        [XmlIgnore]
        public SuouText SText
        {
            get
            {

                if (this.Items.Count == 0)
                {
                    return new SuouText();
                }
                else
                {
                    return this.Items.BookPages.First().SText;
                }
            }
            set
            {
            }
        }
        #endregion
        [field: NonSerialized]
        public event BookPageContentChangeEventHandler EditStateChanged;

        #region Method
        public void ResetEvent()
        {
            this.Items.ContentChanged += this.ContentChenged;
            this.Items.ResetEvent();
        }
        private void ContentChenged(object sender, BookContentChangeEventArgs e)
        {
            if (e.Type == BookContentChangeType.Other || e.Type == BookContentChangeType.Save)
            {
                throw new NotSupportedException();
            }
            else
            {
                var v = new BookPageContentChangeEventArgs(this, e);
                if (this.EditStateChanged != null) this.EditStateChanged(this, v);
            }
        }
        private void LightContentChenged(object sender, BookPageContentChangeEventArgs e)
        {
            var v = new BookPageContentChangeEventArgs(this, PageContentChangeType.Text);

            if (this.EditStateChanged != null) this.EditStateChanged(this, v);
        }

        public ObsoleteChapterBookPage()
        {
            this.Items = new ObsoleteSectionBook();
            this.Items.ContentChanged += this.ContentChenged;
            this.SText = new SuouText();
        }
        public IBookPage IBookPageCopy()
        {
            var v = new ObsoleteChapterBookPage();
            v.Items.Clear();
            v.Name = this.Name;
            this.Items.BookPages.ForEach((w) => v.Items.Add(w.IBookPageCopy()));
            return v;
        }
        public KeyValuePair<ObsoleteChapterBookPage, ObsoleteChapterBookPage> Split(int n)
        {
            var a = new ObsoleteChapterBookPage();
            a.Items.Pages.Clear();
            var b = new ObsoleteChapterBookPage();
            b.Items.Pages.Clear();
            this.Items.Pages.Take(n).ForEach((v) => a.Items.Pages.Add(v.Copy()));
            this.Items.Pages.Skip(n).ForEach((v) => b.Items.Pages.Add(v.Copy()));
            a.Name = this.Name;
            b.Name = this.Name;
            return new KeyValuePair<ObsoleteChapterBookPage, ObsoleteChapterBookPage>(a, b);
        }
        public Size GetLooseSize(int ySpace)
        {
            return this.SText.GetLooseSize(ySpace);
        }
        #endregion
        #region XML

        public XElement CreateSerializer()
        {
            var xe = new XElement(XMLName);            
            xe.Add(new XAttribute("Name", this.Name));
            xe.Add(this.Items.CreateSerializer());

            return xe;
        }
        public static ObsoleteChapterBookPage Parse(XElement parser)
        {
            var name = parser.Attribute("Name").Value;
            var book = ObsoleteSectionBookParser.Parse(parser.Element(ObsoleteSectionBook.ClassName));

            var p = new ObsoleteChapterBookPage() { Name = name };
            p.Items = book;
            p.ResetEvent();
            return p;

        }
        public static SuperObservableCollection<ObsoleteChapterBookPage> ListParse(XElement xe)
        {
            var g = new SuperObservableCollection<ObsoleteChapterBookPage>();
            var f = xe.Elements().Select((v) => ObsoleteChapterBookPage.Parse(v)).ToList();
            f.ForEach((v) => g.Add(v));
            return g;
        }
        #endregion

        public string Text
        {
            get
            {
                return this.SText.Text;
            }
        }
        public SuouText GetSTextWithoutInvokeEvent()
        {
            return this.SText;
        }
        public void RefreshText()
        {
        }
    }

}
