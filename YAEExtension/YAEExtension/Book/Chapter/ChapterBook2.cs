﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using SuouBase.Interface;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Collections.Specialized;
using SuouBase.Book;
using SuouBase.Text;
namespace SuouBase.Book.Chapter
{
    [Serializable]
    public class ChapterBook2 : BaseBook<ChapterBookPage2>, IBook, IParse
    {
        public override ChapterBookPage2 CreateNewBookPage()
        {
            var v = new ChapterBookPage2();
            v.Items.Model = this.Model;
            return v;
        }
        #region Constructor
        public ChapterBook2()
        {
            this.Name = "NewFile";
            this.Add(new ChapterBookPage2() { Name = "新しいチャプター" });
            //this.Pages.CollectionChanged += this.Pages_ContentChanged;
        }
        public ChapterBook2(IBook book)
        {
            if (book is ObsoleteChapterBook)
            {
                var bk = book as ObsoleteChapterBook;
                foreach (var v in bk.Pages)
                {
                    this.Add(v.ToBookGrouperPage2());
                }
                this.Name = book.Name;
            }
            else if (book is ChapterBook2)
            {
                throw new Exception();
            }
            else if (book is SectionNBook)
            {
                var p = (SectionNBook)book;
                this.Model = p.Model;
                this.Add(new ChapterBookPage2() { Name = "新しいチャプター" });
                this.Pages[0].Items.Clear();
                foreach (SectionPage page in book.BookPages)
                {
                    this.Pages[0].Items.Add(page);
                }
                this.Name = book.Name;

            }
            else if (book is EditingFile)
            {
                this.Model = SuouBookPageModel.CreateNaiveModel();
                this.Add(new ChapterBookPage2() { Name = "新しいチャプター" });
                this.Pages[0].Items.Clear();
                foreach (AstPage page in book.BookPages)
                {
                    this.Pages[0].Items.Add(page.ToSectionPage2(this.Model));
                }
                this.Name = book.Name;

            }
            else
            {
                throw new Exception();
            }
        }
        #endregion
        /*
        public override void Add(IBookPage p)
        {
            this.Add(p.ToBookGrouperPage2());
        }
        public override void Insert(int n, IBookPage page)
        {
            this.Insert(n, page.ToBookGrouperPage2());
        }
        */
        #region BaseProperty

        #endregion
        private SuouBookPageModel _Model = new SuouBookPageModel();
        public SuouBookPageModel Model
        {
            get { return this._Model; }
            set
            {
                if (value != null)
                {
                    this._Model = value;
                    foreach (var v in this.Pages)
                    {
                        v.Items.Model = this._Model;
                    }
                }
            }
        }

        #region GetProperty


        public override IBookPage VirtualSelectedPage
        {
            get
            {
                var page = this.GetBookPage(this.SelectedPageIndex);
                if (page == null) return null;
                return page.Items.VirtualSelectedPage;
            }
        }
        public override IEnumerable<IBookPage> VirtualSelectedGrupePages
        {
            get
            {
                var page = this.GetBookPage(this.SelectedPageIndex);
                if (page == null) return null;
                return page.Items.OutputPages;
            }
        }

        private ChapterBookPage2 GetBookPage(int index)
        {
            if (index < 0 || index >= this.Pages.Count)
            {
                return null;
            }
            else
            {
                return this.Pages[index];
            }
        }

        public KeyValuePair<ChapterBookPage2, int> TranslateSubIndex(int index)
        {
            var n = 0;
            var m = 0;
            ChapterBookPage2 k = null;
            foreach (var b in this.Pages)
            {
                n += b.Items.Count;
                if (n > index)
                {
                    m = n - b.Items.Count;
                    k = b;
                    break;
                }
            }
            return new KeyValuePair<ChapterBookPage2, int>(k, index - m);

        }
        #endregion
        #region Iterator



        //public int SelectedSectionIndex { get; set; }
        public override IBookPage this[int i]
        {
            get { return this.Pages[i]; }
            set { this.Pages[i] = value.ToBookGrouperPage2(); }
        }
        public override IEnumerable<IBookPage> OutputPages
        {
            get
            {
                foreach (var v in this.Pages)
                {
                    foreach (var w in v.Items.OutputPages)
                    {
                        yield return w;
                    }
                }
            }
        }
        public override IEnumerable<IBookPage> BookPages
        {
            get
            {
                foreach (var v in this.Pages)
                {
                    yield return v;
                }
            }
            set
            {
                var p = new SuperObservableCollection<ChapterBookPage2>(value.Select((v) => v.ToBookGrouperPage2()).ToList());
                this.Pages = p;
                //this.InvokeContentChangeEvent(BookContentChangeEventArgs.Create(this,BookContentChangeType.Page,NotifyCollectionChangedAction.Reset,0));
            }
        }
        public override IEnumerable<IBookPage> DetailBookPages
        {
            get
            {
                return this.Pages.SelectMany((v) => v.Items.DetailBookPages);
            }
        }

        #endregion
        #region Method
        public override void Save(string filename, bool OffIsEdit)
        {
            if (IODirectory.CheckExistDirectory(filename))
            {
                this.SaveXML(filename);
                base.Save(filename, OffIsEdit);
            }
            else
            {
                throw new MyException("");
            }
        }

        public void ResetEvent()
        {

            this.Pages.ForEach2((v) => v.EditStateChanged += this.Page_StateChanged).ForEach((v) => v.ResetEvent());
        }
        protected override void Page_StateChanged(object sender, BookPageContentChangeEventArgs e)
        {
            base.Page_StateChanged(sender, e);
        }
        [Obsolete]
        public static ObsoleteChapterBook Load(string filename)
        {
            throw new MyException("");
            /*
            var v = BSystem.XML.BinarySerialize.LoadFromBinaryFile<ChapterBook>(filename);
            if (v == null) return null;
            var r = (ChapterBook)v;
            var f = new FileInfo(filename);
            r.Name = f.Name;
            r.IsEdit = false;
            r.FullFilePath = filename;
            r.ResetEvent();
            return r;
            */
        }
        public static Tuple<string, bool> SanitizeLoadString(string filename)
        {
            StreamReader sr = new StreamReader(filename, Encoding.UTF8);
            string text = sr.ReadToEnd();
            sr.Close();

            var sb = new System.Text.StringBuilder();

            foreach (var c in text)
            {
                var code = (int)c;

                if (code == 0x9 ||
                    code == 0xa ||
                    code == 0xd ||
                    (0x20 <= code && code <= 0xd7ff) ||
                    (0xe000 <= code && code <= 0xfffd) ||
                    (0x10000 <= code && code <= 0x10ffff))
                {
                    sb.Append(c);
                }
            }

            string newText = sb.ToString();
            return new Tuple<string, bool>(newText, newText.Length == text.Length);

        }
        public static bool LoadCheck(string filename)
        {
            var t = SanitizeLoadString(filename);
            if (!t.Item2)
            {
                File.WriteAllText(filename, t.Item1, Encoding.UTF8);
                return false;
            }
            else
            {
                return true;
            }
        }
        public static ChapterBook2 XMLLoad(string filename)
        {
            //var x = XElement.Load(filename, LoadOptions.PreserveWhitespace);

            var sanL = SanitizeLoadString(filename);
            var x = XElement.Parse(sanL.Item1, LoadOptions.PreserveWhitespace);


            var r = ChapterBook2.Parse(x);
            var f = new FileInfo(filename);
            r.Name = f.Name;
            r.IsEdit = false;
            r.FullFilePath = f.FullName;
            return r;
        }
        #endregion
        #region XML
        public XElement CreateSerializer()
        {
            var xe = new XElement(ClassName);
            xe.Add(new XAttribute("Name", this.Name));
            xe.Add(new XAttribute("Version", 0));
            xe.Add(this.Model.CreateSerializer());
            var items = new XElement("Pages");
            this.Pages.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => items.Add(v));
            xe.Add(items);

            return xe;
        }
        public static ChapterBook2 Parse(XElement parser)
        {
            var version = parser.AttributeIntValue("Version");
            switch (version)
            {
                case 0: return Parse0(parser);
                default: throw new NotSupportedVertionException();
            }

        }
        private static ChapterBook2 Parse0(XElement parser)
        {
            var name = parser.Attribute("Name").Value;
            var pages = ChapterBookPage2.ListParse(parser.Element("Pages"));
            var p = new ChapterBook2() { Name = name, Pages = pages };

            var mk = parser.Element(SuouBookPageModel.ClassName);
            var model = mk == null ? new SuouBookPageModel() : SuouBookPageModel.Parse(mk);
            p.Model = model;
            p.ResetEvent();
            p.IsEdit = false;
            return p;

        }
        private static string ClassName = "ChapterBook";
        #endregion

        private void SetNumber()
        {
            int t = 1;
            foreach (var v in this.Pages)
            {
                v.SectionPageNumber = t;
                t += v.Items.Count;
            }
        }
        protected override void Pages_ContentChanged(object sender, SuperObservableCollectionEventArgs<ChapterBookPage2> e)
        {
            base.Pages_ContentChanged(sender, e);
            this.SetNumber();
        }
        protected override void SetPagesSetting(SuperObservableCollection<ChapterBookPage2> newItems)
        {
            base.SetPagesSetting(newItems);
            this.SetNumber();
        }
        public void Copy(int selectSection, IEnumerable<SectionPage> pages)
        {
            /*
            var newPages = this.Pages[selectSection].Items.Pages.Skip(pageIndex).ToArray();
            foreach (var v in newPages)
            {
                this.Pages[selectSection].Items.Remove(v);
            }
            */
            var page = this.CreateNewBookPage();
            page.Items.Clear();
            page.Name = this.Pages[selectSection].Name + "@";
            foreach (var v in pages)
            {
                page.Items.Add(v);
            }
            this.Insert(selectSection+1, page);
        }
    }
}
