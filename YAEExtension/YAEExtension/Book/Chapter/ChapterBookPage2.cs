﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using SuouBase.Interface;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Collections.Specialized;
using SuouBase.Book;
using SuouBase.Text;
using SuouBase.Book.Section2;
namespace SuouBase.Book.Chapter
{
    [Serializable]
    public class ChapterBookPage2 : IBookPage, IParse
    {
        /*
        public static ChapterBookPage2 Merge(ChapterBookPage up, ChapterBookPage down)
        {
            var r = (ChapterBookPage2)up.IBookPageCopy();
            down.Items.Pages.ForEach((v) => r.Items.Add(v.Copy()));
            return r;
        }
        */

        public void RefreshText()
        {
        }
        public SuouText GetSTextWithoutInvokeEvent()
        {
            return this.SText;
        }
        public ChapterBookPage2()
        {
            this.Items = new SectionNBook();
            this.Items.ContentChanged += this.ContentChenged;
            this.SText = new SuouText();
        }
        public ChapterBookPage2(IBookPage page):this()
        {
            if (page is ObsoleteChapterBookPage)
            {
                var chaPage = (ObsoleteChapterBookPage)page;
                this.Items = chaPage.Items.ToSectionNBook();
                this.Name = page.Name;
                //this.Number = page.Number;
            }
            else
            {
                throw new Exception();
            }
        }
        public static ChapterBook2 CreateNaiveChapterBook2()
        {
            var p = new ChapterBook2();
            p.Model = SuouBookPageModel.CreateNaiveModel();
            p.Add(new ChapterBookPage2() { Name = "新しいチャプター" });
            return p;
        }

        public int Number { get; set; }
        public int SectionPageNumber { get; set; }
        
        #region Name
        private static string XMLName = "ChapterBookPage2";
        #endregion
        #region Property
        public SectionNBook Items { get; private set; }
        //private string _Name = "";
        public string Name
        {
            get { return this.Items.Name; }
            set
            {
                if (value != null)
                {
                    this.Items.Name = value;
                    this.LightContentChenged(this, new BookPageContentChangeEventArgs(this, PageContentChangeType.Name));
                }
            }
        }
        [XmlIgnore]
        public SuouText SText
        {
            get
            {

                if (this.Items.Count == 0)
                {
                    return new SuouText();
                }
                else
                {
                    return this.Items.BookPages.First().SText;
                }
            }
            set
            {
            }
        }
        #endregion
        [field: NonSerialized]
        public event BookPageContentChangeEventHandler EditStateChanged;

        #region Method
        public void ResetEvent()
        {
            this.Items.ContentChanged += this.ContentChenged;
            this.Items.ResetEvent();
        }
        private void ContentChenged(object sender, BookContentChangeEventArgs e)
        {
            var v = new BookPageContentChangeEventArgs(this, e);
            if (e.Type == BookContentChangeType.Other || e.Type == BookContentChangeType.Save)
            {
                if (this.EditStateChanged != null) this.EditStateChanged(this, v);
            }
            else
            {
                if (this.EditStateChanged != null) this.EditStateChanged(this, v);
            }
        }
        private void LightContentChenged(object sender, BookPageContentChangeEventArgs e)
        {
            var v = new BookPageContentChangeEventArgs(this, PageContentChangeType.Text);

            if (this.EditStateChanged != null) this.EditStateChanged(this, v);
        }

        public IBookPage IBookPageCopy()
        {
            var v = new ChapterBookPage2();
            v.Items.Clear();
            v.Name = this.Name;
            this.Items.BookPages.ForEach((w) => v.Items.Add(w.IBookPageCopy()));
            return v;
        }
        public KeyValuePair<ChapterBookPage2, ChapterBookPage2> Split(int n)
        {
            var a = new ChapterBookPage2();
            a.Items.Pages.Clear();
            var b = new ChapterBookPage2();
            b.Items.Pages.Clear();
            this.Items.Pages.Take(n).ForEach((v) => a.Items.Pages.Add((SectionPage)v.IBookPageCopy()));
            this.Items.Pages.Skip(n).ForEach((v) => b.Items.Pages.Add((SectionPage)v.IBookPageCopy()));
            a.Name = this.Name;
            b.Name = this.Name;
            return new KeyValuePair<ChapterBookPage2, ChapterBookPage2>(a, b);
        }
        public Size GetLooseSize(int ySpace)
        {
            return this.SText.GetLooseSize(ySpace);
        }
        #endregion
        #region XML

        public XElement CreateSerializer()
        {
            var xe = new XElement(XMLName);
            xe.Add(new XAttribute("Name", this.Name));
            xe.Add(this.Items.CreateSerializer());

            return xe;
        }
        public static ChapterBookPage2 Parse(XElement parser)
        {
            var name = parser.Attribute("Name").Value;
            var book = SectionNBookParser.Parse(parser.Element(SectionNBook.ClassName));

            var p = new ChapterBookPage2() { Name = name };
            p.Items = book;
            p.ResetEvent();
            return p;
            

        }
        public static SuperObservableCollection<ChapterBookPage2> ListParse(XElement xe)
        {
            var g = new SuperObservableCollection<ChapterBookPage2>();
            var f = xe.Elements().Select((v) => ChapterBookPage2.Parse(v)).ToList();
            f.ForEach((v) => g.Add(v));
            return g;
        }
        #endregion

        public string Text
        {
            get
            {
                return this.SText.Text;
            }
        }
    }
}
