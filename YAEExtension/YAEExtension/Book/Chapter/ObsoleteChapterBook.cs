﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using SuouBase.Interface;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Collections.Specialized;
using SuouBase.Book;
using SuouBase.Text;
using SuouBase.Book.Chapter;
namespace SuouBase.Book
{

    [Serializable]
    public class ObsoleteChapterBook:BaseBook<ObsoleteChapterBookPage>,IBook,IParse
    {
        public override ObsoleteChapterBookPage CreateNewBookPage()
        {
            var v = new ObsoleteChapterBookPage();
            v.Items.Model = this.Model;
            return v;
        }
        public ObsoleteChapterBook()
        {
            this.Add(new ObsoleteChapterBookPage() { Name ="新しいチャプター"});
            //this.Pages.CollectionChanged += this.Pages_ContentChanged;
        }
        public ObsoleteChapterBook(IBook book)
        {
            if (book is ObsoleteChapterBook)
            {
                throw new Exception();
            }
            else
            {
                this.Add(new ObsoleteChapterBookPage() { Name = "新しいチャプター" });
                foreach (var page in book.BookPages)
                {
                    this.Pages[0].Items.Add(page);
                }
                this.Name = book.Name;
                this.Pages[0].Name = book.Name;
            }
        }
        /*
        public override void Add(IBookPage p)
        {
            this.Add(p.ToBookGrouperPage());
        }
        public override void Insert(int n, IBookPage page)
        {
            this.Insert(n, page.ToBookGrouperPage());
        }
        */
        #region BaseProperty

        #endregion
        private SuouBookPageModel _Model = new SuouBookPageModel();
        public SuouBookPageModel Model
        {
            get { return this._Model; }
            set
            {
                if (value != null)
                {
                    this._Model = value;
                }
            }
        }

        #region GetProperty

        
        public override IBookPage VirtualSelectedPage
        {
            get
            {
                var page = this.GetBookPage(this.SelectedPageIndex);
                if (page == null) return null;
                return page.Items.VirtualSelectedPage;
            }
        }
        public override IEnumerable<IBookPage> VirtualSelectedGrupePages
        {
            get 
            {
                var page = this.GetBookPage(this.SelectedPageIndex);
                if (page == null) return null;
                return page.Items.OutputPages; 
            }
        }

        private ObsoleteChapterBookPage GetBookPage(int index)
        {
            if (index < 0 || index >= this.Pages.Count)
            {
                return null;
            }
            else
            {
                return this.Pages[index];
            }
        }

        public KeyValuePair<ObsoleteChapterBookPage, int> TranslateSubIndex(int index)
        {
            var n = 0;
            var m = 0;
            ObsoleteChapterBookPage k = null;
            foreach (var b in this.Pages)
            {
                n += b.Items.Count;
                if (n > index)
                {
                    m = n - b.Items.Count;
                    k = b;
                    break;
                }
            }
            return new KeyValuePair<ObsoleteChapterBookPage, int>(k, index - m);

        }
        #endregion
        #region Iterator

        
        
        //public int SelectedSectionIndex { get; set; }
        public override IBookPage this[int i]
        {
            get { return this.Pages[i]; }
            set { this.Pages[i] = value.ToBookGrouperPage(); }
        }
        public override IEnumerable<IBookPage> OutputPages
        {
            get
            {
                foreach (var v in this.Pages)
                {
                    foreach (var w in v.Items.OutputPages)
                    {
                        yield return w;
                    }
                }
            }
        }
        public override IEnumerable<IBookPage> BookPages
        {
            get
            {
                foreach (var v in this.Pages)
                {
                    yield return v;
                }
            }
            set
            {
                var p = new SuperObservableCollection<ObsoleteChapterBookPage>(value.Select((v) => v.ToBookGrouperPage()).ToList());
                this.Pages = p;
                //this.InvokeContentChangeEvent(BookContentChangeEventArgs.Create(this,BookContentChangeType.Page,NotifyCollectionChangedAction.Reset,0));
            }
        }
        public override IEnumerable<IBookPage> DetailBookPages
        {
            get
            {
                return this.Pages.SelectMany((v) => v.Items.DetailBookPages);
            }
        }

        #endregion
        #region Method
        public override void Save(string filename, bool OffIsEdit)
        {
            if (IODirectory.CheckExistDirectory(filename))
            {
                this.SaveXML(filename);
                base.Save(filename, OffIsEdit);
            }
            else
            {
                throw new MyException("");
            }
        }
        
        public void ResetEvent()
        {

            this.Pages.ForEach2((v) => v.EditStateChanged += this.Page_StateChanged).ForEach((v) => v.ResetEvent());
        }
        protected override void Page_StateChanged(object sender, BookPageContentChangeEventArgs e)
        {
            base.Page_StateChanged(sender, e);
        }
        [Obsolete]
        public static ObsoleteChapterBook Load(string filename)
        {
            throw new MyException("");
            /*
            var v = BSystem.XML.BinarySerialize.LoadFromBinaryFile<ChapterBook>(filename);
            if (v == null) return null;
            var r = (ChapterBook)v;
            var f = new FileInfo(filename);
            r.Name = f.Name;
            r.IsEdit = false;
            r.FullFilePath = filename;
            //foreach (var w in r.Pages) w.Refresh();
            r.ResetEvent();
            return r;
            */
        }
        public static ObsoleteChapterBook XMLLoad(string filename)
        {
            var x = XElement.Load(filename,LoadOptions.PreserveWhitespace);
            var r = ObsoleteChapterBook.Parse(x);
            var f = new FileInfo(filename);
            r.Name = f.Name;
            r.IsEdit = false;
            r.FullFilePath = f.FullName;
            return r;
        }
        public ChapterBook2 ToChapterBook2()
        {
            return new ChapterBook2(this);
        }
        #endregion
        #region XML
        public XElement CreateSerializer()
        {
            var xe = new XElement(ClassName);
            xe.Add(new XAttribute("Name", this.Name));
            xe.Add(new XAttribute("Version", 0));
            xe.Add(this.Model.CreateSerializer());
            var items = new XElement("Pages");
            this.Pages.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => items.Add(v));
            xe.Add(items);

            return xe;
        }
        public static ObsoleteChapterBook Parse(XElement parser)
        {
            var version = parser.AttributeIntValue("Version");
            switch (version)
            {
                case 0: return Parse0(parser);
                default: return Parse0(parser);
            }

        }
        private static ObsoleteChapterBook Parse0(XElement parser)
        {
            var name = parser.Attribute("Name").Value;
            var pages = ObsoleteChapterBookPage.ListParse(parser.Element("Pages"));
            var p = new ObsoleteChapterBook() { Name = name, Pages = pages};

            var mk = parser.Element(SuouBookPageModel.ClassName);
            var model = mk == null ? new SuouBookPageModel() : SuouBookPageModel.Parse(mk);
            p.Model = model;
            p.ResetEvent();
            p.IsEdit = false;
            return p;

        }
        private static string ClassName = "ChapterBook";
        #endregion
    }

}
