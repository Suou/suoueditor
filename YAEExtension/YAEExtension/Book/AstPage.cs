﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using SuouBase.Interface;
using System.Threading.Tasks;
using System.Diagnostics;
using SuouBase.Text;
using WebExtension;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Xml.Serialization;
using SuouBase.Book;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
namespace SuouBase.Book
{

    [Serializable()]
    public class AstPage : IBookPage
    {
        public int Number { get; set; }
        public event BookPageContentChangeEventHandler EditStateChanged;
        private string _Name = null;
        public string Name
        {
            get { return this._Name; }
            set
            {
                if (this._Name != value)
                {
                    this._Name = value;
                    this.InvokeContentChangeEvent(PageContentChangeType.Name);
                }
            }
        }
        public SuouText GetSTextWithoutInvokeEvent()
        {
            return this.SText;
        }
        public string Text
        {
            get
            {
                return string.Join(System.Environment.NewLine, this._Text);
            }
        }
        private string[] _Text;
        public int TextRowCount
        {
            get
            {
                return this._Text.Length;
            }
        }


        private void TextClear()
        {
            var f = new List<string>();
            f.Add("");
            ListText = f.ToArray();
        }
        public System.Drawing.Size GetLooseSize(int ySpace)
        {
            int n = this.ListText.Length;
            var height = n * (SuouEditorConst.AAFont.Height + ySpace);
            var width = SuouEditorConst.LooseWidth;
            return new System.Drawing.Size(width, height);
        }
        public SuouText SText
        {
            get
            {

                var text = this._Text.Select((v) => new SuouLine(SuouEditorConst.AAFontChar, SuouEditorConst.TextColorChar, v)).ToArray();
                var r = new SuouText(SuouEditorConst.AAFont, SuouEditorConst.TextColor, text);
                return r;
            }
            set
            {
                var f = new List<string>();
                foreach (var v in value.Lines)
                {
                    f.Add(v.ToString());
                }
                this._Text = f.ToArray();
                this.InvokeContentChangeEvent(PageContentChangeType.Text);
            }
        }

        private void InvokeContentChangeEvent(PageContentChangeType type)
        {
            if (EditStateChanged != null) EditStateChanged(this, new BookPageContentChangeEventArgs(this, type));
        }
        public string[] ListText
        {
            get { return _Text; }
            set
            {
                if (value == null) throw new ArgumentNullException();
                if (value.Length == 0) throw new ArgumentOutOfRangeException();
                _Text = value;
                this.InvokeContentChangeEvent(PageContentChangeType.Text);
            }
        }

        public void RefreshText()
        {
        }
        public AstPage()
        {
            Name = "";
            TextClear();
        }

        public AstPage(string name, string Text)
        {
            Name = name;
            this.ListText = Text.ToParagraphList().ToArray();
        }
        public AstPage(string name, List<string> TextLines)
        {
            Name = name;
            if (TextLines.Count == 0)
            {
                this.TextClear();
            }
            else
            {
                this.ListText = TextLines.ToArray();
            }
        }

        public AstPage(AstPage ap)
        {
            Name = ap.Name;
            this.ListText = ap.ListText;
        }

        public AstPage Copy()
        {
            AstPage ap = new AstPage(this);
            return ap;
        }
        public AstPage DecodeCopy()
        {
            AstPage ap = new AstPage(this.Name,this.Text.DecodeToSjis());            
            return ap;
        }

        public IBookPage IBookPageCopy()
        {
            return this.Copy();
        }

        public string toString()
        {
            return string.Join(System.Environment.NewLine, SText);
        }
        public string toSjisString()
        {
            var s = string.Join(System.Environment.NewLine, SText);
            return HTTPEncode.ToEncodeSJIS10(s);
        }


        public int PageSize
        {
            get
            {
                int n = 0;
                foreach (string s in this.ListText)
                {
                    foreach (char c in s)
                    {
                        n += c.LenB_EUC();
                        if (c == '"')
                        {
                            n += 5;//6
                        }
                        else if (c == '<')
                        {
                            n += 3;//4;
                        }
                        else if (c == '>')
                        {
                            n += 3;//4;
                        }
                        else if (c == '&')
                        {
                            n += 4;//5;
                        }
                    }
                }

                n += (ListText.Length - 1) * 4;
                return n;
            }
        }
        public SectionScene TranslateMergeBookPage()
        {
            var r = new SectionScene(this);

            return r;
        }
        public SectionPage ToSectionPage2(SuouBookPageModel model)
        {
            var r = new SectionPage(this,model);
            
            return r;
        }
        public AstPage[] Split(int limit)
        {
            List<string[]> r = new List<string[]>();
            List<string> p = new List<string>();
            foreach (var v in this._Text)
            {
                if (v.Length == 0 && p.Count == 0) continue;
                if (v.Length == 0)
                {
                    r.Add(p.ToArray());
                    p.Clear();
                }
                else
                {
                    p.Add(v);
                }
            }
            if (p.Count > 0) r.Add(p.ToArray());
            while (r.Count > limit) r.RemoveAt(r.Count - 1);
            return r.Select((w) => new AstPage(this.Name, w.ToList())).ToArray();
        }
        public static explicit operator SectionScene(AstPage page)
        {
            return page.TranslateMergeBookPage();
        }
    }

    public class AstPageSplitter
    {
        List<string> TextList = new List<string>();
        private List<AstPage> Items = new List<AstPage>();
        public string SplitWord = "[SPLIT]";
        public string Name = "";
        public AAFileExtension Type;
        public bool CheckSplit(string s)
        {
            if (s.Length < SplitWord.Length) return false;
            var sSplit = s.Substring(0, SplitWord.Length);
            //this.Name = this.GetName(s);
            return sSplit == SplitWord;
        }
        public string GetName(string s)
        {
            if (this.Type == AAFileExtension.Mlt) return "";
            var sName = s.Substring(SplitWord.Length);
            if (sName.Length >= 2)
            {
                return sName.Substring(1, sName.Length - 2);
            }
            else
            {
                return "";
            }
        }
        public AstPageSplitter(AAFileExtension type)
        {
            this.Type = type;
            if (this.Type == AAFileExtension.Ast)
            {
                this.SplitWord = "[AA]";
            }
            else if (this.Type == AAFileExtension.Mlt)
            {
                this.SplitWord = "[SPLIT]";
            }
        }
        public void Add(string line)
        {
            if (this.CheckSplit(line))
            {
                this.CreateAndAddPage();
                if (this.Type == AAFileExtension.Ast) this.Name = this.GetName(line);
            }
            else
            {
                TextList.Add(line);

            }
        }
        private void CreateAndAddPage()
        {

            var page = new AstPage(Name, TextList);

            this.Items.Add(page);
            TextList = new List<string>();
        }
        public List<AstPage> Finish()
        {
            if (this.TextList.Count > 0)
            {
                this.CreateAndAddPage();
            }
            if (this.Type == AAFileExtension.Ast)
            {
                if (this.Items.Count > 0) this.Items.RemoveAt(0);
                return this.Items;
            }
            else
            {
                return this.Items;
            }
        }

        #region Load
        public static List<AstPage> ReadStoryFile(string filename)
        {
            var f = new FileInfo(filename);
            return ReadStoryFile(f);
        }
        public static List<AstPage> ReadStoryFile(FileInfo FI)
        {
            return ReadStoryFile(FI, false);
        }
        public static List<AstPage> ReadStoryFile(FileInfo FI,bool isDecode)
        {
            List<AstPage> rList;

            if (!FI.Exists)
            {
                ErrorMessage.Show(ErrorMessage.Warning.NotFoundFile, FI.FullName);
                return null;
            }

            switch (FI.Extension)
            {
                case ".ast":
                    rList = isDecode ? ReadDecodeASTFile(FI.FullName) : ReadASTFile(FI.FullName);
                    break;
                case ".mlt":
                    rList = isDecode ? ReadDecodeMLTFile(FI.FullName) : ReadMLTFile(FI.FullName);
                    break;
                default:
                    rList = ReadTXTFile(FI.FullName);
                    break;
            }

            return rList;
        }

        private static List<AstPage> ReadASTFile(string filename)
        {
            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("Shift_JIS"));
            string s;
            var splitter = new AstPageSplitter(AAFileExtension.Ast);
            while ((s = sr.ReadLine()) != null)
            {
                splitter.Add(s);
            }
            sr.Close();
            return splitter.Finish();
        }
        private static List<AstPage> ReadDecodeASTFile(string filename)
        {
            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("Shift_JIS"));
            string s;
            var splitter = new AstPageSplitter(AAFileExtension.Ast);
            while ((s = sr.ReadLine()) != null)
            {
                splitter.Add(s.DecodeToSjis());
            }
            sr.Close();
            return splitter.Finish();
        }
        private static List<AstPage> ReadSlowASTFile(string filename)
        {
            List<AstPage> NowPage = new List<AstPage>();

            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("Shift_JIS"));
            string s = sr.ReadToEnd();
            sr.Close();
            s = HTTPEncode.DecodeToSjis(s);

            s = s.CheckNewLine();
            string[] Separate = new string[] { "[AA]" };
            string[] Separate2 = new string[] { System.Environment.NewLine };
            string[] textarr = s.Split(Separate, StringSplitOptions.RemoveEmptyEntries);

            foreach (string str in textarr)
            {

                var str2 = str.Split(Separate2, StringSplitOptions.None);
                string title = str2.Take(1).ToArray()[0];
                string title2;
                if (title.Length > 2)
                {
                    title2 = title.Substring(1, title.Length - 2);
                }
                else
                {
                    title2 = "";
                }

                string text = String.Join(System.Environment.NewLine, str2.Skip(1).Take(str2.Length - 2));

                AstPage ap = new AstPage(title2, text);
                NowPage.Add(ap);
            }

            return NowPage;

        }
        private static List<AstPage> ReadMLTFile(string filename)
        {
            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("Shift_JIS"));
            string s;
            var splitter = new AstPageSplitter(AAFileExtension.Mlt);
            while ((s = sr.ReadLine()) != null)
            {
                splitter.Add(s);
            }
            sr.Close();
            return splitter.Finish();
        }
        private static List<AstPage> ReadDecodeMLTFile(string filename)
        {
            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("Shift_JIS"));
            string s;
            var splitter = new AstPageSplitter(AAFileExtension.Mlt);
            while ((s = sr.ReadLine()) != null)
            {
                splitter.Add(s.DecodeToSjis());
            }
            sr.Close();
            return splitter.Finish();
        }
        private static List<AstPage> ReadSlowMLTFile(string filename)
        {

            List<AstPage> NowPage = new List<AstPage>();

            string[] Separate2 = new string[] { System.Environment.NewLine };
            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("Shift_JIS"));
            string s = sr.ReadToEnd();

            sr.Close();
            s = HTTPEncode.DecodeToSjis(s);
            s = s.CheckNewLine();

            string[] Separate = new string[] { "[SPLIT]" + System.Environment.NewLine };
            string[] text = s.Split(Separate, StringSplitOptions.RemoveEmptyEntries);

            foreach (string str in text)
            {
                var str2 = (str.Length >= System.Environment.NewLine.Length) ? str.Substring(0, str.Length - System.Environment.NewLine.Length) : str;
                AstPage ap = new AstPage("", str2);
                NowPage.Add(ap);
            }

            return NowPage;

        }

        private static List<AstPage> ReadTXTFile(string filename)
        {
            List<AstPage> NowPage = new List<AstPage>();

            StreamReader sr = new StreamReader(filename, Encoding.GetEncoding("Shift_JIS"));
            string s = sr.ReadToEnd();
            sr.Close();
            s = HTTPEncode.DecodeToSjis(s);

            s = s.CheckNewLine();
            var page = new AstPage("", s);
            NowPage.Add(page);

            return NowPage;

        }
        #endregion
    }
}
