﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using SuouBase.Text;
using BSystem.Collections;
using System.Xml.Serialization;
namespace SuouBase.Book
{

    public abstract class BaseBook<T> : IBook where T : IBookPage
    {
        public abstract T CreateNewBookPage();
        public object Owner { get; set; }
        public IBookPage CreateNewPage()
        {
            return this.CreateNewBookPage();
        }
        #region Property

        public BaseBook()
        {
            this.SetPagesSetting(this.Pages);
            
        }
        public event BookContentChangeEventHandler ContentChanged;
        public event EventHandler SelectedIndexChanged;
        private SuperObservableCollection<T> _Pages = new SuperObservableCollection<T>();
        public SuperObservableCollection<T> Pages
        {
            get { return this._Pages; }
            set {
                this.RemovePagesSetting(this._Pages);
                this.SetPagesSetting(value);
                this.InvokeContentChangeEvent(BookContentChangeEventArgs.Create(this, BookContentChangeType.Page, SuperObservableCollectionAction.Clear, 0));
            }
        }
        [XmlIgnore]
        public string FullFilePath
        {
            get;
            set;
        }
        public bool IsExistFile
        {
            get
            {
                if (this.FullFilePath == null) return false;
                var fi = new FileInfo(this.FullFilePath);
                return fi.Exists;
            }
        }
        private IBookPage _SelectedPage;
        public IBookPage SelectedPage
        {
            get { return this._SelectedPage; }
            set {
                if (value == this._SelectedPage) return;
                this._SelectedPage = value;
                if (this.SelectedIndexChanged != null) this.SelectedIndexChanged(this, new EventArgs());
            }
        }
        //private int _SelectedPageIndex = 0;
        [XmlIgnore]
        public int SelectedPageIndex
        {
            get {
                for (int i = 0; i < this.Count; i++)
                {
                    if (this.Pages[i].Equals(this.SelectedPage)) return i;
                }
                return -1;
            }
            set
            {
                if (this.SelectedPageIndex == value) return;
                if (value == -1)
                {
                    this.SelectedPage = null;
                }
                else
                {
                    this.SelectedPage = this.Pages[value];
                }
            }
        }
        [XmlIgnore]
        public bool IsEdit { get; set; }
        public string Name { get; set; }
        /*
        [XmlIgnore]
        private int _TopNumber = 1;
        public int TopNumber
        {
            get { return this._TopNumber; }
            set
            {
                this._TopNumber = value;
                this.InvokeContentChangeEvent(new BookContentChangeEventArgs(this, BookContentChangeType.TopNumber));
                //this.ReNumber();
            }
        }
        */
        #endregion

        private void RemovePagesSetting(SuperObservableCollection<T> oldItems)
        {
            foreach (var v in oldItems) v.EditStateChanged -= this.Page_StateChanged;
            this._Pages.CollectionChanged -= this.Pages_ContentChanged;
        }
        /*
        private void ReNumber()
        {
            int i = this.TopNumber;
            foreach (var v in this.Pages)
            {
                v.Number = i++;
            }
        }
        */
        protected virtual void SetPagesSetting(SuperObservableCollection<T> newItems)
        {
            foreach (var v in newItems)
            {
                v.EditStateChanged += this.Page_StateChanged;
            }
            this._Pages = newItems;
            this.Pages.CollectionChanged += this.Pages_ContentChanged;
            this.IsEdit = true;
            //this.ReNumber();
        }
        #region IteratorProperty
        public abstract IBookPage this[int i] { get; set; }
        public IEnumerator<IBookPage> GetEnumerator()
        {
            foreach (var v in this.Pages)
            {
                yield return v;
            }
        }
        public abstract IEnumerable<IBookPage> OutputPages { get; }
        public abstract IEnumerable<IBookPage> DetailBookPages { get; }
        public abstract IEnumerable<IBookPage> VirtualSelectedGrupePages { get; }
        public abstract IBookPage VirtualSelectedPage { get; }
        public abstract IEnumerable<IBookPage> BookPages { get; set; }

        public int Count
        {
            get { return this.Pages.Count; }
        }
        public virtual void Save(string filename, bool OfIsEdit)
        {
            if (OfIsEdit)
            {
                IsEdit = false;
                this.InvokeContentChangeEvent(new BookContentChangeEventArgs(this, BookContentChangeType.Save));
            }
        }

        public virtual IEnumerable<IReadOnlyBookPage> ReadOnlyBookPages
        {
            get { return this.BookPages; }
        }
        #endregion
        #region Method
        public void Insert(int n, IBookPage Page)
        {
            if (Page is T)
            {
                this.Insert(n, (T)Page);
            }
            else
            {
                throw new Exception();
            }
        }
        
        protected void Insert(int n, T Page)
        {
            if (Page == null) throw new Exception();
            this.Pages.Insert(n, Page);
        }
        public void ChangeTitle(int n, string title)
        {
            this.Pages[n].Name = title;
            this.IsEdit = true;
        }
        public void Delete(int n)
        {
            this.Pages[n].EditStateChanged -= this.Page_StateChanged;
            this.Pages.RemoveAt(n);
        }
        public void Delete(int[] ns)
        {
            var p = ns.ToList().NewSort((a,b)=>b - a);
            foreach (var n in p)
            {
                this.Delete(n);
            }
        }
        public void Remove(IBookPage item)
        {
            if (item is T)
            {
                T page = (T)item;
                var b = this.Pages.Remove(page);
                if (!b) throw new Exception();
            }
            else
            {
                throw new Exception();
            }
        }

        public void Move(int oldIndex, int newIndex)
        {
            this.Pages.Move(oldIndex, newIndex);
        }
        public void Clear()
        {
            this.Pages = new SuperObservableCollection<T>();
        }
        public void Add(IBookPage p)
        {
            if (p is T)
            {
                this.Add((T)p);
            }
            else
            {
                throw new Exception();
            }
        }
        protected void Add(T p)
        {
            if (p == null) throw new Exception();
            this.Pages.Add(p);
            p.EditStateChanged += this.Page_StateChanged;
        }
        #endregion
        #region Event
        protected void InvokeContentChangeEvent(BookContentChangeEventArgs e)
        {
            if (ContentChanged != null) ContentChanged(this, e);
        }
        protected virtual void Page_StateChanged(object sender, BookPageContentChangeEventArgs e)
        {
            
            IsEdit = true;
            this.InvokeContentChangeEvent(BookContentChangeEventArgs.Create(this,e));
        }
        protected virtual void Pages_ContentChanged(object sender, SuperObservableCollectionEventArgs<T> e)
        {
            this.IsEdit = true;
            /*
            for (int i = 0; i < this.Pages.Count; i++)
            {
                this.Pages[i].Number = i;
            }
            */
            if (e.Action == SuperObservableCollectionAction.Add)
            {
                //((IBookPage)e.NewItems[0]).Number = e.NewStartingIndex;

                var p = BookContentChangeEventArgs.Create(this, e.Action, e.ChangedIndex);
                this.InvokeContentChangeEvent(p);
            }
            else if (e.Action == SuperObservableCollectionAction.Move)
            {
                var p = BookContentChangeEventArgs.Create(this, e.Action, e.MoveToIndex,e.ChangedIndex);
                this.InvokeContentChangeEvent(p);
            }
            else if (e.Action == SuperObservableCollectionAction.Remove)
            {
                if (this.SelectedPageIndex == -1) this.SelectedPageIndex = 0;
                var p = BookContentChangeEventArgs.Create(this, e.Action, e.ChangedIndex);
                this.InvokeContentChangeEvent(p);
            }
            else if (e.Action == SuperObservableCollectionAction.Clear)
            {
                var p = BookContentChangeEventArgs.Create(this, e.Action, 0);
                this.InvokeContentChangeEvent(p);
            }
            else if (e.Action == SuperObservableCollectionAction.Insert)
            {
                var p = BookContentChangeEventArgs.Create(this, e.Action, e.ChangedIndex);
                this.InvokeContentChangeEvent(p);
            }
            else if (e.Action == SuperObservableCollectionAction.Replace)
            {
                var p = BookContentChangeEventArgs.Create(this, e.Action, e.ChangedIndex);
                this.InvokeContentChangeEvent(p);
            }
            else
            {
                throw new Exception("Replace!");
            }


        }
        #endregion
    }
}
