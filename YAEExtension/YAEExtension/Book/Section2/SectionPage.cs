﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using SuouBase.Interface;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Collections.Specialized;
using SuouBase.Book;
using SuouBase.Text;
namespace SuouBase.Book
{
    [Serializable]
    public class SectionPage : IBookPage, IParse, IXmlSerializable
    {
        //public List<SectionBookPage> Items { get; set; }
        /*
        public static ChapterBookPage Merge(ChapterBookPage up, ChapterBookPage down)
        {
            var r = (ChapterBookPage)up.IBookPageCopy();
            down.Items.Pages.ForEach((v) => r.Items.Add(v.Copy()));
            return r;
        }
        */
        public void SetModelToItem(SuouBookPageModel model)
        {
            foreach (SectionScene v in this.Items)
            {
                v.JoinSpaceLine = v.JoinTop ? model.CenterAppendSpaceCount : model.TopAppendSpaceCount;
            }
        }
        public static SectionPage CreateEmpty()
        {
            return new SectionPage();
        }
        public static SectionPage CreateNewPage(SuouBookPageModel model)
        {            
            var v = new SectionPage();
            var fstPage = new SectionScene();
            fstPage.JoinSpaceLine = model.TopAppendSpaceCount;
            v.Items.Add(fstPage);
            return v;
        }
        public SectionPage()
        {            
            this.Items = new SuperObservableCollection<SectionScene>() {};
            this.Items.CollectionChanged += this.Items_CollectionChenged;
            //this.SText = new SuouText();
        }
        public SectionPage(AstPage page,SuouBookPageModel model):this()
        {
            var bp = page.TranslateMergeBookPage();
            bp.JoinSpaceLine =  model == null ? 0 : model.TopAppendSpaceCount;
            this.Items.Clear();
            this.Items.Add(bp);
            this.Name = bp.Name;
        }
        public SectionPage(IBookPageCluster cluster)
            : this()
        {
            var p = cluster.Pages.Cast<SectionScene>();
            p.ForEach((v) => this.Items.Add(v));
        }
        public SectionPage(IEnumerable<SectionScene> items)
            : this()
        {
            items.ForEach((v) => this.Items.Add(v));
        }
        //public int Number { get; set; }
        #region Name
        private static string XMLName = "SectionPage";
        #endregion
        #region Property
        public SuperObservableCollection<SectionScene> Items { get; private set; }
        private string _Name = "";
        public string Name
        {
            get { return this._Name; }
            set
            {
                if (this._Name != value)
                {
                    this._Name = value;
                    this.LightContentChenged(this, new BookPageContentChangeEventArgs(this, PageContentChangeType.Name));
                }
            }
        }
        [XmlIgnore]
        public SuouText SText
        {
            get
            {

                if (this.Items.Count == 0)
                {
                    return new SuouText();
                }
                else
                {
                    return ObsoleteSectionBook.Create(this.Items).SText;
                }
            }
            set
            {
            }
        }
        #endregion
        [field: NonSerialized]
        public event BookPageContentChangeEventHandler EditStateChanged;

        #region Method
        public void ResetEvent()
        {
            
            //this.Items.ContentChanged += this.ContentChenged;
            //this.Items.ResetEvent();
        }
        private void Items_CollectionChenged(object sender, SuperObservableCollectionEventArgs<SectionScene> e)
        {
            if (e.Action == SuperObservableCollectionAction.Add)
            {
                if (this.Items.Count == e.AddedItems.Count)
                {
                    int t = 0;
                    foreach (var v in this.Items)
                    {
                        v.JoinTop = t > 0;
                        t++;
                    }
                }
                else
                {
                    foreach (var v in e.AddedItems) v.JoinTop = true;                    
                }
                foreach (var v in e.AddedItems)
                {
                    v.EditStateChanged += this.ContentChenged;
                }
            }
            else if (e.Action == SuperObservableCollectionAction.Remove)
            {
                foreach (var v in e.RemovedItems)
                {
                    v.EditStateChanged -= this.ContentChenged;
                }
            }
            var p = new BookPageContentChangeEventArgs(this, PageContentChangeType.TextPartsCount);
            if (this.EditStateChanged != null) this.EditStateChanged(this, p);
        }
        
        private void ContentChenged(object sender, BookPageContentChangeEventArgs e)
        {
            switch (e.Type)
            {
                case PageContentChangeType.ChapterPageInnerType:
                    break;
                case PageContentChangeType.JoinSpaceLine:
                    break;
                case PageContentChangeType.JoinTop:
                    break;
                case PageContentChangeType.Name:
                    break;
                case PageContentChangeType.Other:
                    break;
                case PageContentChangeType.Text:
                    break;
                case PageContentChangeType.TextPart:
                    break;
                case PageContentChangeType.TextPartsCount:
                    break;

            }
            if (this.EditStateChanged != null) this.EditStateChanged(this, e);
        }
        
        private void LightContentChenged(object sender, BookPageContentChangeEventArgs e)
        {
            var v = new BookPageContentChangeEventArgs(this, PageContentChangeType.Text);

            if (this.EditStateChanged != null) this.EditStateChanged(this, v);
        }
        

        

        public IBookPage IBookPageCopy()
        {
            var v = new SectionPage();
            v.Items.Clear();
            //v.Number = this.Number;
            v.Name = this.Name;
            this.Items.ForEach((w) => v.Items.Add((SectionScene)w.IBookPageCopy()));
            return v;
        }
        public Size GetLooseSize(int ySpace)
        {
            return this.SText.GetLooseSize(ySpace);
        }
        #endregion
        #region XML

        public XElement CreateSerializer()
        {
            var xe = new XElement(XMLName);
            xe.Add(new XAttribute("Name", this.Name));
            var items = new XElement("Parts");
            this.Items.Select((v) => v.CreateSerializer()).ForEach((v) => items.Add(v));
            xe.Add(items);

            return xe;
            
        }
        public static SectionPage Parse(XElement parser)
        {
            
            var name = parser.Attribute("Name").Value;
            var parts = parser.Element("Parts");
            var listpages = SectionScene.ListParse2(parts);
            var p = new SectionPage() { Name = name };
            p.Items.Clear();
            foreach (var v in listpages) p.Items.Add(v);
            p.ResetEvent();
            return p;
            
        }
        public static SuperObservableCollection<SectionPage> ListParse(XElement xe)
        {
            var g = new SuperObservableCollection<SectionPage>();
            foreach (var v in xe.Elements())
            {
                var p = SectionPage.Parse(v);
                g.Add(p);
            }
            return g;
        }
        #endregion

        public string Text
        {
            get
            {
                return this.SText.Text;
            }
        }

        public static SectionPage Merge(SectionPage item1, SectionPage item2,SuouBookPageModel model)
        {
            var r = new SectionPage();
            r.Items.Clear();
            item1.Items.ForEach((v) => r.Items.Add(v));            
            item2.Items.ForEach((v) => r.Items.Add(v));
            r.Items[item1.Items.Count].JoinSpaceLine = model.CenterAppendSpaceCount;
            return r;
        }
        public AstPage CreateAstPage()
        {
            var r = ObsoleteSectionBook.Create(this.Items);
            return r;
        }
        public KeyValuePair<SectionPage,SectionPage> Split(int i)
        {
            if (i == 0) throw new Exception();
            var fs = this.Items.Take(i).Select((v)=>v.Copy());
            var es = this.Items.Skip(i).Select((v) => v.Copy());
            var r1 = new SectionPage(fs);
            var r2 = new SectionPage(es);
            return new KeyValuePair<SectionPage, SectionPage>(r1, r2);
        }

        public void WriteXml(System.Xml.XmlWriter writer)
        {
            this.CreateSerializer().WriteTo(writer);

        }
        public void ReadXml(System.Xml.XmlReader reader)
        {
            var v = XElement.Load(reader);
            //Parse0(v.Element("SuouText"), this);
        }
        public System.Xml.Schema.XmlSchema GetSchema() { return null; }


        public bool IsChanged
        {
            get { return this.Items.Any((v) => v.IsChanged); }
        }
        public void RefreshText()
        {
            this.Items.ForEach((v) => v.RefreshText());
        }
        public SuouText GetSTextWithoutInvokeEvent()
        {
            return this.SText;
        }
    }
}
