﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using SuouBase.Book;
namespace SuouBase.Book.Section2
{
    class SectionNBookParser
    {
        public static SectionNBook Parse(XElement parser)
        {
            var version = parser.AttributeIntValue("Version");
            switch (version)
            {
                case 0: return ObsoleteSectionBookParser.Parse(parser).ToSectionNBook();
                case 1: return ObsoleteSectionBookParser.Parse(parser).ToSectionNBook();
                case 2: return SectionNBookParser.Parse0(parser);
                default: throw new NotSupportedVertionException();
            }
        }
        private static SectionNBook Parse0(XElement parser)
        {
            if (parser == null) throw new MyException("");
            var nameAtt = parser.Attribute("Name");
            var name = nameAtt == null ? "NoName" : nameAtt.Value;
            var pages = new SuperObservableCollection<SectionPage>(SectionPage.ListParse(parser.Element("Pages")));
            var outline = OutlineParse(parser.Element("Outline"));
            var p = new SectionNBook() { Name = name, Pages = pages,Outline = outline };            
            p.IsEdit = false;
            return p;
        }
        private static string OutlineParse(XElement ele)
        {
            if (ele == null)
            {
                return null;
            }
            else
            {
                var p = ele.Value.Split('\n');
                return string.Join(System.Environment.NewLine, p);
            }
        }
    }
}
