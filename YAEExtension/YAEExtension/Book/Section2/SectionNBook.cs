﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Diagnostics;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using SuouBase.Book;

namespace SuouBase.Book
{
    [Serializable]
    public class SectionNBook:BaseBook<SectionPage>,IBook,IParse
    {
        public void SetModelToItem()
        {
            foreach (SectionPage v in this.BookPages)
            {
                v.SetModelToItem(this.Model);
            }
        }
        public override SectionPage CreateNewBookPage()
        {
            var p = SectionPage.CreateNewPage(this.Model);
            return p;
        }
        /*
        private void ReNumbering()
        {
            var t = this.TopNumber;
            foreach (var v in this.Pages)
            {
                v.Number = t++;
            }
        }
        */
        #region BaseProperty
        private string _Outline = null;
        public string Outline
        {
            get { return this._Outline; }
            set
            {
                this._Outline = value;
                this.IsEdit = true;
                this.InvokeContentChangeEvent(new BookContentChangeEventArgs(this, BookContentChangeType.Other));
            }
        }
        #endregion
        #region Property
        private SuouBookPageModel _Model = new SuouBookPageModel();
        public SuouBookPageModel Model
        {
            get { return this._Model; }
            set
            {
                if (value != null)
                {
                    this._Model = value;
                    this.IsEdit = true;
                }
            }
        }

        public override IEnumerable<IBookPage> OutputPages
        {
            get
            {
                return this.Pages;
            }
        }
        /*
        public IEnumerable<IBookPageCluster> Clusters
        {
            get
            {
                return SectionBook.GetClusters(this.Pages, this.Model);
            }
        }
        */
        public override IEnumerable<IBookPage> BookPages
        {
            get
            {
                return this.Pages;
            }
            set
            {
                if (value != null)
                {
                    var f = value.Select((v) => (SectionPage)v).ToList();
                    this.Pages = new SuperObservableCollection<SectionPage>(f);
                }
            }
        }
        public override IEnumerable<IBookPage> DetailBookPages
        {
            get
            {
                return this.Pages;
            }
        }
        public override IBookPage VirtualSelectedPage
        {
            get
            {
                if (this.VirtualSelectedIndex == -1) return null;
                return this.OutputPages.Skip(this.VirtualSelectedIndex).FirstOrDefault();
            }
        }
        public int VirtualSelectedIndex
        {
            get
            {
                return this.SelectedPageIndex;
            }
        }
        public override IEnumerable<IBookPage> VirtualSelectedGrupePages
        {
            get { return this.OutputPages; }
        }

        #endregion
        #region ShowProperty
        public override IBookPage this[int i]
        {
            get
            {
                return this.Pages[i];
            }
            set
            {
                this.Pages[i] = (SectionPage)value;
            }
        }
        public void ResetEvent()
        {
            /*
            this.Pages = this.Pages;
            this.Pages.ForEach((v) => v.Refresh());
             */
        }
        #endregion
        #region Constructor
        public SectionNBook()
        {
            this.Name = SuouEditorConst.NewFileName(AAFileExtension.Sst);
            this.Pages = new SuperObservableCollection<SectionPage>() { SectionPage.CreateNewPage(this.Model)};
        }
        public SectionNBook(EditingFile ef)
        {
            this.Name = ef.Name;
            var page = (SectionPage)this.CreateNewPage();
            this.Pages = new SuperObservableCollection<SectionPage>() { page};
            foreach (var v in ef.Pages)
            {
                this.Pages.Add(new SectionPage(v,this.Model));
            }
        }
        #endregion
        #region Method
        public override void Save(string filename,bool OffIsEdit) 
        {
            if (IODirectory.CheckExistDirectory(filename))
            {
                this.SaveXML(filename);
                base.Save(filename, OffIsEdit);
            }
            else
            {
                throw new MyException("Error");
            }
        }
        public static SectionNBook XMLLoad(string filename)
        {
            SectionNBook r = null;
            try
            {
                var x = XElement.Load(filename,LoadOptions.PreserveWhitespace);
                r = SuouBase.Book.Section2.SectionNBookParser.Parse(x);
                /*
                var xy = x.Element("Pages");
                r = new SectionNBook();
                r.Pages = SectionPage.ListParse(xy);
                */
                foreach (var w in r.Pages) w.ResetEvent();
            }
            finally
            {
                var f = new FileInfo(filename);
                r.Name = f.Name;
                r.IsEdit = false;
                r.FullFilePath = f.FullName;
            }
            return r;
        }
        #endregion
        #region Other
        public static SectionScene MergePage(SectionScene UpPage, SectionScene DownPage)
        {
            var r = new SectionScene();
            var text = UpPage.SText.Clone();
            for (int i = 0; i < DownPage.JoinSpaceLine; i++) text.Lines.Add(new SuouLine());
            DownPage.SText.Lines.ForEach((v) => text.Lines.Add(v.Clone()));
            
            r.Parts.Clear();
            var c = new SectionScenePart(HukidasiSaveData.NoneDefault(),SpaceLineSet.DefaultSet){SText = text.Clone()};
            r.Parts.Add(c);
            //r.SText = text.Clone();

            r.JoinSpaceLine = UpPage.JoinSpaceLine;
            r.JoinTop = UpPage.JoinTop;
            r.Name = UpPage.Name;

            return r;
        }
        public AstPage[] CreatePages()
        {
            return this.Pages.Select((v) => v.ToAstPage()).ToArray();
        }
        /*
        public static AstPage Create(IEnumerable<SectionBookPage> items)
        {
            if (items.Count() == 0) return new AstPage();
            var name = items.First().Name;
            var tex = items.First().CreateText();

            foreach (var v in items.Skip(1))
            {
                tex %= v.CreateText();
            }
            var w = new AstPage(name,tex.Text);
            return w;
        }
        */
        
        #endregion
        /*
        public override void Add(IBookPage p)
        {
            this.Add(p.ToSectionPage2());
        }
        public override void Insert(int n, IBookPage Page)
        {
            if (Page is SectionPage)
            {
                this.Insert(n, (SectionPage)Page);
            }
            else
            {
                throw new Exception();
            }
            
        }
        */
        #region XML
        public static string ClassName = "SectionBook";
        public XElement CreateSerializer()
        {
            var xe = new XElement(ClassName);
            
            xe.Add(new XAttribute("Name", this.Name));
            xe.Add(this.Model.CreateSerializer());
            xe.Add(new XElement("Outline",this.Outline));
            xe.Add(new XAttribute("Version", 2));
            var items = new XElement("Pages");
            this.Pages.Select((v) => v.CreateSerializer()).ToList().ForEach((v) => items.Add(v));
            xe.Add(items);

            return xe;
        }
        
        #endregion
        
        #region Iterator
        /*
        public static int TranslateVirtualIndex(IEnumerable<SectionScene> ims,int n,SuouBookPageModel model)
        {
            int i = 0;
            var p = ObsoleteSectionBook.GetClusters(ims,model);
            var gp = p.Select(v => v.Pages.Count());
            foreach (var v in gp)
            {
                n -= v;
                if (n < 0) return i;
                i++;
            }
            return i;
        }
        */
        /*
        public static IEnumerable<IBookPageCluster> GetClusters(IEnumerable<SectionScene> ims,SuouBookPageModel model)
        {

            List<SectionScene[]> r = new List<SectionScene[]>();
            var tmp = new List<SectionScene>();
            foreach (var v in ims)
            {
                if (v.JoinTop)
                {
                    tmp.Add(v);
                }
                else
                {
                    if (tmp.Count != 0) r.Add(tmp.ToArray());
                    tmp.Clear();
                    tmp.Add(v);
                }
            }
            r.Add(tmp.ToArray());
            return r.Select((p) => new IBookPageCluster(p,model));
        }
        */
        #endregion

        public static SectionNBook Translate(ObsoleteSectionBook book)
        {
            var r = new SectionNBook();
            r.Clear();
            foreach (var v in book.Clusters)
            {
                var p = new SectionPage(v);
                r.Add(p);
            }
            r.Name = book.Name;
            return r;
            
        }
        public static SectionPage MergePage(SectionPage page)
        {
            var f = new Stack<SectionScene>(page.Items);
            while (f.Count > 1)
            {
                var down = f.Pop();
                var up = f.Pop();
                var now = ObsoleteSectionBook.MergePage(up, down);
                f.Push(now);
            }
            var r = f.Pop();
            var p = (SectionPage)page.IBookPageCopy();
            p.Items.Clear();
            p.Items.Add(r);
            return p;
        }
    }
}
