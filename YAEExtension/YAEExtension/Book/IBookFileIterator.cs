﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using System.Runtime.InteropServices;
using SuouBase.Interface;
using System.Xml;
using BSystem.Collections;
using SuouBase.Text;
namespace SuouBase.Book
{
    public static class IBookFileIterator
    {
        public static IEnumerable<string> AAFileNameIterator(this DirectoryInfo di)
        {
            foreach (var v in BSystem.FolderSuperVisor.FileNameIterator(di))
            {
                var ext = Path.GetExtension(v);
                if (SuouEditorConst.BookFileExtensions.Contains(ext))
                {
                    yield return v;
                }
            }

        }
        public static IEnumerable<TextInfo> GetAATextInfoCollection(this DirectoryInfo di)
        {
            return IBookFileIterator.AAFileNameIterator(di).Select((v) => new FileInfo(v)).Select((v) => new TextInfo(v));
        }
        public static bool IsAAFile(this FileInfo fi)
        {
            return SuouBase.SuouEditorConst.BookFileExtensions.Contains(fi.Extension);
        }

        public static IEnumerable<IBook> AAFileIteratorAsync(this DirectoryInfo di, bool console = false)
        {
            Queue<AsyncBook> Books = new Queue<AsyncBook>();
            Queue<string> pathQueue = new Queue<string>();
            var nameIte = AAFileNameIterator(di);
            nameIte.ForEach((v) => pathQueue.Enqueue(v));

            int total = pathQueue.Count;
            int nowcount = 0;

            do
            {
                pathQueue.MaxDequeue(5 - Books.Count).ForEach((v) => Books.Enqueue(new AsyncBook(v)));
                var book = Books.Dequeue();

                yield return book.Book;
                nowcount++;
            } while (Books.Count != 0);

        }
        public static IEnumerable<IBook> AAFileIterator(this DirectoryInfo di)
        {
            return IBookFileIterator.AAFileNameIterator(di).Select((v) => new FileInfo(v)).Select((v) => IBookHandler.Load(v.FullName));
        }
    }
}
