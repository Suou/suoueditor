﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using SuouBase.Interface;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Drawing;
using SuouBase.Drawing;
using SuouBase.Base;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Xml;
using SuouBase.Text.Search;
using SuouBase.Book;
using SuouBase.Book.Chapter;
using SuouBase.Text;
namespace SuouBase.Book
{

    public static class IBookHandler
    {
        public static bool CanSave(this IBook book)
        {
            if (book.FullFilePath == null) return false;
            return IODirectory.CheckExistDirectory(book.FullFilePath);
        }
        public static bool CanMoveLeftPage(this IBook book, int index)
        {
            return index > 0;
        }
        public static bool CanMoveRightPage(this IBook book, int index)
        {
            return index >= 0 && index < book.BookPages.Count() - 1;
        }
        private static bool IsSuouFile(string filepath)
        {
            var file = new FileInfo(filepath);
            if (!file.Exists) return false;
            var ext = file.Extension;
            if (ext == SuouEditorConst.SectionBookExtension)
            {
                return true;

            }
            else if (ext == SuouEditorConst.NewSectionBookExtension)
            {
                return true;
            }
            else if (ext == SuouEditorConst.ChapterBookExtension)
            {
                return true;
            }
            else if (ext == SuouEditorConst.TestExtension)
            {
                return true;
            }
            else
            {
                return false;

            }
        }
        public static IBook TryLoad(string filepath)
        {
            if (filepath == null) return null;

            /*
            if (IsSuouFile(filepath))
            {
                var res = ChapterBook2.LoadCheck(filepath);
                if (!res)
                {

                    MessageBox.Show($"ファイルに不正な文字が含まれていたので削除しました ({filepath})");
                }
            }
            */


            IBook book = null;
            try
            {
                book = Load(filepath);
            }
            catch(NotSupportedVertionException)
            {
                MessageBox.Show("未対応のバージョンです。最新のソフトを使用してください。");
            }
            return book;
        }
        public static IBook Load(string filePath)
        {
            
            IBook StoryFile;
            var file = new FileInfo(filePath);
            if (!file.Exists) return null;
            var ext = file.Extension;
            if (ext == SuouEditorConst.SectionBookExtension)
            {
                ObsoleteSectionBook book = TimeDebug.Watch2(() => ObsoleteSectionBook.XMLLoad(filePath), "LoadMergeBook/" + file.Name);

                StoryFile = book;

            }
            else if (ext == SuouEditorConst.NewSectionBookExtension)
            {
                StoryFile = SectionNBook.XMLLoad(filePath);
            }
            else if (ext == SuouEditorConst.ChapterBookExtension)
            {
                StoryFile = ChapterBook2.XMLLoad(filePath);
            }
            else if (ext == SuouEditorConst.TestExtension)
            {
                StoryFile = ObsoleteSectionBook.XMLLoad(filePath);
            }
            else
            {
                StoryFile = new EditingFile(filePath);
            }
            return StoryFile;
        }
        public static ChapterBook2 ToChapterBook2(this IBook item)
        {
            if (item is ChapterBook2)
            {
                return (ChapterBook2)item;
            }
            else if (item is ObsoleteChapterBook)
            {
                throw new Exception();
            }
            else
            {
                return new ChapterBook2(item);
            }
        }
        public static EditingFile ToEditingFile(this IBook item)
        {
            if (item is EditingFile)
            {
                return (EditingFile)item;
            }
            else if (item is ObsoleteSectionBook)
            {
                var r = new EditingFile();
                r.Pages.Clear();
                r.Name = item.Name;
                r.SelectedPageIndex = item.SelectedPageIndex;


                var op = ((ObsoleteSectionBook)item).CreatePages();
                op.ForEach((v) => r.Pages.Add(v));

                return r;
            }
            else if (item is ObsoleteChapterBook)
            {
                var r = new EditingFile();
                r.Pages.Clear();
                var p = item.OutputPages.ToList();
                r.Name = item.Name;
                r.SelectedPageIndex = item.SelectedPageIndex;
                p.ForEach((v) => r.Add(v));
                return r;
            }
            return null;
        }
        public static IBookPage FirstFind(this IBook item, string name)
        {
            var p = item.BookPages.FirstOrDefault((v) => v.Name == name);
            return p;
        }
        public static LineMergeCollection CreateLineMergeCollection(this IBook item, ISpaceMaker spacemaker)
        {
            var r = new LineMergeCollection(item);
            item.BookPages.Select((v) => v.SText.ToLineMerge(spacemaker, true)).ToList().ForEach((v) => r.Add(v));
            return r;
        }
        public static ObsoleteSectionBook MergeBookLoad(string filePath)
        {
            ObsoleteSectionBook StoryFile;
            var file = new FileInfo(filePath);
            var ext = file.Extension;
            if (ext == SuouEditorConst.SectionBookExtension)
            {
                StoryFile = ObsoleteSectionBook.XMLLoad(filePath);
            }
            else
            {
                var v = TimeDebug.Watch2(() => new EditingFile(filePath), "CreateEditing");
                StoryFile = TimeDebug.Watch2(() => v.ToMergeBook(), "ToMergeBook");
            }
            return StoryFile;
        }
        public static SectionScene ToMergeBookPage(this IBookPage page)
        {
            if (page is AstPage)
            {
                var ap = (AstPage)page;
                return ap.TranslateMergeBookPage();
            }
            else if (page is SectionScene)
            {
                var mpage = (SectionScene)page;
                return mpage;
            }
            else if (page is SectionPage)
            {
                var sPage = page as SectionPage;
                var astPage = sPage.CreateAstPage();
                return astPage.ToMergeBookPage();
            }
            else
            {
                throw new Exception();
            }
        }
        public static SectionPage ToSectionPage2(this IBookPage page,SuouBookPageModel model)
        {
            if (page is AstPage)
            {
                var ap = (AstPage)page;
                return ap.ToSectionPage2(model);
            }
            else if (page is SectionScene)
            {
                var mpage = (SectionScene)page;
                return mpage.ToSectionPage2(model);
            }
            else if (page is SectionPage)
            {
                var mpage = (SectionPage)page;
                var newmpage = (SectionPage)mpage.IBookPageCopy();
                if(model != null)newmpage.SetModelToItem(model);
                return newmpage;
            }
            return null;
        }

        public static ObsoleteChapterBookPage ToBookGrouperPage(this IBookPage page)
        {
            if (page is ObsoleteChapterBookPage)
            {
                return (ObsoleteChapterBookPage)page;
            }
            else
            {
                throw new Exception();

            }

            //return null;
        }
        public static ChapterBookPage2 ToBookGrouperPage2(this IBookPage page)
        {
            if (page is ChapterBookPage2)
            {
                return (ChapterBookPage2)page;
            }
            else
            {
                return new ChapterBookPage2(page);
            }

            //return null;
        }
        public static ObsoleteChapterBook ToBookGrupe(this ObsoleteSectionBook item)
        {
            var f = new ObsoleteChapterBook();

            List<SectionScene> pages = new List<SectionScene>();
            foreach (var p in item.Pages)
            {
                if (p.Name != null && p.Name.Length > 0)
                {
                    var gp = new ObsoleteChapterBookPage();
                    gp.Items.Clear();
                    pages.ForEach((v) => gp.Items.Pages.Add(v));
                    f.Pages.Add(gp);
                    pages = new List<SectionScene>();
                }
                pages.Add(p);

            }
            var gp2 = new ObsoleteChapterBookPage();
            gp2.Items.Clear();
            pages.ForEach((v) => gp2.Items.Pages.Add(v));
            f.Pages.Add(gp2);
            pages = new List<SectionScene>();

            return f;
        }
        public static IEnumerable<TextThumbImage> TextImagize(this IBook item)
        {
            if (item is ObsoleteChapterBook)
            {
                var v = (ObsoleteChapterBook)item;
                return v.TextImagize2();
            }
            else if (item is EditingFile)
            {
                return null;
            }
            else if (item is ObsoleteSectionBook)
            {
                return null;
            }
            else
            {
                return null;
            }
        }
        public static TextThumbImage TextImagize(this IBookPage item)
        {
            return new TextThumbImage(item, 0);
        }

        private static IEnumerable<TextThumbImage> TextImagize2(this ObsoleteChapterBook item)
        {
            Color PageColor = Color.Aqua;
            List<TextThumbImage> sparray = new List<TextThumbImage>();
            foreach (IBookPage ap in item.BookPages)
            {
                PageColor = PageColor == Color.Aqua ? Color.Beige : Color.Aqua;
                var stt = new TextThumbImage(ap, 0) { ControlBackColor = PageColor };
                sparray.Add(stt);

            }
            return sparray;
        }


        public static AstPage ToAstPage(this IBookPage page)
        {
            if (page is AstPage)
            {
                var ap = (AstPage)page;
                return ap;
            }
            else if (page is SectionScene)
            {
                var v = new AstPage(page.Name, page.SText.Text);
                return v;
            }
            else if (page is SectionPage)
            {
                var v = new AstPage(page.Name, page.SText.Text);
                return v;
            }
            throw new NotSupportedException(page.GetType().ToString());
            //return null;
        }
    }
}
