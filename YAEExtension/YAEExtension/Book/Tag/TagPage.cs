﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace SuouBase.Book.Tag
{
    public class TagPage
    {
        public string FirstCharacterClass { get; set; }
        public int Number { get; set; }
        public bool IsOriginal { get; set; }
        public int OriginalNumber { get; set; }
        public string[] Tags { get; set; }
        public string AuthorName { get; set; }
        public string Trip { get; set; }
        public DateTime RegisterTime { get; set; }
        public string IP { get; set; }
        public string Text { get; set; }
        public string Comment { get; set; }
    }
}
