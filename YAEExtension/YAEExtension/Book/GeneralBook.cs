﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
namespace SuouBase.Text
{
    /*
    class GeneralBook:IBook
    {
        public string Name { get; set; } //Fileの名前
        public string FullFilePath { get; set; }
        public bool IsEdit { get; set; }
        public AAFileExtension Type { get; set; }
        public event BookContentChangeEventHandler ContentChanged;
        public int SelectedPageIndex { get; set; }
        public IBookPage this[int i]
        {
            get
            {
                return this._BookPages[i];
            }
            set
            {
                this._BookPages[i] = value;
            }
        }
        public IEnumerator<IBookPage> GetEnumerator()
        {
            foreach (var page in this.BookPages) yield return page;
        }
        private List<IBookPage> _BookPages;
        public IEnumerable<IBookPage> BookPages
        {
            get
            {
                return this._BookPages;
            }
        }
        private void InvokeContentChangeEvent(BookContentChangeType type)
        {
            if (ContentChanged != null) ContentChanged(this, new BookContentChangeEventArgs(this, type));
        }
        public int Count
        {
            get { return this._BookPages.Count; }
        }
        public void Save(string filename, bool OfIsEdit)
        {
        }
        public IBookPage SelectedPage { get; private set; }

        public void ChangeTitle(int n, string title)
        {
            throw new NotImplementedException();
        }
        public void Delete(int n)
        {
            throw new NotImplementedException();
        }
        public void Insert(int n, IBookPage[] Pages)
        {
            List<IBookPage> r = new List<IBookPage>();
            if (this.Type == AAFileExtension.Ast || this.Type == AAFileExtension.Mlt)
            {
                Pages.ToList().ForEach((v) => r.Add(new AstPage(v.Name,v.Text.Text)));
            }
            else
            {
                Pages.ToList().ForEach((v) => r.Add(new MergeBookPage(v)));
            }
            this._BookPages.InsertRange(n, r.ToArray());
            this.InvokeContentChangeEvent(BookContentChangeType.PageInsert);
        }
        public void Swap(int a, int b)
        {
            throw new NotImplementedException();
        }
        public void Clear()
        {

            this._BookPages.Clear();
            this.IsEdit = true;
        }
        public void Add(IBookPage p)
        {
            this._BookPages.Add(p.IBookPageCopy());
        }

        public GeneralBook(IBook ib)
        {
            ib.BookPages.ToList().ForEach((v) => this.Add(v));
        }
        
    }
    */
}
