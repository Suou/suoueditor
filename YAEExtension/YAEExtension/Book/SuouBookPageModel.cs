﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Collections.ObjectModel;
using System.IO;
using SuouBase.Interface;
using System.Xml.Linq;
using System.Xml.Serialization;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Collections.Specialized;
using SuouBase.Book;
using SuouBase.Text;
namespace SuouBase.Book
{

    [Serializable]
    public class SuouBookPageModel
    {
        public static SuouBookPageModel CreateNaiveModel()
        {
            var p = new SuouBookPageModel();
            p.TopAppendSpaceCount = 0;
            return p;
        }
        private int _CenterAppendSpaceCount = 3;
        private int _TopAppendSpaceCount = 5;
        public int CenterAppendSpaceCount
        {
            get { return _CenterAppendSpaceCount; }
            set { _CenterAppendSpaceCount = value; }
        }
        public int TopAppendSpaceCount
        {
            get { return _TopAppendSpaceCount; }
            set { _TopAppendSpaceCount = value; }
        }
        public int _MaxPageWidth = 1000;
        public int MaxPageWidth
        {
            get { return this._MaxPageWidth; }
            set { this._MaxPageWidth = value; }
        }
        public int _MaxPageCount = 1000;
        public int MaxPageCount
        {
            get { return this._MaxPageCount; }
            set { this._MaxPageCount = value; }
        }
        public int _MaxSize = 4096;
        public int MaxSize
        {
            get { return this._MaxSize; }
            set { this._MaxSize = value; }
        }

        private SpaceLineSet _DefaultMergeSet = SpaceLineSet.DefaultSet;
        public SpaceLineSet DefaultMergeSet
        {
            get { return this._DefaultMergeSet; }
            set
            {

                this._DefaultMergeSet = value;
            }
        }
        private int _CopyEncodeType = (int)EncodeType.SJIS16;
        public EncodeType CopyEncodeType
        {
            get { return (EncodeType)this._CopyEncodeType; }
            set { this._CopyEncodeType = (int)value; }
        }

        public static string ClassName = "SuouBookPageModel";
        public XElement CreateSerializer()
        {
            var xe = new XElement(ClassName);
            xe.Add(new XAttribute("CenterAppendSpaceCount", this.CenterAppendSpaceCount));
            xe.Add(new XAttribute("TopAppendSpaceCount", this.TopAppendSpaceCount));
            xe.Add(new XAttribute("MaxPageWidth", this.MaxPageWidth));
            xe.Add(new XAttribute("MaxPageCount", this.MaxPageCount));
            xe.Add(new XAttribute("MaxSize", this.MaxSize));
            xe.Add(new XAttribute("CopyEncodeType", this._CopyEncodeType));
            xe.Add(new XAttribute("Version", 1));
            xe.Add(this.DefaultMergeSet.CreateSerializer());

            return xe;
        }
        public static SuouBookPageModel Parse(XElement xe)
        {
            var p = new SuouBookPageModel();
            p.CenterAppendSpaceCount = xe.AttributeIntValue("CenterAppendSpaceCount");
            p.TopAppendSpaceCount = xe.AttributeIntValue("TopAppendSpaceCount");
            p.MaxPageCount = xe.AttributeIntValue("MaxPageCount");
            p.MaxPageWidth = xe.AttributeIntValue("MaxPageWidth");
            p.MaxSize = xe.AttributeIntValue("MaxSize");
            p._CopyEncodeType = xe.AttributeIntValue("CopyEncodeType");
            p.DefaultMergeSet = SpaceLineSet.Parse(xe.Element(SpaceLineSet.ParserName));
            return p;
        }
        public bool CheckPage(SectionPage page)
        {
            var b1 = page.SText.GetSitarabaByteSize < this.MaxSize;
            var b2 = page.SText.Count < this.MaxPageCount;
            var b3 = page.SText.Width < this.MaxPageWidth;
            return b1 && b2 && b3;
        }
    }
}
