﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase.Text;
namespace SuouBase.Book
{
    public interface IReadOnlyBookPage
    {
        string Name { get; }
        string Text { get; }

    }

    public interface IReadOnlyBook
    {
        string Name { get; } //Fileの名前
        string FullFilePath { get; }
        int Count { get; }
        IEnumerable<IReadOnlyBookPage> ReadOnlyBookPages { get; }
    }

    public interface IBook:IReadOnlyBook
    {
        new string Name { get; set; } //Fileの名前
        new string FullFilePath { get; set; }
        bool IsEdit { get; set; }
        event BookContentChangeEventHandler ContentChanged;
        int SelectedPageIndex { get; set; }
        IBookPage this[int i] { get; set; }
        IEnumerator<IBookPage> GetEnumerator();
        /// <summary>
        /// 最終的に出力するPagesを返す
        /// </summary>
        IEnumerable<IBookPage> OutputPages { get; }
        /// <summary>
        /// 最も細かいPagesを返す。
        /// </summary>
        IEnumerable<IBookPage> DetailBookPages { get; }
        IEnumerable<IBookPage> VirtualSelectedGrupePages { get; }
        IBookPage VirtualSelectedPage { get; }
        /// <summary>
        /// 生のPagesを返す。
        /// </summary>
        IEnumerable<IBookPage> BookPages { get; set; }

        //new int Count { get; }
        void Save(string filename, bool OfIsEdit);
        IBookPage SelectedPage { get; }
        bool IsExistFile { get; }
        object Owner { get; set; }

        /// <summary>
        /// 引数と実際に追加されるPageは同一であること。
        /// </summary>
        /// <param name="n"></param>
        /// <param name="page"></param>
        void Insert(int n, IBookPage page);
        void ChangeTitle(int n, string title);
        void Delete(int n);
        void Delete(int[] ns);
        void Remove(IBookPage item);
        
        void Move(int a, int b);
        void Clear();
        /// <summary>
        /// 引数と実際に追加されるPageは同一であること。
        /// </summary>
        /// <param name="p"></param>
        void Add(IBookPage p);
        IBookPage CreateNewPage();
    }
    public interface IBookPage:IReadOnlyBookPage
    {
        new string Name { get; set; }
        //int Number { get; set; }

        /// <summary>
        /// STextを取得した時に内部の状態に変更がないことを保証する（非同期用）
        /// </summary>
        void RefreshText();
        SuouText SText { get;  }
        SuouText GetSTextWithoutInvokeEvent();
        event BookPageContentChangeEventHandler EditStateChanged;
        IBookPage IBookPageCopy();
        Size GetLooseSize(int ySpace);
    }
}
