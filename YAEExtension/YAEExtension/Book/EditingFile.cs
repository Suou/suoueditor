﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using SuouBase.Interface;
using System.Threading.Tasks;
using System.Diagnostics;
using SuouBase.Text;
using WebExtension;
using BSystem.Collections;
using BSystem.Text;
using BSystem;
using System.Xml.Serialization;
using SuouBase.Book;
using System.Collections.Specialized;
using System.Collections.ObjectModel;
namespace SuouBase.Book
{
    [Serializable()]
    public class EditingFile:BaseBook<AstPage>, IBook
    {
        public override AstPage CreateNewBookPage()
        {
            return new AstPage();        
        }
        public new IEnumerable<IReadOnlyBookPage> ReadOnlyBookPages => this.BookPages;
        /*
        public override void Add(IBookPage p)
        {
            this.Add(p.ToAstPage());
        }
        public override void Insert(int n, IBookPage Page)
        {
            var astPage = Page.ToAstPage();
            this.Insert(n,astPage);
        }
        */
        #region Property
        /// <summary>
        /// ファイル名
        /// </summary>

        public override IBookPage VirtualSelectedPage
        {
            get
            {
                return this.SelectedPage;
            }
        }
        public override IEnumerable<IBookPage> VirtualSelectedGrupePages
        {
            get { return this.OutputPages; }
        }
        private bool IsBack { get; set; }
        
        public override IEnumerable<IBookPage> OutputPages
        {
            get
            {
                foreach (var v in this)
                {
                    yield return v;
                }
            }
        }
        public void SetPage(List<AstPage> ap)
        {
            this.Pages = new SuperObservableCollection<AstPage>(ap);
            //this.IsEdit = true;
            //this.InvokeContentChangeEvent(BookContentChangeType.PageCount);
        }
        /*
        public override void Clear()
        {
            this.Pages = new ObservableCollection<AstPage>();            
        }
        */
        /*
        public override void Add(IBookPage ibp)
        {
            var p = new AstPage();
            p.Name = ibp.Name;
            p.SText = ibp.SText.Clone();
            this.Pages.Add(p);
        }
        */
        public void AddReadOnly(AstPage ibp)
        {
            var p = new AstPage();
            p.Name = ibp.Name;
            p.SText = ibp.SText;
            this.Pages.Add(p);
        }
        /*
        public override IBookPage SelectedPage
        {
            get
            {
                if (Pages == null) return null;
                if (this.SelectedPageIndex >= 0 && this.SelectedPageIndex < Pages.Count) return Pages[this.SelectedPageIndex];
                return null;
            }
        }
        */
        private bool _IsExist = false;
        public bool IsExist
        {
            get
            {
                if (!_IsExist)
                {
                    if (Pages != null)
                    {
                        _IsExist = true;
                    }
                }

                return _IsExist;
            }
        }
        /*
        private bool _IsEdit;
        public bool IsEdit 
        {
            get { return _IsEdit; }
            set 
            { 
                _IsEdit = value;
                if (value) IsBack = true;
            }
        }
        */

        #endregion
        #region Event
      
        #endregion
        public override IEnumerable<IBookPage> DetailBookPages
        {
            get
            {
                return this.BookPages;
            }
        }
        public static bool CanLoad(string filename)
        {
            var d = new FileInfo(filename);
            return d.Exists;
        }
        
        public override IEnumerable<IBookPage> BookPages
        {
            get
            {
                return this.Pages;
            }
            set
            {
                if (value != null)
                {
                    var f = value.ToList().Select((v) => v.ToAstPage()).ToList();
                    this.SetPage(f);
                }
            }
        }

        #region Initialize
        public EditingFile():this((new Random().Next(int.MaxValue)))
        {
        }
        public EditingFile(int i)
        {
            string s = "NewFile" + i.ToString() + ".ast";
            //FIe = new FileInfo(GeneralMethod.GetCurrentDirectory()+"/" + s);
            var f = new FileInfo(s);
            this.FullFilePath = f.FullName;
            this.Name = s;
            Pages = new SuperObservableCollection<AstPage>();
            this.Add(new AstPage());
            IsEdit = false;

        }
        public EditingFile(IEnumerable<IBookPage> pages):this()
        {
            pages.ForEach((v) => this.Add(v));
        }

        public EditingFile(FileInfo fi)
        {
            this.FullFilePath = fi.FullName;
            this.Name = fi.Name;
            IsEdit = false;
            this.SetPage(AstPageSplitter.ReadStoryFile(this.FullFilePath));
            
        }

        public EditingFile(string filename):this(new FileInfo(filename))
        {
        }
        public EditingFile(string filename,int i)
        {

            IsEdit = false;

            var f = new FileInfo(filename);
            this.Name = f.Name;
            this.FullFilePath = f.FullName;
            //FIe = new FileInfo(filename);
        }
        #endregion

        #region ReadWrite
        public static class ReadFile
        {
            public static EditingFile LoadEditingFile(string filename)
            {
                var f = new EditingFile(filename);
                return f;
            }

            
        }
        public static class WriteFile
        {
            public static void WriteStoryFile(FileInfo FI, List<AstPage> Page)
            {
                StreamWriter sw;
                switch (FI.Extension)
                {
                    case ".ast":
                        sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                        sw.Write(OutPutASTString(Page));
                        sw.Close();
                        break;
                    case ".mlt":
                        sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                        sw.Write(OutPutMLTString(Page));
                        sw.Close();
                        break;
                    default:
                        {
                            if (Page.Count == 1)
                            {
                                sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                                sw.Write(Page[0].toSjisString());
                                sw.Close();
                                break;
                            }
                            else
                            {
                                sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                                sw.Write(OutPutASTString(Page));
                                sw.Close();
                                break;
                            }
                        }
                }
            }
            public static void WriteStoryFile(FileInfo FI, IBook Book)
            {
                StreamWriter sw;
                switch (FI.Extension)
                {
                    case ".ast":
                        sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                        sw.Write(OutPutASTString(Book));
                        sw.Close();
                        break;
                    case ".mlt":
                        sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                        sw.Write(OutPutMLTString(Book));
                        sw.Close();
                        break;
                    default:
                        {
                            if (Book.Count == 1)
                            {
                                sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                                sw.Write(WebExtension.StringExtension.ToSjis10String(Book[0].SText.ToString()));
                                sw.Close();
                                break;
                            }
                            else
                            {
                                sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                                sw.Write(OutPutASTString(Book));
                                sw.Close();
                                break;
                            }
                        }
                }

            }
            private static void WriteStoryFile(FileInfo FI, EditingFile Book)
            {
                StreamWriter sw;
                switch (FI.Extension)
                {
                    case ".ast":
                        sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                        sw.Write(OutPutASTString(Book));
                        sw.Close();
                        break;
                    case ".mlt":
                        sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                        sw.Write(OutPutMLTString(Book));
                        sw.Close();
                        break;
                    default:
                        {
                            if (Book.Count == 1)
                            {
                                sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                                sw.Write(WebExtension.StringExtension.ToSjis10String(Book[0].SText.ToString()));
                                sw.Close();
                                break;
                            }
                            else
                            {
                                sw = new StreamWriter(FI.FullName, false, Encoding.GetEncoding("Shift_JIS"));
                                sw.Write(OutPutASTString(Book));
                                sw.Close();
                                break;
                            }
                        }
                }
            }

            private static string OutPutMLTString(List<AstPage> Page)
            {
                StringBuilder s = new StringBuilder();
                int k = 0;
                while (k < Page.Count)
                {

                    s.Append(Page[k].toSjisString());
                    if (k == Page.Count - 1) break;
                    s.Append(System.Environment.NewLine+ "[SPLIT]"+ System.Environment.NewLine);
                    k++;
                }
                return s.ToString();
            }
            private static string OutPutMLTString(IBook Book)
            {
                StringBuilder s = new StringBuilder();
                int k = 0;
                while (k < Book.Count)
                {

                    s.Append(Book[k].SText.ToString().ToSjis10String());
                    if (k == Book.Count - 1) break;
                    s.Append(System.Environment.NewLine + "[SPLIT]" + System.Environment.NewLine);
                    k++;
                }
                return s.ToString();
            }

            private static string OutPutASTString(List<AstPage> Page)
            {

                StringBuilder s = new StringBuilder();
                int k = 0;
                while (Page.Count >= 1)
                {
                    s.Append("[AA]");
                    s.Append("[");
                    s.Append(Page[k].Name);
                    s.Append("]");
                    s.Append(System.Environment.NewLine);
                    s.Append( Page[k].toSjisString());
                    s.Append(System.Environment.NewLine);
                    if (k == Page.Count - 1) break;
                    k++;
                }
                
                return s.ToString();
            }
            private static string OutPutASTString(IBook Book)
            {

                StringBuilder s = new StringBuilder();
                int k = 0;
                var count = Book.OutputPages.Count();
                var pages = Book.OutputPages.ToList();
                while (count >= 1)
                {
                    s.Append("[AA]");
                    s.Append("[");
                    s.Append(pages[k].Name);
                    s.Append("]");
                    s.Append(System.Environment.NewLine);
                    s.Append(pages[k].SText.ToString().ToSjis10String());
                    s.Append(System.Environment.NewLine);
                    if (k == count - 1) break;
                    k++;
                }

                return s.ToString();
            }
        }
        #endregion
        #region IO
        public override void Save(string FileName,bool OffIsEdit)
        {
            var f = new FileInfo(FileName);
            WriteFile.WriteStoryFile(f, this.Pages.ToList());
            base.Save(FileName, OffIsEdit);
        }
        public void Save(string FileName, bool OffIsEdit,string extension)
        {

            var name = Path.GetFileNameWithoutExtension(FileName);
            var root = Path.GetDirectoryName(FileName);
            if (extension == SuouEditorConst.AstExtension)
            {
                name += ".ast";
            }
            else if(extension == SuouEditorConst.MltExtension)
            {
                name += ".mlt";
            }
            name = root + "/" + name;
            var f = new FileInfo(name);
            WriteFile.WriteStoryFile(f, this.Pages.ToList());
            if (OffIsEdit) IsEdit = false;
        }
        #endregion

        #region PageOperation
        public AstPage[] Cut(int[] n)
        {
            List<AstPage> rList = new List<AstPage>();
            foreach (int i in n)
            {
                rList.Add(Pages[i]);
            }
            Delete(n);
            IsEdit = true;
            return rList.ToArray();
        }
        public AstPage[] Copy(int[] ns)
        {
            List<AstPage> apl = new List<AstPage>();
            foreach (int n in ns)
            {
                apl.Add((AstPage)this.Pages[n].Copy());
            }
            return apl.ToArray();
        }

        public void Paste(int n, AstPage[] argPages)
        {
            if (argPages == null) throw new Exception();
            IsEdit = true;
            for (int i = 0; i < argPages.Length; i++)
                this.Paste(n+i, argPages[i]);
        }
        public void Paste(int n, AstPage argPage)
        {
            if (argPage == null) throw new Exception();
            IsEdit = true;
            Pages.Insert(n, (AstPage)argPage.Copy());
        }

        public void SetText(int n, string str)
        {
            IsEdit = true;
            Pages[n].ListText = str.ToParagraphList().ToArray();
        }

        public void PasteLast(AstPage[] argPages)
        {
            IsEdit = true;
            foreach (AstPage ap in argPages)
                Pages.Add((AstPage)ap.Copy());
        }
        public void PasteLast(AstPage argPage)
        {
            IsEdit = true;
            Pages.Add((AstPage)argPage.Copy());
        }
        public void MoveUp()
        {
            IsEdit = true;
            MoveUp(this.SelectedPageIndex);
        }

        public void MoveUp(int i)
        {
            IsEdit = true;
            if (i != 0)
            {
                AstPage tmp = Pages[i - 1];
                Pages.RemoveAt(i - 1);
                Pages.Insert(i, tmp);
                this.SelectedPageIndex = i - 1;
            }
        }

        public void MoveDown()
        {
            IsEdit = true;
            MoveDown(this.SelectedPageIndex);
        }

        public void MoveDown(int i)
        {

            IsEdit = true;
            if (i < this.Pages.Count - 1)
            {
                AstPage tmp = Pages[i];
                Pages.RemoveAt(i);
                Pages.Insert(i + 1, tmp);
                this.SelectedPageIndex = i + 1;
            }
        }

        //---------------------------------------------------
        #endregion

        public static EditingFile Create(IEnumerable<IBookPage> pages)
        {
            var r = new EditingFile();
            r.Clear();
            pages.ForEach((v) => r.Add(v.ToAstPage()));
            return r;
        }
        public static EditingFile CreateReadOnly(IEnumerable<AstPage> pages)
        {
            var r = new EditingFile();
            r.Clear();
            pages.ForEach((v) => r.Add(v));
            return r;
        }
        
        public override IBookPage this[int i]
        {
            get
            {
                return this.Pages[i];
            }
            set
            {
                this.Pages[i] = (AstPage)value;
            }
        }
        
        public ObsoleteSectionBook ToMergeBook()
        {
            var r = new ObsoleteSectionBook();
            r.FullFilePath = this.FullFilePath;
            r.Name = this.Name;
            r.IsEdit = this.IsEdit;
            r.Pages.Clear();
            this.Pages.ForEach((v) => r.Pages.Add(v.TranslateMergeBookPage()));
            return r;
        }
        public EditingFile CreateSplitFile(int limit)
        {
            var ef = EditingFile.Create(this.Pages.SelectMany((v) => v.Split(limit)));
            ef.Name = this.Name;
            return ef;
        }
    }
}
