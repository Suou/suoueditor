﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
using StoryViewer.BookEditor.Section;
using SuouBase.Text;
using StoryViewer.AASelecter;
using System.Drawing;
using SuouBase.Book;

namespace StoryViewer.OutlineEditor
{
    public class OutlineEditorPlugin :StoryPageListClass,IChapterEditorPlugin
    {
        private static string SettingPath = SuouBase.SuouEditorConst.GetSettingFile("OutlineEditor.stn");
        public OutlineEditor Editor
        {
            get
            {
                return (OutlineEditor)this.Viewer;
            }
        }
        #region Constructor
        public OutlineEditorPlugin(EditorInfo dLLFunction):base(dLLFunction)
        {
            this.Viewer = new OutlineEditor();

            this.Info.StartSettingEnded += this.DLLFunction_StartSettingEnded;
            var p = BSystem.XML.NonBinarySerializer.Load<OutlineEditorSetting>(SettingPath);
            if (p != null) this.Editor.Setting = p;  
        }
        #endregion
        #region Property
        public override SettingPage[] SettingPage { get {
            return new SettingPage[] {  }; } }
        public IBookPage SelectedPage { get; set; }
        public override Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public override string Name
        {
            get
            {
                return "アウトラインエディタ";
            }
        }
        private IBook _Item;
        public override IBook Item
        {
            get
            {
                return this._Item;
            }
            set
            {
                if (this.Book != null) this.Book.SelectedIndexChanged -= this.Book_SelectedIndexChanged;
                this._Item = value;
                if (this.Book != null) this.Book.SelectedIndexChanged += this.Book_SelectedIndexChanged;
                if (value != null)
                {
                    var p = (SuouBase.Book.Chapter.ChapterBook2)this._Item;
                    if (p.SelectedPageIndex == -1) p.SelectedPageIndex = 0;
                    this.FocusSection(p.SelectedPageIndex);
                }
                else
                {
                    this.HaveControl.Book = null;
                }
            }
        }
        public SuouBase.Book.Chapter.ChapterBook2 Book
        {
            get
            {
                return (SuouBase.Book.Chapter.ChapterBook2)this.Item;
            }
        }

        public AstPage[] CopyPages
        {
            get { return null; }
            set { }
        }
        public OutlineEditor HaveControl
        {
            get { return (OutlineEditor)this.Viewer; }
        }
        public override ContainerControl Control
        {
            get { return HaveControl; }
        }
        public override Form Form
        {
            get { return null; }
        }
        public override ToolStripItemCollection ToolStripItems
        {
            get
            {
                return null;
            }
            set
            {
            }
        }

        #endregion
        #region Method
        public void FocusSection(int n)
        {
            if (n != -1)
            {
                this.Book.SelectedPageIndex = n;
                this.HaveControl.StartNumber = this.Book.Pages[n].SectionPageNumber;
                this.HaveControl.Book = this.Book.Pages[n].Items;
            }
            else
            {
                this.Book.SelectedPageIndex = -1;
                this.HaveControl.StartNumber = 0;
                this.HaveControl.Book = null;
            }
        }
        public override void Call()
        {
        }
        private void SetItem()
        {
        }
        public override void FocusPage(int n)
        {
            
        }
        public override bool DisposeCheck()
        {
            BSystem.XML.NonBinarySerializer.Save(SettingPath,this.Editor.Setting);
            return true;
        }
        public override void Paste(IBookPage ap)
        {
            
        }
        public override void CheckToolStripItems()
        {
        }
        private ToolStripMenuItem CreateMenuItem()
        {
            var f = new ToolStripMenuItem() { Text = "ページ" };
            f.DropDownOpening += this.StoryListMenuItem_Opening;
            return f;
        }
        #endregion
        #region Event
        private void DLLFunction_StartSettingEnded(object sender, EventArgs e)
        {
            this.HaveControl.Plugin = (StoryViewer.BookControl.Viewer.SuouViewer.SuouViewerPlugin)this.Info.TemplatePageViewPlugin;
            //this.HaveControl.ParentForm.MainMenuStrip.Items.Insert(1, this.MenuItem);
        }
        private void StoryListMenuItem_Opening(object sender, EventArgs e)
        {
            
            var item = (ToolStripMenuItem)sender;
            //this.HaveControl.st.ItemMenuStrip_Opening(this, null);
            item.DropDownItems.Clear();
            //if(this.HaveControl.RightContextMenuStrip != null)SuouBase.MenuStripMethod.CopyStripList(item.DropDownItems, this.HaveControl.RightContextMenuStrip.Items);

        }
        #endregion

        private void Book_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FocusSection(this.Book.SelectedPageIndex);

        }
    }
    [Serializable]
    public class OutlineEditorSetting
    {
        public OutlineEditorSetting()
        {
        }
        public StoryViewer.ImageViewer.ImageControlOption SizeOption { get; set; }
        public int SplitterValue { get; set; }
        public bool IsHideTextBox { get; set; }
    }
}
