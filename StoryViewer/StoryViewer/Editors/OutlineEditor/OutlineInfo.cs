﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Text;
using System.Drawing;
namespace StoryViewer.OutlineEditor
{
    public class OutlineInfo
    {
        List<OutlineInfoLine> LineItems { get; set; }
        public static IEnumerable<OutlineInfoLine> Parse(string text)
        {
            var r = new List<OutlineInfoLine>();
            var lines = text.LineSprit().Select((v)=>OutlineInfoLine.Parse(v));
            return lines;
        }
    }
    public class OutlineInfoLine:SuouTextBox2.Controls.Ruler.IRulerLine
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string PictureName { get; set; }
        public OutlineInfoLineType Type { get; set; }

        public OutlineInfoLine()
        {
        }
        public string ToSentence()
        {
            if (this.Type == OutlineInfoLineType.Background)
            {
                return string.Format("[{0}]＠{1}", this.PictureName, this.Name);
            }
            else
            {
                return string.Format("[{0}]{1}「{2}」", this.PictureName, this.Name, this.Text);
            }
        }
        public override string ToString()
        {
            return string.Format("[{0} @{1} {2}:{3}]",this.PictureName,this.Type,this.Name,this.Text);
        }
        public Color BackColor
        {
            get
            {
                if (this.PictureName == null)
                {
                    return Color.Red;
                }
                else
                {
                    return Color.LightCoral;
                }
            }
        }
        public Color TextColor
        {
            get
            {
                return Color.Black;
            }
        }

        public static OutlineInfoLine Parse(string s)
        {
            if (s.Length == 0) return new OutlineInfoLine() { Type = OutlineInfoLineType.Unknown };
            var remainText = s;
            string pic = null;
            if (remainText[0] == '[')
            {
                var p = remainText.IndexOf(']');
                if (p != -1)
                {
                    pic = remainText.Substring(1, p - 1);
                    remainText = remainText.Substring(p + 1);
                }
            }

            if (remainText.GetChar(0) == '＠')
            {
                remainText = remainText.Substring(1);
                return new OutlineInfoLine() { Type = OutlineInfoLineType.Background, Name = remainText, Text = null, PictureName = pic };
            }
            else
            {
                var kakkos = remainText.IndexOf('「');
                var kakkoe = remainText.Last() == '」';


                if (kakkoe && kakkos != -1)
                {
                    var name = remainText.Substring(0, kakkos);
                    var text = remainText.Substring(kakkos + 1, remainText.Length - kakkos - 2);
                    return new OutlineInfoLine() { Type = OutlineInfoLineType.Person, Name = name, Text = text, PictureName = pic };
                }
                else
                {
                    return new OutlineInfoLine() { Type = OutlineInfoLineType.Unknown, Text = remainText, PictureName = pic };
                }
            }
        }
    }
    public enum OutlineInfoLineType
    {
        Unknown, Person,Background, Other
    }
    public static class StringExtension
    {
        public static char? GetChar(this string s, int i)
        {
            if (s.Length == 0)
            {
                return null;
            }
            else
            {
                return s[i];
            }
        }
    }
}
