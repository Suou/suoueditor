﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoryViewer.OutlineEditor
{
    public partial class OutlineSuouTextBox : UserControl
    {
        public OutlineSuouTextBox()
        {
            InitializeComponent();
            //this.Outline = new List<OutlineInfoLine>();
        }
        private bool IsEdit { get; set; }
        public event EventHandler CaretMoved;
        [Browsable(true)]
        public new event EventHandler TextChanged;
        public Point CaretPosition
        {
            get
            {
                return new Point(this.suouTextBox1.Caret.StringPos.X, this.suouTextBox1.Caret.StringPos.Y);
            }
        }
        public new string Text
        {
            get
            {
                return this.suouTextBox1.Text;
            }
            set
            {
                this.suouTextBox1.Text = value;
            }
        }
        private OutlineInfoLine[] _Items { get; set; }
        public OutlineInfoLine[] Items
        {
            get
            {
                if (this.IsEdit)
                {
                    this._Items = this.CreateLines();
                    this.IsEdit = false;
                }
                return this._Items;
            }
        }
        private OutlineInfoLine[] CreateLines()
        {
            return OutlineInfo.Parse(this.Text).ToArray();
        }
        public OutlineInfoLine GetPrevNamingLine(int i)
        {
            for (int x = i - 1; x >= 0; x--)
            {
                if (this[x].PictureName != null) return this[x];
            }
            return null;
        }

        public event Action<object, int> OutlineRulerClicked;
        //private List<OutlineInfoLine> _Outline = null;
        /*
        [Browsable(false)]
        [ReadOnly(true)]
        public List<OutlineInfoLine> Outline
        {
            get { return this._Outline; }
            set
            {
                if(value != null)this._Outline = value;
                if (value != null)
                {
                    this.outlineVRuler1.ColorList = value.Cast<SuouTextBox2.Controls.Ruler.IRulerLine>().ToList();
                }
                else
                {
                    this.outlineVRuler1.ColorList = null;
                }
            }
        }
        */
        public OutlineInfoLine this[int i]
        {
            get
            {
                return OutlineInfoLine.Parse(this.suouTextBox1.RawText.Lines[i].Sentence);
            }
            set
            {
                this.suouTextBox1.TextOperator.ReplaceText(i,new SuouBase.Text.SuouLine(value.ToSentence()));
            }
        }

        private void OutlineSuouTextBox_Load(object sender, EventArgs e)
        {

        }

        private void outlineVRuler1_RulerLineClicked(object arg1, int arg2)
        {
            if (arg2 < this.suouTextBox1.RawText.Count)
            {
                if (this.OutlineRulerClicked != null)
                {
                    this.OutlineRulerClicked(arg1, arg2);
                }

            }
        }

        

        private void suouTextBox1_CaretMoved_1(object sender, EventArgs e)
        {
            if (this.CaretMoved != null)
            {
                this.CaretMoved(this, e);
            }
        }

        private void suouTextBox1_TextChanged(object sender, EventArgs e)
        {
            this.IsEdit = true;
            if (this.TextChanged != null)
            {
                this.TextChanged(sender, e);
            }
        }

    }
}
