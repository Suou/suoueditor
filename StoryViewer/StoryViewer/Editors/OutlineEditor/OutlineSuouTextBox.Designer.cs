﻿namespace StoryViewer.OutlineEditor
{
    partial class OutlineSuouTextBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SuouTextBox2.Operator.InversePen inversePen1 = new SuouTextBox2.Operator.InversePen();
            SuouTextBox2.Base.PrimitiveChoice primitiveChoice1 = new SuouTextBox2.Base.PrimitiveChoice();
            SuouBase.Text.InverseDataLapper inverseDataLapper1 = new SuouBase.Text.InverseDataLapper();
            SuouBase.Text.InverseDataCollection inverseDataCollection1 = new SuouBase.Text.InverseDataCollection();
            this.suouTextBox1 = new SuouTextBox2.SuouTextBox();
            this.outlineVRuler1 = new StoryViewer.OutlineEditor.OutlineVRuler();
            this.SuspendLayout();
            // 
            // suouTextBox1
            // 
            this.suouTextBox1.AllowDrop = true;
            this.suouTextBox1.BackColor = System.Drawing.Color.White;
            this.suouTextBox1.CanOriginalDrag = false;
            this.suouTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.suouTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.suouTextBox1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F);
            this.suouTextBox1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.suouTextBox1.IsFixedScroll = false;
            this.suouTextBox1.IsInnerScrollBar = true;
            this.suouTextBox1.IsRuler = false;
            this.suouTextBox1.IsVisibleCaret = true;
            this.suouTextBox1.Location = new System.Drawing.Point(30, 0);
            this.suouTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.suouTextBox1.Name = "suouTextBox1";
            this.suouTextBox1.PaddingSize = new System.Drawing.Size(5, 25);
            inversePen1.ClickButton = System.Windows.Forms.MouseButtons.Left;
            inversePen1.Ctrl = this.suouTextBox1;
            primitiveChoice1.IsContinuity = false;
            inversePen1.ICB = primitiveChoice1;
            inverseDataLapper1.Item = inverseDataCollection1;
            inversePen1.InverseData = inverseDataLapper1;
            this.suouTextBox1.Pen = inversePen1;
            this.suouTextBox1.Size = new System.Drawing.Size(325, 341);
            this.suouTextBox1.TabIndex = 1;
            this.suouTextBox1.UseDebug = false;
            this.suouTextBox1.UserInputEnable = true;
            this.suouTextBox1.CaretMoved += new System.EventHandler(this.suouTextBox1_CaretMoved_1);
            this.suouTextBox1.TextChanged += new System.EventHandler(this.suouTextBox1_TextChanged);
            // 
            // outlineVRuler1
            // 
            this.outlineVRuler1.BackColor = System.Drawing.Color.MistyRose;
            this.outlineVRuler1.ColorList = null;
            this.outlineVRuler1.Dock = System.Windows.Forms.DockStyle.Left;
            this.outlineVRuler1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F);
            this.outlineVRuler1.Location = new System.Drawing.Point(0, 0);
            this.outlineVRuler1.Margin = new System.Windows.Forms.Padding(4);
            this.outlineVRuler1.MaximumSize = new System.Drawing.Size(80, 0);
            this.outlineVRuler1.MinimumSize = new System.Drawing.Size(30, 30);
            this.outlineVRuler1.Name = "outlineVRuler1";
            this.outlineVRuler1.ParentTextBox = this.suouTextBox1;
            this.outlineVRuler1.Pos = 0;
            this.outlineVRuler1.Size = new System.Drawing.Size(30, 341);
            this.outlineVRuler1.TabIndex = 2;
            this.outlineVRuler1.RulerLineClicked += new System.Action<object, int>(this.outlineVRuler1_RulerLineClicked);
            // 
            // OutlineSuouTextBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.suouTextBox1);
            this.Controls.Add(this.outlineVRuler1);
            this.Name = "OutlineSuouTextBox";
            this.Size = new System.Drawing.Size(355, 341);
            this.Load += new System.EventHandler(this.OutlineSuouTextBox_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private SuouTextBox2.SuouTextBox suouTextBox1;
        private OutlineVRuler outlineVRuler1;

    }
}
