﻿namespace StoryViewer.OutlineEditor
{
    partial class OutlineEditor
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.hideEditorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outlineSuouTextBox1 = new StoryViewer.OutlineEditor.OutlineSuouTextBox();
            this.oaaViewer1 = new StoryViewer.OutlineEditor.OAAViewer();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.Color.LightGray;
            this.splitter1.Location = new System.Drawing.Point(434, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(5, 409);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            this.splitter1.SplitterMoving += new System.Windows.Forms.SplitterEventHandler(this.splitter1_SplitterMoving);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.hideEditorToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(137, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // hideEditorToolStripMenuItem
            // 
            this.hideEditorToolStripMenuItem.Name = "hideEditorToolStripMenuItem";
            this.hideEditorToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.hideEditorToolStripMenuItem.Text = "HideEditor";
            this.hideEditorToolStripMenuItem.Click += new System.EventHandler(this.hideEditorToolStripMenuItem_Click);
            // 
            // outlineSuouTextBox1
            // 
            this.outlineSuouTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outlineSuouTextBox1.Location = new System.Drawing.Point(434, 0);
            this.outlineSuouTextBox1.Name = "outlineSuouTextBox1";
            this.outlineSuouTextBox1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.outlineSuouTextBox1.Size = new System.Drawing.Size(260, 409);
            this.outlineSuouTextBox1.TabIndex = 2;
            this.outlineSuouTextBox1.CaretMoved += new System.EventHandler(this.outlineSuouTextBox1_CaretMoved);
            this.outlineSuouTextBox1.TextChanged += new System.EventHandler(this.outlineSuouTextBox1_TextChanged);
            this.outlineSuouTextBox1.OutlineRulerClicked += new System.Action<object, int>(this.outlineSuouTextBox1_OutlineRulerClicked);
            // 
            // oaaViewer1
            // 
            this.oaaViewer1.AutoScroll = true;
            this.oaaViewer1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.oaaViewer1.Book = null;
            this.oaaViewer1.CanEditPage = true;
            this.oaaViewer1.Dock = System.Windows.Forms.DockStyle.Left;
            this.oaaViewer1.IsInnerDragMove = true;
            this.oaaViewer1.IsNotWarningClose = true;
            this.oaaViewer1.IsShowSerihuBox = false;
            this.oaaViewer1.Location = new System.Drawing.Point(0, 0);
            this.oaaViewer1.Name = "oaaViewer1";
            this.oaaViewer1.NotifyPageInfoFunc = null;
            this.oaaViewer1.Size = new System.Drawing.Size(434, 409);
            this.oaaViewer1.TabIndex = 1;
            this.oaaViewer1.TopNumber = 0;
            this.oaaViewer1.SubPictureLeftClicked += new System.Action<object, SuouBase.Book.SectionScene>(this.oaaViewer1_SubPictureLeftClicked);
            this.oaaViewer1.Load += new System.EventHandler(this.oaaViewer1_Load);
            // 
            // OutlineEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.outlineSuouTextBox1);
            this.Controls.Add(this.oaaViewer1);
            this.Name = "OutlineEditor";
            this.Size = new System.Drawing.Size(694, 409);
            this.Load += new System.EventHandler(this.OutlineViewer_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private OAAViewer oaaViewer1;
        private OutlineSuouTextBox outlineSuouTextBox1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem hideEditorToolStripMenuItem;
    }
}
