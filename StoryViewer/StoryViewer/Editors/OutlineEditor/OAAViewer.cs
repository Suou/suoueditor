﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StoryViewer.ImageViewer.AA.Section;
namespace StoryViewer.OutlineEditor
{
    public partial class OAAViewer : StoryViewer.ImageViewer.AA.SectionBookEditor
    {
        public OAAViewer()
        {
            InitializeComponent();
            this.SizeOption = new ImageViewer.ImageControlOption(SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer, 0.5f, new Size(350, 500));
        }

        public static int Compare(SectionPagePicture a, SectionPagePicture b)
        {
            throw new Exception();
        }
        public new void Move(string prevName, string itemName)
        {
            var p1 = this.GetNameIndex(prevName);
            var p2 = this.GetNameIndex(itemName);
            if (p1 != p2 && p1 + 1 != p2)
            {
                this.Items.Move(p2, p1 + 1);
            }
        }
    }
}
