﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Book;
using SuouBase.Plugin;
using SuouBase.Interface;
namespace StoryViewer.OutlineEditor
{
    public partial class OutlineEditor : UserControl, SuouBase.Interface.IStoryViewer
    {
        public OutlineEditor()
        {
            InitializeComponent();
            ToolStripManager.Merge(this.contextMenuStrip1, this.oaaViewer1.ContextMenuStrip);
            this.oaaViewer1.ContextMenuStrip.Opening += this.contextMenuStrip1_Opening;
        }

        public event Action<object, SectionScene> ItemLeftClicked;
        public SmallInfo Info { get; set; }
        private bool IsInitialize { get; set; }
        public StoryViewer.ImageViewer.ImageControlOption Option
        {
            get { return this.oaaViewer1.SizeOption; }
            set { this.oaaViewer1.SizeOption = value; }
        }
        public int StartNumber
        {
            get { return this.oaaViewer1.TopNumber; }
            set { this.oaaViewer1.TopNumber = value; }
        }
        public SectionNBook Book
        {
            get
            {
                return (SectionNBook)this.oaaViewer1.Book;
            }
            set
            {
                this.IsInitialize = true;
                this.oaaViewer1.Book = value;
                if (value != null)
                {
                    this.outlineSuouTextBox1.Text = value.Outline;
                }
                else
                {
                    this.outlineSuouTextBox1.Text = "";
                }
                this.IsInitialize = false;
            }
        }
        private void oaaViewer1_Load(object sender, EventArgs e)
        {
            
        }
        public OutlineEditorSetting Setting
        {
            get { 
                return new OutlineEditorSetting() {
                    SizeOption = this.Option,SplitterValue = this.oaaViewer1.Width, IsHideTextBox = this.IsHideTextBox};
            }
            set
            {
                if (value != null)
                {
                    this.oaaViewer1.SizeOption = value.SizeOption;
                    this.oaaViewer1.Width = value.SplitterValue;
                    this.IsHideTextBox = value.IsHideTextBox;
                }
            }
        }
        private void OutlineViewer_Load(object sender, EventArgs e)
        {

            //this.oaaViewer1.Book = new SuouBase.Book.SectionNBook();
        }
        public StoryViewer.BookControl.Viewer.SuouViewer.SuouViewerPlugin Plugin { get; set; }
        private void outlineSuouTextBox1_OutlineRulerClicked(object arg1, int arg2)
        {
            if (this.Book == null)
            {
                MessageBox.Show(this, "ファイルを開いてから使用してください");
                return;
            }
            if (this.Plugin != null)
            {
                var item = this.outlineSuouTextBox1[arg2];

                var page = this.Plugin.SelectPage(this.ParentForm,item.Name);
                if (page != null)
                {
                    var p = page.ToSectionPage2(this.Book.Model);
                    var name = this.oaaViewer1.CreateNewName();
                    item.PictureName = name;
                    p.Items[0].Name = name;

                    if (item.Text != null)
                    {
                        p.Items[0].AddSerihuPart(item.Text.GetHashCode().ToString(), item.Text);
                    }
                    var cname = this.outlineSuouTextBox1.GetPrevNamingLine(arg2);
                    if (cname != null)
                    {
                        var i = this.oaaViewer1.GetNameIndex(cname.PictureName);
                        if (i != -1)
                        {
                            this.oaaViewer1.Insert(i+1, new IBookPage[] { p });
                        }
                        else
                        {
                            this.oaaViewer1.AddPage(p);
                        }
                    }
                    else
                    {
                        this.oaaViewer1.AddPage(p);
                    }
                    this.outlineSuouTextBox1[arg2] = item;
                }
            }
            /*
            var f = new StoryViewer.BookControl.Viewer.SuouViewer.SuouViewerForm();
            
            f.StartPosition = FormStartPosition.CenterParent;
            var p = f.ShowDialog(this.ParentForm);
            if (p == DialogResult.OK)
            {
                this.oaaViewer1.AddPage(f.SelectedPage);
            }
            */
        }

        private void outlineSuouTextBox1_CaretMoved(object sender, EventArgs e)
        {
            var p = this.outlineSuouTextBox1[this.outlineSuouTextBox1.CaretPosition.Y];
            if (p.PictureName != null)
            {
                this.oaaViewer1.FocusName(p.PictureName.ToString());
            }
        }

        private void outlineSuouTextBox1_TextChanged(object sender, EventArgs e)
        {
            if (this.Book != null && !this.IsInitialize)
            {
                this.Book.Outline = this.outlineSuouTextBox1.Text;
            }
        }

        private void oaaViewer1_SubPictureLeftClicked(object arg1, SectionScene arg2)
        {
            if (this.ItemLeftClicked != null) this.ItemLeftClicked(this, arg2);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.outlineSuouTextBox1.Items == null) return;
            var nameItems = this.outlineSuouTextBox1.Items.Where((v)=>v.PictureName != null).ToArray();
            for (int i = 0; i < nameItems.Length - 1; i++)
            {
                this.oaaViewer1.Move(nameItems[i].PictureName, nameItems[i + 1].PictureName);
            }
        }

        private void hideEditorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.IsHideTextBox = !this.IsHideTextBox;
            Console.WriteLine(this.splitter1.Location);
        }

        private bool _IsHideTextBox = false;
        private bool IsHideTextBox
        {
            get
            {
                return this._IsHideTextBox;
            }
            set
            {
                this._IsHideTextBox = value;
                if (this._IsHideTextBox)
                {
                    this.outlineSuouTextBox1.Visible = false;
                    //this.splitter1.Location = new Point(0, 0);
                    this.splitter1.Visible = false;
                    this.oaaViewer1.Dock = DockStyle.Fill;

                }
                else
                {
                    this.outlineSuouTextBox1.Visible = true;
                    //this.splitter1.Location = new Point(0, 0);
                    this.splitter1.Visible = true;
                    this.oaaViewer1.Dock = DockStyle.Left;
                    this.ShowChecker();
                }
            }

        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (this.IsHideTextBox)
            {
                this.hideEditorToolStripMenuItem.Text = "アウトラインを表示する";
            }
            else
            {
                this.hideEditorToolStripMenuItem.Text = "アウトラインを隠す";
            }
        }


        private int SplitterLimitX
        {
            get
            {
                return this.Size.Width - 200;
            }
        }

        private void splitter1_SplitterMoving(object sender, SplitterEventArgs e)
        {
            if (e.SplitX > this.SplitterLimitX)
            {
                e.SplitX = this.SplitterLimitX;
            }
        }

        private void ShowChecker()
        {
            if (this.splitter1.Location.X > this.SplitterLimitX)
            {
                this.oaaViewer1.Width  = this.SplitterLimitX;
            }
        }
    }
}
