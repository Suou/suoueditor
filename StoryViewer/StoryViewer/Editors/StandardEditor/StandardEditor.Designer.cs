﻿namespace StoryViewer.BookEditor.StandardEditor
{
    partial class StandardEditor
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.storyViewerBox21 = new StoryViewer.ImageViewer.AA.SectionBookEditor();
            this.chapterMenubar1 = new StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.ChapterMenubar();
            this.SuspendLayout();
            // 
            // storyViewerBox21
            // 
            this.storyViewerBox21.AllowDrop = true;
            this.storyViewerBox21.AutoScroll = true;
            this.storyViewerBox21.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.storyViewerBox21.Book = null;
            this.storyViewerBox21.CanEditPage = true;
            this.storyViewerBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.storyViewerBox21.EditorInfo = null;
            this.storyViewerBox21.IsInnerDragMove = true;
            this.storyViewerBox21.IsNotWarningClose = true;
            this.storyViewerBox21.IsShowSerihuBox = true;
            this.storyViewerBox21.Location = new System.Drawing.Point(0, 4);
            this.storyViewerBox21.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.storyViewerBox21.Name = "storyViewerBox21";
            this.storyViewerBox21.NotifyPageInfoFunc = null;
            this.storyViewerBox21.Size = new System.Drawing.Size(491, 445);
            this.storyViewerBox21.TabIndex = 0;
            this.storyViewerBox21.TopNumber = 0;
            this.storyViewerBox21.SubPictureLeftClicked += new System.Action<object, SuouBase.Book.SectionScene>(this.storyViewerBox21_SubPictureLeftClicked);
            this.storyViewerBox21.DragDrop += new System.Windows.Forms.DragEventHandler(this.storyViewerBox21_DragDrop);
            this.storyViewerBox21.DragEnter += new System.Windows.Forms.DragEventHandler(this.storyViewerBox21_DragEnter);
            this.storyViewerBox21.MouseEnter += new System.EventHandler(this.storyViewerBox21_MouseEnter);
            // 
            // chapterMenubar1
            // 
            this.chapterMenubar1.AllowDrop = true;
            this.chapterMenubar1.BackColor = System.Drawing.Color.PaleVioletRed;
            this.chapterMenubar1.Book = null;
            this.chapterMenubar1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.chapterMenubar1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.chapterMenubar1.Location = new System.Drawing.Point(0, 0);
            this.chapterMenubar1.Name = "chapterMenubar1";
            this.chapterMenubar1.RightSelectedIndex = -1;
            this.chapterMenubar1.Size = new System.Drawing.Size(491, 4);
            this.chapterMenubar1.TabIndex = 1;
            this.chapterMenubar1.Text = "chapterMenubar1";
            this.chapterMenubar1.Visible = false;
            this.chapterMenubar1.ItemSelected += new System.Action<SuouBase.Book.SectionNBook, System.EventArgs>(this.chapterMenubar1_ItemSelected);
            // 
            // StandardEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.storyViewerBox21);
            this.Controls.Add(this.chapterMenubar1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "StandardEditor";
            this.Size = new System.Drawing.Size(491, 449);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ImageViewer.AA.SectionBookEditor storyViewerBox21;
        private ChapterViewer.BookGrouperViewerF.ChapterMenubar chapterMenubar1;
    }
}
