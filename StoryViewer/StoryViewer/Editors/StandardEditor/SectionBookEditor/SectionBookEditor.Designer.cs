﻿namespace StoryViewer.ImageViewer.AA
{
    partial class SectionBookEditor
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.Dispose2();
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mergeToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.splitSectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mergeToolStripMenuItem2,
            this.splitSectionToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(241, 52);
            // 
            // mergeToolStripMenuItem2
            // 
            this.mergeToolStripMenuItem2.Name = "mergeToolStripMenuItem2";
            this.mergeToolStripMenuItem2.Size = new System.Drawing.Size(240, 24);
            this.mergeToolStripMenuItem2.Text = "一つのページにする";
            this.mergeToolStripMenuItem2.Click += new System.EventHandler(this.mergeToolStripMenuItem2_Click);
            // 
            // splitSectionToolStripMenuItem
            // 
            this.splitSectionToolStripMenuItem.Name = "splitSectionToolStripMenuItem";
            this.splitSectionToolStripMenuItem.Size = new System.Drawing.Size(240, 24);
            this.splitSectionToolStripMenuItem.Text = "ここからセクションを分離する";
            this.splitSectionToolStripMenuItem.Click += new System.EventHandler(this.splitSectionToolStripMenuItem_Click);
            // 
            // Section2Viewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.Name = "Section2Viewer";
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mergeToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem splitSectionToolStripMenuItem;
    }
}
