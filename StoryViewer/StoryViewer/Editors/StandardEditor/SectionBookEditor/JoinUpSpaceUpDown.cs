﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace StoryViewer.ImageViewer.AA.Section
{
    public partial class JoinUpSpaceUpDown : NumericUpDown
    {
        public JoinUpSpaceUpDown()
        {
            InitializeComponent();
        }
        protected override void OnMouseWheel(MouseEventArgs e)
        {
            //base.OnMouseWheel(e);
        }
        public JoinUpSpaceUpDown(IContainer container)
        {
            container.Add(this);

            InitializeComponent();
        }
        public new int Value
        {
            get { return (int)base.Value; }
            set { base.Value = value; }
        }
    }
}
