﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StoryViewer.ImageViewer.AA;
using SuouBase.Book;
using SuouBase.Interface;
using StoryViewer.ImageViewer.AA.Section;
using SuouBase.Text;
using BSystem.Collections;
namespace StoryViewer.ImageViewer.AA
{
    public partial class SectionBookEditor : AABookViewer
    {
        public SectionBookEditor()
        {
            InitializeComponent();
            
            this.Info.IsLimitedOneArrange = true;
            if (this.SizeOption != null)
            {
                this.SizeOption.Size = new Size(500, 500);
                this.SizeOption.Percent = 0.5f;
                this.SizeOption.Mode = SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer;
            }
            this.OnMouseItemChanged += this.OnMouseItem_Changed;
            this.ItemContextMenuStrip.AllowMerge = true;
            this.contextMenuStrip1.AllowMerge = true;
            ToolStripManager.Merge(this.contextMenuStrip1, this.ItemContextMenuStrip);
            this.CanEditPage = true;
            
            
        }
        public SectionNBook SectionBook
        {
            get { return (SectionNBook)this._Book; }
            
        }
        public SectionPagePicture[] SectionItems
        {
            get
            {
                return this.Items.Cast<SectionPagePicture>().ToArray();
            }
        }
        public string CreateNewName()
        {
            var dic = new HashSet<string>();
            foreach (SectionPagePicture v in this.Items)
            {
                dic.Add(v.Page.Name);
                foreach (var w in v.Page.Items)
                {
                    dic.Add(w.Name);
                }
            }
            int i = 0;
            while (dic.Contains(i.ToString()))
            {
                i++;
            }
            return i.ToString();
        }
        public int GetNameIndex(string name)
        {
            for (int i = 0; i < this.Items.Count; i++)
            {
                var p = (SectionPagePicture)this.Items[i];
                if (p.ContainsName(name)) return i;
            }
            return -1;
        }
        public override bool FocusName(string name)
        {
            var p = this.GetNameIndex(name);
            if (p != -1)
            {
                this.ScrollControlIntoView(this.Items[p]);
                return true;
            }
            else
            {
                return false;
            }
        }
        private SectionPageSubpicture1 _OnMouseSubPicture;
        private SectionPageSubpicture1 OnMouseSubPicture
        {
            get { return this._OnMouseSubPicture; }
            set
            {
                if (this._OnMouseSubPicture == value) return;
                var p = this._OnMouseSubPicture;
                this._OnMouseSubPicture = value;
                if (p != null) p.IsMouseOn = false;
                /*
                if (this._OnMouseSubPicture != null && this.OnMouseItem != null)
                {
                    this.OnMouseItem.IsMouseOn = false;
                }
                */
            }
        }
        public event Action<object, SectionScene> SubPictureLeftClicked;


        public override void Insert(int position, IBookPage[] pages)
        {
            var r1 = this.Create(pages.Select((v) => v.ToSectionPage2(this.SectionBook.Model)));
            this.Items.Insert(position,r1);
        }
        protected override void Add(IBookPage[] pages)
        {
            var r1 = this.Create(pages.Select((v) => v.ToSectionPage2(this.SectionBook.Model)));
            this.Items.AddRange(r1);            
        }
        public void AddPage(IBookPage page)
        {
            this.Add(new[] { page });
        }
        protected IEnumerable<ImageControl> CreateImageControls(SectionNBook book)
        {
            int t = this.TopNumber;
            var conts = book.Pages.Select((v) => new StoryViewer.ImageViewer.ImageSectionAATag(v, book.Model))
                .Select((v) => new StoryViewer.ImageViewer.AA.Section.SectionPagePicture(v, this.WindowInfo, this.SizeOption,t++))
                .ToArray();
            return conts;
        }
        protected override ImageControl[] Create(IEnumerable<IBookPage> pages)
        {
            var p = this.TopNumber;
            var r = pages.Select((v) => this.Create(v)).ToArray();
            foreach (var v in r)
            {
                v.Number = p++;
            }
            return r;
        }
        protected ImageControl Create(IBookPage page)
        {
            var r1 = new StoryViewer.ImageViewer.ImageSectionAATag(page, this.SectionBook.Model);
            var r2 = new StoryViewer.ImageViewer.AA.Section.SectionPagePicture(r1, this.WindowInfo, this.SizeOption,-1);
            return r2;
        }

        protected override void SetEvent(ImageControl c)
        {
            var nc = (SectionPagePicture)c;
            nc.IsShowSerihuBox = this.IsShowSerihuBox;
            nc.Joined += this.SectionPage2Picture_Joined;
            nc.Splited += this.SectionPage2Picture_Splited;
            nc.SubPictureOnMouseChanged += this.SectionPage2Picture_SubPictureOnMouseChanged;
            nc.SubPictureOnMouseChanging += this.SectionPage2Picture_IsOnMouseChanging;
            nc.SubPictureLeftItemClicked += this.SubPicture_LeftClicked;
            base.SetEvent(c);
        }
        protected override void RemoveEvent(ImageControl c)
        {
            var nc = (SectionPagePicture)c;
            nc.Joined -= this.SectionPage2Picture_Joined;
            nc.Splited -= this.SectionPage2Picture_Splited;
            
            nc.SubPictureOnMouseChanged -= this.SectionPage2Picture_SubPictureOnMouseChanged;
            nc.SubPictureOnMouseChanging -= this.SectionPage2Picture_IsOnMouseChanging;
            nc.SubPictureLeftItemClicked -= this.SubPicture_LeftClicked;

            base.RemoveEvent(c);
        }
        private void SubPicture_LeftClicked(object sender, EventArgs e)
        {
            if (this.OnMouseSubPicture != null)
            {
                if (this.EditorSmallInfo != null)
                {
                    this.EditorSmallInfo.Edit(this.OnMouseSubPicture.Page, this.SmallReplace);
                }
                else
                {
                    if (this.SubPictureLeftClicked != null) this.SubPictureLeftClicked(sender, this.OnMouseSubPicture.Page);
                }
            }
        }
        private void OnMouseItem_Changed(object sender, EventArgs e)
        {
            this.OnMouseSubPicture = null;
        }
        private bool _IsShowSerihuBox = true;
        public bool IsShowSerihuBox
        {
            get { return this._IsShowSerihuBox; }
            set
            {
                this._IsShowSerihuBox = value;
                foreach (SectionPagePicture v in this.Items)
                {
                    v.IsShowSerihuBox = value;
                }
            }
        }

        #region SubPicture
        private void SectionPage2Picture_Splited(object sender, SectionSplitEventArgs e)
        {
            var i = this.Items.IndexOf(e.OldItem);
            e.NewItem2.Items[0].JoinSpaceLine = this.SectionBook.Model.TopAppendSpaceCount;

            SuouBase.Api.WINAPI.StopControlDraw(this.Handle);
            this.Items.Remove(e.OldItem);
            this.Insert(i, new[] { e.NewItem1, e.NewItem2 });
            SuouBase.Api.WINAPI.StartControlDraw(this.Handle);
            this.Refresh();
        }
        private void SectionPage2Picture_Joined(object sender, EventArgs e)
        {
            var item = (SectionPagePicture)sender;
            var i = this.Items.IndexOf(item);
            if (i == 0)
            {
            }
            else
            {
                var item2 = (SectionPagePicture)this.Items[i-1];
                var newPage = SectionPage.Merge(item2.Page, item.Page,this.SectionBook.Model);
                SuouBase.Api.WINAPI.StopControlDraw(this.Handle);
                this.Items.Remove(item);
                this.Items.Remove(item2);
                this.Insert(i-1,new[]{newPage});
                SuouBase.Api.WINAPI.StartControlDraw(this.Handle);
                this.Refresh();
            }
        }
        private void SectionPage2Picture_IsOnMouseChanging(object sender, CancelEventArgs e)
        {
            if (this.IsContextOpening)
            {
                e.Cancel = true;
            }
        }
        private void SectionPage2Picture_SubPictureOnMouseChanged(object sender, SectionPageSubpicture1 e)
        {
            if (e.IsMouseOn)
            {
                this.OnMouseSubPicture = e;
            }
            else
            {
                if (this.OnMouseSubPicture == e)
                {
                    this.OnMouseSubPicture = null;
                }
            }
        }
        private void DeleteSubPicture(SectionPageSubpicture1 picture)
        {
            var parent = this.Items.Cast<SectionPagePicture>().First((v) => v.Items.Contains(picture));
            parent.Items.Remove(picture);
            if (parent.Items.Count == 0) this.Items.Remove(parent);
        }
        #endregion
        protected override void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //var page = this.ItemContextMenuStrip.Tag as SectionPagePicture;
            if (this.OnMouseSubPicture != null)
            {
                this.CopySubPicture(this.OnMouseSubPicture.Page, EncodeType.UTF8);
            }
            else
            {
                base.copyToolStripMenuItem_Click(sender, e);
            }
        }
        protected override void copySJIS10ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //var page = this.ItemContextMenuStrip.Tag as SectionPagePicture;
            if (this.OnMouseSubPicture != null)
            {
                this.CopySubPicture(this.OnMouseSubPicture.Page, EncodeType.SJIS10);
            }
            else
            {
                base.copyToolStripMenuItem_Click(sender, e);
            }
        }

        protected override void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.OnMouseSubPicture != null)
            {
                this.CopySubPicture(this.OnMouseSubPicture.Page, EncodeType.UTF8);
                this.DeleteSubPicture(this.OnMouseSubPicture);
            }
            else
            {
                base.cutToolStripMenuItem_Click(sender, e);
            }
        }
        
        private void CopySubPicture(SectionScene page, EncodeType type)
        {
            if (this.EditorSmallInfo != null)
            {
                if (this.OnMouseSubPicture != null)
                {
                    this.EditorSmallInfo.Copy(this.OnMouseSubPicture.Page, type);
                }
                else
                {
                    this.EditorSmallInfo.Copy(page, type);
                }
            }

        }
        protected override void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.OnMouseSubPicture != null)
            {
                this.DeleteSubPicture(this.OnMouseSubPicture);
            }
            else
            {
                base.deleteToolStripMenuItem_Click(sender, e);
            }
        }

        protected override void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.OnMouseSubPicture != null)
            {
                this.OnMouseSubPicture.IsMouseOn = false;
            }
            base.panel1_MouseMove(sender, e);
        }
        public override ImageControl CreateImageControl(IIMageControlTag tag)
        {
            return new SectionPagePicture((ImageSectionAATag)tag,this.WindowInfo,this.SizeOption,-1);
        }
        protected override ImageAATag CreateAATag(IBookPage page)
        {
            return new ImageSectionAATag(page,this.SectionBook.Model);
        }
        protected override void ItemContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (this.OnMouseSubPicture != null)
            {
                this.mergeToolStripMenuItem2.Visible = false;
                //this.splitSectionToolStripMenuItem.Visible = false;
            }
            else
            {
                var p = this.OnMouseItem as SectionPagePicture;
                base.ItemContextMenuStrip_Opening(sender, e);
                if (p != null)
                {
                    this.mergeToolStripMenuItem2.Visible = p.Page.Items.Count > 1;
                    //Console.WriteLine(this.splitSectionToolStripMenuItem.Visible + "/" + (this.EditorInfo == null)+this.EditorInfo.SelectedStoryFile);
                    this.splitSectionToolStripMenuItem.Visible = this.IsChapterBook;
                    //this.splitSectionToolStripMenuItem.Visible = true;
                    //Console.WriteLine(this.splitSectionToolStripMenuItem.Visible + "/" + this.IsChapterBook);
                }
                else
                {
                    this.mergeToolStripMenuItem2.Visible = false;
                    //this.splitSectionToolStripMenuItem.Visible = false;
                }

            }
        }
        private bool IsChapterBook
        {
            get
            {
                if (this.EditorInfo != null)
                {
                    return this.EditorInfo.SelectedStoryFile is SuouBase.Book.Chapter.ChapterBook2;
                }
                return false;
            }
        }

        private void mergeToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            var page = (SectionPagePicture)this.OnMouseItem;
            var newPage = SectionNBook.MergePage(page.Page);
            var index = this.Items.IndexOf(page);
            var newItem = this.Create(newPage);
            this.Items[index] = newItem;
        }
        private void Dispose2()
        {

            this.OnMouseItemChanged -= this.OnMouseItem_Changed;
        }

        /*
        protected override IIMageControlTag GetDropItem(IDataObject obj)
        {
            var page = this.GetDroppedPage(obj);
            if (page == null) return null;
            if (this.Book == null)
            {
                return null;
            }
            else
            {
                
                if (obj.GetDataPresent(typeof(ImageAATag)))
                {
                    var item = (ImageAATag)obj.GetData(typeof(ImageAATag));
                    var page = item.Page.ToSectionPage2(this.SectionBook.Model);
                    return new ImageSectionAATag(page, this.SectionBook.Model);
                }
                else if (obj.GetDataPresent(typeof(ImageSectionAATag)))
                {
                    var item = (ImageSectionAATag)obj.GetData(typeof(ImageSectionAATag));
                    return item.Copy();
                }
                else if (obj.GetDataPresent(typeof(SuouBase.Drawing.TextThumbImage)))
                {
                    var item = (SuouBase.Drawing.TextThumbImage)obj.GetData(typeof(SuouBase.Drawing.TextThumbImage));
                    return new ImageSectionAATag(item.Page.ToSectionPage2(this.SectionBook.Model), this.SectionBook.Model);
                }
                else
                {
                    return null;
                }
                
            }
        }
        */
        protected override IIMageControlTag GetDropIBookPage(IBookPage page)
        {
            return new ImageSectionAATag(page.ToSectionPage2(this.SectionBook.Model), this.SectionBook.Model);
        }
        private void SmallReplace(IBookPage page, IBookPage newPage)
        {
            if (page.SText.Text != newPage.SText.Text)
            {
                var part = (SectionScene)page;
                var rst = (SectionPagePicture)this.Items.FirstOrDefault((v) =>
                {
                    var p = (ImageSectionAATag)v.DrawItem;
                    var p2 = (SectionPage)p.Page;
                    return p2.Items.Contains(page);
                });
                if (rst == null) return;

                var rst2 = rst.Items.FindIndex((v) => v.Page == page);
                rst.Page.Items[rst2] = newPage.ToMergeBookPage();
                rst.Reset();
            }

        }
        public EditorInfo EditorInfo { get; set; }

        private void splitSectionToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var p = this.DragRectangle.InsertPosition;
            if (p == -1) p = 0;
            var book = this.EditorInfo.SelectedStoryFile as SuouBase.Book.Chapter.ChapterBook2;
            var controls = this.Items.Skip(p).Cast<SectionPagePicture>().ToArray();
            var pages = controls.Select((v) => v.Page).ToArray();
            this.Items.RemoveRange(controls);
            
            if (book != null)
            {
                book.Copy(book.SelectedPageIndex, pages);
            }
            
        }
    }
}
