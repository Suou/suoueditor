﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Book;
namespace StoryViewer.ImageViewer.AA
{
    public partial class SectionPageSubpicture1 : AstPagePicture
    {
        public SectionPageSubpicture1()
            : base()
        {
            InitializeComponent();
        }
        public SectionPageSubpicture1(ImageAATag drawTag, ParentControlInfo info, SuouBookPageModel model, ImageControlOption option)
            : base(drawTag, info, option)
        {
            InitializeComponent();
            this.commentBoxList1.InitializeSet(this.Page, this.pictureBox1, model);
            this.UpdateJoinSpaceLine();
            this.label1.Text = this.Page.Name;
            //this.Page.EditStateChanged += this.Page_Changed2;
        }
        private bool _IsShowSerihuBox = true;
        public bool IsShowSerihuBox
        {
            get { return this._IsShowSerihuBox; }
            set
            {
                this._IsShowSerihuBox = value;
                this.addSerihuButton.Visible = value;
                this.commentBoxList1.Visible = value;
            }
        }
        protected override void Page_Changed(object sender, SuouBase.BookPageContentChangeEventArgs e)
        {
            if (e.Type == SuouBase.Text.PageContentChangeType.Name)
            {
                this.label1.Text = this.Page.Name;
            }
            else if (e.Type == SuouBase.Text.PageContentChangeType.TextPartsCount)
            {
                this.commentBoxList1.Reconsutruct();
            }
            else if (e.Type == SuouBase.Text.PageContentChangeType.JoinSpaceLine)
            {
                this.UpdateJoinSpaceLine();
            }
            base.Page_Changed(sender, e);
        }
        private void UpdateJoinSpaceLine()
        {
            if (this.NumericUpDownValue != this.Page.JoinSpaceLine)
            {
                this.numericUpDown1.Value = (int)this.Page.JoinSpaceLine;
            }
        }
        public override bool CanCreateImage
        {
            get
            {
                return this.NeedLoadImage && this.IsHandleCreated;
            }
        }
        public void MyDispose()
        {
            //this.Page.EditStateChanged -= this.Page_Changed2;
        }
        public new SectionScene Page
        {
            get
            {
                return (SectionScene)this.DrawItem.Page;
            }
        }
        public event EventHandler JoinButtonClicked;

        protected override void CreateFunction()
        {
            base.CreateFunction();
            Func<Bitmap> func1 = () =>
            {
                lock (this.DrawItem)
                {
                    var sz = this.pictureBox1.Size;
                    return this.DrawItem.CreateProportional(sz);
                }
            };
            this.CreateBitmapFunc = func1;
        }

        protected override void LoadPicture()
        {
            if (!this.IsHandleCreated) return;
            if (this.DrawItem.IsNeedPreAsyncProcess) this.DrawItem.PreAsyncProcess();

            this.DrawItem.DeterminImageData();

            BSystem.Async<Bitmap>.Run(this.CreateBitmapFunc, this.CompleteRunSetImageAction);

        }


        public override Size OriginalSize
        {
            get
            {
                if (this.IsShowSerihuBox)
                {
                    var h = this.panel1.Height + this.DrawItem.OriginalSize.Height + this.commentBoxList1.Height;
                    var w = new[] { this.panel1.Width, this.DrawItem.OriginalSize.Width, this.commentBoxList1.Width }.Max();
                    return new Size(w, h);
                }
                else
                {
                    var h = this.panel1.Height + this.DrawItem.OriginalSize.Height;
                    var w = new[] { this.panel1.Width, this.DrawItem.OriginalSize.Width}.Max();
                    return new Size(w, h);
                }
            }
        }
        public override void SetModePercent()
        {
            var percent = this.Option.Percent;
            var w = (int)(this.DrawItem.OriginalSize.Width * percent);
            var h = (int)(this.DrawItem.OriginalSize.Height * percent);
            this.pictureBox1.Size = new Size(w, h);
            var sz = RectangleArrange.EasyTopDownArrangeControl(new Control[] { this.panel1, this.pictureBox1, this.commentBoxList1 });
            this.SSize = new Size(sz.Width, sz.Height + 30);

        }
        public override void ClearImage()
        {
            base.ClearImage();
        }
        private void JoinUpButton_Click(object sender, EventArgs e)
        {
            if (this.JoinButtonClicked != null) this.JoinButtonClicked(this, e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.commentBoxList1.AddNewItem();
        }
        /*
        private void commentBoxList1_SizeChanged(object sender, EventArgs e)
        {
            this.ResizeAction();
        }
        */
        public override void SetModeWidthSizeHeigthPer()
        {
            if (this.IsShowSerihuBox)
            {
                var percent = this.Option.Percent;
                var w = this.Option.Size.Width;
                var h = (int)(this.DrawItem.OriginalSize.Height * percent);
                this.pictureBox1.Size = new Size(w, h);
                this.commentBoxList1.Width = w;
                var sz = RectangleArrange.EasyTopDownArrangeControl(new Control[] { this.panel1, this.pictureBox1, this.commentBoxList1 });
                this.SSize = new Size(sz.Width, sz.Height + 30);
            }
            else
            {
                var percent = this.Option.Percent;
                var w = this.Option.Size.Width;
                var h = (int)(this.DrawItem.OriginalSize.Height * percent);
                this.pictureBox1.Size = new Size(w, h);
                this.commentBoxList1.Width = w;
                var sz = RectangleArrange.EasyTopDownArrangeControl(new Control[] { this.panel1, this.pictureBox1});
                this.SSize = new Size(sz.Width, sz.Height + 30);
            }
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
        }

        private int NumericUpDownValue
        {
            get { return (int)this.numericUpDown1.Value; }
        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (this.Page.JoinSpaceLine != this.NumericUpDownValue)
            {
                this.Page.JoinSpaceLine = this.NumericUpDownValue;
            }
        }

        private void commentBoxList1_VisibleChanged(object sender, EventArgs e)
        {
            if (this.commentBoxList1.Visible && !this.IsShowSerihuBox)
            {
                this.commentBoxList1.Visible = false;
            }
        }


    }
}
