﻿namespace StoryViewer.ImageViewer.AA.Section
{
    partial class SectionPagePicture
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.MyDispose();
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SectionPage2Picture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackColor = System.Drawing.SystemColors.Highlight;
            this.MinimumSize = new System.Drawing.Size(50, 50);
            this.Name = "SectionPage2Picture";
            this.Padding = new System.Windows.Forms.Padding(30, 3, 30, 3);
            this.Size = new System.Drawing.Size(60, 46);
            this.SSize = new System.Drawing.Size(64, 50);
            this.IsOnMouseChanged += new System.EventHandler(this.SectionPage2Picture_IsSelectedChanged);
            this.IsInnerChanged += new System.EventHandler(this.SectionPage2Picture_IsInnerChanged);
            this.SizeChanged += new System.EventHandler(this.SectionPage2Picture_SizeChanged);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SectionPage2Picture_Paint);
            this.MouseEnter += new System.EventHandler(this.SectionPage2Picture_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.SectionPage2Picture_MouseLeave);
            this.ResumeLayout(false);

        }

        #endregion

    }
}
