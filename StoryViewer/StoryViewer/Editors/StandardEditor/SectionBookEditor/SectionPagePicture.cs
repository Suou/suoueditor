﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Book;
using BSystem.Collections;
namespace StoryViewer.ImageViewer.AA.Section
{
    public partial class SectionPagePicture : ImageControl
    {
        public SectionPagePicture()
        {
            InitializeComponent();
        }
        protected Color _InvalidColor = Color.Red;
        public SectionPagePicture(ImageSectionAATag drawTag, ParentControlInfo info,ImageControlOption option,int number):base(drawTag,option)
        {            
            InitializeComponent();
            this.Number = number;
            this.Items = new SuperObservableCollection<SectionPageSubpicture1>();
            this.Items.CollectionChanged += this.Items_CollectionChanged;
            this.WindowInfo = info;
            this.Set();
            
            //this.SSize = this.OriginalSize;
        }
        private bool IsSetting { get; set; }
        private bool _IsShowSerihuBox = true;
        public bool IsShowSerihuBox
        {
            get { return this._IsShowSerihuBox; }
            set
            {
                this._IsShowSerihuBox = value;
                foreach (var v in this.Items)
                {
                    v.IsShowSerihuBox = value;
                }
            }
        }
        public bool ContainsName(string name)
        {
            if (this.Page.Name == name)
            {
                return true;
            }
            else
            {
                var p = this.Page.Items.FirstOrDefault((v) => v.Name == name);
                return p != null;
            }
        }
        public override int Number
        {
            get
            {
                return base.Number;
            }
            set
            {
                if (base.Number != value)
                {
                    base.Number = value;
                    if (this.Items != null) this.Items.ForEach((v) => v.Number = value);
                    this.BackColor = this.NonSelectedColor;
                    if(this.Items != null)this.Items.ForEach((v) => v.BackColor = v.NonSelectedColor);
                }
            }
        }
        public override Color NonSelectedColor
        {
            get
            {
                if (this.DrawItem.Model.CheckPage(this.Page))
                {
                    return base.NonSelectedColor;
                }
                else
                {
                    return this._InvalidColor;
                }
            }
        }
        public override bool CanCreateImage
        {
            get
            {
                return this.IsInner && this.Items.Any((v)=>v.CanCreateImage);
            }
        }
        public override void LoadingPicture()
        {
            this.Items.Where((v)=>v.CanCreateImage).ForEach((v) => v.LoadingPicture());
        }
        public SuperObservableCollection<SectionPageSubpicture1> Items { get; private set; }
        private void ClearItems(IEnumerable<SectionPageSubpicture1> items)
        {
            foreach (var v in items)
            {
                this.RemoveEvent(v);
                this.Controls.Remove(v);
            }
        }
        private void Items_CollectionChanged(object sender, SuperObservableCollectionEventArgs<SectionPageSubpicture1> e)
        {
            if (e.Action == SuperObservableCollectionAction.Add)
            {
                foreach (var v in e.AddedItems)
                {
                    this.SetEvent(v);
                    this.Controls.Add(v);
                    if(!this.IsSetting)this.Page.Items.Add(v.Page);
                    v.BringToFront();
                }
            }
            else if (e.Action == SuperObservableCollectionAction.Clear)
            {
                this.ClearItems(e.RemovedItems);
                foreach (var v in e.RemovedItems)
                {
                    if (!this.IsSetting) this.Page.Items.Remove(v.Page);
                }
            }
            else if (e.Action == SuperObservableCollectionAction.Insert)
            {
                throw new NotSupportedException();
            }
            else if (e.Action == SuperObservableCollectionAction.Move)
            {
                throw new NotSupportedException();
            }
            else if (e.Action == SuperObservableCollectionAction.Remove)
            {
                this.ClearItems(e.RemovedItems);
                foreach (var v in e.RemovedItems)
                {
                    if (!this.IsSetting) this.Page.Items.Remove(v.Page);
                }
            }
            else if (e.Action == SuperObservableCollectionAction.Replace)
            {
                throw new Exception();
                /*
                this.ClearItems();
                this.Set();
                this.Page.Items[e.ChangedIndex] = e.AddedItems.First().Page;
                */
            }
            else
            {
                throw new NotSupportedException();
            }

        }
        private void Set()
        {
            this.IsSetting = true;
            foreach (var v in this.Page.Items)
            {
                var tag = new ImageAATag(v);
                var cont = new SectionPageSubpicture1(tag,this.WindowInfo,this.DrawItem.Model,this.Option);
                cont.Dock = DockStyle.Top;
                this.Items.Add(cont);
                //this.RelocationAndSizeItems();
            }
            this.IsSetting = false;
        }
        public void Reset()
        {
            this.IsSetting = true;
            this.Items.Clear();
            this.Set();
            this.IsSetting = false;
            this.SetControlSize();
        }
        public event EventHandler Joined;
        public event Action<object,SectionSplitEventArgs> Splited;
        public event Action<object, SectionPageSubpicture1> SubPictureOnMouseChanged;
        public event CancelEventHandler SubPictureOnMouseChanging;
        public event EventHandler SubPictureLeftItemClicked;

        public override bool NeedLoadImage
        {
            get
            {
                return this.Items.Any((v) => v.NeedLoadImage);
            }
        }
        public void SubPageLeftClicked(object sender, EventArgs e)
        {
            if (this.SubPictureLeftItemClicked != null) this.SubPictureLeftItemClicked(this, new EventArgs());
            //this.OnItemLeftClick();
        }

        private void SetEvent(SectionPageSubpicture1 control)
        {
            control.JoinButtonClicked += this.SectionPagePicture_JoinButtonClicked;
            control.IsOnMouseChanged += this.SectionPagePicture_IsOnMouseChanged;
            control.SizeChanged += this.SectionPagePicture_SizeChanged;
            control.IsOnMouseChanging += this.SectionPagePicture_IsSelectedChanging;
            control.ItemLeftClicked += this.SubPageLeftClicked;
        }
        private void RemoveEvent(SectionPageSubpicture1 control)
        {
            control.JoinButtonClicked -= this.SectionPagePicture_JoinButtonClicked;
            control.IsOnMouseChanged -= this.SectionPagePicture_IsOnMouseChanged;
            control.SizeChanged -= this.SectionPagePicture_SizeChanged;
            control.IsOnMouseChanging -= this.SectionPagePicture_IsSelectedChanging;
            control.ItemLeftClicked -= this.SubPageLeftClicked;
        }
        private void SectionPagePicture_IsSelectedChanging(object sender,CancelEventArgs e)
        {
            if (this.SubPictureOnMouseChanging != null)
            {
                this.SubPictureOnMouseChanging(this, e);
            }
        }
        private void SectionPagePicture_JoinButtonClicked(object sender, EventArgs e)
        {
            var pic = (SectionPageSubpicture1)sender;
            var i = this.Items.IndexOf(pic);
            if (i == 0)
            {
                if (this.Joined != null) this.Joined(this, e);
            }
            else
            {
                var f = this.Page.Split(i);
                var ne = new SectionSplitEventArgs(this, f.Key, f.Value);
                if (this.Splited != null) this.Splited(this, ne);
            }
        }
        private void SectionPagePicture_SizeChanged(object sender, EventArgs e)
        {
            //this.RelocationAndSizeItems();
        }
        /*
        private void RelocationAndSizeItems()
        {
            this.SSize = RectangleArrange.EasyTopDownArrangeControl(this.Items);
        }
        */
        public new ImageSectionAATag DrawItem
        {
            get
            {
                return (ImageSectionAATag)base.DrawItem;
            }
        }
        public SectionPage Page
        {
            get
            {
                return (SectionPage)this.DrawItem.Page;
            }
        }
        public override Size OriginalSize
        {
            get
            {
                var w = this.Items.Max((v) => v.OriginalSize.Width);
                var h = this.Items.Sum((v) => v.OriginalSize.Height);
                return new Size(w, h);
            }
        }
        
        protected override void ChangeReverseImage()
        {
            base.ChangeReverseImage();
            this.Items.ForEach((v) => v.BackColor = v.SelectedColor);
            //this.Items.ForEach((v)=>v.changere)
        }
        protected override void ChangeNormalImage()
        {
            base.ChangeNormalImage();
            this.Items.Where((v)=>!v.IsSelected).ForEach((v) => v.BackColor = v.NonSelectedColor);
            /*
            if (this.IsLoadPicture)
            {
                this.pictureBox1.Image = this.NormalImage;
            }
            */
        }
        protected override void ChangeDragImage()
        {
            this.Items.ForEach((v) => v.BackColor = v.DraggedColor);
            base.ChangeDragImage();
        }
        
        public override void ClearImage()
        {
            this.Items.ForEach((v) => v.ClearImage());
        }

        private void SectionPage2Picture_IsInnerChanged(object sender, EventArgs e)
        {
            this.Items.ForEach((v) => v.IsInner = this.IsInner);
        }
        public override void SetModePercent()
        {
            
            var percent = this.Option.Percent;
            
            this.Items.ForEach((v) => v.Option.Percent = percent);
            Console.WriteLine("change" + this.GetHashCode());
            this.Items.ForEach((v) => v.SetModePercent());
           // this.re();
            
        }
        public override void SetModeWidthSizeHeigthPer()
        {
            var w = this.Option.Size.Width + this.Padding.Left + this.Padding.Right;
            this.MinimumSize = new System.Drawing.Size(w,this.MinimumSize.Height);
            this.Width = w;
            this.Items.ForEach((v) => v.SetControlSize());
            //this.RelocationAndSizeItems();
        }

        private void SectionPage2Picture_IsSelectedChanged(object sender, EventArgs e)
        {
            //this.BackColor = this.IsSelected ? Color.Pink : Color.Plum;
            //this.Items.ForEach((v) => v.IsSelected = this.IsSelected);
        }
        private void SectionPagePicture_IsOnMouseChanged(object sender, EventArgs e)
        {
            var item = (SectionPageSubpicture1)sender;
            if (this.SubPictureOnMouseChanged != null) this.SubPictureOnMouseChanged(this, item);
        }

        private void SectionPage2Picture_SizeChanged(object sender, EventArgs e)
        {
            this.SSize = this.Size;
        }
        private void SectionPage2Picture_MouseEnter(object sender, EventArgs e)
        {
            //this = true;
        }

        private void SectionPage2Picture_MouseLeave(object sender, EventArgs e)
        {
            //this.IsSelected = false;
        }
        private void MyDispose()
        {
            this.IsSetting = true;
            this.Items.Clear();
            this.IsSetting = false;
        }

        private void DrawingInfo(Graphics g)
        {
            if (this.Page != null)
            {
                var p = this.Number;
                g.DrawString(p.ToString(), this.Font, Brushes.Black, new Point(10, 10));
            }
        }
        private void SectionPage2Picture_Paint(object sender, PaintEventArgs e)
        {
            this.DrawingInfo(e.Graphics);
        }
    }
}
