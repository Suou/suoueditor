﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Book;
namespace StoryViewer.ImageViewer.AA.Section
{
    public class SectionSplitEventArgs
    {
        public SectionPagePicture OldItem { get; private set; }
        public SectionPage NewItem1 { get; private set; }
        public SectionPage NewItem2 { get; private set; }
        public SectionSplitEventArgs(SectionPagePicture old, SectionPage new1, SectionPage new2)
        {
            this.OldItem = old;
            this.NewItem1 = new1;
            this.NewItem2 = new2;
        }
    }
}
