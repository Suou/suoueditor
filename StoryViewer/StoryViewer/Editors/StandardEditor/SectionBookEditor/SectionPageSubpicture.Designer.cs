﻿namespace StoryViewer.ImageViewer.AA
{
    partial class SectionPageSubpicture1
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.MyDispose();
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.addSerihuButton = new System.Windows.Forms.Button();
            this.JoinUpButton = new System.Windows.Forms.Button();
            this.numericUpDown1 = new StoryViewer.ImageViewer.AA.Section.JoinUpSpaceUpDown(this.components);
            this.commentBoxList1 = new StoryViewer.ImageViewer.AA.Section.CommentBoxList();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 25);
            this.pictureBox1.Size = new System.Drawing.Size(392, 155);
            this.pictureBox1.MouseLeave += new System.EventHandler(this.pictureBox1_MouseLeave);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.addSerihuButton);
            this.panel1.Controls.Add(this.JoinUpButton);
            this.panel1.Controls.Add(this.numericUpDown1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(392, 25);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(100, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "label1";
            // 
            // addSerihuButton
            // 
            this.addSerihuButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.addSerihuButton.Location = new System.Drawing.Point(0, 0);
            this.addSerihuButton.Margin = new System.Windows.Forms.Padding(4);
            this.addSerihuButton.Name = "addSerihuButton";
            this.addSerihuButton.Size = new System.Drawing.Size(100, 25);
            this.addSerihuButton.TabIndex = 5;
            this.addSerihuButton.Text = "セリフ追加";
            this.addSerihuButton.UseVisualStyleBackColor = true;
            this.addSerihuButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // JoinUpButton
            // 
            this.JoinUpButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.JoinUpButton.Location = new System.Drawing.Point(296, 0);
            this.JoinUpButton.Margin = new System.Windows.Forms.Padding(4);
            this.JoinUpButton.Name = "JoinUpButton";
            this.JoinUpButton.Size = new System.Drawing.Size(53, 25);
            this.JoinUpButton.TabIndex = 4;
            this.JoinUpButton.Text = "▲";
            this.JoinUpButton.UseVisualStyleBackColor = true;
            this.JoinUpButton.Click += new System.EventHandler(this.JoinUpButton_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.CausesValidation = false;
            this.numericUpDown1.Dock = System.Windows.Forms.DockStyle.Right;
            this.numericUpDown1.InterceptArrowKeys = false;
            this.numericUpDown1.Location = new System.Drawing.Point(349, 0);
            this.numericUpDown1.Margin = new System.Windows.Forms.Padding(4);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.ReadOnly = true;
            this.numericUpDown1.Size = new System.Drawing.Size(43, 22);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.Value = 0;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // commentBoxList1
            // 
            this.commentBoxList1.BorderColor = System.Drawing.Color.Black;
            this.commentBoxList1.Dock = System.Windows.Forms.DockStyle.Top;
            this.commentBoxList1.GroupBoxName = "セリフ";
            this.commentBoxList1.IsEditing = false;
            this.commentBoxList1.IsNeedRefresh = false;
            this.commentBoxList1.Location = new System.Drawing.Point(0, 180);
            this.commentBoxList1.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.commentBoxList1.Name = "commentBoxList1";
            this.commentBoxList1.Size = new System.Drawing.Size(392, 89);
            this.commentBoxList1.TabIndex = 5;
            this.commentBoxList1.Visible = false;
            this.commentBoxList1.VisibleChanged += new System.EventHandler(this.commentBoxList1_VisibleChanged);
            // 
            // SectionPageSubPicture1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.commentBoxList1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(7, 6, 7, 6);
            this.Name = "SectionPageSubPicture1";
            this.Size = new System.Drawing.Size(392, 279);
            this.SSize = new System.Drawing.Size(396, 283);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.commentBoxList1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button addSerihuButton;
        private System.Windows.Forms.Button JoinUpButton;
        private Section.CommentBoxList commentBoxList1;
        private StoryViewer.ImageViewer.AA.Section.JoinUpSpaceUpDown numericUpDown1;
        private System.Windows.Forms.Label label1;
    }
}
