﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Interface;
namespace StoryViewer.BookEditor.StandardEditor
{
    public class StandardEditorPlugin : StoryPageListClass,IChapterEditorPlugin
    {
        private static string SettingPath = SuouBase.SuouEditorConst.GetSettingFile("StandardEditor.stn");
        public StandardEditor Editor { get; set; }
        public override SuouBase.Book.IBook Item
        {
            get
            {
                return this.Editor.InnerBook;
            }
            set
            {
                
                if (value == null)
                {
                    this.Editor.InnerBook = null;
                }
                else
                {
                    if (value is SuouBase.Book.Chapter.ChapterBook2)
                    {
                        this.Editor.InnerBook = value as SuouBase.Book.Chapter.ChapterBook2;
                    }
                    else
                    {
                        //throw new Exception();
                        this.Editor.InnerBook = SuouBase.Book.IBookHandler.ToChapterBook2(value);
                    }
                
                }
            }
        }
        public StandardEditorPlugin():base()
        {
            this.Editor = new StandardEditor();
            
        }
        public StandardEditorPlugin(EditorInfo info):base(info)
        {
            var ctrl = new StandardEditor();
            ctrl.SuperInfo = info;
            this.Editor = ctrl;
            this.Viewer = this.Editor;
            this.Info = info;
            this.Viewer.Info = this.Info.Info;
            ctrl.GotFocus += (a, b) => info.DisplayType = EditorDisplay.PageViewer;
            this.Editor.Option = BSystem.XML.BinarySerialize.LoadFromBinaryFile<StoryViewer.ImageViewer.ImageControlOption>(SettingPath);
        }
        public override void Call()
        {
            throw new NotImplementedException();
        }
        public override void CheckToolStripItems()
        {
            throw new NotImplementedException();
        }
        public override System.Drawing.Size CompactSize
        {
            get { return new System.Drawing.Size(250, 250); }
        }
        public override System.Windows.Forms.ContainerControl Control
        {
            get { return this.Editor; }
        }
        public override bool DisposeCheck()
        {
            BSystem.XML.BinarySerialize.SaveToBinaryFile(this.Editor.Option, SettingPath);
            return true;
        }
        public override void FocusPage(int index)
        {
            this.Editor.ScrollPage(index);
        }
        /*
        public void FocusSection(int index)
        {
            this.Editor.ViewSection(this.Editor.InnerBook.Pages[index]);
        }
        */

        public override System.Windows.Forms.Form Form
        {
            get { throw new NotImplementedException(); }
        }
        public override string Name
        {
            get { return "StandardEditor"; }
        }
        public override void Paste(SuouBase.Book.IBookPage ap)
        {
            throw new NotImplementedException();
        }
        public override SettingPage[] SettingPage
        {
            get
            {
                return new SettingPage[] { };
            }
        }
        public override System.Windows.Forms.ToolStripItemCollection ToolStripItems
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
    }
}
