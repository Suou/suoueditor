﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Drawing;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using StoryViewer.BookEditor;
using SuouBase.Book;
using SuouBase.Book.Chapter;
namespace StoryViewer.BookEditor.StandardEditor
{
    public partial class StandardEditor : UserControl,SuouBase.Interface.IStoryViewer
    {
        public StandardEditor()
        {
            InitializeComponent();
        }
        public StoryViewer.ImageViewer.ImageControlOption Option
        {
            get
            {
                return this.storyViewerBox21.SizeOption;
            }
            set
            {
                if (value != null)
                {
                    this.storyViewerBox21.SizeOption = value;
                }
            }

        }
        public event Action<object, SectionScene> ItemLeftClicked;
        /*
        public StoryViewer.BookEditor.Section.SectionViewer SubBox
        {
            get { return this.storyViewerBox21; }
        }
        public ContextMenuStrip RightContextMenuStrip
        {
            get { return this.storyViewerBox21.RightContextMenuStrip; }
        }
        */
        /*
        public StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.BookGrouperSavedata Savedata
        {
            get
            {
                return new StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.BookGrouperSavedata() { SavedataA = this.storyViewerBox21.SaveData };
            }
            set
            {
                if (value != null)
                {
                    this.storyViewerBox21.SaveData = (value.SavedataA);
                }
            }
        }
        */
        /*
        private SuouEditorDataClass _BookData = null;
        public SuouEditorDataClass BookData
        {
            get { return this._BookData; }
            set
            {
                this._BookData = value;
                this.LoadBookData();
            }
        }
        */
        public ChapterBook2 InnerBook
        {
            get
            {
                return this.chapterMenubar1.Book;
            }
            set
            {
                if(this.InnerBook != null)this.InnerBook.SelectedIndexChanged -= this.Book_SelectedIndexChanged;
                this.chapterMenubar1.Book = value;

                if (this.InnerBook != null)this.InnerBook.SelectedIndexChanged += this.Book_SelectedIndexChanged;
                this.LoadBookData();
            }
        }
        private void LoadBookData()
        {
            if (this.InnerBook != null && this.InnerBook.Count > 0)
            {
                //var book = this.BookData.StoryFile.ToChapterBook2();
                //this.InnerBook = book;
                if (this.InnerBook.SelectedPageIndex == -1) this.InnerBook.SelectedPageIndex = 0;
                this.ViewSection(this.InnerBook.SelectedPageIndex);
            }
            else
            {
                this.storyViewerBox21.Close();
            }
        }

        public SmallInfo Info
        {
            get { return this.storyViewerBox21.EditorSmallInfo; }
            set { this.storyViewerBox21.EditorSmallInfo = value; }
        }

        private EditorInfo _SuperInfo = null;
        public EditorInfo SuperInfo
        {
            get { return this._SuperInfo; }
            set
            {
                this._SuperInfo = value;
                this.storyViewerBox21.NotifyPageInfoFunc = this.SuperInfo.MasterForm.NotifyPageSize;
                this.storyViewerBox21.EditorInfo = value;
            }
        }
        /*
        private ChapterBook2 _Item;
        public ChapterBook2 Item
        {
            get { return this._Item; }
            set
            {
                this.chapterMenubar1.Book = value;
                this._Item = value;
                //this.Set();
            }
        }
        */
        public void ScrollPage(int i)
        {
            int t = 0;
            foreach (var v in this.InnerBook.Pages)
            {
                if (i >= v.Items.Count)
                {
                    i -= v.Items.Count;
                }
                else
                {
                    this.ViewSection(t);
                    this.storyViewerBox21.FocusPage(i);
                    break;
                }
                t++;
            }
        }

        //public event BookPageContentChangeEventHandler ItemClicked;

        public bool IsOperateEnable
        {
            get
            {
                return this.InnerBook != null;
            }
        }
        private void Book_SelectedIndexChanged(object sender,EventArgs e)
        {
            this.ViewSection(this.InnerBook.SelectedPageIndex);

        }
        /*
        public void FocusPage(int n)
        {
            var k = this.Item.TranslateSubIndex(n);
            var index = this.Item.Pages.IndexOf(k.Key);
            var subindex = k.Value;
            this.Item.SelectedPageIndex = index;
            this.storyViewerBox21.EditItem = k.Key.Items;

            this.storyViewerBox21.FocusPage(subindex);
        }
        public void UpdatePage()
        {
            //this.thumContViewBox1.Operator.CheckChangePicture();
            this.storyViewerBox21.UpdatePage();
        }
        private void Set()
        {
            this.storyViewerBox21.EditItem = null;
            if (this.Item == null)
            {
                //this.thumContViewBox1.Operator.Clear();

            }
            else
            {
                //var pages = this.Item.TextImagize();
                //this.thumContViewBox1.Operator.ImageItemSet(pages);
                if (this.Item.Pages.Count > 0)
                {
                    this.storyViewerBox21.EditItem = this.Item.Pages[this.Item.SelectedPageIndex].Items;
                    this.UpdateGroupBoxName();
                }
                else
                {
                }
            }
        }
        private void UpdateGroupBoxName()
        {
            this.groupBox1.Text = string.Format("セクション({0})", this.Item.SelectedPage.Name);
        }
        private void thumContViewBox1_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            var p = (ChapterBookPage)e.SelectedPage;
            this.Item.SelectedPageIndex = e.SelectedIndex;
            this.storyViewerBox21.EditItem = p.Items;
            this.UpdateGroupBoxName();

            //this.tabControl1.SelectedIndex = 1;
        }


        private void addNewPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ap = new ChapterBookPage() { Name = "新しいチャプター" };
            this.Item.Add(ap);

            //var f = new TextThumbImage(ap, 0);
            //this.thumContViewBox1.Operator.InsertToSelectedLocation(f);
        }


        private void splitSectionToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var book = this.storyViewerBox21.Split();
            this.Item.Pages[this.Item.SelectedPageIndex].Items.Pages = book.Key.Pages;
            var p = new ChapterBookPage();
            p.Items.Pages = book.Value.Pages;
            // this.thumContViewBox1.Operator.Insert(this.Item.SelectedSectionIndex + 1, p.TextImagize());
            this.storyViewerBox21.EditItem = null;
            //this.tabControl1.SelectedIndex = 0;

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {


        }

        private void storyViewerBox21_ItemClicked(object sender, BookPageContentChangeEventArgs e)
        {
            if (this.ItemClicked != null) this.ItemClicked(this, e);

        }




        private void chapterMenubar1_ItemSelected(ObsoleteSectionBook arg1, EventArgs arg2)
        {
            if (this.DLLFunction != null) this.DLLFunction.DisplayType = EditorDisplay.PageViewer;
            this.storyViewerBox21.EditItem = arg1;
            this.Item.SelectedPageIndex = this.Item.Pages.FindIndex((v) => v.Items == arg1);
            this.groupBox1.Text = string.Format("セクション({0})", arg1.Name);
        }
        */
        public bool CanCut
        {
            get { return false; ; }
        }
        public bool CanCopy
        {
            get { return false; }
        }
        public bool CanPaste
        {
            get { return false; }
        }
        public bool CanUndo
        {
            get { return false; }
        }
        public bool CanSettingModel
        {
            get
            {
                return this.InnerBook != null;
            }
        }
        public void SettingModel()
        {
            PageModelSetting.SettingModel(this.chapterMenubar1.Book);
        }
        public void Undo()
        {
            //this.storyViewerBox21.Undo();
        }
        public void Cut()
        {
            //this.storyViewerBox21.Cut();
        }
        public void Paste()
        {
            //this.storyViewerBox21.Paste();
        }
        public void Copy()
        {
            //this.storyViewerBox21.Copy();
        }
        public void AddNewPage()
        {
            //this.storyViewerBox21.AddNewPage();
        }
        public void AddNewSection()
        {
            this.chapterMenubar1.AddNewSection();
        }

        public bool CanAddNewPage
        {
            get { return this.InnerBook != null; }
        }
        public bool CanAddNewSection
        {
            get { return this.InnerBook != null; }
        }

        private void chapterMenubar1_ItemSelected(SectionNBook arg1, EventArgs arg2)
        {
            this.OnGotFocus(new EventArgs());
            var fst = this.InnerBook.Pages.First((v) => v.Items == arg1);
            this.InnerBook.SelectedPage = fst;
        }
        public void ViewSection(int i)
        {
            this.InnerBook.SelectedPageIndex = i;
            this.storyViewerBox21.TopNumber = this.InnerBook.Pages[this.InnerBook.SelectedPageIndex].SectionPageNumber;
            this.storyViewerBox21.Book = this.InnerBook.Pages[this.InnerBook.SelectedPageIndex].Items;

        }

        private void storyViewerBox21_SubPictureLeftClicked(object arg1, SectionScene arg2)
        {
            if (this.ItemLeftClicked != null) this.ItemLeftClicked(this, arg2);
        }

        private void storyViewerBox21_MouseEnter(object sender, EventArgs e)
        {
            this.OnGotFocus(e);

        }

        
        private void storyViewerBox21_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.All;
        }

        private void storyViewerBox21_DragDrop(object sender, DragEventArgs e)
        {
            var page = this.storyViewerBox21.GetDroppedPage(e.Data);
            if (this.SuperInfo != null)
            {
                var p = new ChapterBook2();
                p.Pages[0].Items.Clear();
                p.Pages[0].Items.Add(page.ToSectionPage2(p.Model));
                var q = new SuouEditorDataClass(p);
                this.SuperInfo.FileData.Add(q);
            }
            else
            {
                var p = new ChapterBook2();
                p.Pages[0].Items.Clear();
                p.Pages[0].Items.Add(page.ToSectionPage2(p.Model));
                this.InnerBook = p;
            }
        }

    }
}
