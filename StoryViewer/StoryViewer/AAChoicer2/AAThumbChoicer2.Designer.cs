﻿namespace AAEditTabControl.AAChoicer
{
    partial class AAThumbChoicer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tripleSpliter1 = new AAEditTabControl.TripleSpliter();
            this.SuspendLayout();
            // 
            // tripleSpliter1
            // 
            this.tripleSpliter1.CenterControl = null;
            this.tripleSpliter1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tripleSpliter1.LeftUpperControl = null;
            this.tripleSpliter1.Location = new System.Drawing.Point(0, 0);
            this.tripleSpliter1.Name = "tripleSpliter1";
            this.tripleSpliter1.RightBottomControl = null;
            this.tripleSpliter1.Size = new System.Drawing.Size(150, 150);
            this.tripleSpliter1.SplitterDirection = AAEditTabControl.TripleDirection.Horizontal;
            this.tripleSpliter1.TabIndex = 0;
            // 
            // AAThumbChoicer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tripleSpliter1);
            this.Name = "AAThumbChoicer";
            this.ResumeLayout(false);

        }

        #endregion

        public TripleSpliter tripleSpliter1;

    }
}
