﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using AAEditTabControl;
using SuouBase;
namespace AAEditTabControl.AAChoicer
{
    [IComponentPlugin("AAChoicer", PluginType.Control|PluginType.Form)]
    public class AAChoicerPlugin:SuouBase.Interface.IComponentPlugin
    {
        public AAChoicerPlugin()
        {

            HaveForm = new AAThumbChoicerForm();
            HaveControl = new AAThumbChoicer() { Dock = DockStyle.Fill };

            HaveForm.Load += this.HaveForm_Loaded;
        }
        public AAChoicerPlugin(PluginDelegate pd):this()
        {

            LocalAAPlugin = new LocalTemplateOperation(pd);
            GlobalAAPlugin = new SimpleGlobalTemplate.SimpleGlobalTemplate(pd);
            ThumbnailPlugin = new AAThumbnailView.AAThumbnailViewPlugin(pd);

            HaveControl.tripleSpliter1.LeftUpperControl = LocalAAPlugin.Control;
            HaveControl.tripleSpliter1.RightBottomControl = GlobalAAPlugin.Control;
            HaveControl.tripleSpliter1.CenterControl = ThumbnailPlugin.Control;

            ThumbnailPlugin.Control.MouseEnter += this.ThumbnailView_MouseEnter;

            ((NotifyControl)LocalAAPlugin).FocusNotifiedEventHandler += this.FocusNotified;
            ((NotifyControl)GlobalAAPlugin).FocusNotifiedEventHandler += this.FocusNotified;
            ((NotifyControl)ThumbnailPlugin).FocusNotifiedEventHandler += this.FocusNotified;

            HaveForm.SetControl=HaveControl;
            HaveForm.Owner = pd.MasterForm.Form;

            pd.LocalTemplatePlugin = LocalAAPlugin;
            pd.GlobalTemplatePlugin = GlobalAAPlugin;
            pd.TemplatePageViewPlugin = ThumbnailPlugin;

            var obj = BinarySerialize.LoadFromBinaryFile(SaveFileName);
            if (obj != null)
            {
                SaveData = (AAChoicerSaveData)obj;
                bSaveData = true;
            }

        }
        private AAChoicerSaveData SaveData;
        private bool bSaveData = false;
        public void HaveForm_Loaded(object obj, EventArgs e)
        {
            if (bSaveData)
            {
                HaveControl.tripleSpliter1.SaveData = SaveData.AATripleSpliterSaveData;
                HaveForm.SaveData = SaveData.AAFormSaveData;
                
            }
        }

        public string SaveFileName = SuouEditorConst.GetSettingFile("AAChoicer.stn");
        public LocalTemplateOperation LocalAAPlugin = null;
        public SimpleGlobalTemplate.SimpleGlobalTemplate GlobalAAPlugin = null;
        public AAThumbnailView.AAThumbnailViewPlugin ThumbnailPlugin = null;

        private AAThumbChoicerForm HaveForm;
        private AAThumbChoicer HaveControl;
        public ContainerControl Control
        {
            get { return HaveControl; }
        }
        public Form Form
        {
            get { return HaveForm; }
        }
        public void Dispose()
        {


            var f = new AAChoicerSaveData(HaveForm,HaveControl.tripleSpliter1);
            BinarySerialize.SaveToBinaryFile(f, SaveFileName);

            LocalAAPlugin.Dispose();
            GlobalAAPlugin.Dispose();
            ThumbnailPlugin.Dispose();
        }
        private void ThumbnailView_MouseEnter(object sender, EventArgs e)
        {
            ThumbnailPlugin.Control.Focus();
        }
        private void FocusNotified(object sender,EventArgs e)
        {
            if (HaveForm != null)
            {
                if (!HaveForm.Visible) HaveForm.Visible = true;
                HaveForm.Focus();
            }
        }
    }
    [Serializable()]
    public struct AAChoicerSaveData
    {
        public FormSaveData AAFormSaveData;
        public TripleSpliterSaveData AATripleSpliterSaveData;
        public AAChoicerSaveData(AAChoicer.AAThumbChoicerForm f, TripleSpliter ts)
        {
            AAFormSaveData = f.SaveData;
            AATripleSpliterSaveData = ts.SaveData;
        }
    }
}
