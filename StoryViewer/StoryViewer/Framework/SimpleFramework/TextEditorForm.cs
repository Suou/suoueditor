﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Controls;
using BSystem.Collections;
using BSystem.Controls;
using SuouBase.Base;
using SuouBase;
namespace StoryViewer.Framework.SimpleFramework
{
    public partial class TextEditorForm : SerializableForm,INotifyStripForm
    {
        public TextEditorForm()
        {
            InitializeComponent();

        }
        public EditorInfo DLLFunction { get; set; }
        public void NotifyMessage(string text)
        {
            this.toolStripStatusLabel1.Text = text;
        }
        public void SetPlugin(int n, Control c,string name,Size defaultSize)
        {

            if (c == null) return;

            //Control c = plg.Control;
            if (n == 11)
            {
                this.panel1.Controls.Add(c);
                c.Dock = DockStyle.Fill;
                
            }
            else
            {
                this.dockingToolStrip1.AddControl(c,name,defaultSize, n > 15,SuouTextBox2.Properties.Resources.turu_perple);
            }


        }
        public new TextEditorFormSaveData Setting
        {
            get
            {
                var p = base.Setting;
                var r = new TextEditorFormSaveData() { FormData = p, Info = this.dockingToolStrip1.InfoDic };
                
                return r;
            }
            set
            {
                if (value == null) return;
                this.dockingToolStrip1.InfoDic = value.Info;
                base.Setting = value.FormData;
            }
        }

        private void TextEditorForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                if (this.DLLFunction != null && this.DLLFunction.TextBoxPlugin != null)
                {
                    this.DLLFunction.Info.EditBack(this.DLLFunction.TextBoxPlugin.Page);
                    //this.DLLFunction.TextBoxPlugin.Determine();
                    this.DLLFunction.TextBoxPlugin.Page = null;
                }
                e.Cancel = true;
                this.Visible = false;
            }
        }

        private void previewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.DLLFunction != null)
            {
                this.DLLFunction.PreviewPlugin.SetPageItem(this.DLLFunction.TextBoxPlugin.Page);
                this.DLLFunction.PreviewPlugin.Form.Show(this.ParentForm);
            }
        }

        private void TextEditorForm_Shown(object sender, EventArgs e)
        {
            this.previewToolStripMenuItem.Visible = this.DLLFunction != null;
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void largeCharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var v = StoryViewer.LargeCharBox.Create(this.DLLFunction);
            v.Show();
        }
    }
    
    [Serializable]
    public class TextEditorFormSaveData
    {
        public SerializableDictionary<string, DockingPanelInfo> Info { get; set; }
        public FormSaveData FormData { get; set; }
    }
    
}
