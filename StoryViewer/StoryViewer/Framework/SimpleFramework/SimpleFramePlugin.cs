﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using System.Windows.Forms;
using System.IO;
using SuouBase;
using BSystem.Collections;
using BSystem.Controls;
using System.Drawing;

namespace StoryViewer.Framework.SimpleFramework
{
    [IComponentPlugin("SimpleFramework", PluginType.Framework)]
    public class SimpleFramePlugin:IFrameworkPlugin
    {
        private TextEditorForm EditorForm = new TextEditorForm() { Size = new Size(800, 500) };
        private SimpleFrame FrameControl = new SimpleFrame();
        public void NotifyFinishAddPluginList()
        {
            this.FrameControl.FinishAddPlugin();

            var mlt = BSystem.XML.NonBinarySerializer.Load<SuouBase.Controls.ShortCutKeyDictionary>(SaveShortCutKeyFileName);
            if (mlt != null) mlt.SetShortCutKey(this.EditorForm.MainMenuStrip);
        }
        public SettingPage[] SettingPage
        {
            get
            {
                return new SettingPage[] { new SuouBase.Controls.ShortCutRegister(this.EditorForm.MainMenuStrip){PageName ="テキストボックスフォーム"} };
            }
        }
        public SimpleFramePlugin()
        {
        }

        public SimpleFramePlugin(Size size)
        {
            FrameControl.Size = size;
        }
        public SimpleFramePlugin(EditorInfo pd)
            : this()
        {
            DLLFunction = pd;
            this.FrameControl.DLLFunction = pd;
            var savedata = SimpleFrameSaveData.Load(SimpleFramePlugin.SaveFileName);
            if (savedata != null)
            {
                this.FrameControl.SaveData = savedata;
                this.EditorForm.Setting = savedata.SaveData;
            }

            this.DLLFunction.MasterForm.Form.Activated += this.MasterForm_Focused;

            this.EditorForm.DLLFunction = this.DLLFunction;
            this.Items = new List<IComponentPluginMetaData>();
        }
        public void AllShow()
        {
            this.FrameControl.AllShow();
        }
        public void Remove(IComponentPluginMetaData cpmd)
        {
            if (cpmd.Position >= 0)
            {
                this.FrameControl.Remove(cpmd.Plugin.Control);
                this.Items.Remove(cpmd);
            }
        }

        public void Replace(IComponentPluginMetaData before, IComponentPluginMetaData after)
        {
            var p = before.Position;
            this.Remove(before);
            after.Position = p;
            this.SetPlugin(after);
            before.Position = -1;
        }
        private void EditroForm_Shown(object sender, EventArgs e)
        {
        }
        private void MasterForm_Focused(object sender,EventArgs e)
        {
            /*
            var p = this.EditorForm.PointToClient(Control.MousePosition);
            if (this.EditorForm != null && this.DLLFunction.TextBoxPlugin != null && !this.EditorForm.DisplayRectangle.Contains(p))
            {
                this.DLLFunction.TextBoxPlugin.Page = null;
                this.EditorForm.Visible = false;
            }
            */
        }
        public static string SaveFileName = SuouEditorConst.SettingDirectory + "/SimpleFrame.stn";
        public static string SaveShortCutKeyFileName = SuouEditorConst.SettingDirectory + "/SimpleFrameKey.stn";
        public EditorInfo DLLFunction { get; set; }
        public ContainerControl Ctrl
        {
            get { return FrameControl; }
        }
        public void SetControl(Control c, string name, int position)
        {
            throw new Exception();
        }
        public List<IComponentPluginMetaData> Items { get; set; }
        public void SetPlugin(IComponentPluginMetaData pd)
        {
            if (pd.Position == -1)
            {
            }
            else
            {
                if (pd.Position <= 10)
                {
                    string s = pd.PluginName;
                    if (pd.Plugin is ITabPlugin)
                    {
                        s = ((ITabPlugin)pd.Plugin).Name;
                    }
                    FrameControl.SetPlugin(pd.Position, pd.Plugin);
                }
                else
                {
                    string s = pd.PluginName;
                    if (pd.Plugin is ITabPlugin)
                    {
                        s = ((ITabPlugin)pd.Plugin).Name;
                    }
                    this.EditorForm.SetPlugin(pd.Position, pd.Plugin.Control,pd.Plugin.Name,pd.Plugin.CompactSize);

                }
                
            }

            if (pd.Plugin is ITextEditClass)
            {
                var p = (ITextEditClass)pd.Plugin;
                p.PageChanged += this.TextBox_PageChanged;
            }
            if (pd.Plugin is IStoryPageListClass)
            {
                var p = (IStoryPageListClass)pd.Plugin;
                p.ViewerPageSelected += this.Viewer_PageSelected;
            }
            this.Items.Add(pd);
        }
        private void TextBox_PageChanged(object sender, EventArgs e)
        {
            if(this.DLLFunction.TextBoxPlugin.Page != null)this.OpenEditorForm();
        }
        private void OpenEditorForm()
        {
            this.EditorForm.Owner = this.Ctrl.ParentForm;

            if (this.EditorForm.Visible)
            {
                if(this.EditorForm.WindowState == FormWindowState.Minimized)this.EditorForm.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.EditorForm.Show(this.Ctrl.ParentForm);
            }
            //var p = this.EditorForm.ShowDialog(this.Ctrl.ParentForm);

        }
        private void Viewer_PageSelected(object sender, BookPageContentChangeEventArgs e)
        {
        }
        public bool CanSetPlugin(IComponentPlugin pb, int nth)
        {
            return true;
        }
        public bool InitializePluginList()
        {
            return true;
        }
        public bool DisposeCheck()
        {
            
            var p = this.FrameControl.SaveData;
            p.SaveData = this.EditorForm.Setting;
            p.Save(SimpleFramePlugin.SaveFileName);

            var mlt = SuouBase.Controls.ShortCutKeyDictionary.Create(this.EditorForm.MainMenuStrip);
            BSystem.XML.NonBinarySerializer.Save(SaveShortCutKeyFileName, mlt);

            this.FrameControl.UserDispose();
            return true;
        }
        public bool Focus(IComponentPlugin pb)
        {
            if (pb.Control != null)
            {
                this.FrameControl.Focus(pb.Control);
            }
            if (pb is ITextEditClass)
            {
                this.OpenEditorForm();
            }
            return true;
        }
        public void Clear()
        {
            var p = this.Items.ToArray();
            foreach (var v in p)
            {
                this.Remove(v);
            }
        }
        /*
        public bool LocalFocus()
        {
            if (pb.Control != null)
            {
                this.Control.Focus(pb.Control);
            }
            return true;
        }
        */
        
    }
    [Serializable]
    public class SimpleFrameSaveData
    {
        public Point SplitterLocation { get; set; }
        public Size PanelSize { get; set; }
        public Size TabSize { get; set; }
        public Size FormSize { get; set; }
        public SerializableDictionary<string, DockingPanelInfo> Info1 { get; set; }
        public TextEditorFormSaveData SaveData { get; set; }
        

        public void Save(string filename)
        {
            BSystem.XML.NonBinarySerializer.Save(filename,this);
        }
        public static SimpleFrameSaveData Load(string filename)
        {
            var f = BSystem.XML.NonBinarySerializer.Load<SimpleFrameSaveData>(filename);
            if (f == null)
            {
                return null;
            }
            else
            {
                return (SimpleFrameSaveData)f;
            }
        }
 
    }
}
