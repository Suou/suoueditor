﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Controls;
using BSystem.Collections;

namespace StoryViewer.Framework.SimpleFramework
{
    public partial class SimpleFrame : UserControl
    {
        public SimpleFrame()
        {
            InitializeComponent();
            
            
        }
        public EditorInfo DLLFunction { get; set; }
        public SimpleFrameSaveData SaveData
        {
            get
            {
                return new SimpleFrameSaveData() { PanelSize = this.panel2.Size,Info1 = this.dockingToolStrip1.InfoDic };
            }
            set
            {
                this.dockingToolStrip1.InfoDic = value.Info1;
            }
        }


        

        #region Method
        public void SetPlugin(int n, IComponentPlugin plg)
        {

            if (plg.Control == null) return;

            Control c = plg.Control;
            if (n == 0)
            {
                this.panel2.Controls.Add(c);
                c.Dock = DockStyle.Fill;
            }
            else
            {
                this.dockingToolStrip1.AddControl(c, plg.Name, plg.CompactSize, n > 5, SuouTextBox2.Properties.Resources.turu_perple);
            }
            this.PositionDic[plg.Control] = n;


        }
        public Dictionary<Control, int> PositionDic = new Dictionary<Control, int>();
        public void AllShow()
        {
        }
        public void FocusTab()
        {
        }
        public void Save(string path)
        {
        }
        public void Remove(Control ctrl)
        {
            var p = this.PositionDic[ctrl];
            if (p == 0)
            {
                this.panel2.Controls.Remove(ctrl);
            }
            else
            {
                this.Controls.Remove(ctrl);
            }
            this.PositionDic.Remove(ctrl);
            //p = null;
        }
        public void Focus(Control ctrl)
        {
        }
        public void FinishAddPlugin()
        {
        }
        public void UserDispose()
        {
        }
        public bool ContainControl(Control ctrl, Control target)
        {
            bool b = false;
            if (ctrl.Controls.Contains(target)) b = true;
            foreach (Control v in ctrl.Controls)
            {
                
                if (this.ContainControl(v, target)) b = true;
            }
            return b;
        }
        #endregion
        #region Event
        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            //対象のTabControlを取得
            TabControl tab = (TabControl)sender;
            TabPage page = tab.TabPages[e.Index];
            //タブページのテキストを取得
            string txt = page.Text;

            //StringFormatを作成
            StringFormat sf = new StringFormat();
            //縦書きにする
            sf.FormatFlags = StringFormatFlags.DirectionVertical;
            //ついでに、水平垂直方向の中央に、行が完全に表示されるようにする
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            sf.FormatFlags |= StringFormatFlags.LineLimit;

            //背景の描画
            Brush backBrush = new SolidBrush(page.BackColor);
            e.Graphics.FillRectangle(backBrush, e.Bounds);
            backBrush.Dispose();

            //Textの描画
            Brush foreBrush = new SolidBrush(page.ForeColor);
            e.Graphics.DrawString(txt, page.Font, foreBrush, e.Bounds, sf);
            foreBrush.Dispose();
        }
        private void tabControl2_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FocusTab();
        }

        private void SimpleFrame_ParentChanged(object sender, EventArgs e)
        {
        }
        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void SimpleFrame_Load(object sender, EventArgs e)
        {
        }
        #endregion
    }

}
