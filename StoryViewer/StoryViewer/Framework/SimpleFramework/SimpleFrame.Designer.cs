﻿namespace StoryViewer.Framework.SimpleFramework
{
    partial class SimpleFrame
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.dockingToolStrip1 = new BSystem.Controls.DockingToolStrip();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // dockingToolStrip1
            // 
            this.dockingToolStrip1.Dock = System.Windows.Forms.DockStyle.Right;
            this.dockingToolStrip1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.dockingToolStrip1.InnerControl = this.panel2;
            this.dockingToolStrip1.IsAutoOpenAndShutAboutMouseMove = false;
            this.dockingToolStrip1.Location = new System.Drawing.Point(554, 0);
            this.dockingToolStrip1.Name = "dockingToolStrip1";
            this.dockingToolStrip1.SensibleDistance = 50;
            this.dockingToolStrip1.Size = new System.Drawing.Size(32, 432);
            this.dockingToolStrip1.TabIndex = 4;
            this.dockingToolStrip1.Text = "dockingToolStrip1";
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(554, 432);
            this.panel2.TabIndex = 5;
            // 
            // SimpleFrame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.dockingToolStrip1);
            this.Name = "SimpleFrame";
            this.Size = new System.Drawing.Size(586, 432);
            this.Load += new System.EventHandler(this.SimpleFrame_Load);
            this.ParentChanged += new System.EventHandler(this.SimpleFrame_ParentChanged);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private BSystem.Controls.DockingToolStrip dockingToolStrip1;
        private System.Windows.Forms.Panel panel2;

    }
}
