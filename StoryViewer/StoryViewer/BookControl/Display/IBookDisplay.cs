﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Book;
using SuouBase.Text;
namespace StoryViewer.BookControl.Display
{
    public interface IBookDisplay
    {
        void SetBook(IBook ef);
        void SetBook(IBook ef,bool isEdit);
    }
}
