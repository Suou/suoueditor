﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Book;

namespace StoryViewer
{
    [IComponentPlugin("AAThumbnailViewPlugin", PluginType.Control)]
    public class AAThumbnailViewPlugin : ITemplatePageListClass
    {
        public IBookPage SelectPage(Form form, string text)
        {
            throw new Exception();

        }
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public AAThumbnailViewPlugin(EditorInfo pd)
        {

            this.DLLFunction = pd;
            Ctrl.BackColor = Color.Gray;
            Load();
        }
        public AAThumbnailViewPlugin(EditorInfo pd, StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay ctrl)
        {

            this.DLLFunction = pd;
            this.Ctrl = ctrl;
            Ctrl.BackColor = Color.Gray;
            Load();
        }

        public SettingPage[] SettingPage { get { return null; } }

        private EditorInfo DLLFunction
        {
            get { return null; ; }
            set { this.Ctrl.EditorSmallInfo = DLLFunction.Info; }
        }

        private StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay Ctrl = new StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay();

        public IBook Item
        {
            get { return this.Ctrl.Item; }
            set
            {
                this.Ctrl.Item = value;
            }
        }
        public Font Font = new Font("ＭＳ Ｐゴシック", 12);
        public Form Form { get { return null; } }
        public ContainerControl Control
        {
            get { return this.Ctrl; }
        }
        public bool DisposeCheck()
        {
            Save();
            return true;
        }
        //public string SaveDataFile = SuouEditorConst.GetSettingFile("AAthumbnail.stn");
        public void Save()
        {
           // BinarySerialize.SaveToBinaryFile(this.Ctrl.GetSaveData(), SaveDataFile);
        }
        public void Load()
        {
            //var f = BinarySerialize.LoadFromBinaryFile(SaveDataFile);
            //if (f == null) return;
            //this.Ctrl.SetSaveData((ThumbnailView_SaveData)f);
        }
        public event EventHandler FocusNotifiedEventHandler;
        public void FocusNotify()
        {
            if (FocusNotifiedEventHandler != null) FocusNotifiedEventHandler(this, new EventArgs());
        }
    }
}
