﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class AAThumbSettingPage : UserControl
    {
        public ThumContViewBox Control;
        public AAThumbSettingPage()
        {
            InitializeComponent();
        }
        public AAThumbSettingPage(ThumContViewBox ctrl)
            : this()
        {
            this.Control = ctrl;
            BoxValueSet();
        }

        //public event ThumbnailViewEventHandler ItemClicked;
        public ContextMenuStrip ItemContext { get; set; }
        public ContextMenuStrip ControlContext { get; set; }

        /*
        public long LimitCacheFolderSize
        public bool IsCacheLimited
        public bool IsCached
        public int ItemInterval
        public ThumbnailOrder OrderMode
        public Size ItemSize
        public Size MinItemSize
        public float Percent
        public ThumbnailMode ViewMode
            */

        public void BoxValueSet()
        {
            var sz = Control.ItemSize;
            switch (sz.Width)
            {
                case 100:
                    this.comboBox1.SelectedIndex = 0;
                    break;
                case 150:
                    this.comboBox1.SelectedIndex = 1;
                    break;
                case 200:
                    this.comboBox1.SelectedIndex = 2;
                    break;
            }

            /*
            if (Control.IsCached)
            {
                this.radioButton1.Checked = true;
            }
            else
            {
                this.radioButton2.Checked = true;
            }

            if (Control.IsCacheLimited)
            {
                this.radioButton3.Checked = true;
            }
            else
            {
                this.radioButton4.Checked = true;
            }

            this.numericUpDown1.Value = Control.LimitCacheFolderSize / 1000000;
            */
        }
        public void BoxValueGet()
        {
            switch (this.comboBox1.SelectedIndex)
            {
                case 0:
                    this.Control.ItemSize = new Size(100, 100);
                    break;
                case 1:
                    this.Control.ItemSize = new Size(150, 150);
                    break;
                case 2:
                    this.Control.ItemSize = new Size(200, 200);
                    break;
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.BoxValueGet();
            this.ParentForm.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.ParentForm.Close();
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            this.numericUpDown1.Enabled = this.radioButton3.Checked;
        }
    }
}
