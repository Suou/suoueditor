﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using SuouBase.Drawing;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using System.Globalization;
using System.Collections.Specialized;
using SuouBase.Book;
namespace StoryViewer.BookControl.Display.SingleBookDisplay
{
    public partial class SingleBookDisplay : UserControl,IBookDisplay
    {
        
        public Action<IBookPage> AddFavoriteButtonAction { get; set; }
        #region BaseProperty
        Form SettingForm = null;
        public event Action<IBookPage, EventArgs> ItemTouched;
        public event Action<IBookPage, EventArgs> ItemCopied;
        public event Action<IBookPage, EventArgs> EditButtonClicked;
        public event ThumbnailViewEventHandler DragStarting;
        public event ThumbnailViewEventHandler ItemClicked;
        public event EventHandler ItemChenged;
        public event EventHandler SavedataChanged;
        public bool ManualDrag
        {
            get { return this.thumbnailViewBox1.ManualDragDo; }
            set { this.thumbnailViewBox1.ManualDragDo = value; }
        }
        public bool IsTransparentDrag
        {
            get { return this.thumbnailViewBox1.IsTransparentDrag; }
            set { this.thumbnailViewBox1.IsTransparentDrag = value; }
        }
        private bool _EditLock = true;
        public bool EditLock
        {
            get { return this._EditLock; }
            set
            {
                this._EditLock = value;
                if (this.lockToolStripMenuItem.Checked != this.EditLock)
                {
                    this.lockToolStripMenuItem.Checked = this.EditLock;
                    
                }
            }

        }
        private SuouEditorDataClass _DataBook = null;
        public SuouEditorDataClass DataBook
        {
            get { return this._DataBook; }
            set
            {
                this._DataBook = value;
            }
        }
        private void Item_ItemChanged(object sender, BookContentChangeEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (e.Type == BookContentChangeType.Page)
            {
                if (e.PagesAction == SuperObservableCollectionAction.Add)
                {

                    var number = e.ChangeIndex;
                    this.thumbnailViewBox1.Operator.Insert(number, this.CreateImage(e.Book[number]));
                }
                else if (e.PagesAction == SuperObservableCollectionAction.Remove)
                {
                    var number = e.ChangeIndex;
                    this.thumbnailViewBox1.Operator.Delete(number);
                }
                else
                {
                    throw new Exception();
                }
            }
            if (this.ItemChenged != null) this.ItemChenged(this, e);

        }
        public IBook Item
        {
            get 
            {
                if (this.DataBook == null) return null;
                return this.DataBook.StoryFile; 
            }
            set
            {
                if (this.Item != null && this.Setting.IsSaveCheck) this.SaveCheck();
                if (this.DataBook != null)
                {
                    this.DataBook.StoryFile.ContentChanged -= this.Item_ItemChanged;
                }
                if (value == null)
                {
                    this.DataBook = null;                    
                }
                else
                {
                    
                    this.DoEditLock(true);
                    this.DataBook = new SuouEditorDataClass(value);
                    this.DataBook.StoryFile.ContentChanged += this.Item_ItemChanged;
                }
                this.RefreshImage();
            }
        }
        private void RefreshImage()
        {
            this.thumbnailViewBox1.Operator.Clear();
            if (this.Item == null) return;
            var f = TextThumbImage.CreateTextBitmaps(this.Item, 0);
            this.thumbnailViewBox1.Operator.ImageItemSet(f);
        }
        private bool _UseContext = true;
        public bool UseContext
        {
            get
            {
                return _UseContext;
            }
            set
            {
                _UseContext = value;
                if (_UseContext)
                {
                    this.thumbnailViewBox1.ControlContext = ControlContext;
                    this.thumbnailViewBox1.ItemContext = ItemContext;
                }
                else
                {
                    this.thumbnailViewBox1.ControlContext = null;
                    this.thumbnailViewBox1.ItemContext = null;
                }
            }
        }
        [ReadOnly(true)]
        [Browsable(false)]
        public SmallInfo EditorSmallInfo { get; set; }
        public bool PermitCategory
        {
            get { return this.thumbnailViewBox1.PermitCategory; }
            set { this.thumbnailViewBox1.PermitCategory = value; }
        }

        private BookDisplaySetting _Setting = new BookDisplaySetting();
        //[ReadOnly(true)]
        public BookDisplaySetting Setting
        {
            
            get { return this._Setting; }
            set
            {
                if(value != null)this._Setting = value;
            }
        }
        public ThumbnailView_SaveData GetSaveData()
        {
            return this.thumbnailViewBox1.Operator.SaveData;
        }
        public void SetSaveData(ThumbnailView_SaveData val)
        {
            this.thumbnailViewBox1.Operator.SaveData = (val);
        }
        public NameTextBoxType EnableNameTextBox
        {
            get { return this.thumbnailViewBox1.EnableNameTextBox; }
            set { this.thumbnailViewBox1.EnableNameTextBox = value; }
        }

        public int SelectedIndex
        {
            get { return this.thumbnailViewBox1.Operator.MapOperator.LastSelectedIndex; }
        }
        #endregion
        #region Constructor
        public SingleBookDisplay()
        {
            InitializeComponent();

        }
        public SingleBookDisplay(IBook ef, SmallInfo smallInfo, ThumbnailView_SaveData savedata):this()
        {
            //this.DLLFunction = DLLFunction;
            if (smallInfo == null) throw new Exception();
            this.EditorSmallInfo = smallInfo;
            this.SetSaveData(savedata);
            this.Item = ef;
            this.Setting = new BookDisplaySetting() { EditLockButtonVisible = true, IsSaveCheck = true };

        }
        #endregion
        #region Method

        private void DoEditLock(bool b)
        {
            this.EditLock = b;
            this.lockToolStripMenuItem.Checked = b;
        }
        public bool SaveCheck()
        {
            if (this.Item.IsEdit == true)
            {
                DialogResult result = GeneralMethod.SaveKakuninBox(this.Item.Name);
                switch (result)
                {
                    case DialogResult.Yes:
                        this.Item.Save(this.Item.FullFilePath, true);
                        return true;
                }
            }
            return false;
        }
        public IBookPage SelectedPage
        {
            get
            {
                var v = this.thumbnailViewBox1.Operator.MapOperator.LastSelectedItem;
                if (v != null) return v.Page;
                return null;
            }
        }
        public void LoadItem(System.IO.FileInfo io)
        {
            
            var book = IBookHandler.Load(io.FullName);
            this.Item = book;
        }
        public bool Close()
        {
            return this.DataBook.Close();
        }
        public void SetBook(IBook ef)
        {
            this.Item = ef;
        }
        public void SetBook(IBook ef,bool isEdit)
        {
            this.Item = ef;
            if (isEdit) this.EditLock = false;
        }
        public void CallBackMethod(IAsyncResult ar)
        {
        }
        public void Clear()
        {
            this.Item = null;
            this.thumbnailViewBox1.Operator.Clear();
        }
        public void ClearImage()
        {
            this.thumbnailViewBox1.ClearImage();
        }
        private TextThumbImage CreateImage(IBookPage page)
        {
            return new TextThumbImage(page, 0);
        }
        public void Paste(IBookPage p)
        {
            var n = this.thumbnailViewBox1.Operator.MapOperator.NearItemIndex + 1;
            this.Item.Insert(n,p);
        }
        public bool CanPaste
        {
            get
            {
                if (this.EditorSmallInfo != null)
                {
                    return !this.EditLock && this.EditorSmallInfo.IsClipPage;
                }
                else
                {
                    return !this.EditLock && ClipBoardExtensions.HasClipBoardData;
                }
            }
        }
        public bool CanHyperPaste
        {
            get
            {
                if (this.EditorSmallInfo == null) return false;
                return !this.EditLock && this.EditorSmallInfo.IsClipPage && this.thumbnailViewBox1.Operator.MapOperator.NearItemIndex != -1;
            }
        }
        #endregion
        #region ButtonMethod
        private void ViewPercent50Button_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.Percent = (float)0.5;
        }

        private void ViewPercent75Button_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.Percent = (float)0.75;
        }
        private void ViewSize200Button_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.ItemSize = new Size(200, 200);
        }
        private void ViewSize125Button_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.ItemSize = new Size(125, 125);
        }
        private void ViewPercent100Button_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.ViewMode = ThumbnailMode.Raw;
        }
        private void ViewSize100Button_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.ItemSize = new Size(100, 100);
        }
        private void ViewSize150Button_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.ItemSize = new Size(150, 150);
        }
        
        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = this.thumbnailViewBox1.Operator.MapOperator.LastSelectedImageBoxItems.Select((v)=>v.TextItem.Page).ToArray();
            if (f != null && this.EditorSmallInfo != null)
            {
                this.EditorSmallInfo.Copy(f,EncodeType.UTF8);
                
                if (this.ItemCopied != null)
                {
                    var page = this.EditorSmallInfo.GetCopy().First();
                    this.ItemCopied(page, e);
                }
            }
        }
        
        private void 設定ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingForm = new Form();
            SettingForm.Controls.Clear();
            var f = new AAThumbSettingPage(this.thumbnailViewBox1);
            var sz = new Size(f.Size.Width + 50, f.Size.Height + 50);
            SettingForm.Size = sz;
            SettingForm.MinimumSize = sz;
            SettingForm.Controls.Add(f);
            SettingForm.Dock = DockStyle.Fill;

            SettingForm.Visible = true;
            SettingForm.Focus();
        }
        
        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.EditButtonClicked != null) this.EditButtonClicked(this.SelectedPage, e);
            /*
            if (this.DLLFunction == null) return;
            var w = this.DLLFunction.PluginList.First((v) => v.Plugin is StoryViewerEditPlugin);
            if (w != null)
            {
                var viewer = (StoryViewerEditPlugin)w.Plugin;
                viewer.BookPage = this.SelectedPage;

                if (this.SelectedPage is SectionBookPage)
                {
                    var p = (SectionBookPage)this.SelectedPage;
                }
            }
            */
        }
        
        private void hyperPasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.EditorSmallInfo.GetCopy().ForEach((v) => this.Paste(v));
        }
        private void Paste()
        {
            if (this.EditorSmallInfo != null)
            {
                if (this.EditorSmallInfo.IsClipPage)
                {
                    foreach (var v in this.EditorSmallInfo.GetCopy()) this.Paste(v.ToMergeBookPage());
                    
                }
            }
            else
            {
                MessageBox.Show("Error");
            }
        }
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Item.Delete(this.thumbnailViewBox1.Operator.MapOperator.LastSelectedIndexes);
            //this.thumbnailViewBox1.DeleteSelectedItem();
            //this.thumbnailViewBox1.DeleteItem(this.thumbnailViewBox1.LastSelectedIndex);
        }


        private void lockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.DoEditLock(!this.EditLock);
        }

        private void renameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var p = this.SelectedPage;
            var f = new SuouBase.Controls.InputForm("名前をつける", p.Name);
            var result = f.ShowDialog();
            if (result == DialogResult.OK)
            {
                p.Name = f.Message;
            }
            else
            {
            }
        }

        private void montageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            if (this.DLLFunction != null && this.DLLFunction.Common.Montage != null)
            {
                this.DLLFunction.Common.Montage(this.SelectedPage.SText.Clone());
            }
            */
        }

        private void addNewPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Item is EditingFile)
            {
                this.Paste(new AstPage());
            }
            else
            {
                this.Paste(new SectionScene());
            }
        }

        private void thumbnailViewBox1_ItemDragStarting(object sender, ThumbnailViewEventArgs e)
        {
            if (this.DragStarting != null) this.DragStarting(sender, e);
        }

        private void openAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.OpenAll();
        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.CloseAll();

        }

        private void threadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.OrderMode = ThumbnailOrder.Thread;
        }

        private void tableToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.thumbnailViewBox1.OrderMode = ThumbnailOrder.SmallTile;

        }
        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Paste();
            
        }
        #endregion
        #region EventMethod
        private void ItemContext_Opening(object sender, CancelEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.deleteToolStripMenuItem.Visible = !this.EditLock;
            this.editToolStripMenuItem.Visible = !this.EditLock;
            this.pasteToolStripMenuItem.Visible = !this.EditLock;

            this.pasteToolStripMenuItem.Enabled = this.CanPaste;
            this.renameToolStripMenuItem.Visible = !this.EditLock;
            if (this.EditorSmallInfo != null)
            {
                //this.CopyToolStripMenuItem.Enabled = this.EditorSmallInfo.GetCopy().Count() > 0;
            }
        }
        private void ViewModeCheck()
        {
            var r = this.thumbnailViewBox1.ViewMode;
            //this.ToolStrip1MenuItem.Checked = false;
            var items = new[] { this.p50MenuItem, this.p75MenuItem, this.p100MenuItem, this.s100MenuItem, this.s125MenuItem, this.s150MenuItem, this.s200MenuItem };
            items.ForEach((v) => v.Checked = false);
            if (r == ThumbnailMode.Percent && this.thumbnailViewBox1.Percent == (float)0.5)
            {
                if (this.thumbnailViewBox1.Percent == (float)0.5)
                {
                    this.p50MenuItem.Checked = true;
                }
                else if (this.thumbnailViewBox1.Percent == (float)0.75)
                {
                    this.p75MenuItem.Checked = true;
                }
                else if (this.thumbnailViewBox1.Percent == (float)1)
                {
                    this.p100MenuItem.Checked = true;
                }
                //this.ToolStrip1MenuItem.Checked = true;
            }
            else if (r == ThumbnailMode.Size)
            {
                if (this.thumbnailViewBox1.ItemSize == new Size(100, 100))
                {
                    this.s100MenuItem.Checked = true;
                }
                else if (this.thumbnailViewBox1.ItemSize == new Size(150, 150))
                {
                    this.s150MenuItem.Checked = true;
                }
                else if (this.thumbnailViewBox1.ItemSize == new Size(125, 125))
                {
                    this.s125MenuItem.Checked = true;
                }
                else if (this.thumbnailViewBox1.ItemSize == new Size(200, 200))
                {
                    this.s200MenuItem.Checked = true;
                }
            }
            else
            {

            }
        }
        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.ViewModeCheck();
            
            this.addNewPageToolStripMenuItem.Visible = !this.EditLock;
            this.lockToolStripMenuItem.Visible = this.Setting.EditLockButtonVisible;

            this.pasteToolStripMenuItem1.Enabled = this.CanPaste;
            this.pasteToolStripMenuItem1.Visible = !this.EditLock;
            this.renameToolStripMenuItem.Visible = !this.EditLock;
            this.fileMergeToolStripMenuItem.Visible = !this.EditLock;
        }
        #endregion
        #region ThumbnailEventMethod
        private void thumbnailViewBox1_ImageBoxMouseEnter(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;


            if (this.thumbnailViewBox1.Operator.SelectedCluster.ClusterItem.Page != null)
            {
                var p = this.thumbnailViewBox1.Operator.SelectedCluster.ClusterItem.Page;
                if (this.ItemTouched != null) this.ItemTouched(p, new EventArgs());
            }
        }

        private void thumbnailViewBox1_SavedataChanged(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (this.SavedataChanged != null) this.SavedataChanged(this, e);
        }
        private void thumbnailViewBox1_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (this.ItemClicked != null) this.ItemClicked(sender, e);
            this.thumbnailViewBox1.ShowRawToolTip();
        }
        private void thumbnailViewBox1_MouseEnter(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.OnMouseEnter(e);
        }
        private void thumbnailViewBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (e.Data.GetDataPresent(System.Windows.Forms.DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        private void thumbnailViewBox1_DragDrop(object sender, DragEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var strs = (string[])e.Data.GetData(DataFormats.FileDrop);
                var f = new EditingFile(strs.First());
                this.Item = f;

            }
        }
        #endregion

        private void addFavoriteAAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.AddFavoriteButtonAction != null) this.AddFavoriteButtonAction(this.SelectedPage);
        }

        private void fileMergeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var book = this.EditorSmallInfo.OpenBookShowDialog();

            if (book != null)
            {
                var thisBook = this.Item;
                this.DataBook.StoryFile.ContentChanged -= this.Item_ItemChanged;
                if (thisBook is EditingFile)
                {
                    foreach (var v in book.BookPages)
                    {
                        thisBook.Add(v);
                    }
                }
                else if (thisBook is ObsoleteSectionBook)
                {
                    foreach (var v in book.BookPages)
                    {
                        thisBook.Add(v);
                    }
                }
                else
                {
                    throw new Exception();
                }
                this.DataBook.StoryFile.ContentChanged += this.Item_ItemChanged;
                this.RefreshImage();
            }
        }

        private void d2525ToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.thumbnailViewBox1.ItemSize = new Size(25, 25);
        }
        public void MyDispose()
        {
            this.thumbnailViewBox1.Operator.Clear();
        }
    }



}
