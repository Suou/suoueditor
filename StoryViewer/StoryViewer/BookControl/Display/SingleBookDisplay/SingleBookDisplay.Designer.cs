﻿namespace StoryViewer.BookControl.Display.SingleBookDisplay
{
    partial class SingleBookDisplay
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.CopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.renameToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addFavoriteAAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.s200MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.s150MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.s125MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.s100MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.p100MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.p75MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.p50MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.lockToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMethodToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.threadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tableToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMergeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thumbnailViewBox1 = new StoryViewer.ThumContViewBox();
            this.d2525ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemContext.SuspendLayout();
            this.ControlContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // ItemContext
            // 
            this.ItemContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CopyToolStripMenuItem,
            this.editToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.renameToolStripMenuItem,
            this.addFavoriteAAToolStripMenuItem});
            this.ItemContext.Name = "contextMenuStrip1";
            this.ItemContext.Size = new System.Drawing.Size(221, 136);
            this.ItemContext.Opening += new System.ComponentModel.CancelEventHandler(this.ItemContext_Opening);
            // 
            // CopyToolStripMenuItem
            // 
            this.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem";
            this.CopyToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.CopyToolStripMenuItem.Text = "コピー";
            this.CopyToolStripMenuItem.Click += new System.EventHandler(this.CopyToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.editToolStripMenuItem.Text = "編集";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.pasteToolStripMenuItem.Text = "貼り付け";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.deleteToolStripMenuItem.Text = "削除";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // renameToolStripMenuItem
            // 
            this.renameToolStripMenuItem.Name = "renameToolStripMenuItem";
            this.renameToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.renameToolStripMenuItem.Text = "名前を編集";
            this.renameToolStripMenuItem.Click += new System.EventHandler(this.renameToolStripMenuItem_Click);
            // 
            // addFavoriteAAToolStripMenuItem
            // 
            this.addFavoriteAAToolStripMenuItem.Name = "addFavoriteAAToolStripMenuItem";
            this.addFavoriteAAToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.addFavoriteAAToolStripMenuItem.Text = "お気に入りファイルに追加";
            this.addFavoriteAAToolStripMenuItem.Click += new System.EventHandler(this.addFavoriteAAToolStripMenuItem_Click);
            // 
            // ControlContext
            // 
            this.ControlContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewToolStripMenuItem,
            this.pasteToolStripMenuItem1,
            this.lockToolStripMenuItem,
            this.addNewPageToolStripMenuItem,
            this.viewMethodToolStripMenuItem,
            this.fileMergeToolStripMenuItem});
            this.ControlContext.Name = "contextMenuStrip2";
            this.ControlContext.Size = new System.Drawing.Size(197, 158);
            this.ControlContext.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.s200MenuItem,
            this.s150MenuItem,
            this.s125MenuItem,
            this.s100MenuItem,
            this.p100MenuItem,
            this.p75MenuItem,
            this.p50MenuItem,
            this.d2525ToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.viewToolStripMenuItem.Text = "表示";
            // 
            // s200MenuItem
            // 
            this.s200MenuItem.Name = "s200MenuItem";
            this.s200MenuItem.Size = new System.Drawing.Size(152, 22);
            this.s200MenuItem.Text = "200×200";
            this.s200MenuItem.Click += new System.EventHandler(this.ViewSize200Button_Click);
            // 
            // s150MenuItem
            // 
            this.s150MenuItem.Name = "s150MenuItem";
            this.s150MenuItem.Size = new System.Drawing.Size(152, 22);
            this.s150MenuItem.Text = "150×150";
            this.s150MenuItem.Click += new System.EventHandler(this.ViewSize150Button_Click);
            // 
            // s125MenuItem
            // 
            this.s125MenuItem.Name = "s125MenuItem";
            this.s125MenuItem.Size = new System.Drawing.Size(152, 22);
            this.s125MenuItem.Text = "125×125";
            this.s125MenuItem.Click += new System.EventHandler(this.ViewSize125Button_Click);
            // 
            // s100MenuItem
            // 
            this.s100MenuItem.Name = "s100MenuItem";
            this.s100MenuItem.Size = new System.Drawing.Size(152, 22);
            this.s100MenuItem.Text = "100×100";
            this.s100MenuItem.Click += new System.EventHandler(this.ViewSize100Button_Click);
            // 
            // p100MenuItem
            // 
            this.p100MenuItem.Name = "p100MenuItem";
            this.p100MenuItem.Size = new System.Drawing.Size(152, 22);
            this.p100MenuItem.Text = "100%";
            this.p100MenuItem.Click += new System.EventHandler(this.ViewPercent100Button_Click);
            // 
            // p75MenuItem
            // 
            this.p75MenuItem.Name = "p75MenuItem";
            this.p75MenuItem.Size = new System.Drawing.Size(152, 22);
            this.p75MenuItem.Text = "75%";
            this.p75MenuItem.Click += new System.EventHandler(this.ViewPercent75Button_Click);
            // 
            // p50MenuItem
            // 
            this.p50MenuItem.Name = "p50MenuItem";
            this.p50MenuItem.Size = new System.Drawing.Size(152, 22);
            this.p50MenuItem.Text = "50%";
            this.p50MenuItem.Click += new System.EventHandler(this.ViewPercent50Button_Click);
            // 
            // pasteToolStripMenuItem1
            // 
            this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
            this.pasteToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.pasteToolStripMenuItem1.Text = "貼り付け";
            this.pasteToolStripMenuItem1.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // lockToolStripMenuItem
            // 
            this.lockToolStripMenuItem.Name = "lockToolStripMenuItem";
            this.lockToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.lockToolStripMenuItem.Text = "編集ロック";
            this.lockToolStripMenuItem.Click += new System.EventHandler(this.lockToolStripMenuItem_Click);
            // 
            // addNewPageToolStripMenuItem
            // 
            this.addNewPageToolStripMenuItem.Name = "addNewPageToolStripMenuItem";
            this.addNewPageToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.addNewPageToolStripMenuItem.Text = "新しいページを追加";
            this.addNewPageToolStripMenuItem.Click += new System.EventHandler(this.addNewPageToolStripMenuItem_Click);
            // 
            // viewMethodToolStripMenuItem
            // 
            this.viewMethodToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.threadToolStripMenuItem,
            this.tableToolStripMenuItem});
            this.viewMethodToolStripMenuItem.Name = "viewMethodToolStripMenuItem";
            this.viewMethodToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.viewMethodToolStripMenuItem.Text = "ViewMethod";
            this.viewMethodToolStripMenuItem.Visible = false;
            // 
            // threadToolStripMenuItem
            // 
            this.threadToolStripMenuItem.Name = "threadToolStripMenuItem";
            this.threadToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.threadToolStripMenuItem.Text = "Thread";
            this.threadToolStripMenuItem.Click += new System.EventHandler(this.threadToolStripMenuItem_Click);
            // 
            // tableToolStripMenuItem
            // 
            this.tableToolStripMenuItem.Name = "tableToolStripMenuItem";
            this.tableToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tableToolStripMenuItem.Text = "Table";
            this.tableToolStripMenuItem.Click += new System.EventHandler(this.tableToolStripMenuItem_Click);
            // 
            // fileMergeToolStripMenuItem
            // 
            this.fileMergeToolStripMenuItem.Name = "fileMergeToolStripMenuItem";
            this.fileMergeToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.fileMergeToolStripMenuItem.Text = "ファイルをマージする";
            this.fileMergeToolStripMenuItem.Click += new System.EventHandler(this.fileMergeToolStripMenuItem_Click);
            // 
            // thumbnailViewBox1
            // 
            this.thumbnailViewBox1.AllowDrop = true;
            this.thumbnailViewBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.thumbnailViewBox1.BoxType = StoryViewer.ImageItemBoxType.Normal;
            this.thumbnailViewBox1.CanItemDrag = true;
            this.thumbnailViewBox1.CanItemMove = false;
            this.thumbnailViewBox1.ControlContext = this.ControlContext;
            this.thumbnailViewBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumbnailViewBox1.EnableNameTextBox = StoryViewer.NameTextBoxType.None;
            this.thumbnailViewBox1.IsLoose = false;
            this.thumbnailViewBox1.IsMouseMoveFocus = false;
            this.thumbnailViewBox1.IsTransparentDrag = false;
            this.thumbnailViewBox1.ItemContext = this.ItemContext;
            this.thumbnailViewBox1.ItemInterval = 5;
            this.thumbnailViewBox1.ItemSize = new System.Drawing.Size(100, 100);
            this.thumbnailViewBox1.Location = new System.Drawing.Point(0, 0);
            this.thumbnailViewBox1.ManualDragDo = false;
            this.thumbnailViewBox1.MinItemSize = new System.Drawing.Size(16, 16);
            this.thumbnailViewBox1.MouseDownItem = null;
            this.thumbnailViewBox1.Name = "thumbnailViewBox1";
            this.thumbnailViewBox1.OrderMode = StoryViewer.ThumbnailOrder.SmallTile;
            this.thumbnailViewBox1.Percent = 0.5F;
            this.thumbnailViewBox1.PermitCategory = true;
            this.thumbnailViewBox1.ShiftItemContext = null;
            this.thumbnailViewBox1.Size = new System.Drawing.Size(208, 212);
            this.thumbnailViewBox1.TabIndex = 0;
            this.thumbnailViewBox1.ViewMode = SuouBase.Drawing.ThumbnailMode.Size;
            this.thumbnailViewBox1.ItemClicked += new StoryViewer.ThumbnailViewEventHandler(this.thumbnailViewBox1_ItemClicked);
            this.thumbnailViewBox1.ItemDragStarting += new StoryViewer.ThumbnailViewEventHandler(this.thumbnailViewBox1_ItemDragStarting);
            this.thumbnailViewBox1.SavedataChanged += new System.EventHandler(this.thumbnailViewBox1_SavedataChanged);
            this.thumbnailViewBox1.ImageBoxMouseEnter += new System.EventHandler(this.thumbnailViewBox1_ImageBoxMouseEnter);
            this.thumbnailViewBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.thumbnailViewBox1_DragDrop);
            this.thumbnailViewBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.thumbnailViewBox1_DragEnter);
            this.thumbnailViewBox1.MouseEnter += new System.EventHandler(this.thumbnailViewBox1_MouseEnter);
            // 
            // d2525ToolStripMenuItem
            // 
            this.d2525ToolStripMenuItem.Name = "d2525ToolStripMenuItem";
            this.d2525ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.d2525ToolStripMenuItem.Text = "25×25";
            this.d2525ToolStripMenuItem.Click += new System.EventHandler(this.d2525ToolStripMenuItem_Click);
            // 
            // SingleBookDisplay
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.thumbnailViewBox1);
            this.Name = "SingleBookDisplay";
            this.Size = new System.Drawing.Size(208, 212);
            this.ItemContext.ResumeLayout(false);
            this.ControlContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ThumContViewBox thumbnailViewBox1;
        private System.Windows.Forms.ContextMenuStrip ItemContext;
        private System.Windows.Forms.ToolStripMenuItem CopyToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ControlContext;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem p100MenuItem;
        private System.Windows.Forms.ToolStripMenuItem s200MenuItem;
        private System.Windows.Forms.ToolStripMenuItem s150MenuItem;
        private System.Windows.Forms.ToolStripMenuItem s125MenuItem;
        private System.Windows.Forms.ToolStripMenuItem s100MenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem p50MenuItem;
        private System.Windows.Forms.ToolStripMenuItem lockToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem p75MenuItem;
        private System.Windows.Forms.ToolStripMenuItem renameToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMethodToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem threadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tableToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addFavoriteAAToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fileMergeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem d2525ToolStripMenuItem;
    }
}
