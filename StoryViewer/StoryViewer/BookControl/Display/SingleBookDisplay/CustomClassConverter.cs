﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using SuouBase.Drawing;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using System.Globalization;
namespace StoryViewer.BookControl.Display.SingleBookDisplay
{
    public class CustomClassConverter : ExpandableObjectConverter
    {
        //コンバータがオブジェクトを指定した型に変換できるか
        //（変換できる時はTrueを返す）
        //ここでは、CustomClass型のオブジェクトには変換可能とする
        public override bool CanConvertTo(
            ITypeDescriptorContext context, Type destinationType)
        {
            if (destinationType == typeof(BookDisplaySetting))
                return true;
            return base.CanConvertTo(context, destinationType);
        }

        //指定した値オブジェクトを、指定した型に変換する
        //CustomClass型のオブジェクトをString型に変換する方法を提供する
        public override object ConvertTo(ITypeDescriptorContext context,
            CultureInfo culture, object value, Type destinationType)
        {
            if (destinationType == typeof(string) &&
                value is BookDisplaySetting)
            {
                BookDisplaySetting cc = (BookDisplaySetting)value;
                return cc.EditLockButtonVisible.ToString() + "," + cc.IsSaveCheck;
            }
            return base.ConvertTo(context, culture, value, destinationType);
        }

        //コンバータが特定の型のオブジェクトをコンバータの型に変換できるか
        //（変換できる時はTrueを返す）
        //ここでは、String型のオブジェクトなら変換可能とする
        public override bool CanConvertFrom(
            ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
                return true;
            return base.CanConvertFrom(context, sourceType);
        }

        //指定した値をコンバータの型に変換する
        //String型のオブジェクトをCustomClass型に変換する方法を提供する
        public override object ConvertFrom(ITypeDescriptorContext context,
            CultureInfo culture, object value)
        {
            if (value is string)
            {
                string[] ss = value.ToString().Split(new char[] { ',' }, 2);
                BookDisplaySetting cc = new BookDisplaySetting();
                cc.EditLockButtonVisible = bool.Parse(ss[0]);
                cc.IsSaveCheck = bool.Parse(ss[1]);

                return cc;
            }
            return base.ConvertFrom(context, culture, value);
        }
    }

    [TypeConverter(typeof(CustomClassConverter))]
    public class BookDisplaySetting
    {
        public bool IsSaveCheck { get; set; }
        public bool EditLockButtonVisible { get; set; }
    }
}
