﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer
{
    [ToolboxItem(false)]
    class AAThumContTabPage:TabPage
    {
        public IBook File { get; set; }
        public StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay Display = new StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay();
        public EditorInfo DLLFunction { get; set; }

        public AAThumContTabPage(IBook ef,EditorInfo DLLFunction,ThumbnailView_SaveData savedata)
        {

            this.Display.SetSaveData(savedata);
            this.File = ef;
            Display.Item = ef;

            this.Controls.Add(Display);
            Display.Dock = DockStyle.Fill;

            Display.EditorSmallInfo = DLLFunction.Info;
            this.Text = this.Display.DataBook.Title;
            this.Display.DataBook.DataChanged += this.DataChanged;
        }
        private void DataChanged(object sender, EventArgs e)
        {
            this.Text = this.Display.DataBook.Title;
        }
        public void SetSaveData(ThumbnailView_SaveData save)
        {
            this.Display.SetSaveData(save);
        }
        public void Clear()
        {
            this.File = null;
            this.DLLFunction = null;
            this.Controls.Remove(this.Display);
            this.Display.Clear();
        }
        public void ClearImage()
        {
            this.Display.ClearImage();
        }
        public bool Close()
        {
            return this.Display.Close();
        }
    }
}
