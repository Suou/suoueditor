﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer.BookControl.Display.TabBooksDisplay
{
    [ToolboxItem(false)]
    public partial class TabBooksDisplay : UserControl,IBookDisplay
    {
        public TabBooksDisplay()
        {
            InitializeComponent();
            CheckTabControl();

        }
        public TabBooksDisplay(EditorInfo DLLFunction):this()
        {
            this.DLLFunction = DLLFunction;
        }
        private ThumbnailView_SaveData _ThumbSaveData = new ThumbnailView_SaveData();
        public ThumbnailView_SaveData ThumbSaveData
        {
            get { return this._ThumbSaveData; }
            set
            {
                if (value != null)
                {
                    this._ThumbSaveData = value;
                    foreach (AAThumContTabPage v in this.tabControl1.TabPages)
                    {
                        v.SetSaveData(this._ThumbSaveData);
                    }
                }
            }
        }
        EditorInfo _DLLFunction;
        public EditorInfo DLLFunction
        {
            get { return _DLLFunction; }
            set
            {
                _DLLFunction = value;
                for (int i = 0; i < this.tabControl1.TabCount; i++)
                {
                    var f = (AAThumContTabPage)this.tabControl1.TabPages[i];
                    f.Display.EditorSmallInfo = DLLFunction.Info;
                }
            }
        }

        public IBook SelectedFile
        {
            get
            {
                var stab = this.tabControl1.SelectedTab;
                if (stab == null) return null;
                var ef = ((AAThumContTabPage)stab).File;
                return ef;
            }
        }
        private AAThumContTabPage SelectedTabPage
        {
            get
            {
                var stab = this.tabControl1.SelectedTab;
                if (stab == null) return null;
                var ef = ((AAThumContTabPage)stab);
                return ef;
            }
        }
        private int FindEditingFileIndex(IBook ef)
        {
            var tp = this.tabControl1.TabPages;
            for (int i = 0; i < tp.Count; i++)
            {
                if (((AAThumContTabPage)tp[i]).File == ef) return i;
            }
            return -1;
        }
        private bool ContainEditingFile(IBook ef)
        {
            var n = this.FindEditingFileIndex(ef);
            return n != -1;
        }
        private AAThumContTabPage GetTabpage(IBook ef)
        {
            var n = this.FindEditingFileIndex(ef);
            if (n != -1)
            {
                return (AAThumContTabPage)this.tabControl1.TabPages[n];
            }
            else
            {
            } return null;
        }
        public void Add(IBook ef)
        {
            if (!this.ContainEditingFile(ef))
            {
                var tp = new AAThumContTabPage(ef, DLLFunction,this.ThumbSaveData);
                this.tabControl1.TabPages.Add(tp);
                this.tabControl1.SelectedTab = tp;
               
            }
            else
            {
                var v = this.GetTabpage(ef);
                this.tabControl1.SelectedTab = v;
            }
        }
        public void SetBook(IBook ef)
        {
            this.Add(ef);
        }
        public void SetBook(IBook ef,bool isEdit)
        {
            throw new NotSupportedException();
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            var f = this.tabControl1.SelectedTab;
            var fq = (AAThumContTabPage)f;
            if (fq.Display.Close())
            {
                this.tabControl1.TabPages.Remove(f);
                fq.Clear();
            }
        }
        private bool CloseTabItem(AAThumContTabPage item)
        {
            if (item.Display.Close())
            {
                this.tabControl1.TabPages.Remove(item);
                item.Clear();
                return true;
            }
            return false;
        }
        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                for (int i = 0; i < tabControl1.TabCount; i++)
                {
                    //タブとマウス位置を比較し、クリックしたタブを選択
                    if (tabControl1.GetTabRect(i).Contains(e.X, e.Y))
                    {
                        ControlContext.Show(this.tabControl1,e.Location);
                        tabControl1.SelectedTab = tabControl1.TabPages[i];
                        break;
                    }
                }
            }
        }

        private void tabControl1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.Forms.DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Move;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void tabControl1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var strs = (string[])e.Data.GetData(DataFormats.FileDrop);

                //AALoad(strs.First());
                var f = new EditingFile(strs.First());
                this.Add(f);
                

            }
        }

        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            //対象のTabControlを取得
            TabControl tab = (TabControl)sender;
            TabPage page = tab.TabPages[e.Index];
            //タブページのテキストを取得
            string txt = page.Text;
            //StringFormatを作成
            StringFormat sf = new StringFormat();
            //縦書きにする
            sf.FormatFlags = StringFormatFlags.DirectionVertical;
            //ついでに、水平垂直方向の中央に、行が完全に表示されるようにする
            sf.LineAlignment = StringAlignment.Center;
            sf.Alignment = StringAlignment.Center;
            sf.FormatFlags |= StringFormatFlags.LineLimit;

            //背景の描画
            Brush backBrush = new SolidBrush(page.BackColor);
            e.Graphics.FillRectangle(backBrush, e.Bounds);
            backBrush.Dispose();

            //Textの描画
            Brush foreBrush = new SolidBrush(page.ForeColor);
            e.Graphics.DrawString(txt, this.tabControl1.Font, foreBrush, e.Bounds, sf);
            foreBrush.Dispose();
        }
        private void CheckTabControl()
        {
            if (this.tabControl1.TabPages.Count == 0)
            {
                this.tabControl1.Visible = false;
            }
            else
            {
                this.tabControl1.Visible = true;
            }
        }

        private void tabControl1_ControlAdded(object sender, ControlEventArgs e)
        {
            CheckTabControl();
        }

        private void tabControl1_ControlRemoved(object sender, ControlEventArgs e)
        {
            CheckTabControl();
        }

        TabPage PreSelectedTabPage { get; set; }
        AAThumContTabPage[] TabPages
        {
            get
            {
                var v = new List<AAThumContTabPage>();
                for (int i = 0; i < this.tabControl1.TabCount; i++)
                {
                    v.Add((AAThumContTabPage)this.tabControl1.TabPages[i]);
                }
                return v.ToArray();
            }
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.PreSelectedTabPage != null)
            {
                var tabpage = (AAThumContTabPage)this.PreSelectedTabPage;
                tabpage.ClearImage();
            }
            this.PreSelectedTabPage = this.tabControl1.SelectedTab;

            CheckTabControl();
        }
        public bool Close()
        {
            return this.TabPages.All((v) => this.CloseTabItem(v));
        }

        private void addNewPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = new ObsoleteSectionBook();
            this.Add(f);
        }

        private void overWriteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var f = this.SelectedTabPage;
            f.Display.DataBook.SaveOverRide();
        }

        private void ControlContext_Opening(object sender, CancelEventArgs e)
        {
            var f = this.SelectedTabPage;
            this.overWriteToolStripMenuItem.Visible = !f.Display.EditLock;
        }

        private void テストToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
    }
}
