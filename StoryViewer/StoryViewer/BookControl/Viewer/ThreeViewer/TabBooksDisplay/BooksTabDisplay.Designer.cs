﻿namespace StoryViewer.BookControl.Display.TabBooksDisplay
{
    partial class TabBooksDisplay
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.テストToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ControlContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overWriteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.ItemContext.SuspendLayout();
            this.ControlContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // ItemContext
            // 
            this.ItemContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.テストToolStripMenuItem});
            this.ItemContext.Name = "contextMenuStrip1";
            this.ItemContext.Size = new System.Drawing.Size(153, 48);
            // 
            // テストToolStripMenuItem
            // 
            this.テストToolStripMenuItem.Name = "テストToolStripMenuItem";
            this.テストToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.テストToolStripMenuItem.Text = "コピー";
            this.テストToolStripMenuItem.Click += new System.EventHandler(this.テストToolStripMenuItem_Click);
            // 
            // ControlContext
            // 
            this.ControlContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem,
            this.addNewPageToolStripMenuItem,
            this.overWriteToolStripMenuItem});
            this.ControlContext.Name = "contextMenuStrip2";
            this.ControlContext.Size = new System.Drawing.Size(197, 70);
            this.ControlContext.Opening += new System.ComponentModel.CancelEventHandler(this.ControlContext_Opening);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.closeToolStripMenuItem.Text = "タブを閉じる";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // addNewPageToolStripMenuItem
            // 
            this.addNewPageToolStripMenuItem.Name = "addNewPageToolStripMenuItem";
            this.addNewPageToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.addNewPageToolStripMenuItem.Text = "新しいファイルを作成";
            this.addNewPageToolStripMenuItem.Click += new System.EventHandler(this.addNewPageToolStripMenuItem_Click);
            // 
            // overWriteToolStripMenuItem
            // 
            this.overWriteToolStripMenuItem.Name = "overWriteToolStripMenuItem";
            this.overWriteToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.overWriteToolStripMenuItem.Text = "保存";
            this.overWriteToolStripMenuItem.Click += new System.EventHandler(this.overWriteToolStripMenuItem_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Alignment = System.Windows.Forms.TabAlignment.Left;
            this.tabControl1.AllowDrop = true;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.DrawMode = System.Windows.Forms.TabDrawMode.OwnerDrawFixed;
            this.tabControl1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tabControl1.ItemSize = new System.Drawing.Size(18, 16);
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(418, 372);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.tabControl1_DrawItem);
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.tabControl1_ControlAdded);
            this.tabControl1.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.tabControl1_ControlRemoved);
            this.tabControl1.DragDrop += new System.Windows.Forms.DragEventHandler(this.tabControl1_DragDrop);
            this.tabControl1.DragEnter += new System.Windows.Forms.DragEventHandler(this.tabControl1_DragEnter);
            this.tabControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tabControl1_MouseDown);
            // 
            // AAThumContViewCControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.tabControl1);
            this.Name = "AAThumContViewCControl";
            this.Size = new System.Drawing.Size(418, 372);
            this.ItemContext.ResumeLayout(false);
            this.ControlContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip ItemContext;
        private System.Windows.Forms.ToolStripMenuItem テストToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip ControlContext;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem overWriteToolStripMenuItem;
    }
}
