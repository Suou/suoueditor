﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using SuouBase.Book;
using SuouBase.Interface;

using SuouBase.Text;
namespace StoryViewer
{
    [IComponentPlugin("AAThumbnailViewCPlugin", PluginType.Control)]
    public class AAThumbnailViewCPlugin:ITemplatePageListClass
    {
        public IBookPage SelectPage(Form form, string text)
        {
            throw new Exception();
        }
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public AAThumbnailViewCPlugin(EditorInfo pd)
        {
            this.DLLFunction = pd;
            this.Ctrl.DLLFunction = pd;
            Ctrl.BackColor = Color.Gray;
            pd.TemplatePageViewPlugin = this;
            Load();
        }

        public SettingPage[] SettingPage { get { return null; } }

        private EditorInfo DLLFunction
        {
            get { return this.Ctrl.DLLFunction; }
            set { this.Ctrl.DLLFunction = value; }
        }
        private StoryViewer.BookControl.Display.TabBooksDisplay.TabBooksDisplay Ctrl = new StoryViewer.BookControl.Display.TabBooksDisplay.TabBooksDisplay();

        public IBook Item
        {
            get { return this.Ctrl.SelectedFile; }
            set
            {
                this.Ctrl.Add(value);
            }
        }
        public Font Font = new Font("ＭＳ Ｐゴシック", 12);
        public Form Form { get { return null; } }
        public ContainerControl Control
        {
            get { return this.Ctrl; }
        }
        public bool DisposeCheck()
        {
            this.Ctrl.Close();
            Save();
            return true;
        }
        public string SaveDataFile = SuouEditorConst.GetSettingFile("AAThumbnailViewC.stn");
        public void Save()
        {
            BSystem.XML.BinarySerialize.SaveToBinaryFile(this.Ctrl.ThumbSaveData, SaveDataFile);
        }
        public void Load()
        {
            var f = BSystem.XML.BinarySerialize.LoadFromBinaryFile<ThumbnailView_SaveData>(SaveDataFile);
            if (f == null) return;
            this.Ctrl.ThumbSaveData = ((ThumbnailView_SaveData)f);
        }
        public event EventHandler FocusNotifiedEventHandler;
        public void FocusNotify()
        {
            if (FocusNotifiedEventHandler != null) FocusNotifiedEventHandler(this, new EventArgs());
        }
    }
}
