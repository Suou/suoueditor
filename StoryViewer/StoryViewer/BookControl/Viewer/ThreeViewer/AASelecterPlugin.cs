﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using AAEditTabControl;
using AAEditTabControl.SimpleGlobalTemplate;
using SuouBase;
using System.Drawing;
using SuouBase.Text;
using SuouBase.Files;
using System.Diagnostics;
using SuouBase.Book;

namespace StoryViewer.AASelecter
{
    [IComponentPlugin("AASelecter", PluginType.Control)]
    public class AASelecterPlugin:IComponentPlugin,IAASelectClass
    {
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public string Name
        {
            get
            {
                return "AA選択";
            }
        }
        public static string SaveFileName = SuouEditorConst.SettingDirectory + "/AASelecter.stn";
        #region BaseProperty
        public SuouFileTreeCollection GlobalFolders = null;
        public SuouFileTreeCollection RegisterFolders = null;
        public AAThumbnailViewCPlugin ThumbnailPlugin = null;
        public BookFoldersDisplay GlobalBrowserControl;
        public BookFoldersDisplay FavoriteBrowserControl;
        public DropDownItem GlobalDropDown = null;
        public DropDownItem RegisterDropDown = null;
        public BookDictionary BookDic = new BookDictionary();
        public bool IsAppendSuperVisorMenubar = false;

        public EditorInfo DLLFunction { get; set; }
        #endregion
        #region Property
        public IBook HistoryBook
        {
            get { return this.HaveControl.HistoryBook; }
        }
        private AASelecterControl HaveControl = null;
        public ContainerControl Control
        {
            get { return HaveControl; }
        }
        public Form Form
        {
            get { return null; }
        }
        public SettingPage[] SettingPage
        {
            get
            {
                List<SettingPage> r = new List<SettingPage>();
                if (this.ThumbnailPlugin.SettingPage != null) r.AddRange(this.ThumbnailPlugin.SettingPage);
                return r.ToArray();
            }
        }
        #endregion
        #region Method
        public void NotifyDropPageItem(IBookPage page){
            this.HistoryBook.Insert(0, page);
        }
        public bool DisposeCheck()
        {
            if (this.HistoryBook.Count > 150)
            {
                for (int i = this.HistoryBook.Count-1; i >= 150; i--)
                {
                    this.HaveControl.HistoryBook.Pages.RemoveAt(i);
                }
            }
            //GlobalFolders.Dispose();
            //RegisterFolders.Dispose();
            ThumbnailPlugin.DisposeCheck();
            this.Save();
            return true;
        }
        public void Save()
        {
            var f = new AASelecterSaveData() { TempleControlSize = this.HaveControl.GlobalSize,
                HistoryBook = this.HaveControl.HistoryBook,GlobalWebSize = this.GlobalBrowserControl.Size,RegisterWebSize = this.FavoriteBrowserControl.Size };
            BSystem.XML.BinarySerialize.SaveToBinaryFile(f, AASelecterPlugin.SaveFileName);
        }
        public static AASelecterSaveData Load()
        {
            var f = BSystem.XML.BinarySerialize.LoadFromBinaryFile<AASelecterSaveData>(AASelecterPlugin.SaveFileName);
            if (f == null)
            {
                return null;
            }
            else
            {
                return (AASelecterSaveData)f;
            }
        }
        
        #endregion
        #region Constructor

        public AASelecterPlugin()
        {



        }
        public AASelecterPlugin(EditorInfo pd)
            : this()
        {
            
            //GlobalFolders = new SimpleGlobalTemplate(pd);
            //RegisterFolders = new LocalTemplateOperation(pd, SuouEditorConst.GetSettingFile("registerAAplugin.stn"));
            ThumbnailPlugin = new AAThumbnailViewCPlugin(pd);
            this.GlobalBrowserControl = new BookFoldersDisplay(GlobalFolders,AATemplateType.Global);
            //this.GlobalBrowserControl.TreeItemAdded += this.BookFileItem_Clicked;
            this.GlobalBrowserControl.MltItemRightClicked += this.BookFileItem_RegisterAdd;
            //this.GlobalBrowserControl.AddFolderEvented += this.Global_AddFolderEvented;

            this.FavoriteBrowserControl = new BookFoldersDisplay(RegisterFolders,AATemplateType.Register);
            this.FavoriteBrowserControl.MltItemRightClicked += this.BookFileItem_DeleteNode;
            //this.FavoriteBrowserControl.TreeItemAdded += this.BookFileItem_Clicked;

            //GlobalFolders.Control.Dock = System.Windows.Forms.DockStyle.None;

            //HaveControl = new AASelecterControl(GlobalFolders.Control, (BooksTabDisplay)ThumbnailPlugin.Control,RegisterFolders.Control) { Dock = DockStyle.Fill };

            //pd.GlobalTemplatePlugin = GlobalFolders;
            //pd.RegisterTemplatePlugin = RegisterFolders;

            
            var data = AASelecterPlugin.Load();
            if (data != null)
            {
                this.HaveControl.HistoryBook = data.HistoryBook;
                this.GlobalBrowserControl.MinimumSize = data.GlobalWebSize;
                this.GlobalBrowserControl.Size = data.GlobalWebSize;
                this.FavoriteBrowserControl.Size = data.RegisterWebSize;
                this.FavoriteBrowserControl.MinimumSize = data.RegisterWebSize;
            }
            if (this.HaveControl.HistoryBook == null)
            {
                this.HaveControl.HistoryBook = new ObsoleteSectionBook() { Name = "履歴",Pages = new BSystem.Collections.SuperObservableCollection<SectionScene>()};
                var size = new Size(300, 300);
                this.GlobalBrowserControl.MinimumSize = size;
                this.GlobalBrowserControl.Size = size;
                this.FavoriteBrowserControl.Size = size;
                this.FavoriteBrowserControl.MinimumSize = size;
            }
            this.DLLFunction = pd;
            this.BookDic = this.DLLFunction.Info.IBookDictionary;
            var manustrip = new ToolStripMenuItem();
            manustrip.Text = "AAテンプレート";
            var favoritestrip = new ToolStripMenuItem();
            favoritestrip.Text = "お気に入り";
            var Historystrip = new ToolStripMenuItem();
            Historystrip.Text = "履歴";
            Historystrip.Click += this.HistoryStrip_Clicked;

            GlobalDropDown = new DropDownItem(this.GlobalBrowserControl);
            manustrip.DropDown = GlobalDropDown;

            
            RegisterDropDown = new DropDownItem(this.FavoriteBrowserControl);
            favoritestrip.DropDown = RegisterDropDown;

            if (IsAppendSuperVisorMenubar)
            {
                this.DLLFunction.MasterForm.Form.MainMenuStrip.Items.Add(manustrip);
                this.DLLFunction.MasterForm.Form.MainMenuStrip.Items.Add(favoritestrip);
                this.DLLFunction.MasterForm.Form.MainMenuStrip.Items.Add(Historystrip);
            }
            else
            {
                this.HaveControl.menuStrip2.Items.Add(manustrip);
                this.HaveControl.menuStrip2.Items.Add(favoritestrip);
                this.HaveControl.menuStrip2.Items.Add(Historystrip);
            }
            
        }
        #endregion
        #region Event
        private void HistoryStrip_Clicked(object sender, EventArgs e)
        {
            if (this.HistoryBook == null) throw new Exception();
            DLLFunction.TemplatePageViewPlugin.Item = this.HistoryBook;
            this.DLLFunction.Framework.Focus(this);
        }
        private void BookFileItem_Clicked(object sender, EventArgs e)
        {
            var filename = (string)sender;
            var f = BookDic.GetBook(filename);
            GlobalDropDown.Close();
            RegisterDropDown.Close();
            this.DLLFunction.TemplatePageViewPlugin.Item = f;

            this.DLLFunction.Framework.Focus(this);
        }
        private void BookFileItem_RegisterAdd(object sender, EventArgs e)
        {
            var filenode = (TreeNode)sender;
            
            if (filenode != null)
            {
                this.DLLFunction.RegisterTemplatePlugin.AddNode(filenode);
                this.FavoriteBrowserControl.RefreshTab();
                this.FavoriteBrowserControl.RefreshItem();
                var f = new System.IO.FileInfo(filenode.Name);
                MessageBox.Show("登録しました。" +"["+ f.Name +"]");
                //this.FavoriteBrowserControl.ItemShow();
                
            }
            else
            {
                MessageBox.Show("Error");
            }
            
        }
        private void BookFileItem_DeleteNode(object sender, EventArgs e)
        {
            var filenode = (TreeNode)sender;

            if (filenode != null)
            {
                filenode.Remove();
                this.FavoriteBrowserControl.RefreshTab();
            }
            else
            {
                MessageBox.Show("Error");
            }

        }
        private void Global_AddFolderEvented(object sender, EventArgs e)
        {
            /*
            var f = (GlobalTreeControl)this.GlobalFolders.Control;
            f.OpenMLTFolder();
            this.GlobalBrowserControl.RefreshItem();
            */
        }
        #endregion
    }
    [Serializable]
    public class AASelecterSaveData
    {
        public Size GlobalWebSize { get; set; }
        public Size RegisterWebSize { get; set; }
        public ObsoleteSectionBook HistoryBook { get; set; }
        public Size TempleControlSize { get; set; }
    }
}
