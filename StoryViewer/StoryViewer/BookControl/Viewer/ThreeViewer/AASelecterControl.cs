﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AAEditTabControl.SimpleGlobalTemplate;
using AAEditTabControl;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer.AASelecter
{
    [ToolboxItem(false)]
    public partial class AASelecterControl : UserControl
    {
        private UserControl GlobalControl { get; set; }
        private UserControl RegisterControl { get; set; }

        //public DropDownItem GlobalDropDown { get; set; }
        //public DropDownItem RegisterDropDown { get; set; }

        private StoryViewer.BookControl.Display.TabBooksDisplay.TabBooksDisplay CenterControl { get; set; }
        public ObsoleteSectionBook HistoryBook { get; set; }

        public AASelecterControl()
        {
            InitializeComponent();
        }
        public AASelecterControl(Control GlobalCtrl, StoryViewer.BookControl.Display.TabBooksDisplay.TabBooksDisplay CenterCtrl, Control RegisterCtrl)
            : this()
        {
            this.GlobalControl = (UserControl)GlobalCtrl;

            this.CenterControl = CenterCtrl;
            this.toolStripContainer1.ContentPanel.Controls.Add(CenterControl);
            this.CenterControl.Dock = DockStyle.Fill;
            this.CenterControl.BringToFront();
            this.CenterControl.Visible = true;
            
            
        }
        public void SetGlobalControl(Control GlobalCtrl)
        {
        }
        
        public Size GlobalSize
        {
            get { return GlobalControl.Size; }
            set
            {
                GlobalControl.Size = value;
            }
        }
       


        private void historyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.HistoryBook == null) throw new Exception();
            this.CenterControl.Add(this.HistoryBook);   
        }

        private void registerToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {

        }

        private void registerToolStripMenuItem_DropDownClosed(object sender, EventArgs e)
        {

        }

        private void registerToolStripMenuItem_DropDownItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            
        }

        private void menuStrip2_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            
            LinearGradientBrush gb = new LinearGradientBrush(g.VisibleClipBounds, Color.LightYellow, Color.LightGreen, LinearGradientMode.Horizontal);

            //四角を描く
            g.FillRectangle(gb, g.VisibleClipBounds);
        }

       
    }
}
