﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.IO;
using SuouBase.Text;
namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    public class IndexSupervisor
    {
        public DirectoryInfo IndexFolderInfo { get; private set; }
        public FileInfo SupervisorFile { get; set; }
        public Dictionary<string, int> Dic = new Dictionary<string, int>();

        public IndexSupervisor(string foldername)
        {
            this.IndexFolderInfo = new DirectoryInfo(foldername);
            
            var supervisorFileName = foldername + "/" + "dic.obj";
            this.SupervisorFile = new FileInfo(supervisorFileName);
            if (!this.SupervisorFile.Directory.Exists) this.SupervisorFile.Directory.Create();
            if (this.SupervisorFile.Exists)
            {
                this.LoadSupervisorFile();
            }
        }
        private void LoadSupervisorFile()
        {
            this.Dic = BSystem.XML.BinarySerialize.LoadFromBinaryFile<Dictionary<string, int>>(this.SupervisorFile.FullName);
        }
        private void SaveSupervisorFile()
        {
            BSystem.XML.BinarySerialize.SaveToBinaryFile(this.Dic,this.SupervisorFile.FullName);

        }

        public bool CheckExistFile(string fullpath)
        {
            if (this.Dic.ContainsKey(fullpath))
            {
                return true;
                /*
                var name = this.Dic[fullpath].ToString();
                if (Directory.Exists(fullpath))
                {
                    var info = new DirectoryInfo(fullpath);
                    var v = new IndexCreaterWithLineHashSearchInDirectory(info, this.IndexFolderInfo, name);
                    var b = v.IsCreatedFileList && v.IsCreatedIndex;
                    v.Dispose();
                    return b;
                }
                else
                {
                    var info = new FileInfo(fullpath);
                    var v = new IndexCreaterWithLineHashSearchInFile(info, this.IndexFolderInfo, name);
                    var b = v.IsCreatedFileList && v.IsCreatedIndex;
                    v.Dispose();
                    return b;
                }
                */
                
            }
            else
            {
                return false;
            }
        }

        public string CreateNewNumber(string fullpath)
        {
            if (this.Dic.ContainsKey(fullpath))
            {
            }
            else
            {
                this.Dic[fullpath] = this.Dic.Count > 0 ? this.Dic.Values.Max() + 1 : 0;
                
            }
            return this.Dic[fullpath].ToString();
        }
        public bool DisposeCheck()
        {
            this.SaveSupervisorFile();
            return true;
        }
    }

}
