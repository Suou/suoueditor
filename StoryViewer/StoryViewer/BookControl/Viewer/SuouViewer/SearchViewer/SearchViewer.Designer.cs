﻿namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    partial class SearchViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            StoryViewer.BookControl.Display.SingleBookDisplay.BookDisplaySetting bookDisplaySetting1 = new StoryViewer.BookControl.Display.SingleBookDisplay.BookDisplaySetting();
            this.groupBox1 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.searchResultButton = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.bookPagePicture1 = new SuouBase.Controls.BookPagePicture();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.searchFolderNameButton = new System.Windows.Forms.Button();
            this.searchFolderNameTextBox = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.textSearchTextBox = new System.Windows.Forms.TextBox();
            this.textSearchButton = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.createSubIndexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createSubIndexToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.bookDisplay1 = new StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoScroll = true;
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox6);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupBox1.Location = new System.Drawing.Point(0, 26);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(281, 509);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.Text = "groupBox1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.searchResultButton);
            this.groupBox2.Controls.Add(this.listBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 373);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(264, 218);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "検索結果";
            // 
            // searchResultButton
            // 
            this.searchResultButton.AutoSize = true;
            this.searchResultButton.Enabled = false;
            this.searchResultButton.Location = new System.Drawing.Point(17, 18);
            this.searchResultButton.Name = "searchResultButton";
            this.searchResultButton.Size = new System.Drawing.Size(97, 23);
            this.searchResultButton.TabIndex = 1;
            this.searchResultButton.Text = "抽出結果の表示";
            this.searchResultButton.UseVisualStyleBackColor = true;
            this.searchResultButton.Click += new System.EventHandler(this.searchResultButton_Click);
            // 
            // listBox1
            // 
            this.listBox1.DisplayMember = "Name";
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(3, 43);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(258, 172);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button1);
            this.groupBox3.Controls.Add(this.button3);
            this.groupBox3.Controls.Add(this.bookPagePicture1);
            this.groupBox3.Controls.Add(this.button2);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 147);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(264, 226);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "行検索";
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(17, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(212, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "検索ボックスにコピーしているテキストを貼る";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button3
            // 
            this.button3.AutoSize = true;
            this.button3.Location = new System.Drawing.Point(158, 197);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 23);
            this.button3.TabIndex = 5;
            this.button3.Text = "インデックスの作成";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // bookPagePicture1
            // 
            this.bookPagePicture1.AutoSizeType = SuouBase.Controls.ShrinkType.None;
            this.bookPagePicture1.EnableCopy = true;
            this.bookPagePicture1.EnablePaste = true;
            this.bookPagePicture1.Item = null;
            this.bookPagePicture1.Location = new System.Drawing.Point(17, 43);
            this.bookPagePicture1.Name = "bookPagePicture1";
            this.bookPagePicture1.Page = null;
            this.bookPagePicture1.Size = new System.Drawing.Size(241, 148);
            this.bookPagePicture1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.Location = new System.Drawing.Point(17, 197);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(135, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "行検索(インデックス使用)";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.searchFolderNameButton);
            this.groupBox6.Controls.Add(this.searchFolderNameTextBox);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox6.Location = new System.Drawing.Point(0, 107);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(264, 40);
            this.groupBox6.TabIndex = 10;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ファイル名検索";
            // 
            // searchFolderNameButton
            // 
            this.searchFolderNameButton.Location = new System.Drawing.Point(185, 13);
            this.searchFolderNameButton.Name = "searchFolderNameButton";
            this.searchFolderNameButton.Size = new System.Drawing.Size(79, 23);
            this.searchFolderNameButton.TabIndex = 1;
            this.searchFolderNameButton.Text = "検索";
            this.searchFolderNameButton.UseVisualStyleBackColor = true;
            this.searchFolderNameButton.Click += new System.EventHandler(this.searchFolderNameButton_Click);
            // 
            // searchFolderNameTextBox
            // 
            this.searchFolderNameTextBox.Location = new System.Drawing.Point(17, 15);
            this.searchFolderNameTextBox.Name = "searchFolderNameTextBox";
            this.searchFolderNameTextBox.Size = new System.Drawing.Size(162, 19);
            this.searchFolderNameTextBox.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.textSearchTextBox);
            this.groupBox5.Controls.Add(this.textSearchButton);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox5.Location = new System.Drawing.Point(0, 71);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(264, 36);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "テキスト検索";
            // 
            // textSearchTextBox
            // 
            this.textSearchTextBox.Location = new System.Drawing.Point(17, 11);
            this.textSearchTextBox.Name = "textSearchTextBox";
            this.textSearchTextBox.Size = new System.Drawing.Size(162, 19);
            this.textSearchTextBox.TabIndex = 1;
            // 
            // textSearchButton
            // 
            this.textSearchButton.Location = new System.Drawing.Point(185, 12);
            this.textSearchButton.Name = "textSearchButton";
            this.textSearchButton.Size = new System.Drawing.Size(79, 24);
            this.textSearchButton.TabIndex = 0;
            this.textSearchButton.Text = "検索";
            this.textSearchButton.UseVisualStyleBackColor = true;
            this.textSearchButton.Click += new System.EventHandler(this.textSearchButton_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.comboBox1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 28);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(264, 43);
            this.groupBox4.TabIndex = 7;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "検索フォルダ指定";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(17, 17);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(242, 20);
            this.comboBox1.TabIndex = 4;
            this.comboBox1.DropDown += new System.EventHandler(this.comboBox1_DropDown);
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button4);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(264, 28);
            this.panel1.TabIndex = 8;
            // 
            // button4
            // 
            this.button4.AutoSize = true;
            this.button4.Location = new System.Drawing.Point(66, 1);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(132, 24);
            this.button4.TabIndex = 6;
            this.button4.Text = "検索可能フォルダの追加";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(281, 26);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 509);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createSubIndexToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(623, 26);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Visible = false;
            // 
            // createSubIndexToolStripMenuItem
            // 
            this.createSubIndexToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createSubIndexToolStripMenuItem1});
            this.createSubIndexToolStripMenuItem.Name = "createSubIndexToolStripMenuItem";
            this.createSubIndexToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.createSubIndexToolStripMenuItem.Text = "CreateSubIndex";
            this.createSubIndexToolStripMenuItem.Click += new System.EventHandler(this.createSubIndexToolStripMenuItem_Click);
            // 
            // createSubIndexToolStripMenuItem1
            // 
            this.createSubIndexToolStripMenuItem1.Name = "createSubIndexToolStripMenuItem1";
            this.createSubIndexToolStripMenuItem1.Size = new System.Drawing.Size(170, 22);
            this.createSubIndexToolStripMenuItem1.Text = "CreateSubIndex";
            this.createSubIndexToolStripMenuItem1.Click += new System.EventHandler(this.createSubIndexToolStripMenuItem1_Click);
            // 
            // bookDisplay1
            // 
            this.bookDisplay1.AddFavoriteButtonAction = null;
            this.bookDisplay1.DataBook = null;
            this.bookDisplay1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bookDisplay1.EditLock = true;
            this.bookDisplay1.EnableNameTextBox = StoryViewer.NameTextBoxType.ReadOnly;
            this.bookDisplay1.IsTransparentDrag = false;
            this.bookDisplay1.Item = null;
            this.bookDisplay1.Location = new System.Drawing.Point(284, 26);
            this.bookDisplay1.ManualDrag = false;
            this.bookDisplay1.Name = "bookDisplay1";
            this.bookDisplay1.PermitCategory = true;
            bookDisplaySetting1.EditLockButtonVisible = false;
            bookDisplaySetting1.IsSaveCheck = false;
            this.bookDisplay1.Setting = bookDisplaySetting1;
            this.bookDisplay1.Size = new System.Drawing.Size(339, 509);
            this.bookDisplay1.TabIndex = 0;
            this.bookDisplay1.UseContext = true;
            // 
            // SearchViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bookDisplay1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "SearchViewer";
            this.Size = new System.Drawing.Size(623, 535);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay bookDisplay1;
        private System.Windows.Forms.Panel groupBox1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Button button3;
        private SuouBase.Controls.BookPagePicture bookPagePicture1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button searchResultButton;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button searchFolderNameButton;
        private System.Windows.Forms.TextBox searchFolderNameTextBox;
        private System.Windows.Forms.TextBox textSearchTextBox;
        private System.Windows.Forms.Button textSearchButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem createSubIndexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createSubIndexToolStripMenuItem1;
    }
}
