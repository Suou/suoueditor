﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text.Search;
using System.IO;
using SuouBase.Text;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Files;
using SuouBase;
using SuouBase.Book;

namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    public partial class SearchViewer : UserControl
    {
        public SearchViewer()
        {
            InitializeComponent();
            this.button3.Enabled = false;
            this.button2.Enabled = false;

        }
        private LineHashSearchIndexSupervisor _Supervisor = null;
        public LineHashSearchIndexSupervisor Supervisor
        {
            get
            {
                if (this._Supervisor == null) this._Supervisor = new LineHashSearchIndexSupervisor();
                return this._Supervisor;
            }
            private set
            {
                this._Supervisor = value;
            }
        }
        //public IndexSupervisor Supervisor { get; private set; }
        public SuouViewerTemplate AATemplate { get; set; }
        private SmallInfo _DLLSmallInfo = null;
        public SmallInfo DLLSmallInfo
        {
            get { return this._DLLSmallInfo; }
            set
            {
                this._DLLSmallInfo = value;
            }
        }
        public void Set()
        {
            if (this.AATemplate == null) return;
            foreach (var v in this.AATemplate.GlobalAATemplate)
            {
                this.comboBox1.Items.Add(v.Name);
            }
        }

        private void comboBox1_DropDown(object sender, EventArgs e)
        {
            this.comboBox1.Items.Clear();
            this.Set();
        }
        public DirectoryInfo SelectedDirectoryInfo
        {
            get
            {
                var n = this.comboBox1.SelectedIndex;
                if (n == -1) return null;
                return new DirectoryInfo(this.AATemplate.GlobalAATemplate[n].FullPath);
            }
        }
        
        


        private void button3_Click(object sender, EventArgs e)
        {
            this.CreateIndex();
        }
        private void CreateIndex()
        {
            if (this.SelectedDirectoryInfo != null)
            {
                if (this.Supervisor.CheckIndexCreated(this.SelectedDirectoryInfo.FullName))
                {
                    DialogResult result = MessageBox.Show(msg2, "インデックスの作成",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    if (result == DialogResult.Yes)
                    {
                        var creater = this.Supervisor.CreateIndexCreater(this.SelectedDirectoryInfo);
                        creater.Delete();
                        var f = new IndexCreatingDialog(creater);
                        f.ShowDialog();
                        creater.Dispose();
                    }
                    if (this.createSubIndexToolStripMenuItem1.Checked)
                    {
                        var w = this.Supervisor.GetSubIndexCrater(this.SelectedDirectoryInfo);
                        //w.ForEach((v) => v());
                        var pf = new TaskDialog(w);
                        pf.ShowDialog();

                    }
                }
                else
                {
                    DialogResult result = MessageBox.Show(msg, "インデックスの作成",
                           MessageBoxButtons.YesNoCancel, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button2);
                    if (result == DialogResult.Yes)
                    {
                        var creater = this.Supervisor.CreateIndexCreater(this.SelectedDirectoryInfo);
                        var f = new IndexCreatingDialog(creater);
                        f.ShowDialog();
                    }
                    
                }
            }
            else
            {
                this.PopupMessage();
            }
 
        }
        public SuouBase.Book.IBookPage SearchText
        {
            get
            {
                return this.bookPagePicture1.Page;
            }
        }
        public void PopupMessage()
        {
            MessageBox.Show("フォルダが指定されていません");
        }
        string msg = @"選択したフォルダのインデックスがありません。インデックスを作成しますか？
(インデックスの作成には数分かかる場合があります。)";
        string msg2 = @"インデックスは既に作成されています。再構築して最新のインデックスにしますか？
(インデックスの作成には数分かかる場合があります。)";
        private void button2_Click(object sender, EventArgs e)
        {
            if (this.SelectedDirectoryInfo != null)
            {
                if (!this.Supervisor.CheckIndexCreated(this.SelectedDirectoryInfo.FullName))
                {
                    this.CreateIndex();
                }

                if (this.SearchText != null)
                {
                    var lines = this.SearchText.SText.Text.LineSprit();
                    var page = this.SearchText.IBookPageCopy();

                    var searchResult = this.Supervisor.GetIndex(this.SelectedDirectoryInfo.FullName).BinarySearchBook(page);
                    if (searchResult.IsHit)
                    {
                        this.ShowLineHashSearchResult(searchResult);
                    }
                    else
                    {
                        MessageBox.Show("指定した行を含むようなテキストは見つかりませんでした");
                    }

                }
                else
                {
                    MessageBox.Show("検索テキストを入力してください");
                }

            }
            else
            {
                this.PopupMessage();
            }
        }
        private void ShowLineHashSearchResult(LineHashSearchResult searchResult)
        {
            this.bookDisplay1.Item = searchResult.CreateBook();
            this.listBox1.Items.Clear();
            searchResult.FileNameList.Select((v) => new FileInfo(v)).ForEach((v) => this.listBox1.Items.Add(v));
        }
        IBook _SearchResultItem = null;
        IBook SearchResultItem
        {
            get
            {
                return this._SearchResultItem;
            }
            set
            {
                this._SearchResultItem = value;
                this.searchResultButton.Enabled = value != null;
            }
        }
        public void PopupMessage2()
        {
            MessageBox.Show("検索テキストを入力してください");
        }
        private void textSearchButton_Click(object sender, EventArgs e)
        {
            var s = this.textSearchTextBox.Text;
            if (this.SelectedDirectoryInfo != null)
            {
                if (s.Length != 0)
                {
                    var p = new SearchTask(this.SelectedDirectoryInfo, this.textSearchTextBox.Text);
                    var dialog = new SuouBase.Controls.Task.TaskDialog(p) { Text = "検索"};
                    dialog.ShowDialog();

                    this.SearchResultItem = EditingFile.Create(p.Result.Select((v) => v.Page));
                    var rstnames = p.Result.Select((v) => v.IBookInfo).Distinct().ToArray();
                    this.listBox1.Items.Clear();
                    rstnames.ForEach((v) => this.listBox1.Items.Add(v));
                    if (rstnames.Length == 0) MessageBox.Show("指定した文字列を含むファイルは見つかりませんでした");
                }
                else
                {
                    this.PopupMessage2();
                }
            }
            else
            {
                this.PopupMessage();
            }
        }

        private void searchResultButton_Click(object sender, EventArgs e)
        {
            this.bookDisplay1.Item = this.SearchResultItem;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var item = this.listBox1.SelectedItem;
            if (item != null)
            {
                var info = (FileInfo)item;
                this.bookDisplay1.Item = this.DLLSmallInfo.IBookDictionary[info.FullName];
            }
        }
        public SuouBase.Controls.BookFolderOpenDialog Dialog = new SuouBase.Controls.BookFolderOpenDialog();
        private void button4_Click(object sender, EventArgs e)
        {
            var p = this.Dialog.ShowDialog();
            if (p != null)
            {
                this.AATemplate.GlobalAATemplate.Add(p);
                MessageBox.Show("追加しました");
            }
        }

        private void searchFolderNameButton_Click(object sender, EventArgs e)
        {
            var s = this.searchFolderNameTextBox.Text;
            if (this.SelectedDirectoryInfo != null)
            {
                if (s.Length != 0)
                {
                    var p = new SearchFolderTask(this.SelectedDirectoryInfo, this.searchFolderNameTextBox.Text);
                    var dialog = new SuouBase.Controls.Task.TaskDialog(p) { Text = "検索" };
                    dialog.ShowDialog();

                    this.SearchResultItem = null;
                    var rstnames = p.Result.Select((v) => v).Distinct().ToArray();
                    this.listBox1.Items.Clear();
                    rstnames.ForEach((v) => this.listBox1.Items.Add(v));
                    if (rstnames.Length == 0) MessageBox.Show("指定した文字列を含むファイル名は見つかりませんでした");
                }
                else
                {
                    this.PopupMessage2();
                }
            }
            else
            {
                this.PopupMessage();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.bookPagePicture1.Paste();
        }

        private void postToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /*
            if (this.Supervisor.SelectedSearcherCreater != null)
            {
                if (!this.Supervisor.SelectedSearcherCreater.IsCreated)
                {
                    this.CreateIndex();
                }

                if (this.Supervisor.SelectedSearcherCreater.IsCreated)
                {
                    var r = new SimilaritySearch(this.Supervisor.SelectedSearcherCreater.Searcher);
                    var task = new SimilaritySearchTask(this.SelectedDirectoryInfo, "", r);
                    var form = new SuouBase.Controls.TaskDialog(task);
                    var rst = form.ShowDialog();
                    if (rst == DialogResult.OK)
                    {
                        r.Save(new DirectoryInfo("Data/Prot"));
                        MessageBox.Show("保存しました");
                    }
                }

            }
            else
            {
                this.PopupMessage();
            }
            */
        }

        private void tstToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.Supervisor.SelectedSearcherCreater.OnMainMemory = true;
        }
        public void DisposeCheck()
        {
            this.Supervisor.DisposeCheck();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var n = this.comboBox1.SelectedIndex;
            if (n == -1) return;
            this.Supervisor.SelectedDirectoryInfo = new DirectoryInfo(this.AATemplate.GlobalAATemplate[n].FullPath);
        }

        private void createSubIndexToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void createSubIndexToolStripMenuItem1_Click(object sender, EventArgs e)
        {

            this.createSubIndexToolStripMenuItem1.Checked = !this.createSubIndexToolStripMenuItem1.Checked;
        }

        
    }
}
