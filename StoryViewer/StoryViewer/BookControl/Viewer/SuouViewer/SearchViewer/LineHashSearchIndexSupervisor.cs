﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.IO;
using SuouBase.Text;
using SuouBase;
using SuouBase.Files;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    public class LineHashSearchIndexSupervisor2:LineHashSearchIndexSupervisor
    {
        public static List<Action> TranslateAction(IEnumerable<AbstractIndexCreaterWithLineHashSearch> item,LineHashSearchIndexSupervisor2 item2)
        {
            List<Action> r = new List<Action>();
            item.Select((v) => TranslateAction(v)).ForEach((v) => r.Add(v));
            
            r.Add(() => item2.DisposeCheck());
            return r;
        }
        public static Action TranslateAction(AbstractIndexCreaterWithLineHashSearch item)
        {
            return () => item.CreateAndSaveIndex(null);
        }
        public IEnumerable<AbstractIndexCreaterWithLineHashSearch> CreateIndexCreaters(DirectoryInfo info)
        {
            var files = info.AAFileNameIterator();
            List<AbstractIndexCreaterWithLineHashSearch> r = new List<AbstractIndexCreaterWithLineHashSearch>();
            foreach (var v in files)
            {
                var fi = new FileInfo(v);
                var n = this.Indexer.CreateNewNumber(v);
                var f = new IndexCreaterWithLineHashSearchInFile(fi, this.IndexFolderPathInfo, n) { OnMainMemory = true };
                r.Add(f);
            }
            return r;
        }
        public LineHashSearchIndexSupervisor2(string path)
        {
            this.IndexFolderPath = path;
            this.Indexer = new IndexSupervisor(this.IndexFolderPath);
        }
    }
    public class LineHashSearchIndexSupervisor
    {
        protected IndexSupervisor Indexer { get; set; }
        public DirectoryInfo SelectedDirectoryInfo { get; set; }
        public Dictionary<string, LineHashSearch> Items = new Dictionary<string, LineHashSearch>();
        public string IndexFolderPath = SuouBase.SuouEditorConst.IndexDirectoryName;
        public Dictionary<string, LineHashSearchIndexSupervisor2> SubDic = new Dictionary<string, LineHashSearchIndexSupervisor2>();
        protected DirectoryInfo _IndexFolderPathInfo;
        public DirectoryInfo IndexFolderPathInfo
        {
            get
            {
                if (this._IndexFolderPathInfo == null)
                {
                    this._IndexFolderPathInfo = new DirectoryInfo(this.IndexFolderPath);
                    if (!_IndexFolderPathInfo.Exists) _IndexFolderPathInfo.Create();
                }
                return this._IndexFolderPathInfo;
            }
        }
        public LineHashSearchIndexSupervisor()
        {
            this.Indexer = new IndexSupervisor(this.IndexFolderPath);
        }
        public LineHashSearchIndexSupervisor(string path)
        {
            this.IndexFolderPath = path;
            this.Indexer = new IndexSupervisor(this.IndexFolderPath);
        }
        /*
        private bool IsCreatedIndexSearchCreater(string fullpath)
        {
            return this.Items.ContainsKey(fullpath);
        }
        */
        /*
        private void CreateLineHashIndexesCreater(DirectoryInfo searchingFolderinfo)
        {
            var finfo = new DirectoryInfo(this.IndexFolderPath);
            if (!finfo.Exists) finfo.Create();

            var newNumber = this.Indexer.CreateNewNumber(this.SelectedDirectoryInfo.FullName);
            this.Items[this.SelectedDirectoryInfo.FullName] = new IndexCreaterWithLineHashSearchInDirectory(this.SelectedDirectoryInfo, finfo, newNumber);
        }
        */
        /*
        public AbstractIndexCreaterWithLineHashSearch SelectedSearcherCreater
        {
            get
            {
                if (this.SelectedDirectoryInfo == null) return null;
                if (!this.IsCreatedIndexSearchCreater(this.SelectedDirectoryInfo.FullName))
                {
                    this.CreateIndex(this.SelectedDirectoryInfo);
                }
                return this.Items[this.SelectedDirectoryInfo.FullName];
            }
        }
        */
        /*
        public LineHashSearch SelectedSearcher
        {
            get { return this.SelectedSearcherCreater.Searcher; }
        }
        */
        public LineHashSearchIndexSupervisor GetSubSupervisor(string path)
        {
            var n = this.Indexer.Dic[path];
            var r = new LineHashSearchIndexSupervisor(this.IndexFolderPath + "/" + n);
            return r;
        }
        public bool CheckIndexCreated(string fullpath)
        {
            return this.Indexer.CheckExistFile(fullpath);
        }
        public void CreateIndex(DirectoryInfo info)
        {
            if (this.Indexer.CheckExistFile(info.FullName))
            {
                throw new Exception();
            }
            else
            {
                var n = this.Indexer.CreateNewNumber(info.FullName);
                var v = new IndexCreaterWithLineHashSearchInDirectory(info, this.IndexFolderPathInfo, n);
                v.CreateAndSaveIndex(null);

            }
        }
        
        

        public void DeleteIndex(DirectoryInfo info)
        {
            var p = this.GetIndexCreater(info.FullName);
            p.Delete();
            if(this.Items.ContainsKey(info.FullName))this.Items.Remove(info.FullName);
            this.Indexer.Dic.Remove(info.FullName);
        }
        public AbstractIndexCreaterWithLineHashSearch CreateIndexCreater(DirectoryInfo info)
        {
            if (this.Indexer.CheckExistFile(info.FullName))
            {
                this.DeleteIndex(info);
            }
            
            var n = this.Indexer.CreateNewNumber(info.FullName);
            var v = new IndexCreaterWithLineHashSearchInDirectory(info, this.IndexFolderPathInfo, n);
            return v;
        }
        public List<Action> GetSubIndexCrater(DirectoryInfo info)
        {
            if (this.Indexer.CheckExistFile(info.FullName))
            {
                var n = this.Indexer.Dic[info.FullName].ToString();
                var p = new LineHashSearchIndexSupervisor2(this.IndexFolderPath + "/" + n);
                this.SubDic[n] = p;
                var tasks = p.CreateIndexCreaters(info);
                return LineHashSearchIndexSupervisor2.TranslateAction(tasks,p);
            }
            else
            {
                throw new Exception();
            }
        }
        private AbstractIndexCreaterWithLineHashSearch GetIndexCreater(string fullpath)
        {
            if (this.Indexer.CheckExistFile(fullpath))
            {
                var name = this.Indexer.Dic[fullpath].ToString();
                if (Directory.Exists(fullpath))
                {
                    var info = new DirectoryInfo(fullpath);
                    return new IndexCreaterWithLineHashSearchInDirectory(info, this.IndexFolderPathInfo, name);
                }
                else
                {
                    var info = new FileInfo(fullpath);
                    return new IndexCreaterWithLineHashSearchInFile(info, this.IndexFolderPathInfo, name);
                }
            }
            else
            {
                throw new Exception();
            }
        }
        public LineHashSearch GetIndex(string fullpath,bool onMemory = false)
        {
            if (this.Items.ContainsKey(fullpath))
            {
                return this.Items[fullpath];
            }
            else if (this.Indexer.CheckExistFile(fullpath))
            {
                var v = this.GetIndexCreater(fullpath);
                v.OnMainMemory = onMemory;
                this.Items[fullpath] = v.Searcher;
                return this.Items[fullpath];
            }
            else
            {
                return null;
            }
        }

        public bool DisposeCheck()
        {
            return this.Indexer.DisposeCheck();
        }
    }
}
