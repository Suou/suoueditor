﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Text;
using SuouBase;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    public class LocalLineHashSearch
    {
        private Queue<LineHashSearch> Dic { get; set; }
        private LineHashSearch General { get; set; }
        public float LowerBound { get; private set; }
        public int CashLimit { get; private set; }

        private LineHashSearchIndexSupervisor TopSupervisor { get; set; }
        private LineHashSearchIndexSupervisor SubSupervisor { get; set; }
        public string TargetFolderPath { get; private set; }

        public LocalLineHashSearch(LineHashSearchIndexSupervisor top, string path,int cashLimit,float lowerBound)
        {
            this.TopSupervisor = top;
            this.SubSupervisor = this.TopSupervisor.GetSubSupervisor(path);
            this.TargetFolderPath = path;

            this.General = this.TopSupervisor.GetIndex(this.TargetFolderPath);
            this.Dic = new Queue<LineHashSearch>();
            this.LowerBound = lowerBound;
            this.CashLimit = cashLimit;
        }

        private void RegistData(string path)
        {
            var v = this.SubSupervisor.GetIndex(path,true);
            this.Dic.Enqueue(v);
            while (this.Dic.Count > CashLimit) this.Dic.Dequeue();
        }
        public LineHashSearchSubResult BinarySearchBook(IBookPage page)
        {
            foreach(var v in this.Dic){
                var p = v.BinarySearchBook(page,LowerBound);
                if(p.IsHit){
                    return p.TopHitData;
                }
            }
            var q = this.General.BinarySearchBook(page, LowerBound);
            if (q.IsHit)
            {
                this.RegistData(q.TopHitData.FileName);
                return q.TopHitData;
            }
            else
            {
                return null;
            }
        }
    }
}
