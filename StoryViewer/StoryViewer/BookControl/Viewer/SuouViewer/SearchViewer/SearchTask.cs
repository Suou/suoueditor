﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text.Search;
using System.IO;
using SuouBase.Text;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Files;
using SuouBase;
using SuouBase.Controls.Task;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    public class BaseSearchTask : List<TaskDialogTask>
    {
        public DirectoryInfo Info { get; set; }
        public string SearchText { get; set; }

        public BaseSearchTask(DirectoryInfo info,string searchText)
        {
            this.Info = info;
            this.SearchText = searchText;
        }

        public TaskDialogTask Task1
        {
            get
            {
                var r = new TaskDialogTask(
                    "フォルダを収集しています",
                    (bar) =>
                    {
                        var p = this.Info.AAFileNameIterator().Count();
                        bar.ProgressMaximum = p;
                    },
                    "フォルダを収集しています"
                    );
                return r;

            }
        }
    }
    public class SearchTask:BaseSearchTask
    {

        public TaskDialogTask _Task { get; set; }
        public SuperTaskDialogTask<BookSearchResult> _Task2 { get; set; }
        public List<TextSearchResult> Result
        {
            get
            {
                return this._Task2.Result.SelectMany((v)=>v.Pages).ToList();
            }
        }
        public SearchTask(DirectoryInfo info, string searchText):base(info,searchText)
        {
            this._Task = this.Task1;
            this._Task2 = this.Task2;
            this.Add(this._Task);
            this.Add(this._Task2);
        }

        
        public SuperTaskDialogTask<BookSearchResult> Task2
        {
            get
            {
                var r = new SuperTaskDialogTask<BookSearchResult>(
                    "検索中",
                    this.Info.AAFileIteratorAsync().Select((v) => v.TextSearch(this.SearchText)),
                    "検索終了"
                    );
                return r;
            }
        }
    }

    public class SearchFolderTask:BaseSearchTask
    {
        public TaskDialogTask _Task { get; set; }
        public SuperTaskDialogTask<FileInfo> _Task2 { get; set; }
        public SearchFolderTask(DirectoryInfo info, string searchText):base(info,searchText)
        {
            this._Task = this.Task1;
            this._Task2 = this.Task2;
            this.Add(this._Task);
            this.Add(this._Task2);
        }
        public SuperTaskDialogTask<FileInfo> Task2
        {
            get
            {
                var r = new SuperTaskDialogTask<FileInfo>(
                    "検索中",
                    this.Info.AAFileNameIterator().Select((v) => new FileInfo(v)).Select((v)=> v.Name.IndexOf(this.SearchText) != -1 ? v : null),
                    "検索終了"
                    );
                return r;
            }
        }
        public List<FileInfo> Result
        {
            get
            {
                return this._Task2.Result.Where((v)=>v != null).ToList();
            }
        }
    }
}
