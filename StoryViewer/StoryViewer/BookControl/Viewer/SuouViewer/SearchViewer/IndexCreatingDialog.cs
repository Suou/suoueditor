﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Base;
namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    public partial class IndexCreatingDialog : Form
    {
        public IndexCreatingDialog()
        {
            InitializeComponent();
            
        }
        public IndexCreatingDialog(AbstractIndexCreaterWithLineHashSearch t)
            : this()
        {
            this.Item = t;
        }
        public AbstractIndexCreaterWithLineHashSearch Item { get; set; }
        public int AAFileCount { get; set; }
        public void Create()
        {
            var prepare3 = this.BoxedDo(()=>this.Provide3());

            AsyncLinQ.StartDo(() => this.Provide1())
                .Do(this.BoxedDo(()=>this.DoItem1()))
                .Do(() => this.Item.Create2())
                .Do(() => this.Item.Create3(prepare3))
                .Do(this.BoxedDo(() => this.DoItem3()))
                .Do(this.BoxedDo(() => this.EndDoItem()))
                .EndDo();
        }
        public void Provide1()
        {
            Action t1 = () => Provide();
            this.Item.Create1(() => this.Do(t1));

        }
        public void Provide()
        {
            if (this.progressBar1.Value == 0)
            {
                //this.AAFileCount = this.Item.IndexLength;
                this.listBox1.SelectedIndex = 0;
                this.listBox1.Items[0] = string.Format("step 1:AAファイルの収集中...");
                this.progressBar1.Maximum = this.Item.IndexLength;
                this.progressBar1.Value = 0;
                this.listBox1.SelectedIndex = 1;
            }

            this.progressBar1.Value++;
            this.listBox1.Items[1] = string.Format("step 2:インデックスの作成中...{0}/{1}", this.progressBar1.Value, this.progressBar1.Maximum);
        }
        public void Provide3()
        {
            if (this.progressBar1.Maximum != this.Item.SortingTimes)
            {
                this.progressBar1.Maximum = this.Item.SortingTimes;
                this.progressBar1.Value = 0;
                this.listBox1.SelectedIndex = 3;
            }
            this.progressBar1.Value++;
            this.listBox1.Items[3] = string.Format("step 4:整理しています...{0}/{1}", this.progressBar1.Value, this.progressBar1.Maximum);
        }

        public void DoItem1()
        {
            this.listBox1.SelectedIndex = 2;
            this.listBox1.Items[2] = "step 3: 保存しています...";
        }
        public void DoItem3()
        {
            this.listBox1.SelectedIndex = 4;
            this.listBox1.Items[4] = "step 5:終了しました";
        }
        public void EndDoItem()
        {
            this.IsFinished = true;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void IndexCreatingDialog_Shown(object sender, EventArgs e)
        {
            this.Create();
        }
        public bool IsFinished { get; set; }
        private void IndexCreatingDialog_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (e.CloseReason == CloseReason.UserClosing && !this.IsFinished)
            {
                MessageBox.Show("途中終了は出来ません(未実装)");
                e.Cancel = true;
            }
        }
    }
}
