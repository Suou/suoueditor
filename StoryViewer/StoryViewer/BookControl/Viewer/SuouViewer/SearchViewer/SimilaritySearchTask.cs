﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text.Search;
using System.IO;
using SuouBase.Text;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Files;
using SuouBase;
using SuouBase.Controls.Task;
using SuouBase.Controls;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    class SimilaritySearchTask:BaseSearchTask
    {
        public SimilaritySearch Similar { get; set; }
        public TaskDialogTask _Task { get; set; }
        public SuperTaskDialogTask<IBook> _Task2 { get; set; }
        public System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
        public SimilaritySearchTask(DirectoryInfo info, string searchText,SimilaritySearch searcher)
            : base(info, searchText)
        {
            this.Similar = searcher;
            this._Task = this.Task1;
            this._Task2 = this.Task2;
            this.Add(this._Task);
            this.Add(this._Task2);

        }

        
        public SuperTaskDialogTask<IBook> Task2
        {
            get
            {
                var r = new SuperTaskDialogTask<IBook>(
                    "検索中",
                    this.Info
                    .AAFileIterator()
                    .ForEach2((v) => sw.Start())
                    .Select((v)=>v.ToEditingFile())
                    .ForEach2((v)=>this.Similar.Search(v.Pages,0.5f))
                    .ForEach2((v) =>
                    {
                        sw.Stop();
                        sw.Reset();
                            ;
                    })
                    ,
                    "検索終了"
                    );
                return r;
            }
        }
    }
}
