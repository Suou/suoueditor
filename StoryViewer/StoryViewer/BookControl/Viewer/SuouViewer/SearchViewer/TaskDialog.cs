﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Base;
namespace StoryViewer.BookControl.Viewer.SearchViewer
{
    public partial class TaskDialog : Form
    {
        public TaskDialog()
        {
            InitializeComponent();
            this.listBox1.Items.Add("NoContent");
            this.PlusAction = this.BoxedDo(() =>
            {
                this.progressBar1.Value++;
                this.listBox1.Items[0] = string.Format("{0}/{1}", (int)this.progressBar1.Value, (int)this.progressBar1.Maximum);
            });
        }
        public TaskDialog(IEnumerable<Action> items):this()
        {
            if (items == null) throw new Exception();
            this.TaskList = items.ToList();
        }
        public List<Action> TaskList { get; set; }
        public void Do()
        {
            this.progressBar1.Maximum = this.TaskList.Count;
            this.progressBar1.Value = 0;
            AsyncLinQ.StartDo(() => this.CreateAction()())
                  .EndDo();
        }
        public Action CreateAction()
        {
            var f = new Action(
                () =>
                {
                    foreach (var v in TaskList)
                    {
                        v();
                        
                        this.PlusAction();
                    }
                }

            );
            return f;
        }
        public Action PlusAction { get; set; }

        private void TaskDialog_Shown(object sender, EventArgs e)
        {

            this.Do();
        }
        
    }
}
