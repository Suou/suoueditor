﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Files;
namespace StoryViewer.BookControl.Viewer.BooksViewer
{
    public interface ISuouViewer
    {
        
    }
    public interface IBookViewer
    {
        void SetDisplayItem(string filename);
    }
    public interface IDirectoryViewer
    {
        PathCollectionCollection MainPathCollection { get; set; }
    }
}
