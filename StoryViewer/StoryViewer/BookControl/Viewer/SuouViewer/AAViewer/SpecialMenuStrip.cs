﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Files;
using System.IO;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.DirectoryMenuStrip
{
    [ToolboxItem(false)]
    public class SpecialMenuStrip:MenuStrip
    {
        private SmallInfo _DLLSmallInfo = null;
        public SmallInfo DLLSmallInfo
        {
            get { return this._DLLSmallInfo; }
            set
            {
                this._DLLSmallInfo = value;
                if (this._DLLSmallInfo != null)
                {
                    this.LoadBook();
                    this.DLLSmallInfo.PageCopied += this.EditorInfo_PageCopied;
                }
            }
        }
        public event Action<object, SpecialMenuStripItemEventArgs> FilePathClicked;
        public SpecialMenuStrip():base()
        {
            this.InitializeComponent();
            this.HistoryMenuItem.MouseDown += this.HistoryMenuItemMouseDown;
            this.Items.Add(this.HistoryMenuItem);
            this.Items.Add(this.AAHistoryMenuItem);
            this.Items.Add(this.FavoriteMenuItem);
            this.HistoryMenuItem.DropDownOpening += (a, b) => this.CreateFilePathHistory();
            this.AAHistoryMenuItem.Click += (a, b) => {
                if (this.DLLSmallInfo != null) this.DLLSmallInfo.IBookDictionary[this.AAHistoryBook.FullFilePath] = this.AAHistoryBook;
                if (this.FilePathClicked != null)
                {
                    var args = new SpecialMenuStripItemEventArgs(new FileInfo(this.AAHistoryBook.FullFilePath), false);
                    this.FilePathClicked(this, args);
                } 
            };
            this.FavoriteMenuItem.Click += (a, b) => {
                if(this.DLLSmallInfo!=null)this.DLLSmallInfo.IBookDictionary[this.FavoriteBook.FullFilePath] = this.FavoriteBook;
                if (this.FilePathClicked != null && this.FavoriteBook.FullFilePath != null)
                {
                    var args = new SpecialMenuStripItemEventArgs(new FileInfo(this.FavoriteBook.FullFilePath), true);
                    this.FilePathClicked(this, args);
                }
            };
            this.HistoryContext = this.CreateContext();
        }
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SpecialMenuStrip
            // 
            this.BackColor = System.Drawing.Color.Aqua;
            this.ResumeLayout(false);

        }
        private ContextMenuStrip HistoryContext { get; set; }
        public ContextMenuStrip CreateContext()
        {
            var r = new ContextMenuStrip();
            var c1 = new ToolStripMenuItem() { Text = "履歴を消去する"};
            c1.Click += (a, b) => this.FilePathHistory.Clear();
            r.Items.Add(c1);
            return r;
        }
        
        private string FavoritePath = SuouBase.SuouEditorConst.AutoBookDirectoryName + "/Favorite.sec";
        private string AAHistoryPath = SuouBase.SuouEditorConst.AutoBookDirectoryName + "/AAHistory.sec";
        private int AAHistoryBookPageMaxCount = 200;

        private void EditorInfo_PageCopied(object sender, IBookPage page)
        {
            this.AddPageToHistoryBook(page);
        }
        private void HistoryMenuItemMouseDown(object sender, MouseEventArgs e)
        {
            
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                var p = this.PointToScreen(e.Location);
                this.HistoryContext.Show(p);
            }
        }
        
        private void CreateFilePathHistory()
        {
            this.HistoryMenuItem.DropDownItems.Clear();
            if (this.FilePathHistory.Count != 0)
            {
                foreach (var v in this.FilePathHistory)
                {
                    var fi = new FileInfo(v);
                    var p = new ToolStripMenuItem() { Text = fi.Name, Tag = new FileInfo(fi.FullName) };
                    var args = new SpecialMenuStripItemEventArgs((FileInfo)p.Tag, false);
                    p.Click += (a, b) => { if (FilePathClicked != null)this.FilePathClicked(p, args); };
                    this.HistoryMenuItem.DropDownItems.Add(p);
                }
            }
            else
            {
                var p = new ToolStripMenuItem() { Text = "履歴はありません" };
                this.HistoryMenuItem.DropDownItems.Add(p);
            }
        }
        private List<string> FilePathHistory = new List<string>();
        public void AddFilePathTo(FileInfo info)
        {
            if (info.FullName == this.AAHistoryBook.FullFilePath || info.FullName == this.FavoriteBook.FullFilePath) return;
            if (this.FilePathHistory.Any((v) => v == info.FullName))
            {
            }
            else
            {
                this.FilePathHistory.Add(info.FullName);
            }
        }
        private void AddPageToHistoryBook(IBookPage page)
        {
            if (this.AAHistoryBook.Owner == null)
            {
                this.AAHistoryBook.Owner = this;
                this.AAHistoryBook.Add(page.ToMergeBookPage());
                if (this.AAHistoryBook.Count > 1)
                {
                    while (this.AAHistoryBook.Count > this.AAHistoryBookPageMaxCount)
                    {
                        this.AAHistoryBook.Delete(0);
                    }
                }
                this.AAHistoryBook.Owner = null;
            }
        }
        public void AddFavoritePage(IBookPage page)
        {
            this.FavoriteBook.Add(page.ToMergeBookPage());
        }
        #region Load
        private void LoadBook()
        {
            this.LoadAAHistory(this.AAHistoryPath);
            this.LoadFavorite(this.FavoritePath);
        }
        private void LoadFavorite(string path)
        {
            this.FavoritePath = path;
            this.FavoriteBook = this.DLLSmallInfo.IBookDictionary.GetBook(this.FavoritePath) ?? new ObsoleteSectionBook() { FullFilePath = (new FileInfo(this.FavoritePath)).FullName};
        }
        private void LoadAAHistory(string path)
        {
            this.AAHistoryPath = path;
            this.AAHistoryBook = 
                this.DLLSmallInfo.IBookDictionary.GetBook(this.AAHistoryPath) ??
                new ObsoleteSectionBook() { FullFilePath = (new FileInfo(this.AAHistoryPath)).FullName };     
        }
        #endregion
        private IBook AAHistoryBook = new ObsoleteSectionBook();
        private IBook FavoriteBook = new ObsoleteSectionBook();
        private ToolStripMenuItem HistoryMenuItem = new ToolStripMenuItem() { Text = "履歴" };
        private ToolStripMenuItem AAHistoryMenuItem = new ToolStripMenuItem() { Text = "使用AA履歴" };
        private ToolStripMenuItem FavoriteMenuItem = new ToolStripMenuItem() { Text = "お気に入りAA" };

        

        private Timer timer1;
        private IContainer components;

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (!this.DesignMode)
            {
                if (this.AAHistoryBook.IsEdit && this.AAHistoryBook.CanSave()) this.AAHistoryBook.Save(this.AAHistoryBook.FullFilePath, true);
                if (this.FavoriteBook.IsEdit && this.FavoriteBook.CanSave()) this.FavoriteBook.Save(this.FavoriteBook.FullFilePath, true);
            }
        }
        public void SuperDispose()
        {
            if (this.AAHistoryBook.IsEdit && this.AAHistoryBook.CanSave()) this.AAHistoryBook.Save(this.AAHistoryBook.FullFilePath, true);
            if (this.FavoriteBook.IsEdit && this.FavoriteBook.CanSave()) this.FavoriteBook.Save(this.FavoriteBook.FullFilePath, true);
        }
        [ReadOnly(true)]
        [Browsable(false)]
        public SpecialMenuStripSaveData Setting
        {
            get
            {
                var n = this.FilePathHistory.Count;
                var gp = n > 10 ? this.FilePathHistory.Skip(n-10).ToList() : this.FilePathHistory;
                var r = new SpecialMenuStripSaveData() { FilePaths = gp, AAHistoryPath = this.AAHistoryPath, FavoritePath = this.FavoritePath };
                return r;
            }
            set
            {
                if (value != null)
                {
                    this.FilePathHistory = value.FilePaths;
                    this.AAHistoryPath = value.AAHistoryPath;
                    this.FavoritePath = value.FavoritePath;
                    if (this._DLLSmallInfo != null)
                    {
                        this.LoadAAHistory(value.AAHistoryPath);
                        this.LoadFavorite(value.FavoritePath);
                    }
                }
            }
        }
        public int MaxFilePathLength = 10;
    }
    public class SpecialMenuStripItemEventArgs : EventArgs
    {
        public bool IsEdit { get; private set; }
        public FileInfo ItemInfo { get; private set; }
        public SpecialMenuStripItemEventArgs(FileInfo info, bool isEdit)
        {
            this.ItemInfo = info;
            this.IsEdit = isEdit;
        }
    }
    [Serializable]
    public class SpecialMenuStripSaveData
    {
        public List<string> FilePaths { get; set; }
        public string AAHistoryPath { get; set; }
        public string FavoritePath { get; set; }
    }
}
