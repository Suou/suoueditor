﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Files;
using SuouBase.Interface;
using SuouBase.Book;
using BSystem.Collections;
using StoryViewer.ImageViewer;
namespace StoryViewer.BookControl.Viewer.BooksViewer
{
    [ToolboxItem(false)]
    public partial class BooksViewerExtra : UserControl,IDirectoryViewer,IBookViewer
    {
        public BooksViewerExtra()
        {
            InitializeComponent();
            //this.tabViewer1.AddFavoriteButtonAction = (page) => this.specialMenuStrip1.AddFavoritePage(page);
            /*
            if (false)
            {
                this.Display = this.menuBooksDisplay1;
            }
            */
            {
                /*
                this.Controls.Remove(this.menuBooksDisplay1);
                this.Tab = new StoryViewer.BookControl.Viewer.Tab.TabViewer() { Dock = DockStyle.Fill};
                
                this.Controls.Add(this.Tab);
                this.Tab.BringToFront();
                this.Display = this.Tab;
                */
                this.Tab = this.tabViewer1;
                this.Display = this.Tab;
            }
        }
        private StoryViewer.BookControl.Viewer.Tab.TabViewer Tab { get; set; }

        public PathCollectionCollection MainPathCollection
        {
            get { return this.directoryMenuStrips1.PathItems; }
            set
            {
                this.directoryMenuStrips1.PathItems = value;
            }
        }



        public SmallInfo DLLSmallInfo
        {
            get { return this.specialMenuStrip1.DLLSmallInfo; }
            set {
                this.specialMenuStrip1.DLLSmallInfo = value;
                //this.menuBooksDisplay1.EditorSmallInfo = value;
                if (this.Tab != null) this.Tab.Info = value;
            }
        }
        private StoryViewer.BookControl.Display.IBookDisplay Display { get; set; }
        public DirectoryMenuStrip.DirectoryMenuStrips DirectoryMenuStripsContextMenu
        {
            get { return this.directoryMenuStrips1; }
        }
        public Func<IBook> RelateFilePathWithFileTree
        {
            get { return this.directoryMenuStrips1.RelatedFunc; }
            set { this.directoryMenuStrips1.RelatedFunc = value; }
        }
        public void LoadPathTree(IBook book)
        {
            this.directoryMenuStrips1.LoadRelatedItem(book);
        }
        public event Action<IBookPage, EventArgs> ThumbnailItemTouched;
        public event Action<IBookPage, EventArgs> AAClicked;
        public event Action<IBookPage, EventArgs> EditButtonClicked;
        [ReadOnly(true)]
        [Browsable(false)]
        public PathCollectionCollection LocalTemplate
        {
            get { return this.directoryMenuStrips1.PathItems; }
            set { this.directoryMenuStrips1.PathItems = value; }
        }
        public void SetDisplayItem(string filename,bool isEdit)
        {

            var book = this.DLLSmallInfo != null ? this.DLLSmallInfo.IBookDictionary.GetBook(filename) : IBookHandler.Load(filename);
            this.Display.SetBook(book,isEdit);
            
            this.specialMenuStrip1.AddFilePathTo(new System.IO.FileInfo(filename));
            
        }
        public void SetDisplayItem(string filename)
        {
            this.SetDisplayItem(filename, false);
        }
        /*
        public void AddItem(FileNode f)
        {
            this.directoryMenuStrips1.PathItems.Add(f,"人物");
        }
        public void AddItem(FileNode f,string name)
        {
            this.directoryMenuStrips1.PathItems.Add(f,name);
        }
        */

        private void directoryMenuStrip1_FileItemClicked(FileNode arg1, EventArgs arg2)
        {
            this.SetDisplayItem(arg1.FullPath,false);
        }


        public bool DisposeCheck()
        {
            this.specialMenuStrip1.SuperDispose();
            this.tabViewer1.MyDispose();
            //var b = this.menuBooksDisplay1.DisposeCheck();
            return true;
        }

        [ReadOnly(true)]
        [Browsable(false)]
        public BooksViewerExtraSavedata Setting
        {
            get
            {
                
                var r = new BooksViewerExtraSavedata() { Data1 = this.specialMenuStrip1.Setting };
                return r;
            }
            set
            {
                if (value != null)
                {
                    this.specialMenuStrip1.Setting = value.Data1;
                }
            }
        }
        
        private void menuBooksDisplay1_ItemTouched(IBookPage arg1, EventArgs arg2)
        {
            if (this.ThumbnailItemTouched != null) this.ThumbnailItemTouched(arg1, arg2);
        }

        private void menuBooksDisplay1_EditButtonClicked(IBookPage arg1, EventArgs arg2)
        {
            if (this.EditButtonClicked != null) this.EditButtonClicked(arg1, arg2);
        }
        
        /*
        private void menuBooksDisplay1_ItemCopied(IBookPage arg1, EventArgs arg2)
        {
            this.specialMenuStrip1.AddPage(arg1);
        }
        */
        /*
        private void menuBooksDisplay1_ItemDragStarting(IBookPage arg1, EventArgs arg2)
        {
            this.specialMenuStrip1.AddPage(arg1);

        }
        */


        private void specialMenuStrip1_FilePathClicked(object arg1, DirectoryMenuStrip.SpecialMenuStripItemEventArgs arg2)
        {
            this.SetDisplayItem(arg2.ItemInfo.FullName, arg2.IsEdit);
        }


        private void tabViewer1_Page_Clicked(object arg1, IBookPage arg2)
        {

            if (this.AAClicked != null)
            {
                this.AAClicked(arg2, new EventArgs());
            }
        }
    }

    [Serializable]
    public class BooksViewerExtraSavedata
    {
        public StoryViewer.BookControl.Viewer.DirectoryMenuStrip.SpecialMenuStripSaveData Data1 { get; set; }

    }
}
