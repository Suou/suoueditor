﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Files;
using BSystem.Collections;
using BSystem;
using System.IO;
using SuouBase;
using SuouBase.Book;
using SuouBase.Text;
namespace StoryViewer.BookControl.Viewer.DirectoryMenuStrip
{
    public partial class DirectoryMenuStrips : UserControl
    {
        #region Property
        public event Action<FileNode, EventArgs> FileItemClicked;
        private PathCollectionCollection _PathItems = new PathCollectionCollection() { Name = "Default" };
        private List<DirectoryMenuStrip> StripItems = new List<DirectoryMenuStrip>();
        private Color[] StripColores = new Color[] { Color.CadetBlue, Color.Chocolate, Color.Crimson, Color.DarkOrange, Color.MintCream };

        public Func<IBook> RelatedFunc { get; set; }
        public static string UserSavedataDirPath = SuouBase.SuouEditorConst.UserDirectory + "/PathTree";
        public static string UserRelatedDirPath = SuouBase.SuouEditorConst.UserDirectory + "/PathTree/Related";
        [ReadOnly(true)]
        [Browsable(false)]
        public PathCollectionCollection PathItems
        {
            get
            {
                return this._PathItems;
            }
            set
            {
                if (value != null)
                {
                    this._PathItems = value;
                    this._PathItems.CollectionChanged += (a, b) => this.Set();
                    this.Set();
                }
            }
            
        }
        
        public MenuStrip FormMenuStrip
        {
            get { return null; }
        }
        #endregion
        #region Constructor
        public DirectoryMenuStrips()
        {
            InitializeComponent();
            var v = new PathCollectionCollection() { Name = "Default" };
            this.PathItems = v;
        }
        #endregion
        #region Method
        private void PathItemsCollectionChanged()
        {
            this.ResizeMethod();
        }
        public void ResizeMethod()
        {
            int i = 0;
            foreach (var v in this.StripItems)
            {
                i += v.Height;
            }
            this.Size = new Size(this.Width, i);
        }
        public void Set()
        {
            this.Controls.Clear();
            this.StripItems.Clear();
            int i=0;
            foreach (var v in this.PathItems)
            {
                if (i >= StripColores.Length) i = 0;
                var p = new DirectoryMenuStrip() { TreeItems = v,BackColor = StripColores[i++],ContextMenuStrip = this.contextMenuStrip1 };
                p.FileItemChanged += this.TreeItemChanged;
                p.FileItemClicked += (a, b) => { if (this.FileItemClicked != null)this.FileItemClicked(a, b); };
                this.Controls.Add(p);
                this.StripItems.Add(p);
            }
            this.PathItems.CollectionChanged += (a, b) => this.PathItemsCollectionChanged();
            this.ResizeMethod();
        }
        
        
        public void TestSet()
        {
            var p = new System.IO.DirectoryInfo("Data/HukuTemp");
            this.PathItems.Add(p.FullName, "HukuTemp");
        }
        private void CheckPathTreeFolder(string dir)
        {
            var fi = new DirectoryInfo(dir);
            if (!fi.Exists) fi.Create();
        }
        private void SaveTreeFile(string dir, string name,bool isShowedMessageBox)
        {
            this.CheckPathTreeFolder(dir);
            this.PathItems.Name = name;
            var filepath = string.Format("{0}/{1}.{2}", dir, name, "trr");
            BSystem.XML.NonBinarySerializer.Save(filepath, this.PathItems);
            if(isShowedMessageBox)MessageBox.Show("保存しました");
        }
        private string GetRelatedTreeFile(string name)
        {
            var filepath = string.Format("{0}/{1}.{2}", UserRelatedDirPath, name, "trr");
            return filepath;
        }
        private void CreateTreePathMenuItems(ToolStripMenuItem item)
        {
            this.CheckPathTreeFolder(UserSavedataDirPath);
            var fi = new DirectoryInfo(UserSavedataDirPath);
            item.DropDownItems.Clear();
            var files = fi.GetFiles().Where((v) => Path.GetExtension(v.FullName) == ".trr").ToArray();
            var strips = files.Select((v) =>
            {
                var w = new ToolStripMenuItem() { Text = Path.GetFileNameWithoutExtension(v.FullName), Tag = v.FullName };
                w.Click += this.LoadTool_Clicked;
                return w;
            }).ToArray();
            strips.ForEach((v) => item.DropDownItems.Add(v));

        }
        public void LoadRelatedItem(IBook book)
        {
            var name = Path.GetFileNameWithoutExtension(book.Name);
            if (name == this.PathItems.Name) return;
            if (book.IsExistFile && name.Length > 0)
            {
                var p = string.Format("{0}/{1}.{2}", UserRelatedDirPath, name, "trr");
                var fi = new FileInfo(p);
                if (fi.Exists)
                {
                    var item = BSystem.XML.NonBinarySerializer.Load<PathCollectionCollection>(fi.FullName);
                    if (item != null)
                    {
                        item.Name = name;
                        this.PathItems = item;
                    }
                }

            }
        }
        public void RemoveRelatedItem(string dir, string name)
        {
            this.CheckPathTreeFolder(dir);
            this.PathItems.Name = name;
            var filepath = string.Format("{0}/{1}.{2}", dir, name, "trr");
            FileInfo fi = new FileInfo(filepath);
            fi.Delete();
            MessageBox.Show("削除しました");
        }
        public void CreateMainMenuStrips(ToolStripMenuItem item)
        {
            item.DropDownOpening += (a, b) => this.CreateTreePathMenuItems((ToolStripMenuItem)item.DropDownItems[0]);
            item.DropDownItems[1].Click += this.saveTreeFileToolStripMenuItem_Click;
        }
        #endregion
        #region Event

        private void DirectoryMenuStrips_Load(object sender, EventArgs e)
        {
            this.Set();
        }

        private void DirectoryMenuStrips_Resize(object sender, EventArgs e)
        {
            this.ResizeMethod();
        }
        private void saveTreeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CheckPathTreeFolder(UserSavedataDirPath);
            var f = SuouBase.Controls.InputForm.CreateNameBox("保存するファイルの名前を入力してください", "", this.ParentForm, FormStartPosition.CenterParent);

            if (f != null && f.Length > 0)
            {
                var checkExistInvalidChar = Path.GetInvalidPathChars().Any((v) => f.ToCharArray().Contains(v));
                if (checkExistInvalidChar)
                {
                }
                else
                {
                    this.SaveTreeFile(UserSavedataDirPath, f,true);
                }
            }
        }
        private void LoadTool_Clicked(object sender, EventArgs e)
        {
            var path = (string)((ToolStripItem)sender).Tag;
            var item = BSystem.XML.NonBinarySerializer.Load<PathCollectionCollection>(path);
            if (item != null)
            {
                this.PathItems = item;
                MessageBox.Show("読み込みました");
            }
        }

        private void relateWithStoryFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.RelatedFunc != null)
            {
                var book = this.RelatedFunc();
                if (book != null && book.IsExistFile)
                {
                    this.SaveTreeFile(UserRelatedDirPath, Path.GetFileNameWithoutExtension(book.FullFilePath),true);
                }
            }
        }
        private void TreeItemChanged(object sender,EventArgs e)
        {
            if (this.RelatedFunc != null)
            {
                var book = this.RelatedFunc();
                if (book != null && book.IsExistFile)
                {
                    var path1 = Path.GetFileNameWithoutExtension(book.FullFilePath);
                    var path2 = this.GetRelatedTreeFile(path1);
                    var fi = new FileInfo(path2);
                    if (!fi.Exists) return;
                    this.SaveTreeFile(UserRelatedDirPath, path1,false);
                }
            }
        }



        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            this.CreateTreePathMenuItems(this.loadTreeFileToolStripMenuItem);

            this.relateWithStoryFileToolStripMenuItem.Visible = this.RelatedFunc != null;
            this.relateWithStoryFileToolStripMenuItem.Enabled = this.RelatedFunc != null && this.RelatedFunc() != null;

            this.removeRelatedFileToolStripMenuItem.Visible = this.CheckRelatedFileToolStripVisible;
        }
        private void optionToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            //this.CreateTreePathMenuItems(this.toolStripMenuItem1);
        }
        #endregion

        private void removeRelatedFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var book = this.RelatedFunc();
            if (book.IsExistFile)
            {
                this.RemoveRelatedItem(UserRelatedDirPath, Path.GetFileNameWithoutExtension(book.FullFilePath));
            }
        }
        private bool CheckRelatedFileToolStripVisible
        {
            get
            {
                if (this.RelatedFunc != null)
                {
                    var book = this.RelatedFunc();

                    if (book != null && book.IsExistFile)
                    {
                        var name = Path.GetFileNameWithoutExtension(book.FullFilePath);
                        var p = string.Format("{0}/{1}.{2}", UserRelatedDirPath, name, "trr");
                        var fi = new FileInfo(p);
                        if (fi.Exists)
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
        }

        private void loadDefaultFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.PathItems = PathCollectionCollection.CreateDefaultPathCollections();
        }
        
        

        
        
        

    }
}
