﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BSystem.Collections;
using SuouBase;
using SuouBase.Book;
using SuouBase.Interface;
using SuouBase.Plugin;
using StoryViewer.BookControl.Display;
using StoryViewer.ImageViewer;
namespace StoryViewer.BookControl.Viewer.Tab
{
    public partial class TabViewer : UserControl,IBookDisplay
    {
        private static string SavedataPath = SuouBase.SuouEditorConst.SettingDirectory + "/TabViewer.stn";

        public event Action<object, IBookPage> Page_Clicked;
        private BSystem.Collections.SerializableDictionary<string, ImageControlOption> SaveDataDic { get; set; }
        public TabViewer()
        {
            InitializeComponent();
            this.Items = new SuperObservableCollection<IBook>();
            this.LoadSetting();
            this.Items.CollectionChanged += this.Item_Changed;
        }
        public SmallInfo Info { get; set; }
        private void LoadSetting()
        {
            var p = BSystem.XML.NonBinarySerializer.Load<BSystem.Collections.SerializableDictionary<string, StoryViewer.ImageViewer.ImageControlOption>>(SavedataPath);
            if (p == null) p = new SerializableDictionary<string, ImageViewer.ImageControlOption>();
            this.SaveDataDic = p;
        }
        
        public SuperObservableCollection<IBook> Items { get; private set; }
        private IEnumerable<AABookViewerTabpage> TabPages
        {
            get
            {
                return this.superTabControl1.TabPages.Cast<AABookViewerTabpage>();
            }
        }
        public void SetBook(IBook book)
        {
            this.SetBook(book, false);
        }
        public void SetBook(IBook book, bool isEdit)
        {
            if (this.Items.Contains(book))
            {
                var p = this.TabPages.First((v) => v.Book == book);
                this.superTabControl1.SelectedTab = p;
            }
            else
            {
                this.Items.Add(book);
            }
        }

        private void Item_Changed(object sender, SuperObservableCollectionEventArgs<IBook> e)
        {
            if (e.Action == SuperObservableCollectionAction.Add)
            {
                foreach (var v in e.AddedItems)
                {
                    var tab = this.Create(v);
                    this.superTabControl1.Controls.Add(tab);
                    this.superTabControl1.SelectedTab = tab;
                }
            }
            else if (e.Action == SuperObservableCollectionAction.Remove)
            {
                foreach (var book in e.RemovedItems)
                {
                    var tab = this.superTabControl1.TabPages.Cast<AABookViewerTabpage>().First((w) => w.Book == book);
                    this.superTabControl1.TabPages.Remove(tab);
                }
            }
        }
        private AABookViewerTabpage Create(IBook data)
        {
            var p = new StoryViewer.ImageViewer.AA.AABookViewer() { Dock = DockStyle.Fill,IsInnerDragMove = false};
            if (data.FullFilePath != null)
            {
                if (!this.SaveDataDic.ContainsKey(data.FullFilePath))
                {
                    this.SaveDataDic[data.FullFilePath] = new ImageViewer.ImageControlOption();
                }
                p.SizeOption = this.SaveDataDic[data.FullFilePath];
            }
            p.ItemClicked += this.Picture_Clicked; 
            p.Book = data;
            p.EditorSmallInfo = this.Info;
            p.ContextMenuStrip = this.contextMenuStrip1;
            var tabpage = new AABookViewerTabpage(p);
            return tabpage;
        }
        private void Picture_Clicked(object sender, IIMageControlTag tag)
        {
            var p = tag as ImageAATag;
            if (this.Page_Clicked != null && p != null)
            {
                this.Page_Clicked(this, p.Page);
            }
        }
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

            foreach (var v in this.superTabControl1.TabPages)
            {
                if (v != this.superTabControl1.SelectedTab)
                {
                    var p = (AABookViewerTabpage)v;
                    p.Item.ClearCache();
                }
            }
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var tab = (AABookViewerTabpage)this.superTabControl1.TabPageContextMenuStrip.Tag;
            if (tab != null)
            {
                this.Close(tab);
            }
        }
        private bool Close(AABookViewerTabpage tab)
        {
            
            if (tab.Book.FullFilePath != null)
            {
                this.SaveDataDic[tab.Book.FullFilePath] = tab.Item.SizeOption;
            }

            var b = tab.DoClose();
            if (b)
            {
                var b1 = this.Items.Remove(tab.Book);
            }
            return b;

        }
        private bool CloseAll()
        {
            var p = true;
            foreach (AABookViewerTabpage tab in this.superTabControl1.TabPages)
            {
                var b = this.Close(tab);
                p = b && p;
            }
            return p;
        }

        private void closeAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CloseAll();
        }
        public void MyDispose()
        {
            this.CloseAll();
            var fi = new System.IO.DirectoryInfo(SuouBase.SuouEditorConst.SettingDirectory);
            if (!fi.Exists) fi.Create();

            BSystem.XML.NonBinarySerializer.Save(SavedataPath, this.SaveDataDic);
        }
    }
}
