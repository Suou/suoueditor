﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Windows.Forms;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.Tab
{
    public class GenericTabPage<T> : TabPage where T : Control
    {
        public GenericTabPage():base()
        {
        }
        public GenericTabPage(T control):base()
        {
            this.Controls.Add(control);
            control.Dock = DockStyle.Fill;
            this.Item = control;
        }
        public T Item { get; private set; }

    }
    public class AABookViewerTabpage:GenericTabPage<StoryViewer.ImageViewer.AA.AABookViewer>
    {
        public IBook Book { get; set; }
        public AABookViewerTabpage():base()
        {
        }
        public AABookViewerTabpage(StoryViewer.ImageViewer.AA.AABookViewer viewer):base(viewer)
        {
            this.Text = viewer.Book.Name;
            this.Book = viewer.Book;
        }
        public bool DoClose()
        {
            return this.Item.Close();
        }
    }
}
