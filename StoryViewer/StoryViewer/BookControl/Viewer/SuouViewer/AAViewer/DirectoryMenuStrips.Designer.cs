﻿namespace StoryViewer.BookControl.Viewer.DirectoryMenuStrip
{
    partial class DirectoryMenuStrips
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.loadTreeFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveTreeFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.relateWithStoryFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removeRelatedFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadDefaultFolderToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadTreeFileToolStripMenuItem,
            this.saveTreeFileToolStripMenuItem,
            this.relateWithStoryFileToolStripMenuItem,
            this.removeRelatedFileToolStripMenuItem,
            this.loadDefaultFolderToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(261, 136);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // loadTreeFileToolStripMenuItem
            // 
            this.loadTreeFileToolStripMenuItem.Name = "loadTreeFileToolStripMenuItem";
            this.loadTreeFileToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.loadTreeFileToolStripMenuItem.Text = "お気に入りフォルダを読み込む";
            // 
            // saveTreeFileToolStripMenuItem
            // 
            this.saveTreeFileToolStripMenuItem.Name = "saveTreeFileToolStripMenuItem";
            this.saveTreeFileToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.saveTreeFileToolStripMenuItem.Text = "お気に入りフォルダを保存する";
            this.saveTreeFileToolStripMenuItem.Click += new System.EventHandler(this.saveTreeFileToolStripMenuItem_Click);
            // 
            // relateWithStoryFileToolStripMenuItem
            // 
            this.relateWithStoryFileToolStripMenuItem.Name = "relateWithStoryFileToolStripMenuItem";
            this.relateWithStoryFileToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.relateWithStoryFileToolStripMenuItem.Text = "メインAAファイルと関連付ける";
            this.relateWithStoryFileToolStripMenuItem.Click += new System.EventHandler(this.relateWithStoryFileToolStripMenuItem_Click);
            // 
            // removeRelatedFileToolStripMenuItem
            // 
            this.removeRelatedFileToolStripMenuItem.Name = "removeRelatedFileToolStripMenuItem";
            this.removeRelatedFileToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.removeRelatedFileToolStripMenuItem.Text = "メインAAファイルとの関連を外す";
            this.removeRelatedFileToolStripMenuItem.Click += new System.EventHandler(this.removeRelatedFileToolStripMenuItem_Click);
            // 
            // loadDefaultFolderToolStripMenuItem
            // 
            this.loadDefaultFolderToolStripMenuItem.Name = "loadDefaultFolderToolStripMenuItem";
            this.loadDefaultFolderToolStripMenuItem.Size = new System.Drawing.Size(260, 22);
            this.loadDefaultFolderToolStripMenuItem.Text = "デフォルト状態に戻す";
            this.loadDefaultFolderToolStripMenuItem.Click += new System.EventHandler(this.loadDefaultFolderToolStripMenuItem_Click);
            // 
            // DirectoryMenuStrips
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.MinimumSize = new System.Drawing.Size(50, 30);
            this.Name = "DirectoryMenuStrips";
            this.Load += new System.EventHandler(this.DirectoryMenuStrips_Load);
            this.Resize += new System.EventHandler(this.DirectoryMenuStrips_Resize);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loadTreeFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveTreeFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relateWithStoryFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removeRelatedFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadDefaultFolderToolStripMenuItem;


    }
}
