﻿namespace StoryViewer.BookControl.Viewer.BooksViewer
{
    partial class BooksViewerExtra
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.directoryMenuStrips1 = new StoryViewer.BookControl.Viewer.DirectoryMenuStrip.DirectoryMenuStrips();
            this.specialMenuStrip1 = new StoryViewer.BookControl.Viewer.DirectoryMenuStrip.SpecialMenuStrip();
            this.tabViewer1 = new StoryViewer.BookControl.Viewer.Tab.TabViewer();
            this.SuspendLayout();
            // 
            // directoryMenuStrips1
            // 
            this.directoryMenuStrips1.Dock = System.Windows.Forms.DockStyle.Top;
            this.directoryMenuStrips1.Location = new System.Drawing.Point(0, 26);
            this.directoryMenuStrips1.MinimumSize = new System.Drawing.Size(50, 30);
            this.directoryMenuStrips1.Name = "directoryMenuStrips1";
            this.directoryMenuStrips1.RelatedFunc = null;
            this.directoryMenuStrips1.Size = new System.Drawing.Size(404, 30);
            this.directoryMenuStrips1.TabIndex = 2;
            this.directoryMenuStrips1.FileItemClicked += new System.Action<SuouBase.Files.FileNode, System.EventArgs>(this.directoryMenuStrip1_FileItemClicked);
            // 
            // specialMenuStrip1
            // 
            this.specialMenuStrip1.BackColor = System.Drawing.Color.Aqua;
            this.specialMenuStrip1.DLLSmallInfo = null;
            this.specialMenuStrip1.Location = new System.Drawing.Point(0, 0);
            this.specialMenuStrip1.Name = "specialMenuStrip1";
            this.specialMenuStrip1.Size = new System.Drawing.Size(404, 26);
            this.specialMenuStrip1.TabIndex = 3;
            this.specialMenuStrip1.Text = "specialMenuStrip1";
            this.specialMenuStrip1.FilePathClicked += new System.Action<object, StoryViewer.BookControl.Viewer.DirectoryMenuStrip.SpecialMenuStripItemEventArgs>(this.specialMenuStrip1_FilePathClicked);
            // 
            // tabViewer1
            // 
            this.tabViewer1.BackColor = System.Drawing.SystemColors.Control;
            this.tabViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabViewer1.Info = null;
            this.tabViewer1.Location = new System.Drawing.Point(0, 56);
            this.tabViewer1.Name = "tabViewer1";
            this.tabViewer1.Size = new System.Drawing.Size(404, 322);
            this.tabViewer1.TabIndex = 4;
            this.tabViewer1.Page_Clicked += new System.Action<object, SuouBase.Book.IBookPage>(this.tabViewer1_Page_Clicked);
            // 
            // BooksViewerExtra
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabViewer1);
            this.Controls.Add(this.directoryMenuStrips1);
            this.Controls.Add(this.specialMenuStrip1);
            this.Name = "BooksViewerExtra";
            this.Size = new System.Drawing.Size(404, 378);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DirectoryMenuStrip.DirectoryMenuStrips directoryMenuStrips1;
        private DirectoryMenuStrip.SpecialMenuStrip specialMenuStrip1;
        private Tab.TabViewer tabViewer1;
    }
}
