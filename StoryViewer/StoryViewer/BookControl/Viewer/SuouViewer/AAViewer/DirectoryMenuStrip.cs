﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Files;

namespace StoryViewer.BookControl.Viewer.DirectoryMenuStrip
{
    public class DirectoryMenuStrip:ToolStrip
    {
        public event Action<FileNode, EventArgs> FileItemClicked;
        //public event Action<FileNode, EventArgs> FileItemDeleted;
        public event EventHandler FileItemChanged;
        public DirectoryMenuStrip():base()
        {
            this.TreeItems = new PathCollection();
            this.LayoutStyle = ToolStripLayoutStyle.Flow;
            this.MaxStripItemCount = 100;
        }
        public int MaxStripItemCount { get; set; }
        public int NowStripItemCount
        {
            get
            {
                return this.Items.Count;
            }
        }
        private Color SelectedColor = Color.Aqua;
        private string _SelectedPageName = null;
        public string SelectedPageName
        {
            get { return this._SelectedPageName; }
            set
            {
                this._SelectedPageName = value;
                this.ColoringSelectedButton();
            }
        }
        public bool IsSelectedColor { get; set; }
        public bool AlternativeSelect { get; set; }
        private void ColoringSelectedButton()
        {
            if (this.IsSelectedColor)
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    if (this.Items[i].BackColor == this.SelectedColor) this.Items[i].BackColor = this.BackColor;
                    if (this.TreeItems.Items[i] == this.SelectedPageName)
                    {
                        this.Items[i].BackColor = this.SelectedColor;
                    }
                }
            }

        }
        private void Item_Clicked(FileNode fn)
        {
            for (int i = 0; i < this.TreeItems.Items.Count;i++ )
            {
                if (this.TreeItems.Items[i] == fn.FullPath)
                {
                    this.SelectedPageName = fn.FullPath;
                }
            }
            if (this.FileItemClicked != null) this.FileItemClicked(fn, new EventArgs());
        }
        private PathCollection _TreeItems = new PathCollection();
        [ReadOnly(true)]
        [Browsable(false)]
        public PathCollection TreeItems
        {
            get
            {
                return this._TreeItems;
            }
            set
            {
                if (value != null)
                {
                    this._TreeItems = value;
                    this._TreeItems.Items.CollectionChanged += (a,b) => this.Set();
                    this.Set();
                }
            }
        }
        #region Method
        public void OnClick(FileNode f)
        {
            this.Item_Clicked(f);
        }
        
        
        public void Set()
        {
            this.Items.Clear();
            foreach (var v in this.TreeItems.Items)
            {
                var items = FileNode.Create(v);
                if(items != null)this.Items.Add(CreateItem(items,true));
            }
            if (this.IsSelectedColor) this.ColoringSelectedButton();
            if (this.FileItemChanged != null) this.FileItemChanged(this, new EventArgs());
        }
        public ToolStripMenuItem CreateItem(FileNode item,bool isTop)
        {
            var r = new ToolStripMenuItem() { Text = item.Name };
            if (isTop)
            {
                r.Tag = this.CreateMenuStrip(item);
                r.Overflow = ToolStripItemOverflow.AsNeeded;
                r.MouseDown += (a, b) =>
                {
                    if (b.Button == System.Windows.Forms.MouseButtons.Right)
                    {
                        var p = (ContextMenuStrip)r.Tag;
                        p.Show(Control.MousePosition);
                    }
                };
            }
            if (item.IsFile)
            {
                r.Click += (a, b) => { this.Item_Clicked(item); };
            }
            else
            {
                item.Nodes.ForEach((v) => r.DropDownItems.Add(this.CreateItem(v,false)));
            }
            return r;
        }
        public ContextMenuStrip CreateMenuStrip(FileNode item)
        {
            var r = new ContextMenuStrip();
            var c1 = new ToolStripMenuItem() { Text = "このノードを削除する" };
            c1.Click += (a, b) =>
            {
                this.TreeItems.Delete(item);
            };
            r.Items.Add(c1);
            return r;
        }
        #endregion

        
    }
}
