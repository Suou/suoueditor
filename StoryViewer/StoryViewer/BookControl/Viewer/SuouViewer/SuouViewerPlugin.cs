﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using SuouBase.Plugin;
using System.Windows.Forms;
using System.Drawing;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.SuouViewer
{
    [IComponentPlugin("SuouViewerPlugin", PluginType.Control)]
    public class SuouViewerPlugin : AbstractControlPlugin<SuouViewer>, ITabPlugin, ITemplatePageListClass
    {
        public override Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public IBook Item { get; set; }
        public SuouViewerPlugin():base()
        {
            this._Ctrl = new SuouViewer();
            this.FormItem = new SuouViewerForm();
            this.Setting = BSystem.XML.NonBinarySerializer.Load<SuouViewerPluginSetting>(SaveFilePath);
            var tempSetting = BSystem.XML.NonBinarySerializer.Load<SuouViewerTemplate>(AATemplateFilePath) ?? new SuouViewerTemplate();
            this._Ctrl.SuouViewerTemplateItem = tempSetting;

            this.FormItem.suouViewer1.SuouViewerTemplateItem = this._Ctrl.SuouViewerTemplateItem;
            this.FormItem.suouViewer1.Setting = this._Ctrl.Setting;
            
        }
        public void TabClick()
        {
        }
        public SuouViewerPlugin(EditorInfo pd)
            : this()
        {
            this.DLLFunction = pd;
            
            this._Ctrl.EditButtonClickedAction = (page) =>
            {
                if (this.DLLFunction.TextBoxPlugin == null)
                {
                    MessageBox.Show("テキストボックスプラグインが存在しません");
                }
                else
                {
                    this.DLLFunction.TextBoxPlugin.Page = page;
                }
            };
            this._Ctrl.NotifyViewPageSizeAction = (page) => this.DLLFunction.MasterForm.ViewPageSize(page);
            this._Ctrl.OpenByMainFile = (filenode) =>
            {
                if (filenode.IsFile)
                {
                    this.DLLFunction.FileData.AddFilePath(filenode.FullPath);
                }
            };



            this._Ctrl.DLLSmallInfo = pd.Info;
            this._Ctrl.RelateFilePathWithPathTreeFunc = this.GetNowFile;
            this.DLLFunction.FileData.TabChanged += this.FileData_TabChanged;
            
        }
        private void FileData_TabChanged(object sender, SuouBase.Interface.SuouEditorDataClass data)
        {
            if(this.DLLFunction.FileData.SelectData!=null){
                this._Ctrl.LoadPathTree(this.DLLFunction.FileData.SelectData.StoryFile);
            }
        }
        public override string Name
        {
            get { return "蘇芳Viewer"; }
        }
        private void Tool_Clicked(object sender, EventArgs e)
        {
            if (this.Form.Visible)
            {
                this.Form.Focus();
            }
            else
            {
                this.Form.Show();
            }
        }
        private IBook GetNowFile()
        {
            if (this.DLLFunction.FileData.SelectData != null)
            {
                return this.DLLFunction.FileData.SelectData.StoryFile;
            }
            return null;
        }

        public override bool DisposeCheck()
        {
            var r = this.Setting;
            var r2 = this._Ctrl.SuouViewerTemplateItem;
            r2.LocalAATemplate.Name = "Default";
            BSystem.XML.NonBinarySerializer.Save<SuouViewerPluginSetting>(SaveFilePath, r);
            BSystem.XML.NonBinarySerializer.Save(AATemplateFilePath, r2);
            var b = this._Ctrl.DisposeCheck();
            //var c = this.SubViewer.DisposeCheck();
            //this._Ctrl.SuperDispose();
            return b;
        }
        private SuouViewerPluginSetting _Setting = null;
        private SuouViewerPluginSetting Setting
        {
            get
            {
                var r = this._Ctrl.Setting;
                var r2 = this.FormItem.Setting;
                return new SuouViewerPluginSetting() { SaveData = r, FormSetting = r2 };
            }
            set
            {
                this._Setting = value;
                if (value != null)
                {
                    this._Ctrl.Setting = value.SaveData;
                    this.FormItem.Setting = value.FormSetting;
                }
            }
        }
        public string SaveFilePath = SuouBase.SuouEditorConst.GetSettingFile("SuouViewer.stn");
        public string AATemplateFilePath = SuouBase.SuouEditorConst.GetSettingFile("SuouViewerTemplate.svt");
        private SuouViewerForm FormItem { get; set; }
        public IBookPage SelectPage(Form form,string text)
        {
            
            this.FormItem.StartPosition = FormStartPosition.CenterParent;
            this.FormItem.suouViewer1.OpenSearchBox(text);
            var p = this.FormItem.ShowDialog(form);
            if (p == DialogResult.OK)
            {
                return this.FormItem.SelectedPage;
            }
            else
            {
                return null;
            }

        }
        
    }
    [Serializable]
    public class SuouViewerPluginSetting
    {
        public SuouViewerSaveData SaveData { get; set; }
        public BSystem.Controls.FormSaveData FormSetting { get; set; }
    }
}
