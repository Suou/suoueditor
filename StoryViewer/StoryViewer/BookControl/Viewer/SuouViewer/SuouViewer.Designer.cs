﻿namespace StoryViewer.BookControl.Viewer.SuouViewer
{
    partial class SuouViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SuouViewer));
            this.SearchPage = new System.Windows.Forms.TabPage();
            this.searchViewer1 = new StoryViewer.BookControl.Viewer.SearchViewer.SearchViewer();
            this.FileChoicePage = new System.Windows.Forms.TabPage();
            this.aaTemplateSelectControl1 = new StoryViewer.AAFormer.BookFoldersViewer();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.booksViewerExtra1 = new StoryViewer.BookControl.Viewer.BooksViewer.BooksViewerExtra();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.SearchPage.SuspendLayout();
            this.FileChoicePage.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // SearchPage
            // 
            this.SearchPage.Controls.Add(this.searchViewer1);
            this.SearchPage.Location = new System.Drawing.Point(4, 22);
            this.SearchPage.Name = "SearchPage";
            this.SearchPage.Padding = new System.Windows.Forms.Padding(3);
            this.SearchPage.Size = new System.Drawing.Size(550, 442);
            this.SearchPage.TabIndex = 2;
            this.SearchPage.Text = "検索";
            this.SearchPage.UseVisualStyleBackColor = true;
            // 
            // searchViewer1
            // 
            this.searchViewer1.AATemplate = null;
            this.searchViewer1.DLLSmallInfo = null;
            this.searchViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.searchViewer1.Location = new System.Drawing.Point(3, 3);
            this.searchViewer1.Name = "searchViewer1";
            this.searchViewer1.Size = new System.Drawing.Size(544, 436);
            this.searchViewer1.TabIndex = 0;
            // 
            // FileChoicePage
            // 
            this.FileChoicePage.Controls.Add(this.aaTemplateSelectControl1);
            this.FileChoicePage.Location = new System.Drawing.Point(4, 22);
            this.FileChoicePage.Name = "FileChoicePage";
            this.FileChoicePage.Padding = new System.Windows.Forms.Padding(3);
            this.FileChoicePage.Size = new System.Drawing.Size(550, 442);
            this.FileChoicePage.TabIndex = 1;
            this.FileChoicePage.Text = "ファイル選択";
            this.FileChoicePage.UseVisualStyleBackColor = true;
            // 
            // aaTemplateSelectControl1
            // 
            this.aaTemplateSelectControl1.DLLFunction = null;
            this.aaTemplateSelectControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aaTemplateSelectControl1.Location = new System.Drawing.Point(3, 3);
            this.aaTemplateSelectControl1.Name = "aaTemplateSelectControl1";
            this.aaTemplateSelectControl1.OpenByMainFileAction = null;
            this.aaTemplateSelectControl1.Size = new System.Drawing.Size(544, 436);
            this.aaTemplateSelectControl1.TabIndex = 0;
            this.aaTemplateSelectControl1.FileSelected += new System.Action<System.IO.FileInfo, System.EventArgs>(this.aaTemplateSelectControl1_FileSelected);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.booksViewerExtra1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(550, 442);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "AA選択";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // booksViewerExtra1
            // 
            this.booksViewerExtra1.DLLSmallInfo = null;
            this.booksViewerExtra1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.booksViewerExtra1.Location = new System.Drawing.Point(3, 3);
            this.booksViewerExtra1.Name = "booksViewerExtra1";
            this.booksViewerExtra1.RelateFilePathWithFileTree = null;
            this.booksViewerExtra1.Size = new System.Drawing.Size(544, 436);
            this.booksViewerExtra1.TabIndex = 0;
            this.booksViewerExtra1.ThumbnailItemTouched += new System.Action<SuouBase.Book.IBookPage, System.EventArgs>(this.ThumbnailItem_Touched);
            this.booksViewerExtra1.AAClicked += new System.Action<SuouBase.Book.IBookPage, System.EventArgs>(this.booksViewerExtra1_AAClicked);
            this.booksViewerExtra1.EditButtonClicked += new System.Action<SuouBase.Book.IBookPage, System.EventArgs>(this.booksViewerExtra1_EditButtonClicked);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.FileChoicePage);
            this.tabControl1.Controls.Add(this.SearchPage);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(558, 468);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Resize += new System.EventHandler(this.tabControl1_Resize);

            // 
            // SuouViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.MinimumSize = new System.Drawing.Size(300, 300);
            this.Name = "SuouViewer";
            this.Size = new System.Drawing.Size(558, 468);
            this.Load += new System.EventHandler(this.SuouViewer_Load);
            this.SizeChanged += new System.EventHandler(this.SuouViewer_SizeChanged);
            this.SearchPage.ResumeLayout(false);
            this.FileChoicePage.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage SearchPage;
        private Viewer.SearchViewer.SearchViewer searchViewer1;
        private System.Windows.Forms.TabPage FileChoicePage;
        private AAFormer.BookFoldersViewer aaTemplateSelectControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private Viewer.BooksViewer.BooksViewerExtra booksViewerExtra1;
        private System.Windows.Forms.TabControl tabControl1;

    }
}
