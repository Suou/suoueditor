﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
namespace StoryViewer.BookControl.Viewer.SuouViewer
{
    [Serializable]
    public class SuouViewerFormSaveData
    {
        public SuouViewerFormSaveData()
        {
            this.Save1 = new StoryViewer.BookControl.Viewer.SuouViewer.SuouViewerSaveData();
            this.Size = new Size(500, 500);
        }
        public StoryViewer.BookControl.Viewer.SuouViewer.SuouViewerSaveData Save1 { get; set; }
        public Size Size { get; set; }
    }
}
