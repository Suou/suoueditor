﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using System.IO;
using StoryViewer.BookControl;
using BSystem.Collections;
using SuouBase.Files;
using SuouBase;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer.BookControl.Viewer.SuouViewer
{
    public partial class SuouViewer : UserControl
    {
        public SuouViewer()
        {
            
            InitializeComponent();
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.aaTemplateSelectControl1.FavoriteTreeAddCommanded += this.aaTemplateSelectControl1_FavoriteTreeAddCommanded;
        }
        /*
        public void LoadSetting(string path, string templateFilePath)
        {
            this.LoadSetting(path);
            this.LoadTemplateFile(templateFilePath);
        }
        public bool LoadSetting(string path)
        {
            var r = BSystem.XML.NonBinarySerializer.Load<SuouViewerSaveData>(path);
            if (r != null)
            {
                this.Setting = r;
                return true;
            }
            else
            {
                this.InitializeCreate();
                return false;
            }
        }
        
        public void LoadTemplateFile(string templateFilePath)
        {
            this.SuouViewerTemplateItem = BSystem.XML.NonBinarySerializer.Load<SuouViewerTemplate>(templateFilePath) ?? new SuouViewerTemplate();
            var b = this.SuouViewerTemplateItem.LocalAATemplate.Any((v) => v.Name == SuouViewer.OtherName);
            if (!b) this.InitializeCreate();
        }
         */
        public Viewer.DirectoryMenuStrip.DirectoryMenuStrips DirectoryMenuStripsContextMenu
        {
            get {
                return this.booksViewerExtra1.DirectoryMenuStripsContextMenu;
            }
        }
        public event Action<object, IBookPage> PageClicked;
        public Action<IBookPage> NotifyViewPageSizeAction { get; set; }
        public Action<IBookPage> EditButtonClickedAction { get; set; }
        public Action<FileNode> OpenByMainFile
        {
            get { return this.aaTemplateSelectControl1.OpenByMainFileAction; }
            set { this.aaTemplateSelectControl1.OpenByMainFileAction = value; }
        }
        private void ThumbnailItem_Touched(IBookPage page, EventArgs e)
        {
            if (this.NotifyViewPageSizeAction != null) this.NotifyViewPageSizeAction(page);
            
            if (this.ViewerThumbnailItemTouched != null) this.ViewerThumbnailItemTouched(page, e);
        }
        public event Action<IBookPage, EventArgs> ViewerThumbnailItemTouched;
        private SmallInfo _DLLSmallInfo = null;
        public SmallInfo DLLSmallInfo
        {
            get { return this._DLLSmallInfo; }
            set
            {
                if (value == null) return;
                this._DLLSmallInfo = value;
                this.searchViewer1.DLLSmallInfo = this._DLLSmallInfo;
                this.booksViewerExtra1.DLLSmallInfo = this._DLLSmallInfo;
                this.aaTemplateSelectControl1.SetCachePath(SuouBase.SuouEditorConst.IndexDirectoryName);

            }
        }
        public bool DisposeCheck()
        {
            //this.booksViewer.SuperDispose();
            this.searchViewer1.DisposeCheck();
            var b = this.booksViewerExtra1.DisposeCheck();
            this.aaTemplateSelectControl1.SuperDispose();
            return b;
        }

        private TabPage SearchTab
        {
            get { return this.tabControl1.TabPages.Cast<TabPage>().FirstOrDefault((v) => v.Name == "SearchPage"); }
        }
        private TabPage FolderChoiceTab
        {
            get { return this.tabControl1.TabPages.Cast<TabPage>().FirstOrDefault((v) => v.Name == "FolderChoicePage"); }
        }
        private TabPage CreateNewSearchTab()
        {
            var p = new TabPage() { Name = "SearchPage",Text ="検索" };
            this.searchViewer1 = new Viewer.SearchViewer.SearchViewer() { Dock = DockStyle.Fill};
            p.Controls.Add(this.searchViewer1);
            return p;
        }
        private TabPage CreateNewFileChoiceTab()
        {
            var p = new TabPage() { Name = "FileChoicePage", Text = "ファイル選択" };
            this.aaTemplateSelectControl1 = new AAFormer.BookFoldersViewer() { Dock = DockStyle.Fill };
            p.Controls.Add(this.aaTemplateSelectControl1);
            return p;
        }

        public bool IsSearchOKFlag
        {
            get
            {
                return this.SearchTab != null;
            }
            set
            {
                if (value && !this.IsSearchOKFlag)
                {
                    this.tabControl1.TabPages.Add(this.CreateNewSearchTab());
                }
                else if(!value && this.IsSearchOKFlag)
                {
                    this.tabControl1.TabPages.Remove(this.SearchTab);
                    this.searchViewer1.Dispose();
                }
            }
        }
        public bool IsFileChoiceOKFlag
        {
            get
            {
                return this.FileChoicePage != null;
            }
            set
            {
                if (value && !this.IsFileChoiceOKFlag)
                {
                    this.tabControl1.TabPages.Add(this.CreateNewFileChoiceTab());
                }
                else if (!value && this.IsFileChoiceOKFlag)
                {
                    this.tabControl1.TabPages.Remove(this.FileChoicePage);
                    this.aaTemplateSelectControl1.Dispose();
                }
            }
        }

        private void aaTemplateSelectControl1_FileSelected(System.IO.FileInfo arg1, EventArgs arg2)
        {
            this.booksViewerExtra1.SetDisplayItem(arg1.FullName);
            this.tabControl1.SelectedIndex = 0;

        }

        //public string SaveFilePath = "Data/test.stn";
        public string SamplePersonFilePath4 = "Data/HukuTemp";

        
        public void InitializeCreate()
        {
            //var r = new DirectoryInfo(SonotaAAFolderPath);
            var r2 = new DirectoryInfo(this.SamplePersonFilePath4);
            if (r2.Exists) this.SuouViewerTemplateItem.GlobalAATemplate.Add(new SuouFileTree(r2));
            this.booksViewerExtra1.LocalTemplate = PathCollectionCollection.CreateDefaultPathCollections();
            /*
            if (r.Exists)
            {
                //r.GetDirectories().Select((v)=>FileNode.Create(v.FullName)).Where((v)=>v!=null).ForEach((v) => this.booksViewerExtra1.MainPathCollection.Add(v, OtherName));
            }
            */
        }
        public void MyDispose()
        {
            this.DisposeCheck();
        }

        private void aaTemplateSelectControl1_FavoriteTreeAddCommanded(KeyValuePair<SuouBase.Files.FileNode,string> arg1, EventArgs arg2)
        {

            this.booksViewerExtra1.MainPathCollection.Add(arg1.Key,arg1.Value);
            this.tabControl1.SelectedIndex = 0;
        }
        [ReadOnly(true)]
        [Browsable(false)]
        public SuouViewerTemplate SuouViewerTemplateItem
        {
            get
            {
                var r = new SuouViewerTemplate() { LocalAATemplate = this.booksViewerExtra1.LocalTemplate, GlobalAATemplate = this.aaTemplateSelectControl1.GlobalTemplate };
                return r;
            }
            set
            {
                if (value != null)
                {
                    value.LocalAATemplate.TrimNoContent();
                    this.aaTemplateSelectControl1.GlobalTemplate = value.GlobalAATemplate;
                    this.booksViewerExtra1.LocalTemplate = value.LocalAATemplate;
                    this.searchViewer1.AATemplate = value;
                    //this.listBoxAAViewer1.AATemplate = value;
                    //this.aaSearchControl1.AATemplate = value;

                }
            }
        }
        private SuouViewerSaveData _Setting = null;
        [ReadOnly(true)]
        [Browsable(false)]
        public SuouViewerSaveData Setting
        {
            get
            {
                var r = new SuouViewerSaveData() { Data2 = this.booksViewerExtra1.Setting };
                if (this.EditorForm == null)
                {
                    if(this._Setting!=null)r.SaveData = this._Setting.SaveData;
                }
                else
                {
                    r.SaveData = this.EditorForm.Setting;
                }
                return r;
            }
            set
            {
                if (value != null)
                {
                    this.booksViewerExtra1.Setting = value.Data2;
                    this._Setting = value;
                }
            }
        }

        private void SuouViewer_Load(object sender, EventArgs e)
        {
        }
        public void LoadPathTree(IBook book)
        {
            this.booksViewerExtra1.LoadPathTree(book);
        }
        private Framework.SimpleFramework.TextEditorForm EditorForm { get; set; }
        private SuouTextBox2.Controls.SimoziBox TextBox { get; set; }
        public Func<IBook> RelateFilePathWithPathTreeFunc
        {
            get { return this.booksViewerExtra1.RelateFilePathWithFileTree; }
            set { this.booksViewerExtra1.RelateFilePathWithFileTree = value; }
        }
        private void booksViewerExtra1_EditButtonClicked(IBookPage arg1, EventArgs arg2)
        {
            if (this.EditButtonClickedAction==null)
            {
                if (this.EditorForm == null)
                {
                    this.EditorForm = new Framework.SimpleFramework.TextEditorForm();
                    if(this._Setting != null)this.EditorForm.Setting = this._Setting.SaveData;
                    this.TextBox = new SuouTextBox2.Controls.SimoziBox();
                    this.EditorForm.SetPlugin(11, this.TextBox,"蘇芳エディタ",this.TextBox.Size);
                    this.ParentForm.Activated += this.MasterForm_Focused;
                }
                this.TextBox.BookPage = arg1;
                if (this.EditorForm.Visible)
                {
                    this.EditorForm.Visible = true;
                }
                else
                {
                    this.EditorForm.Show();
                }
            }
            else
            {
                if(this.EditButtonClickedAction != null)this.EditButtonClickedAction(arg1);
                //this.DLLFunction.TextBoxPlugin.Page = arg1;
            }
        }
        private void MasterForm_Focused(object sender, EventArgs e)
        {
            this.TextBox.BookPage = null;
            this.EditorForm.Visible = false;
        }
        private void tabControl1_Resize(object sender, EventArgs e)
        {
            //this.booksViewerExtra1.Dock = DockStyle.None;
            //this.booksViewerExtra1.Dock = DockStyle.Fill;
            //this.booksViewerExtra1.Size = this.panel1.Size;
        }

        private void SuouViewer_SizeChanged(object sender, EventArgs e)
        {

        }

        private void booksViewerExtra1_AAClicked(IBookPage arg1, EventArgs arg2)
        {
            if (this.PageClicked != null)
            {
                this.PageClicked(this, arg1);
            }
        }

        private void listBoxAAViewer1_PageClicked(object arg1, IBookPage arg2)
        {
            if (this.PageClicked != null)
            {
                this.PageClicked(this, arg2);
            }

        }
        public void OpenSearchBox(string text)
        {
            //this.tabControl1.SelectedTab = this.tabPage3;
            //this.aaSearchControl1.OpenFileSearchText(text);
            //this.listBoxAAViewer1.SearchText = text;
        }
    }
    [Serializable]
    public class SuouViewerSaveData
    {
        public StoryViewer.BookControl.Viewer.BooksViewer.BooksViewerExtraSavedata Data2 { get; set; }
        public StoryViewer.Framework.SimpleFramework.TextEditorFormSaveData SaveData { get; set; }
    }
}
