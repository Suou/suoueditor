﻿namespace StoryViewer.AAFormer
{
    partial class BookFoldersViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.bookFoldersDisplay = new StoryViewer.AASelecter.BookFoldersDisplay();
            this.SuspendLayout();
            // 
            // bookFoldersDisplay
            // 
            this.bookFoldersDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.bookFoldersDisplay.Location = new System.Drawing.Point(0, 0);
            this.bookFoldersDisplay.MinimumSize = new System.Drawing.Size(100, 100);
            this.bookFoldersDisplay.Name = "bookFoldersDisplay";
            this.bookFoldersDisplay.Padding = new System.Windows.Forms.Padding(3);
            this.bookFoldersDisplay.Size = new System.Drawing.Size(386, 353);
            this.bookFoldersDisplay.TabIndex = 0;
            this.bookFoldersDisplay.Type = StoryViewer.AASelecter.AATemplateType.Global;
            //this.bookFoldersDisplay.FevoriteTreeAddCommanded += new System.Action<SuouBase.Files.FileNode, System.EventArgs>(this.htmlAATemplate1_TreeItemAdded);
            // 
            // BookFoldersViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bookFoldersDisplay);
            this.Name = "BookFoldersViewer";
            this.Size = new System.Drawing.Size(386, 353);
            this.ResumeLayout(false);

        }

        #endregion

        private AASelecter.BookFoldersDisplay bookFoldersDisplay;
    }
}
