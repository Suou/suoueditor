﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Files;
using System.Security.Permissions;
using StoryViewer.WebYaruoAAViewer;
using SuouBase.Controls;
using SuouBase.Text;
using BSystem.Collections;
using System.IO;
using SuouBase.Text.Search;
using SuouBase.Book;
namespace StoryViewer.AASelecter
{
    [ToolboxItem(false)]
    public partial class BookFoldersDisplay : UserControl
    {
        #region Property
        public Action<FileNode> OpenByMainFileAction { get; set; }
        private SearchType TextSearchType
        {
            get
            {
                if (this.toolStripComboBox1.SelectedIndex == 0)
                {
                    return SearchType.FileName;
                }
                else if (this.toolStripComboBox1.SelectedIndex == 1)
                {
                    return SearchType.FirstLine;
                }
                else
                {
                    return SearchType.FileName;
                }
            }
        }
        private SuouFileTreeCollection _Item = new SuouFileTreeCollection();
        //public event Action<string, object> ItemSelected;
        [ReadOnly(true)]
        [Browsable(false)]
        public SuouFileTreeCollection Item
        {
            get
            {
                return this._Item;
            }
            set
            {
                if (value != null)
                {
                    this._Item = value;
                    this._Item.CollectionChanged += this.TreeCollectionItems_Changed;
                    this.RefreshItem();
                }
            }
        }
        private IList<BookDirectoryDisplay> _PageItems = new List<BookDirectoryDisplay>();
        [ReadOnly(true)]
        [Browsable(false)]
        public IList<BookDirectoryDisplay> PageItems
        {
            get { return this._PageItems; }
        }
        public event Action<FileInfo, EventArgs> FileClicked;
        public event Action<DirectoryInfo, EventArgs> FolderClicked;
        public event EventHandler MltItemRightClicked;
        public event Action<KeyValuePair<FileNode,string>, EventArgs> FevoriteTreeAddCommanded;
        //public EventHandler AddFolderEvented;
        private ContextMenuStrip ControlContextStrip
        {
            get
            {
                return this.contextMenuStrip3;
            }
        }
        public SearchBooks CacheBook { get; set; }
        public AATemplateType Type { get; set; }
        #endregion
        #region Constructor
        public BookFoldersDisplay()
        {
            InitializeComponent();
        }
        public BookFoldersDisplay(SuouFileTreeCollection item, AATemplateType type)
            : this()
        {
            this.Item = item;
            
            this.SetTemplate(Item, type);
        }
        #endregion
        #region Method
        public void RefreshItem()
        {
            foreach (var v in this.Item)
            {
                var b = this.PageItems.ToList().Exists((w) => w.TreeItem == v);
                if (!b)
                {
                    var f = new BookDirectoryDisplay(v,this.Type,false);
                    this.PageItems.Add(f);
                    this.AddTab(f,false);
                }
            }
        }

        public void SetTemplate(SuouFileTreeCollection item, AATemplateType type)
        {
            this.Item = item;
            this.Type = type;
            this._PageItems = this.Item.Select((v) => new BookDirectoryDisplay(v, this.Type,false)).ToList();

            //this.ContextMenuStrip = this.contextMenuStrip1;

            if (this.Type == AATemplateType.Global)
            {
                //this.ControlContextStrip = this.contextMenuStrip3;
                this.PageItems.ToList().ForEach((v) => v.AddFolderButtonClicked += this.addFolderToolStripMenuItem_Click);
            }


            foreach (var v in this.PageItems)
            {
                this.AddTab(v,false);
            }
        }

        public TabPage AddTab(BookDirectoryDisplay w,bool isSearchTab)
        {

            w.MltItemLeftClicked += this.WebTemplate_ItemClicked;
            w.MltItemRightClicked += this.WebTemplate_ItemRightClicked;
            w.TreeItem.GroupNum = this.RowNumber;
            var tp = new BookFoldersDisplayTab(w,isSearchTab);
            this.tabControl1.TabPages.Add(tp);
            return tp;
        }
        public SuouFileTree SelectedTree
        {
            get
            {
                return this.SelectedTab != null ? this.SelectedTab.Tree : null;
            }
        }
        private BookFoldersDisplayTab SelectedTab
        {
            get
            {
                return this.tabControl1.SelectedTab as BookFoldersDisplayTab;
            }
        }
        public void RefreshTab()
        {
            this.PageItems.ToList().ForEach((v) => v.Refresh());
        }
        #endregion
        #region Click
        private void WebTemplate_ItemClicked(object sender,EventArgs e)
        {
            var filename = (string)sender;

            var di = new System.IO.DirectoryInfo(filename);
            if (di.Exists)
            {
                if (FolderClicked != null) FolderClicked(di, new EventArgs());
            }
            else
            {
                if (FileClicked != null) FileClicked(new FileInfo(filename), new EventArgs());
            }
        }
        private void WebTemplate_ItemRightClicked(object sender, EventArgs e)
        {
            var p = Control.MousePosition;
            if (this.Type == AATemplateType.Global)
            {
                this.contextMenuStrip1.Show(p);
            }
            else if (this.Type == AATemplateType.Register)
            {
                this.contextMenuStrip2.Show(p);
            }
        }

        private void addRegisterFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectbrowser = ((BookDirectoryDisplay)this.tabControl1.SelectedTab.Tag);

            var filenode = selectbrowser.SelectedFileNode;
            this.CommandAddFavorite(filenode,"人物");
        }
        private void CommandAddFavorite(FileNode node,string str)
        {
            var p = new KeyValuePair<FileNode, string>(node, str);
            if (this.FevoriteTreeAddCommanded != null) this.FevoriteTreeAddCommanded(p, new EventArgs());
        }
        private void deleteItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectbrowser = ((BookDirectoryDisplay)this.tabControl1.SelectedTab.Tag);

            var filenode = selectbrowser.SelectedFileNode;

            if (MltItemRightClicked != null) MltItemRightClicked(filenode, new EventArgs());
        }

        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            bool tabcon = false;
            if (e.Button == MouseButtons.Right)
            {
                for (int i = 0; i < tabControl1.TabCount; i++)
                {
                    //タブとマウス位置を比較し、クリックしたタブを選択
                    if (tabControl1.GetTabRect(i).Contains(e.X, e.Y))
                    {
                        if (this.Type == AATemplateType.Global)
                        {
                            contextMenuStrip4.Show(Control.MousePosition);
                        }
                        tabControl1.SelectedTab = tabControl1.TabPages[i];
                        tabcon = true;
                        break;
                    }
                    
                }
                if (!tabcon)
                {
                    if (ControlContextStrip != null) ControlContextStrip.Show(Control.MousePosition);   
                }
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.tabControl1.SelectedIndex;
            
            if (n != -1)
            {
                var tabp = this.tabControl1.TabPages[n];
                var ntag = (BookDirectoryDisplay)(tabp.Tag);
                if (ntag.IsInstant)
                {
                    this.tabControl1.TabPages.RemoveAt(n);
                }
                else
                {
                    Item.Remove(Item[n]);
                    this.tabControl1.TabPages.RemoveAt(n);
                }
            }
        }

        private void addFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.AddFolder();
        }
        private void AddFolder()
        {
            var p = new BookFolderOpenDialog();
            var filetree = p.ShowDialog();
            if (filetree != null)
            {
                filetree.GroupNum = this.RowNumber;
                this.Item.Add(filetree);
                this.RefreshItem();
            }
        }
        private void TreeCollectionItems_Changed(object sender, EventArgs e)
        {
            this.RefreshItem();
        }
        private void testToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            var selectbrowser = ((BookDirectoryDisplay)this.tabControl1.SelectedTab.Tag);
            var filenode = selectbrowser.SelectedFileNode;

            var file = IBookHandler.Load(filenode.Name);
            var v = new FileUpgradeBox();
            v.UpgradeItem = file;
            var f = new BSystem.Controls.SingleForm(v,"");
            f.Show();


        }
        private void BookFoldersDisplay_MouseDown(object sender, MouseEventArgs e)
        {
            //bool tabcon = false;
            if (e.Button == MouseButtons.Right)
            {
                if (ControlContextStrip != null) ControlContextStrip.Show(Control.MousePosition);

            }

        }

        private void addFolderToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.AddFolder();
        }
        private void addFavoriteFolderStripToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("追加したいフォルダまたはファイルの名前の上で右クリックを押してください");
        }

        private void addOtherFolderToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectbrowser = ((BookDirectoryDisplay)this.tabControl1.SelectedTab.Tag);

            var filenode = selectbrowser.SelectedFileNode;
            this.CommandAddFavorite(filenode, SuouBase.Files.PathCollectionCollection.OtherName);
        }

        private void openByMainFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.OpenByMainFileAction == null) return;
            var selectbrowser = ((BookDirectoryDisplay)this.tabControl1.SelectedTab.Tag);

            var filenode = selectbrowser.SelectedFileNode;
            this.OpenByMainFileAction(filenode);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            var selectbrowser = ((BookDirectoryDisplay)this.tabControl1.SelectedTab.Tag);
            var filenode = selectbrowser.SelectedFileNode;
            this.openByMainFileToolStripMenuItem.Visible = filenode.IsFile && this.OpenByMainFileAction != null;
        }


        private void preToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //this.CreateCache();
        }
        #endregion
        #region Search
        private void FirstLineSearch()
        {

            var searchWord = this.toolStripTextBox2.Text;
            var rst = this.CacheBook.Search(searchWord, this.SelectedTree);

            if (rst.Length != 0)
            {
                var items = new SuouFileTree(rst.Select((v) => v.Key).ToArray(), searchWord);
                var disp = new BookDirectoryDisplay(items, AATemplateType.Global, true);
                this.tabControl1.SelectedTab = this.AddTab(disp,true);
                MessageBox.Show(string.Format("{0}件見つかりました", rst.Length));
            }
            else
            {
                MessageBox.Show("見つかりませんでした");
            }
        }
        private void FileNameSearch()
        {
            var searchWord = this.toolStripTextBox2.Text;
            var rst = this.SelectedTree.FileNodeIterator().Where((v) => v.Name.IndexOf(searchWord) != -1).ToArray();
            if (rst.Length != 0)
            {
                var items = new SuouFileTree(rst.Select((v) => v.FullPath).ToArray(), searchWord);
                var disp = new BookDirectoryDisplay(items, AATemplateType.Global, true);
                this.tabControl1.SelectedTab = this.AddTab(disp,true);
                MessageBox.Show(string.Format("{0}件見つかりました", rst.Length));
            }
            else
            {
                MessageBox.Show("見つかりませんでした");
            }

        }
        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (this.SelectedTab == null){
                MessageBox.Show("検索するフォルダタブを指定してください");
            }
            else if (this.SelectedTab.IsSearchTab)
            {
                MessageBox.Show("検索結果タブを閉じてください");
            }
            else
            {
                if (this.TextSearchType == SearchType.FileName)
                {
                    this.FileNameSearch();
                }
                else
                {
                    this.FirstLineSearch();
                }
            }
        }
        #endregion

        private int RowNumber
        {
            get
            {
                return (this.Width / 300) + 1;
            }
        }
        private void BookFoldersDisplay_SizeChanged(object sender, EventArgs e)
        {
            var n = this.RowNumber;
            if (n != this.Item.GroupNum)
            {
                this.Item.GroupNum = n;
                foreach (BookFoldersDisplayTab tp in this.tabControl1.TabPages)
                {
                    tp.Display.Refresh();
                }
            }
        }


    }
    class BookFoldersDisplayTab:TabPage
    {
        public bool IsSearchTab { get; private set; }
        public SuouFileTree Tree { get; private set; }
        public BookDirectoryDisplay Display
        {
            get { return (BookDirectoryDisplay)this.Tag; }
        }
        public BookFoldersDisplayTab(BookDirectoryDisplay w,bool isSearchTab):base()
        {

            this.Controls.Add(w);
            this.Tree = w.TreeItem;
            this.Text = w.TreeName;
            this.Tag = w;
            w.Dock = DockStyle.Fill;
            this.IsSearchTab = isSearchTab;
        }
    }
    public enum AATemplateType
    {
        Global,Register
    }
    enum SearchType
    {
        FileName,FirstLine
    }
}
