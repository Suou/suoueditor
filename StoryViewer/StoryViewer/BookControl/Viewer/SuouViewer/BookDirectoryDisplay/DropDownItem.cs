﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AAEditTabControl.SimpleGlobalTemplate;
using AAEditTabControl;
using SuouBase.Text;
using SuouBase.Interface;

namespace StoryViewer.AASelecter
{
    [ToolboxItem(false)]
    public class DropDownItem:ToolStripDropDownMenu
    {
        public DropDownItem(Control Control)
            : base()
        {
            this.ContainerItem = new ToolStripControlHost(Control);
            this.ItemControl = Control;
            this.Items.Add(ContainerItem);
            /*
            if (this.ItemControl is IDropDownItem)
            {
                var conf = (IDropDownItem)this.ItemControl;
                if (conf != null)
                {
                    foreach (var v in conf.ContextItems)
                    {
                        v.Opening += this.ItemContext_Opening;
                        v.Opened += this.ItemContext_Opened;
                    }
                }
            }
            */
            this.CloseTimer = new Timer() { Enabled = true, Interval = 100 };
            this.CloseTimer.Tick += this.CloseTimer_Tick;
            ChangerSoft = new DAndDSizeChanger(this.ItemControl, this.ItemControl, DAndDArea.Right | DAndDArea.Bottom, 15);
        }
        
        public Timer CloseTimer { get; set; }
        public ToolStripControlHost ContainerItem { get; set; }
        public Control ItemControl { get; set; }
        public bool IsOpen { get; set; }
        public bool NeedClose { get; set; }
        private DAndDSizeChanger ChangerSoft;

        private void CloseTimer_Tick(object sender, EventArgs e)
        {
            var p = this.PointToClient(Control.MousePosition);
            if (!this.ClientRectangle.Contains(p) && NeedClose )
            {
                this.Close();
                NeedClose = false;
            }
        }
        private void ItemContext_Opening(object sender, EventArgs e)
        {
            IsOpen = true;
        }
        private void ItemContext_Opened(object sender, EventArgs e)
        {
            IsOpen = false;
        }
        protected override void OnClosed(ToolStripDropDownClosedEventArgs e)
        {
            NeedClose = false;
            base.OnClosed(e);
        }
        protected override void OnClosing(ToolStripDropDownClosingEventArgs e)
        {
            if (this.IsOpen)
            {
                NeedClose = true;
                e.Cancel = true;
            }
            else
            {
                
            }
            base.OnClosing(e);
        }
    }
}
