﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Files;
using System.Runtime.Remoting.Messaging;
using System.Security.Permissions;
using StoryViewer.AAImageViewer.AASelecter.StringTemplate;
namespace StoryViewer.AASelecter
{
    [ToolboxItem(false)]
    [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
    [System.Runtime.InteropServices.ComVisibleAttribute(true)]
    public class BookDirectoryDisplay : WebBrowser
    {
        #region Property
        public event EventHandler MltItemLeftClicked;
        public event EventHandler MltItemRightClicked;
        public event EventHandler AddFolderButtonClicked;
        //public FolderData Item { get; set; }
        public string TreeName { get; set; }
        public SuouFileTree TreeItem { get; private set; }
        public FileNode SelectedFileNode { get; set; }
        IFileTreeBrowser TemplateItem { get; set; }
        public bool IsInstant { get; set; }

        private string HomeTemplateName { get; set; }
        #endregion
        #region Constructor
        private void InitializeComponent()
        {
            this.SuspendLayout();
            this.ResumeLayout(false);

        }
        public BookDirectoryDisplay()
            : base()
        {
            //this.TemplateItem = new TreeStringTemplate();
            this.TemplateItem = new TreeRazorEnzine();
            this.AllowWebBrowserDrop = false;
            this.IsWebBrowserContextMenuEnabled = false;
            this.WebBrowserShortcutsEnabled = false;
            this.ObjectForScripting = this;
            
        }
        public BookDirectoryDisplay(SuouFileTree tdc,AATemplateType type,bool isInstant)
            : this()
        {
            this.IsInstant = isInstant;
            if (type == AATemplateType.Global)
            {
                this.HomeTemplateName = "template1_g";
            }
            else
            {
                this.HomeTemplateName = "template1";
            }
            this.TreeItem = tdc;
            this.Set();   
        }
        #endregion
        #region Method
        private void Set()
        {
            //this.Item = tdc;
            this.TreeName = this.TreeItem.Name;
            this.Refresh();

        }
        public new void Refresh()
        {
            //this.TreeItem = new SuouFileTree(this.TreeItem.);
            AsyncCallback callback = new AsyncCallback(this.some_Completed);
            var func = this.TemplateItem.Create(this.HomeTemplateName,this.TreeItem);
            var rst = func.BeginInvoke(callback,null);

            
        }
        private void some_Completed(IAsyncResult async)
        {
            // AsyncResult にキャストします。
            AsyncResult asyncResult = (AsyncResult)async;
            
            // AsyncDelegate で SomeDelegate のインスタンスが取得できます。
            Func<string> some = (Func<string>)asyncResult.AsyncDelegate;
            var text = (string)some.EndInvoke(async);
            this.DocumentText = text;
        }
        #endregion
        #region Javascript_Function
        
        public void c_sharp_func(String message)
        {
            var filename = this.TreeItem.NodeDic[message].FullPath;
            if (MltItemLeftClicked != null) MltItemLeftClicked(filename, new EventArgs());
        }
        
        public void rightclick_func(String message)
        {
            
            this.SelectedFileNode = this.TreeItem.NodeDic[message];
            if (MltItemRightClicked != null) MltItemRightClicked(message, new EventArgs());
        }
        public void addFolderFunc_func()
        {
            if (AddFolderButtonClicked != null) AddFolderButtonClicked(this, new EventArgs());
        }
        #endregion
    }
}
