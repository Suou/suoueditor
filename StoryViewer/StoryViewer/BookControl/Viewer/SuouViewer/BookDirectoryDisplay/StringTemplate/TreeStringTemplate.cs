﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Files;
using RazorEngine;

using BSystem.Collections;

namespace StoryViewer.AAImageViewer.AASelecter.StringTemplate
{
    public interface IFileTreeBrowser
    {
        Func<string> Create(string templateName, SuouFileTree TreeItem);
    }
    public class TreeStringTemplate:IFileTreeBrowser
    {
        public Func<string> Create(string templateName,SuouFileTree TreeItem)
        {
            Func<string> t = () =>
            {
                var grp = new Antlr3.ST.StringTemplateGroup("mygroup", "Data/Template");
                var st = grp.GetInstanceOf(templateName);

                st.SetAttribute("Root", TreeItem);
                return st.ToString();
            };
            return t;
        }
    }
    public class TreeRazorEnzine : IFileTreeBrowser
    {
        public TreeRazorEnzine()
        {
            this.Template = BSystem.IOLibrary.Load(StoryViewer.Properties.Resources.FolderBrowserTemplatePath);
        }
        public string Template { get; set; }
        
        public Func<string> Create(string templateName, SuouFileTree TreeItem)
        {
            Func<string> t = () =>
            {
                var p = Razor.Parse(this.Template,TreeItem);
                //BaseExtension.WriterExtensions.WriteLine(p,"logp.txt");
                return p;
            };
            return t;
        }
    }
}
