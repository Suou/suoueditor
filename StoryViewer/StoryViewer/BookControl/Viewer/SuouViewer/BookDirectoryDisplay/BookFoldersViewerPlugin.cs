﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using SuouBase.Plugin;
using System.Windows.Forms;
using System.Drawing;
using SuouBase.Text;

namespace StoryViewer.AAFormer
{
    [IComponentPlugin("AATemplateSelectControlPlugin", PluginType.Control)]
    public class AATemplateSelectControlPlugin :IComponentPlugin,ITabPlugin
    {
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public BookFoldersViewer _Ctrl { get; set; }
        private EditorInfo DLLFunction { get; set; }
        public AATemplateSelectControlPlugin()
        {



        }
        public void TabClick()
        {
        }
        public AATemplateSelectControlPlugin(EditorInfo pd)
            : this()
        {
            this.DLLFunction = pd;
            this._Ctrl = new BookFoldersViewer(pd);
        }
        public string Name
        {
            get { return "AAフォルダ一覧"; }
        }
        private void Tool_Clicked(object sender, EventArgs e)
        {
            if (this.Form.Visible)
            {
                this.Form.Focus();
            }
            else
            {
                this.Form.Show();
            }
        }
        public bool DisposeCheck()
        {
            this._Ctrl.SuperDispose();
            return true;
        }
        public ContainerControl Control
        {
            get { return this._Ctrl; }
        }
        public Form Form
        {
            get { return null; }
        }
        public SettingPage[] SettingPage
        {
            get
            {
                return null;
            }
        }
    }
}
