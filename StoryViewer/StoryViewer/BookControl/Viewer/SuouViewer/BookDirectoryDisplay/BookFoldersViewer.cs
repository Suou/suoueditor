﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Plugin;
using SuouBase.Interface;
using SuouBase.Files;
using StoryViewer.AASelecter;
using AAEditTabControl.SimpleGlobalTemplate;
using AAEditTabControl;
using System.IO;
using SuouBase;

namespace StoryViewer.AAFormer
{
    public partial class BookFoldersViewer : UserControl
    {

        public EditorInfo DLLFunction { get; set; }
        public BookDictionary BookDic = new BookDictionary();
        public Action<FileNode> OpenByMainFileAction
        {
            get { return this.bookFoldersDisplay.OpenByMainFileAction; }
            set { this.bookFoldersDisplay.OpenByMainFileAction = value; }
        }
        [ReadOnly(true)]
        [Browsable(false)]
        public SuouFileTreeCollection GlobalTemplate
        {
            get
            {
                return this.bookFoldersDisplay.Item;
            }
            set
            {
                if (value != null)
                {
                    value.Check();
                    this.bookFoldersDisplay.Item = value;
                }
            }
        }

        //public string SaveFilePath = "Data/Test.xml";
        public event Action<FileInfo, EventArgs> FileSelected;
        public event Action<KeyValuePair<FileNode,string>, EventArgs> FavoriteTreeAddCommanded;
        public BookFoldersViewer()
        {
            InitializeComponent();
            //this.ContextMenuStrip = this.bookFoldersDisplay.contextMenuStrip3;
            this.bookFoldersDisplay.FileClicked += this.BookFileItem_FileClicked;
            this.bookFoldersDisplay.MltItemRightClicked += this.BookFileItem_RegisterAdd;
            this.bookFoldersDisplay.FevoriteTreeAddCommanded += this.htmlAATemplate1_TreeItemAdded;
        }
        public BookFoldersViewer(EditorInfo dLLFunction):this()
        {
            this.DLLFunction = dLLFunction;
        }
        public void SetCachePath(string path)
        {
            this.bookFoldersDisplay.CacheBook = new SuouBase.Text.Search.SearchBooks(path);
        }
        private void Global_AddFolderEvented(object sender, EventArgs e)
        {
        }
        private void BookFileItem_FileClicked(FileInfo info, EventArgs e)
        {
            if (this.FileSelected != null) this.FileSelected(info, e);
        }
        /*
        public void FocusViewerControl()
        {
            var con = this.DLLFunction.PluginList.FirstOrDefault((v) => v.Plugin is AASelectFormPlugin);
            if (con != null)
            {
                this.DLLFunction.Framework.Focus(con.Plugin);
            }
        }
        */
        private void BookFileItem_RegisterAdd(object sender, EventArgs e)
        {
            var filenode = (TreeNode)sender;

            if (filenode != null)
            {
                if (this.DLLFunction.LocalTemplatePlugin != null)
                {
                    this.DLLFunction.LocalTemplatePlugin.AddNode(filenode);
                    var f = new System.IO.FileInfo(filenode.Name);
                    MessageBox.Show("登録しました。" + "[" + f.Name + "]");
                    //this.FocusViewerControl();
                }

            }
            else
            {
                MessageBox.Show("Error");
            }

        }
        public void SuperDispose()
        {
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void htmlAATemplate1_TreeItemAdded(KeyValuePair<FileNode,string> arg1, EventArgs arg2)
        {
            if (this.FavoriteTreeAddCommanded != null) this.FavoriteTreeAddCommanded(arg1, arg2);
        }
    }
}
