﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoryViewer.BookControl.Viewer.SuouViewer
{
    public partial class SuouViewerForm : BSystem.Controls.SerializableForm
    {
        public SuouViewerForm():base()
        {
            InitializeComponent();
        }
        /*
        public SuouViewerForm(string path,string path2):this()
        {
            this.suouViewer1.LoadSetting(path,path2);
        }
        */
        public SuouBase.Book.IBookPage SelectedPage { get; set; }
        private void suouViewer1_PageClicked(object arg1, SuouBase.Book.IBookPage arg2)
        {
            this.SelectedPage = arg2;
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void SuouViewerForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Visible = false;
            }
        }
    }
}
