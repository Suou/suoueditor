﻿namespace StoryViewer.BookControl.Viewer.SuouViewer
{
    partial class SuouViewerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.suouViewer1 = new StoryViewer.BookControl.Viewer.SuouViewer.SuouViewer();
            this.SuspendLayout();
            // 
            // suouViewer1
            // 
            this.suouViewer1.DLLSmallInfo = null;
            this.suouViewer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.suouViewer1.EditButtonClickedAction = null;
            this.suouViewer1.IsFileChoiceOKFlag = true;
            this.suouViewer1.IsSearchOKFlag = true;
            this.suouViewer1.Location = new System.Drawing.Point(0, 0);
            this.suouViewer1.MinimumSize = new System.Drawing.Size(300, 300);
            this.suouViewer1.Name = "suouViewer1";
            this.suouViewer1.NotifyViewPageSizeAction = null;
            this.suouViewer1.OpenByMainFile = null;
            this.suouViewer1.RelateFilePathWithPathTreeFunc = null;
            this.suouViewer1.Size = new System.Drawing.Size(558, 364);
            this.suouViewer1.TabIndex = 0;
            this.suouViewer1.PageClicked += new System.Action<object, SuouBase.Book.IBookPage>(this.suouViewer1_PageClicked);
            // 
            // SuouViewerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 364);
            this.Controls.Add(this.suouViewer1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "SuouViewerForm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AA選択フォーム";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.SuouViewerForm_FormClosed);
            this.ResumeLayout(false);

        }

        #endregion

        public SuouViewer suouViewer1;

    }
}