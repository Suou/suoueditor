﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Files;
namespace StoryViewer.BookControl
{
    [Serializable]
    public class SuouViewerTemplate
    {
        public SuouViewerTemplate()
        {
            this.GlobalAATemplate = new SuouFileTreeCollection();
            this.LocalAATemplate = new PathCollectionCollection();
        }
        public SuouFileTreeCollection GlobalAATemplate { get; set; }
        public PathCollectionCollection LocalAATemplate { get; set; }

        
    }
}
