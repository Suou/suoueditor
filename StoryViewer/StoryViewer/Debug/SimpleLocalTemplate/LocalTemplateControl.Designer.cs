﻿namespace AAEditTabControl.SimpleLocalTemplate
{
    partial class LocalTemplateControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.DeleteNodeMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.haveTabTreeControl1 = new AAEditTabControl.HaveTabTreeControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.contextMenuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.DeleteNodeMenuItem1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(173, 26);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // DeleteNodeMenuItem1
            // 
            this.DeleteNodeMenuItem1.Name = "DeleteNodeMenuItem1";
            this.DeleteNodeMenuItem1.Size = new System.Drawing.Size(172, 22);
            this.DeleteNodeMenuItem1.Text = "ノードを削除する";
            this.DeleteNodeMenuItem1.Click += new System.EventHandler(this.deleteNodeToolStripMenuItem_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "ツリーファイルを開く";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // haveTabTreeControl1
            // 
            this.haveTabTreeControl1.Data = null;
            this.haveTabTreeControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.haveTabTreeControl1.Location = new System.Drawing.Point(0, 37);
            this.haveTabTreeControl1.MinimumSize = new System.Drawing.Size(100, 100);
            this.haveTabTreeControl1.Name = "haveTabTreeControl1";
            this.haveTabTreeControl1.Size = new System.Drawing.Size(233, 222);
            this.haveTabTreeControl1.TabIndex = 0;
            this.haveTabTreeControl1.TreeBoxContext = this.contextMenuStrip1;
            this.haveTabTreeControl1.NodeDoubleClicked += new System.Windows.Forms.TreeViewEventHandler(this.haveTabTreeControl1_NodeDoubleClicked);
            this.haveTabTreeControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.haveTabTreeControl1_MouseDown);
            this.haveTabTreeControl1.MouseEnter += new System.EventHandler(this.haveTabTreeControl1_MouseEnter);
            this.haveTabTreeControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.haveTabTreeControl1_MouseMove);
            this.haveTabTreeControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.haveTabTreeControl1_MouseUp);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(233, 37);
            this.panel1.TabIndex = 2;
            // 
            // LocalTemplateControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.haveTabTreeControl1);
            this.Controls.Add(this.panel1);
            this.MinimumSize = new System.Drawing.Size(200, 200);
            this.Name = "LocalTemplateControl";
            this.Size = new System.Drawing.Size(233, 259);
            this.contextMenuStrip1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public HaveTabTreeControl haveTabTreeControl1;
        public System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem DeleteNodeMenuItem1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel1;
    }
}
