﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
using AAEditTabControl.SimpleLocalTemplate;
using System.IO;
using SuouBase.Book;
using SuouBase.Text;


namespace AAEditTabControl.SimpleLocalTemplate
{
    //[ToolboxItem(false)]
    public partial class LocalTemplateControl : UserControl
    {
        public LocalTemplateControl()
        {
            InitializeComponent();
            this.haveTabTreeControl1.canUseNewTabmenu(true);
            
        }
        public bool EnableTreeButton
        {
            get { return this.panel1.Visible; }
            set
            {
                this.panel1.Visible = value;
            }
        }
        public FolderDataCollection Item
        {
            get
            {
                return this.Data;
            }
            set
            {
                this.Data = value;
            }
        }
        public ContextMenuStrip[] ContextItems
        {
            get
            {
                return new ContextMenuStrip[] { this.contextMenuStrip1, this.haveTabTreeControl1.ContextItem };
            }
        }

        public event TreeViewEventHandler NodeItemViewed;
        public FolderDataCollection Data
        {
            set
            {
                this.haveTabTreeControl1.Data = value;
            }
            get
            {
                return this.haveTabTreeControl1.Data;
            }
        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (this.haveTabTreeControl1.SelectedNode == null)
            {
                DeleteNodeMenuItem1.Enabled = false;
            }
            else
            {
                DeleteNodeMenuItem1.Enabled = true;
            }
        }


        private void deleteNodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.haveTabTreeControl1.SelectTabTreeSubControl.DeleteNode();
        }

        private void splitContainer1_Panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private Dictionary<IBook, FolderDataCollection> Dic = new Dictionary<IBook, FolderDataCollection>();

        //private IBook NowEditingFile { get; set; }

        public void OpenSaveFile()
        {
            var f = SuouBase.GeneralMethod.OpenViewerDialogBox();
            if (f.ShowDialog() == DialogResult.OK)
            {
                string s = f.FileName;
                this.OpenSpecialReadViewerFile(s);
            }
        }

        public void OpenReadViewerFile(string storyfilename)
        {

            this.OpenSpecialReadViewerFile(storyfilename);
        }
        public new void Dispose()
        {
            foreach (KeyValuePair<IBook, FolderDataCollection> ky in Dic)
            {
                LocalTemplateXML.SaveNowTabPage(ky.Key.Name, ky.Value);
                //f.SaveNowTabPage(ky.Key.Name, ky.Value);
            }
        }
        /// <summary>
        /// いつか統合する
        /// </summary>
        /// <param name="xmlfile"></param>
        private void OpenSpecialReadViewerFile(string xmlfile)
        {
            var f = ReadViewerFile(xmlfile);
            if (f == null)
            {
                throw new MyException("");
            }
            else
            {
                this.Item = f;
            }
            
        }

        private FolderDataCollection ReadViewerFile(string xmlfile)
        {
            var fi = new FileInfo(xmlfile);
            if (fi.Exists)
            {
                var f = AAFileTreeClass.ReadFileTreeSetting(fi.FullName);
                return f;
            }
            else
            {
                return new FolderDataCollection();
            }
        }
        public void Refresh(IBook f)
        {
            /*
            NowEditingFile = f;
            if (f == null)
            {
                this.Item = null;
            }
            else
            {
                if (Dic.ContainsKey(f))
                {
                    this.Item = Dic[f];
                }
                else
                {
                    OpenReadViewerFile(f.Name);
                }
            }
            */
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.OpenSaveFile();
        }

        private void haveTabTreeControl1_NodeDoubleClicked(object sender, TreeViewEventArgs e)
        {

            if (this.haveTabTreeControl1.SelectedNode == null) return;
            if (NodeItemViewed != null) NodeItemViewed(this, new TreeViewEventArgs(this.haveTabTreeControl1.SelectedNode));
        }
        /*
        private string HelpMessage =
@"このローカルテンプレートは
MLTファイルの選択を補助します。

ローカルテンプレートは編集ファイルごとに持つことができ
MLTファイルを選択して開くことが出来ます。

ローカルテンプレートにAAファイルまたはMLTフォルダを追加したいときは
グローバルテンプレート中のアイテムを右クリックして
「ローカルテンプレートにコピーする」を選択してください。
";
        */
        private void button2_Click(object sender, EventArgs e)
        {
            //Help.ShowPopup(this, HelpMessage, SuouBase.FolderGeneralMethod.GetControlLocation(this.button2));
        }

        private void haveTabTreeControl1_MouseDown(object sender, MouseEventArgs e)
        {
            this.OnMouseDown(MouseEventToClient(e));
        }

        private void haveTabTreeControl1_MouseEnter(object sender, EventArgs e)
        {
            this.OnMouseEnter(e);
        }

        private void haveTabTreeControl1_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(MouseEventToClient(e));
        }

        private void haveTabTreeControl1_MouseUp(object sender, MouseEventArgs e)
        {
            this.OnMouseUp(MouseEventToClient(e));
        }
        private MouseEventArgs MouseEventToClient(MouseEventArgs e)
        {
            var p = this.PointToClient(Control.MousePosition);
            var e2 = new MouseEventArgs(e.Button, e.Clicks, p.X, p.Y, e.Delta);
            return e2;
        }
        
    }
}
