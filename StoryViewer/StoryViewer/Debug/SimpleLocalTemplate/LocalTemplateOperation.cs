﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using System.IO;
using System.Windows.Forms;
using SuouBase;
using AAEditTabControl.SimpleLocalTemplate;
using SuouBase.Text;
using System.Drawing;
using SuouBase.Book;

namespace AAEditTabControl
{
    [IComponentPlugin("LocalTemplate",PluginType.Control)]
    public class LocalTemplateOperation:IRegisterTemplateClass,ILocalTemplateClass,NotifyControl
    {
        #region Property
        public EditorInfo DLLFunction { get; set; }
        
        public FolderDataCollection Item
        {
            get
            {
                return this._control.Data;
            }
            set
            {
                this._control.Data = value;
            }
        }
        
        public int Position { get; set; }


        public SettingPage[] SettingPage { get { return null; } }
        public Form Form
        {
            get { return null; }
        }
        private string FileName = SuouEditorConst.SettingDirectory+"/"+"FavoriteTemplate.stn";
        public TreeNodeCollection SelectedNodes
        {
            get
            {
                return this._control.haveTabTreeControl1.SelectedTabNodes;
            }
        }

        public TreeNode SelectedNode
        {
            get
            {
                return this._control.haveTabTreeControl1.SelectedNode;
            }
        }
        
        public FolderData SelectedTemplateData
        {
            get
            {
                return this._control.haveTabTreeControl1.SelectedTemplateData;
            }
        }
        public EventHandler NodeCliced;

        //public event TreeViewEventHandler NodeSelected;

        private LocalTemplateControl _control;
        public ContainerControl Control
        {
            get { return _control; }
            private set 
            {
                this._control = (LocalTemplateControl)value;
                this._control.NodeItemViewed += TreeControl_NodeSelected;
            }
        }
        #endregion
        #region Method
        public bool DisposeCheck()
        {
            var save = new FavoriteTemplateSaveData() { CtrlSize = this._control.Size, MinimumCtrlSize = this._control.MinimumSize };
            save.Save(FileName);

            this._control.Dispose();
            this.RemoveTool();
            return true;
            
        }
        private void RemoveTool()
        {
            DLLFunction.FileData.Menubar.OtherToolStrip.DropDownItems.Remove(tp);
            DLLFunction.FileData.Menubar.OtherToolStrip.DropDownOpened -= this.OpenTool_DropDownOpened;
        }
        public void OpenReadViewerFile(string storyfilename)
        {
            this._control.OpenReadViewerFile(storyfilename);
        }
        
        public void AddNode(TreeNode tn)
        {
            _control.haveTabTreeControl1.AddTopTreeNode(tn);
        }
        public void FocusNotify()
        {
            if (FocusNotifiedEventHandler != null) FocusNotifiedEventHandler(this, new EventArgs());
        }
        #endregion
        #region Constructor
        public LocalTemplateOperation()
        {
            this.Control = new LocalTemplateControl();

        }
        public LocalTemplateOperation(EditorInfo dLLFunction):this()
        {

            DLLFunction = dLLFunction;
            //OpenToolStripMenuItem();
            var f = DLLFunction.FileData.SelectData;
            var storyfile = (f != null) ? f.StoryFile : null;
            this._control.Refresh(storyfile);
            DLLFunction.FileData.TabChanged += this.SuperVisor_PageChanged;
            DLLFunction.LocalTemplatePlugin = this;

            var save = FavoriteTemplateSaveData.Load(FileName);
            if (save != null)
            {
                this._control.Size = save.CtrlSize;
                this._control.MinimumSize = save.MinimumCtrlSize;
            }

            
        }
        public LocalTemplateOperation(EditorInfo dLLFunction,string filename)
            : this(dLLFunction)
        {
            this._control.Refresh(new ObsoleteSectionBook() { Name ="RegisterFile",FullFilePath ="RegisterFile"});
        }
        public LocalTemplateOperation(EditorInfo dLLFunction, string filename,LocalTemplateControl ctrl)
        {
            this._control = ctrl;

            if (dLLFunction != null)
            {
                DLLFunction = dLLFunction;
                DLLFunction.LocalTemplatePlugin = this;
            }
            this._control.OpenReadViewerFile(filename);
            var save = FavoriteTemplateSaveData.Load(FileName);
            if (save != null)
            {
                this._control.Size = save.CtrlSize;
                this._control.MinimumSize = save.MinimumCtrlSize;
            }
            this._control.Refresh(new ObsoleteSectionBook() { Name = "RegisterFile", FullFilePath = "RegisterFile" });
            this._control.NodeItemViewed += this.TreeControl_NodeSelected;
        }
        public LocalTemplateOperation(string filename, LocalTemplateControl ctrl)
        {
            this._control = ctrl;

            this._control.OpenReadViewerFile(filename);
            var save = FavoriteTemplateSaveData.Load(FileName);
            if (save != null)
            {
                this._control.Size = save.CtrlSize;
                this._control.MinimumSize = save.MinimumCtrlSize;
            }
            this._control.Refresh(new ObsoleteSectionBook() { Name = "RegisterFile", FullFilePath = "RegisterFile" });
            this._control.NodeItemViewed += this.TreeControl_NodeSelected;
        }
        #endregion
        #region Event
        private void SuperVisor_PageChanged(object sender, SuouBase.Interface.SuouEditorDataClass data)
        {
            if (data == null)
            {
                this._control.Refresh(null);
            }
            else
            {
                var f = data.StoryFile;
                this._control.Refresh(f);
            }
        }
        public void TreeControl_NodeSelected(object sender, TreeViewEventArgs e)
        {
            NodeSelected(sender, e);
        }

        private void NodeSelected(object sender, TreeViewEventArgs e)
        {
            var tn = e.Node;
            if (tn != null)
            {
                if (tn.Tag is FileNodeClass)
                {
                    var tnt = tn.Tag as FileNodeClass;
                    if (this.NodeCliced != null) this.NodeCliced(tnt.FullName, e);
                    /*
                    if (!tnt.IsFile)
                    {
                        MessageBox.Show(tnt.Name + "が見つかりません");
                    }
                    else
                    {
                        DLLFunction.TemplatePageViewPlugin.Item = tnt.File;
                    }
                    */
                }
            }
        }
        public event EventHandler FocusNotifiedEventHandler;
        #endregion
        #region OpenButton
        ToolStripMenuItem tp = null;
        private void OpenToolStripMenuItem()
        {
            tp = new ToolStripMenuItem();
            tp.Name = "OpenLocalTemplateFile";
            tp.Text = "ローカルテンプレートファイルを開く";
            tp.Click += this.OpenToolStrip_Clicked;
            DLLFunction.FileData.Menubar.OtherToolStrip.DropDownOpened += this.OpenTool_DropDownOpened;
            DLLFunction.FileData.Menubar.OtherToolStrip.DropDownItems.Add(tp);
            
        }
        
        private void OpenToolStrip_Clicked(object sender,EventArgs e)
        {
            this._control.OpenSaveFile();
        }
        private void OpenTool_DropDownOpened(object sender, EventArgs e)
        {

            if (DLLFunction.FileData.SelectData == null)
            {
                tp.Enabled = false;
            }
            else
            {
                tp.Enabled = true;
            }
        }
        #endregion
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        [Serializable]
        public class FavoriteTemplateSaveData
        {
            public Size CtrlSize { get; set; }
            public Size MinimumCtrlSize { get; set; }

            public void Save(string filename)
            {
                BSystem.XML.BinarySerialize.SaveToBinaryFile(this, filename);
            }
            public static FavoriteTemplateSaveData Load(string filename)
            {
                var r = BSystem.XML.BinarySerialize.LoadFromBinaryFile<FavoriteTemplateSaveData>(filename);
                if (r != null)
                {
                    return (FavoriteTemplateSaveData)r;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
