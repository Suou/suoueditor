﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using System.IO;
using System.Threading; 
using System.Threading.Tasks; 
using System.Text.RegularExpressions;
using System.Xml;
using SuouBase.Interface;
using System.Diagnostics;

namespace AAEditTabControl
{
    public partial class TreeViewEx2 : TreeView
    {

        public TreeViewEx2()
        {
            InitializeComponent();
        }

        public void Renew()
        {
            RenewRecursive(this.Nodes);
        }

        private void RenewRecursive(TreeNodeCollection TNC)
        {
            foreach (TreeNode TN in TNC)
            {
                if (TN.Tag is FolderNodeClass)
                {
                    FolderNodeClass f = TN.Tag as FolderNodeClass;
                    if(TN.Name != f.Name)TN.Name = f.Name;
                    if (TN.Text != f.Name) TN.Text = f.Name;

                    RenewRecursive(TN.Nodes);
                }
                else
                {
                    FileNodeClass f = TN.Tag as FileNodeClass;
                    if (TN.Name != f.Name) TN.Name = f.Name;
                    if (TN.Text != f.Name) TN.Text = f.Name;
                }
            }
        }

        

        public bool IsSelected()
        {
            return this.SelectedNode != null;
        }


        public void SaveFileTree(XmlTextWriter xtw)
        {
             SaveXMLRecursive(xtw, this.Nodes);
        }
        private void SaveXMLRecursive(XmlTextWriter xtw,TreeNodeCollection TNC)
        {
            foreach (TreeNode TN in TNC)
            {
                if (TN.Tag is FolderNodeClass)
                {
                    xtw.WriteStartElement("Folder");
                    {
                        xtw.WriteAttributeString("Name", TN.Text);
                        xtw.WriteAttributeString("Path", TN.FullPath);
                        SaveXMLRecursive(xtw, TN.Nodes);
                    }
                    xtw.WriteEndElement();
                }
                else
                {
                    xtw.WriteStartElement("File");
                    {
                        xtw.WriteAttributeString("Name", TN.Text);
                        xtw.WriteElementString("Path", ((FileNodeClass)TN.Tag).FullName);
                    }
                    xtw.WriteEndElement();
                }
            }
        }

        public void Reload()
        {
        }


        public void DeleteNode()
        {
            var ti = this.SelectedNode;
            ti.Remove();
        }

        public void MoveLeft()
        {
            var ti = this.SelectedNode;
            if (ti.Level >= 2)
            {
                var te = ti.Parent.Parent.Nodes;
                ti.Remove();
                te.Add(ti);
            }else if(ti.Level == 1){
                ti.Remove();
                this.Nodes.Add(ti);
            }
        }

        private void TreeViewEx2_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Delete:
                    this.DeleteNode();
                    break;
            }
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                        this.MoveLeft();
                        break;
                }
            }
        }
    }
}
