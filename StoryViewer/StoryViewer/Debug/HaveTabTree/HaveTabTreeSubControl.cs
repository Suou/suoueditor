﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using SuouBase.Interface;
namespace AAEditTabControl
{
    [ToolboxItem(false)]
    public partial class HaveTabTreeSubControl : UserControl
    {

        public event TreeViewEventHandler NodeChanged;
        public event TreeViewEventHandler NodeDoubleClicked;
        public HaveTabTreeSubControl()
        {
            InitializeComponent();

            UInt32 OldStyle = SuouBase.Api.WINAPI.GetWindowLong(treeView1.Handle, (-16));
            OldStyle = OldStyle | (0x80);
            SuouBase.Api.WINAPI.SetWindowLong(treeView1.Handle, (-16), OldStyle);
        }

        private FolderData _TemplateData;
        public FolderData TemplateData
        {
            get { return _TemplateData; }
            private set
            {
                _TemplateData = value;
            }
        }

        private HaveTabTreeSubControl(TreeNode tn):this()
        {
            this.AVAFile(tn);
        }

        private HaveTabTreeSubControl(TreeNodeCollection tnc):this()
        {
            
            foreach (TreeNode tn in tnc)
            {
                this.AVAFile(tn);
            }
        }

        public HaveTabTreeSubControl(FolderData ltdc)
            : this(ltdc.TemplateData)
        {
            this.Text = ltdc.Title;
            this.TemplateData = ltdc;
        }

        public void AddFileTreeNode(TreeNode tn)
        {
            this.AVAFile(tn);
            this.TemplateData.TemplateData = treeView1.Nodes;
        }
        public void AddTopFileTreeNode(TreeNode tn)
        {
            this.AddTopTreeNode(tn);
            this.TemplateData.TemplateData = treeView1.Nodes;
        }

        
        public void Reload()
        {
        }




        private void treeViewEx21_AfterSelect(object sender, TreeViewEventArgs e)
        {

            if (NodeChanged != null)
            {
                NodeChanged(sender, e);
            }
        }


        private void treeViewEx21_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (NodeDoubleClicked != null)
            {
                NodeDoubleClicked(sender, new TreeViewEventArgs(e.Node));
            }
        }


        public void SaveXML(XmlTextWriter xtw)
        {
            xtw.WriteAttributeString("Name", this.Text);
            SuouBase.AAFileTreeClass.SaveFileTree(xtw, treeView1.Nodes);
        }



        public void AVAFile(TreeNode tn)
        {

            TreeNode TN = this.treeView1.SelectedNode;
            var tnn = (TreeNode)tn.Clone();
            if (TN == null)
            {
                if (!this.treeView1.Nodes.Contains(tn)) this.treeView1.Nodes.Add(tnn);
            }
            else if (TN.Parent == null)
            {
                if (!TN.Nodes.Contains(tn)) this.treeView1.Nodes.Add(tnn);
            }
            else
            {
                if (!TN.Parent.Nodes.Contains(tn)) TN.Parent.Nodes.Add(tnn);
            }
        }
        public void AddTopTreeNode(TreeNode tn)
        {

            TreeNode TN = this.treeView1.SelectedNode;
            var tnn = (TreeNode)tn.Clone();
            if (!this.treeView1.Nodes.Contains(tn)) this.treeView1.Nodes.Add(tnn);
            
        }


        #region AATab用

        public new void Load(string DirName)
        {
            this.Text = new FileInfo(DirName).Name;
            ParallelLoad(DirName);
        }

        private void ParallelLoad(string DirName)
        {
            Task task = Task.Factory.StartNew(() =>
            {
                var tv = AAFileTreeClass.MakeTree(DirName);
                Rush ru = AAFileTreeClass.TreeConc;
                this.BeginInvoke(ru, new object[] { treeView1.Nodes, tv });
            });


        }
        #endregion

        private void treeView1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Delete:
                    this.DeleteNode();
                    break;
                case Keys.Enter:
                    var e2 = new TreeViewEventArgs(this.treeView1.SelectedNode);
                    if (NodeDoubleClicked != null) NodeDoubleClicked(sender, e2);
                    break;
            }
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                        this.MoveLeft();
                        break;
                }
            }
        }

        public void DeleteNode()
        {
            var ti = this.treeView1.SelectedNode;
            ti.Remove();
            this.TemplateData.TemplateData = treeView1.Nodes;
        }

        public void MoveLeft()
        {
            var ti = this.treeView1.SelectedNode;
            if (ti.Level >= 2)
            {
                var te = ti.Parent.Parent.Nodes;
                ti.Remove();
                te.Add(ti);
            }
            else if (ti.Level == 1)
            {
                ti.Remove();
                this.treeView1.Nodes.Add(ti);
            }
            this.TemplateData.TemplateData = treeView1.Nodes;
        }

        

        private void treeView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                treeView1.SelectedNode = treeView1.GetNodeAt(e.X, e.Y);
            }
            this.OnMouseDown(e);
        }

        private void treeView1_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(e);
        }

        private void treeView1_MouseUp(object sender, MouseEventArgs e)
        {
            this.OnMouseUp(e);
        }


        
    }
}
