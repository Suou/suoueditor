﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using SuouBase.Interface;

namespace AAEditTabControl
{
    public partial class HaveTabTreePage : TabPage
    {

        private TemplateDataClass _TemplateData;
        public TemplateDataClass TemplateData
        {
            get { return _TemplateData; }
            private set
            {
                _TemplateData = value;
            }
        }

        private HaveTabTreePage(TreeNode tn)
        {
            InitializeComponent();
            this.AVAFile(tn);
        }

        private HaveTabTreePage(TreeNodeCollection tnc)
        {
            InitializeComponent();
            
            foreach (TreeNode tn in tnc)
            {
                this.AVAFile(tn);
            }
        }

        public HaveTabTreePage(TemplateDataClass ltdc):this(ltdc.TemplateData)
        {
            this.Text = ltdc.Title;
            this.TemplateData = ltdc;
        }

        public void AddFileTreeNode(TreeNode tn)
        {
            this.AVAFile(tn);
            this.TemplateData.TemplateData = treeView1.Nodes;
        }

        
        public void Reload()
        {
        }



        public event TreeViewEventHandler NodeSelected;
        public event TreeViewEventHandler NodeChanged;

        private void treeViewEx21_AfterSelect(object sender, TreeViewEventArgs e)
        {

            if (NodeChanged != null)
            {
                NodeChanged(sender, e);
            }
        }


        private void treeViewEx21_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (NodeChanged != null)
            {
                var p = new TreeViewEventArgs(e.Node);
                NodeSelected(sender, p);
            }

        }




        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        public void SaveXML(XmlTextWriter xtw)
        {
            xtw.WriteAttributeString("Name", this.Text);
            SuouBase.AAFileTreeClass.SaveFileTree(xtw, treeView1.Nodes);
        }

        private void HaveTabTreePage_DoubleClick(object sender, EventArgs e)
        {
            ChangeName();   
        }

        public void ChangeName()
        {
            
            string inputText = Interaction.InputBox("名前", "タブに名前をつける", this.Text, -1, -1);
            this.Text = inputText;
            this.TemplateData.Title = inputText;
        }

        public void AVAFile(TreeNode tn)
        {

            TreeNode TN = this.treeView1.SelectedNode;
            var tnn = (TreeNode)tn.Clone();
            if (TN == null)
            {
                if (!this.treeView1.Nodes.Contains(tn)) this.treeView1.Nodes.Add(tnn);
            }
            else if (TN.Parent == null)
            {
                if (!TN.Nodes.Contains(tn)) this.treeView1.Nodes.Add(tnn);
            }
            else
            {
                if (!TN.Parent.Nodes.Contains(tn)) TN.Parent.Nodes.Add(tnn);
            }
        }


        #region AATab用

        public void Load(string DirName)
        {
            this.Text = new FileInfo(DirName).Name;
            ParallelLoad(DirName);
        }

        private void ParallelLoad(string DirName)
        {
            Task task = Task.Factory.StartNew(() =>
            {
                var tv = AAFileTreeClass.MakeTree(DirName);
                Rush ru = AAFileTreeClass.TreeConc;
                this.BeginInvoke(ru, new object[] { treeView1.Nodes, tv });
            });


        }
        #endregion

        private void treeView1_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Delete:
                    this.DeleteNode();
                    break;
                case Keys.Enter:
                    var e2 = new TreeViewEventArgs(this.treeView1.SelectedNode);
                    if(NodeSelected != null) NodeSelected(sender,e2);
                    break;
            }
            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                        this.MoveLeft();
                        break;
                }
            }
        }

        public void DeleteNode()
        {
            var ti = this.treeView1.SelectedNode;
            ti.Remove();
            this.TemplateData.TemplateData = treeView1.Nodes;
        }

        public void MoveLeft()
        {
            var ti = this.treeView1.SelectedNode;
            if (ti.Level >= 2)
            {
                var te = ti.Parent.Parent.Nodes;
                ti.Remove();
                te.Add(ti);
            }
            else if (ti.Level == 1)
            {
                ti.Remove();
                this.treeView1.Nodes.Add(ti);
            }
            this.TemplateData.TemplateData = treeView1.Nodes;
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            DeleteNode();
        }

        private void contextMenuStrip1_Opening_1(object sender, CancelEventArgs e)
        {
            if (this.treeView1.SelectedNode == null)
            {
                DeleteNodeMenuItem1.Enabled = false;
            }
            else
            {
                DeleteNodeMenuItem1.Enabled = true;
            }
        }
    }
}
