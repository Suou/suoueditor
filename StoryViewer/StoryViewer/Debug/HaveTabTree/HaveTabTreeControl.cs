﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using SuouBase;
using SuouBase.Interface;
using Microsoft.VisualBasic;

namespace AAEditTabControl
{
    [ToolboxItem(false)]
    public partial class HaveTabTreeControl:UserControl
    {
        private FolderDataCollection _data;
        public ContextMenuStrip TreeBoxContext { get; set; }
        public FolderDataCollection Data
        {
            get { return this._data; }
            set
            {
                this._data = value;
                this.RenewMakeTabTree();
            }
        }
        public void OpenFile(string filename)
        {
            this.Data.Add(filename);
            this.Data = this.Data;
        }
        public void RenewMakeTabTree()
        {
            MethodInvoker a = this.MakeTabTree;
            if (this.IsHandleCreated)
            {
                this.BeginInvoke(a);
            }
            else
            {
                this.MakeTabTree();
            }
        }

        public void canUseNewTabmenu(bool b)
        {
            addNewTabToolStripMenuItem.Visible = b;
        }

        public ContextMenuStrip ContextItem
        {
            get
            {
                return this.contextMenuStrip1;
            }
        }

        public FolderData SelectedTemplateData
        {
            get
            {
                var v = this.SelectTabTreeSubControl;
                if (v != null)
                {
                    return v.TemplateData;
                }
                else
                {
                    return null;
                }
            }
        }

        public TreeNodeCollection SelectedTabNodes
        {
            get
            {
                var v = this.SelectTabTreeSubControl;
                if (v != null)
                {
                    return v.treeView1.Nodes;
                }
                else
                {
                    return null;
                }
            }
        }

        public TreeNode SelectedNode
        {
            get
            {
                var v = this.SelectTabTreeSubControl;
                if (v != null)
                {
                    return v.treeView1.SelectedNode;
                }
                else
                {
                    return null;
                }
            }
        }

        public void MakeTabTree()
        {
            if (this.IsDisposed) return;
            this.tabControl1.TabPages.Clear();
            if (this.Data == null)
            {
                return;
            }
            foreach (FolderData ltd in Data)
            {
                NewTabPage(ltd);
            }
            
        }
        public event TreeViewEventHandler NodeDoubleClicked;
        public event TreeViewEventHandler NodeSelected;
        public event TreeViewEventHandler NodeChanged;

        public HaveTabTreeControl()
        {
            InitializeComponent();
            this.Data = new FolderDataCollection();
        }

        private TabPage NewTabPage()
        {
            var tdc = new FolderData();
            //HaveTabTreePage tp = new HaveTabTreePage(tdc);
            return NewTabPage(tdc);
        }
        private TabPage NewTabPage(FolderData tdc)
        {
            if (this.Data == null)
            {
                this.Data = new FolderDataCollection();
            }
            this.Data.Add(tdc);
            var tp = CreateSubTabPage(tdc);
            tp.MouseMove += tabControl1_MouseMove;
            tp.MouseDown += tabControl1_MouseDown;
            tp.MouseUp += tabControl1_MouseUp;
            this.tabControl1.TabPages.Add(tp);
            return tp;
        }


        private TabPage CreateSubTabPage(FolderData tdc)
        {
            var ctrl = new HaveTabTreeSubControl(tdc);
            ctrl.treeView1.ContextMenuStrip = this.TreeBoxContext;

            var tabpage = new TabPage();
            tabpage.Controls.Add(ctrl);
            ctrl.Dock = DockStyle.Fill;
            ctrl.MouseMove += tabControl1_MouseMove;
            ctrl.MouseDown += tabControl1_MouseDown;
            ctrl.MouseUp += tabControl1_MouseUp;
            tabpage.MouseMove += tabControl1_MouseMove;
            tabpage.MouseDown += tabControl1_MouseDown;
            tabpage.MouseUp += tabControl1_MouseUp;
            tabpage.Tag = ctrl;
            tabpage.Text = tdc.Title;
            ctrl.NodeDoubleClicked += this.HabTabTreePage_NodeDoubleClicked;
            ctrl.NodeChanged += this.HabTabTreePage_NodeChanged;
            tabpage.DoubleClick += this.HaveTabTreePage_DoubleClick;
            return tabpage;
        }

        public void AddTreeNode(TreeNode tn)
        {
            TabPage tp = this.tabControl1.SelectedTab;
            if (tp == null) tp = this.NewTabPage();
            
            tp.MouseMove += tabControl1_MouseMove;
            tp.MouseDown += tabControl1_MouseDown;
            tp.MouseUp += tabControl1_MouseUp;
            var ctrl = (HaveTabTreeSubControl)tp.Tag;
            ctrl.AddFileTreeNode(tn);
            
            
        }
        public void AddTopTreeNode(TreeNode tn)
        {
            TabPage tp = this.tabControl1.SelectedTab;
            if (tp == null) tp = this.NewTabPage();

            tp.MouseMove += tabControl1_MouseMove;
            tp.MouseDown += tabControl1_MouseDown;
            tp.MouseUp += tabControl1_MouseUp;
            var ctrl = (HaveTabTreeSubControl)tp.Tag;
            ctrl.AddTopFileTreeNode(tn);


        }

        private void HabTabTreePage_NodeDoubleClicked(object sender, TreeViewEventArgs e)
        {
            if (this.NodeDoubleClicked != null)
            {
                this.NodeDoubleClicked(sender, e);
            }
        }

        private void HabTabTreePage_NodeSelected(object sender, TreeViewEventArgs e)
        {
            if (this.NodeSelected != null)
            {
                this.NodeSelected(sender, e);
            }
        }

        private void HabTabTreePage_NodeChanged(object sender, TreeViewEventArgs e)
        {
            if (this.NodeChanged != null)
            {
                this.NodeChanged(sender, e);
            }
        }


        [Obsolete]
        public void Renew()
        {
            /*
            for (int i = 0; i < this.tabControl1.TabPages.Count; i++)
            {
                ((HaveTabTreePage)this.tabControl1.TabPages[i]).Reload();
            }
            */
        }


        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            addNewTabToolStripMenuItem.Enabled = (this.Data != null);
            if (this.tabControl1.TabPages.Count > 0)
            {
                closeNowTabToolStripMenuItem.Enabled = true;
                changeTabNameToolStripMenuItem.Enabled = true;
            }
            else
            {

                closeNowTabToolStripMenuItem.Enabled = false;
                changeTabNameToolStripMenuItem.Enabled = false;
            }
        }

        private void addNewTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.NewTabPage();
        }

        private void closeNowTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var v = this.SelectTabTreeSubControl;
             
            if (v != null)
            {
                this.Data.Remove(v.TemplateData);
                this.tabControl1.TabPages.Remove(this.tabControl1.SelectedTab);
            }
        }

        [Obsolete]
        public void SaveNowTabPage(string FileName)
        {
            var d = new DirectoryInfo("filetree");
            if (!d.Exists) d.Create();

            XmlTextWriter xtw = null;
            string fname = @"filetree/" + FileName + ".xml";
            xtw = new XmlTextWriter(fname, Encoding.Default);
            xtw.Formatting = Formatting.Indented;
            string[] separator = { "\r\n" };

            xtw.WriteStartDocument();//XML宣言
            xtw.WriteStartElement("FileTreeSetting");//開始タグ
            {
                /*
                foreach (HaveTabTreePage tp in this.tabControl1.TabPages)
                {
                    xtw.WriteStartElement("File");
                    {
                        tp.SaveXML(xtw);
                    }
                    xtw.WriteEndElement();
                }
                 */
            }
            xtw.WriteEndElement();//htmlタグを閉じる
            xtw.WriteEndDocument();
            xtw.Close();
        }

        [Obsolete]
        public void SaveNowTabNames(XmlTextWriter xtw)
        {
            /*
            xtw.WriteStartElement("TabFileNames");
            {
                foreach (HaveTabTreePage tp in this.tabControl1.TabPages)
                {
                        xtw.WriteElementString("FolderName",tp.Text);
                }
            }
            xtw.WriteEndElement();
            */
        }

        public void ReadSettingFile(string FileName)
        {
            var f = AAFileTreeClass.ReadFileTreeSetting(FileName);
            foreach (FolderData tnc in f)
            {
                NewTabPage(tnc);
            }
        }

        public HaveTabTreeSubControl SelectTabTreeSubControl
        {
            get
            {
                var f = this.tabControl1.SelectedTab;
                if (f == null) return null;
                var e = f.Tag as HaveTabTreeSubControl;
                return e;
            }
        }

        private void changeTabNameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedTab != null)
            {
                this.ChangeName(this.tabControl1.SelectedTab);
            }
        }

        public void MakeNameDictionary(Dictionary<string,List<ToolStripItem>> Dic)
        {
            SuouBase.AAEditorXml.MakeNameDictionary(Dic, this.contextMenuStrip1.Items);
        }

        private void HaveTabTreePage_DoubleClick(object sender, EventArgs e)
        {
            TabPage tp = (TabPage)sender;
            ChangeName(tp);
        }

        public HaveTabTreeSubControl GetSubControl(TabPage tp)
        {
            var f = tp.Tag as HaveTabTreeSubControl;
            return f;
        }

        public void ChangeName(TabPage tp)
        {
            /*
            var f = GetSubControl(tp);
            string inputText = Interaction.InputBox("名前", "タブに名前をつける", this.Text, -1, -1);
            tp.Text = inputText;
            f.TemplateData.Title = inputText;
            */
        }

        private void tabControl1_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(e);
        }

        private void tabControl1_MouseDown(object sender, MouseEventArgs e)
        {
            this.OnMouseDown(e);
        }

        private void tabControl1_MouseUp(object sender, MouseEventArgs e)
        {
            this.OnMouseUp(e);
        }


    }
}
