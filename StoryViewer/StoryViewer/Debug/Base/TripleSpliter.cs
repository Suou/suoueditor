﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AAEditTabControl
{
    [ToolboxItem(false)]
    public partial class TripleSpliter : UserControl
    {
        public TripleSpliter()
        {
            InitializeComponent();
            Initializer();
            
        }
        [Browsable(false)]
        public TripleSpliterSaveData SaveData
        {
            get
            {
                var f = new TripleSpliterSaveData() 
                    { LeftUpperSaveData = LeftUpperDrawer.SaveData,RightBottomSaveData = RightBottomDrawer.SaveData};
                return f;
            }
            set
            {
                LeftUpperDrawer.SaveData = value.LeftUpperSaveData;
                RightBottomDrawer.SaveData = value.RightBottomSaveData;
            }
        }
        public bool IsFirst { get; set; }
        private Control _LeftUpperControl = null;
        public Control LeftUpperControl
        {
            get { return _LeftUpperControl; }
            set
            {
                if (IsFirst && value != null) LeftUpperDrawer.OpenSize = value.Size;
                _LeftUpperControl = value;

                LeftUpperPanelCheck();
            }
        }
        public bool LeftUpperDrawerIsDrawer
        {
            get { return LeftUpperDrawer.IsDrawer; }
            set { LeftUpperDrawer.IsDrawer = value; }
        }
        public bool RightBottomDrawerIsDrawer
        {
            get { return RightBottomDrawer.IsDrawer; }
            set { RightBottomDrawer.IsDrawer = value; }
        }
        private void LeftUpperPanelCheck()
        {
            if (LeftUpperControl != null)
            {
                this.LeftUpperDrawer.Ctrl = LeftUpperControl;
                //LeftUpperControl.Dock = DockStyle.Fill;
                if (LeftUpperDrawer.IsDrawer)
                {
                    LeftUpperDistance = LeftUpperDrawer.ButtonWidth;
                }
                else
                {
                    LeftUpperPanel.Size = LeftUpperDrawer.SaveData.OpenSize;
                }
                this.LeftUpperSplitter.Enabled = this.LeftUpperDrawer.DrawerOpen;
            }
            else
            {
                this.LeftUpperSplitter.Enabled = false;
                this.LeftUpperDistance = 0;
            }
            
        }
        private void RightBottomPanelCheck()
        {
            if (_RightBottomControl != null)
            {
                this.RightBottomDrawer.Ctrl = RightBottomControl;
                //RightBottomControl.Dock = DockStyle.Fill;
                if (RightBottomDrawer.IsDrawer)
                {
                    RightBottomDistance = RightBottomDrawer.ButtonWidth;
                }
                else
                {
                    RightBottomPanel.Size = RightBottomDrawer.SaveData.OpenSize;
                }
                this.RightBottomSplitter.Enabled = this.RightBottomDrawer.DrawerOpen;
            }
            else
            {
                this.RightBottomSplitter.Enabled = false;
                this.RightBottomDistance = 0;
            }
        }

        private Control _RightBottomControl = null;

        public Control RightBottomControl
        {
            get { return _RightBottomControl; }
            set
            {

                if (IsFirst && value !=null) RightBottomDrawer.OpenSize = value.Size;
                _RightBottomControl = value;
                RightBottomPanelCheck();

            }
        }
        private Control _CenterControl = null;
        public Control CenterControl
        {
            get
            {
                return _CenterControl;
            }
            set
            {
                _CenterControl = value;
                if (CenterControl == null) return;
                this.CenterPanel.Controls.Add(_CenterControl);
                _CenterControl.Dock = DockStyle.Fill;
            }
        }

        private TripleDirection _SplitterDirection = TripleDirection.Horizontal;

        public TripleDirection SplitterDirection
        {
            get
            {
                return _SplitterDirection;
            }
            set
            {
                _SplitterDirection = value;
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    LeftUpperPanel.Dock = DockStyle.Left;
                    LeftUpperSplitter.Dock = DockStyle.Left;
                    RightBottomPanel.Dock = DockStyle.Right;
                    RightBottomSplitter.Dock = DockStyle.Right;
                    RightBottomDrawer.DrawerDirection = DrawerDirection.Left;
                    LeftUpperDrawer.DrawerDirection = DrawerDirection.Right;
                }
                else
                {
                    LeftUpperPanel.Dock = DockStyle.Top;
                    LeftUpperSplitter.Dock = DockStyle.Top;
                    RightBottomPanel.Dock = DockStyle.Bottom;
                    RightBottomSplitter.Dock = DockStyle.Bottom;
                    RightBottomDrawer.DrawerDirection = DrawerDirection.Up;
                    LeftUpperDrawer.DrawerDirection = DrawerDirection.Down;
                }
            }
        }

        int RightBottomFixingLength = 30;
        int LeftUpperFixingLength = 30;
        int MinLength = 30;//真中パネルの最小長

        int RightBottomMaxLength
        {
            get
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    return RightBottomMaxSize.Width;
                }
                else
                {
                    return RightBottomMaxSize.Height;
                }
            }
        }
        int LeftUpperMaxLength
        {
            get
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    return LeftUpperMaxSize.Width;
                }
                else
                {
                    return LeftUpperMaxSize.Height;
                }
            }
        }
        Size RightBottomMaxSize
        {
            get
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    return new Size(this.Size.Width - LeftUpperSplitter.Location.X - MinLength, this.Size.Height);
                }
                else
                {
                    return new Size(this.Size.Width, this.Size.Height - LeftUpperSplitter.Location.Y - MinLength);
                }
            }
        }
        Size LeftUpperMaxSize
        {
            get
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    return new Size(RightBottomSplitter.Location.X - MinLength, this.Size.Height);
                }
                else
                {
                    return new Size(this.Size.Width, RightBottomSplitter.Location.Y - MinLength);
                }
            }
        }

        private void CreatePanelSize()
        {
            if (SplitterDirection == TripleDirection.Horizontal)
            {
                LeftUpperPanel.Size = new Size(LeftUpperFixingLength, this.Size.Height);
                RightBottomPanel.Size = new Size(RightBottomFixingLength, this.Size.Height);
            }
            else
            {
                LeftUpperPanel.Size = new Size(this.Size.Width, LeftUpperFixingLength);
                RightBottomPanel.Size = new Size(this.Size.Width, RightBottomFixingLength);
            }
        }

        private void TripleHorizontalSpliter_SizeChanged(object sender, EventArgs e)
        {
            //CreatePanelSize();            
        }

        public int RightBottomDistance
        {
            get
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    return this.PanelLength - RightBottomSplitter.Location.X;
                }
                else
                {
                    return this.PanelLength - RightBottomSplitter.Location.Y;
                }
            }
            set
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    RightBottomPanel.Size = new Size(value, this.Size.Height);
                }
                else
                {
                    RightBottomPanel.Size = new Size(this.Size.Width, value);
                }
            }
        }
        public int LeftUpperDistance
        {
            get
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    return LeftUpperSplitter.Location.X;
                }
                else
                {
                    return LeftUpperSplitter.Location.Y;
                }
            }
            set
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    LeftUpperPanel.Size = new Size(value, this.Size.Height);
                }
                else
                {
                    LeftUpperPanel.Size = new Size(this.Size.Width, value);
                }
            }
        }
        private int SplitterEventArgsDistance(SplitterEventArgs e)
        {
            return SplitterDirection == TripleDirection.Horizontal ? e.X : e.Y;
        }
        private int AppropriateDistance(Muki m, SplitterEventArgs e)
        {
            if (m == Muki.LeftUpper)
            {
                return SplitterEventArgsDistance(e) - 1;
            }
            else
            {
                var len = SplitterDirection == TripleDirection.Horizontal ? this.Size.Width : this.Size.Height;
                return len - SplitterEventArgsDistance(e) - 1;
            }
        }

        private void RightSplitter_SplitterMoving(object sender, SplitterEventArgs e)
        {
            if (SplitterEventArgsDistance(e) == RightBottomDistance) return;

            var MouseLen = SplitterEventArgsDistance(e);
            var RightLimitLen = LeftUpperDistance + MinLength;

            RightBottomDistance = MouseLen < RightLimitLen ? RightBottomMaxLength : AppropriateDistance(Muki.RightBottom, e);

            this.Refresh();           
        }
        private int PanelLength
        {
            get
            {
                if (SplitterDirection == TripleDirection.Horizontal)
                {
                    return this.Size.Width;
                }
                else
                {
                    return this.Size.Height;
                }
            
            }
        }

        private void RightSplitter_SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (LeftUpperDistance + RightBottomDistance > this.PanelLength) RightBottomPanel.Size = RightBottomMaxSize;
            RightBottomFixingLength = RightBottomDistance;
        }

        private void LeftSplitter_SplitterMoving(object sender, SplitterEventArgs e)
        {
            
            if (SplitterEventArgsDistance(e) == LeftUpperDistance) return;

            var MouseLen = SplitterEventArgsDistance(e);
            var RightLimitLen = LeftUpperDistance + MinLength;

            LeftUpperDistance = MouseLen > LeftUpperMaxLength ? LeftUpperMaxLength : AppropriateDistance(Muki.LeftUpper, e);

            this.Refresh();
            CenterPanel.Refresh();
        }

        private void LeftSplitter_SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (LeftUpperDistance + RightBottomDistance > this.PanelLength)
            {
                LeftUpperPanel.Size = LeftUpperMaxSize;
            }
            LeftUpperFixingLength = LeftUpperDistance;
        }

        private void RightPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.Dock = DockStyle.Fill;

        }

        private void CenterPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.Dock = DockStyle.Fill;
        }

        private void LeftPanel_ControlAdded(object sender, ControlEventArgs e)
        {
            e.Control.Dock = DockStyle.Fill;
        }

        private void drawerContainer1_SizeChanged(object sender, EventArgs e)
        {

        }


        private void Initializer()
        {
            LeftUpperPanelCheck();
            RightBottomPanelCheck();
            LeftUpperSplitter.ContextMenuStrip = LeftUpperDrawer.ButtonContext;
            RightBottomSplitter.ContextMenuStrip = RightBottomDrawer.ButtonContext;

        }


        private void RightBottomDrawer_DrawerChanged(object sender, DrawerEventArgs e)
        {
            RightBottomPanel.Size = e.ChangeSize;
            RightBottomFixingLength = RightBottomDistance;
            RightBottomSplitter.Enabled = e.Open;
        }

        private void LeftUpperDrawer_DrawerChanged(object sender, DrawerEventArgs e)
        {
            LeftUpperPanel.Size = e.ChangeSize;
            LeftUpperFixingLength = LeftUpperDistance;
            LeftUpperSplitter.Enabled = e.Open;
        }

        private void RightBottomDrawer_SizeChanged(object sender, EventArgs e)
        {
        }

        private void RightBottomDrawer_ParentChanged(object sender, EventArgs e)
        {
        }

        private void RightBottomDrawer_Load(object sender, EventArgs e)
        {

        }

        private void RightBottomPanel_SizeChanged(object sender, EventArgs e)
        {

        }

    }
    [Serializable()]
    public struct TripleSpliterSaveData
    {
        public DrawerContainerSaveData LeftUpperSaveData;
        public DrawerContainerSaveData RightBottomSaveData;
        
    }
    public enum TripleDirection
    {
        Vertical,Horizontal
    }
    public enum Muki
    {
        LeftUpper,RightBottom
    }
}
