﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Drawing;
using BSystem.Drawing;
namespace AAEditTabControl
{
    [ToolboxItem(false)]
    public partial class DrawerContainer : UserControl
    {
        public DrawerContainer()
        {
            InitializeComponent();
            ResetFix();

        }
        Bitmap LeftBmp = DrawTextBmp.CreateBitmapRapid("<]", SuouBase.SuouEditorConst.AAFont, 2, Color.Black,Color.Transparent);
        Bitmap RightBmp = DrawTextBmp.CreateBitmapRapid("[>", SuouBase.SuouEditorConst.AAFont, 2, Color.Black, Color.Transparent);
        Bitmap UpBmp = DrawTextBmp.CreateBitmapRapid("▲", SuouBase.SuouEditorConst.AAFont, 2, Color.Black, Color.Transparent);
        Bitmap DownBmp = DrawTextBmp.CreateBitmapRapid("▼", SuouBase.SuouEditorConst.AAFont, 2, Color.Black, Color.Transparent);

        public event DrawerEventHandler DrawerChanged;

        private DrawerDirection _DrawerDirection;
        public DrawerDirection DrawerDirection
        {
            get { return _DrawerDirection; }
            set
            {
                _DrawerDirection = value;
                ArrangeControl();
                ResetOrientation();
                ResetButtonText();
                ResetFix();
            }
        }

        private DrawerContainerSaveData _SaveData = new DrawerContainerSaveData() 
        {IsDrawer = true,AutoClose = false,AutoOpen = false,OpenSize = new Size(260,260) };

        [Browsable(false)]
        public DrawerContainerSaveData SaveData
        {
            get { return _SaveData; }
            set
            {
                OpenSize = value.OpenSize;
                AutoOpen = value.AutoOpen;
                AutoClose = value.AutoClose;
                IsDrawer = value.IsDrawer;
            }
        }


        private void ArrangeControl()
        {
            ControlPanel.Controls.Clear();
            DrawerButtonPanel.Controls.Clear();
            if (Ctrl != null)
            {
                ControlPanel.Controls.Add(Ctrl);
                Ctrl.Dock = DockStyle.Fill;
            }
            DrawerButtonPanel.Controls.Add(button1);
            ResetDrawer();
        }

        private void ResetButtonText()
        {
            
            if (!DrawerOpen)
            {
                button1.Text = null;
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                        button1.Image = UpBmp;
                        //button1.Text = "▲";
                        break;
                    case DrawerDirection.Left:
                        button1.Image = LeftBmp;
                        //button1.Text = "<]";
                        break;
                    case DrawerDirection.Down:
                        button1.Image = DownBmp;
                        //button1.Text = "▼";
                        break;
                    case DrawerDirection.Right:
                        button1.Image = RightBmp;
                        //button1.Text = "[>";
                        break;
                    default:
                        throw new Exception();
                }
            }
            else
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                        button1.Image = DownBmp;
                        //button1.Text = "▼";
                        break;
                    case DrawerDirection.Left:
                        button1.Image = RightBmp;
                        //button1.Text = "[>";
                        break;
                    case DrawerDirection.Down:
                        button1.Image = UpBmp;
                        //button1.Text = "▲";
                        break;
                    case DrawerDirection.Right:
                        button1.Image = LeftBmp;
                        //button1.Text = "<]";
                        break;
                    default:
                        throw new Exception();
                }
            }
        }

        int _ButtonWidth
        {
            get { return 25; }
        }
        public int ButtonWidth
        {
            get { return _ButtonWidth; }
        }

        Size BaseButtonSize
        {
            get
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Left:
                    case DrawerDirection.Right:
                        return new Size(_ButtonWidth, button1.Height);
                    case DrawerDirection.Down:
                    case DrawerDirection.Up:
                        return new Size(button1.Width,_ButtonWidth);
                    default:
                        throw new Exception();
                }
            }
        }
        int BaseSplitDistance
        {
            get
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                    case DrawerDirection.Left:
                        return _ButtonWidth;
                    case DrawerDirection.Down:
                        int c = splitContainer1.Size.Height - _ButtonWidth;
                        if (c < 0)
                        {
                            return 1;
                        }
                        else
                        {
                            return splitContainer1.Size.Height - _ButtonWidth;
                        }
                    case DrawerDirection.Right:
                        int a = splitContainer1.Size.Width - _ButtonWidth;
                        if (a < 0)
                        {
                            return 1;
                        }
                        else
                        {
                            return splitContainer1.Size.Width - _ButtonWidth;
                        }
                    default:
                        throw new Exception();
                }
            }
        }
        Control _Ctrl = null;
        public Control Ctrl
        {
            get { return _Ctrl; }
            set
            {
                RemoveCtrl();
                _Ctrl = value;
                AddCtrl();
                
            }
        }

        private void AddCtrl()
        {
            if (Ctrl != null)
            {
                Ctrl.MouseLeave += this.button1_MouseLeave;
                ArrangeControl();
            }
        }
        private void RemoveCtrl()
        {
            if (_Ctrl != null)
            {
                Ctrl.MouseLeave -= this.button1_MouseLeave;
                ControlPanel.Controls.Remove(_Ctrl);
            }
        }

        public bool IsCtrl { get { return Ctrl != null; } }

        private bool DrawerButtonCollapsed
        {
            get
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                    case DrawerDirection.Left:
                        return splitContainer1.Panel1Collapsed;
                    case DrawerDirection.Down:
                    case DrawerDirection.Right:
                        return splitContainer1.Panel2Collapsed;
                    default:
                        throw new Exception();
                }
            }
            set
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                    case DrawerDirection.Left:
                        splitContainer1.Panel1Collapsed = value;
                        break;
                    case DrawerDirection.Down:
                    case DrawerDirection.Right:
                        splitContainer1.Panel2Collapsed = value;
                        break;
                    default:
                        throw new Exception();
                }
            }
        }
        private bool ControlCollapsed
        {
            get
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                    case DrawerDirection.Left:
                        return splitContainer1.Panel2Collapsed;
                    case DrawerDirection.Down:
                    case DrawerDirection.Right:
                        return splitContainer1.Panel1Collapsed;
                    default:
                        throw new Exception();
                }
            }
            set
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                    case DrawerDirection.Left:
                        splitContainer1.Panel2Collapsed = value;
                        break;
                    case DrawerDirection.Down:
                    case DrawerDirection.Right:
                        splitContainer1.Panel1Collapsed = value;
                        break;
                    default:
                        throw new Exception();
                }
            }
        }
        private Panel ControlPanel
        {
            get
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                    case DrawerDirection.Left:
                        return splitContainer1.Panel2;
                    case DrawerDirection.Down:
                    case DrawerDirection.Right:
                        return splitContainer1.Panel1;
                    default:
                        throw new Exception();
                }
            }
        }
        private Panel DrawerButtonPanel
        {
            get
            {
                switch (DrawerDirection)
                {
                    case DrawerDirection.Up:
                    case DrawerDirection.Left:
                        return splitContainer1.Panel1;
                    case DrawerDirection.Down:
                    case DrawerDirection.Right:
                        return splitContainer1.Panel2;
                    default:
                        throw new Exception();
                }
            }
        }

        private void ResetDrawer()
        {
            if (IsDrawer)
            {
                Close();
                DrawerButtonCollapsed = false;
                ControlCollapsed = true;
            }
            else
            {
                Open();
                DrawerButtonCollapsed = true;
                ControlCollapsed = false;
            }
        }
        public bool IsDrawer
        {
            get { return SaveData.IsDrawer; }
            set 
            {
                _SaveData.IsDrawer = value;
                ResetDrawer();
            }
        }
        public bool AutoOpen
        {
            get { return SaveData.AutoOpen; }
            set { _SaveData.AutoOpen = value; }
        }
        public bool AutoClose
        {
            get { return SaveData.AutoClose; }
            set { _SaveData.AutoClose = value; }
        }

        private bool _DrawerOpen = false;
        public bool DrawerOpen
        {
            get { return _DrawerOpen; }
            set
            {
                if (_DrawerOpen == value) return;
                _DrawerOpen = value;
                if (DrawerOpen)//Open
                {
                    ControlCollapsed = false;

                    if (DrawerChanged != null) DrawerChanged(this, new DrawerEventArgs(OpenSize, _DrawerOpen));
                    this.splitContainer1.SplitterDistance = BaseSplitDistance;
                    
                }
                else//Close
                {
                    //OpenSize = this.Size;
                    if (DrawerChanged != null) DrawerChanged(this, new DrawerEventArgs(BaseButtonSize, _DrawerOpen));
                    ControlCollapsed = true;
                }
                ResetButtonText();
            }
        }
        public Size OpenSize
        {
            get { return SaveData.OpenSize; }
            set { _SaveData.OpenSize = value; }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (DrawerOpen)
            {
                Close();
            }
            else
            {
                Open();

            }
        }

        public void Open()
        {
            DrawerOpen = true;
        }
        public void Close()
        {
            DrawerOpen = false;            
        }



        private void DrawerContainer_SizeChanged(object sender, EventArgs e)
        {
            //button1.Dock = DockStyle.Fill;
            //splitContainer1.IsSplitterFixed = true;

            splitContainer1.Size = this.Size;
            if (DrawerOpen)
            {
                OpenSize = this.Size;
                button1.Size = BaseButtonSize;//DrawerButtonPanel.Size;
            }
            else
            {
                button1.Size = splitContainer1.Size;
            }
            //if(Ctrl!=null)Ctrl.Dock = DockStyle.Fill;
            //this.Invalidate();
        }

        private void DrawerContainer_MouseLeave(object sender, EventArgs e)
        {
            if (!CloseTimer.Enabled)
            {
                CloseTimer.Start();
            }
        }

        private void OpenMenu_Click(object sender, EventArgs e)
        {

        }

        private void DrawerContainer_MouseEnter(object sender, EventArgs e)
        {
            timer1.Start();

            
        }

        private void AutoOpenMenu_Click(object sender, EventArgs e)
        {
            AutoOpenMenu.Checked = !AutoOpenMenu.Checked;
            AutoOpen = AutoOpenMenu.Checked;
        }

        private void AutoCloseMenu_Click(object sender, EventArgs e)
        {
            AutoCloseMenu.Checked = !AutoCloseMenu.Checked;
            AutoClose = AutoCloseMenu.Checked;
        }

        private void OpenMenu_DropDownOpening(object sender, EventArgs e)
        {
            NormalOpenMenu.Checked = IsDrawer;
            AutoOpenMenu.Checked = AutoOpen;
            AutoCloseMenu.Checked = AutoClose;
        }


        private void button1_MouseLeave(object sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }

        private void button1_MouseHover(object sender, EventArgs e)
        {
            this.OnMouseEnter(e);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            var p = this.PointToClient(Cursor.Position);
            if (this.ClientRectangle.Contains(p) && AutoOpen)
            {
                Open();
            }
        }

        private void CloseTimer_Tick(object sender, EventArgs e)
        {
            CloseTimer.Stop();
            var p = this.PointToClient(Cursor.Position);
            if (!this.ClientRectangle.Contains(p) && AutoClose)
            {
                Close();
            }
        }

        private void NormalOpenMenu_Click(object sender, EventArgs e)
        {
            NormalOpenMenu.Checked = !NormalOpenMenu.Checked;
            IsDrawer = NormalOpenMenu.Checked;
        }

        private void button1_DockChanged(object sender, EventArgs e)
        {
        }

        private void splitContainer1_SplitterMoving(object sender, SplitterCancelEventArgs e)
        {
        }


        private void ResetOrientation()
        {

            
            switch (DrawerDirection)
            {
                case DrawerDirection.Up:
                case DrawerDirection.Down:
                    splitContainer1.Orientation = Orientation.Horizontal;
                    break;
                case DrawerDirection.Left:
                case DrawerDirection.Right:
                    splitContainer1.Orientation = Orientation.Vertical;
                    break;
                default:
                    throw new Exception();
            }
        }
        private void ResetFix()
        {
            b = true;
            switch (DrawerDirection)
            {
                case DrawerDirection.Up:
                case DrawerDirection.Left:
                    splitContainer1.FixedPanel = FixedPanel.Panel1;
                    if (DrawerOpen)
                    {
                        splitContainer1.SplitterDistance = BaseSplitDistance;
                    }
                    break;
                case DrawerDirection.Down:
                case DrawerDirection.Right:

                    splitContainer1.FixedPanel = FixedPanel.Panel2;
                    if (DrawerOpen)
                    {
                        splitContainer1.SplitterDistance = BaseSplitDistance;
                    }
                    break;
                default:
                    throw new Exception();
            }
            b = false;
        }
        bool b = false;
        private void splitContainer1_SplitterMoved(object sender, SplitterEventArgs e)
        {
            if (b) return;
            if (!DrawerOpen) return;
            //ResetFix();
            
        }

        private void dockToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Dock = DockStyle.Fill;
                button1.Size = DrawerButtonPanel.Size;

                button1.Dock = DockStyle.Fill;

                //splitContainer1.Panel1Collapsed = true;
                splitContainer1.Size = this.Size;
            }
            catch
            {

            }
        }

        private void DrawerContainer_Load(object sender, EventArgs e)
        {
            //if (DrawerChanged != null) DrawerChanged(this, new DrawerEventArgs(BaseButtonSize, _DrawerOpen));
        }


    }
    [Serializable()]
    public struct DrawerContainerSaveData
    {
        public bool IsDrawer;
        public bool AutoOpen;
        public bool AutoClose;
        public Size OpenSize;

        
    }

    public delegate void DrawerEventHandler(object sender, DrawerEventArgs e);

    public enum DrawerDirection
    {
        Up,Down,Left,Right
    }
    public class DrawerEventArgs : EventArgs
    {
        public Size ChangeSize;
        public bool Open;
        public DrawerEventArgs(Size size,bool open)
        {
            this.ChangeSize = size;
            this.Open = open;
        }
    }
}
