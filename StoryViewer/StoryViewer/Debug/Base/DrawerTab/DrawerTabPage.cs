﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace DrawerTab
{
    class DrawerTabPage:TabPage
    {
        public Control Ctrl { get; set; }
        public DrawerTabPage(Control ctrl,string Name)
        {
            this.Ctrl = ctrl;
            this.Controls.Add(ctrl);
            ctrl.Dock = DockStyle.Fill;
            this.Text = Name;
        }
    }
}
