﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Book;
namespace AAEditTabControl.Base
{
    [ToolboxItem(false)]
    public partial class EditingFileTab : UserControl
    {
        public event EditingFileEventHandler TabSelected;

        public EditingFileTab()
        {
            InitializeComponent();
        }

        public EditingFile SelectedEditingFile
        {
            get
            {
                var sp = this.tabControl1.SelectedTab;
                if (sp != null)
                {
                    var p = (EditingFile)sp.Tag;
                    return p;
                }
                else
                {
                    return null;
                }
            }
        }
        public void Add(EditingFile ef)
        {
            if (!this.Contains(ef))
            {
                var p = new TabPage();
                p.Tag = ef;
                this.tabControl1.TabPages.Add(p);
            }
            Select(ef);
            
        }

        public TabPage Select(EditingFile ef)
        {
            foreach (TabPage tp in this.tabControl1.TabPages)
            {
                var p = (EditingFile)tp.Tag;
                if (p == ef)
                {
                    this.tabControl1.SelectedTab = tp;
                    return tp;
                }
            }
            return null;
        }

        public bool Contains(EditingFile ef)
        {
            foreach (TabPage tp in this.tabControl1.TabPages)
            {
                var p = (EditingFile)tp.Tag;
                if (p == ef) return true;
            }
            return false;
        }
        
        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var p = this.SelectedEditingFile;
            if (p != null)
            {
                if (TabSelected != null) TabSelected(sender, new EditingFileEventArgs(p));
            }
        }

        private void closeTabPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.tabControl1.TabPages.Remove(this.tabControl1.SelectedTab);
            if (this.tabControl1.SelectedTab == null)
            {
                if (this.TabSelected != null) this.TabSelected(this, new EditingFileEventArgs(null));
            }
        }

        private void closeAllTabToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.tabControl1.TabPages.Clear();
            if (this.tabControl1.SelectedTab == null)
            {
                if (this.TabSelected != null) this.TabSelected(this, new EditingFileEventArgs(null));
            }
        }
        
    }
}
