﻿namespace AAEditTabControl
{
    partial class TripleSpliter
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TripleSpliter));
            this.LeftUpperSplitter = new System.Windows.Forms.Splitter();
            this.RightBottomSplitter = new System.Windows.Forms.Splitter();
            this.RightBottomPanel = new System.Windows.Forms.Panel();
            this.RightBottomDrawer = new AAEditTabControl.DrawerContainer();
            this.CenterPanel = new System.Windows.Forms.Panel();
            this.LeftUpperPanel = new System.Windows.Forms.Panel();
            this.LeftUpperDrawer = new AAEditTabControl.DrawerContainer();
            this.RightBottomPanel.SuspendLayout();
            this.LeftUpperPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // LeftUpperSplitter
            // 
            this.LeftUpperSplitter.Location = new System.Drawing.Point(281, 0);
            this.LeftUpperSplitter.Name = "LeftUpperSplitter";
            this.LeftUpperSplitter.Size = new System.Drawing.Size(3, 510);
            this.LeftUpperSplitter.TabIndex = 12;
            this.LeftUpperSplitter.TabStop = false;
            this.LeftUpperSplitter.SplitterMoving += new System.Windows.Forms.SplitterEventHandler(this.LeftSplitter_SplitterMoving);
            this.LeftUpperSplitter.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.LeftSplitter_SplitterMoved);
            // 
            // RightBottomSplitter
            // 
            this.RightBottomSplitter.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightBottomSplitter.Location = new System.Drawing.Point(571, 0);
            this.RightBottomSplitter.Name = "RightBottomSplitter";
            this.RightBottomSplitter.Size = new System.Drawing.Size(1, 510);
            this.RightBottomSplitter.TabIndex = 13;
            this.RightBottomSplitter.TabStop = false;
            this.RightBottomSplitter.SplitterMoving += new System.Windows.Forms.SplitterEventHandler(this.RightSplitter_SplitterMoving);
            this.RightBottomSplitter.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.RightSplitter_SplitterMoved);
            // 
            // RightBottomPanel
            // 
            this.RightBottomPanel.BackColor = System.Drawing.SystemColors.Control;
            this.RightBottomPanel.Controls.Add(this.RightBottomDrawer);
            this.RightBottomPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightBottomPanel.Location = new System.Drawing.Point(572, 0);
            this.RightBottomPanel.Name = "RightBottomPanel";
            this.RightBottomPanel.Size = new System.Drawing.Size(272, 510);
            this.RightBottomPanel.TabIndex = 14;
            this.RightBottomPanel.SizeChanged += new System.EventHandler(this.RightBottomPanel_SizeChanged);
            this.RightBottomPanel.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.RightPanel_ControlAdded);
            // 
            // RightBottomDrawer
            // 
            this.RightBottomDrawer.AutoClose = false;
            this.RightBottomDrawer.AutoOpen = false;
            this.RightBottomDrawer.Ctrl = null;
            this.RightBottomDrawer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.RightBottomDrawer.DrawerDirection = AAEditTabControl.DrawerDirection.Left;
            this.RightBottomDrawer.DrawerOpen = false;
            this.RightBottomDrawer.IsDrawer = true;
            this.RightBottomDrawer.Location = new System.Drawing.Point(0, 0);
            this.RightBottomDrawer.Name = "RightBottomDrawer";
            this.RightBottomDrawer.Size = new System.Drawing.Size(272, 510);
            this.RightBottomDrawer.TabIndex = 0;
            this.RightBottomDrawer.DrawerChanged += new AAEditTabControl.DrawerEventHandler(this.RightBottomDrawer_DrawerChanged);
            this.RightBottomDrawer.Load += new System.EventHandler(this.RightBottomDrawer_Load);
            this.RightBottomDrawer.SizeChanged += new System.EventHandler(this.RightBottomDrawer_SizeChanged);
            this.RightBottomDrawer.ParentChanged += new System.EventHandler(this.RightBottomDrawer_ParentChanged);
            // 
            // CenterPanel
            // 
            this.CenterPanel.BackColor = System.Drawing.SystemColors.Control;
            this.CenterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CenterPanel.Location = new System.Drawing.Point(284, 0);
            this.CenterPanel.Name = "CenterPanel";
            this.CenterPanel.Size = new System.Drawing.Size(287, 510);
            this.CenterPanel.TabIndex = 15;
            this.CenterPanel.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.CenterPanel_ControlAdded);
            // 
            // LeftUpperPanel
            // 
            this.LeftUpperPanel.BackColor = System.Drawing.SystemColors.Control;
            this.LeftUpperPanel.Controls.Add(this.LeftUpperDrawer);
            this.LeftUpperPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftUpperPanel.Location = new System.Drawing.Point(0, 0);
            this.LeftUpperPanel.Name = "LeftUpperPanel";
            this.LeftUpperPanel.Size = new System.Drawing.Size(281, 510);
            this.LeftUpperPanel.TabIndex = 9;
            this.LeftUpperPanel.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.LeftPanel_ControlAdded);
            // 
            // LeftUpperDrawer
            // 
            this.LeftUpperDrawer.AutoClose = false;
            this.LeftUpperDrawer.AutoOpen = false;
            this.LeftUpperDrawer.Ctrl = null;
            this.LeftUpperDrawer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LeftUpperDrawer.DrawerDirection = AAEditTabControl.DrawerDirection.Right;
            this.LeftUpperDrawer.DrawerOpen = false;
            this.LeftUpperDrawer.IsDrawer = true;
            this.LeftUpperDrawer.Location = new System.Drawing.Point(0, 0);
            this.LeftUpperDrawer.Name = "LeftUpperDrawer";
            this.LeftUpperDrawer.Size = new System.Drawing.Size(281, 510);
            this.LeftUpperDrawer.TabIndex = 0;
            this.LeftUpperDrawer.DrawerChanged += new AAEditTabControl.DrawerEventHandler(this.LeftUpperDrawer_DrawerChanged);
            // 
            // TripleSpliter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CenterPanel);
            this.Controls.Add(this.RightBottomSplitter);
            this.Controls.Add(this.LeftUpperSplitter);
            this.Controls.Add(this.LeftUpperPanel);
            this.Controls.Add(this.RightBottomPanel);
            this.Name = "TripleSpliter";
            this.Size = new System.Drawing.Size(844, 510);
            this.SizeChanged += new System.EventHandler(this.TripleHorizontalSpliter_SizeChanged);
            this.RightBottomPanel.ResumeLayout(false);
            this.LeftUpperPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Splitter LeftUpperSplitter;
        private System.Windows.Forms.Splitter RightBottomSplitter;
        private System.Windows.Forms.Panel RightBottomPanel;
        private System.Windows.Forms.Panel CenterPanel;
        private DrawerContainer RightBottomDrawer;
        private System.Windows.Forms.Panel LeftUpperPanel;
        private DrawerContainer LeftUpperDrawer;
    }
}
