﻿namespace AAEditTabControl
{
    partial class DrawerContainer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button1 = new System.Windows.Forms.Button();
            this.ButtonContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.OpenMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.NormalOpenMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.AutoOpenMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.AutoCloseMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.CloseTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ButtonContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            this.splitContainer1.Panel1MinSize = 1;
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.Control;
            this.splitContainer1.Panel2MinSize = 1;
            this.splitContainer1.Size = new System.Drawing.Size(150, 150);
            this.splitContainer1.SplitterDistance = 25;
            this.splitContainer1.TabIndex = 0;
            this.splitContainer1.SplitterMoving += new System.Windows.Forms.SplitterCancelEventHandler(this.splitContainer1_SplitterMoving);
            this.splitContainer1.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitContainer1_SplitterMoved);
            // 
            // button1
            // 
            this.button1.ContextMenuStrip = this.ButtonContext;
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Font = new System.Drawing.Font("MS UI Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.button1.Location = new System.Drawing.Point(0, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(25, 150);
            this.button1.TabIndex = 0;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.DockChanged += new System.EventHandler(this.button1_DockChanged);
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            this.button1.MouseHover += new System.EventHandler(this.button1_MouseHover);
            // 
            // ButtonContext
            // 
            this.ButtonContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenMenu});
            this.ButtonContext.Name = "ButtonContext";
            this.ButtonContext.Size = new System.Drawing.Size(153, 48);
            // 
            // OpenMenu
            // 
            this.OpenMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NormalOpenMenu,
            this.AutoOpenMenu,
            this.AutoCloseMenu});
            this.OpenMenu.Name = "OpenMenu";
            this.OpenMenu.Size = new System.Drawing.Size(152, 22);
            this.OpenMenu.Text = "モード";
            this.OpenMenu.DropDownOpening += new System.EventHandler(this.OpenMenu_DropDownOpening);
            this.OpenMenu.Click += new System.EventHandler(this.OpenMenu_Click);
            // 
            // NormalOpenMenu
            // 
            this.NormalOpenMenu.Name = "NormalOpenMenu";
            this.NormalOpenMenu.Size = new System.Drawing.Size(152, 22);
            this.NormalOpenMenu.Text = "ノーマル";
            this.NormalOpenMenu.Click += new System.EventHandler(this.NormalOpenMenu_Click);
            // 
            // AutoOpenMenu
            // 
            this.AutoOpenMenu.Name = "AutoOpenMenu";
            this.AutoOpenMenu.Size = new System.Drawing.Size(152, 22);
            this.AutoOpenMenu.Text = "自動オープン";
            this.AutoOpenMenu.Click += new System.EventHandler(this.AutoOpenMenu_Click);
            // 
            // AutoCloseMenu
            // 
            this.AutoCloseMenu.Name = "AutoCloseMenu";
            this.AutoCloseMenu.Size = new System.Drawing.Size(152, 22);
            this.AutoCloseMenu.Text = "自動クローズ";
            this.AutoCloseMenu.Click += new System.EventHandler(this.AutoCloseMenu_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 300;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // CloseTimer
            // 
            this.CloseTimer.Interval = 1000;
            this.CloseTimer.Tick += new System.EventHandler(this.CloseTimer_Tick);
            // 
            // DrawerContainer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "DrawerContainer";
            this.Load += new System.EventHandler(this.DrawerContainer_Load);
            this.SizeChanged += new System.EventHandler(this.DrawerContainer_SizeChanged);
            this.MouseEnter += new System.EventHandler(this.DrawerContainer_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.DrawerContainer_MouseLeave);
            this.splitContainer1.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ButtonContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolStripMenuItem OpenMenu;
        private System.Windows.Forms.ToolStripMenuItem NormalOpenMenu;
        private System.Windows.Forms.ToolStripMenuItem AutoOpenMenu;
        private System.Windows.Forms.ToolStripMenuItem AutoCloseMenu;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer CloseTimer;
        public System.Windows.Forms.ContextMenuStrip ButtonContext;
    }
}
