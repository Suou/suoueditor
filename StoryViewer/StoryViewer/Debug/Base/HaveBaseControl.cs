﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;

namespace AAEditTabControl
{
    [ToolboxItem(false)]
    public partial class HaveBaseControl : UserControl
    {
        public HaveBaseControl()
        {
            InitializeComponent();
        }

        public void AddTreeNode(TreeNode tn)
        {
        }

        public void SaveFileXML()
        {
        }

        public void ReadFileXML()
        {
        }

        #region event
        public event AstEventHandler SelectFileChanged;
        // 内部で発生したイベントを外部に知らせる
        protected void OnTextChange(AstEventArgs e)
        {
            EventArgs es = new EventArgs();
            

            // 外部のイベントハンドラが接続されているなら
            if (SelectFileChanged != null)
            {
                SelectFileChanged(this, e);
            }
        }

        #endregion
    }
}
