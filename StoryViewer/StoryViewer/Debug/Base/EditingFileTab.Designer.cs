﻿namespace AAEditTabControl.Base
{
    partial class EditingFileTab
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.closeTabPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeAllTabToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.ContextMenuStrip = this.contextMenuStrip1;
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(292, 28);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeTabPageToolStripMenuItem,
            this.closeAllTabToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 70);
            // 
            // closeTabPageToolStripMenuItem
            // 
            this.closeTabPageToolStripMenuItem.Name = "closeTabPageToolStripMenuItem";
            this.closeTabPageToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.closeTabPageToolStripMenuItem.Text = "タブを閉じる";
            this.closeTabPageToolStripMenuItem.Click += new System.EventHandler(this.closeTabPageToolStripMenuItem_Click);
            // 
            // closeAllTabToolStripMenuItem
            // 
            this.closeAllTabToolStripMenuItem.Name = "closeAllTabToolStripMenuItem";
            this.closeAllTabToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.closeAllTabToolStripMenuItem.Text = "全てのタブを閉じる";
            this.closeAllTabToolStripMenuItem.Click += new System.EventHandler(this.closeAllTabToolStripMenuItem_Click);
            // 
            // EditingFileTab
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "EditingFileTab";
            this.Size = new System.Drawing.Size(292, 28);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem closeTabPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeAllTabToolStripMenuItem;

    }
}
