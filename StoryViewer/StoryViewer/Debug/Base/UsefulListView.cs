﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace AAEditTabControl.Base
{


     [ToolboxItem(false)]
        public class UsefulListView : ListView
        {
            public event IndexChangedEventHandler FocusedIndexChanged
            {
                add
                {
                    this.Events.AddHandler(EventFocusedIndexChanged, value);
                }
                remove
                {
                    this.Events.RemoveHandler(EventFocusedIndexChanged, value);
                }
            }
            private static readonly object EventFocusedIndexChanged = new object();
            protected virtual void OnFocusedIndexChanged(IndexChangedEventArgs e)
            {
                IndexChangedEventHandler handler = this.Events[EventFocusedIndexChanged] as IndexChangedEventHandler;

                if (handler != null)
                    handler(this, e);
            }
            protected override void WndProc(ref Message m)
            {
                // LVN_ITEMCHANGED は親フォームに WM_NOTIFY(0x4E)で送信され、
                //    親フォームはそれを該当コントロールに
                //    WM_REFLECT(WM_USER(0x400)+0x1C00) を追加したメッセージコードで通知する。
                const int ReflectedItemChanged = 0x400 + 0x1c00 + 0x4e;
                // LVN_ITEMCHANGED
                const int ItemChanged = -101;
                // NMHDR / NMLISTVIEW 構造体におけるメンバのオフセット（先頭の hdr 除く）
                const int OffsetCode = 4, OffsetIndex = 8, OffsetState = 16;
                if (m.Msg == ReflectedItemChanged)
                {
                    // LPARAM を NMHDR に見立てて通知コード(code)を取得
                    int code = Marshal.ReadInt32(m.LParam, IntPtr.Size + OffsetCode);
                    if (code == ItemChanged)
                    {
                        // LPARAM を NMLISTVIEW に見立ててアイテムの状態(uNewState)を取得
                        int state = Marshal.ReadInt32(m.LParam, IntPtr.Size + OffsetState);
                        const int Focused = 1;
                        // 最下位ビットが立っている場合フォーカスが存在すると言う意味になる
                        // フォーカスがアイテム 1 からアイテム 2 に動いた場合、
                        //    ItemChanged は複数回送られる。
                        //    一般的に、アイテム 1 の選択解除、アイテム 1 のフォーカス解除、
                        //    アイテム 2 のフォーカスと選択状態の獲得、の順である。
                        //    アイテム 1 の状態については uOldState の方に現れるので、
                        //    state (uNewState) に出ることはない
                        if ((state & Focused) != 0)
                        {
                            // LPARAM を NMLISTVIEW に見立ててインデックス(iItem)を取得
                            int index = Marshal.ReadInt32(m.LParam, IntPtr.Size + OffsetIndex);
                            this.OnFocusedIndexChanged(new IndexChangedEventArgs(index));
                        }
                    }
                }
                base.WndProc(ref m);
            }
        }
        public delegate void IndexChangedEventHandler(object sender, IndexChangedEventArgs e);
        public class IndexChangedEventArgs : EventArgs
        {
            public IndexChangedEventArgs(int index) { this.index = index; }
            private int index;
            public int Index { get { return index; } }
        }

}
