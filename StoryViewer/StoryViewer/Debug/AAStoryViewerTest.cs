﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Book;
namespace StoryViewer.Debug
{
    public partial class AAStoryViewerTest : Form
    {
        public AAStoryViewerTest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var v = this.openFileDialog1.ShowDialog();
            if (v == System.Windows.Forms.DialogResult.OK)
            {
                var f = IBookHandler.MergeBookLoad(this.openFileDialog1.FileName);
                this.storyViewerBox21.EditItem = f;
            }
        }
    }
}
