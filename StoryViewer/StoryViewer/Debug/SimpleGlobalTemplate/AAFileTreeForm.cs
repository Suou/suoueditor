﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using AAEditTabControl;
using SuouBase;
using SuouBase.Interface;

namespace AAEditTabControl.SimpleGlobalTemplate
{
    public partial class AAFileTreeForm : Form
    {
        //public ControlTabForm pointer;
        private GlobalTreeControl _Ctrl;
        public GlobalTreeControl Ctrl 
        {
            get
            {
                return _Ctrl;
            }
            set 
            {
                _Ctrl = value;
                this.panel1.Controls.Add(_Ctrl);
                _Ctrl.Dock = DockStyle.Fill;
                //_Ctrl.Dock = DockStyle.Fill;
            }
        }

        public AAFileTreeForm()
        {
            InitializeComponent();
        }

        private void AAFileTreeForm_Load(object sender, EventArgs e)
        {
            //this.ShowInTaskbar = false;
        }

        private void AAFileTreeForm_KeyDown(object sender, KeyEventArgs e)
        {
            //pointer.OperationObject.FocusMode(e.KeyCode);
        }

        private void AAFileTreeForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }

        private void renewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //_Ctrl.Renew();
        }



        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ctrl.OpenMLTFolder();
        }

    }
}
