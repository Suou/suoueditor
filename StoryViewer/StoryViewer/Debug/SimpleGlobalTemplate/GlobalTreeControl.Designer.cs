﻿namespace AAEditTabControl.SimpleGlobalTemplate
{
    partial class GlobalTreeControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.globalContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gotoLocalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteNodeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.button1 = new System.Windows.Forms.Button();
            this.haveTabTreeControl1 = new AAEditTabControl.HaveTabTreeControl();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.globalContextMenuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // globalContextMenuStrip
            // 
            this.globalContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.gotoLocalToolStripMenuItem,
            this.deleteNodeToolStripMenuItem});
            this.globalContextMenuStrip.Name = "globalContextMenuStrip";
            this.globalContextMenuStrip.Size = new System.Drawing.Size(269, 70);
            this.globalContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.globalContextMenuStrip_Opening);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.openToolStripMenuItem.Text = "開く";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // gotoLocalToolStripMenuItem
            // 
            this.gotoLocalToolStripMenuItem.Name = "gotoLocalToolStripMenuItem";
            this.gotoLocalToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.gotoLocalToolStripMenuItem.Text = "ローカルテンプレートにコピーする";
            this.gotoLocalToolStripMenuItem.Click += new System.EventHandler(this.gotoLocalToolStripMenuItem_Click);
            // 
            // deleteNodeToolStripMenuItem
            // 
            this.deleteNodeToolStripMenuItem.Name = "deleteNodeToolStripMenuItem";
            this.deleteNodeToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.deleteNodeToolStripMenuItem.Text = "ノードを削除する";
            this.deleteNodeToolStripMenuItem.Click += new System.EventHandler(this.DeleteNodeMenuItem1_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.button1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.haveTabTreeControl1);
            this.splitContainer1.Panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_MouseDown);
            this.splitContainer1.Panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_MouseMove);
            this.splitContainer1.Panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_MouseUp);
            this.splitContainer1.Size = new System.Drawing.Size(200, 200);
            this.splitContainer1.SplitterDistance = 31;
            this.splitContainer1.TabIndex = 2;
            this.splitContainer1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_MouseDown);
            this.splitContainer1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_MouseMove);
            this.splitContainer1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.splitContainer1_MouseUp);
            // 
            // button1
            // 
            this.button1.AutoSize = true;
            this.button1.Location = new System.Drawing.Point(0, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(104, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "MLTフォルダ追加";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // haveTabTreeControl1
            // 
            this.haveTabTreeControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.haveTabTreeControl1.Location = new System.Drawing.Point(0, 0);
            this.haveTabTreeControl1.Name = "haveTabTreeControl1";
            this.haveTabTreeControl1.Size = new System.Drawing.Size(200, 165);
            this.haveTabTreeControl1.TabIndex = 0;
            this.haveTabTreeControl1.TreeBoxContext = this.globalContextMenuStrip;
            this.haveTabTreeControl1.NodeDoubleClicked += new System.Windows.Forms.TreeViewEventHandler(this.haveTabTreeControl1_NodeDoubleClicked);
            this.haveTabTreeControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.haveTabTreeControl1_MouseDown);
            this.haveTabTreeControl1.MouseLeave += new System.EventHandler(this.haveTabTreeControl1_MouseLeave);
            this.haveTabTreeControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.haveTabTreeControl1_MouseMove);
            this.haveTabTreeControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.haveTabTreeControl1_MouseUp);
            // 
            // toolTip1
            // 
            this.toolTip1.ToolTipTitle = "テストタイトル";
            // 
            // GlobalTreeControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(200, 200);
            this.Name = "GlobalTreeControl";
            this.Size = new System.Drawing.Size(200, 200);
            this.globalContextMenuStrip.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public HaveTabTreeControl haveTabTreeControl1;
        public System.Windows.Forms.ContextMenuStrip globalContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gotoLocalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteNodeToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
