﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase.Interface;
using SuouBase;
using AAEditTabControl;
using System.Diagnostics;
namespace AAEditTabControl.SimpleGlobalTemplate
{
    [IComponentPlugin("MLTツリー", PluginType.Form | PluginType.Control)]
    public class SimpleGlobalTemplate : SuouBase.Interface.IGlobalTemplateClass,NotifyControl
    {
        private SimpleGlobalTemplateSerialize SaveSerialize;
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public EditorInfo DLLFunction { get; set; }
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        private string SaveFileName = SuouEditorConst.GetSettingFile("SimpleGlobalTemplate.stn");
        public Form Form
        {
            get { return _Form; }
        }
        private FolderDataCollection GlobalTemplateData = new FolderDataCollection();

        public SettingPage[] SettingPage { get { return null; } }
        public bool DisposeCheck()
        {
            SaveSerialize.Save(SaveFileName);
            return true;
        }
        public void AddNode(TreeNode tn)
        {
            _control.haveTabTreeControl1.AddTreeNode(tn);
        }

        public FolderDataCollection Item
        {
            get
            {
                return this._control.Data;
            }
            set
            {
                this._control.Data = value;
            }
        }

        //public event TreeViewEventHandler NodeItemViewed;
        //public event TreeViewEventHandler NodeItemLocalMoved;

        private AAFileTreeForm _Form;
        private GlobalTreeControl _control;
        public ContainerControl Control
        {
            get { return _control; }
            private set
            {
                this._control = (GlobalTreeControl)value;
                _control.NodeItemViewed += this.NodeItemViewedFun;
                _control.NodeItemRegisterMoved += this.NodeItemRegisterMovedFun;

                //this._control.NodeSelected += TreeControl_NodeSelected;
            }
        }

        
        public SimpleGlobalTemplate()
        {
            this._Form = new AAFileTreeForm();
            this.Control = new GlobalTreeControl();
            this._Form.Ctrl = _control;
            //this.NodeItemViewed += this.NodeItemViewedFun;
            //this.NodeItemLocalMoved += this.NodeItemRegisterMovedFun;

            SaveSerialize = new SimpleGlobalTemplateSerialize(_control, _Form, GlobalTemplateData);
            SaveSerialize.Load(SaveFileName,true);
            this.Item = this.GlobalTemplateData;
        }
        public SimpleGlobalTemplate(EditorInfo DLLFunction):this()
        {
            this.DLLFunction = DLLFunction;
        }
        public SimpleGlobalTemplate(EditorInfo DLLFunction,string filename)
        {
            this._Form = new AAFileTreeForm();
            this.Control = new GlobalTreeControl();
            this._Form.Ctrl = _control;
            //this.NodeItemViewed += this.NodeItemViewedFun;
            //this.NodeItemLocalMoved += this.NodeItemRegisterMovedFun;
            SaveFileName = filename;
            SaveSerialize = new SimpleGlobalTemplateSerialize(_control, _Form, GlobalTemplateData);
            SaveSerialize.Load(SaveFileName,false);
            this.Item = this.GlobalTemplateData;
            this.DLLFunction = DLLFunction;
        }
        private void NodeItemRegisterMovedFun(object sender, TreeViewEventArgs e)
        {
            if (DLLFunction != null)
            {
                DLLFunction.RegisterTemplatePlugin.AddNode(e.Node);
                DLLFunction.RegisterTemplatePlugin.Control.Focus();
                
            }
        }

        private void NodeItemViewedFun(object sender, TreeViewEventArgs e)
        {
            if (DLLFunction != null)
            {
                var tn = e.Node;
                if (tn != null)
                {
                    if (tn.Tag is FileNodeClass)
                    {
                        var tnt = tn.Tag as FileNodeClass;
                        /*
                        if (!tnt.IsFile)
                        {
                            
                            MessageBox.Show(tnt.Name + "が見つかりません");
                        }
                        else
                        {
                            DLLFunction.TemplatePageViewPlugin.Item = tnt.File;
                        }
                        */
                    }
                }
            }
        }
        public event EventHandler FocusNotifiedEventHandler;
        public void FocusNotify()
        {
            if (FocusNotifiedEventHandler != null) FocusNotifiedEventHandler(this, new EventArgs());
        }
    }
    [Serializable()]
    public class SimpleGlobalTemplateSerialize
    {

        public List<TempStruct> GlobalTemplate { get; set; }
        public Point Position;
        public Size Size;
        public Size MinimumSize;
        public FormWindowState FormState;
        [NonSerialized()]
        private GlobalTreeControl ctrl;
        [NonSerialized()]
        private AAFileTreeForm form;
        [NonSerialized()]
        private FolderDataCollection GlobalData;

        public SimpleGlobalTemplateSerialize()
        {
        }

        public SimpleGlobalTemplateSerialize(GlobalTreeControl ctrl, AAFileTreeForm form, FolderDataCollection data)
        {
            this.ctrl = ctrl;
            this.form = form;
            this.GlobalData = data;
        }

        public void Save(string filename)
        {
            this.GlobalTemplate = this.GlobalData.ToTempStruct();
        
            if (form.WindowState == FormWindowState.Minimized)
            {
                return;
            }
            this.Position = form.Location;
            this.Size = form.Size;
            this.FormState = form.WindowState;
            this.MinimumSize = ctrl.MinimumSize;

            SuouEditorConst.CheckFolder();

            BSystem.XML.BinarySerialize.SaveToBinaryFile(this, filename);
        }

        private void GlobalDataLoad(List<TempStruct> tdc)
        {
            try
            {
                foreach (var ts in tdc)
                {
                    if (ts.FolderPath == null)
                    {
                        throw new MyException("Error");
                    }
                    var di = new DirectoryInfo(ts.FolderPath);

                    if (di.Exists && ts.Title != null && ts.FolderPath != null)
                    {
                        this.GlobalData.Add(new FolderData(ts.Title, ts.FolderPath));
                    }
                }
            }
            catch
            {
                throw new Exception("Error");
            }
        }
        private void InitialLoad()
        {
            var fi = new DirectoryInfo(SuouEditorConst.GetDataFile("MLTFolder"));
            if (fi.Exists)
            {
                var p = new TempStruct() { Title = "MLTFolder", FolderPath = SuouEditorConst.GetDataFile("MLTFolder") };

                var pp = new List<TempStruct>();
                pp.Add(p);
                GlobalDataLoad(pp);
            }
        }
        public bool Load(string filename,bool PreLoad)
        {
            FileInfo v = new FileInfo(filename);
            if (v.Exists)
            {

                SimpleGlobalTemplateSerialize cls = null;
                cls = BSystem.XML.BinarySerialize.LoadFromBinaryFile<SimpleGlobalTemplateSerialize>(filename);
                
                if (cls != null)
                {
                    if (cls.GlobalTemplate != null)
                    {
                        if (cls.GlobalTemplate.Count != 0)
                        {
                            GlobalDataLoad(cls.GlobalTemplate);
                        }
                        else
                        {
                            if(PreLoad)InitialLoad();
                        }
                    }
                    else
                    {
                        if (PreLoad) InitialLoad();
                    }
                    form.Location = cls.Position;
                    form.Size = cls.Size;
                    form.WindowState = cls.FormState;
                    if(cls.MinimumSize.Width > 100) ctrl.MinimumSize = cls.MinimumSize;
                }

            }
            else
            {
                if (PreLoad) InitialLoad();
            }

            return true;
        }
    }

}
