﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
namespace AAEditTabControl.SimpleGlobalTemplate
{
    public partial class GlobalTreeControl : UserControl
    {
        public GlobalTreeControl()
        {
            InitializeComponent();
            this.haveTabTreeControl1.canUseNewTabmenu(false);
        }
        public void SetData(string folderpath)
        {
            var v = new FolderDataCollection(folderpath);
            this.Data = v;
        }
        public FolderDataCollection Data
        {
            set
            {
                this.haveTabTreeControl1.Data = value;
            }
            get
            {
                return this.haveTabTreeControl1.Data;
            }
        }
        public ContextMenuStrip[] ContextItems
        {
            get
            {
                return new ContextMenuStrip[] { this.globalContextMenuStrip, this.haveTabTreeControl1.ContextItem };
            }
        }
        public event FilePathEventHandler FileDoubleClicked;
        public event TreeViewEventHandler NodeItemViewed;
        //public event TreeViewEventHandler NodeItemLocalMoved;
        public event TreeViewEventHandler NodeItemRegisterMoved;

        private void globalContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (this.haveTabTreeControl1.SelectedNode == null)
            {
                deleteNodeToolStripMenuItem.Enabled = false;
                openToolStripMenuItem.Enabled = false;
                gotoLocalToolStripMenuItem.Enabled = false;
            }
            else
            {
                deleteNodeToolStripMenuItem.Enabled = true;
                openToolStripMenuItem.Enabled = true;
                gotoLocalToolStripMenuItem.Enabled = true;
            }
        }
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.haveTabTreeControl1.SelectedNode == null) return;
            if (NodeItemViewed != null) NodeItemViewed(this, new TreeViewEventArgs(this.haveTabTreeControl1.SelectedNode));
        }



        private void gotoLocalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var e2 = new TreeViewEventArgs(this.haveTabTreeControl1.SelectedNode);
            if (NodeItemRegisterMoved != null) NodeItemRegisterMoved(sender, e2);
        }

        private void DeleteNodeMenuItem1_Click(object sender, EventArgs e)
        {
            this.haveTabTreeControl1.SelectTabTreeSubControl.DeleteNode();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenMLTFolder();
        }
        public void OpenMLTFolder()
        {
            var f = new FolderBrowserDialog();

            if (f.ShowDialog() == DialogResult.OK)
            {
                if (haveTabTreeControl1 == null)
                {
                    throw new Exception();
                }
                else
                {
                    haveTabTreeControl1.OpenFile(f.SelectedPath);
                    var file = new System.IO.DirectoryInfo(f.SelectedPath);
                    MessageBox.Show("追加しました。" + "[" + file.Name + "]");
                }
            }
            
        }

        private void haveTabTreeControl1_NodeDoubleClicked(object sender, TreeViewEventArgs e)
        {
            if (this.haveTabTreeControl1.SelectedNode == null) return;
            if (NodeItemViewed != null)
            {
                NodeItemViewed(this, new TreeViewEventArgs(this.haveTabTreeControl1.SelectedNode));
                if (this.haveTabTreeControl1.SelectedNode.Tag is FileNodeClass)
                {
                    var obj = (FileNodeClass)this.haveTabTreeControl1.SelectedNode.Tag;
                    if (FileDoubleClicked != null) FileDoubleClicked(this, new FilePathEventArgs(obj.FullName));
                }
            }
        }
        /*
        private string HelpMessage = 
@"このグローバルテンプレートは
MLTファイルの選択を補助します。

グローバルテンプレートにMLTフォルダを追加したいときは
「MLTフォルダを追加」ボタンを押して
MLTフォルダ(HukuTempなど)を選択してください。

よく使うMLTファイルは右クリックして
「ローカルテンプレートにコピー」を選択するといいでしょう。
";
        */

        private void button2_Click(object sender, EventArgs e)
        {
            //Help.ShowPopup(this, HelpMessage, SuouBase.FolderGeneralMethod.GetControlLocation(this.button2));
        }

        private void haveTabTreeControl1_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(MouseEventToClient(e));
        }

        private void haveTabTreeControl1_MouseDown(object sender, MouseEventArgs e)
        {
            this.OnMouseDown(MouseEventToClient(e));
        }

        private void haveTabTreeControl1_MouseUp(object sender, MouseEventArgs e)
        {
            this.OnMouseUp(MouseEventToClient(e));
        }

        private void splitContainer1_MouseDown(object sender, MouseEventArgs e)
        {
            this.OnMouseDown(MouseEventToClient(e));
        }

        private void splitContainer1_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(MouseEventToClient(e));
        }

        private void splitContainer1_MouseUp(object sender, MouseEventArgs e)
        {
            
            this.OnMouseUp(MouseEventToClient(e));
        }

        private void haveTabTreeControl1_MouseLeave(object sender, EventArgs e)
        {
            this.OnMouseLeave(e);
        }
        
        private MouseEventArgs MouseEventToClient(MouseEventArgs e)
        {
            var p = this.PointToClient(Control.MousePosition);
            var e2 = new MouseEventArgs(e.Button, e.Clicks, p.X, p.Y, e.Delta);
            return e2;
        }
        

    }
    public delegate void FilePathEventHandler(object sender,FilePathEventArgs e);
    public class FilePathEventArgs : EventArgs
    {
        public string FilePath { get; private set; }
        public FilePathEventArgs(string s)
        {
            this.FilePath = s;
        }
    }
}
