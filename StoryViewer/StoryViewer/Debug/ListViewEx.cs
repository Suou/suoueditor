﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using Microsoft.VisualBasic;
using SuouBase.Book;
using SuouBase.Interface;

namespace AAEditTabControl
{
    [ToolboxItem(false)]
    public partial class ListViewEx : ListView
    {
        private EditingFile PageFileData;
        public event AstEventHandler ReturnKeyPushed;
        public event AstEventHandler ShowPagePushed;
        private int[] NowSelection = new int[]{};

        public ListViewEx()
        {
            InitializeComponent();
        }


        public EditingFile Data
        {
            get { return PageFileData; }
            set
            {
                PageFileData = value;
                Renew();

            }
        }

        #region Select
        /*
        public int SelectedIndex()
        {
            if (IsSelected())
            {
                return this.SelectedIndices[0];
            }
            else return -1;

        }
         */

        /// <summary>
        /// ItemSelectionChanged用
        /// 現在のフォーカスを返す
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        private int Selecton(int[] a,int[] b)
        {
            

            if (a.Length > b.Length)
            {
                int k = a.First((int n) => (b.All((int m) => (n != m))));
                if (a[0] == k)
                {
                    return k + 1;
                }
                else
                {
                    return k - 1;
                }
            }
            else if (a.Length == b.Length)
            {
                return -1;
            }
            else
            {
                return b.First((int n) => (a.All((int m) => (n != m))));
            }
        }

        public int FocusIndex
        {
            get
            {
                for (int i = 0; i < this.Items.Count; i++)
                {
                    if (this.Items[i] != null)
                    {
                        if (this.Items[i].Focused) return i;
                    }
                }
                return -1;
            }

            set
            {
                if (0 <= value && value < this.Items.Count && this.SelectedItems.Count <= 1)
                {
                    this.PageFileData.SelectedPageIndex = value;
                    this.Items[value].Focused = true;
                    this.Items[value].Selected = true;
                }
                else if (0 <= value && value < this.Items.Count && this.SelectedItems.Count > 1)
                {
                    this.PageFileData.SelectedPageIndex = value;
                    this.Items[value].Focused = true;
                }
                else if (this.Items.Count > 0 && value != -1)
                {
                    this.PageFileData.SelectedPageIndex = 0;
                    this.Items[0].Focused = true;
                    this.Items[0].Selected = true;
                }
            }
        }


        public bool IsSelected()
        {
            return (this.SelectedIndices.Count > 0);
        }

        public int[] SelectionIntIndices()
        {
            var s = this.SelectedIndices;
            int[] n = new int[s.Count];
            for (int i = 0; i < s.Count; i++)
            {
                n[i] = s[i];
            }
            Array.Sort(n);
            return n;
        }

        public void SelectOnlyColumn(int n)
        {
            var s = this.SelectedItems;
            for (int i = 0; i < s.Count; i++)
            {
                s[i].Selected = false;

            }
            if (n < this.Items.Count && n >= 0)
            {

                this.Items[n].Selected = true;
                this.Items[n].Focused = true;               
            }
        }

        public void InitializeListView(string Number,string Name)
        {
            this.Clear();

            var columnName = new ColumnHeader();
            var columnNumber = new ColumnHeader();

            columnNumber.Text = Number;
            columnNumber.Width = 30;
            columnName.Text = Name;
            columnName.Width = this.Width;
            columnName.Width -= columnNumber.Width;
            ColumnHeader[] colHeaderRegValue = { columnNumber, columnName };
            this.Columns.AddRange(colHeaderRegValue);
        }

        public int ItemCount
        {
            get
            {
                return this.Items.Count;
            }
        }
        #endregion

        #region Operation


        public void Quit()
        {
            /*
            if (ShowPage != null)
            {
                ShowPage(null);
            }
             */ 
        }

        public void AddNewPage()
        {
            this.Paste(new AstPage());
            
        }

        private AstPage SelectedPage()
        {
            int i = this.FocusIndex;
            if (i != -1)
            {
                return PageFileData.Pages[i];
            }
            return null;
        }

        private void ReIndex()
        {

            if (this.Data != null)
            {
                if (PageFileData.Pages == null) throw new Exception();
                
                for(int i=0;i<PageFileData.Pages.Count;i++)
                {
                    this.Items[i].SubItems[1].Text = this.PageFileData.Pages[i].Name;
                    this.Items[i].SubItems[0].Text = (i + 1).ToString();
                }
            }
        }

        private void Renew()
        {
            //Clear
            this.InitializeListView("No.", "名前");
            int k = 0;

            this.BeginUpdate();
            if (this.Data != null)
            {
                    //アイテムを入れる
                    if (PageFileData.Pages == null) throw new Exception();
                    foreach (AstPage ap in PageFileData.Pages)
                    {
                        string[] item1 = { (++k).ToString(), ap.Name };
                        var lvi = new ListViewItem(item1);

                        this.Items.Add(lvi);

                    }
            }
            this.EndUpdate();

            ReFocus();

            if (PageFileData != null && PageFileData.SelectedPageIndex >= 0 && PageFileData.SelectedPageIndex < this.Items.Count)
            {
                this.EnsureVisible(PageFileData.SelectedPageIndex);
            }
 
        }
        public void ShowText()
        {
            AstPage ap = SelectedPage();
            AstEventArgs e = new AstEventArgs(ap);
            if (ShowPagePushed != null)
            {
                ShowPagePushed(this, e);
            }
            /*
            if (ShowPage != null && ap != null)
            {
                ShowPage(ap);
            }
            else
            {
                ShowPage(null);
            }
             */

        }

        public void ReFocus()
        {

            if (this.PageFileData != null)
            {
                int i = this.PageFileData.SelectedPageIndex;
                this.ReFocus(i);
            }


        }

        public void ReFocus(int i)
        {
            foreach (ListViewItem lvi in this.Items)
            {
                lvi.Selected = false;
            }

            this.FocusIndex = i;

        }



        public AstPage[] Cut()
        {
            int[] ns = this.SelectionIntIndices();
            var c = Copy(ns);
            this.Delete(ns);
            return c;
        }

        public AstPage[] Copy()
        {
            int[] ns = this.SelectionIntIndices();
            if (ns.Length == 0) return null;

            return this.Data.Copy(ns);
        }

        public AstPage[] Copy(int[] NS)
        {
            if (NS.Length == 0) return null;

            return this.Data.Copy(NS);
        }

        public void Delete()
        {
            int[] ns = this.SelectionIntIndices();
            this.Delete(ns);
        }

        public void Delete(int[] NS)
        {
            if (NS.Length == 0) return;


            List<ListViewItem> lvil = new List<ListViewItem>();
            foreach (int i in NS)
            {
                lvil.Add(this.Items[i]);
            }
            foreach (ListViewItem lvi in lvil)
            {
                this.Items.Remove(lvi);
            }
            this.Data.Delete(NS);
            ReFocus();
        }


        public void Paste(AstPage[] ap)
        {
            int n = this.FocusIndex;
            if (n == -1)
            {
                this.Data.PasteLast(ap);

                for (int i = 0; i < ap.Length; i++)
                {
                    var lvi = new ListViewItem(new string[]{i.ToString(), ap[i].Name});
                    this.Items.Insert(this.Items.Count, lvi);
                }
                this.ReFocus(this.Items.Count - 1);
                this.ReIndex();
            }
            else
            {
                this.Data.Paste(n+1, ap);

                for (int i = 0; i < ap.Length; i++)
                {
                    var lvi = new ListViewItem(new string[] { i.ToString(), ap[i].Name });
                    this.Items.Insert(n + i + 1, lvi);
                }
                this.ReFocus(n + ap.Length);
                this.ReIndex();
            }



        }

        public void Paste(AstPage ap)
        {
            var aps = new AstPage[] { ap };
            this.Paste(aps);
        }

        public void MoveUp()
        {
            Data.MoveUp(Data.SelectedPageIndex);
            ReFocus(this.PageFileData.SelectedPageIndex);
            ReIndex();
        }

        public void MoveDown()
        {
            Data.MoveDown(Data.SelectedPageIndex);
            ReFocus(this.PageFileData.SelectedPageIndex);
            ReIndex();
        }

        #endregion

        #region Event
        protected override void OnSelectedIndexChanged(EventArgs e)
        {

            base.OnSelectedIndexChanged(e);
        }
        protected override void OnItemSelectionChanged(ListViewItemSelectionChangedEventArgs e)
        {

            if (NowSelection.Length == 0)
            {
                NowSelection = this.SelectionIntIndices();
            }

            if (NowSelection.Length == 1 && this.SelectionIntIndices().Length == 1)
            {
                Data.SelectedPageIndex = this.FocusIndex;
                NowSelection = this.SelectionIntIndices();
                ShowText();
            }
            else if (NowSelection.Length > 1 || this.SelectionIntIndices().Length > 1)
            {
                this.FocusIndex = this.Selecton(NowSelection, this.SelectionIntIndices());
                Data.SelectedPageIndex = this.FocusIndex;
                NowSelection = this.SelectionIntIndices();
                ShowText();
            }
            base.OnItemSelectionChanged(e);
        }


        protected override void OnKeyDown(KeyEventArgs e)
        {
            switch (e.KeyData)
            {
                case Keys.Return :
                    AstEventArgs ASTe = new AstEventArgs(this.SelectedPage());
                    if (ReturnKeyPushed != null)
                    {
                        ReturnKeyPushed(this, ASTe);
                    }
                    break;
                case Keys.Delete :
                    this.Delete();
                    break;

            }


            if (e.Control)
            {
                switch (e.KeyCode)
                {
                    case Keys.Up:
                        MoveUp();
                        break;
                    case Keys.Down:
                        MoveDown();
                        break;
                    case Keys.C:
                        Clipboard.SetText(this.SelectedPage().toString());
                        break;
                }

                e.SuppressKeyPress = true;
            }

            if (e.Alt)
            {
                switch (e.KeyCode)
                {
                    case Keys.I:
                        this.AddNewPage();
                        break;
                }
                e.SuppressKeyPress = true;
            }
            
            base.OnKeyDown(e);
        }

        
        private void ListViewEx_DoubleClick(object sender, EventArgs e)
        {
            /*
            var p = this.SelectedPage();
            if (p != null)
            {
                string inputText = Interaction.InputBox("名前", "", "", -1, -1);
                p.Name = inputText;
                ReIndex();
            }
            */
        }
        /*
        public void AutoBackUp(string TimeStamp)
        {
            Data.AutoBackUp(TimeStamp);
        }
        */


        public bool IsFocus()
        {
            foreach (ListViewItem s in this.Items)
            {
                if (s.Focused) return true;
            }
            return false;
        }

        #endregion

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

    }
}
