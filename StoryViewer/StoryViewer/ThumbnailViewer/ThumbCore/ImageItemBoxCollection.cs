﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.ObjectModel;
using SuouBase.Api;
using SuouBase;
using SuouBase.Drawing;
using StoryViewer.ThumbnailViewer.StoryImageItem;
using StoryViewer;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
namespace StoryViewer.ThumbnailViewer
{
    /// <summary>
    /// データリスト
    /// </summary>
    public class ImageItemBoxCollection:ObservableCollection<ImageItemBox>
    {

        #region Property
        private DisplaySetting DisplaySet;
        private ThumbnailViewerOperator ThumContItems;
        private ImageItemBoxType _BoxType = ImageItemBoxType.Normal;
        public ImageItemBoxType BoxType
        {
            get { return _BoxType; }
            set { _BoxType = value; }
        }

        public event EventHandler ItemArrangeChanged;
        public ImageItemBoxCollection(DisplaySetting DS,ThumbnailViewerOperator tcc)
        {
            this.DisplaySet = DS;
            this.DisplaySet.ImageContItem = this;
            this.ThumContItems = tcc;
           
        }

        
        #endregion
        #region Method
        
        public int GetNth(ImageItemBox item)
        {
            if (this.Items.Contains(item))
            {
                return this.IndexOf(item);
            }
            return -1;
        }
        public new void Clear()
        {
            foreach (var v in this.Items)
            {
                this.DisplaySet.Ctrl.Controls.Remove(v);
                v.Dispose();
            }
            this.Items.Clear();
        }

        private ImageItemBox CreateSetItemBox(TextThumbImage item)
        {
            var v = this.CreateItemBox(item);
            this.SetImageItemBox(v);
            return v;
        }
        private ImageItemBox CreateItemBox(TextThumbImage item)
        {
            //var r = new List<ImageItemBox>();
            //foreach (var ev in tvis)
            {
                ImageItemBox av;

                if (BoxType == ImageItemBoxType.Normal)
                {
                    
                    av = new ImageItemBox(item,this.DisplaySet) { EnableNameTextBox = this.DisplaySet.Ctrl.EnableNameTextBox};
                }
                else if (BoxType == ImageItemBoxType.Story)
                {
                    av = new StoryImageItemBox(item,this.DisplaySet) { EnableNameTextBox = NameTextBoxType.HalfEnable};
                }
                else
                {
                    throw new Exception();
                }
                return av;
                //r.Add(av);
            }
            //return r.ToArray();

            
        }
        private void SetImageItemBox(ImageItemBox av)
        {
            av.SuspendLayout();
            this.DisplaySet.Ctrl.Controls.Add(av);
            this.ThumContItems.SetEvent(av);
            av.ResumeLayout(false);
        }
        
        #endregion
        #region GetMethod
        private IEnumerable<ImageItemBox> GetStockPages(ImageItemBox ib)
        {
            if (ib.TextItem.IsCategory)
            {
                var nth = this.GetNth(ib);
                yield return ib;
                var n = this.Items.Skip(nth + 1).TakeWhile((v) => v.TextItem.Type != TextPictureType.Category);
                foreach (var w in n) yield return w;

            }
        }
        public List<ImageItemBox> GetTagImageBoxList()
        {
            List<ImageItemBox> r = new List<ImageItemBox>();
            var IsOpen = true;
            foreach (var v in this.Items)
            {
                if (v.TextItem.IsCategory)
                {
                    IsOpen = v.Open;
                    r.Add(v);
                }
                else if (v.TextItem.Type == TextPictureType.Picture)
                {
                    if (IsOpen)
                    {
                        r.Add(v);
                    }
                    else
                    {
                        v.MapLocation = new Point(-1000, -1000);
                    }
                }
            }
            return r;
        }
        #endregion
        #region BaseMethod
        private void InvokeItemArrangeChanged()
        {
            if (this.ItemArrangeChanged != null) this.ItemArrangeChanged(this, new EventArgs());
        }
        public IEnumerable<ImageItemBox> Insert(int n,IEnumerable<TextThumbImage> tvis)
        {
            var list = tvis.Select((v) => this.CreateSetItemBox(v)).ToList();
            System.Console.WriteLine(base.Count);
            var r = list.ForEach2((v)=>base.Insert(n++,v)).ToList();
            //items.ForEach((v) => this.SetImageItemBox(v));
            //items.ToList().ForEach((v) => base.Insert(n++, v));
            this.InvokeItemArrangeChanged();
            return r;
        }
        public void MoveUp(int n)
        {
            if (n == 0) throw new Exception();
            if (n >= this.Items.Count) throw new Exception();
            var f = this.Items[n];
            this.Items.RemoveAt(n);
            this.Items.Insert(n - 1, f);
            this.InvokeItemArrangeChanged();
        }
        public void MoveDown(int n)
        {
            if (n + 1 == this.Items.Count - 1) throw new Exception();
            MoveUp(n + 1);
        }
        public IEnumerable<ImageItemBox> FirstAddRange(IEnumerable<TextThumbImage> tvis)
        {
            this.DisplaySet.MapMethod.EndMapLocation();
            var plist = tvis.Select((v) => this.CreateSetItemBox(v)).ForEach2((v)=>this.DisplaySet.MapMethod.AddMapLocation(v)).ForEach2((v)=>base.Add(v)).ToList();
            //this.DisplaySet.MapMethod.EndMapLocation();
            return plist;
        }
        public IEnumerable<ImageItemBox> AddRange(IEnumerable<TextThumbImage> tvis)
        {
            var plist = tvis.Select((v) => this.CreateSetItemBox(v)).ForEach2((v) => this.DisplaySet.MapMethod.AddMapLocation(v)).ForEach2((v) => base.Add(v)).ToList();
            //this.DisplaySet.MapMethod.EndMapLocation();
            return plist;
        }
        public ImageItemBox Delete(int n)
        {
            var item = this.Items[n];
            
            return this.Delete(item);
        }
        public ImageItemBox Delete(ImageItemBox item)
        {
            
            //this.DisplaySet.Ctrl.Controls.Remove(item);
            var n = this.GetNth(item);
            this.Items.RemoveAt(n);
            this.InvokeItemArrangeChanged();
            return item;
        }
        public void Delete(IEnumerable<ImageItemBox> items)
        {
            var ritems = this.Items.Except(items).ToList();
            base.Clear();
            
            ritems.ForEach((v) => this.Add(v));
            this.InvokeItemArrangeChanged();
        }
        
        public new void Insert(int nth,ImageItemBox item)
        {
            
            if (this.Items.Contains(item))
            {
                var nownth = this.IndexOf(item);
                if (nownth != nth)
                {
                    this.Delete(item);
                }
                else { return; }
            }
            this.Items.Insert(nth, item);
            this.InvokeItemArrangeChanged();
        }
        #endregion
        #region CompareSwap

        private ImageItemBox[] GetMinaxY(Point NowMapPoint)
        {
            var y = this.Items.Min((t) => System.Math.Abs(t.MapLocation.Y - NowMapPoint.Y));
            var ims = this.Items.Where((t) => System.Math.Abs(t.MapLocation.Y - NowMapPoint.Y) == y);
            return ims.ToArray();
        }
        private ImageItemBox GetMinaxX(Point NowMapPoint, ImageItemBox[] ims)
        {

            var x = ims.Min((t) => System.Math.Abs(t.MapLocation.X - NowMapPoint.X));

            var im = ims.First((t) => System.Math.Abs(t.MapLocation.X - NowMapPoint.X) == x);
            return im;
        }
        private ImageItemBox GetMinax(Point ItemMapLocation)
        {
            var f = this.GetMinaxY(ItemMapLocation);
            var g = this.GetMinaxX(ItemMapLocation, f);
            return g;
        }
        public void OptimalSwap(ImageItemBox item, Point ItemMapLocation)
        {
            var f = this.GetMinax(ItemMapLocation);
            if (f != item)
            {
                var nth = this.IndexOf(item);
                var optimalnth = this.IndexOf(f);
                this.Items.RemoveAt(nth);
                if (optimalnth < nth)
                {
                    this.Items.Insert(optimalnth, item);
                }
                else
                {
                    this.Items.Insert(optimalnth, item);
                }
                this.DisplaySet.MapMethod.SetMapLocation();
                this.DisplaySet.MapMethod.SetLocation();


                //this.DragStartLocation = this.Ctrl.PointToClient(Control.MousePosition);
            }
        }
        
        #endregion
    }
}
