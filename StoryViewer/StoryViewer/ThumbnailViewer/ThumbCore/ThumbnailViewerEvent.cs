﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase;
using SuouBase.Text;
using StoryViewer;
using StoryViewer.ThumbnailViewer.StoryImageItem;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
namespace StoryViewer.ThumbnailViewer
{
    public partial class ThumbnailViewerOperator
    {
        #region Event

        public void InvokeSettingChanged()
        {
            ThumSettingEventArgs e = new ThumSettingEventArgs(true);
            this.SaveData_SettingChanged(null, e);
        }
        private void SaveData_SettingChanged(object sender, ThumSettingEventArgs e)
        {
            if (this.ItemCount == 0) return;

            this.RowItems.ForEach((v) =>
            {
                v.Percent = this.SaveData.Percent;
                v.PictureSize = this.SaveData.ItemSize;
                v.MinimumSize = this.SaveData.MinItemSize;
                v.Mode = this.SaveData.ViewMode;

            }
            );
            if (e.IsPictureDelete)
            {
                this.ClearImage();
            }
            this.RearrangeImage();
            this.DisplaySet.ScrollBarReset();
            if (this.SavedataChanged != null) this.SavedataChanged(sender, e);


        }
        private void ArrangeItemCollection_CatalogSizeChanged(object sender, EventArgs e)
        {
            DisplaySet.ScrollBarReset();
        }
        private void EnlargeItemCollection_ItemSizeChanged(object sender, EventArgs e)
        {
            this.MapOperator.SetMapLocation();
        }

        #endregion
        #region SetEventMethod
        public void SetDragItemEvent(ImageItemBox item)
        {
            item.MouseWheel += this.NormalTextBox_MouseWheel;
            item.ImageCreated += this.Item_ImageCreated;
            item.MouseMove += this.ImageItemBox_MouseMove;
            item.MouseUp += this.ImageItemBox_MouseUp;
            item.DragOver += this.Ctrl.ImageItemBox_DragOver;
            item.DragDrop += this.Ctrl.ImageItemBox_DragDrop;
            item.AllowDrop = this.Ctrl.AllowDrop;
        }
        public void SetEvent(ImageItemBox item)
        {

            item.MouseWheel += this.NormalTextBox_MouseWheel;
            item.ImageCreated += this.Item_ImageCreated;
            item.MouseMove += this.ImageItemBox_MouseMove;
            item.PictureBoxMouseDowned += this.ImageItemBox_MouseDown;
            item.PictureBoxMouseDowned += this.Ctrl.ImageItemBox_MouseDown;
            item.MouseUp += this.ImageItemBox_MouseUp;
            item.DragOver += this.Ctrl.ImageItemBox_DragOver;
            item.DragDrop += this.Ctrl.ImageItemBox_DragDrop;
            item.MouseClick += this.Ctrl.ImageItemBox_MouseClick;
            item.ToolTipShowing += this.ImageItemBox_ToolTipShowing;
            item.ItemIntervalChanged += this.ImageBox_ItemIntervalChanged;
            item.MouseLeave += this.Ctrl.ImageItemBox_MouseLeave;
            item.SizeChanged += this.ImageItemBox_SizeChanged;
            item.SpecialSizeChanged += this.ImageItemBox_SpecialSizeChanged;
            item.MouseEnter += this.ImageItemBox_MouseEnter;
            item.ImageDisposed += this.Item_ImageDisposed;
            item.ContentChanged += this.ImageItemBox_TextContentChange;
            item.RightClickContext = this.Ctrl.ItemContext;
            item.ShiftRightClickContext = this.Ctrl.ShiftItemContext;
            item.AllowDrop = this.Ctrl.AllowDrop;


        }
        public void RemoveEvent(ImageItemBox item)
        {

            item.MouseWheel -= this.NormalTextBox_MouseWheel;
            item.ImageCreated -= this.Item_ImageCreated;
            item.MouseMove -= this.ImageItemBox_MouseMove;
            item.PictureBoxMouseDowned -= this.ImageItemBox_MouseDown;
            item.PictureBoxMouseDowned -= this.Ctrl.ImageItemBox_MouseDown;
            item.MouseUp -= this.ImageItemBox_MouseUp;
            item.DragOver -= this.Ctrl.ImageItemBox_DragOver;
            item.DragDrop -= this.Ctrl.ImageItemBox_DragDrop;
            item.MouseClick -= this.Ctrl.ImageItemBox_MouseClick;
            item.ToolTipShowing -= this.ImageItemBox_ToolTipShowing;
            item.ItemIntervalChanged -= this.ImageBox_ItemIntervalChanged;
            item.MouseLeave -= this.Ctrl.ImageItemBox_MouseLeave;
            item.SizeChanged -= this.ImageItemBox_SizeChanged;
            item.SpecialSizeChanged -= this.ImageItemBox_SpecialSizeChanged;
            item.MouseEnter -= this.ImageItemBox_MouseEnter;
            item.ImageDisposed -= this.Item_ImageDisposed;
            item.ContentChanged -= this.ImageItemBox_TextContentChange;
            item.RightClickContext = null;
            item.ShiftRightClickContext = null;
            item.AllowDrop = false;

            item.SuperDispose();

        }
        #endregion
        #region ImageItemBoxEvent
        public void ImageItemBox_TextContentChange(object sender, EventArgs e)
        {
            var item = (ImageItemBox)sender;
            this.InvalidCheckItem(item);
        }
        

        public Point GetPointDistance(Point a, Point b)
        {
            var p = new Point(b.X - a.X, b.Y - a.Y);
            return p;
        }

        private void ImageItemBox_Reversed(object sender, EventArgs e)
        {
            if (!(sender is ImageItemBox)) return;
            var v = (ImageItemBox)sender;
            this.MapOperator.NowReverseImageBox = v;
        }
        private void ImageItemBox_SizeChanged(object sender, EventArgs e)
        {
            return;
            /*
            var ib = (ImageItemBox)sender;
            if (!ib.Visible) return;
            this.RearrangeImage();
            */
        }
        private void ImageItemBox_SpecialSizeChanged(object sender, EventArgs e)
        {
            
            var ib = (ImageItemBox)sender;
            if (!ib.Visible) return;
            this.RearrangeImage();
        }
        private void ImageItemBox_MouseDown(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                this.StartMouseDownPoint = Control.MousePosition;

                this.Ctrl.MouseDownItem = this.MapOperator.SelectedImageBoxItem;
            }
        }
        private void ImageItemBox_MouseUp(object sender, MouseEventArgs e)
        {
            this.DragItem.DragEnd();
            this.Ctrl.MouseDownItem = null;


        }
        private void ImageItemBox_MouseEnter(object sender, EventArgs e)
        {
            this.MouseEnterItem = (ImageItemBox)sender;
            if (this.ImageBoxMouseEnter != null) this.ImageBoxMouseEnter(this, e);
        }
        private void Item_ImageDisposed(object sender, EventArgs e)
        {
            var size = (Size)sender;
            var sizeint = size.Width * size.Height;
            this.TotalImageSize -= sizeint;

            if (this.TotalImageSize < 0) this.TotalImageSize = 0;
        }
        private void Item_ImageCreated(object sender, EventArgs e)
        {
            var size = (Size)sender;
            var sizeint = size.Width * size.Height;
            this.TotalImageSize += sizeint;


            if (this.TotalImageSize > this.SaveData.MaxTotalImageSize)
            {
                this.ClearImage();
            }

            if (ImageCreated != null) ImageCreated(this, e);
        }
        private void ImageItemBox_MouseMove(object sender, MouseEventArgs e)
        {
            this.Ctrl.InvokeMouseMove(e);
            this.DragItem.MoveCheckDragVirtualItem();
        }
        private void ImageItemBox_ToolTipShowing(object sender, EventArgs e)
        {
            this.ToolTipHideAll();
        }

        private void ThumContViewBox_MouseMove(object sender, MouseEventArgs e)
        {
            this.DragItem.MoveCheckDragVirtualItem();
        }
        public void ImageBox_ItemIntervalChanged(object sender, EventArgs e)
        {
            this.MapOperator.SetMapLocation();
        }
        private void Items_ArrangeChanged(object sender, EventArgs e)
        {
            
            this.RearrangeImage();
        }


        #endregion
    }
}
