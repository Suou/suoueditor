﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase;
using SuouBase.Text;
using StoryViewer;
using SuouBase.Interface;
using StoryViewer.ThumbnailViewer.StoryImageItem;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using StoryViewer.ThumbnailViewer.ThumbCore;
namespace StoryViewer.ThumbnailViewer
{
    /// <summary>
    /// ThumbnaiViewerの主要操作
    /// </summary>
    public partial class ThumbnailViewerOperator
    {
        #region Property

        private LockActionWindowsTimer MapResizeTimer { get; set; }
        public void MapResise(){
            this.MapResizeTimer.Do();
        }
        public int TotalImageSize { get; set; }
        public Point StartMouseDownPoint { get; set; }
        private ThumbnailView_SaveData _SaveData = null;
        public ThumbnailView_SaveData SaveData
        {
            get
            {
                return this._SaveData;
            }
            set
            {
                if (value != null)
                {
                    if (this._SaveData != null) this._SaveData.SettingChanged -= this.SaveData_SettingChanged;
                    this._SaveData = value;
                    this.SaveData.SettingChanged += this.SaveData_SettingChanged;
                    this.InvokeSettingChanged();
                }
            }
        }
        public DragItem DragItem;
        private ImageItemBoxCollection ImageItemBoxItems;

        public MapPointer MapOperator { get; private set; }
        public ImageItemBoxType BoxType
        {
            get { return this.ImageItemBoxItems.BoxType; }
            set
            {
                this.ImageItemBoxItems.BoxType = value;
            }
        }
        private ThumContViewBox _Ctrl;
        public ThumContViewBox Ctrl
        {
            get { return _Ctrl; }
            private set
            {
                this._Ctrl = value;
            }
        }
        public event ImageItemBoxEventHandler ImageCreated;
        public EditorInfo DLLFunction { get; set; }

        private DisplaySetting DisplaySet;
        public event EventHandler ImageBoxMouseEnter;
        public event EventHandler SavedataChanged;
        public ImageItemBox MouseEnterItem { get; set; }
        #endregion
        #region GetProperty
        public TextThumbImage[] RowItems
        {
            get
            {
                return this.ImageItemBoxItems.Select((v) => v.TextItem).ToArray();
            }
        }
        public int ItemCount
        {
            get
            {
                return this.ImageItemBoxItems.Count();
            }
        }
        public IEnumerable<ImageItemBoxesLapper> Clasters
        {
            get
            {
                return ImageItemBoxesLapper.GetClusters(this.ImageItemBoxItems,this.SaveData.Model);
                
            }
        }
        public ImageItemBoxesLapper SelectedCluster
        {
            get
            {
                var v = this.Clasters;
                if (v != null)
                {
                    return v.FirstOrDefault((w) => w.Pages.Contains(this.MouseEnterItem.TextItem.Page));
                }
                else
                {
                    return null;
                }
            }
        }
        #endregion
        #region Constructor
        public ThumbnailViewerOperator(ThumContViewBox ctrl, DisplaySetting DS)
        {
            this.Ctrl = ctrl;
            this.DisplaySet = DS;
            this.DisplaySet.ContItem = this;
            ImageItemBoxItems = new ImageItemBoxCollection(DisplaySet, this);
            this.ImageItemBoxItems.ItemArrangeChanged += this.Items_ArrangeChanged;
            this.MapOperator = new MapPointer(this.DisplaySet, this.ImageItemBoxItems);

            this.MapOperator.CatalogSizeChenged += this.ArrangeItemCollection_CatalogSizeChanged;

            this.Ctrl.MouseWheel += this.NormalTextBox_MouseWheel;
            DragItem = new DragItem(this.DisplaySet);
            this.Ctrl.MouseMove += this.ThumContViewBox_MouseMove;
            this.MapResizeTimer = new DelayLockActionWindowsTimer(() => { this.MapOperator.SetMapLocation();  }, 50);

        }
        #endregion
        #region Method
        public void InvalidCheckItem(ImageItemBox item)
        {
            var n = this.ImageItemBoxItems.GetNth(item);
            var p = ImageItemBoxesLapper.GetCluster(this.ImageItemBoxItems, n,this.SaveData.Model);
            p.CheckInvalid();
            if (n > 0)
            {
                var p2 = ImageItemBoxesLapper.GetCluster(this.ImageItemBoxItems, n - 1,this.SaveData.Model);
                p2.CheckInvalid();
            }
            if (n < this.ImageItemBoxItems.Count - 1)
            {
                var p3 = ImageItemBoxesLapper.GetCluster(this.ImageItemBoxItems, n + 1,this.SaveData.Model);
                p3.CheckInvalid();
            }
        }
        public void OpenAll()
        {
            this.ImageItemBoxItems.Where((v) => v.TextItem.IsCategory).ForEach((v) => v.Open = true);
            this.RearrangeImage();
        }
        public void CloseAll()
        {
            this.ImageItemBoxItems.Where((v) => v.TextItem.IsCategory).ForEach((v) => v.Open = false);
            this.RearrangeImage();
        }

        public void Add(TextThumbImage tvi)
        {
            this.SetModetoItem(tvi);
            var r = new TextThumbImage[] { tvi };
            this.AddRange(r);
        }
        public void MoveDrag()
        {
            this.DragItem.MoveCheckDragVirtualItem();
        }
        private void SetModetoItem(TextThumbImage item)
        {
            item.Mode = this.SaveData.ViewMode;
            if (this.SaveData.ViewMode == ThumbnailMode.Percent) item.Percent = this.SaveData.Percent;
            if (this.SaveData.ViewMode == ThumbnailMode.Size) item.PictureSize = this.SaveData.ItemSize;
            item.IsSmallerRawView = this.SaveData.SmallerRawView;
            item.IsLoose = this.SaveData.IsLoose;
            item.MinimumSize = this.SaveData.MinItemSize;
            item.ProhibitCategory = !this.SaveData.PermitCategory;
        }
        //全てのアイテムのイベントが除かれる保証は無い
        public void RowItemDispose()
        {
            foreach (var v in this.ImageItemBoxItems)
            {
                this.RemoveEvent(v);
            }
        }
        public IEnumerable<IImageItemBox> BoxItems
        {
            get
            {
                return this.ImageItemBoxItems.Select((v) => (IImageItemBox)v);
            }
        }
        public void MoveViewPoint(Point p)
        {
            this.DisplaySet.MapLocation = p;
            this.MapOperator.SetLocation();
        }
        public void MoveViewPoint(int index)
        {
            var p = this.ImageItemBoxItems[index];
            this.MoveViewPoint(p.MapLocation);
        }
        public void HyperClear()
        {
            this.MapOperator.VisibleItemBox.ForEach((v) => v.Refresh());

        }
        public void Clear()
        {
            RowItemDispose();
            this.ImageItemBoxItems.Clear();
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Clear);
            this.TotalImageSize = 0;
            this.MapOperator.EndMapLocation();
        }

        public void CreanNoVisibleImage()
        {
            foreach (var imagebox in this.ImageItemBoxItems)
            {
                if(!imagebox.Visible)imagebox.ClearImage();
            }
        }

        public void InsertDragItem(TextThumbImage tvi)
        {
            this.SetModetoItem(tvi);
            var tvis = new TextThumbImage[] { tvi };
            {
                //var w = this.ImageItemBoxItems.AddRange(tvis);
                var w = this.ImageItemBoxItems.Insert(0,tvis);
                var MousePoint = this.Ctrl.PointToClient(Control.MousePosition);
                DragItem.DragStart(w.First(), MousePoint, DragMode.Drag);
            }
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Insert);
        }
        public void Insert(int n, TextThumbImage tvi)
        {
            this.SetModetoItem(tvi);
            var r = new TextThumbImage[] { tvi };
            this.InsertRange(n, r);
        }
        public void InsertToSelectedLocation(TextThumbImage tvi)
        {
            if (this.MapOperator.NearItemIndex == -1)
            {
                this.Add(tvi);
            }
            else
            {
                this.Insert(this.MapOperator.NearItemIndex + 1, tvi);
            }
        }
        public void InsertToSelectedLocation(IEnumerable<TextThumbImage> tvis)
        {
            if (this.MapOperator.NearItemIndex == -1)
            {
                this.AddRange(tvis);
            }
            else
            {
                this.InsertRange(this.MapOperator.NearItemIndex + 1, tvis);
            }
        }
        public void InsertRange(int n, IEnumerable<TextThumbImage> tvis)
        {
            var ntvis = tvis.ForEach2((tvi)=> this.SetModetoItem(tvi)).ToArray();
            {
                this.ImageItemBoxItems.Insert(n,ntvis);
            }
            if (this.Ctrl.ItemViewChanged != null) this.Ctrl.ItemViewChanged(this, new EventArgs());
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Insert);

            
        }
        public void MoveUp(int n)
        {
            this.ImageItemBoxItems.MoveUp(n);
            if (this.Ctrl.ItemViewChanged != null) this.Ctrl.ItemViewChanged(this, new EventArgs());
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Move);
        }
        public void MoveDown(int n)
        {
            this.MoveUp(n + 1);
        }
        public void AddRange(IEnumerable<TextThumbImage> tvis)
        {
            //if (tvis.Count() == 1) throw new MyException("");
            {
                this.ImageItemBoxItems.AddRange(tvis);
            }
            if (this.Ctrl.ItemViewChanged != null) this.Ctrl.ItemViewChanged(this, new EventArgs());
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Add);
        }
        public void Delete(int n)
        {
            var item = this.ImageItemBoxItems.Delete(n);
            this.Ctrl.Controls.Remove(item);
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Delete);

        }
        public void DeleteSelectedItem(IEnumerable<ImageItemBox> items)
        {
            this.ImageItemBoxItems.Delete(items);
            items.ForEach((v) => this.Ctrl.Controls.Remove(v));
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Delete);

        }
        public void ImageItemSet(IEnumerable<TextThumbImage> tvis)
        {
            tvis.ForEach((v) => this.SetModetoItem(v));

            DisplaySet.Reset2();

            {
                this.Clear();
                this.ImageItemBoxItems.FirstAddRange(tvis);
                this.MapOperator.SetMapLocation(); 
            }
            this.RearrangeImage();
            if (this.Ctrl.ItemViewChanged != null) this.Ctrl.ItemViewChanged(this, new EventArgs());
            this.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.NewSet);
        }
        public void CheckChangePicture()
        {
            var items = this.ImageItemBoxItems.Where((v) => v.IsPictureChanged);

            if (items.Count() != 0)
            {
                //items.ForEach((v) => v.ClearImage());
                items.ForEach((v) => v.Formize());
                items.ForEach((v) => v.RefreshImage());
                
                this.RearrangeImage();
            }
        }

        
        public void RearrangeImage()
        {
            this.MapOperator.SetMapLocation();

            this.MapOperator.SetLocation();
        }
        #endregion
    }
}
