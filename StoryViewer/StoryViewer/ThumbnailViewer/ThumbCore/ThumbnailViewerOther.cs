﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase;
using SuouBase.Text;
using StoryViewer;
using StoryViewer.ThumbnailViewer.StoryImageItem;
using BSystem.Collections;
using SuouBase.Book;
namespace StoryViewer.ThumbnailViewer
{
    public partial class ThumbnailViewerOperator
    {
        #region Draw
        private void DrawPointerLine(ImageItemBox ib, Graphics g)
        {
            if (!ib.Visible) return;
            if (this.SaveData.OrderMode == ThumbnailOrder.Thread)
            {
                var p = ib.BottomLocation;
                g.DrawLine(Pens.Red, new Point(0, p.Y), new Point(DisplaySet.DisplaySize.Width, p.Y));
            }
            else
            {
                var p1 = new Point(ib.Location.X + ib.Width, ib.Location.Y);
                var p2 = new Point(ib.Location.X + ib.Width, ib.BottomLocation.Y);
                g.DrawLine(Pens.Red, p1, p2);
            }
        }
        private int DrawPointerIndex = -1;
        public void DrawNearBoxPointerLine(Graphics g)
        {
            if (this.MapOperator.NearItemIndex == -1)
            {
            }
            else
            {
                if (this.DrawPointerIndex != this.MapOperator.NearItemIndex)
                {
                    this.DrawPointerIndex = this.MapOperator.NearItemIndex;
                    this.Ctrl.Refresh();

                }
                else
                {
                    var box = this.MapOperator.NearItemBox;
                    if (box != null)
                    {
                        this.DrawPointerLine(box, g);
                    }
                }
            }
        }
        #endregion
        #region OtherMethod
        
        public void ClearImage()
        {
            this.ImageItemBoxItems.ToList().ForEach((v) => v.ClearImage());
        }

        public long UsingMemory
        {
            get
            {
                long r = 0;
                this.ImageItemBoxItems.ForEach((v) => r += v.UsingMemory);
                return r;
            }
        }
        public ImageItemBoxesLapper GetCluster(StoryImageItemBox ibs)
        {
            var v = (SectionScene)ibs.TextItem.Page;
            var p = this.Clasters.FirstOrDefault((w) => w.Pages.Contains(v));
            return p;
        }

        public void CheckBoxes(StoryImageItemBox[] imageboxes)
        {
            //var boxes = imageboxes.Select((v) => (StoryImageItemBox)v);
            foreach (var v in imageboxes)
            {
                var p = this.GetCluster(v);
                v.PictureBackColor = p.IsInvalid ? Color.Red : Color.White;
            }
        }
        #endregion
        public void NormalTextBox_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            var p = this.Ctrl.PointToClient(Control.MousePosition);
            var rec = new Rectangle(Point.Empty, this.Ctrl.Size);
            if (rec.Contains(p))
            {
                int k = SystemInformation.MouseWheelScrollLines * this.DisplaySet.vScrollBar.SmallChange;
                if (e.Delta < 0) k = -k;
                var max = 1 + this.DisplaySet.vScrollBar.Maximum - this.DisplaySet.vScrollBar.LargeChange;
                var m = this.DisplaySet.vScrollBar.Value;
                if (m - k > max) k = m - max;
                if (m - k < this.DisplaySet.vScrollBar.Minimum) k = m - this.DisplaySet.vScrollBar.Minimum;
                this.DisplaySet.vScrollBar.Value -= k;
            }
        }
        public void ToolTipHideAll()
        {
            foreach (var v in this.ImageItemBoxItems) v.HideToolTip();
        }
    }
}
