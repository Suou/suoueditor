﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.ObjectModel;
using SuouBase.Api;
using SuouBase;
using SuouBase.Drawing;
using StoryViewer;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
namespace StoryViewer.ThumbnailViewer.ThumbCore
{
    /// <summary>
    /// 座標点の入力に対するデータの操作，入力サイズに対するデータの位置設定を行う
    /// </summary>
    public class MapPointer
    {
        //LastMouseOnSelectedItem
        //MouseOnSelectedItem
        //GroupSlectedItem

        DisplaySetting DisplaySet { get; set; }
        ImageItemBoxCollection ImageItemBoxItems { get; set; }
        public event EventHandler CatalogSizeChenged;
        public MapPointer(DisplaySetting ds,ImageItemBoxCollection items)
        {
            this.DisplaySet = ds;
            this.ImageItemBoxItems = items;
            this.SelectRectangle = new DragRectangle(this.DisplaySet);
            this.LastSelectedImageBoxItems = new ImageItemBox[] { };
        }
        #region MapGetProperty
        public ImageItemBox CursorPickUpBox
        {
            get
            {
                var np = this.ConvertCtrlPToEditP(this.DisplaySet.Ctrl.PointToClient(System.Windows.Forms.Cursor.Position));
                var box = this.SinglePick(np);
                return box;
            }
        }
        public Rectangle WatchingRectangle
        {
            get
            {
                var si = this.DisplaySet.Ctrl.Size;
                var location = new Point(this.DisplaySet.hScrollBar.Value, this.DisplaySet.vScrollBar.Value);
                return new Rectangle(location, si);
            }
        }

        #endregion
        #region Index
        public int NearItemIndex { get; private set; }
        public void SetNearItemIndex()
        {
            var p = this.DisplaySet.Ctrl.PointToClient(Control.MousePosition);
            var f = this.GetNearItemIndex(p);
            this.NearItemIndex = f;
        }
        public ImageItemBox NearItemBox
        {
            get
            {
                if (this.NearItemIndex == -1 || this.ImageItemBoxItems.Count <= this.NearItemIndex) return null;
                return this.ImageItemBoxItems[this.NearItemIndex];
            }
        }
        public ImageItemBox[] LastSelectedImageBoxItems {get;set;}
        public int[] LastSelectedIndexes
        {
            get
            {
                if (this.LastSelectedImageBoxItems == null) return new int[] { };
                return this.LastSelectedImageBoxItems.Select((v) => this.GetNth(v)).ToArray();
            }
        }
        public ImageItemBox SelectedImageBoxItem
        {
            get
            {
                var v = this.LastSelectedImageBoxItems;
                if (v.Length != 0) return v.First();
                return null;
            }
        }
        public DragRectangle SelectRectangle { get; set; }
        public int SelectedItemIndex
        {
            get
            {

                var f = this.SelectedImageBoxItem;
                if (f == null) return -1;
                var nth = this.GetNth(f);
                return nth;
            }
        }
        public TextThumbImage SelectedItem
        {
            get
            {
                var v = this.SelectedImageBoxItem;
                if (v == null) return null;
                return v.TextItem;
            }
        }
        public TextThumbImage MouseOnSelectedItem
        {
            get
            {
                var v = this.CursorPickUpBox;
                if (v == null)
                {
                    return null;
                }
                else
                {
                    return v.TextItem;
                }
            }
        }
        private int _LastSelectedIndex = -1;
        public int LastSelectedIndex
        {
            get
            {
                if (this._LastSelectedIndex >= this.ImageItemBoxItems.Count) return -1;
                return this._LastSelectedIndex;
            }
            set
            {
                if (value != -1) this._LastSelectedIndex = value;
            }
        }
        public TextThumbImage LastSelectedItem
        {
            get
            {
                if (this.LastSelectedIndex == -1) return null;
                var v = this.GetItem(this.LastSelectedIndex);
                return v;
            }
        }
        public int GetNth(ImageItemBox f)
        {
            return this.ImageItemBoxItems.GetNth(f);
        }
        public TextThumbImage GetItem(int index)
        {
            if (index == -1 || this.ImageItemBoxItems.Count <= index) throw new MyException("");
            return this.ImageItemBoxItems[index].TextItem;
        }
        #endregion

        #region MapLop
        /*
        private void SetMapLocationThread()
        {
            var tick = new Stopwatch();
            var items = this.ImageItemBoxItems.GetTagImageBoxList();
            Point ItemLocation = new Point(0, 0);
            var FirstImageBox = items.Count == 0 ? null : items.First();
            var topY = FirstImageBox != null ? FirstImageBox.ItemInterval : 5;
            Point pbase = new Point(0, topY);

            int nth = 1;
            int realpageNumber = 1;
            foreach (ImageItemBox tvi in items)
            {
                tick.Start();
                tvi.Formize();
                tick.Stop();
                pbase.Y += tvi.ItemInterval;
                tvi.MapLocation = new Point(ItemLocation.X, pbase.Y);
                tvi.Number = nth++;
                realpageNumber = this.SetImageBoxRealPage(tvi, realpageNumber);

                tvi.BackColor = tvi.TextItem.ControlBackColor;

                pbase.Y += tvi.Rec.Height;
                ItemLocation = new Point(0, 0);
            }
            if (this.ImageItemBoxItems.Count != 0) pbase.X = items.Max((v) => v.Rec.Width + v.ItemInterval);
            DisplaySet.CatalogSize = new Size(pbase.X, pbase.Y + ItemLocation.Y);
            if (CatalogSizeChenged != null) CatalogSizeChenged(this, new EventArgs());
        }
        */
        /// <summary>
        /// 各アイテムの仮想座標上の位置を更新します
        /// </summary>
        public void SetMapLocation()
        {
            if (this.DisplaySet.Ctrl.ParentForm == null || !this.DisplaySet.Ctrl.Visible) return;
            {
                this.EndMapLocation();
                foreach (var v in this.ImageItemBoxItems)
                {
                    this.AddMapLocation(v);
                }
                
                if(this.MapMapper!=null)this.MapMapper.SetDisplaySize();

                //this.SetMapLocationOther();
            }
        }
        public static int TestNumber = 0;
        private int SetImageBoxRealPage(ImageItemBox tvi, int realpageNumber)
        {
            if (tvi is StoryImageItemBox)
            {
                var v = (StoryImageItemBox)tvi;
                v.RealPageNumber = realpageNumber++;
                if (v.JoinTop) realpageNumber--;
            }
            return realpageNumber;
        }

        public void AddMapLocation(ImageItemBox ib)
        {
            if (this.DisplaySet.Ctrl.ParentForm == null || !this.DisplaySet.Ctrl.Visible) return;
            if (this.MapMapper == null)
            {
                this.FirstSet();
            }
            this.MapMapper.SetPoint(ib);
        }
        public void EndMapLocation()
        {
            if(this.MapMapper != null)this.MapMapper.End();
        }

        Mapper MapMapper { get; set; }
        private void FirstSet()
        {
            this.MapMapper = new Mapper(this.DisplaySet);
            this.DisplaySet.Ctrl.Refresh();
            this.MapMapper.Ended += this.Map_EndMapLocation;
        }
        private void Map_EndMapLocation(object sender,EventArgs e)
        {
            this.SetLocation();
            if (CatalogSizeChenged != null) CatalogSizeChenged(this, new EventArgs());
            
            this.MapMapper.Ended -= this.Map_EndMapLocation;
            this.MapMapper = null;
        }
        /*
        private void SetMapLocationOther()
        {
            
            var items = this.ImageItemBoxItems.GetTagImageBoxList();
            //var items = this.ImageItemBoxItems;
            int YokoNum = 0;
            //ImageItemBox ttv = null;
            Point p = new Point(0, 0);
            var FirstImageBox = this.ImageItemBoxItems.Count == 0 ? null : items.First();
            var topY = FirstImageBox != null ? FirstImageBox.ItemInterval : 5;
            Point pbase = new Point(0, topY);

            int nth = 0;
            int realpageNumber = 1;
            foreach (ImageItemBox tvi in items)
            {
                tvi.Formize();
                if (YokoNum >= 1 && (p.X + tvi.Size.Width + tvi.ItemInterval > DisplaySet.DisplaySize.Width) || (this.DisplaySet.ContItem.SaveData.OrderMode == ThumbnailOrder.Thread))
                {
                    pbase.Y += p.Y + tvi.ItemInterval;
                    p = new Point(0, 0);
                    YokoNum = 0;
                }
                tvi.MapLocation = new Point(p.X, pbase.Y);
                tvi.Number = nth;
                nth++;
                YokoNum++;
                realpageNumber = this.SetImageBoxRealPage(tvi, realpageNumber);

                p.X += tvi.Size.Width + tvi.ItemInterval;
                if (p.Y < tvi.Rec.Height) p.Y = tvi.Rec.Height;
                if (pbase.X < p.X) pbase.X = p.X;
                //ttv = tvi;
                tvi.BackColor = tvi.TextItem.ControlBackColor;
            }
            
            DisplaySet.CatalogSize = new Size(pbase.X, pbase.Y + p.Y);
            if (CatalogSizeChenged != null) CatalogSizeChenged(this, new EventArgs());
        }
        */
        public Point GetLocation(ImageItemBox tvi)
        {
            return new Point(tvi.MapLocation.X - DisplaySet.MapLocation.X, tvi.MapLocation.Y - DisplaySet.MapLocation.Y);
        }
        public void SetLocation()
        {
            if (this.DisplaySet.Ctrl.ParentForm == null || !this.DisplaySet.Ctrl.Visible) return;
            //SuouBase.Api.WINAPI.StopControlDraw(this.DisplaySet.Ctrl.Handle);
            {
                var InnerItems = this.Pick(this.DisplaySet.MapRectangle);
                var OuterItems = this.OutPick(this.DisplaySet.MapRectangle);
                OuterItems.Where((v) => v.VisibleFlag).ForEach((v) => v.VisibleFlag = false);
                InnerItems.ForEach2((v) => v.Location = this.GetLocation(v)).ForEach((v) => { if(!v.Visible)v.VisibleFlag = true; });

                if (this.ImageItemBoxItems.BoxType == ImageItemBoxType.Story && this.VisibleItemBox != null)
                {
                    var p = InnerItems.Except(this.VisibleItemBox);
                    this.DisplaySet.ContItem.CheckBoxes(p.Select((v)=>(StoryImageItemBox)v).ToArray());
                }
                this.VisibleItemBox = InnerItems;
            }
            //SuouBase.Api.WINAPI.StartControlDraw(this.DisplaySet.Ctrl.Handle);
            
            this.DisplaySet.Ctrl.Refresh();
        }
        #endregion
        #region ConverPoint
        public Point ConvertCtrlPToEditP(Point p)
        {
            var r = new Point(p.X + this.DisplaySet.hScrollBar.Value, p.Y + this.DisplaySet.vScrollBar.Value);
            return r;
        }
        public Rectangle ConvertCtrlPToEditP(Rectangle rec)
        {
            var r = rec;
            r.Location = ConvertCtrlPToEditP(rec.Location);
            return r;
        }
        public Point ConvertEditPToCtrlP(Point p)
        {
            var r = new Point(p.X - this.DisplaySet.hScrollBar.Value, p.Y - this.DisplaySet.vScrollBar.Value);
            return r;
        }
        public Rectangle ConvertEditPToCtrlP(Rectangle rec)
        {
            Rectangle r = rec;
            r.Location = ConvertEditPToCtrlP(rec.Location);
            return r;
        }
        #endregion
        #region RestoreItem

        public ImageItemBox[] VisibleItemBox { get; set; }
        private ImageItemBox _NowReverseImageBox = null;
        public ImageItemBox NowReverseImageBox
        {
            get
            {
                return this._NowReverseImageBox;
            }
            set
            {
                if (this.NowReverseImageBox != null) this.NowReverseImageBox.CheckReverse();
                this._NowReverseImageBox = value;
            }
        }
        #endregion
        #region Other
        public void SetSelectedImageBoxItems()
        {
            ImageItemBox[] r = new ImageItemBox[] { };
            if (this.DisplaySet.MapMethod.SelectRectangle.IsEnable)
            {
                r = this.Pick(this.DisplaySet.MapMethod.SelectRectangle.MapRectangle);
            }
            else
            {
                var box = this.DisplaySet.MapMethod.CursorPickUpBox;

                if (box != null)
                {
                    r = new ImageItemBox[] { box };
                }
            }

            if (this.DisplaySet.MapMethod.LastSelectedImageBoxItems != null)
            {
                foreach (var v in this.DisplaySet.MapMethod.LastSelectedImageBoxItems)
                {
                    if (!r.Contains(v)) v.IsSelected = false;
                }
            }
            r.ForEach((v) => v.IsSelected = true);
            r.ForEach((v) => v.Refresh());
            this.LastSelectedImageBoxItems = r;

        }
        #endregion
        

        #region Pick
        public ImageItemBox SinglePick(Point p)
        {
            ImageItemBox r2 = null;
            var r = this.ImageItemBoxItems.Where((ImageItemBox tv) => { return tv.Rec.Contains(p); }).ToArray();
            if (r != null && r.Length > 0)
            {
                r2 = r.First();
            }
            return r2;
        }
        public ImageItemBox[] Pick(Rectangle rec)
        {

            var r = this.ImageItemBoxItems.Where((ImageItemBox tv) => { return rec.IntersectsWith(tv.Rec); }).ToArray();

            return r;
        }
        public ImageItemBox[] OutPick(Rectangle rec)
        {

            var r = this.ImageItemBoxItems.Where((ImageItemBox tv) => { return !rec.IntersectsWith(tv.Rec); }).ToArray();

            return r;
        }
        public ImageItemBox[] Pick(Point p)
        {
            var r = this.ImageItemBoxItems.Where((ImageItemBox tv) => { return tv.Rec.Contains(p); }).ToArray();

            return r;
        }
        public ImageItemBox GetNearItem(Point p)
        {
            ImageItemBox r = null;
            //var n = 0;

            if (this.DisplaySet.MapMethod.VisibleItemBox != null)
            {
                if (this.DisplaySet.Ctrl.OrderMode == ThumbnailOrder.Thread)
                {
                    r = this.DisplaySet.MapMethod.VisibleItemBox.Where((v) => v.Location.Y < p.Y).Min2((v) => this.GetDistance(v.Location, p));
                }
                else
                {
                    r = this.DisplaySet.MapMethod.VisibleItemBox.Where((v) => v.Location.Y < p.Y && v.Location.X < p.X).Min2((v) => this.GetDistance(v.Location, p));
                }
            }
            if (r == null && this.ImageItemBoxItems.Count > 0) return this.ImageItemBoxItems.First();
            return r;
        }
        public int GetNearItemIndex(Point p)
        {
            var v = GetNearItem(p);
            if (v != null)
            {
                return this.GetNth(v);
            }
            else
            {
                return -1;
            }

        }
        public int GetDistance(Point p1, Point p2)
        {
            return System.Math.Abs(p1.X - p2.X) + System.Math.Abs(p1.Y - p2.Y);
        }
        #endregion
    }
}
