﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Diagnostics;
using System.Collections.ObjectModel;
using SuouBase.Api;
using SuouBase;
using SuouBase.Drawing;
using StoryViewer;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
namespace StoryViewer.ThumbnailViewer.ThumbCore
{

    class Mapper
    {
        public Point BasePoint;
        Size NowRowSize;
        public int Number = 0;
        public int TopY = 5;
        public int RealPageNumber = 1;
        public int YokoNum { get; set; }
        public bool IsFirst { get; set; }
        public event EventHandler Ended;
        DisplaySetting DisplaySet;
        public Mapper(DisplaySetting ds)
        {
            this.DisplaySet = ds;
        }
        public Mapper(DisplaySetting ds, bool first)
        {
            this.DisplaySet = ds;
        }
        public void SetPoint(ImageItemBox tvi)
        {

            if (this.IsFirst)
            {
                this.BasePoint.Y += tvi.ItemInterval;
                this.IsFirst = false;
            }
            tvi.Formize();
            if (YokoNum >= 1 && (NowRowSize.Width + tvi.Width + tvi.ItemInterval > DisplaySet.DisplaySize.Width) || (this.DisplaySet.ContItem.SaveData.OrderMode == ThumbnailOrder.Thread))
            {
                BasePoint.Y += NowRowSize.Height + tvi.ItemInterval;
                NowRowSize = new Size(0, 0);
                YokoNum = 0;
            }
            tvi.MapLocation = new Point(NowRowSize.Width, BasePoint.Y);
            tvi.Number = Number;
            Number++;
            YokoNum++;
            RealPageNumber = this.SetImageBoxRealPage(tvi, RealPageNumber);
            //終了時にやたらとここを通っているのが気になる

            NowRowSize.Width += tvi.Width + tvi.ItemInterval;
            if (NowRowSize.Height < tvi.Height) NowRowSize.Height = tvi.Height;
            if (BasePoint.X < NowRowSize.Width) BasePoint.X = NowRowSize.Width;
            tvi.BackColor = tvi.TextItem.ControlBackColor;
            this.SetLocation(tvi);
        }
        private int SetImageBoxRealPage(ImageItemBox tvi, int realpageNumber)
        {
            if (tvi is StoryImageItemBox)
            {
                var v = (StoryImageItemBox)tvi;
                v.RealPageNumber = realpageNumber++;
                if (v.JoinTop) realpageNumber--;
            }
            return realpageNumber;
        }

        public void End()
        {
            if (Ended != null) Ended(this, new EventArgs());
        }
        public void SetDisplaySize()
        {
            DisplaySet.CatalogSize = new Size(this.BasePoint.X, this.BasePoint.Y + this.NowRowSize.Height);
        }

        public void SetLocation(ImageItemBox ib)
        {
            if (this.DisplaySet.Ctrl.ParentForm == null || !this.DisplaySet.Ctrl.Visible) return;
            {
                if (IsInner(ib))
                {
                    ib.Location = this.DisplaySet.MapMethod.GetLocation(ib);
                    if (!ib.Visible)
                    {
                        ib.VisibleFlag = true;
                    }
                    //this.DisplaySet.Ctrl.Refresh();
                }
                else
                {
                    if (ib.Visible) ib.VisibleFlag = false;
                }
            }
        }
        public bool IsInner(ImageItemBox ib)
        {
            return this.DisplaySet.MapRectangle.IntersectsWith(ib.Rec);
        }
    }
}
