﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase;
using SuouBase.Plugin;
using SuouBase.Interface;
using SuouBase.Book;
namespace StoryViewer
{
    public partial class ImageItemBox : UserControl,IImageItemBox,IJoinTop
    {

        #region ImageItemBoxEvent
        
        private void ImageItemBox_ParentChanged(object sender, EventArgs e)
        {
            
            if (this.Parent != null)
            {
                this.AllowDrop = true;
                this.ParentControl = this.Parent;
            }
            else
            {
                this.ParentControl = null;
                
                this.AllowDrop = false;
            }
        }

        private void ImageItemBox_MouseEnter(object sender, EventArgs e)
        {
            //2013/03/39
            //this.Focus();
        }

        private void ImageItemBox_MouseClick(object sender, MouseEventArgs e)
        {
            var rightDiwn = (Control.MouseButtons & System.Windows.Forms.MouseButtons.Right) == System.Windows.Forms.MouseButtons.Right;
            var rightClick = (e.Button & System.Windows.Forms.MouseButtons.Right) == System.Windows.Forms.MouseButtons.Right;
            var leftClick = (e.Button & System.Windows.Forms.MouseButtons.Left) == System.Windows.Forms.MouseButtons.Left;
            var shift = Control.ModifierKeys == Keys.Shift;
            
            if (rightDiwn && leftClick)
            {
                if (ToolTipShowing != null) ToolTipShowing(this, new EventArgs());                
                this.aaToolTip1.ToolTipShow(Point.Empty,BSystem.RectCornerDirection.TopRightCorner);
            }
            else if (rightClick)
            {
                if (shift)
                {
                    if (this.ShiftRightClickContext != null) this.ShiftRightClickContext.Show(Cursor.Position);
                }
                else
                {
                    this.aaToolTip1.ToolTipHide();
                    if (this.RightClickContext != null) this.RightClickContext.Show(Cursor.Position);
                }
            }
        }

        private void ImageItemBox_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.VisibleFlag && this.Visible) this.Visible = false;
        }

        private void ImageItemBox_SizeChanged(object sender, EventArgs e)
        {
            if (this.TextItem != null)
            {
                if (this.Size != this.TextItem.PictureSize)
                {
                    //throw new Exception();
                    //this.Size = this.TextItem.PictureSize;
                }
            }
        }
        #endregion
        #region PictureEvent
        private void LoadPicture()
        {
            if (this.DesignMode || this.TextItem == null) return;
            if (this.NowPictureType != this.GetPictureType)
            {
                this.ClearImage();
                //if (this.PictureImage.Size != this.TextItem.PictureSize) return;
                var bmp = new Bitmap(this.TextItem.PictureSize.Width, this.TextItem.PictureSize.Height,System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
                
                using (var g = Graphics.FromImage(bmp))
                {
                    this.LoadPicture(g);
                }
                this.PictureImage = bmp;
                this.pictureBox1.Image = this.PictureImage;
                if (this.ImageCreated != null) ImageCreated(this.pictureBox1.Size, new EventArgs());
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            this.LoadPicture();
        }
        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.pictureBox1.Focus();
            
            this.OnMouseEnter(e);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            
            this.OnMouseLeave(e);
        }
        private void pictureBox1_MouseWheel(object sender, MouseEventArgs e)
        {
            
            this.OnMouseWheel(e);
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(e);
        }
        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.PictureBoxMouseDowned != null) this.PictureBoxMouseDowned(sender, e);
            //2013/03/29
            //this.OnMouseDown(e);
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            this.OnMouseUp(e);
        }

        protected virtual void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            this.OnMouseClick(e);
        }

        private void pictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            this.OnMouseDoubleClick(e);
        }
        #endregion
        #region OtherEvent
        public void OnSpecialSizeChanged(EventArgs e)
        {
            if (this.SpecialSizeChanged != null) this.SpecialSizeChanged(this, e);
        }
        protected void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.TextItem.Page.Name = this.NameTextBox.Text;
        }
        private void TextItem_Changed(object sender, BookPageContentChangeEventArgs e)
        {
            if (this.ContentChanged != null) this.ContentChanged(this, e);
            if (e.Type == PageContentChangeType.Name)
            {
                this.CheckNameTextBox();
            }
            else
            {
                if (e.Page is SectionScene)
                {
                    
                    var v = (SectionScene)e.Page;
                    this.IsPictureChanged = true;
                    
                    if (!v.IsChanged)
                    {
                        if (this.PictureChanged != null) PictureChanged(this, new EventArgs());
                    }
                    
                }
                else
                {
                    this.IsPictureChanged = true;
                    if (this.PictureChanged != null) PictureChanged(this, new EventArgs());
                }
            }
        }
        #endregion



    }
}
