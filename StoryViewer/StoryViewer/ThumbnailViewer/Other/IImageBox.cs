﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase;
namespace StoryViewer
{
    public interface IImageItemBox
    {
        Point MapLocation { get; }
        TextThumbImage TextItem { get;}
    }
}
