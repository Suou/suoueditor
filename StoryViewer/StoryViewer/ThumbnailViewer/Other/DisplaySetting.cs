﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using StoryViewer.ThumbnailViewer;
using SuouBase.Interface;
using StoryViewer.ThumbnailViewer.ThumbCore;
namespace StoryViewer
{
    public class DisplaySetting
    {
        public VScrollBar vScrollBar { get; set; }
        public HScrollBar hScrollBar { get; set; }
        public EditorInfo DLLFunction
        {
            get
            {
                return this.ContItem.DLLFunction;
            }
        }
        private Size _CatalogSize;
        public Size CatalogSize
        {
            get { return this._CatalogSize; }
            set 
            { 
                this._CatalogSize = value;
                this.ScrollBarReset();
            }
        }
        public ThumContViewBox Ctrl { get; set; }
        public ThumbnailViewerOperator ContItem { get; set; }
        public ImageItemBoxCollection ImageContItem { get; set; }
        public MapPointer MapMethod
        {
            get { return this.ContItem.MapOperator; }
        }

        public Size DisplaySize {
            get {
                return new Size(this.Ctrl.Width - this.vScrollBar.Width, this.Ctrl.Height - this.hScrollBar.Height); 
            } 
        }
        public int ItemInterval { get; set; }
        public int vLargeChange = 36 * 36;
        public int hLargeChange = 16 * 16;
        public int Yohaku = 100;
        public Point MapLocation
        {
            get
            {
                return new Point(hScrollBar.Value, vScrollBar.Value);
            }
            set
            {
                hScrollBar.Value = value.X;
                vScrollBar.Value = value.Y;
            }
        }
        public Rectangle MapRectangle
        {
            get
            {
                return new Rectangle(this.MapLocation, this.DisplaySize);
            }
        }
        public Point CursorLocation
        {
            get
            {
                return this.Ctrl.PointToClient(Control.MousePosition);
            }
        }
        public Point CursorMapLocation
        {
            get
            {
                return this.GetMapLocation(this.CursorLocation);
            }
        }
        public Point GetMapLocation(Point p)
        {
            return new Point(p.X + this.MapLocation.X, p.Y + this.MapLocation.Y);
        }
        public Point GetLocation(Point mapLocation)
        {
            return new Point(mapLocation.X - this.MapLocation.X, mapLocation.Y - this.MapLocation.Y);
        }

        public DisplaySetting(VScrollBar vsb,HScrollBar hsb,ThumContViewBox Ctrl)
        {
            this.vScrollBar = vsb;
            this.hScrollBar = hsb;
            this.Ctrl = Ctrl;

            this.vScrollBar.LargeChange = vLargeChange;
            this.vScrollBar.SmallChange = 64;
            this.hScrollBar.LargeChange = hLargeChange;
            this.hScrollBar.SmallChange = 16;
        }

        public void ScrollBarReset()
        {
            if (hScrollBar == null || vScrollBar == null) return;
            var height = this.CatalogSize.Height - this.DisplaySize.Height + this.hScrollBar.Height + ItemInterval + vLargeChange - 1 + Yohaku + 300;
            var width = this.CatalogSize.Width - this.DisplaySize.Width + +this.vScrollBar.Width + ItemInterval + hLargeChange - 1;
            if (this.vScrollBar.Minimum != 0) this.vScrollBar.Minimum = 0;
            if (this.hScrollBar.Minimum != 0) this.hScrollBar.Minimum = 0;

            var hTrueMax = height - this.hScrollBar.LargeChange + 1;
            var vTrueMax = height - this.vScrollBar.LargeChange + 1;
            if (vScrollBar.Value > vTrueMax && vTrueMax > 0) vScrollBar.Value = vTrueMax;
            if (hScrollBar.Value > hTrueMax && hTrueMax > 0) hScrollBar.Value = hTrueMax;

            if (height != this.vScrollBar.Maximum) this.vScrollBar.Maximum = height > 0 ? height : 0;
            if (this.hScrollBar.Maximum != width) this.hScrollBar.Maximum = width > 0 ? width : 0;
        }
        
        public void Reset2()
        {
            vScrollBar.Value = 0;
            hScrollBar.Value = 0;
        }
    }
    public enum ImageType
    {
        Drag = 1, Reverse = 2, Normal = 4, None = 8, Invalid = 16
    }
    public enum NameTextBoxType
    {
        None, Enable, HalfEnable,ReadOnly
    }
}
