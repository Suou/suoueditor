﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer
{


    public enum ThumbnailOrder
    {
        SmallTile,Thread
    }

    public enum ThumbAsyncMode
    {
        Stop, Go, WaitStop, Finish, WaitStart,OneStop,OneStart
    }

    
    public enum DrawOwner
    {
        None,Normal,Lazy
    }


    public enum ThumbnailChangeItemMode
    {
        NewSet,Add,Clear,Delete,Insert,Move
    }
    public delegate void ThumbnailChangeItemEventHandler(object sender,ThumbnailChangeItemEventArgs e);
    public class ThumbnailChangeItemEventArgs : EventArgs
    {
        public ThumbnailChangeItemMode Mode { get; set; }
        public ThumbnailChangeItemEventArgs(ThumbnailChangeItemMode mode):base()
        {
            this.Mode = mode;
        }
    }
    public class ThumbnailViewEventArgs : EventArgs
    {
        public TextThumbImage Item { get; set; }
        public int SelectedIndex { get; set; }
        public IBookPage SelectedPage
        {
            get
            {
                return this.Item.Page;
            }
        }
        public ThumbnailViewEventArgs(TextThumbImage tvi,int index)
            : base()
        {
            this.Item = tvi;
            this.SelectedIndex = index;
        }
        
    }
    public class ImageItemBoxEventArgs : EventArgs
    {
        public ImageItemBox Item { get; set; }
        public ImageItemBoxEventArgs(ImageItemBox tvi)
            : base()
        {
            this.Item = tvi;
        }
    }
    public delegate void ThumSettingEventHandler(object sender,ThumSettingEventArgs e);
    public class ThumSettingEventArgs:EventArgs
    {
        public bool IsPictureDelete { get; private set; }

        public ThumSettingEventArgs(bool isPictureDelete)
        {
            this.IsPictureDelete = isPictureDelete;
        }
    }

    public delegate void ThumbnailViewEventHandler(object sender, ThumbnailViewEventArgs e);
    public delegate void ImageItemBoxEventHandler(object sender, EventArgs e);
}
