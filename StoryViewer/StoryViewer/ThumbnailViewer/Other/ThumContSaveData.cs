﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer
{

    [Serializable]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class ThumbnailView_SaveData
    {
        public ThumbnailView_SaveData()
        {
        }
        private bool InvokeEventFlag = false;
        private bool IsPictureDeleteFlag = false;

        public const long MAX_TOTAL_IMAGE_SIZE = 10000000;
        private long _MaxTotalImageSize = MAX_TOTAL_IMAGE_SIZE;

        private void InvokeSettingChanged(bool isPictureDelete)
        {
            if (!this.IsPictureDeleteFlag) this.IsPictureDeleteFlag = isPictureDelete;
            if (this.InvokeEventFlag)
            {
                if (SettingChanged != null) SettingChanged(this, new ThumSettingEventArgs(this.IsPictureDeleteFlag));
                this.IsPictureDeleteFlag = false;
            }
        }
        public long MaxTotalImageSize
        {
            get 
            {
                if (this._MaxTotalImageSize < MAX_TOTAL_IMAGE_SIZE) this._MaxTotalImageSize = MAX_TOTAL_IMAGE_SIZE;
                return _MaxTotalImageSize; 
            }
            //set { _MaxTotalImageSize = value; }
        }

        private bool _SmallerRawView = true;
        public bool SmallerRawView
        {
            get { return this._SmallerRawView; }
            set
            {
                this._SmallerRawView = value;
                this.InvokeSettingChanged(false);
            }
        }


        private int _OrderMode = (int)ThumbnailOrder.SmallTile;
        public ThumbnailOrder OrderMode
        {
            get { return (ThumbnailOrder)this._OrderMode; }
            set
            {
                this._OrderMode = (int)value;
                this.InvokeSettingChanged(false);
                //if (ItemViewChanged != null) ItemViewChanged(this, new EventArgs());
            }
        }
        private SuouBookPageModel _Model = new SuouBookPageModel();
        public SuouBookPageModel Model
        {
            get { return this._Model; }
            set
            {
                if (value == null) return;
                this._Model = value;
                this.InvokeSettingChanged(true); 
            }
        }

        private int _ItemInterval = 5;
        public int ItemInterval
        {
            get { return this._ItemInterval; }
            set
            {
                this._ItemInterval = value;
                this.InvokeSettingChanged(false);
            }
        }

        private Size _ItemSize = new Size(100, 100);
        public Size ItemSize
        {
            get { return _ItemSize; }
            set
            {
                _ItemSize = value;
                this.InvokeEventFlag = false;
                ViewMode = ThumbnailMode.Size;
                this.InvokeEventFlag = true;
                this.InvokeSettingChanged(true);
            }
        }
        private bool _IsLoose = false;
        public bool IsLoose
        {
            get { return this._IsLoose; }
            set
            {
                this._IsLoose = value;
                this.InvokeSettingChanged(true);
            }
        }
        private Size _MinItemSize = new Size(16, 16);
        public Size MinItemSize
        {
            get { return _MinItemSize; }
            set
            {
                _MinItemSize = value;
                this.InvokeSettingChanged(true);
            }
        }
        private float _Percent = (float)0.5;
        public float Percent
        {
            get
            {
                return _Percent;
            }
            set
            {
                _Percent = value;
                this.InvokeEventFlag = false;
                ViewMode = ThumbnailMode.Percent;
                this.InvokeEventFlag = true;
                this.InvokeSettingChanged(true);
            }
        }
        private bool _PermitCategory = true;
        public bool PermitCategory
        {
            get { return this._PermitCategory; }
            set
            {
                this._PermitCategory = value;
                this.InvokeSettingChanged(true);
            }
        }
        private int _ViewMode = (int)ThumbnailMode.Size;
        public ThumbnailMode ViewMode
        {
            get
            {
                return (ThumbnailMode)this._ViewMode;
            }
            set
            {
                this._ViewMode = (int)value;
                this.InvokeSettingChanged(true);
            }
        }

        [field: NonSerialized]
        public event ThumSettingEventHandler SettingChanged;
    }
}
