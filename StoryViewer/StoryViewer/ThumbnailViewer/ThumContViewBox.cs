﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using StoryViewer.ThumbnailViewer;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class ThumContViewBox : UserControl
    {
        #region Property
        public event ThumbnailViewEventHandler ItemClicked;
        public event ThumbnailViewEventHandler ItemDragStarting;
        public event ThumbnailViewEventHandler ItemSelected;
        public event ThumbnailViewEventHandler ItemMouseDown;
        public event EventHandler SavedataChanged;
        public bool ManualDragDo { get; set; }
        public EventHandler Redraw;
        public EventHandler ItemViewChanged;
        public event ThumbnailChangeItemEventHandler ItemDataChanged;
        public ContextMenuStrip ItemContext { get; set; }
        public ContextMenuStrip ShiftItemContext { get; set; }
        public ContextMenuStrip ControlContext { get; set; }
        public NameTextBoxType EnableNameTextBox { get; set; }
        public bool IsDrag
        {
            get {
                if (this.Operator.DragItem == null) return false;
                return this.Operator.DragItem.IsDrag; }
        }
        /*
        public TextThumbImage[] RowItems
        {
            get
            {
                return this.Operator.RowItems;
            }
        }
        */
        public bool IsTransparentDrag { get; set; }
        public ImageItemBox GetImageBoxItem(int index)
        {
            if (index == -1) return null;
            return this.DisplaySet.ImageContItem[index];
        }
        
        
        public void CancelSelectedImageBoxItems()
        {
            if (this.DisplaySet.MapMethod.LastSelectedImageBoxItems != null)
            {
                foreach (var v in this.DisplaySet.MapMethod.LastSelectedImageBoxItems)
                {
                    v.IsSelected = false;
                }
            }
            this.DisplaySet.MapMethod.LastSelectedImageBoxItems = new ImageItemBox[] { };
        }
        public bool CanItemDrag { get; set; }
        public bool CanItemMove { get; set; }
        public ImageItemBoxType BoxType
        {
            get { return this.Operator.BoxType; }
            set { this.Operator.BoxType = value; }
        }
        private object BufLockObject=new object();
        public bool IsMouseMoveFocus { get; set; }
        public ThumbnailViewerOperator Operator;
        private bool IsSelected { get; set; }

        [ReadOnly(true)]
        public ThumbnailView_SaveData SaveData
        {
            get
            {
                return Operator.SaveData;
            }
            set
            {
                this.Operator.SaveData = value;
            }
        }       

        public void ClearImage()
        {
            this.Operator.ClearImage();
        }
        #endregion
        #region ItemProperty
        public int ItemInterval
        {
            get { return this.Operator.SaveData.ItemInterval; }
            set { this.Operator.SaveData.ItemInterval = value; }
        }
        public bool PermitCategory
        {
            get { return this.Operator.SaveData.PermitCategory; }
            set { this.Operator.SaveData.PermitCategory = value; }
        }
        public Size ItemSize
        {
            get { return this.Operator.SaveData.ItemSize; }
            set { this.Operator.SaveData.ItemSize = value; }
        }
        public Size MinItemSize
        {
            get { return this.Operator.SaveData.MinItemSize; }
            set { this.Operator.SaveData.MinItemSize = value; }
        }
        public float Percent
        {
            get { return this.Operator.SaveData.Percent; }
            set { this.Operator.SaveData.Percent = value; }
        }
        public ThumbnailMode ViewMode
        {
            get { return this.Operator.SaveData.ViewMode; }
            set { this.Operator.SaveData.ViewMode = value; }
        }
        public ThumbnailOrder OrderMode
        {
            get { return this.Operator.SaveData.OrderMode; }
            set { this.Operator.SaveData.OrderMode = value; }
        }
        public bool IsLoose
        {
            get { return this.Operator.SaveData.IsLoose; }
            set { this.Operator.SaveData.IsLoose = value; }
        }
        #endregion
        #region Property2

        public event EventHandler ImageBoxMouseEnter;
        private DisplaySetting DisplaySet = null;
        #endregion
        #region Constructor
        /// <summary>
        /// O(n)操作
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ThumContViewBox()
        {
            InitializeComponent();
            SetStyle(ControlStyles.StandardClick |ControlStyles.StandardDoubleClick, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            DisplaySet = new DisplaySetting(this.vScrollBar1, this.hScrollBar1, this);

            this.Operator = new ThumbnailViewerOperator(this, DisplaySet); 
            this.Operator.SaveData = (new ThumbnailView_SaveData());
            this.Operator.SavedataChanged += (a, b) => { if (this.SavedataChanged != null)this.SavedataChanged(a, b); };

            this.MouseDown += this.DisplaySet.MapMethod.SelectRectangle.Ctrl_MouseDown;
            this.MouseMove += this.DisplaySet.MapMethod.SelectRectangle.Ctrl_MouseMove;
            this.MouseUp += this.DisplaySet.MapMethod.SelectRectangle.Ctrl_MouseUp;
            this.Operator.ImageBoxMouseEnter += this.ImageItemBox_MouseEnter;
        }
        #endregion
        #region PrivateOperate
        public TextThumbImage GetItem(int index)
        {
            return this.DisplaySet.MapMethod.GetItem(index);
        }
        public void RefreshItems()
        {
            
            this.DisplaySet.MapMethod.SetMapLocation();
        }
        public void ShowRawToolTip()
        {
            var item = this.DisplaySet.MapMethod.SelectedImageBoxItem;
            if (item != null)
            {
                item.ShowRawToolTip();
            }
        }
        #endregion
        #region Operate
        public void FinishDrag()
        {
            if (this.Operator.DragItem.IsDrag)
            {
                this.Operator.DragItem.DragEnd();
            }
        }
        public void DeleteSelectedItem()
        {
            this.Operator.DeleteSelectedItem(this.DisplaySet.MapMethod.LastSelectedImageBoxItems);
            this.DisplaySet.MapMethod.SelectRectangle.Cancel();
        }
        
        public int ItemCount
        {
            get
            {
                return this.Operator.ItemCount;
            }

        }
        public void DoDrag()
        {
            if (this.Operator.DragItem != null)
            {
                if (ItemDragStarting != null) ItemDragStarting(this, new ThumbnailViewEventArgs(this.Operator.DragItem.Item.TextItem, this.DisplaySet.MapMethod.SelectedItemIndex));
                if (ManualDragDo)
                {
                    
                }
                else
                {
                    if (this.IsTransparentDrag)
                    {
                        this.DisplaySet.Ctrl.ParentForm.TransparencyKey = this.DisplaySet.Ctrl.BackColor;
                        this.DisplaySet.ImageContItem.ForEach((v) => v.Visible = false);
                        
                    }
                    this.Operator.DragItem.DragDroping();
                    var a = this.Operator.DragItem;
                    var b = a.Item;
                    this.DoDragDrop(b.TextItem, DragDropEffects.All);

                }
            }
        }
        #endregion
        #region Point

        
        
        #endregion
        #region Invoke
        public void InvokeDataChenge(ThumbnailChangeItemMode mode)
        {
            if (this.ItemDataChanged != null) ItemDataChanged(this, new ThumbnailChangeItemEventArgs(mode));
        }
        public void InvokeMouseMove(MouseEventArgs e)
        {
            this.OnMouseMove(e);
        }
        #endregion
        #region MouseEvent

        private void ImageItemBox_MouseEnter(object sender, EventArgs e)
        {
            if (this.ImageBoxMouseEnter != null) this.ImageBoxMouseEnter(this, e);
        }
        private void MouseMoveMethod(MouseEventArgs e)
        {
            //何のためのフォーカスだ？
            this.DisplaySet.MapMethod.SetNearItemIndex();
            if (IsMouseMoveFocus) this.Focus();
            if (!this.Operator.DragItem.IsDragStart) this.Operator.DrawNearBoxPointerLine(this.CreateGraphics());

            //MouseMoveCheck(e.Location);
            var rec = new Rectangle(Point.Empty, this.Size);
            var p = this.PointToClient(Control.MousePosition);
            if (!rec.Contains(p))
            {
                ThumContViewBox_MouseLeave(null, null);
            }
            this.DisplaySet.MapMethod.SetSelectedImageBoxItems();
            this.DisplaySet.MapMethod.LastSelectedIndex = this.DisplaySet.MapMethod.SelectedItemIndex;


            if (!this.Operator.DragItem.IsDragStart && this.MouseDownItem != null && e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                var MousePoint = this.PointToClient(Control.MousePosition);
                this.Operator.DragItem.DragStart(this.MouseDownItem, MousePoint, DragMode.Move);

            }
            if (this.Operator.DragItem.IsDragStart && this.MouseDownItem != null && e.Button != System.Windows.Forms.MouseButtons.Left)
            {
                this.Operator.DragItem.DragEnd();
                this.MouseDownItem = null;
            }
        }
        private void ThumContViewBox_MouseMove(object sender, MouseEventArgs e)
        {
            this.MouseMoveMethod(e);

        }
        private void ThumContViewBox_MouseLeave(object sender, EventArgs e)
        {
            this.Operator.ToolTipHideAll();

        }
        private void ThumContViewBox_MouseClick(object sender, MouseEventArgs e)
        {
            var mp = this.DisplaySet.MapMethod.SelectedImageBoxItem;


            if (ItemClicked != null && e.Button.HasFlag(MouseButtons.Left))
            {
                if (mp != null) ItemClicked(this, new ThumbnailViewEventArgs(mp.TextItem, this.DisplaySet.MapMethod.SelectedItemIndex));
            }
            if (e.Button.HasFlag(MouseButtons.Right))
            {
                if (mp != null)
                {
                    //ToolTipShow(e.Location);
                }
                else
                {

                    if (ControlContext != null)
                    {
                        ControlContext.Show(Cursor.Position);
                    }
                }
            }

        }
        private void ThumContViewBox_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            if (e.Button.HasFlag(MouseButtons.Left))
            {
            }
            else if (e.Button.HasFlag(MouseButtons.Right))
            {
                //ToolTipHide();
                if (ItemContext != null) ItemContext.Show(Cursor.Position);
            }
        }

        private ImageItemBox _MouseDownItem = null;
        public ImageItemBox MouseDownItem
        {
            get { return this._MouseDownItem; }
            set 
            {
                this._MouseDownItem = value;
            }
        }

        private void ThumContViewBox_MouseDown(object sender, MouseEventArgs e)
        {
            this.vScrollBar1.Focus();
        }
        
        private void ThumContViewBox_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.Operator.DragItem.IsDragStart)
            {
                this.Operator.DragItem.DragEnd();
                this.MouseDownItem = null;
            }
        }
        #endregion
        #region ScrollEvent
        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            this.DisplaySet.MapMethod.SetLocation();

            //Draw(this.CreateGraphics());
        }
        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            this.DisplaySet.MapMethod.SetLocation();

            //Draw(this.CreateGraphics());
        }
        #endregion
        #region ImageItemBoxEvent
        public void ImageItemBox_DragOver(object sender, DragEventArgs e)
        {
            this.OnDragOver(e);
        }
        public void ImageItemBox_DragDrop(object sender, DragEventArgs e)
        {
            this.OnDragDrop(e);
        }
        public void SetItemBoxOpen(ImageItemBox tvi)
        {
            tvi.Open = !tvi.Open;
            this.DisplaySet.MapMethod.SetMapLocation();
            this.DisplaySet.MapMethod.SetLocation();
        }
        public void OpenAll()
        {
            this.Operator.OpenAll();
        }
        public void CloseAll()
        {
            this.Operator.CloseAll();
        }
        public PageJumpBox CreatePageJumpBox()
        {
            var ctrl = new PageJumpBox(this);
            ToolStripControlHost v = new ToolStripControlHost(ctrl);
            return ctrl;
        }

        public void ImageItemBox_MouseClick(object sender, MouseEventArgs e)
        {

            var rightDown = (Control.MouseButtons & System.Windows.Forms.MouseButtons.Right) == System.Windows.Forms.MouseButtons.Right;
            var leftClick = (e.Button & System.Windows.Forms.MouseButtons.Left) == System.Windows.Forms.MouseButtons.Left;
            if (this.DisplaySet.MapMethod.SelectedItem != null)
            {
                if (this.DisplaySet.MapMethod.SelectedItem.IsCategory && this.BoxType == ImageItemBoxType.Normal)
                {
                    this.SetItemBoxOpen(this.DisplaySet.MapMethod.SelectedImageBoxItem);
                }
                else
                {

                    if (ItemClicked != null && leftClick && !rightDown)
                    {
                        ItemClicked(this, new ThumbnailViewEventArgs(this.DisplaySet.MapMethod.SelectedItem, this.DisplaySet.MapMethod.SelectedItemIndex));
                    }
                }
            }

        }
        public void ImageItemBox_MouseLeave(object sender, EventArgs e)
        {
            if (this.DisplaySet.MapMethod.SelectRectangle.IsEnable)
            {
            }
            else
            {
                if (this.DisplaySet.MapMethod.SelectedImageBoxItem != null)
                {
                    this.DisplaySet.MapMethod.SelectedImageBoxItem.CheckMouse();
                }
            }
        }
        public void ImageItemBox_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.DisplaySet.MapMethod.SelectedItem != null)
            {
                if (this.ItemMouseDown != null) ItemMouseDown(this, new ThumbnailViewEventArgs(this.DisplaySet.MapMethod.SelectedItem, this.DisplaySet.MapMethod.SelectedItemIndex));
            }
        }
        #endregion
        #region OtherEvent
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Visible && this.ParentForm != null)
            {
                this.Operator.CheckChangePicture();
            }
        }
        private void ThumContViewBox_ControlAdded(object sender, ControlEventArgs e)
        {

            //memoryreak
            e.Control.MouseMove += this.ThumContViewBox_MouseMove;
        }
        private void ThumContViewBox_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if ((e.KeyState & 1) != 1)
            {
                this.Operator.DragItem.DragEnd();
            }

            if (e.Action == DragAction.Drop)
            {
                var v = this.Operator.DragItem;
                if (v != null && v.Item != null)
                {

                    var thume = new ThumbnailViewEventArgs(v.Item.TextItem, this.DisplaySet.MapMethod.SelectedItemIndex);

                    if (ItemSelected != null) ItemSelected(this, thume);
                }
            }

        }
        private void ThumContViewBox_Paint(object sender, PaintEventArgs e)
        {
            this.Operator.DrawNearBoxPointerLine(e.Graphics);
            //this.SelectRectangle.DrawRectangle(e.Graphics);
        }
        private void ThumContViewBox_VisibleChanged(object sender, EventArgs e)
        {

            if (this.Parent != null && this.ParentForm != null && this.Visible)
            {
                this.RefreshItems();
            }
        }
        private void ThumContViewBox_Resize(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (!this.Visible) return;
            DisplaySet.ScrollBarReset();
            this.Operator.MapResise();
        }
        #endregion

        private void ThumContViewBox_ControlRemoved(object sender, ControlEventArgs e)
        {
        }

        private void ThumContViewBox_KeyDown(object sender, KeyEventArgs e)
        {
        }

        private void ThumContViewBox_Load(object sender, EventArgs e)
        {
            if (this.DesignMode) return;


            
        }
    }
    public enum ImageItemBoxType
    {
        Normal,Story
    }

}
