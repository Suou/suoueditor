﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase;
using SuouBase.Plugin;
using SuouBase.Interface;
using SuouBase.Book;
namespace StoryViewer
{
    public partial class ImageItemBox : UserControl, IImageItemBox, IJoinTop
    {   
        #region EventProperty
        public event MouseEventHandler PictureBoxMouseDowned;
        public event EventHandler ItemIntervalChanged;
        public event EventHandler ToolTipShowing;
        public event EventHandler PictureChanged;
        public event EventHandler SpecialSizeChanged;
        public event EventHandler ImageDisposed;
        public event EventHandler ContentChanged;
        protected event EventHandler NumberChanged;
        public event ImageItemBoxEventHandler ImageCreated;
        #endregion
        #region Property

        private bool _VisibleFlag = true;
        [ReadOnly(true)]
        [Browsable(false)]
        public bool VisibleFlag
        {
            get { return this._VisibleFlag; }
            set
            {
                this._VisibleFlag = value;
                //2013/3/28                    
                this.Visible = value;


            }
        }
        [ReadOnly(true)]
        [Browsable(false)]
        public Point MapLocation{get;set;}
        public int YSpace { get; set; }
        private TextThumbImage _TextItem = null;
        public TextThumbImage TextItem
        {
            get { return this._TextItem; }
            set
            {
                this._TextItem = value;
                this.aaToolTip1.TextItem = value.Page.TextImagize();
            }
        }
        public TextBox NameTextBox { get; protected set; }
        private int _ItemInteval = 5;
        public virtual int ItemInterval
        {
            get { return _ItemInteval; }
            set
            {
                _ItemInteval = value;
                if (ItemIntervalChanged != null) ItemIntervalChanged(this, new EventArgs());
            }
        }
        private Bitmap PictureImage = null;
        public Point BottomLocation
        {
            get
            {
                return new Point(this.Location.X,this.Location.Y+this.Height);
            }
        }
        private int _Number = 0;
        public int Number
        {
            get { return _Number; }
            set 
            { 
                _Number = value;
                this.InvokeNumberChanged();
            }
        }
        public Rectangle Rec
        {
            get
            {
                Rectangle r = new Rectangle(this.MapLocation, this.Size);
                return r;
            }
        }
        protected Control ParentControl { get; set; }
        public ContextMenuStrip RightClickContext { get; set; }
        public ContextMenuStrip ShiftRightClickContext { get; set; }
        public long UsingMemory
        {
            get
            {
                if (this.PictureImage != null)
                {
                    return this.PictureImage.Width * this.PictureImage.Height;
                }
                return 0;
            }
        }
        private bool _IsSelected = false;
        public bool IsSelected
        {
            get { return _IsSelected; }
            set 
            {
                if (_IsSelected != value)
                {
                    _IsSelected = value;
                    this.LoadPicture();
                }
            }
        }
        private bool _IsDrag = false;
        public bool IsDrag 
        {
            get { return _IsDrag; }
            set
            {
                _IsDrag = value;
                this.LoadPicture();
                //LoadPicture(this.pictureBox1.CreateGraphics());
            }
        }
        protected DisplaySetting DisplaySet { get; set; }        
        public EditorInfo DLLFunction
        {
            get
            {
                return this.DisplaySet.DLLFunction;
            }
        }
        
        #endregion
        #region Property2
        [ReadOnly(true)]
        [Browsable(false)]
        public bool Open { get; set; }
        public virtual bool JoinTop
        {
            get
            {
                return false;
            }
        }
        public bool IsPictureChanged { get; private set; }
        private NameTextBoxType _EnableNameTextBox = NameTextBoxType.None;
        public NameTextBoxType EnableNameTextBox
        {
            get 
            {
                return this._EnableNameTextBox;
            }
            set
            {
                this._EnableNameTextBox = value;
                this.CheckNameTextBox();
            }
        }
        private void CheckNameTextBox()
        {
            
            if (this.EnableNameTextBox == NameTextBoxType.Enable || this.EnableNameTextBox == NameTextBoxType.ReadOnly)
            {
                if(this.NameTextBox == null)this.CreateNemeTextBox();
            }
            else if (this.EnableNameTextBox == NameTextBoxType.None)
            {
                this.DisposeNameTextBox();
            }
            else
            {
                if (this.TextItem.Page.Name.Length == 0)
                {
                    this.DisposeNameTextBox();
                }
                else
                {
                    this.CreateNemeTextBox();
                }
            }
        }
        protected virtual void CreateNemeTextBox()
        {
            var b = this.EnableNameTextBox == NameTextBoxType.ReadOnly;
            this.NameTextBox = new TextBox() { BackColor = Color.LawnGreen, Dock = DockStyle.Top,ReadOnly = b };
            this.NameTextBox.TextChanged += this.textBox1_TextChanged;
            this.Controls.Add(this.NameTextBox);
            this.NameLoad();
            //this.Formize();
        }
        protected virtual void DisposeNameTextBox()
        {
            if (this.NameTextBox != null)
            {
                this.Controls.Remove(this.NameTextBox);
                this.NameTextBox.TextChanged -= this.textBox1_TextChanged;
                this.NameTextBox = null;
            }
        }
        #endregion
        #region Constructor
        public ImageItemBox()
        {
            InitializeComponent();
            this.Visible = false;
            SetStyle(ControlStyles.StandardClick | ControlStyles.StandardDoubleClick, true);
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            this.pictureBox1.MouseWheel += this.pictureBox1_MouseWheel;
        }
        public ImageItemBox(TextThumbImage text,DisplaySetting displaySet):this()
        {
            this.TextItem = text;
            this.DisplaySet = displaySet;
            this._TextItem.PropertyChanged += this.TextItem_Changed;
            //this.ItemSize = text.GetSize(this.YSpace);
            this.NameLoad();
            Formize();

        }

        public virtual void SuperDispose()
        {
            this.pictureBox1.MouseWheel -= this.pictureBox1_MouseWheel;
            this.TextItem.PropertyChanged -= this.TextItem_Changed;
            this.aaToolTip1.Dispose();
            this.DisposeNameTextBox();
            this._TextItem = null;
            this.DisplaySet = null;

        }
        #endregion
        #region Method


        public void CheckMouse()
        {
            var p = this.PointToClient(Control.MousePosition);
            var rec = new Rectangle(Point.Empty, this.Size);
            var b = !rec.Contains(p);
            if (b)
            {
                this.IsSelected = false;
            }
        }
        public void HideToolTip()
        {
            this.aaToolTip1.ToolTipHide();
        }
        protected void InvokeNumberChanged()
        {
            if (NumberChanged != null) NumberChanged(this, new EventArgs());
        }
        public void ShowRawToolTip()
        {
            if (ToolTipShowing != null) ToolTipShowing(this, new EventArgs());
            this.aaToolTip1.ToolTipShow(BSystem.RectCornerDirection.TopRightCorner);
        }
        

        public Color _PictureBackColor = Color.White;
        public Color PictureBackColor
        {
            get { return this._PictureBackColor; }
            set
            {
                if (value != this._PictureBackColor)
                {
                    this._PictureBackColor = value;
                    
                    this.LoadPicture();
                }
            }
        }
        private void Draw(Graphics g)
        {
            var color = this.PictureBackColor;
            if (this.TextItem.IsCategory)
            {
                color = this.Open ? Color.Violet : Color.Aquamarine;
            }
            if (this.TextItem != null)
            {
                this.TextItem.DrawText(g, Color.Black, color);
                //this.PictureImage = BSystem.Drawing.TurboFillter2.Create1bppImage(this.PictureImage);
            }
        }
        private void ReverseDraw(Graphics g)
        {
            if (this.TextItem != null)
            {
                this.TextItem.DrawText(g, Color.White, Color.Black);
            }
        }
        private void DragDraw(Graphics g)
        {
            if (this.TextItem != null)
            {
                this.TextItem.DrawText(g, Color.Black, Color.LightGreen);
            }
        }
        //private object DrawLocker = new object();
        private ImageType NowPictureType = ImageType.None;
        private ImageType GetPictureType
        {
            get
            {
                if (this.IsDrag)
                {
                    return ImageType.Drag;
                }
                else if (this.IsSelected)
                {
                    return ImageType.Reverse;
                }
                else if (this.PictureBackColor == Color.Red)
                {
                    return ImageType.Invalid;
                }
                else
                {
                    return ImageType.Normal;
                }
            }
        }
        private void LoadPicture(Graphics g)
        {
            //this.IsReverse = false;
            BufferedGraphicsContext currentContext;
            BufferedGraphics myBuffer;
            currentContext = BufferedGraphicsManager.Current;
            myBuffer = currentContext.Allocate(g, this.TextItem.PictureDisplayRectangle);

            {
                if (this.IsDrag)
                {
                    this.DragDraw(myBuffer.Graphics);
                    this.NowPictureType = ImageType.Drag;
                }
                else if (this.IsSelected)
                {
                    this.ReverseDraw(myBuffer.Graphics);
                    this.NowPictureType = ImageType.Reverse;
                }
                else if (this.GetPictureType == ImageType.Invalid)
                {
                    this.Draw(myBuffer.Graphics);
                    this.NowPictureType = ImageType.Invalid;
                }
                else
                {
                    this.Draw(myBuffer.Graphics);
                    this.NowPictureType = ImageType.Normal;
                }
            }
            myBuffer.Render();
            myBuffer.Graphics.Dispose();
        }
        public void CheckReverse()
        {
            this.IsSelected = false;
        }

        protected void NameLoad()
        {
            if (this.TextItem != null && this.NameTextBox != null)
            {
                this.NameTextBox.Text = this.TextItem.Page.Name;
            }
        }

        public virtual void Formize()
        {

            if (this.TextItem != null)
            {
                
                var h = (this.NameTextBox != null && this.EnableNameTextBox != NameTextBoxType.None) ? this.NameTextBox.Height : 0;
                var size = new Size(this.TextItem.PictureSize.Width, this.TextItem.PictureSize.Height+h);
                this.Size = size;
                this.pictureBox1.Size = this.TextItem.PictureSize;
                this.pictureBox1.Dock = DockStyle.Fill;
            }
        }
        public void ClearImage()
        {
            this.NowPictureType = ImageType.None;
            var pic = this.PictureImage;
            if (pic != null)
            {
                this.PictureImage = null;
                this.pictureBox1.Image = null;
                pic.Dispose();
                if (this.ImageDisposed != null) ImageDisposed(this.pictureBox1.Size, new EventArgs());
            }

            this.IsPictureChanged = false;
        }
        public void RefreshImage()
        {
            var v = this.TextItem.Page as SectionScene;
            if (v != null)
            {
                v.RefreshMerge();
            }

            this.ClearImage();
            this.LoadPicture();
        }
        public virtual void RefreshFormize()
        {
        }
        #endregion

    }
}
