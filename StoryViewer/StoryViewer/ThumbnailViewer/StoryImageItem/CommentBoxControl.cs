﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using BSystem;
using BSystem.Drawing;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class CommentBoxControl : SuouBase.Controls.ListControlItem
    {
        /// <summary>
        /// もの凄いやっつけ感
        /// </summary>
        public static DelayLockActionWindowsTimer RefreshTimer { get; private set; }
        #region Property
        private int ImageBoxWidth { get; set; }
        public int MinTextBoxWidth
        {
            get
            {
                return this.ImageBoxWidth - this.button1.Width - 30;
            }
        }

        public bool IsTextEditing { get; private set; }
        public SectionScenePart PagePart { get; set; }
        private SectionScene ParentPage { get; set; }
        #endregion
        #region Constructor
        public CommentBoxControl()
        {
            InitializeComponent();
            
        }
        public CommentBoxControl(SectionScenePart pp,SectionScene page,int imageBoxWidth):this()
        {
            this.ParentPage = page;
            this.PagePart = pp;
            this.ImageBoxWidth = imageBoxWidth;
            if (pp.SText == null) throw new Exception();
            this.textBox1.LostFocus += (sender, args) => this.SuperRefresh();
            this.PagePart.ContentChanged += this.PageText_Changed;

            this.IsTextEditing = true;
            {
                this.textBox1.Text = this.PagePart.SText.Text;
            }
            this.IsTextEditing = false;
            this.DoOptimalSize();
        }
        public static CommentBoxControl CreateNew(SectionScenePart backGroundPage, int count, SpaceLineSet lineset, SectionScene page, int imageWidth)
        {
            //var r = SectionScenePart.CreateSerifuPart(count, lineset);
            var r = page.AddSerihuPart("セリフ" + count,"");
            var e = new CommentBoxControl(r, page, imageWidth);
            /*
            var x = backGroundPage.SText.Width;
            if (x > 800) x = 800;
            var y = backGroundPage.SText.Count / 2;
            r.EditLocation = new EditPoint(x, y);
            */
            return e;
        }

        #endregion
        #region Method

        public void SuperRefresh()
        {
            if (RefreshTimer != null) this.TextBox_TextChangedAction();
            //this.RefreshImage();
        }
        private void DoOptimalSize()
        {
            var lines = BSystem.Text.StringExtensions.LineSprit(this.textBox1.Text);
            var f = this.PagePart.SText.Font;
            var textBoxSize = FontSizeDictionary.TextSize(this.textBox1.Font, lines.ToList(), SuouEditorConst.AAFontYInterval);
            textBoxSize.Width = System.Math.Max(TruncateHundred(textBoxSize.Width+30), this.MinTextBoxWidth);
            textBoxSize = new Size(textBoxSize.Width, textBoxSize.Height + 30);
            var controsize = new Size(textBoxSize.Width + this.button1.Width, textBoxSize.Height);
            if (this.textBox1.Size != textBoxSize)
            {
                this.textBox1.Size = textBoxSize;
                this.Size = controsize;
                this.button1.Location = new Point(textBoxSize.Width, this.textBox1.Location.Y);
            }

        }
        private void TextBox_TextChangedAction()
        {
            if (!IsTextEditing)
            {
                IsTextEditing = true;
                this.PagePart.SText = new SuouText(this.textBox1.Text);
                IsTextEditing = false;
            }
            RefreshTimer = null;
        }
        private int TruncateHundred(int n)
        {
            var k = (((n - 1) / 100) + 1) * 100;
            if (k == 0) k = 100;
            return k;
        }
        #endregion
        #region Event
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.DoOptimalSize();
            if (!IsTextEditing)
            {
                if (RefreshTimer == null)
                {
                    RefreshTimer = new DelayLockActionWindowsTimer(() => this.TextBox_TextChangedAction(), 1000);
                    
                }
                RefreshTimer.Do();
            }
        }
        private void PageText_Changed(object sender, SectionBookPagePartContentChangeEventArgs e)
        {
            var sw = System.Diagnostics.Stopwatch.StartNew();
            if (e.Type == MergeContentChangeType.TextDestructive && !this.IsTextEditing)
            {
                this.IsTextEditing = true;
                this.textBox1.Text = BSystem.Text.StringExtensions.JoinString(PagePart.SText.ToListString().ToArray());
                this.IsTextEditing = false;
            }
            sw.Stop();
            System.Console.WriteLine($"PageText_Changed: {sw.ElapsedMilliseconds}");
        }        
        private void button1_Click(object sender, EventArgs e)
        {
            this.ParentPage.Parts.Remove(this.PagePart);
        }
        #endregion
        public void MyDispose()
        {
            this.textBox1.TextChanged -= this.textBox1_TextChanged;
            this.PagePart.ContentChanged -= this.PageText_Changed;
        }

    }
}
