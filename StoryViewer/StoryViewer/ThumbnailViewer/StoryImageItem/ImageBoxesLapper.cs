﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using SuouBase;
using System.Drawing;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Book;
namespace StoryViewer.ThumbnailViewer.StoryImageItem
{
    public class ImageItemBoxesLapper
    {
        ImageItemBox[] Items { get; set; }
        //private SuouBookPageModel Model { get; set; }
        public IBookPageCluster ClusterItem { get; private set; }
        public bool IsInvalid
        {
            get
            {
                return this.ClusterItem.IsInvalid;
            }
        }
        public void CheckInvalid()
        {
            if (this.IsInvalid)
            {
                this.Items.ForEach((v) => v.PictureBackColor = Color.Red);
            }
            else
            {
                this.Items.ForEach((v) => v.PictureBackColor = Color.White);
            }
        }
        public IEnumerable<IBookPage> Pages
        {
            get
            {
                return this.Items.Select((v) => v.TextItem.Page);
            }
        }
        public ImageItemBoxesLapper(IEnumerable<ImageItemBox> item,SuouBookPageModel model)
        {
            this.Items = item.ToArray();
            this.ClusterItem = new IBookPageCluster(item.Select((v) => v.TextItem.Page),model);
        }
        public static ImageItemBoxesLapper GetCluster(IList<ImageItemBox> ims,int index,SuouBookPageModel model)
        {
            List<ImageItemBox> r = new List<ImageItemBox>();
            int n = index;
            int m = index+1;
            while (n >= 0)
            {
                if (!ims[n].JoinTop) break;
                n--;
            }
            while (m < ims.Count)
            {
                if (!ims[m].JoinTop) break;
                m++;
            }
            for (int i = n; i < m; i++)
            {
                if(i>=0 && i < ims.Count)r.Add(ims[i]);
            }
            return new ImageItemBoxesLapper(r,model);
        }

        public static IEnumerable<ImageItemBoxesLapper> GetClusters(IEnumerable<ImageItemBox> ims, SuouBookPageModel model)
        {

            List<ImageItemBox[]> r = new List<ImageItemBox[]>();
            var tmp = new List<ImageItemBox>();
            foreach (var v in ims)
            {
                if (v.JoinTop)
                {
                    tmp.Add(v);
                }
                else
                {
                    if (tmp.Count != 0) r.Add(tmp.ToArray());
                    tmp.Clear();
                    tmp.Add(v);
                }
            }
            r.Add(tmp.ToArray());
            return r.Select((p) => new ImageItemBoxesLapper(p,model));
        }
    }
}
