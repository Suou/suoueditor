﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Drawing;
using SuouBase;
using SuouBase.Book;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public class StoryImageItemBox : ImageItemBox
    {
        #region Property
        private System.Windows.Forms.Label label1;
        private NumericUpDown numericUpDown1;
        private Panel panel1;
        //private Panel panel2;
        private int _RealPageNumber = 0;
        private Button button1;
        private bool NeedReOrder { get; set; }
        private bool IsPageChanged { get; set; }
        public int RealPageNumber
        {
            get { return this._RealPageNumber; }
            set
            {
                this._RealPageNumber = value;
                this.InvokeNumberChanged();
            }
        }
        public int StockPages
        {
            get
            {
                return 0;
                //return this.DisplaySet.ImageContItem.GetContainPages(this);
            }
        }
        //public Size DefaultSize = new Size(500, 100);
        private Button JoinUpButton;
        private ThumbnailViewer.StoryImageItem.CommentBoxListControl commentBoxListControl1;
        private Button button2;
    
        public override int ItemInterval
        {
            get
            {
                return ModifyNumericValue;
            }
            set
            {
                base.ItemInterval = value;
            }
        }
        public SectionScene BookPage { get; set; }
        public override bool JoinTop
        {
            get
            {
                return this.BookPage.JoinTop;
            }
        }

        private Size VirtualPictureSize;
        #endregion
        #region Constructor
        public StoryImageItemBox():base()
        {
        }
        private bool IsInitialized = false;
        public StoryImageItemBox(TextThumbImage ev,DisplaySetting displaySet)
            : base(ev,displaySet)
        {
            InitializeComponent();
            this.IsInitialized = true;
            this.button1.KeyDown += this.StoryImageItemBox_KeyDown;
            this.NumberChanged += this.BaseBox_NumberChanged;
            BookPage = (SectionScene)ev.Page;
            this.numericUpDown1.Value = BookPage.JoinSpaceLine;
            this.ItemInterval = ModifyNumericValue;
            //this.commentBoxPanel1.SetData(BookPage, new Point(this.TextItem.PictureSize.Width, 0),this);
            //this.commentBoxListControl1.SizeChanged += this.SlaveControl_SizeChanged;
            this.BookPage.EditStateChanged += this.BookProperty_Changed;
            this.commentBoxListControl1.KeyDown += this.StoryImageItemBox_KeyDown;
            this.commentBoxListControl1.InitializeSet(this.BookPage, this);
            Formize();
            this.CheckJoinUpButton();
            
        }

        public override void SuperDispose()
        {
            this.BookPage.EditStateChanged -= this.BookProperty_Changed;
            this.commentBoxListControl1.KeyDown -= this.StoryImageItemBox_KeyDown;
            this.button1.KeyDown -= this.StoryImageItemBox_KeyDown;
            this.NumberChanged -= this.BaseBox_NumberChanged;
            this.commentBoxListControl1.MyDispose();
            base.SuperDispose();
        }
        #endregion
        #region Method
        public override void Formize()
        {

            if (this.TextItem != null)
            {
                
                if (this.TextItem.IsCategory)
                {
                    if(this.button1 != null)this.button1.Visible = true;
                }
                else
                {
                    if (this.button1 != null) this.button1.Visible = false;
                }
                
                //:改良可　ここの条件式にVirtualPictureSizeを使えばさらに高速化
                if (!this.PictureSizeCheck && this.IsInitialized)
                {
                    var p = new Point(this.panel1.Location.X,this.panel1.Location.Y+this.panel1.Height);
                    if (this.pictureBox1.Dock != DockStyle.None)
                    {
                        this.pictureBox1.Dock = DockStyle.None;
                    }
                    //this.panel2.Location = p;
                    this.pictureBox1.Location = p;
                    this.VirtualPictureSize = this.TextItem.PictureSize;
                    this.pictureBox1.Size = this.TextItem.PictureSize;

                    
                    if (this.commentBoxListControl1.Dock != DockStyle.None) this.commentBoxListControl1.Dock = DockStyle.None;
                    var p2 = new Point(p.X, p.Y + this.TextItem.PictureSize.Height);
                    this.commentBoxListControl1.Location = p2;
                    this.NeedReOrder = true;
                }

                if (this.commentBoxListControl1 != null) this.commentBoxListControl1.SuperRefresh();
                ReOrder();
            }
        }
        private bool PictureSizeCheck
        {
            get
            {
                var a = Math.Abs(this.pictureBox1.Size.Width - this.TextItem.PictureSize.Width) < 9;
                var b = Math.Abs(this.pictureBox1.Size.Height - this.TextItem.PictureSize.Height) < 9;
                return a && b;
            }
        }

        private int ModifyNumericValue
        {
            get
            {
                var n = (int)this.numericUpDown1.Value;
                return n + (BookPage.JoinTop ? -n : 10);
            }
        }
        protected override void CreateNemeTextBox()
        {
            if (this.NameTextBox == null)
            {
                this.NameTextBox = new TextBox() { BackColor = Color.LawnGreen, Dock = DockStyle.Left };
                this.NameTextBox.TextChanged += this.textBox1_TextChanged;
                this.panel1.Controls.Add(this.NameTextBox);
                this.NameTextBox.BringToFront();
                this.NameLoad();
            }
        }
        protected override void DisposeNameTextBox()
        {
            if (this.NameTextBox != null)
            {
                this.panel1.Controls.Remove(this.NameTextBox);
                this.NameTextBox.TextChanged -= this.textBox1_TextChanged;
                this.NameTextBox = null;
            }
        }
        public void ReOrder()
        {
            //if (this.commentBoxPanel1 == null || !this.commentBoxPanel1.IsOK || !this.NeedReOrder) return;
            if (this.commentBoxListControl1 == null || !this.NeedReOrder) return;


            //if(this.commentBoxListControl1.Dock != DockStyle.Top)this.commentBoxListControl1.Dock = DockStyle.Top;
            var f = new List<Control>() { this.pictureBox1, this.commentBoxListControl1, this.numericUpDown1 };
            var width = Math.Max(this.TextItem.PictureSize.Width, this.commentBoxListControl1.Width);
            var height = f.Sum((v) => v.Height) + 10;
            var sz = new Size(width, height);
            this.NeedReOrder = false;
            if (this.Size != sz)
            {
                this.Size = sz;
                //this.commentBoxListControl1.MinimumSize = new Size(sz.Width, 0);
            }
        }
        private void CheckJoinUpButton()
        {

            if (this.BookPage.JoinTop)
            {
                this.JoinUpButton.Text = "▽";
            }
            else
            {
                this.JoinUpButton.Text = "▲";
            }
        }
        #endregion

        protected new void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.JoinUpButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.commentBoxListControl1 = new StoryViewer.ThumbnailViewer.StoryImageItem.CommentBoxListControl();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(0, 20);
            this.pictureBox1.Size = new System.Drawing.Size(309, 125);
            this.pictureBox1.SizeChanged += new System.EventHandler(this.pictureBox1_SizeChanged);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.Control;
            this.label1.Dock = System.Windows.Forms.DockStyle.Left;
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "label1";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.CausesValidation = false;
            this.numericUpDown1.Dock = System.Windows.Forms.DockStyle.Right;
            this.numericUpDown1.Enabled = false;
            this.numericUpDown1.InterceptArrowKeys = false;
            this.numericUpDown1.Location = new System.Drawing.Point(277, 0);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(32, 19);
            this.numericUpDown1.TabIndex = 2;
            this.numericUpDown1.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            this.numericUpDown1.Leave += new System.EventHandler(this.numericUpDown1_Leave);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.JoinUpButton);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.numericUpDown1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(309, 20);
            this.panel1.TabIndex = 3;
            this.panel1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseClick);
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // button2
            // 
            this.button2.Dock = System.Windows.Forms.DockStyle.Left;
            this.button2.Location = new System.Drawing.Point(110, 0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 20);
            this.button2.TabIndex = 5;
            this.button2.Text = "セリフ追加";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // JoinUpButton
            // 
            this.JoinUpButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.JoinUpButton.Location = new System.Drawing.Point(237, 0);
            this.JoinUpButton.Name = "JoinUpButton";
            this.JoinUpButton.Size = new System.Drawing.Size(40, 20);
            this.JoinUpButton.TabIndex = 4;
            this.JoinUpButton.Text = "▲";
            this.JoinUpButton.UseVisualStyleBackColor = true;
            this.JoinUpButton.Click += new System.EventHandler(this.JoinUpButton_Click);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Left;
            this.button1.Location = new System.Drawing.Point(35, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 20);
            this.button1.TabIndex = 3;
            this.button1.Text = "開く";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // commentBoxListControl1
            // 
            this.commentBoxListControl1.BorderColor = System.Drawing.Color.Black;
            this.commentBoxListControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.commentBoxListControl1.GroupBoxName = "セリフ";
            this.commentBoxListControl1.IsEditing = false;
            this.commentBoxListControl1.IsNeedRefresh = false;
            this.commentBoxListControl1.Location = new System.Drawing.Point(0, 145);
            this.commentBoxListControl1.Name = "commentBoxListControl1";
            this.commentBoxListControl1.ParentCtrl = null;
            this.commentBoxListControl1.Size = new System.Drawing.Size(309, 67);
            this.commentBoxListControl1.TabIndex = 5;
            this.commentBoxListControl1.Visible = false;
            this.commentBoxListControl1.SizeChanged += new System.EventHandler(this.commentBoxListControl1_SizeChanged);
            // 
            // StoryImageItemBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.Controls.Add(this.commentBoxListControl1);
            this.Controls.Add(this.panel1);
            this.Name = "StoryImageItemBox";
            this.Size = new System.Drawing.Size(309, 246);
            this.VisibleChanged += new System.EventHandler(this.StoryImageItemBox_VisibleChanged);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.StoryImageItemBox_MouseClick);
            this.Controls.SetChildIndex(this.panel1, 0);
            this.Controls.SetChildIndex(this.pictureBox1, 0);
            this.Controls.SetChildIndex(this.commentBoxListControl1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }
        #region OnEvent
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.OnMouseMove(e);
        }

        protected override void OnMouseDown(MouseEventArgs e)
        {
            base.OnMouseDown(e);
        }

        private void StoryImageItemBox_KeyDown(object sender, KeyEventArgs e)
        {
            this.OnKeyDown(e);
        }
        #endregion
        #region EventMethod

        private void BaseBox_NumberChanged(object sender, EventArgs e)
        {
            var s = "";
            if (this.label1 != null)
            {
                s = this.Number.ToString() + " (" + this.RealPageNumber + ")";
                var n = this.StockPages;
                if (n != 0) s += "[" + n + "]";
                this.label1.Text = s;
            }
        }
        private void BookProperty_Changed(object sender, BookPageContentChangeEventArgs e)
        {
            if (e.Type == PageContentChangeType.JoinTop)
            {
                this.OnSpecialSizeChanged(e);
            }
            else if (e.Type == PageContentChangeType.JoinSpaceLine)
            {
                var p = (int)this.numericUpDown1.Value;
                if (p != this.BookPage.JoinSpaceLine)
                {
                    this.numericUpDown1.Value = this.BookPage.JoinSpaceLine;
                }
                this.OnSpecialSizeChanged(e);
            }
            else if (e.Type == PageContentChangeType.TextPartsCount)
            {
                this.RefreshFormize();
            }
            this.IsPageChanged = true;
        }
        public override void RefreshFormize()
        {

            this.commentBoxListControl1.IsNeedRefresh = true;
            this.commentBoxListControl1.SuperRefresh();            
            this.Formize();
            this.RefreshImage();
            this.OnSpecialSizeChanged(new EventArgs());

        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            if (ModifyNumericValue != base.ItemInterval)
            {
                this.ItemInterval = ModifyNumericValue;
            }
            if (numericUpDown1.Value != this.BookPage.JoinSpaceLine)
            {
                this.BookPage.JoinSpaceLine = (int)numericUpDown1.Value;
            }
        }
        
        
        private void pictureBox1_SizeChanged(object sender, EventArgs e)
        {
            this.NeedReOrder = true;
            ReOrder();
        }
        private void StoryImageItemBox_VisibleChanged(object sender, EventArgs e)
        {
            
            this.commentBoxListControl1.SuperRefresh();
            
            this.NeedReOrder = true;
            ReOrder();
        }
        private void numericUpDown1_Leave(object sender, EventArgs e)
        {
            this.numericUpDown1.Enabled = false;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            var rec = new Rectangle(this.numericUpDown1.Location, this.numericUpDown1.Size);
            if (rec.Contains(e.Location))
            {
                this.numericUpDown1.Enabled = true;
            }
        }



        #endregion
        #region ClickEvent
        private void button1_Click(object sender, EventArgs e)
        {
            this.Open = !this.Open;
            this.OnSpecialSizeChanged(e);
        }
        protected override void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            base.pictureBox1_MouseClick(sender, e);
        }
        private void JoinUpButton_Click(object sender, EventArgs e)
        {
            this.BookPage.JoinTop = !this.BookPage.JoinTop;

            this.BookPage.JoinSpaceLine = this.BookPage.JoinTop ? this.DisplaySet.ContItem.SaveData.Model.CenterAppendSpaceCount : this.DisplaySet.ContItem.SaveData.Model.TopAppendSpaceCount;

            this.CheckJoinUpButton();

        }
        private void commentBoxPanel1_MouseClick(object sender, MouseEventArgs e)
        {
            this.OnMouseClick(e);
        }

        private void panel1_MouseClick(object sender, MouseEventArgs e)
        {
            this.OnMouseClick(e);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.commentBoxListControl1.AddNewItem();
        }
        private void StoryImageItemBox_MouseClick(object sender, MouseEventArgs e)
        {
            this.commentBoxListControl1.SuperRefresh();
            if (this.IsPageChanged) this.RefreshImage();
        }
        #endregion

        private void commentBoxListControl1_SizeChanged(object sender, EventArgs e)
        {

            this.NeedReOrder = true;
            ReOrder();
        }

        

        
    }
}
