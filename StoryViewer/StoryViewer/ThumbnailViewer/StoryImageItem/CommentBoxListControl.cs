﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Windows.Forms;
using System.Drawing;
using SuouBase;
using BSystem;
using BSystem.Drawing;
using BSystem.Collections;
using SuouBase.Book;

namespace StoryViewer.ThumbnailViewer.StoryImageItem
{
    public partial class CommentBoxListControl : SuouBase.Controls.ListControl
    {
        #region Property
        private SectionScene Page { get; set; }
        private Point StartLocation { get; set; }
        public StoryImageItemBox ParentCtrl { get; set; }
        public bool IsNeedRefresh { get; set; }
        public IEnumerable<CommentBoxControl> CommentBoxItems
        {
            get
            {
                return this.Items.Cast<CommentBoxControl>();
            }
        }
        #endregion
        public CommentBoxListControl()
        {
            InitializeComponent();
            this.InitializeSet();
        }



        #region GetProperty
        private SpaceLineSet SerifuMerge
        {
            get
            {
                var v = this.ParentCtrl.DLLFunction.TextBoxPlugin;
                if (v is SuouTextBox2.SuouTextBoxPlugin)
                {
                    var plg = (SuouTextBox2.SuouTextBoxPlugin)v;
                    var savedata = plg.SaveData;
                    return savedata.SerihuAlgorithm;
                }
                else
                {
                    return SuouEditorConst2.DefaultMergeAlgorithm;
                }
            }
        }
        #endregion
        public bool IsEditing { get; set; }
        #region Method
        public void InitializeSet(SectionScene pg, StoryImageItemBox parentCtrl)
        {
            this.Page = pg;
            this.ParentCtrl = parentCtrl;
            IsNeedRefresh = true;
            this.SuperRefresh();

        }
        public void AddNewItem()
        {
            this.IsEditing = true;
            var item = CommentBoxControl.CreateNew(this.ParentCtrl.BookPage.Parts[0], this.Page.Parts.Count, this.SerifuMerge, this.Page, this.ParentCtrl.TextItem.PictureSize.Width);
            this.Items.Add(item);
            this.Page.Parts.Add(item.PagePart);
            this.SuperRefresh();
            this.IsEditing = false;
        }
        private void Add(SectionScenePart pp)
        {
            var e = new CommentBoxControl(pp, this.Page, this.ParentCtrl.TextItem.PictureSize.Width);
            this.Items.Add(e);
        }
        public void MyDispose()
        {
            this.CommentBoxItems.ForEach((v) => v.MyDispose());
        }
        public void SuperRefresh()
        {
            if (!this.IsEditing)
            {
                if (IsNeedRefresh)
                {
                    this.MyDispose();
                    this.Items.Clear();
                    foreach (var v in this.Page.Parts)
                    {
                        if (v.Type == MergePartsType.Comment) this.Add(v);
                    }
                }
                this.CommentBoxItems.ForEach((v) => v.SuperRefresh());
            }
            IsNeedRefresh = false;
        }
        #endregion
        #region EventMethod
        private void CommentBoxPanel_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            this.OnKeyDown(e);
        }

        #endregion
        


        
    }
}
