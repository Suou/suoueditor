﻿namespace StoryViewer
{
    partial class ThumContViewBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar1.Location = new System.Drawing.Point(333, 0);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(17, 220);
            this.vScrollBar1.TabIndex = 0;
            this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.hScrollBar1.Location = new System.Drawing.Point(0, 203);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(333, 17);
            this.hScrollBar1.TabIndex = 1;
            this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ThumContViewBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.vScrollBar1);
            this.Name = "ThumContViewBox";
            this.Size = new System.Drawing.Size(350, 220);
            this.Load += new System.EventHandler(this.ThumContViewBox_Load);
            this.SizeChanged += new System.EventHandler(this.ThumContViewBox_Resize);
            this.VisibleChanged += new System.EventHandler(this.ThumContViewBox_VisibleChanged);
            this.ControlAdded += new System.Windows.Forms.ControlEventHandler(this.ThumContViewBox_ControlAdded);
            this.ControlRemoved += new System.Windows.Forms.ControlEventHandler(this.ThumContViewBox_ControlRemoved);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.ThumContViewBox_Paint);
            this.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.ThumContViewBox_QueryContinueDrag);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ThumContViewBox_KeyDown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ThumContViewBox_MouseClick);
            this.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.ThumContViewBox_MouseDoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ThumContViewBox_MouseDown);
            this.MouseLeave += new System.EventHandler(this.ThumContViewBox_MouseLeave);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ThumContViewBox_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ThumContViewBox_MouseUp);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Timer timer1;
    }
}
