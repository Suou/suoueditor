﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class PageJumpBox : UserControl
    {
        public PageJumpBox()
        {
            InitializeComponent();
        }
        public PageJumpBox(ThumContViewBox parent):this()
        {
            this.ParentItem = parent;
        }
        private ThumContViewBox ParentItem { get; set; }
        public IEnumerable<IImageItemBox> ListItems { get; set; }
        public void Create()
        {
            this.listBox1.Items.Clear();
            this.ListItems = this.ParentItem.Operator.BoxItems.Where((v) => v.TextItem.PureType == SuouBase.Drawing.TextPictureType.PictureCategory || v.TextItem.PureType == SuouBase.Drawing.TextPictureType.Category);


            foreach (var item in this.ListItems)
            {
                this.listBox1.Items.Add(item.TextItem.CategoryName);
            }

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var n = this.listBox1.SelectedIndex;
            if (n != -1)
            {
                var v = this.ListItems.ToList()[n];
                this.ParentItem.Operator.MoveViewPoint(v.MapLocation);
            }
        }
    }

}
