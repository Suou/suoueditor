﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using SuouBase;
using StoryViewer.ThumbnailViewer;
namespace StoryViewer
{
    public enum DragMode
    {
        Drag,Move,None
    }
    public class DragItem
    {
        #region ImageProperty
        //Point DragStartLocation;
        private DisplaySetting DisplaySet;
        private ImageItemBox _Item = null;
        public ImageItemBox Item
        {
            get { return this._Item; }
            set
            {
                if (value != null)
                {
                    this._Item = value;
                }
            }
        }
        public ImageItemBox VirtualItem = new ImageItemBox();
        public Point MouseDownItemSabunPoint;
        private ImageItemBoxCollection ImageItems
        {
            get
            {
                return this.DisplaySet.ImageContItem;
            }
        }
        public int StartDragItemNth;
        public bool IsDrag
        {
            get
            {
                if (this.Item == null) return false;
                return this.Item.IsDrag;
            }
        }
        public DragMode DragMode = DragMode.None;
        public Rectangle DontDragRectangle;
        public bool IsMoveStart = false;
        public bool IsDragStart = false;
        public Point LastMousePoint { get; set; }
        #endregion
        public Color FormColor { get; set; }
        public DragItem(DisplaySetting DS)
        {
            this.DisplaySet = DS;
            this.DisplaySet.Ctrl.Controls.Add(VirtualItem);
            this.VirtualItem.Visible = false;
            this.DisplaySet.ContItem.SetDragItemEvent(this.VirtualItem);
        }
        public void DragStart(ImageItemBox item,Point MousePoint,DragMode DragMode)
        {
            this.IsDragStart = true;
            this.Item = item;
            this.DragMode = DragMode;
            this.StartDragItemNth = this.ImageItems.GetNth(this.Item);
            this.DontDragRectangle = SuouBase.Base.SuouUsefulMethod.CreateNoDragRectangle(MousePoint);
            var pLoc = this.DisplaySet.Ctrl.PointToClient(Control.MousePosition);
            if (this.DragMode == StoryViewer.DragMode.Move)
            {
                MouseDownItemSabunPoint = new Point(MousePoint.X - pLoc.X, MousePoint.Y - pLoc.Y);
                VirtualItem.Location = pLoc;
            }
            else
            {
                MouseDownItemSabunPoint = new Point(VirtualItem.Width / 2, VirtualItem.Height / 2);
                var p = new Point(MousePoint.X - MouseDownItemSabunPoint.X, MousePoint.Y - MouseDownItemSabunPoint.Y);
                VirtualItem.Location = p;
            }
        }
        public void DragMoveStart()
        {
            IsMoveStart = true;
            this.Item.IsDrag = true;
            this.VirtualItem.ClearImage();
            this.VirtualItem.TextItem = this.Item.TextItem;
            if (!this.VirtualItem.Visible) this.VirtualItem.Visible = true;
            VirtualItem.Focus();
            this.VirtualItem.Refresh();


        }
        public void DragEnd()
        {
            this.IsDragStart = false;
            this.DisplaySet.Ctrl.MouseDownItem = null;
            IsMoveStart = false;
            if (this.Item == null) return;
            this.Item.IsDrag = false;
            this.Item = null;
            this.VirtualItem.Visible = false;
            this.DisplaySet.MapMethod.SetLocation();

            if (this.DisplaySet.Ctrl.ParentForm != null)
            {
                this.DisplaySet.Ctrl.ParentForm.TransparencyKey = Color.Crimson;
            }
            
        }
        public void DragDroping()
        {
            if(this.Item != null)this.Item.IsDrag = true;
            this.VirtualItem.Visible = false;
            this.ImageItems.Insert(this.StartDragItemNth, this.Item); 
            
        }
        public void Move()
        {
            var MousePoint = this.DisplaySet.Ctrl.PointToClient(Control.MousePosition);
            this.VirtualItem.Location = new Point(MousePoint.X - MouseDownItemSabunPoint.X, MousePoint.Y - MouseDownItemSabunPoint.Y);
            this.DisplaySet.Ctrl.Refresh();
        }
        public Point VirtualMapLocation
        {
            get 
            {
                return new Point(DisplaySet.MapLocation.X + VirtualItem.Location.X, DisplaySet.MapLocation.Y + VirtualItem.Location.Y);
            }
        }
        public void MoveCheckDragVirtualItem()
        {
            var nowMousePoint = Control.MousePosition;
            if (this.LastMousePoint == nowMousePoint) return;
            this.LastMousePoint = nowMousePoint;
            var p = this.DisplaySet.Ctrl.PointToClient(Control.MousePosition);
            if (Control.MouseButtons == System.Windows.Forms.MouseButtons.Left)
            {
                if (!this.IsDragStart) return;
                if (this.DisplaySet.Ctrl.CanItemDrag && !this.DontDragRectangle.Contains(p))
                {
                    this.DisplaySet.Ctrl.DoDrag();
                }
                if (this.DisplaySet.Ctrl.CanItemMove && !this.DontDragRectangle.Contains(p) && !this.IsMoveStart)
                {
                    this.DragMoveStart();
                }

                if (this.DisplaySet.Ctrl.CanItemMove)
                {
                    if (this.IsDrag)
                    {
                        this.Move();
                        this.DisplaySet.ImageContItem.OptimalSwap(this.Item, this.VirtualMapLocation);
                        this.DisplaySet.Ctrl.InvokeDataChenge(ThumbnailChangeItemMode.Move);
                    }
                }
                
            }
        }
    }
}
