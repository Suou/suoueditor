﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;

namespace StoryViewer.ThumbnailViewer.Collection
{
    public class RectangleArray<T> where T :ImageItemBox 
    {
        public List<T> Items { get; set; }
        public Size Size { get; set; }
        public int Width { get; set; }
        ThumbnailOrder Mode { get; set; }

        public Point BasePoint;
        Point NowPoint;
        //private int Number = 0;
        //private int TopY = 5;
        //private int RealPageNumber = 1;
        private int YokoNum { get; set; }
        private bool IsFirst { get; set; }
        public void SetPoint(ImageItemBox tvi)
        {
            if (this.IsFirst)
            {
                this.BasePoint.Y += tvi.ItemInterval;
                this.IsFirst = false;
            }

            if (YokoNum >= 1 && (NowPoint.X + tvi.Size.Width + tvi.ItemInterval > this.Width) || (this.Mode == ThumbnailOrder.Thread))
            {
                BasePoint.Y += NowPoint.Y + tvi.ItemInterval;
                NowPoint = new Point(0, 0);
                YokoNum = 0;
            }
            tvi.MapLocation = new Point(NowPoint.X, BasePoint.Y);

            YokoNum++;
            //RealPageNumber = this.SetImageBoxRealPage(tvi, RealPageNumber);

            NowPoint.X += tvi.Size.Width + tvi.ItemInterval;
            if (NowPoint.Y < tvi.Rec.Height) NowPoint.Y = tvi.Rec.Height;
            if (BasePoint.X < NowPoint.X) BasePoint.X = NowPoint.X;
            tvi.BackColor = tvi.TextItem.ControlBackColor;
            //this.SetLocation(tvi);
        }
        /*
        private int SetImageBoxRealPage(ImageItemBox tvi, int realpageNumber)
        {
            if (tvi is StoryImageItemBox)
            {
                var v = (StoryImageItemBox)tvi;
                v.RealPageNumber = realpageNumber++;
                if (v.JoinTop) realpageNumber--;
            }
            return realpageNumber;
        }
        */
        /*
        public void SetLocation(ImageItemBox ib)
        {
            {
                if (IsInner(ib))
                {
                    ib.Location = this.DisplaySet.MapMethod.GetLocation(ib);
                    if (!ib.Visible)
                    {
                        ib.VisibleFlag = true;
                    }
                    //this.DisplaySet.Ctrl.Refresh();
                }
                else
                {
                    if (ib.Visible) ib.VisibleFlag = false;
                }
            }
        }
        public bool IsInner(ImageItemBox ib)
        {
            return this.DisplaySet.MapRectangle.IntersectsWith(ib.Rec);
        }
        */
    }
}
