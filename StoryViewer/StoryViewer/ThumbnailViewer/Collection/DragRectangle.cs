﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using SuouBase.Controls;
using StoryViewer.ThumbnailViewer;

namespace StoryViewer
{
    public class DragRectangle
    {
        private DisplaySetting DisplaySet { get; set; }
        public Point StartPoint { get; private set; }
        public Point EndPoint { get; private set; }
        public bool IsDrag { get; private set; }
        public Rectangle MapRectangle { get; private set; }
        
        public Rectangle DisplayRectangle
        {
            get 
            {
                var p = this.DisplaySet.GetLocation(this.MapRectangle.Location);
                return new Rectangle(p, this.MapRectangle.Size);
            }
        }
        public bool IsEnable { get; set; }
        public Point SmallPoint(Point a, Point b)
        {
            if (a.Y < b.Y)
            {
                return a;
            }
            else if (a.Y == b.Y)
            {
                if (a.X < b.X)
                {
                    return a;
                }
                else if (a.X == b.Y)
                {
                    return a;
                }
                else
                {
                    return b;
                }
            }
            else
            {
                return b;
            }
        }
        public Point LargePoint(Point a, Point b)
        {
            var sp = this.SmallPoint(a, b);
            if (sp == a)
            {
                return b;
            }
            else
            {
                return a;
            }
        }
        public Rubberband Item { get; set; }
        public DragRectangle(DisplaySetting ds)
        {
            this.DisplaySet = ds;
            this.Item = new Rubberband() { BackColor = Color.Red};
            this.DisplaySet.Ctrl.Controls.Add(this.Item);
            this.Item.Visible = false;
        }

        public Point CreateSmallPoint(Point a, Point b)
        {
            var p = new Point(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y));
            return p;
        }

        public Rectangle CreateMapRectangle()
        {
            var size = new Size(Math.Abs(this.StartPoint.X - this.EndPoint.X), Math.Abs(this.StartPoint.Y - this.EndPoint.Y));

            var sp = this.CreateSmallPoint(this.StartPoint, this.EndPoint);
            return new Rectangle(sp, size);

        }
        public void Ctrl_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.IsDrag)
            {
                this.EndPoint = this.DisplaySet.CursorMapLocation;
                this.DisplaySet.MapMethod.SetSelectedImageBoxItems();
                var v = this.DisplaySet.MapMethod.LastSelectedImageBoxItems;
                this.MapRectangle = this.CreateMapRectangle();
                this.RefreshRectangle(true);
            }
        }
        public void Ctrl_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && this.DisplaySet.MapMethod.CursorPickUpBox == null)
            {
                this.DisplaySet.Ctrl.CancelSelectedImageBoxItems();
                this.StartPoint = this.DisplaySet.CursorMapLocation;
                this.EndPoint = this.StartPoint;
                this.IsEnable = this.DisplaySet.MapMethod.SelectedItem == null;
                this.IsDrag = true;
                this.RefreshRectangle(true);
            }
            else
            {
                this.Cancel();
            }
        }
        public void Cancel()
        {
            this.DisplaySet.Ctrl.CancelSelectedImageBoxItems();
            this.IsEnable = false;
            this.RefreshRectangle(false);
        }
        public void Ctrl_MouseUp(object sender, MouseEventArgs e)
        {
            if (this.DisplaySet.MapMethod.LastSelectedImageBoxItems.Length == 0)
            {
                this.IsEnable = false;
            }
            this.IsDrag = false;
            this.RefreshRectangle(false);
        }
        private void RefreshRectangle(bool b)
        {
            this.MapRectangle = this.CreateMapRectangle();
            this.Item.Location = this.DisplayRectangle.Location;
            this.Item.Size = this.DisplayRectangle.Size;
            this.Item.Visible = b;
            this.DisplaySet.Ctrl.Refresh();

        }
        public void DrawRectangle(Graphics g)
        {
            g.Clear(this.DisplaySet.Ctrl.BackColor);
            
            var rec = this.CreateMapRectangle();
            if (this.IsEnable)
            {
                if (this.MapRectangle == rec)
                {
                    //ControlPaint.DrawReversibleFrame(this.ScreenRectangle, this.DisplaySet.Ctrl.BackColor, FrameStyle.Thick);
                }
                else
                {
                    //ControlPaint.DrawReversibleFrame(this.ScreenRectangle, this.DisplaySet.Ctrl.BackColor, FrameStyle.Thick);
                    this.MapRectangle = rec;
                    //ControlPaint.DrawReversibleFrame(this.ScreenRectangle, this.DisplaySet.Ctrl.BackColor, FrameStyle.Thick);
                    //g.DrawRectangle(Pens.Red, rec);
                    this.DisplaySet.Ctrl.Refresh();
                    
                }
            }
        }
    }
}
