﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using StoryViewer;
using SuouBase;
//using StoryViewer2;
using SuouBase.Text;
using SuouTextBox2.Controls;
using SuouTextBox2;
using StoryViewer.StoryViewerEdit;
using StoryViewer.IStoryViewer;
namespace StoryViewer.StoryViewerEdit
{
    [ToolboxItem(false)]
    public partial class StoryViewerEditControl : UserControl
    {
        public StoryViewerEditControl()
        {
            InitializeComponent();

            
        }
        public EditorInfo DLLFunction { get; set; }
        public PageJumpBox StoryPageJumpBox { get; set; }

        IStoryViewerPlugin ViewerPlugin;
        SuouTextBoxPlugin EditPlugin;

        public void CreateLink(PageJumpBox item)
        {
            item.MinimumSize = new System.Drawing.Size(100, 300);
            var v = new ToolStripControlHost(item);
            this.StoryPageJumpBox = item;
            
            //this.linkToolStripMenuItem1.DropDownItems.Add(v);
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            Rectangle rec = new Rectangle(Point.Empty, this.menuStrip1.Size);
            LinearGradientBrush gb = new LinearGradientBrush(g.VisibleClipBounds, Color.LightGreen, Color.LightSkyBlue, LinearGradientMode.Horizontal);

            //四角を描く
            g.FillRectangle(gb, g.VisibleClipBounds);

        }

        private void aA選択ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pl = this.DLLFunction.PluginList.FirstOrDefault((v) => v.ClassName == "AASelectFormPlugin");
            if (pl != null)
            {
                if (pl.Plugin.Form.Visible)
                {
                    pl.Plugin.Form.Focus();
                }
                else
                {
                    pl.Plugin.Form.Show();
                }
            }
        }
        private void Viewer_PageSelected(object sender, SuouBase.BookPageContentChangeEventArgs e)
        {
            ChangeEditor();

        }
        private void BackButton_Clicked(object sender, EventArgs e)
        {
            ChangeViewer();
            //ViewerPlugin.SelectedPage.Text = EditPlugin.Page.Text;
        }
        public void FirstSet(SuouTextBoxPlugin plugin,IStoryViewerPlugin plugin2)
        {
            this.EditPlugin = plugin;
            this.ViewerPlugin = plugin2;
            ViewerPlugin.ViewerPageSelected += this.Viewer_PageSelected;

            this.backToolStripMenuItem.Click += this.BackButton_Clicked;

            this.tabControl1.TabPages[1].Controls.Add(EditPlugin.Control);
            EditPlugin.Control.Dock = DockStyle.Fill;
            this.tabControl1.TabPages[0].Controls.Add(ViewerPlugin.Control);
            this.tabPage1.Text = "編集";
            this.tabPage2.Text = "テキスト編集";
            ViewerPlugin.Control.Dock = DockStyle.Fill;
            ChangeViewer();
            //ChangeEditor();
        }
        public void ChangeViewer()
        {
            //return;
            this.EditPlugin.TemporaryDispose();
            //EditPlugin.Control.Visible = false;

            //this._Control.toolStripContainer1.ContentPanel.Controls.Remove(EditPlugin.Control);
            //this._Control.toolStripContainer1.ContentPanel.Controls.Add(ViewerPlugin.Control);
            //ViewerPlugin.Control.Visible = true;
            this.tabControl1.SelectedIndex = 0;

            this.ViewerPlugin.Call();

            this.backToolStripMenuItem.Visible = false;
        }
        public void ChangeEditor()
        {
            this.EditLockFlag = false;
            this.tabControl1.SelectedIndex = 1;
            //this._Control.toolStripContainer1.ContentPanel.Controls.Remove(ViewerPlugin.Control);
            //ViewerPlugin.Control.Visible = false;
            //this._Control.toolStripContainer1.ContentPanel.Controls.Add(EditPlugin.Control);
            //EditPlugin.Control.Visible = true;
            //EditPlugin.Control.Dock = DockStyle.Fill;
            //this.backToolStripMenuItem.Visible = true;

            //_Control.BackButton.BringToFront();
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl1.SelectedIndex == 1)
            {
                this.DLLFunction.DisplayType = SuouBase.Interface.EditorDisplay.TextEdit;
            }
            else if (this.tabControl1.SelectedIndex == 0)
            {
                this.DLLFunction.DisplayType = SuouBase.Interface.EditorDisplay.PageViewer;
                
            }
        }

        private bool EditLockFlag = true;
        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (e.TabPageIndex == 1)
            {
                if (this.EditLockFlag) e.Cancel = true; ;
            }
            else if (e.TabPageIndex == 0)
            {
                this.EditLockFlag = true;
                
            }
        }

        

    }
}
