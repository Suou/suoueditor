﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
//using StoryViewer2;
using SuouBase.Text;
using SuouTextBox2.Controls;
using SuouTextBox2;
using System.Drawing;
using StoryViewer.StoryViewerEdit;
using StoryViewer.IStoryViewer;
using SuouBase.Book;
namespace StoryViewer
{
    [IComponentPlugin("StoryViewerEdit", PluginType.Control)]
    public class StoryViewerEditPlugin:IComponentPlugin
    {
        IStoryViewerPlugin ViewerPlugin;
        SuouTextBoxPlugin EditPlugin;
        StoryViewerEditControl _Control = new StoryViewerEditControl();
        //ContainerControl _Control = new UserControl();

        EditorInfo DLLFunction {get;set;}
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public StoryViewerEditPlugin()
        {
            
        }
        public StoryViewerEditPlugin(EditorInfo dllfunction)
        {
            
            this._Control.DLLFunction = dllfunction;
            this.DLLFunction = dllfunction;
            ViewerPlugin = new IStoryViewerPlugin(DLLFunction);
            EditPlugin = new SuouTextBoxPlugin(DLLFunction);
            /*
            {
                var a = (IDockingManagerPlugin)EditPlugin;
                var b = (IDockingManagerPlugin)ViewerPlugin;

                var d = new SuouBase.Controls.DockingManagerPanel() { InnerItem = ViewerPlugin.Control, Name = "Viewer" };
                var c = new SuouBase.Controls.DockingManagerPanel() { InnerItem = EditPlugin.Control, Name = "Editor" };
                
                
                a.Panel = c;
                b.Panel = d;
            }
            */
            //DLLFunction.StoryPageViewPlugin = ViewerPlugin;
            DLLFunction.StoryPageViewPlugin.Control.Dock = DockStyle.Fill;
            DLLFunction.TextBoxPlugin = EditPlugin;


            this._Control.FirstSet(this.EditPlugin,this.ViewerPlugin);
            
            //Action func = InitialAsyncLoad;
            //func();
            //this.DLLFunction.MasterForm.Form.BeginInvoke(func, null);//EndInvokeしたほうがいいかな？
            
            //func.BeginInvoke(null, null);
        }
        private void InitialAsyncLoad()
        {

            //ChangeEditor();
        }

        public IBookPage BookPage
        {
            get { return this.EditPlugin.BookPage; }
            set
            {
                this.EditPlugin.BookPage = value;
                this._Control.ChangeEditor();
            }
        }

       
        public bool DisposeCheck()
        {
            EditPlugin.DisposeCheck();
            ViewerPlugin.DisposeCheck();
            return true;
        }



        public Form Form { get { return null; } }

        //public SuouBase.Controls.DockingManagerPanel Panel { get; set; }

        public ContainerControl Control
        {
            get
            {
                //return this.Panel == null ? this._Control : (ContainerControl)this.Panel;
                return this._Control;
            }
        }
        //public ContainerControl Control { get { return this._Control; } }
        public SettingPage[] SettingPage
        {
            get
            {
                List<SettingPage> r = new List<SettingPage>();
                if (this.EditPlugin.SettingPage != null) r.AddRange(this.EditPlugin.SettingPage);
                if (this.ViewerPlugin.SettingPage != null) r.AddRange(this.ViewerPlugin.SettingPage);
                return r.ToArray();
            }
        }
    }
}
