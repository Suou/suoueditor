﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
using StoryViewer.BookEditor.Section;
using SuouBase.Text;
using StoryViewer.AASelecter;
using System.Drawing;
using SuouBase.Plugin;
namespace StoryViewer.IStoryViewer
{
    public class TestPlugin : AbstractControlPlugin<StoryViewer.BookEditor.TestControl>
    {
        IStoryViewerPlugin ViewerPlugin;
        SuouTextBox2.SuouTextBoxPlugin EditPlugin;
        public TestPlugin(EditorInfo info)
        {
            this.DLLFunction = info;
            



            ViewerPlugin = new IStoryViewerPlugin(DLLFunction);
            EditPlugin = new SuouTextBox2.SuouTextBoxPlugin(DLLFunction);

            /*
            var a = (IDockingManagerPlugin)EditPlugin;
            var b = (IDockingManagerPlugin)ViewerPlugin;

            var c = new SuouBase.Controls.DockingManagerPanel() { InnerItem = EditPlugin.Control };
            var d = new SuouBase.Controls.DockingManagerPanel() { InnerItem = ViewerPlugin.Control };
            var e = new SuouBase.Controls.DockingManagerPanel() { InnerItem = this.Control };

            a.Panel = c;
            b.Panel = d;
            */
            //this.DLLFunction.DockingPanels.Add(c);
            //this.DLLFunction.DockingPanels.Add(d);
            
            var p = new StoryViewer.StoryViewerEdit.StoryViewerEditControl();
            //p.toolStripContainer1.ContentPanel.Controls.Add(c);
            //p.toolStripContainer1.ContentPanel.Controls.Add(d);
            //c.Dock = DockStyle.Fill;
            p.Dock = DockStyle.Fill;
            var k = new BookEditor.TestControl();
            this._Ctrl = k;
            this.Contena = p; 
            //DLLFunction.StoryPageViewPlugin = ViewerPlugin;
            DLLFunction.TextBoxPlugin = EditPlugin;


            ViewerPlugin.ViewerPageSelected += this.Viewer_PageSelected;

            this.Contena.backToolStripMenuItem.Click += this.BackButton_Clicked;
            //ChangeViewer();
        }
        public StoryViewer.StoryViewerEdit.StoryViewerEditControl Contena { get; set; }
        public override ContainerControl Control
        {
            get
            {
                return this.Contena;
            }
        }
        private void Viewer_PageSelected(object sender, BookPageContentChangeEventArgs e)
        {
            ChangeEditor();

        }
        private void BackButton_Clicked(object sender, EventArgs e)
        {
            ChangeViewer();
            //ViewerPlugin.SelectedPage.Text = EditPlugin.Page.Text;
        }
        public void ChangeViewer()
        {
            //return;
            this.EditPlugin.TemporaryDispose();
            EditPlugin.Control.Visible = false;

            this.Contena.toolStripContainer1.ContentPanel.Controls.Remove(EditPlugin.Control);
            this.Contena.toolStripContainer1.ContentPanel.Controls.Add(ViewerPlugin.Control);


            this.ViewerPlugin.Call();
            ViewerPlugin.Control.Dock = DockStyle.Fill;

            Contena.backToolStripMenuItem.Visible = false;
        }
        public void ChangeEditor()
        {

            this.Contena.toolStripContainer1.ContentPanel.Controls.Remove(ViewerPlugin.Control);
            this.Contena.toolStripContainer1.ContentPanel.Controls.Add(EditPlugin.Control);
            EditPlugin.Control.Visible = true;
            EditPlugin.Control.Dock = DockStyle.Fill;
            Contena.backToolStripMenuItem.Visible = true;

            //_Control.BackButton.BringToFront();
        }
    }
}
