﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Windows.Forms;
namespace StoryViewer.BookEditor
{
    public interface IBookEditor
    {
        ContextMenuStrip RightContextMenuStrip { get; }
        bool CanCut { get; }
        bool CanCopy { get; }
        bool CanPaste { get; }
        bool CanUndo { get; }
        bool CanAddNewSection { get; }
        bool CanAddNewPage { get; }
        bool CanSettingModel { get; }
        
        void Undo();
        void Cut();
        void Paste();
        void Copy();
        void AddNewPage();
        void AddNewSection();
        void SettingModel();
    }
}
