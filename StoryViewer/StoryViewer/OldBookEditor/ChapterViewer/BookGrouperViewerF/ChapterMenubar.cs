﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Text;
using System.ComponentModel;
using SuouBase.Book;
using SuouBase.Book.Chapter;
namespace StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF
{
    public class ChapterMenubar:MenuStrip
    {
        public ChapterMenubar()
        {
            this.InitializeComponent();
            this.Tip.ParentControl = this;
        }
        #region Initialize
        private ContextMenuStrip contextMenuStrip1;
        private IContainer components;
        private ToolStripMenuItem addNewSectionStripMenuItem;
        private ContextMenuStrip sectionItemContextMenuStrip2;
        private ToolStripMenuItem mergeUpToolStripMenuItem;
        private ToolStripMenuItem mergeDownToolStripMenuItem;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ToolStripMenuItem ChangeNameMenuItem;
        private ToolStripMenuItem moveRightMenuItem;
        private ToolStripMenuItem PageModelMenuItem;
        private ToolStripMenuItem moveLeftMenuItem;

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addNewSectionStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sectionItemContextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mergeUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ChangeNameMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveRightMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveLeftMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.PageModelMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.sectionItemContextMenuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewSectionStripMenuItem,
            this.PageModelMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(233, 48);
            // 
            // addNewSectionStripMenuItem
            // 
            this.addNewSectionStripMenuItem.Name = "addNewSectionStripMenuItem";
            this.addNewSectionStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.addNewSectionStripMenuItem.Text = "新しいセクションを追加";
            this.addNewSectionStripMenuItem.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // sectionItemContextMenuStrip2
            // 
            this.sectionItemContextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mergeUpToolStripMenuItem,
            this.mergeDownToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.ChangeNameMenuItem,
            this.moveRightMenuItem,
            this.moveLeftMenuItem});
            this.sectionItemContextMenuStrip2.Name = "sectionItemContextMenuStrip2";
            this.sectionItemContextMenuStrip2.Size = new System.Drawing.Size(197, 136);
            this.sectionItemContextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.sectionItemContextMenuStrip2_Opening);
            // 
            // mergeUpToolStripMenuItem
            // 
            this.mergeUpToolStripMenuItem.Name = "mergeUpToolStripMenuItem";
            this.mergeUpToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.mergeUpToolStripMenuItem.Text = "上のセクションと融合";
            this.mergeUpToolStripMenuItem.Click += new System.EventHandler(this.mergeUpToolStripMenuItem_Click);
            // 
            // mergeDownToolStripMenuItem
            // 
            this.mergeDownToolStripMenuItem.Name = "mergeDownToolStripMenuItem";
            this.mergeDownToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.mergeDownToolStripMenuItem.Text = "下のセクションと融合";
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.deleteToolStripMenuItem.Text = "削除";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // ChangeNameMenuItem
            // 
            this.ChangeNameMenuItem.Name = "ChangeNameMenuItem";
            this.ChangeNameMenuItem.Size = new System.Drawing.Size(196, 22);
            this.ChangeNameMenuItem.Text = "タイトルを変更する";
            this.ChangeNameMenuItem.Click += new System.EventHandler(this.ChangeNameMenuItem_Click);
            // 
            // moveRightMenuItem
            // 
            this.moveRightMenuItem.Name = "moveRightMenuItem";
            this.moveRightMenuItem.Size = new System.Drawing.Size(196, 22);
            this.moveRightMenuItem.Text = "右へ移動";
            this.moveRightMenuItem.Click += new System.EventHandler(this.moveRightMenuItem_Click);
            // 
            // moveLeftMenuItem
            // 
            this.moveLeftMenuItem.Name = "moveLeftMenuItem";
            this.moveLeftMenuItem.Size = new System.Drawing.Size(196, 22);
            this.moveLeftMenuItem.Text = "左へ移動";
            this.moveLeftMenuItem.Click += new System.EventHandler(this.moveLeftMenuItem_Click);
            // 
            // PageModelMenuItem
            // 
            this.PageModelMenuItem.Name = "PageModelMenuItem";
            this.PageModelMenuItem.Size = new System.Drawing.Size(232, 22);
            this.PageModelMenuItem.Text = "このブックの雛形設定をする";
            this.PageModelMenuItem.Click += new System.EventHandler(this.PageModelMenuItem_Click);
            // 
            // ChapterMenubar
            // 
            this.AllowDrop = true;
            this.BackColor = System.Drawing.Color.PaleVioletRed;
            this.ContextMenuStrip = this.contextMenuStrip1;
            this.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.ChapterMenubar_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.ChapterMenubar_DragEnter);
            this.contextMenuStrip1.ResumeLayout(false);
            this.sectionItemContextMenuStrip2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion
        #region Property
        private ChapterBook2 _Book = null;
        public ChapterBook2 Book
        {
            get
            {
                return this._Book;
            }
            set
            {
                if (this._Book != null)
                {
                    this._Book.Pages.CollectionChanged -= this.Collection_Changed;
                    this._Book.ContentChanged -= this.Content_Changed;
                    this._Book.SelectedIndexChanged -= this.SelectedSection_Changed;
                }

                this._Book = value;
                if (value != null)
                {
                    this._Book.ContentChanged += this.Content_Changed;
                    this._Book.Pages.CollectionChanged += this.Collection_Changed;
                    this._Book.SelectedIndexChanged += this.SelectedSection_Changed;
                }
                this.Set();
            }
        }
        private Color SelectedColor = Color.Aqua;
        public SuouBase.Controls.AAToolTip Tip = new SuouBase.Controls.AAToolTip() { };
        private int _SelectedItemIndex = -1;
        public int RightSelectedIndex
        {
            get
            {
                return this._SelectedItemIndex;
            }
            set
            {
                this._SelectedItemIndex = value;
                this.ColoringSelectedButton();
            }
        }
        public int LeftSelectedIndex
        {
            get
            {
                if (this.Book == null)
                {
                    return -1;
                }
                else
                {
                    return this.Book.SelectedPageIndex;
                }
            }
        }
        public event Action<SectionNBook, EventArgs> ItemSelected;
        #endregion
        //private int Count = 0;

        #region ClickEvent
        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.AddNewSection();
        }

        private void PageModelMenuItem_Click(object sender, EventArgs e)
        {
            PageModelSetting.SettingModel(this.Book);
        }
        private void mergeUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.RightSelectedIndex != 0)
            {
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.RightSelectedIndex != -1)
            {
                if (this.Book.Count == 1)
                {
                    MessageBox.Show("削除できません");
                }
                else
                {
                    this.Book.Delete(this.RightSelectedIndex);
                    if (this.Book.SelectedPageIndex == this.Book.Count)
                    {
                        this.Book.SelectedPageIndex = (this.Book.Count - 1);
                    }
                    else
                    {
                        this.Book.SelectedPageIndex = (this.Book.SelectedPageIndex);
                    }
                    
                }
            }
        }

        private void ChangeNameMenuItem_Click(object sender, EventArgs e)
        {
            if (this.RightSelectedIndex != -1)
            {
                var page = this.Book.Pages[this.RightSelectedIndex];
                var f = SuouBase.Controls.InputForm.CreateNameBox("名前の変更", page.Name);
                if (f != null)
                {
                    page.Name = f;
                    this.Set();
                }
            }
        }

        private void sectionItemContextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            if (this.Book.Count > 1)
            {
                this.deleteToolStripMenuItem.Enabled = true;
            }
            else
            {
                this.deleteToolStripMenuItem.Enabled = false;
            }
            this.moveLeftMenuItem.Enabled = this.Book.CanMoveLeftPage(this.RightSelectedIndex);
            this.moveRightMenuItem.Enabled = this.Book.CanMoveRightPage(this.RightSelectedIndex);
        }

        private void moveLeftMenuItem_Click(object sender, EventArgs e)
        {
            if (this.RightSelectedIndex > 0)
            {
                this.Book.Move(this.RightSelectedIndex, this.RightSelectedIndex - 1);
                //this.Set();
            }
        }

        private void moveRightMenuItem_Click(object sender, EventArgs e)
        {
            if (this.RightSelectedIndex >= 0 && this.RightSelectedIndex < this.Book.Count -1)
            {
                this.Book.Move(this.RightSelectedIndex, this.RightSelectedIndex + 1);
                //this.Set();
            }
        }
        #endregion

        #region DragEvent
        private void ChapterMenubar_DragEnter(object sender, DragEventArgs e)
        {
            if (e.AllowedEffect == DragDropEffects.Move && e.Data.GetDataPresent(typeof(ToolStripMenuItem)))
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        private void ChapterMenubar_DragDrop(object sender, DragEventArgs e)
        {
            if (e.AllowedEffect == DragDropEffects.Move && e.Data.GetDataPresent(typeof(ToolStripMenuItem)))
            {
                var item = (ToolStripMenuItem)e.Data.GetData(typeof(ToolStripMenuItem));

                Point p = new Point(e.X, e.Y);
                var p2 = this.PointToClient(p);

                var n = ContainLocation(p2);
                if (n != -1)
                {
                    var pg = this.Items.IndexOf(item);
                    this.Book.Move(pg, n);
                }
            }
        }
        #endregion
        #region OtherEvent
        private void SelectedSection_Changed(object sender, EventArgs e)
        {
            this._SelectedItemIndex = -1;
            this.ColoringSelectedButton();
            this.ItemSelected(this.Book.Pages[this.Book.SelectedPageIndex].Items, new EventArgs());
        }
        private void Content_Changed(object sender, SuouBase.BookContentChangeEventArgs e)
        {
            var number = e.ChangeIndex;
            if (number != -1 && e.Type == BookContentChangeType.PageContent)
            {
                if (e.PageEventArgs.Type == PageContentChangeType.ChapterPageInnerType)
                {
                    if (e.PageEventArgs.Tag.Type == BookContentChangeType.Page)
                    {
                        this.Items[number].Text = this.GetName(this.Book.Pages[number]);
                    }
                    else if (e.PageEventArgs.Tag.PageEventArgs != null && e.PageEventArgs.Tag.PageEventArgs.Type == PageContentChangeType.JoinTop)
                    {
                        this.Items[number].Text = this.GetName(this.Book.Pages[number]);
                    }
                }
                else
                {
                    this.Items[number].Text = this.GetName(this.Book.Pages[number]);
                }
            }
        }
        private void Collection_Changed(object sender, SuperObservableCollectionEventArgs<ChapterBookPage2> e)
        {
            switch (e.Action)
            {
                case SuperObservableCollectionAction.Add:
                    foreach (var v in e.AddedItems)
                    {
                        this.Items.Add(this.CreateMenuItem(v));                    
                    }
                    break;
                case SuperObservableCollectionAction.Insert:
                    var t = e.ChangedIndex;
                    foreach (var v in e.AddedItems)
                    {
                        this.Items.Insert(t++, this.CreateMenuItem(v));
                    }
                    break;
                case SuperObservableCollectionAction.Remove:                    
                    this.Items.RemoveAt(e.ChangedIndex);
                    break;
                case SuperObservableCollectionAction.Move:
                    this.Items.Insert(e.MoveToIndex, this.Items[e.ChangedIndex]);
                    break;
                case SuperObservableCollectionAction.Replace:
                    throw new Exception();
                    //break;
                case SuperObservableCollectionAction.Clear:
                    throw new Exception();
                    //break;
                default:
                    throw new Exception();
            }
        }
        #endregion
        #region Method
        private int ContainLocation(Point p)
        {
            int i = 0;
            foreach (ToolStripMenuItem v in this.Items)
            {
                if (v.Bounds.Contains(p)) return i;
                i++;
            }
            return -1;
        }


        public Color OpeningColor
        {
            get
            {
                return Color.Red;
            }
        }
        private void ColoringSelectedButton()
        {
            if (this.Book == null) return;
            for (int i = 0; i < this.Book.Count; i++)
            {
                this.Items[i].BackColor = this.BackColor;
            }
            if (this.RightSelectedIndex != -1)
            {
                this.Items[this.RightSelectedIndex].BackColor = this.SelectedColor;
            }
            if (this.LeftSelectedIndex != -1)
            {
                this.Items[this.LeftSelectedIndex].BackColor = this.OpeningColor;
            }

        }
        public void AddNewSection()
        {
            var ap = new ChapterBookPage2() { Name = "新しいセクション" };
            this.Book.Add(ap);
            this.Book.SelectedPageIndex = (this.Book.Count - 1);

        }
        public void Set()
        {
            SuouBase.Api.WINAPI.StopControlDraw(this.Handle);
            this.Items.Clear();
            if (this.Book == null) return;
            foreach (var v in this.Book.Pages)
            {
                this.Items.Add(CreateMenuItem(v));
            }
            this.ColoringSelectedButton();

            SuouBase.Api.WINAPI.StartControlDraw(this.Handle);
            this.Refresh();
        }
        private string GetName(ChapterBookPage2 p)
        {
            var s = string.Format("{0}({1})", p.Name, p.Items.OutputPages.Count());
            return s;
        }
        private ToolStripMenuItem CreateMenuItem(ChapterBookPage2 p)
        {
            var r = new ToolStripMenuItem() { Text = GetName(p), Tag = p };
            r.MouseDown += (a, b) =>
            {
                var n = this.Book.Pages.FindIndex((v) => v == p);

                if (b.Button == System.Windows.Forms.MouseButtons.Left && this.ItemSelected != null)
                {
                    this.Book.SelectedPageIndex = n;
                    //this.Book.SelectedPageIndex = (n);
                }
                else if (b.Button == System.Windows.Forms.MouseButtons.Right)
                {
                    //this.Book.SelectedPageIndex = (n);
                    this.RightSelectedIndex = n;
                    this.sectionItemContextMenuStrip2.Show(this, this.PointToClient(Control.MousePosition));
                }
            };
            r.MouseEnter += (a, b) =>
            {
                if (p.Items.Count > 0)
                {
                    this.Tip.TextItem = p.Items.BookPages.First().TextImagize();
                    this.Tip.TextItem.PictureSize = new Size(300, 300);
                    var x = this.Width - this.Tip.TextItem.PictureSize.Width;
                    this.Tip.ToolTipShow(new Point(x > 0 ? x : 0, this.Bottom));
                }
            };
            r.MouseLeave += (a, b) =>
            {
                this.Tip.ToolTipHide();
                if (Control.MouseButtons == System.Windows.Forms.MouseButtons.Left)
                {
                    this.DoDragDrop(a, DragDropEffects.Move);
                }
            };
            return r;
        }
        #endregion

    }
    public class ChapterItem : ToolStripMenuItem
    {
        public ObsoleteChapterBookPage Page { get; set; }
    }
}
