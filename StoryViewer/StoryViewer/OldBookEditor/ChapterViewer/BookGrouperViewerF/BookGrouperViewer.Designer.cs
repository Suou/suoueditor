﻿namespace StoryViewer.IStoryViewer.BookGrouperViewerF
{
    partial class BookGrouperViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pagecontextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.splitSectionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.storyViewerBox21 = new StoryViewer.BookEditor.Section.SectionViewer();
            this.chapterMenubar1 = new StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.ChapterMenubar();
            this.pagecontextMenuStrip2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pagecontextMenuStrip2
            // 
            this.pagecontextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.splitSectionToolStripMenuItem});
            this.pagecontextMenuStrip2.Name = "contextMenuStrip2";
            this.pagecontextMenuStrip2.Size = new System.Drawing.Size(269, 48);
            // 
            // splitSectionToolStripMenuItem
            // 
            this.splitSectionToolStripMenuItem.Name = "splitSectionToolStripMenuItem";
            this.splitSectionToolStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.splitSectionToolStripMenuItem.Text = "このページを境にセクションを作成";
            this.splitSectionToolStripMenuItem.Click += new System.EventHandler(this.splitSectionToolStripMenuItem_Click);
            // 
            // splitter1
            // 
            this.splitter1.Location = new System.Drawing.Point(0, 4);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 478);
            this.splitter1.TabIndex = 3;
            this.splitter1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.storyViewerBox21);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(649, 478);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "セクション";
            // 
            // storyViewerBox21
            // 
            this.storyViewerBox21.AllowDrop = true;
            this.storyViewerBox21.DLLFunction = null;
            this.storyViewerBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.storyViewerBox21.EditItem = null;
            this.storyViewerBox21.IsUnable = false;
            this.storyViewerBox21.Location = new System.Drawing.Point(3, 15);
            this.storyViewerBox21.Name = "storyViewerBox21";
            this.storyViewerBox21.ShortCutKeys = null;
            this.storyViewerBox21.Size = new System.Drawing.Size(643, 460);
            this.storyViewerBox21.TabIndex = 0;
            this.storyViewerBox21.ItemClicked += new SuouBase.BookPageContentChangeEventHandler(this.storyViewerBox21_ItemClicked);
            // 
            // chapterMenubar1
            // 
            this.chapterMenubar1.AllowDrop = true;
            this.chapterMenubar1.BackColor = System.Drawing.Color.PaleVioletRed;
            this.chapterMenubar1.Book = null;
            this.chapterMenubar1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.chapterMenubar1.Location = new System.Drawing.Point(0, 0);
            this.chapterMenubar1.Name = "chapterMenubar1";
            this.chapterMenubar1.Size = new System.Drawing.Size(652, 4);
            this.chapterMenubar1.TabIndex = 6;
            this.chapterMenubar1.Text = "chapterMenubar1";
            this.chapterMenubar1.ItemSelected += new System.Action<SuouBase.Book.SectionNBook, System.EventArgs>(this.chapterMenubar1_ItemSelected);
            // 
            // BookGrouperViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.chapterMenubar1);
            this.Name = "BookGrouperViewer";
            this.Size = new System.Drawing.Size(652, 482);
            this.pagecontextMenuStrip2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        //private StoryViewer2.StoryViewerBox2 storyViewerBox21;
        private System.Windows.Forms.ContextMenuStrip pagecontextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem splitSectionToolStripMenuItem;
        private StoryViewer.BookEditor.Section.SectionViewer storyViewerBox21;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.GroupBox groupBox1;
        private BookEditor.ChapterViewer.BookGrouperViewerF.ChapterMenubar chapterMenubar1;
    }
}
