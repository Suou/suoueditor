﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Drawing;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using StoryViewer.BookEditor;
using SuouBase.Book;
namespace StoryViewer.IStoryViewer.BookGrouperViewerF
{
    [ToolboxItem(false)]
    public partial class BookGrouperViewer : UserControl,IBookEditor
    {
        public BookGrouperViewer()
        {
            InitializeComponent();

            ToolStripManager.Merge(this.pagecontextMenuStrip2, this.storyViewerBox21.ItemMenuStrip);
            this.storyViewerBox21.ItemMenuStrip.Opening += (i, b) => this.splitSectionToolStripMenuItem.Enabled = true;
        }
        public StoryViewer.BookEditor.Section.SectionViewer SubBox
        {
            get { return this.storyViewerBox21; }
        }
        public ContextMenuStrip RightContextMenuStrip
        {
            get { return this.storyViewerBox21.RightContextMenuStrip; }
        }
        public StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.BookGrouperSavedata Savedata
        {
            get
            {
                return new StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.BookGrouperSavedata() { SavedataA = this.storyViewerBox21.SaveData };
            }
            set
            {
                if (value != null)
                {
                    this.storyViewerBox21.SaveData = (value.SavedataA);
                }
            }
        }
        private EditorInfo _DLLFunction;
        public EditorInfo DLLFunction
        {
            get { return this._DLLFunction; }
            set
            {
                this._DLLFunction = value;
                this.storyViewerBox21.DLLFunction = value;
            }
        }
        private ObsoleteChapterBook _Item;
        public ObsoleteChapterBook Item
        {
            get { return this._Item; }
            set
            {
                this.chapterMenubar1.Book = value.ToChapterBook2();
                this._Item = value;
                this.Set();
            }
        }
        
        public event BookPageContentChangeEventHandler ItemClicked;

        public bool IsOperateEnable
        {
            get
            {
                return this.Item != null;
            }
        }
        public void FocusPage(int n)
        {
            var k = this.Item.TranslateSubIndex(n);
            var index = this.Item.Pages.IndexOf(k.Key);
            var subindex = k.Value;
            this.Item.SelectedPageIndex = index;
            this.storyViewerBox21.EditItem = k.Key.Items;

            //this.tabControl1.SelectedIndex = 1;
            this.storyViewerBox21.FocusPage(subindex);
        }
        public void UpdatePage()
        {
            //this.thumContViewBox1.Operator.CheckChangePicture();
            this.storyViewerBox21.UpdatePage();
        }
        private void Set()
        {
            this.storyViewerBox21.EditItem = null;
            if (this.Item == null)
            {
                //this.thumContViewBox1.Operator.Clear();
                
            }
            else
            {
                //var pages = this.Item.TextImagize();
                //this.thumContViewBox1.Operator.ImageItemSet(pages);
                if (this.Item.Pages.Count > 0)
                {
                    this.storyViewerBox21.EditItem = this.Item.Pages[this.Item.SelectedPageIndex].Items;
                    this.UpdateGroupBoxName();
                }
                else
                {
                }
            }
        }
        private void UpdateGroupBoxName()
        {
            this.groupBox1.Text = string.Format("セクション({0})", this.Item.SelectedPage.Name);
        }
        private void thumContViewBox1_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            var p = (ObsoleteChapterBookPage)e.SelectedPage;
            this.Item.SelectedPageIndex = e.SelectedIndex;
            this.storyViewerBox21.EditItem = p.Items;
            this.UpdateGroupBoxName();

            //this.tabControl1.SelectedIndex = 1;
        }


        private void addNewPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var ap = new ObsoleteChapterBookPage() { Name = "新しいチャプター"};
            this.Item.Add(ap);

            //var f = new TextThumbImage(ap, 0);
            //this.thumContViewBox1.Operator.InsertToSelectedLocation(f);
        }


        private void splitSectionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            var book = this.storyViewerBox21.Split();
            this.Item.Pages[this.Item.SelectedPageIndex].Items.Pages = book.Key.Pages;
            var p = new ObsoleteChapterBookPage();
            p.Items.Pages = book.Value.Pages;
           // this.thumContViewBox1.Operator.Insert(this.Item.SelectedSectionIndex + 1, p.TextImagize());
            this.storyViewerBox21.EditItem = null;
            //this.tabControl1.SelectedIndex = 0;
            
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

            
        }

        private void storyViewerBox21_ItemClicked(object sender, BookPageContentChangeEventArgs e)
        {
            if (this.ItemClicked != null) this.ItemClicked(this, e);
            
        }




        private void chapterMenubar1_ItemSelected(SectionNBook arg1, EventArgs arg2)
        {
            throw new Exception();
            /*
            if (this.DLLFunction != null) this.DLLFunction.DisplayType = EditorDisplay.PageViewer;
            this.storyViewerBox21.EditItem = arg1;
            this.Item.SelectedPageIndex = this.Item.Pages.FindIndex((v) => v.Items == arg1);
            this.groupBox1.Text = string.Format("セクション({0})", arg1.Name);
            */
        }
        public bool CanCut
        {
            get { return this.storyViewerBox21.CanCut; ; }
        }
        public bool CanCopy
        {
            get { return this.storyViewerBox21.CanCopy; }
        }
        public bool CanPaste
        {
            get { return this.storyViewerBox21.CanPaste; }
        }
        public bool CanUndo
        {
            get { return this.storyViewerBox21.CanUndo; }
        }
        public bool CanSettingModel
        {
            get
            {
                return this.Item != null;
            }
        }
        public void SettingModel()
        {
            PageModelSetting.SettingModel(this.chapterMenubar1.Book);
        }
        public void Undo()
        {
            this.storyViewerBox21.Undo();
        }
        public void Cut()
        {
            this.storyViewerBox21.Cut();
        }
        public void Paste()
        {
            this.storyViewerBox21.Paste();
        }
        public void Copy()
        {
            this.storyViewerBox21.Copy();
        }
        public void AddNewPage()
        {
            this.storyViewerBox21.AddNewPage();
        }
        public void AddNewSection()
        {
            this.chapterMenubar1.AddNewSection();
        }

        public bool CanAddNewPage
        {
            get { return this.Item != null; }
        }
        public bool CanAddNewSection
        {
            get { return this.Item != null; }
        }

        
    }
}
