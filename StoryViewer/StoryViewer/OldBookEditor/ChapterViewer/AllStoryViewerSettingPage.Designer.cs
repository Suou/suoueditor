﻿namespace StoryViewer.BookEditor.ChapterViewer
{
    partial class AllStoryViewerSettingPage
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AllStoryViewerSettingPage));
            this.pageModelSetting1 = new StoryViewer.PageModelSetting();
            this.SuspendLayout();
            // 
            // pageModelSetting1
            // 
            this.pageModelSetting1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pageModelSetting1.Location = new System.Drawing.Point(0, 0);
            this.pageModelSetting1.Model = ((SuouBase.Book.SuouBookPageModel)(resources.GetObject("pageModelSetting1.Model")));
            this.pageModelSetting1.Name = "pageModelSetting1";
            this.pageModelSetting1.Size = new System.Drawing.Size(579, 215);
            this.pageModelSetting1.TabIndex = 0;
            // 
            // AllStoryViewerSettingPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pageModelSetting1);
            this.Name = "AllStoryViewerSettingPage";
            this.Size = new System.Drawing.Size(579, 268);
            this.ResumeLayout(false);

        }

        #endregion

        private PageModelSetting pageModelSetting1;
    }
}
