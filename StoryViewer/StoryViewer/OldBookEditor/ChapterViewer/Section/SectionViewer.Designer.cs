﻿namespace StoryViewer.BookEditor.Section
{
    partial class SectionViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ItemMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addNewPageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.upDownOperateMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.unityWithUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unityWithDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.mergeUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.replaceBackGroundToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.namingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backGroundMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addNewPageToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.viewModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewScale25Strip = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewScale50Strip = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewScale75Strip = new System.Windows.Forms.ToolStripMenuItem();
            this.ViewScale100Strip = new System.Windows.Forms.ToolStripMenuItem();
            this.settingPageModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dragTimer = new System.Windows.Forms.Timer(this.components);
            this.mergeTimer = new System.Windows.Forms.Timer(this.components);
            this.shiftItemMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.singleCopyStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundCopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clusterCopyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thumContViewBox1 = new StoryViewer.ThumContViewBox();
            this.WakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemMenuStrip.SuspendLayout();
            this.backGroundMenuStrip.SuspendLayout();
            this.shiftItemMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // ItemMenuStrip
            // 
            this.ItemMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewPageToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem1,
            this.upDownOperateMenuItem,
            this.replaceBackGroundToolStripMenuItem,
            this.toolStripSeparator3,
            this.WakuToolStripMenuItem,
            this.namingToolStripMenuItem,
            this.openToolStripMenuItem});
            this.ItemMenuStrip.Name = "ItemMenuStrip";
            this.ItemMenuStrip.Size = new System.Drawing.Size(185, 252);
            this.ItemMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.ItemMenuStrip_Opening);
            // 
            // addNewPageToolStripMenuItem
            // 
            this.addNewPageToolStripMenuItem.Name = "addNewPageToolStripMenuItem";
            this.addNewPageToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.addNewPageToolStripMenuItem.Text = "新しいページを追加";
            this.addNewPageToolStripMenuItem.Click += new System.EventHandler(this.addNewPageToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.copyToolStripMenuItem.Text = "コピー";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyUTF8ToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.cutToolStripMenuItem.Text = "切り取り";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.pasteToolStripMenuItem.Text = "貼り付け";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem1
            // 
            this.deleteToolStripMenuItem1.Name = "deleteToolStripMenuItem1";
            this.deleteToolStripMenuItem1.Size = new System.Drawing.Size(184, 22);
            this.deleteToolStripMenuItem1.Text = "削除";
            this.deleteToolStripMenuItem1.Click += new System.EventHandler(this.deleteToolStripMenuItem1_Click);
            // 
            // upDownOperateMenuItem
            // 
            this.upDownOperateMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem,
            this.toolStripSeparator1,
            this.unityWithUpToolStripMenuItem,
            this.unityWithDownToolStripMenuItem,
            this.toolStripSeparator2,
            this.mergeUpToolStripMenuItem,
            this.mergeDownToolStripMenuItem});
            this.upDownOperateMenuItem.Name = "upDownOperateMenuItem";
            this.upDownOperateMenuItem.Size = new System.Drawing.Size(184, 22);
            this.upDownOperateMenuItem.Text = "上下操作";
            // 
            // moveUpToolStripMenuItem
            // 
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.moveUpToolStripMenuItem.Text = "上に移動";
            this.moveUpToolStripMenuItem.Click += new System.EventHandler(this.moveUpToolStripMenuItem_Click);
            // 
            // moveDownToolStripMenuItem
            // 
            this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
            this.moveDownToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.moveDownToolStripMenuItem.Text = "下に移動";
            this.moveDownToolStripMenuItem.Click += new System.EventHandler(this.moveDownToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(121, 6);
            // 
            // unityWithUpToolStripMenuItem
            // 
            this.unityWithUpToolStripMenuItem.Name = "unityWithUpToolStripMenuItem";
            this.unityWithUpToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.unityWithUpToolStripMenuItem.Text = "上と結合";
            this.unityWithUpToolStripMenuItem.Click += new System.EventHandler(this.unityWithUpToolStripMenuItem_Click);
            // 
            // unityWithDownToolStripMenuItem
            // 
            this.unityWithDownToolStripMenuItem.Name = "unityWithDownToolStripMenuItem";
            this.unityWithDownToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.unityWithDownToolStripMenuItem.Text = "下と結合";
            this.unityWithDownToolStripMenuItem.Click += new System.EventHandler(this.unityWithDownToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(121, 6);
            // 
            // mergeUpToolStripMenuItem
            // 
            this.mergeUpToolStripMenuItem.Name = "mergeUpToolStripMenuItem";
            this.mergeUpToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.mergeUpToolStripMenuItem.Text = "上と融合";
            this.mergeUpToolStripMenuItem.Click += new System.EventHandler(this.mergeUpToolStripMenuItem_Click);
            // 
            // mergeDownToolStripMenuItem
            // 
            this.mergeDownToolStripMenuItem.Name = "mergeDownToolStripMenuItem";
            this.mergeDownToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.mergeDownToolStripMenuItem.Text = "下と融合";
            this.mergeDownToolStripMenuItem.Click += new System.EventHandler(this.mergeDownToolStripMenuItem_Click);
            // 
            // replaceBackGroundToolStripMenuItem
            // 
            this.replaceBackGroundToolStripMenuItem.Name = "replaceBackGroundToolStripMenuItem";
            this.replaceBackGroundToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.replaceBackGroundToolStripMenuItem.Text = "背景置換";
            this.replaceBackGroundToolStripMenuItem.Click += new System.EventHandler(this.replaceBackGroundToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(181, 6);
            // 
            // namingToolStripMenuItem
            // 
            this.namingToolStripMenuItem.Name = "namingToolStripMenuItem";
            this.namingToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.namingToolStripMenuItem.Text = "名前をつける";
            this.namingToolStripMenuItem.Click += new System.EventHandler(this.namingToolStripMenuItem_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Checked = true;
            this.openToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.openToolStripMenuItem.Text = "開く";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // backGroundMenuStrip
            // 
            this.backGroundMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewPageToolStripMenuItem1,
            this.pasteToolStripMenuItem1,
            this.viewModeToolStripMenuItem,
            this.settingPageModelToolStripMenuItem});
            this.backGroundMenuStrip.Name = "contextMenuStrip1";
            this.backGroundMenuStrip.Size = new System.Drawing.Size(233, 92);
            this.backGroundMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // addNewPageToolStripMenuItem1
            // 
            this.addNewPageToolStripMenuItem1.Name = "addNewPageToolStripMenuItem1";
            this.addNewPageToolStripMenuItem1.Size = new System.Drawing.Size(232, 22);
            this.addNewPageToolStripMenuItem1.Text = "新しいページを追加";
            this.addNewPageToolStripMenuItem1.Click += new System.EventHandler(this.addNewPageToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem1
            // 
            this.pasteToolStripMenuItem1.Name = "pasteToolStripMenuItem1";
            this.pasteToolStripMenuItem1.Size = new System.Drawing.Size(232, 22);
            this.pasteToolStripMenuItem1.Text = "貼り付け";
            this.pasteToolStripMenuItem1.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // viewModeToolStripMenuItem
            // 
            this.viewModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ViewScale25Strip,
            this.ViewScale50Strip,
            this.ViewScale75Strip,
            this.ViewScale100Strip});
            this.viewModeToolStripMenuItem.Name = "viewModeToolStripMenuItem";
            this.viewModeToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.viewModeToolStripMenuItem.Text = "表示";
            // 
            // ViewScale25Strip
            // 
            this.ViewScale25Strip.Name = "ViewScale25Strip";
            this.ViewScale25Strip.Size = new System.Drawing.Size(109, 22);
            this.ViewScale25Strip.Text = "25%";
            this.ViewScale25Strip.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // ViewScale50Strip
            // 
            this.ViewScale50Strip.Name = "ViewScale50Strip";
            this.ViewScale50Strip.Size = new System.Drawing.Size(109, 22);
            this.ViewScale50Strip.Text = "50%";
            this.ViewScale50Strip.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // ViewScale75Strip
            // 
            this.ViewScale75Strip.Name = "ViewScale75Strip";
            this.ViewScale75Strip.Size = new System.Drawing.Size(109, 22);
            this.ViewScale75Strip.Text = "75%";
            this.ViewScale75Strip.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // ViewScale100Strip
            // 
            this.ViewScale100Strip.Name = "ViewScale100Strip";
            this.ViewScale100Strip.Size = new System.Drawing.Size(109, 22);
            this.ViewScale100Strip.Text = "100%";
            this.ViewScale100Strip.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // settingPageModelToolStripMenuItem
            // 
            this.settingPageModelToolStripMenuItem.Name = "settingPageModelToolStripMenuItem";
            this.settingPageModelToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.settingPageModelToolStripMenuItem.Text = "このセクションの雛形を設定";
            this.settingPageModelToolStripMenuItem.Click += new System.EventHandler(this.settingPageModelToolStripMenuItem_Click_1);
            // 
            // dragTimer
            // 
            this.dragTimer.Enabled = true;
            this.dragTimer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mergeTimer
            // 
            this.mergeTimer.Enabled = true;
            this.mergeTimer.Interval = 1000;
            this.mergeTimer.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // shiftItemMenuStrip
            // 
            this.shiftItemMenuStrip.BackColor = System.Drawing.Color.Violet;
            this.shiftItemMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.singleCopyStripMenuItem,
            this.backgroundCopyToolStripMenuItem,
            this.clusterCopyToolStripMenuItem});
            this.shiftItemMenuStrip.Name = "shiftItemMenuStrip";
            this.shiftItemMenuStrip.Size = new System.Drawing.Size(161, 70);
            // 
            // singleCopyStripMenuItem
            // 
            this.singleCopyStripMenuItem.Name = "singleCopyStripMenuItem";
            this.singleCopyStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.singleCopyStripMenuItem.Text = "シングルコピー";
            this.singleCopyStripMenuItem.Click += new System.EventHandler(this.singleCopyStripMenuItem_Click);
            // 
            // backgroundCopyToolStripMenuItem
            // 
            this.backgroundCopyToolStripMenuItem.Name = "backgroundCopyToolStripMenuItem";
            this.backgroundCopyToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.backgroundCopyToolStripMenuItem.Text = "背景コピー";
            this.backgroundCopyToolStripMenuItem.Click += new System.EventHandler(this.backgroundCopyToolStripMenuItem_Click);
            // 
            // clusterCopyToolStripMenuItem
            // 
            this.clusterCopyToolStripMenuItem.Name = "clusterCopyToolStripMenuItem";
            this.clusterCopyToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.clusterCopyToolStripMenuItem.Text = "クラスタコピー";
            this.clusterCopyToolStripMenuItem.Click += new System.EventHandler(this.clusterCopyToolStripMenuItem_Click);
            // 
            // thumContViewBox1
            // 
            this.thumContViewBox1.AllowDrop = true;
            this.thumContViewBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.thumContViewBox1.BoxType = StoryViewer.ImageItemBoxType.Story;
            this.thumContViewBox1.CanItemDrag = false;
            this.thumContViewBox1.CanItemMove = true;
            this.thumContViewBox1.ControlContext = this.backGroundMenuStrip;
            this.thumContViewBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumContViewBox1.EnableNameTextBox = StoryViewer.NameTextBoxType.None;
            this.thumContViewBox1.IsLoose = true;
            this.thumContViewBox1.IsMouseMoveFocus = false;
            this.thumContViewBox1.IsTransparentDrag = false;
            this.thumContViewBox1.ItemContext = this.ItemMenuStrip;
            this.thumContViewBox1.ItemInterval = 5;
            this.thumContViewBox1.ItemSize = new System.Drawing.Size(100, 100);
            this.thumContViewBox1.Location = new System.Drawing.Point(0, 0);
            this.thumContViewBox1.ManualDragDo = false;
            this.thumContViewBox1.MinItemSize = new System.Drawing.Size(150, 16);
            this.thumContViewBox1.MouseDownItem = null;
            this.thumContViewBox1.Name = "thumContViewBox1";
            this.thumContViewBox1.OrderMode = StoryViewer.ThumbnailOrder.Thread;
            this.thumContViewBox1.Percent = 0.5F;
            this.thumContViewBox1.PermitCategory = false;
            this.thumContViewBox1.ShiftItemContext = this.shiftItemMenuStrip;
            this.thumContViewBox1.Size = new System.Drawing.Size(296, 219);
            this.thumContViewBox1.TabIndex = 0;
            this.thumContViewBox1.ViewMode = SuouBase.Drawing.ThumbnailMode.Percent;
            this.thumContViewBox1.ItemClicked += new StoryViewer.ThumbnailViewEventHandler(this.thumContViewBox1_ItemClicked);
            this.thumContViewBox1.ItemDataChanged += new StoryViewer.ThumbnailChangeItemEventHandler(this.thumContViewBox1_ItemDataChanged);
            this.thumContViewBox1.ImageBoxMouseEnter += new System.EventHandler(this.thumContViewBox1_ImageBoxMouseEnter);
            this.thumContViewBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.thumContViewBox1_DragDrop);
            this.thumContViewBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.thumContViewBox1_DragEnter);
            this.thumContViewBox1.DragOver += new System.Windows.Forms.DragEventHandler(this.thumContViewBox1_DragOver);
            this.thumContViewBox1.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.thumContViewBox1_QueryContinueDrag);
            this.thumContViewBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.thumContViewBox1_MouseMove);
            // 
            // WakuToolStripMenuItem
            // 
            this.WakuToolStripMenuItem.Name = "WakuToolStripMenuItem";
            this.WakuToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.WakuToolStripMenuItem.Text = "枠設定";
            this.WakuToolStripMenuItem.Click += new System.EventHandler(this.WakuToolStripMenuItem_Click);
            // 
            // SectionViewer
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.thumContViewBox1);
            this.Name = "SectionViewer";
            this.Size = new System.Drawing.Size(296, 219);
            this.ItemMenuStrip.ResumeLayout(false);
            this.backGroundMenuStrip.ResumeLayout(false);
            this.shiftItemMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private StoryViewer.ThumContViewBox thumContViewBox1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem addNewPageToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        public System.Windows.Forms.ContextMenuStrip ItemMenuStrip;
        private System.Windows.Forms.ContextMenuStrip backGroundMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem addNewPageToolStripMenuItem1;
        private System.Windows.Forms.Timer dragTimer;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem viewModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ViewScale25Strip;
        private System.Windows.Forms.ToolStripMenuItem ViewScale50Strip;
        private System.Windows.Forms.ToolStripMenuItem ViewScale75Strip;
        private System.Windows.Forms.ToolStripMenuItem ViewScale100Strip;
        private System.Windows.Forms.Timer mergeTimer;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem namingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upDownOperateMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem unityWithUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unityWithDownToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem mergeUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mergeDownToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip shiftItemMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem singleCopyStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem backgroundCopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem replaceBackGroundToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clusterCopyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingPageModelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem WakuToolStripMenuItem;

    }
}
