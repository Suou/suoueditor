﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Controls;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Plugin;
using SuouTextBox2.Base;
using StoryViewer.BookEditor.Section;
using SuouBase.Book;
using SuouBase.Book.Chapter;
namespace StoryViewer
{
    public partial class PageModelSetting : UserControl,SettingPage
    {
        public PageModelSetting()
        {
            InitializeComponent();
        }
        public event EventHandler OKClicked;
        public Control Ctrl { get { return this; } }
        public void OK()
        {
            if (this.OKClicked != null) this.OKClicked(this, new EventArgs());
            /*
            if (this.ParentBox != null) ParentBox.SetSaveDataA(this.SaveDataA);
            if (this.SubParentBox != null) SubParentBox.SetSaveDataA(this.SaveDataA);
            */
        }
        public SuouBookPageModel Model
        {
            get
            {
                var f = new SuouBookPageModel()
                {
                    TopAppendSpaceCount = (int)this.TopSpaceLineCountBox.Value,
                    CenterAppendSpaceCount = (int)this.CenterSpaceLineCountBox.Value,
                    DefaultMergeSet = this.spacerComboBox1.SelectedSpaceSet,
                    MaxPageCount = (int)this.MaxCountBox.Value,
                    MaxPageWidth = (int)this.MaxWidthBox.Value,
                    MaxSize = (int)this.MaxSizeBox.Value
                };
                return f;
            }
            set
            {
                if (value != null)
                {
                    this.CenterSpaceLineCountBox.Value = value.CenterAppendSpaceCount;
                    this.TopSpaceLineCountBox.Value = value.TopAppendSpaceCount;
                    this.MaxWidthBox.Value = value.MaxPageWidth;
                    this.MaxCountBox.Value = value.MaxPageCount;
                    this.spacerComboBox1.SelectedSpaceSet = value.DefaultMergeSet;
                    this.MaxSizeBox.Value = value.MaxSize;
                }
            }
        }
        public void Cancel()
        {
        }
        public string PageName { get { return "ストーリービューワー"; } }

        public static void SettingModel(ChapterBook2 book)
        {
            var contro = new PageModelSetting() { Model = book.Model ?? new SuouBookPageModel() };
            var rst = SuouBase.Controls.Forms.SettingForm.Create(contro, "雛形の設定");
            if (rst == DialogResult.OK)
            {
                book.Model = contro.Model;
                DialogResult result = MessageBox.Show("全てのセクションにこの雛形を適用しますか？",
                        "質問",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    foreach (var v in book.Pages)
                    {
                        v.Items.Model = book.Model;
                        v.Items.SetModelToItem();
                        //Section.SectionViewer.SetModelToItem(v.Items);
                    }
                }
            }
        }
    }

}
