﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
using StoryViewer.BookEditor.Section;
using SuouBase.Text;
using StoryViewer.AASelecter;
using System.Drawing;
using SuouBase.Book;
namespace StoryViewer
{
    [IComponentPlugin("StoryViewer", PluginType.Control)]
    public class StoryViewerPlugin:SuouBase.Interface.IStoryPageListClass
    {
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public IBook Item
        {
            get
            {
                return HaveControl.EditItem;
            }
            set
            {
                if (value == null)
                {
                    HaveControl.EditItem = null;
                }else
                {
                    HaveControl.EditItem = value;
                }
            }
        }
        public void Call()
        {
            this.UpdatePage();
        }
        public void FocusPage(int index)
        {
            this.HaveControl.FocusPage(index);
        }
        public PageModelSetting SettingPage1 = new PageModelSetting() { };
        //public StoryViewerShortCutSettingPage SettingPage2 = new StoryViewerShortCutSettingPage();
        public SettingPage[] SettingPage { get { return new SettingPage[]{SettingPage1}; } }

        public IBookPage SelectedPage { get; set; }

        public AstPage[] CopyPages
        {
            get { return null; }
            set {}
        }
        public string SaveFileName = SuouEditorConst.SettingDirectory + "/" + "StoryViewer.stn";

        public SectionViewer HaveControl;
        public ContainerControl Control
        {
            get { return HaveControl; }
        }
        public Form Form
        {
            get { return null; }
        }
        public bool DisposeCheck()
        {
            //this.HaveControl.SaveData.Save(SaveFileName);
            return true;
        }
        public void UpdatePage()
        {
            this.HaveControl.UpdatePage();
        }
        public void Paste(IBookPage ap)
        {
            throw new Exception();
            /*
            if (Item == null) return;
            if (ap is SectionBookPage)
            {
                var mbp = (SectionBookPage)ap;
                Item.Insert(Item.Count, ap);
                this.HaveControl.Paste(mbp);
            }
            else
            {
            }
            */
        }
        public ToolStripItemCollection ToolStripItems
        {
            get
            {
                return null;
            }
        }
        public void CheckToolStripItems()
        {
        }

        public StoryViewerPlugin(EditorInfo DLLFunction)
        {
            HaveControl = new SectionViewer();
            this.DLLFunction = DLLFunction;
            this.HaveControl.DLLFunction = DLLFunction;
            var f = DLLFunction.FileData.SelectData;
            if (f != null)
            {
                this.Item = f.StoryFile;
            }
            else if (f == null)
            {
                this.Item = null;
            }
            else
            {
                throw new Exception();
            }

            this.HaveControl.ItemClicked += this.HaveControl_PageSelected;
            this.HaveControl.PageItemAccepted += this.PageItem_Accepted;

            DLLFunction.FileData.TabChanged += this.SuperVisor_PageChanged;

        }
        private void StoryListMenuItem_Opening(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem)sender;
            this.HaveControl.ItemMenuStrip_Opening(this, null);
            item.DropDownItems.Clear();
            SuouBase.MenuStripMethod.CopyStripList(item.DropDownItems, this.HaveControl.ItemMenuStrip.Items);
        }

        private void HaveControl_PageChanged(object sender, BookPageContentChangeEventArgs e)
        {
            PageSelected(sender, e);
        }
        private void HaveControl_PageSelected(object sender, BookPageContentChangeEventArgs e)
        {

            SelectedPage = e.Page;
            PageSelected(sender, e);
        }
        public EditorInfo DLLFunction { get; set; }
        public event BookPageContentChangeEventHandler ViewerPageSelected;

        private void PageSelected(object sender, BookPageContentChangeEventArgs e)
        {
            if (ViewerPageSelected != null) ViewerPageSelected(sender, e);
            if (DLLFunction != null && DLLFunction.TextBoxPlugin != null)
            {
                
                DLLFunction.TextBoxPlugin.Page = e.Page;
            }
        }
        
        private void SuperVisor_PageChanged(object sender, SuouBase.Interface.SuouEditorDataClass data)
        {
            if (data == null)
            {
                this.Item = null;
            }
            else
            {
                var f = data.StoryFile;
                this.Item = f;
            }
        }
        private void PageItem_Accepted(object sender, BookPageContentChangeEventArgs e)
        {
            var f = DLLFunction.PluginList.FirstOrDefault((v) => v.Plugin is IAASelectClass);
            if (f != null)
            {
                var pf = (IAASelectClass)f.Plugin;
                pf.NotifyDropPageItem(e.Page);
            }
        }

    }
    /*
    [Serializable]
    public class StoryViewerSaveData
    {
        public StoryViewerShortCutKeys ShortCutKeys { get; set; }
        public StoryViewerSettingSaveDataA SaveDataA { get; set; }
        public StoryViewer.ThumbnailView_SaveData ThumbnailSaveData { get; set; }

        public void Save(string filename)
        {
            BSystem.XML.BinarySerialize.SaveToBinaryFile(this, filename);
        }
        public static StoryViewerSaveData Load(string filename)
        {
            var f = BSystem.XML.BinarySerialize.LoadFromBinaryFile<StoryViewerSaveData>(filename);
            if (f != null)
            {
                return (StoryViewerSaveData)f;
            }
            else
            {
                return null;
            }
        }
    }
    */
}
