﻿namespace StoryViewer
{
    partial class PageModelSetting
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.MaxSizeBox = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.MaxCountBox = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.MaxWidthBox = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.spacerComboBox1 = new SuouBase.Controls.SpacerComboBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.CenterSpaceLineCountBox = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.TopSpaceLineCountBox = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxSizeBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCountBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxWidthBox)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CenterSpaceLineCountBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopSpaceLineCountBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.MaxSizeBox);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.MaxCountBox);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.MaxWidthBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.spacerComboBox1);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(570, 248);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "雛形設定";
            // 
            // MaxSizeBox
            // 
            this.MaxSizeBox.Location = new System.Drawing.Point(98, 136);
            this.MaxSizeBox.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.MaxSizeBox.Name = "MaxSizeBox";
            this.MaxSizeBox.Size = new System.Drawing.Size(120, 19);
            this.MaxSizeBox.TabIndex = 9;
            this.MaxSizeBox.Value = new decimal(new int[] {
            4096,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 12);
            this.label6.TabIndex = 8;
            this.label6.Text = "最大サイズ(byte)";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "EUC-JP(したらば)"});
            this.comboBox1.Location = new System.Drawing.Point(128, 164);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 7;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 12);
            this.label5.TabIndex = 6;
            this.label5.Text = "容量計算用文字コード";
            // 
            // MaxCountBox
            // 
            this.MaxCountBox.Location = new System.Drawing.Point(298, 112);
            this.MaxCountBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.MaxCountBox.Name = "MaxCountBox";
            this.MaxCountBox.Size = new System.Drawing.Size(120, 19);
            this.MaxCountBox.TabIndex = 5;
            this.MaxCountBox.Value = new decimal(new int[] {
            300,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(251, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 12);
            this.label4.TabIndex = 4;
            this.label4.Text = "最大行";
            // 
            // MaxWidthBox
            // 
            this.MaxWidthBox.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.MaxWidthBox.Location = new System.Drawing.Point(98, 112);
            this.MaxWidthBox.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.MaxWidthBox.Name = "MaxWidthBox";
            this.MaxWidthBox.Size = new System.Drawing.Size(120, 19);
            this.MaxWidthBox.TabIndex = 3;
            this.MaxWidthBox.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 12);
            this.label3.TabIndex = 2;
            this.label3.Text = "最大幅（ドット）";
            // 
            // spacerComboBox1
            // 
            this.spacerComboBox1.ChengeNowPageSetting = false;
            this.spacerComboBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.spacerComboBox1.Location = new System.Drawing.Point(3, 85);
            this.spacerComboBox1.Name = "spacerComboBox1";
            this.spacerComboBox1.Size = new System.Drawing.Size(564, 26);
            this.spacerComboBox1.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.CenterSpaceLineCountBox);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.TopSpaceLineCountBox);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(564, 70);
            this.panel1.TabIndex = 0;
            // 
            // CenterSpaceLineCountBox
            // 
            this.CenterSpaceLineCountBox.Location = new System.Drawing.Point(100, 34);
            this.CenterSpaceLineCountBox.Name = "CenterSpaceLineCountBox";
            this.CenterSpaceLineCountBox.Size = new System.Drawing.Size(37, 19);
            this.CenterSpaceLineCountBox.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "中間空白行数";
            // 
            // TopSpaceLineCountBox
            // 
            this.TopSpaceLineCountBox.Location = new System.Drawing.Point(100, 9);
            this.TopSpaceLineCountBox.Name = "TopSpaceLineCountBox";
            this.TopSpaceLineCountBox.Size = new System.Drawing.Size(37, 19);
            this.TopSpaceLineCountBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "先頭空白行数";
            // 
            // PageModelSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "PageModelSetting";
            this.Size = new System.Drawing.Size(570, 248);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MaxSizeBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxCountBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MaxWidthBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CenterSpaceLineCountBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopSpaceLineCountBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown TopSpaceLineCountBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown CenterSpaceLineCountBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown MaxCountBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown MaxWidthBox;
        private System.Windows.Forms.Label label3;
        private SuouBase.Controls.SpacerComboBox spacerComboBox1;
        private System.Windows.Forms.NumericUpDown MaxSizeBox;
        private System.Windows.Forms.Label label6;
    }
}
