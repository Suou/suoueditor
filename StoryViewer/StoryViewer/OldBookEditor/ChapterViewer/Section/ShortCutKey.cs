﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using System.Windows.Forms;

namespace StoryViewer
{
    [Serializable]
    public class StoryViewerShortCutKeys:ShortCutCommandCollection
    {
        /*
        public ShortCutCommand AddNewPage = new ShortCutCommand("新しいページを追加") { IsAlt = true, NaiveKey = Keys.I };
        public ShortCutCommand MoveUp = new ShortCutCommand("上に移動") { IsAlt = true, NaiveKey = Keys.U };
        public ShortCutCommand MoveDown = new ShortCutCommand("下に移動") { IsAlt = true, NaiveKey = Keys.J };
        public ShortCutCommand Copy = new ShortCutCommand("コピー") { IsAlt = true, NaiveKey = Keys.C };
        public ShortCutCommand Cut = new ShortCutCommand("切り取り") { IsAlt = true, NaiveKey = Keys.X };
        public ShortCutCommand Paste = new ShortCutCommand("貼り付け") { IsAlt = true, NaiveKey = Keys.V };
        public ShortCutCommand Delete = new ShortCutCommand("削除") { IsAlt = true, NaiveKey = Keys.D };
        public ShortCutCommand JoinUp = new ShortCutCommand("上と結合") { IsAlt = true, NaiveKey = Keys.T };
        public ShortCutCommand JoinDown = new ShortCutCommand("下と結合") { IsAlt = true, NaiveKey = Keys.Y };
        */
        public StoryViewerShortCutKeys()
        {
            this.Name = "ストーリービューワー";

            this.Items = new ShortCutCommand[] { };
            //Items = new ShortCutCommand[] { AddNewPage,MoveUp,MoveDown,Copy,Cut,Paste,Delete,JoinUp,JoinDown };
        }
    }
}
