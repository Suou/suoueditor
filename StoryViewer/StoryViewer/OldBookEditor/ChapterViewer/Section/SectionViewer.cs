﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using SuouBase;
using SuouBase.Interface;
using System.Windows.Forms;
using StoryViewer;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Base;
using BSystem;
using WebExtension;
using BSystem.Text;
using BSystem.Collections;
using StoryViewer.BookEditor;
using SuouBase.Book;
namespace StoryViewer.BookEditor.Section
{
    [ToolboxItem(false)]
    public partial class SectionViewer : UserControl,IBookEditor
    {
        #region BaseProperty
        public event BookPageContentChangeEventHandler ItemClicked;
        private StoryViewerShortCutKeys _ShortCutKeys = null;
        public StoryViewerShortCutKeys ShortCutKeys
        {
            get { return this._ShortCutKeys; }
            set
            {
                this._ShortCutKeys = value;
            }
        }

        public ContextMenuStrip RightContextMenuStrip
        {
            get { return this.backGroundMenuStrip; }
        }
        public IBook _EditItem = null;
        public IBook EditItem
        {
            get
            {
                return _EditItem;
            }
            set
            {
                if (_EditItem != null) this._EditItem.ContentChanged -= this.Book_ContentChanged;
                _EditItem = value;
                if (this.DesignMode) return;
                if (_EditItem != null)
                {
                    this.set();
                    IsUnable = false;
                }
                else
                {
                    this.thumContViewBox1.Operator.Clear();
                    IsUnable = true;
                }
            }
        }

        public bool IsUnable { get; set; }

        public SmallInfo DLLSmallInfo
        {
            get
            {
                return this.DLLFunction.Info;
            }
        }
        public EditorInfo DLLFunction
        {
            get { return this.thumContViewBox1.Operator.DLLFunction; }
            set
            {
                this.thumContViewBox1.Operator.DLLFunction = value;
            }
        }
        bool IsDragAccepted
        {
            get;
            set;
        }
        public PageJumpBox CreagePageJumpBox()
        {
            return this.thumContViewBox1.CreatePageJumpBox();
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public ThumbnailView_SaveData SaveData
        {
            get
            {
                return this.thumContViewBox1.SaveData;
            }
            set
            {
                this.thumContViewBox1.SaveData = value;
            }
        }

        public int SelectedIndex
        {
            get { return this.thumContViewBox1.Operator.MapOperator.LastSelectedIndex; }
        }
        private TextThumbImage SelectedItem
        {
            get { return this.thumContViewBox1.Operator.MapOperator.LastSelectedItem; }
        }
        private SectionScene SelectedMergeBookPage
        {
            get { return (SectionScene)this.SelectedItem.Page; }
        }

        public event BookPageContentChangeEventHandler PageItemAccepted;
        #endregion
        #region Constructor
        public SectionViewer()
        {
            InitializeComponent();
            this.ShortCutKeys = new StoryViewerShortCutKeys();
        }
        #endregion
        #region Drug
        private void thumContViewBox1_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
            if ((e.KeyState & 2) == 2)
            {
                e.Action = DragAction.Cancel;
            }
        }
        private void thumContViewBox1_DragOver(object sender, DragEventArgs e)
        {
            this.thumContViewBox1.Operator.MoveDrag();
        }
        private void thumContViewBox1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var strs = (string[])e.Data.GetData(DataFormats.FileDrop);

                //AALoad(strs.First());

                if (DLLFunction != null)
                {
                    DLLFunction.FileData.AddFilePath(strs.First());
                }
                else
                {
                    AALoad(strs.First());
                }

            }
            else if (e.Data.GetDataPresent(typeof(TextThumbImage)))
            {

                if (this.EditItem != null)
                {
                    this.thumContViewBox1.FinishDrag();
                }
            }
        }
        private void thumContViewBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(System.Windows.Forms.DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Move;
            }
            else if (e.Data.GetDataPresent(typeof(TextThumbImage)))
            {
                if (this.EditItem == null)
                {
                    if (DLLFunction == null) return;
                    DLLFunction.FileData.AddNewFile();

                }
                if (this.EditItem != null)
                {
                    e.Effect = DragDropEffects.Copy;
                    var tti = ((TextThumbImage)e.Data.GetData(typeof(TextThumbImage)));
                    
                    this.DragInsert(tti.Decode());
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }
        public void DragInsert(IBookPage ibp)
        {

            if (this.IsDragAccepted) return;
            var page = new SectionScene(ibp);

            var f = new TextThumbImage(page, 0);
            this.thumContViewBox1.Operator.InsertDragItem(f);

            var pageeventarg = new BookPageContentChangeEventArgs(ibp, PageContentChangeType.Other);
            if (PageItemAccepted != null) PageItemAccepted(this, pageeventarg);
            this.IsDragAccepted = true;
        }

        #endregion
        #region Method
        public void UpdatePage()
        {
            this.thumContViewBox1.Operator.CheckChangePicture();
        }
        public void FocusPage(int n)
        {
            this.thumContViewBox1.Operator.MoveViewPoint(n);
            this.EditItem.SelectedPageIndex = n;
        }
        public void DeleteItem(int nth)
        {
            this.thumContViewBox1.Operator.Delete(nth);
        }
        private IBookPage GetBookPage(int index)
        {
            var v = this.thumContViewBox1.GetItem(index).Page;
            return v;
        }
        public KeyValuePair<ObsoleteSectionBook, ObsoleteSectionBook> Split()
        {
            var n = this.thumContViewBox1.Operator.MapOperator.LastSelectedIndex;
            return ((ObsoleteSectionBook)this.EditItem).Split(n);
        }
        private SectionScene NewPage()
        {
            var r = new SectionScene { JoinSpaceLine = this.thumContViewBox1.Operator.SaveData.Model.TopAppendSpaceCount };
            r.Parts.First().Merger = this.thumContViewBox1.SaveData.Model.DefaultMergeSet;
            return r;
        }
        private SectionScene NewPage(SuouText st)
        {
            var r = new SectionScene(st) { JoinSpaceLine = this.thumContViewBox1.Operator.SaveData.Model.TopAppendSpaceCount };
            r.Parts.First().Merger = this.thumContViewBox1.SaveData.Model.DefaultMergeSet;
            return r;
        }
        public void AALoad(string filename)
        {
            EditItem = new ObsoleteSectionBook(new EditingFile(filename));
            //this.AAThumContViewControl1.Item = v;
        }
        public void ReColor()
        {
            Color PageColor = Color.Aqua;
            foreach (TextThumbImage item in this.thumContViewBox1.Operator.RowItems)
            {
                if (item.Page is SectionScene)
                {
                    var itempage = (SectionScene)item.Page;
                    if (!itempage.JoinTop) PageColor = PageColor == Color.Aqua ? Color.Beige : Color.Aqua;
                    item.ControlBackColor = PageColor;
                }
                else
                {
                    PageColor = PageColor == Color.Aqua ? Color.Beige : Color.Aqua;
                    item.ControlBackColor = PageColor;
                }
            }
            this.thumContViewBox1.RefreshItems();
        }
        private void set()
        {
            this.EditItem.ContentChanged += this.Book_ContentChanged;
            if (this.EditItem is ObsoleteSectionBook)
            {
                this.thumContViewBox1.BoxType = ImageItemBoxType.Story;
                this.thumContViewBox1.SaveData.Model = ((ObsoleteSectionBook)this.EditItem).Model;
                this.ImageSetMergeBook();
                
            }
            else if (this.EditItem is EditingFile)
            {
                this.thumContViewBox1.BoxType = ImageItemBoxType.Normal;
                this.ImageSetEditingFile();
            }
        }
        public void ImageSetMergeBook()
        {

            Color PageColor = Color.Aqua;
            List<TextThumbImage> sparray = new List<TextThumbImage>();
            foreach (SectionScene ap in EditItem.BookPages)
            {
                if (!ap.JoinTop) PageColor = PageColor == Color.Aqua ? Color.Beige : Color.Aqua;
                var stt = new TextThumbImage(ap, 0) { ControlBackColor = PageColor };
                sparray.Add(stt);

            }
            this.thumContViewBox1.Operator.ImageItemSet(sparray.ToArray());
        }
        public void ImageSetEditingFile()
        {
            Color PageColor = Color.Aqua;
            List<TextThumbImage> sparray = new List<TextThumbImage>();
            foreach (AstPage ap in EditItem.BookPages)
            {
                PageColor = PageColor == Color.Aqua ? Color.Beige : Color.Aqua;
                var stt = new TextThumbImage(ap, 0) { ControlBackColor = PageColor };
                sparray.Add(stt);

            }
            this.thumContViewBox1.Operator.ImageItemSet(sparray.ToArray());
        }
        #endregion
        #region PublicMethod
        private void UnityWithUp(int nth)
        {

            var page = this.GetBookPage(nth);
            if (page is SectionScene)
            {
                var pPage = (SectionScene)page;
                pPage.JoinTop = !pPage.JoinTop;

                pPage.JoinSpaceLine = pPage.JoinTop ? this.thumContViewBox1.Operator.SaveData.Model.TopAppendSpaceCount : this.thumContViewBox1.Operator.SaveData.Model.CenterAppendSpaceCount;

                ReColor();
            }
        }

        private void MergeUp(int n)
        {
            var up = (SectionScene)this.thumContViewBox1.Operator.RowItems[n - 1].Page;
            var down = (SectionScene)this.thumContViewBox1.Operator.RowItems[n].Page;
            var r = ObsoleteSectionBook.MergePage(up, down);
            var item = new TextThumbImage(r, 0);
            this.thumContViewBox1.Operator.Delete(n);
            this.thumContViewBox1.Operator.Delete(n - 1);
            this.thumContViewBox1.Operator.Insert(n - 1, item);
        }
        public void AddNewPage()
        {
            var ap = NewPage();
            this.Paste(new[] { ap });
            //EditItem.Add(ap);
            //var f = new TextThumbImage(ap, 0);
            //this.thumContViewBox1.Operator.InsertToSelectedLocation(f);
        }
        public void Paste()
        {
            if (this.DLLFunction.Info.IsClipPage)
            {
                var p = this.DLLFunction.Info.GetCopy();
                this.Paste(p.Select((v) => v.ToMergeBookPage()));
            }

        }

        public void Paste(IEnumerable<SectionScene> aps)
        {
            var tms = aps.Select((v) => new TextThumbImage(v, 0));
            this.thumContViewBox1.Operator.InsertToSelectedLocation(tms);
        }
        public bool CanUndo
        {
            get { return false; }
        }
        public void Undo()
        {

        }
        public void Cut()
        {
            this.Copy();
            this.DeleteItem(this.SelectedIndex);
        }
        public void Copy()
        {
            this.SingleCopy();
        }
        public void SingleCopy()
        {
            if (this.DLLSmallInfo != null)
            {
                var p = this.SelectedItem.Page;
                this.DLLSmallInfo.Copy(p, this.thumContViewBox1.SaveData.Model.CopyEncodeType);
            }
        }
        public void ClusterCopy()
        {
            if (SelectedItem != null)
            {
                var p = this.thumContViewBox1.Operator.SelectedCluster;
                this.DLLFunction.Info.Copy(p.ClusterItem.Page, this.thumContViewBox1.SaveData.Model.CopyEncodeType);
            }
            else
            {
                throw new MyException("選択エラー");
            }

        }
        public bool CanAddNewPage
        {
            get { return this.EditItem != null; }
        }
        public bool CanAddNewSection { get { return false; } }
        public void AddNewSection()
        {
            throw new NotSupportedException();
        }
        public bool CanSettingModel
        {
            get
            {
                return (this.EditItem != null && this.EditItem is ObsoleteSectionBook);
            }
        }
        #endregion
        #region thumbnailEvent
        private void thumContViewBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.thumContViewBox1.Operator.MapOperator.SelectedItem != null)
            {
                var f = (IBookPage)this.SelectedItem.Page;
                if (DLLFunction != null) DLLFunction.MasterForm.NotifyPageSize(f.SText.GetSitarabaByteSize.ToString() + "Byte(したらば)");
            }
            else
            {
                if (DLLFunction != null) DLLFunction.MasterForm.NotifyPageSize("");
            }
        }
        private void thumContViewBox1_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            var page = e.Item.Page;
            this.EditItem.SelectedPageIndex = e.SelectedIndex;
            var ae = new SuouBase.BookPageContentChangeEventArgs(page, PageContentChangeType.Other);
            if (this.DLLFunction != null)
            {
                this.EditItem.SelectedPageIndex = this.EditItem.SelectedPageIndex;
            }
            if (this.ItemClicked != null) this.ItemClicked(this, ae);

        }
        private void thumContViewBox1_ItemDataChanged(object sender, ThumbnailChangeItemEventArgs e)
        {
            if (e.Mode == ThumbnailChangeItemMode.NewSet) return;
            if (e.Mode == ThumbnailChangeItemMode.Clear)
            {
                return;
            }
            else
            {
                var f = this.thumContViewBox1.Operator.RowItems;
                List<IBookPage> aps = new List<IBookPage>();
                foreach (var v in f)
                {
                    aps.Add((IBookPage)v.Page);
                }
                this.EditItem.BookPages = aps;
                ReColor();
            }
        }
        #endregion
        #region EventMethod
        private void timer1_Tick(object sender, EventArgs e)
        {
            var f = Control.MouseButtons;
            if (f != System.Windows.Forms.MouseButtons.Left && this.IsDragAccepted)
            {
                this.thumContViewBox1.FinishDrag();
                this.IsDragAccepted = false;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if (this.EditItem != null && this.EditItem is ObsoleteSectionBook)
            {
                foreach (SectionScene v in this.EditItem.BookPages)
                {
                    if (v.IsChanged)
                    {
                        //v.SText = v.Merge();
                    }
                }
            }
        }
        private void thumContViewBox1_ImageBoxMouseEnter(object sender, EventArgs e)
        {
            if (DLLFunction == null) return;
            var v = this.thumContViewBox1.Operator.SelectedCluster;
            var p = this.thumContViewBox1.Operator.MapOperator.MouseOnSelectedItem;

            if(v != null)DLLFunction.MasterForm.NotifyCaretInformation(v.ClusterItem.Size.ToString() + "Byte(したらば)");
            if(p != null)DLLFunction.MasterForm.NotifyPageSize(p.Page.SText.GetSitarabaByteSize.ToString() + "Byte(したらば)");
            
            
        }
        #endregion
        private void Book_ContentChanged(object sender,BookContentChangeEventArgs e)
        {
            if (e.Type == BookContentChangeType.PageContent)
            {
                if (e.PageEventArgs.Type == PageContentChangeType.JoinTop)
                {
                    this.ReColor();
                }
            }
        }

        private void clusterCopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ClusterCopy();
        }

        public void SettingModel()
        {
            var contro = new PageModelSetting() { Model = this.thumContViewBox1.SaveData.Model };
            var rst = SuouBase.Controls.Forms.SettingForm.Create(contro, "雛形の設定");
            if (rst == DialogResult.OK)
            {
                this.thumContViewBox1.SaveData.Model = contro.Model;
                ((ObsoleteSectionBook)this.EditItem).Model = this.thumContViewBox1.SaveData.Model;
                DialogResult result = MessageBox.Show("現在のセクションにこの雛形を適用しますか？",
                        "質問",
                        MessageBoxButtons.YesNoCancel,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
#pragma warning disable CS0612 // 型またはメンバーが旧型式です
                    SetModelToItem((ObsoleteSectionBook)this.EditItem);
#pragma warning restore CS0612 // 型またはメンバーが旧型式です
                }
            }
        }
        private void settingPageModelToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.SettingModel();
            
        }
        [Obsolete]
        public static void SetModelToItem(ObsoleteSectionBook book)
        {
            foreach (SectionScene v in book.BookPages)
            {
                v.JoinSpaceLine = v.JoinTop ? book.Model.CenterAppendSpaceCount : book.Model.TopAppendSpaceCount;
            }
        }
        /*
        public static void SetModelToItem(SectionNBook book)
        {
            foreach (SectionPage v in book.BookPages)
            {
                SetModelToItem(v, book.Model);
            }
        }
        */
        public static void ShowPageModelSettingDialog(ObsoleteSectionBook book, SuouBookPageModel model)
        {
            foreach (SectionScene v in book.BookPages)
            {
                v.JoinSpaceLine = v.JoinTop ? model.CenterAppendSpaceCount : model.TopAppendSpaceCount;
            }
        }

        private void WakuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.DLLFunction != null && this.DLLFunction.HukidasiSelecterForm != null)
            {
                var form = this.DLLFunction.HukidasiSelecterForm();
                var f = form.ShowDialog();
                if (f == DialogResult.OK)
                {
                    this.SelectedMergeBookPage.BackGround.Waku = form.SelectedSaveData;
                }
                //form.Dispose();
            }
        }
    }
}
