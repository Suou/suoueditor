﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using SuouBase;
using SuouBase.Interface;
using System.Windows.Forms;
using StoryViewer;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Base;
using BSystem;
using WebExtension;
using BSystem.Text;
using BSystem.Collections;
using StoryViewer.BookEditor;
using SuouBase.Book;

namespace StoryViewer.BookEditor.Section
{
    partial class SectionViewer
    {
        private void namingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var v = new SuouBase.Controls.InputForm() { StartPosition = FormStartPosition.CenterParent, Title = "名前を入れてください" };
            var rst = v.ShowDialog();
            if (rst == DialogResult.OK)
            {
                this.SelectedItem.Page.Name = v.Message;
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.SelectedItem != null)
            {
                this.thumContViewBox1.SetItemBoxOpen(this.thumContViewBox1.Operator.MapOperator.SelectedImageBoxItem);
            }
        }
        private void addNewPageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddNewPage();
        }
        private void confineToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        private void addNewPageToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AddNewPage();
        }
        private void mergeDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.SelectedIndex;
            MergeUp(n + 1);
        }
        private void hyperPasteToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        private void unityWithUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.EditItem is ObsoleteSectionBook)
            {
                var nth = this.SelectedIndex;
                UnityWithUp(nth);
            }
        }
        private void unityWithDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var nth = this.SelectedIndex;
            UnityWithUp(nth + 1);
        }
        private void sJIScopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (SelectedItem != null)
            {
                var f = (IBookPage)SelectedItem.Page;
                Clipboard.SetText(BSystem.Text.StringExtensions.ToParagraphJoin(f.SText.ToListString()).ToSjis10String());
            }
        }
        private void copyUTF8ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Copy();
        }
        private void mergeUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.SelectedIndex;
            MergeUp(n);
        }
        private void moveUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.thumContViewBox1.Operator.MapOperator.SelectedItemIndex;
            this.thumContViewBox1.Operator.MoveUp(SelectedIndex);

        }
        private void moveDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.thumContViewBox1.Operator.MoveDown(SelectedIndex);
        }
        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Cut();
        }
        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Paste();

        }
        private void serializeTestToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var p = (SectionScene)this.SelectedItem.Page;
            BSystem.XML.BinarySerialize.SaveToBinaryFile(p, "test.ser");
        }

        private void replaceBackGroundToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var page = this.DLLSmallInfo.GetCopy().First();
            this.SelectedMergeBookPage.BackGround.SText = page.SText;
            this.thumContViewBox1.Operator.MapOperator.SelectedImageBoxItem.RefreshFormize();
        }
        private void singleCopyStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SingleCopy();
        }


        private void showUsingMemoryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("{0:N0}", this.thumContViewBox1.Operator.UsingMemory));
        }

        private void backgroundCopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.DLLSmallInfo != null)
            {
                var p = this.SelectedItem.Page.ToMergeBookPage().Parts.First();
                this.DLLSmallInfo.Copy(p.SText.Text, this.SaveData.Model.CopyEncodeType);
            }
        }
        #region ScaleButtonMethod
        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            this.thumContViewBox1.Percent = 0.25F;
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            this.thumContViewBox1.Percent = 0.50F;
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            this.thumContViewBox1.Percent = 0.75F;
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            this.thumContViewBox1.ViewMode = ThumbnailMode.Raw;
        }
        #endregion
        #region ItemMenuButtonMethod

        public bool CanCut
        {
            get
            {
                return this.ParentForm != null && this.EditItem != null && this.SelectedIndex != -1;
            }
        }
        public bool CanCopy
        {
            get
            {
                return this.ParentForm != null && this.EditItem != null && this.SelectedIndex != -1;
            }
        }
        public bool CanPaste
        {
            get { return this.PasteEnable; }
        }
        public void ItemMenuCheck()
        {
            this.ItemMenuStrip.GetMenuItems().ForEach((v) => v.Enabled = false);
            this.cutToolStripMenuItem.Enabled = this.CanCut;
            this.copyToolStripMenuItem.Enabled = this.CanCopy;
            this.pasteToolStripMenuItem.Enabled = this.CanPaste;
            if (this.ParentForm != null && this.EditItem != null)
            {
                var SelectedNextIndex = this.SelectedIndex + 1;
                if (SelectedIndex == -1)
                {
                    this.addNewPageToolStripMenuItem.Enabled = true;
                }
                else
                {
                    this.addNewPageToolStripMenuItem.Enabled = true;
                    //this.cutToolStripMenuItem.Enabled = true;
                    //this.copyToolStripMenuItem.Enabled = true;
                    this.deleteToolStripMenuItem1.Enabled = true;
                    this.WakuToolStripMenuItem.Enabled = true;
                    this.replaceBackGroundToolStripMenuItem.Enabled = this.DLLSmallInfo.IsClipPage;
                    this.pasteToolStripMenuItem.Enabled = this.PasteEnable;
                    //this.hyperPasteToolStripMenuItem.Enabled = this.DLLFunction.CopyPages != null;
                    this.copyToolStripMenuItem.Enabled = true;
                    this.namingToolStripMenuItem.Enabled = true;
                    this.openToolStripMenuItem.Enabled = this.SelectedItem.IsCategory;
                    this.openToolStripMenuItem.Checked = this.thumContViewBox1.GetImageBoxItem(this.SelectedIndex).Open;

                    this.ItemMenu_SelectIndexCheck();
                    this.ItemMenu_NextIndexCheck();

                }
            }
            this.upDownOperateMenuItem.Enabled = true;
        }
        private void ItemMenu_SelectIndexCheck()
        {
            if (SelectedIndex != 0)
            {
                this.unityWithUpToolStripMenuItem.Enabled = true;
                this.mergeUpToolStripMenuItem.Enabled = true;
                this.moveUpToolStripMenuItem.Enabled = true;
                if (this.GetBookPage(SelectedIndex) is SectionScene)
                {
                    this.unityWithUpToolStripMenuItem.Checked = ((SectionScene)this.GetBookPage(SelectedIndex)).JoinTop;
                }
                else
                {
                    this.unityWithUpToolStripMenuItem.Checked = false;
                }
            }
            else
            {
                this.mergeUpToolStripMenuItem.Enabled = false;
                this.moveUpToolStripMenuItem.Enabled = false;
                this.unityWithUpToolStripMenuItem.Enabled = false;
            }
        }
        private void ItemMenu_NextIndexCheck()
        {
            var SelectedNextIndex = this.SelectedIndex + 1;
            if (SelectedNextIndex < this.EditItem.Count)
            {
                this.unityWithDownToolStripMenuItem.Enabled = true;
                var page = this.GetBookPage(SelectedIndex + 1);
                if (page is SectionScene)
                {
                    var pPage = (SectionScene)page;
                    this.unityWithDownToolStripMenuItem.Checked = pPage.JoinTop;
                }
                else
                {
                    this.unityWithDownToolStripMenuItem.Checked = false;
                }
                this.mergeDownToolStripMenuItem.Enabled = true;
                this.moveDownToolStripMenuItem.Enabled = true;
                //this.unityWithDownToolStripMenuItem.Text = this.EditItem.Pages[SelectedIndex + 1].JoinTop ? "下と分離" : "下と結合";
            }
            else
            {
                this.unityWithDownToolStripMenuItem.Enabled = false;
                this.mergeDownToolStripMenuItem.Enabled = false;
                this.moveDownToolStripMenuItem.Enabled = false;
            }
        }
        public void ItemMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            this.ItemMenuCheck();
        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (this.EditItem == null)
            {
                this.addNewPageToolStripMenuItem1.Enabled = false;
                this.pasteToolStripMenuItem1.Enabled = false;
            }
            else
            {
                this.addNewPageToolStripMenuItem1.Enabled = true;
                this.pasteToolStripMenuItem1.Enabled = true;
                this.pasteToolStripMenuItem1.Enabled = this.PasteEnable;
            }
            this.ViewScale100Strip.Checked = this.thumContViewBox1.ViewMode == ThumbnailMode.Raw;
            this.ViewScale75Strip.Checked = this.thumContViewBox1.Percent == 0.75F;
            this.ViewScale50Strip.Checked = this.thumContViewBox1.Percent == 0.50F;
            this.ViewScale25Strip.Checked = this.thumContViewBox1.Percent == 0.25F;

        }
        #endregion
        #region ButtonMethod


        public bool PasteEnable
        {
            get
            {
                return this.DLLFunction.Info.IsClipPage;
            }
        }
        private void deleteToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var nth = this.SelectedIndex;
            this.DeleteItem(nth);
        }
        #endregion
    }
}
