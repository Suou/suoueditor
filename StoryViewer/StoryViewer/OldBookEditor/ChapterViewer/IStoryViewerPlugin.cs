﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
using StoryViewer.BookEditor.Section;
using SuouBase.Text;
using StoryViewer.AASelecter;
using System.Drawing;
using SuouBase.Book;
namespace StoryViewer.IStoryViewer
{
    public class IStoryViewerPlugin:IStoryPageListClass
    {
        //public StoryViewerShortCutSettingPage SettingPage2 = new StoryViewerShortCutSettingPage();
        public SettingPage[] SettingPage { get {
            return new SettingPage[] {  }; } }
        public IBookPage SelectedPage { get; set; }
        public EditorInfo DLLFunction { get; set; }
        public StoryViewer.BookEditor.ViewerToolStripMenuItem MenuItem = new BookEditor.ViewerToolStripMenuItem() { Text = "ページ"};
        public event BookPageContentChangeEventHandler ViewerPageSelected;
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public IBook Item
        {
            get
            {
                return this.HaveControl.Item;
            }
            set
            {
                this.HaveControl.Item = value;
            }
        }
        public ObsoleteChapterBook BItem
        {
            get { return (ObsoleteChapterBook)this.HaveControl.Item; }
        }
        public void Call()
        {
            this.HaveControl.UpdatePage();
        }
        private void SetItem()
        {
        }
        public void FocusPage(int n)
        {
            this.HaveControl.FocusPage(n);
            
        }
        public AstPage[] CopyPages
        {
            get { return null; }
            set {}
        }
        public string SaveFileName = SuouEditorConst.SettingDirectory + "/" + "SuperStoryViewer.stn";

        public AllStoryViewer HaveControl = new AllStoryViewer();
        public ContainerControl Control
        {
            get { return HaveControl; }
        }
        public Form Form
        {
            get { return null; }
        }
        public bool DisposeCheck()
        {
            BSystem.XML.NonBinarySerializer.Save(SaveFileName, this.HaveControl.Savedata);
            return true;
        }
        public void Paste(IBookPage ap)
        {
            
        }
        public ToolStripItemCollection ToolStripItems
        {
            get
            {
                return null;
            }
        }
        public void CheckToolStripItems()
        {
        }

        public IStoryViewerPlugin(EditorInfo dLLFunction)
        {

            this.DLLFunction = dLLFunction;
            this.MenuItem.HoldingEditor = this.HaveControl;
            //DLLFunction.MasterForm.StoryListMenuItem.DropDownOpening += this.StoryListMenuItem_Opening;
            var f = BSystem.XML.NonBinarySerializer.Load<StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.BookGrouperSavedata>(SaveFileName);
            if (f != null) this.HaveControl.Savedata = f;

            DLLFunction.FileData.TabChanged += this.SuperVisor_PageChanged;
            this.HaveControl.SetDLLFunction(this.DLLFunction);
            this.HaveControl.ItemClicked += this.PageSelected;

            this.DLLFunction.StartSettingEnded += this.DLLFunction_StartSettingEnded;
        }
        private void DLLFunction_StartSettingEnded(object sender, EventArgs e)
        {
            this.HaveControl.ParentForm.MainMenuStrip.Items.Insert(1, this.MenuItem);
        }
        private ToolStripMenuItem CreateMenuItem()
        {
            var f = new ToolStripMenuItem() { Text = "ページ" };
            f.DropDownOpening += this.StoryListMenuItem_Opening;
            return f;
        }
        private void HaveControl_PageChanged(object sender, BookPageContentChangeEventArgs e)
        {
            PageSelected(sender, e);
        }
        private void HaveControl_PageSelected(object sender, BookPageContentChangeEventArgs e)
        { 
            SelectedPage = e.Page;
            PageSelected(sender, e);
        }

        private void PageSelected(object sender, BookPageContentChangeEventArgs e)
        {

            if (ViewerPageSelected != null) ViewerPageSelected(sender, e);
            if (DLLFunction != null && DLLFunction.TextBoxPlugin != null)
            {

                DLLFunction.TextBoxPlugin.Page = e.Page;
            }
        }
        private void SuperVisor_PageChanged(object sender, SuouBase.Interface.SuouEditorDataClass data)
        {
            if (data == null)
            {
                this.Item = null;
            }
            else
            {
                this.Item = data.StoryFile;
            }
        }
        private void PageItem_Accepted(object sender, BookPageContentChangeEventArgs e)
        {
            var f = DLLFunction.PluginList.FirstOrDefault((v) => v.Plugin is IAASelectClass);
            if (f != null)
            {
                var pf = (IAASelectClass)f.Plugin;
                pf.NotifyDropPageItem(e.Page);
            }
        }
        private void StoryListMenuItem_Opening(object sender, EventArgs e)
        {
            
            var item = (ToolStripMenuItem)sender;
            //this.HaveControl.st.ItemMenuStrip_Opening(this, null);
            item.DropDownItems.Clear();
            if(this.HaveControl.RightContextMenuStrip != null)SuouBase.MenuStripMethod.CopyStripList(item.DropDownItems, this.HaveControl.RightContextMenuStrip.Items);
            
        }
        
    }
}
