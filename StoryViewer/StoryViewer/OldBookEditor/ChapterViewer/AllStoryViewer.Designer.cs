﻿namespace StoryViewer.IStoryViewer
{
    partial class AllStoryViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.storyViewerBox21 = new StoryViewer.BookEditor.Section.SectionViewer();
            this.bookGrouperViewer1 = new StoryViewer.IStoryViewer.BookGrouperViewerF.BookGrouperViewer();
            this.SuspendLayout();
            // 
            // storyViewerBox21
            // 
            this.storyViewerBox21.AllowDrop = true;
            this.storyViewerBox21.DLLFunction = null;
            this.storyViewerBox21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.storyViewerBox21.EditItem = null;
            this.storyViewerBox21.IsUnable = false;
            this.storyViewerBox21.Location = new System.Drawing.Point(0, 0);
            this.storyViewerBox21.Name = "storyViewerBox21";
            this.storyViewerBox21.ShortCutKeys = null;
            this.storyViewerBox21.Size = new System.Drawing.Size(409, 377);
            this.storyViewerBox21.TabIndex = 0;
            this.storyViewerBox21.ItemClicked += new SuouBase.BookPageContentChangeEventHandler(this.storyViewerBox21_ItemClicked);
            // 
            // bookGrouperViewer1
            // 
            this.bookGrouperViewer1.Dock = System.Windows.Forms.DockStyle.Top;
            this.bookGrouperViewer1.Item = null;
            this.bookGrouperViewer1.Location = new System.Drawing.Point(0, 0);
            this.bookGrouperViewer1.Name = "bookGrouperViewer1";
            this.bookGrouperViewer1.Size = new System.Drawing.Size(409, 166);
            this.bookGrouperViewer1.TabIndex = 1;
            this.bookGrouperViewer1.Visible = false;
            // 
            // AllStoryViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.bookGrouperViewer1);
            this.Controls.Add(this.storyViewerBox21);
            this.Name = "AllStoryViewer";
            this.Size = new System.Drawing.Size(409, 377);
            this.ResumeLayout(false);

        }

        #endregion

        private StoryViewer.BookEditor.Section.SectionViewer storyViewerBox21;
        private BookGrouperViewerF.BookGrouperViewer bookGrouperViewer1;
    }
}
