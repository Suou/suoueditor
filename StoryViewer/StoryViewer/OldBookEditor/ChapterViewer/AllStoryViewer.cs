﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StoryViewer.StoryViewerEdit;
using StoryViewer.IStoryViewer.BookGrouperViewerF;
using SuouBase.Text;
using SuouBase;
using SuouBase.Interface;
using StoryViewer.BookEditor;
using SuouBase.Book;
namespace StoryViewer.IStoryViewer
{
    [ToolboxItem(false)]
    public partial class AllStoryViewer : UserControl, IBookEditor
    {
        public AllStoryViewer()
        {
            InitializeComponent();
        }

        public void SetDLLFunction(EditorInfo dllfunction)
        {
            this.storyViewerBox21.DLLFunction = dllfunction;
            this.bookGrouperViewer1.DLLFunction = dllfunction;
            this.bookGrouperViewer1.ItemClicked += (sender, e) => { if (this.ItemClicked != null)this.ItemClicked(this, e); };
            this.storyViewerBox21.ItemClicked += (sender, e) => { if (this.ItemClicked != null)this.ItemClicked(this, e); };
        }
        
        private IBook _Item = null;
        public IBook Item
        {
            get
            {
                return this._Item;
            }
            set
            {
                this._Item = value;
                this.Set();
            }
        }

        public StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.BookGrouperSavedata Savedata
        {
            get { return this.bookGrouperViewer1.Savedata; }
            set
            {
                if (value != null)
                {
                    this.bookGrouperViewer1.Savedata = value;
                    this.storyViewerBox21.SaveData = (value.SavedataA);
                }
            }
        }
        private IBookEditor SelectedSubEditor
        {
            get
            {
                if (this.Item is ObsoleteChapterBook)
                {
                    return this.bookGrouperViewer1;
                }
                else if (this.Item != null)
                {
                    return this.storyViewerBox21;
                }
                else
                {
                    return null;
                }
            }
        }
        public ContextMenuStrip RightContextMenuStrip
        {
            get
            {
                if (this.SelectedSubEditor != null) return this.SelectedSubEditor.RightContextMenuStrip;
                return null;
            }
        }
        public void UpdatePage()
        {
            if (this.Item is ObsoleteChapterBook)
            {
                this.bookGrouperViewer1.UpdatePage();
            }
            else if (this.Item is ObsoleteSectionBook)
            {
                this.storyViewerBox21.UpdatePage();
            }
            else if(this.Item is EditingFile)
            {
                this.storyViewerBox21.UpdatePage();
            }
        }
        public void FocusPage(int n)
        {
            if (this.Item is ObsoleteChapterBook)
            {
                this.bookGrouperViewer1.FocusPage(n);
            }
            else if (this.Item is ObsoleteSectionBook)
            {
                this.storyViewerBox21.FocusPage(n);
            }
            else
            {
            }
        }
        private void Set()
        {
            this.bookGrouperViewer1.Visible = false;
            this.storyViewerBox21.Visible = false;
            if (this.Item == null)
            {
                this.bookGrouperViewer1.Item = null;
                this.storyViewerBox21.EditItem = null;

                this.bookGrouperViewer1.Visible = true;
                this.bookGrouperViewer1.Dock = DockStyle.Fill;
                return;
            }
            if (this.Item is ObsoleteChapterBook)
            {
                this.bookGrouperViewer1.Item = (ObsoleteChapterBook)this.Item;
                this.bookGrouperViewer1.Visible = true;
                this.bookGrouperViewer1.Dock = DockStyle.Fill;
            }
            else if (this.Item is ObsoleteSectionBook)
            {
                this.storyViewerBox21.EditItem = this.Item;
                this.storyViewerBox21.Visible = true;
                this.storyViewerBox21.Dock = DockStyle.Fill;
            }
            else if(this.Item is EditingFile)
            {
                this.storyViewerBox21.EditItem = this.Item;
                this.storyViewerBox21.Visible = true;
                this.storyViewerBox21.Dock = DockStyle.Fill;
            }
        }
        public event BookPageContentChangeEventHandler ItemClicked;
        private void storyViewerBox21_ItemClicked(object sender, SuouBase.BookPageContentChangeEventArgs e)
        {
            if (this.ItemClicked != null) this.ItemClicked(this, e);
        }
        public bool CanCut
        {
            get 
            {
                if (this.SelectedSubEditor == null) return false;
                return this.SelectedSubEditor.CanCut;
            }
        }
        public bool CanCopy
        {
            get
            {
                if (this.SelectedSubEditor == null) return false;
                return this.SelectedSubEditor.CanCopy;
            }
        }
        public bool CanPaste
        {
            get
            {
                if (this.SelectedSubEditor == null) return false;
                return this.SelectedSubEditor.CanPaste;
            }
        }
        public bool CanUndo
        {
            get
            {
                if (this.SelectedSubEditor == null) return false;
                return this.SelectedSubEditor.CanUndo;
            }
        }
        public bool CanSettingModel
        {
            get
            {
                if (this.Item == null)
                {
                    return false;
                }
                if (this.Item is ObsoleteChapterBook || this.Item is ObsoleteSectionBook)
                {
                    return true;
                }
                else if (this.Item is EditingFile)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
        }
        public void SettingModel()
        {
            this.SelectedSubEditor.SettingModel();
        }
        public void Undo()
        {
            this.SelectedSubEditor.Undo();
        }
        public void Cut()
        {
            this.SelectedSubEditor.Cut();
        }
        public void Paste()
        {
            this.SelectedSubEditor.Paste();
        }
        public void Copy()
        {
            this.SelectedSubEditor.Copy();
        }
        public void AddNewPage()
        {
            this.SelectedSubEditor.AddNewPage();
        }
        public void AddNewSection()
        {
            this.SelectedSubEditor.AddNewSection();
        }

        public bool CanAddNewPage
        {
            get
            {
                if (this.SelectedSubEditor == null) return false;
                return this.SelectedSubEditor.CanAddNewPage; 
            }
        }
        public bool CanAddNewSection
        {
            get
            {
                if (this.SelectedSubEditor == null) return false;
                return this.SelectedSubEditor.CanAddNewSection; 
            }
        }
    }
}
