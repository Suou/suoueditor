﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using System.Windows.Forms;
namespace StoryViewer.BookEditor.ChapterViewer
{
    public class ChapterListBoxPlugin : SuouBase.Interface.AbstractSimplePlugin
    {
        public ChapterListBoxPlugin(EditorInfo dLLFunction)
            : base()
        {
            var p = new UserControl1();
            p.Name = "セクションリスト";
            var cp = new ChapterListBox();
            p.Controls.Add(cp);
            cp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Control = p;
            this.Item = cp;
            this.Info = dLLFunction;
            cp.DoubleClick += this.Item_Clicked;

            this.Info.FileData.TabChanged += this.Item_Changed;
        }
        private EditorInfo Info { get; set; }
        private ChapterListBox Item { get; set; }
        private void Item_Changed(object sender, SuouBase.Interface.SuouEditorDataClass e)
        {
            if (e != null)
            {
                var p = e.StoryFile as SuouBase.Book.Chapter.ChapterBook2;
                this.Item.Book = p;
            }
        }
        private void Item_Clicked(object sender, EventArgs e)
        {
            var index = this.Item.SelectedIndex;
            if (index != -1)
            {
                this.Item.Book.SelectedPageIndex = index;
                /*
                var p = this.Info.StoryPageViewPlugin as SuouBase.Interface.IChapterEditorPlugin;
                if (p != null)
                {
                    p.FocusSection(index);
                }
                */
                this.Item.UpdateDisplayMember();
            }
        }
    }
}
