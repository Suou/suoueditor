﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Book;

namespace StoryViewer.BookEditor.ChapterViewer
{
    public partial class ChapterListBox : ListBox
    {
        StoryViewer.ImageViewer.DragStartMix Dragger { get; set; }
        public ChapterListBox()
        {
            InitializeComponent();
            this.Dragger = new ImageViewer.DragStartMix(this);
            this.Dragger.DragStarted += this.Drag_Started;
        }
        private void Drag_Started(object sender, EventArgs e)
        {
            if(this.MouseDownIndex != -1)this.DoDragDrop(this.MouseDownIndex, DragDropEffects.Move);
        }
        private SuouBase.Book.Chapter.ChapterBook2 _Book = null;
        public SuouBase.Book.Chapter.ChapterBook2 Book
        {
            get { return this._Book; }
            set
            {
                if (this._Book != null) this._Book.ContentChanged -= this.Book_ContentChanged;
                this._Book = value;
                this.UpdateView();
                if (this._Book != null) this._Book.ContentChanged += this.Book_ContentChanged;
                
            }
        }
        public void UpdateView()
        {
            this.Items.Clear();
            if (this.Book == null) return;
            for (int i = 0; i < this.Book.Count; i++)
            {
                var v = new ChapterListBoxItem(this.Book, i);
                this.Items.Add(v);
            }
        }
        public void UpdateDisplayMember()
        {
            var p = this.DisplayMember;
            this.DisplayMember = null;
            this.DisplayMember = p;
        }
        private void Book_ContentChanged(object sender,EventArgs e)
        {
            this.UpdateView();
        }

        private void ChapterListBox_MouseUp(object sender, MouseEventArgs e)
        {
            // 右クリックされた？
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                // マウス座標から選択すべきアイテムのインデックスを取得
                int index = this.IndexFromPoint(e.Location);

                // インデックスが取得できたら
                if (index >= 0)
                {
                    // すべての選択状態を解除してから
                    this.ClearSelected();

                    // アイテムを選択
                    this.SelectedIndex = index;
                }
                // コンテキストメニューを表示
                Point pos = this.PointToScreen(e.Location);
                contextMenuStrip1.Show(pos);
            }
        }
        private int MouseDownIndex = -1;
        private void ChapterListBox_MouseDown(object sender, MouseEventArgs e)
        {

            var index = this.IndexFromPoint(e.Location);
            this.MouseDownIndex = index;
        }

        private void ChapterListBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.AllowedEffect == DragDropEffects.Move && e.Data.GetDataPresent(typeof(int)))
            {
                e.Effect = DragDropEffects.Move;
            }
        }
        private void ChapterListBox_DragDrop(object sender, DragEventArgs e)
        {
            
            if (e.AllowedEffect == DragDropEffects.Move && e.Data.GetDataPresent(typeof(int)))
            {
                var itemIndex = (int)e.Data.GetData(typeof(int));
                var nowItemIndex = this.IndexFromPoint(this.PointToClient(Control.MousePosition));
                nowItemIndex = nowItemIndex == -1 ? this.Book.Count - 1 : nowItemIndex;
                if (nowItemIndex != -1)
                {
                    this.Book.Move(itemIndex, nowItemIndex);
                }
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (this.SelectedIndex != -1)
            {
                this.mergeUpMenuItem1.Visible = true;
                this.mergeDownMenuItem2.Visible = true;
                this.deleteMenuItem3.Visible = true;
                this.changeNameMenuItem4.Visible = true;
                this.moveUpMenuItem5.Visible = true;
                this.moveDownMenuItem6.Visible = true;
            }
            else
            {
                this.mergeUpMenuItem1.Visible = false;
                this.mergeDownMenuItem2.Visible = false;
                this.deleteMenuItem3.Visible = false;
                this.changeNameMenuItem4.Visible = false;
                this.moveUpMenuItem5.Visible = false;
                this.moveDownMenuItem6.Visible = false;
            }
            if (this.Book.Count == 1)
            {
                this.deleteMenuItem3.Enabled = false;
            }
            if (this.SelectedIndex == 0)
            {
                this.moveUpMenuItem5.Enabled = false;
                this.mergeUpMenuItem1.Enabled = false;
            }
            if (this.SelectedIndex == this.Book.Count - 1)
            {
                this.moveDownMenuItem6.Enabled = false;
                this.mergeDownMenuItem2.Enabled = false;
            }
        }

        private void mergeUpMenuItem1_Click(object sender, EventArgs e)
        {

        }

        private void mergeDownMenuItem2_Click(object sender, EventArgs e)
        {

        }

        private void deleteMenuItem3_Click(object sender, EventArgs e)
        {
            this.Book.Delete(this.SelectedIndex);
        }

        private void changeNameMenuItem4_Click(object sender, EventArgs e)
        {
            var form = this.FindForm();
            var page = this.Book.Pages[this.SelectedIndex];
            var f = SuouBase.Controls.InputForm.CreateNameBox("名前の変更", page.Name,form,FormStartPosition.CenterParent);
            if (f != null)
            {
                page.Name = f;
                //this.Set();
            }
            this.UpdateDisplayMember();
        }

        private void moveUpMenuItem5_Click(object sender, EventArgs e)
        {
            this.Book.Move(this.SelectedIndex, this.SelectedIndex - 1);
        }

        private void moveDownMenuItem6_Click(object sender, EventArgs e)
        {
            this.Book.Move(this.SelectedIndex, this.SelectedIndex + 1);
        }

        private void createNewPageMenuItem7_Click(object sender, EventArgs e)
        {
            var page = this.Book.CreateNewPage();
            if (this.SelectedIndex == -1)
            {
                this.Book.Insert(this.Book.Count, page);
            }
            else
            {
                this.Book.Insert(this.SelectedIndex, page);                
            }
        }

        private void setModelMenuItem8_Click(object sender, EventArgs e)
        {
            PageModelSetting.SettingModel(this.Book);
        }
        
    }
    public class ChapterListBoxItem
    {
        public SuouBase.Book.Chapter.ChapterBook2 Book { get; set; }
        public int Index { get; set; }
        public ChapterListBoxItem(SuouBase.Book.Chapter.ChapterBook2 book, int index)
        {
            this.Book = book;
            this.Index = index;
        }
        public string Name
        {
            get
            {
                var p = this.Book.Pages[this.Index];
                var str = string.Format("{0}({1})", p.Name, p.Items.Count);
                if (this.Book.SelectedPageIndex == this.Index) str = '*' + str;
                return str;
            }
        }
    }
}
