﻿namespace StoryViewer.BookEditor.ChapterViewer
{
    partial class ChapterListBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mergeUpMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeDownMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.changeNameMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.moveUpMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownMenuItem6 = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewPageMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
            this.setModelMenuItem8 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mergeUpMenuItem1,
            this.mergeDownMenuItem2,
            this.deleteMenuItem3,
            this.changeNameMenuItem4,
            this.moveUpMenuItem5,
            this.moveDownMenuItem6,
            this.toolStripSeparator1,
            this.createNewPageMenuItem7,
            this.setModelMenuItem8});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(197, 186);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // mergeUpMenuItem1
            // 
            this.mergeUpMenuItem1.Name = "mergeUpMenuItem1";
            this.mergeUpMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.mergeUpMenuItem1.Text = "上のセクションと融合";
            this.mergeUpMenuItem1.Click += new System.EventHandler(this.mergeUpMenuItem1_Click);
            // 
            // mergeDownMenuItem2
            // 
            this.mergeDownMenuItem2.Name = "mergeDownMenuItem2";
            this.mergeDownMenuItem2.Size = new System.Drawing.Size(196, 22);
            this.mergeDownMenuItem2.Text = "下のセクションと融合";
            this.mergeDownMenuItem2.Click += new System.EventHandler(this.mergeDownMenuItem2_Click);
            // 
            // deleteMenuItem3
            // 
            this.deleteMenuItem3.Name = "deleteMenuItem3";
            this.deleteMenuItem3.Size = new System.Drawing.Size(196, 22);
            this.deleteMenuItem3.Text = "削除";
            this.deleteMenuItem3.Click += new System.EventHandler(this.deleteMenuItem3_Click);
            // 
            // changeNameMenuItem4
            // 
            this.changeNameMenuItem4.Name = "changeNameMenuItem4";
            this.changeNameMenuItem4.Size = new System.Drawing.Size(196, 22);
            this.changeNameMenuItem4.Text = "名前を変更";
            this.changeNameMenuItem4.Click += new System.EventHandler(this.changeNameMenuItem4_Click);
            // 
            // moveUpMenuItem5
            // 
            this.moveUpMenuItem5.Name = "moveUpMenuItem5";
            this.moveUpMenuItem5.Size = new System.Drawing.Size(196, 22);
            this.moveUpMenuItem5.Text = "上に移動";
            this.moveUpMenuItem5.Click += new System.EventHandler(this.moveUpMenuItem5_Click);
            // 
            // moveDownMenuItem6
            // 
            this.moveDownMenuItem6.Name = "moveDownMenuItem6";
            this.moveDownMenuItem6.Size = new System.Drawing.Size(196, 22);
            this.moveDownMenuItem6.Text = "下へ移動";
            this.moveDownMenuItem6.Click += new System.EventHandler(this.moveDownMenuItem6_Click);
            // 
            // createNewPageMenuItem7
            // 
            this.createNewPageMenuItem7.Name = "createNewPageMenuItem7";
            this.createNewPageMenuItem7.Size = new System.Drawing.Size(196, 22);
            this.createNewPageMenuItem7.Text = "新しいページを追加";
            this.createNewPageMenuItem7.Click += new System.EventHandler(this.createNewPageMenuItem7_Click);
            // 
            // setModelMenuItem8
            // 
            this.setModelMenuItem8.Name = "setModelMenuItem8";
            this.setModelMenuItem8.Size = new System.Drawing.Size(196, 22);
            this.setModelMenuItem8.Text = "雛形設定";
            this.setModelMenuItem8.Click += new System.EventHandler(this.setModelMenuItem8_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(193, 6);
            // 
            // ChapterListBox
            // 
            this.AllowDrop = true;
            this.DisplayMember = "Name";
            this.ItemHeight = 12;
            this.Size = new System.Drawing.Size(120, 88);
            this.ValueMember = "Name";
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.ChapterListBox_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.ChapterListBox_DragEnter);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ChapterListBox_MouseDown);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ChapterListBox_MouseUp);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem mergeUpMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem mergeDownMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem changeNameMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem moveUpMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem moveDownMenuItem6;
        private System.Windows.Forms.ToolStripMenuItem createNewPageMenuItem7;
        private System.Windows.Forms.ToolStripMenuItem setModelMenuItem8;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    }
}
