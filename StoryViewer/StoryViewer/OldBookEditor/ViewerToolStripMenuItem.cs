﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;

namespace StoryViewer.BookEditor
{
    public class ViewerToolStripMenuItem:ToolStripMenuItem
    {
        private ToolStripMenuItem undoStripMenuItem;
        private ToolStripMenuItem pasteStripMenuItem;
        private ToolStripMenuItem cutStripMenuItem;
        private ToolStripMenuItem copyStripMenuItem;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem addSectionStripMenuItem;
        private ToolStripMenuItem addNewPageStripMenuItem1;
        private ToolStripMenuItem ModelSettingMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        public IBookEditor HoldingEditor { get; set; }
        public ViewerToolStripMenuItem():base()
        {
            this.InitializeComponent();
        }
        private void InitializeComponent()
        {
            this.undoStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.addSectionStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewPageStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ModelSettingMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            // 
            // undoStripMenuItem
            // 
            this.undoStripMenuItem.Name = "undoStripMenuItem";
            this.undoStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.undoStripMenuItem.Text = "元に戻す";
            this.undoStripMenuItem.Click += new System.EventHandler(this.undoStripMenuItem_Click);
            // 
            // pasteStripMenuItem
            // 
            this.pasteStripMenuItem.Name = "pasteStripMenuItem";
            this.pasteStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.pasteStripMenuItem.Text = "貼り付け";
            this.pasteStripMenuItem.Click += new System.EventHandler(this.pasteStripMenuItem_Click);
            // 
            // cutStripMenuItem
            // 
            this.cutStripMenuItem.Name = "cutStripMenuItem";
            this.cutStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.cutStripMenuItem.Text = "切り取り";
            this.cutStripMenuItem.Click += new System.EventHandler(this.cutStripMenuItem_Click);
            // 
            // copyStripMenuItem
            // 
            this.copyStripMenuItem.Name = "copyStripMenuItem";
            this.copyStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.copyStripMenuItem.Text = "コピー";
            this.copyStripMenuItem.Click += new System.EventHandler(this.copyStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(265, 6);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(265, 6);
            // 
            // addSectionStripMenuItem
            // 
            this.addSectionStripMenuItem.Name = "addSectionStripMenuItem";
            this.addSectionStripMenuItem.Size = new System.Drawing.Size(268, 22);
            this.addSectionStripMenuItem.Text = "新しいセクションを追加";
            this.addSectionStripMenuItem.Click += new System.EventHandler(this.addSectionStripMenuItem_Click);
            // 
            // addNewPageStripMenuItem1
            // 
            this.addNewPageStripMenuItem1.Name = "addNewPageStripMenuItem1";
            this.addNewPageStripMenuItem1.Size = new System.Drawing.Size(268, 22);
            this.addNewPageStripMenuItem1.Text = "新しいページを追加";
            this.addNewPageStripMenuItem1.Click += new System.EventHandler(this.addNewPageStripMenuItem1_Click);
            // 
            // ModelSettingMenuItem
            // 
            this.ModelSettingMenuItem.Name = "ModelSettingMenuItem";
            this.ModelSettingMenuItem.Size = new System.Drawing.Size(268, 22);
            this.ModelSettingMenuItem.Text = "このブックの雛形ページを設定する";
            this.ModelSettingMenuItem.Click += new System.EventHandler(this.ModelSettingMenuItem_Click);
            // 
            // ViewerToolStripMenuItem
            // 
            this.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoStripMenuItem,
            this.toolStripSeparator1,
            this.addNewPageStripMenuItem1,
            this.cutStripMenuItem,
            this.copyStripMenuItem,
            this.pasteStripMenuItem,
            this.toolStripSeparator2,
            this.addSectionStripMenuItem,
            this.ModelSettingMenuItem});
            this.Text = "ページ";
            this.DropDownOpening += new System.EventHandler(this.ViewerToolStripMenuItem_DropDownOpening);

        }

        private void ViewerToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            this.addNewPageStripMenuItem1.Enabled = this.HoldingEditor.CanAddNewPage;
            this.addSectionStripMenuItem.Enabled = this.HoldingEditor.CanAddNewSection;
            this.undoStripMenuItem.Enabled = this.HoldingEditor.CanUndo;
            this.pasteStripMenuItem.Enabled = this.HoldingEditor.CanPaste;
            this.cutStripMenuItem.Enabled = this.HoldingEditor.CanCut;
            this.copyStripMenuItem.Enabled = this.HoldingEditor.CanCopy;
            this.ModelSettingMenuItem.Enabled = this.HoldingEditor.CanSettingModel;
        }

        private void copyStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.HoldingEditor.CanCopy)this.HoldingEditor.Copy();
        }

        private void cutStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.HoldingEditor.CanCut)this.HoldingEditor.Cut();
        }

        private void pasteStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.HoldingEditor.CanPaste)this.HoldingEditor.Paste();
        }

        private void undoStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.HoldingEditor.CanUndo)this.HoldingEditor.Undo();
        }

        private void addNewPageStripMenuItem1_Click(object sender, EventArgs e)
        {
            if(this.HoldingEditor.CanAddNewPage)this.HoldingEditor.AddNewPage();
        }

        private void addSectionStripMenuItem_Click(object sender, EventArgs e)
        {
            if(this.HoldingEditor.CanAddNewSection)this.HoldingEditor.AddNewSection();
        }

        private void ModelSettingMenuItem_Click(object sender, EventArgs e)
        {
            if (this.HoldingEditor.CanSettingModel) this.HoldingEditor.SettingModel();
        }
    }
}
