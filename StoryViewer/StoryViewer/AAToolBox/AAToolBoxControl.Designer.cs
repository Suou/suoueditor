﻿namespace StoryViewer
{
    partial class AAToolBoxControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            SuouBase.Text.IBookCollection iBookCollection1 = new SuouBase.Text.IBookCollection();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.montageBox1 = new StoryViewer.MontageBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(266, 280);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.montageBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(258, 254);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "モンタージュ";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // montageBox1
            // 
            iBookCollection1.Name = "Data/Montage";
            this.montageBox1.Books = iBookCollection1;
            this.montageBox1.DLLFunction = null;
            this.montageBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.montageBox1.Location = new System.Drawing.Point(0, 0);
            this.montageBox1.MatchedItems = null;
            this.montageBox1.MontageText = null;
            this.montageBox1.Name = "montageBox1";
            this.montageBox1.Size = new System.Drawing.Size(258, 254);
            this.montageBox1.TabIndex = 0;
            // 
            // AAToolBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Name = "AAToolBox";
            this.Size = new System.Drawing.Size(266, 280);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private MontageBox montageBox1;

    }
}
