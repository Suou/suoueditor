﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using System.IO;
using SuouBase.Interface;
using SuouBase.Text;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Book;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class LargeCharBox : UserControl
    {
        public LargeCharBox()
        {
            InitializeComponent();

            var v = this.aaThumContViewControl1.GetSaveData();
            v.Percent = 1;
            v.ViewMode = SuouBase.Drawing.ThumbnailMode.Percent;
            this.aaThumContViewControl1.SetSaveData(v);

            this.InitialLoad();

#if DEBUG
            this.button1.Visible = true;
#endif
        }
        public EditorInfo DLLFunction
        {
            get;
            set;
        }
        public List<IBookCollection> RegisterItems { get; private set; }
        public IBookCollection SelectedItem
        {
            get
            {
                var v = this.comboBox1.SelectedItem;
                if (v != null)
                {
                    return (IBookCollection)v;
                }
                else
                {
                    return null;
                }
            }
        }
        private void CreateLargeCharItem()
        {
            
            var v = this.SelectedItem;
            if (v != null)
            {
                this.SelectedLargeCharItem = new LargeSuouText(v.Create());
            }
            
        }
        private LargeSuouText _SelectedLargeCharItem = null;
        public LargeSuouText SelectedLargeCharItem
        {
            get { return this._SelectedLargeCharItem; }
            set
            {
                this._SelectedLargeCharItem = value;
                if (value != null)
                {
                    this.aaThumContViewControl1.Item = this.SelectedLargeCharItem.LargeItem;
                }
            }
        }
        

        public void InitialLoad()
        {
            this.RegisterItems = new List<IBookCollection>();
            var w = LargeCharBox.DefaultItem();
            this.RegisterItems.Add(w);
            this.RegisterItems.Add(LargeCharBox.DefaultItem2());

            this.comboBox1.Items.Clear();
            this.comboBox1.DisplayMember = "Name";
            foreach (var v in this.RegisterItems)
            {
                this.comboBox1.Items.Add(v);
            }
            this.comboBox1.SelectedItem = w; 
        }
        public static IBookCollection DefaultItem()
        {
            var v = new IBookCollection();
            v.ItemPathList.Add("Data/LargeChar/50音カタカナ01.ast");
            v.ItemPathList.Add("Data/LargeChar/50音ひらがな01.ast");
            v.ItemPathList.Add("Data/LargeChar/アルファベット07.ast");
            v.Name = "ひらがな・カタカナ(太)";
            return v;
        }
        public static IBookCollection DefaultItem2()
        {
            var v = new IBookCollection();
            v.ItemPathList.Add("Data/LargeChar/50音カタカナ04.ast");
            v.ItemPathList.Add("Data/LargeChar/50音ひらがな02.ast");
            v.Name = "ひらがな・カタカナ(細)";
            return v;
        }
        public static Form Create(EditorInfo dllfunction)
        {
            var v = new LargeCharBox();
            v.DLLFunction = dllfunction;
            var f = new BSystem.Controls.SingleForm(v, "巨大文字作成ボックス") { Icon = SuouTextBox2.Properties.Resources.turu_perple};
            return f;
        }



        private void button3_Click(object sender, EventArgs e)
        {
            if (this.DLLFunction != null)
            {
                if (this.DLLFunction.TextBoxPlugin.Control.Visible)
                {
                    this.DLLFunction.TextBoxPlugin.AddLayerText(this.CreateText().Text);
                }
                else
                {
                    MessageBox.Show("レイヤーを貼り付けるページを開いてください");
                }
            }
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.CreateLargeCharItem();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {

        }
        private SuouBase.Text.SuouText CreateText()
        {
            var v = this.SelectedLargeCharItem;
            v.SetText(this.textBox1.Text);
            return v.Create();
        }
        private void TestCreate()
        {
            this.suouTextBox1.SetRawText(this.CreateText());
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.TestCreate();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var v = this.suouTextBox1.Text;
            Clipboard.SetText(v);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //this.openFileDialog1.InitialDirectory = System.IO.Directory.GetCurrentDirectory() + "/Data/LargeChar";
            this.openFileDialog1.RestoreDirectory = false;
            var result = this.openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {

                var s = this.openFileDialog1.FileNames;

                this.textBox2.Text = s.JoinString();
                var f = IBookHandler.Load(s.First());
                this.aaThumContViewControl1.Item = f;
            }
        }

        private void button2_Click_2(object sender, EventArgs e)
        {

            var f = (EditingFile)this.aaThumContViewControl1.Item;
            f.Save(f.FullFilePath, true, SuouEditorConst.AstExtension);
            MessageBox.Show("保存しました");
        }
    }
}
