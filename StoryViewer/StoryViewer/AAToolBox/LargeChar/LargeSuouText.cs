﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Text;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Book;
namespace StoryViewer
{

    public class LargeSuouText
    {
        private IBook _LargeItem = null;
        public IBook LargeItem
        {
            get { return this._LargeItem; }
            set
            {
                this._LargeItem = value;
            }
        }
        public void CreateDic()
        {
            this.LargeCharDic.Clear();
        }
        public Dictionary<string, SuouText> LargeCharDic { get; private set; }
        
        public LargeSuouText(IBook item)
        {
            this.LargeCharDic = new Dictionary<string, SuouText>();
            this.LargeItem = item;
            this.Text = new List<LargeSuouLine>();
        }
        public List<LargeSuouLine> Text { get; private set; }

        public void SetText(string text)
        {
            this.Text.Clear();
            text.LineSprit().ForEach((v) => this.Add(v));
        }
        public void Add(string line)
        {
            this.Text.Add(new LargeSuouLine(this.LargeItem, line));
        }
        public SuouText Create()
        {
            var r = this.Text.First().Create();
            foreach (var v in this.Text.Skip(1))
            {
                r %= v.Create();
            }
            return r;
        }
    }
    public class LargeSuouLine
    {
        public IBook LargeItem { get; set; }
        public List<SuouText> Item { get; set; }
        public AltText Spacer = AltText.Default();
        private string _SmallText = "";
        public string SmallText
        {
            get { return this._SmallText; }
            set
            {
                this._SmallText = value;
                this.TranslateLarge();
            }
        }
        public int BaseHeight { get; set; }
        private void TranslateLarge()
        {
            this.Item.Clear();
            foreach (var s in this.SmallText)
            {
                var v = this.LargeItem.FirstFind(s.ToString());
                if (v != null) this.Item.Add(v.SText.Clone());
            }
        }
        public LargeSuouLine(IBook item,string s)
        {
            this.Item = new List<SuouText>();
            this.LargeItem = item;
            this.SmallText = s;
        }
        public SuouText Create()
        {
            var r = SuouText.Empty(BaseHeight);
            this.Item.ForEach((v) => r = r.MergeRectangle(v,Spacer));
            return r;
        }
    }
}
