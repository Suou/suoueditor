﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using System.IO;
using System.Diagnostics;
using SuouBase.Interface;
namespace StoryViewer
{
    public class MojiToolPlugin : IComponentPlugin
    {
        MojiToolBox Ctrl;

        EditorInfo DLLFunction;
        public string Name
        {
            get
            {
                return "文字ツール";
            }
        }
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public MojiToolPlugin(EditorInfo DLLFunction)
        {
            this.Ctrl = new MojiToolBox();
            this.DLLFunction = DLLFunction;
            this.AsyncLoad();
            //Action t = AsyncLoad;
            //t.BeginInvoke(null, null);
            //this.Ctrl.BeginInvoke(t, null);
            //t();
            //t.EndInvoke(t.BeginInvoke(null,null));
        }
        public void AsyncLoad()
        {
            Ctrl.DLLFucntion = this.DLLFunction;
            //Ctrl.Load();
        }
        public bool DisposeCheck()
        {
            Ctrl.Save();
            return true;
        }
        public SettingPage[] SettingPage { get { return null; } }
        public ContainerControl Control
        {
            get { return this.Ctrl; }
        }
        public Form Form
        {
            get { return null; }
        }

    }
}
