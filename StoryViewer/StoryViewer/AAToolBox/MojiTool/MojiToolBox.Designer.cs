﻿namespace StoryViewer
{
    partial class MojiToolBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.ThumbControlContext = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.importToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thumContViewBox1 = new StoryViewer.ThumContViewBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.ThumbControlContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.listBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.thumContViewBox1);
            this.splitContainer1.Size = new System.Drawing.Size(280, 212);
            this.splitContainer1.SplitterDistance = 93;
            this.splitContainer1.TabIndex = 0;
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(0, 0);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(93, 212);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // ThumbControlContext
            // 
            this.ThumbControlContext.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.importToolStripMenuItem});
            this.ThumbControlContext.Name = "ThumbControlContext";
            this.ThumbControlContext.Size = new System.Drawing.Size(118, 26);
            // 
            // importToolStripMenuItem
            // 
            this.importToolStripMenuItem.Name = "importToolStripMenuItem";
            this.importToolStripMenuItem.Size = new System.Drawing.Size(117, 22);
            this.importToolStripMenuItem.Text = "Import";
            this.importToolStripMenuItem.Click += new System.EventHandler(this.importToolStripMenuItem_Click);
            // 
            // thumContViewBox1
            // 
            this.thumContViewBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.thumContViewBox1.BoxType = StoryViewer.ImageItemBoxType.Normal;
            this.thumContViewBox1.CanItemDrag = false;
            this.thumContViewBox1.CanItemMove = false;
            this.thumContViewBox1.ControlContext = this.ThumbControlContext;
            this.thumContViewBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumContViewBox1.EnableNameTextBox = StoryViewer.NameTextBoxType.None;
            this.thumContViewBox1.IsLoose = false;
            this.thumContViewBox1.IsMouseMoveFocus = false;
            this.thumContViewBox1.IsTransparentDrag = false;
            this.thumContViewBox1.ItemContext = null;
            this.thumContViewBox1.ItemInterval = 1;
            this.thumContViewBox1.ItemSize = new System.Drawing.Size(100, 100);
            this.thumContViewBox1.Location = new System.Drawing.Point(0, 0);
            this.thumContViewBox1.ManualDragDo = false;
            this.thumContViewBox1.MinItemSize = new System.Drawing.Size(16, 16);
            this.thumContViewBox1.MouseDownItem = null;
            this.thumContViewBox1.Name = "thumContViewBox1";
            this.thumContViewBox1.OrderMode = StoryViewer.ThumbnailOrder.SmallTile;
            this.thumContViewBox1.Percent = 0.5F;
            this.thumContViewBox1.PermitCategory = false;
            this.thumContViewBox1.ShiftItemContext = null;
            this.thumContViewBox1.Size = new System.Drawing.Size(183, 212);
            this.thumContViewBox1.TabIndex = 0;
            this.thumContViewBox1.ViewMode = SuouBase.Drawing.ThumbnailMode.Raw;
            this.thumContViewBox1.ItemMouseDown += new StoryViewer.ThumbnailViewEventHandler(this.thumContViewBox1_ItemMouseDown);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MojiToolBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitContainer1);
            this.Name = "MojiToolBox";
            this.Size = new System.Drawing.Size(280, 212);
            this.VisibleChanged += new System.EventHandler(this.MojiToolBox_VisibleChanged);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ThumbControlContext.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ContextMenuStrip ThumbControlContext;
        private System.Windows.Forms.ToolStripMenuItem importToolStripMenuItem;
        private ThumContViewBox thumContViewBox1;
        private System.Windows.Forms.Timer timer1;
    }
}
