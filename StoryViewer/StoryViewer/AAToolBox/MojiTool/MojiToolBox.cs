﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.IO;
using System.Diagnostics;
using SuouBase.Drawing;
using BSystem;
using SuouBase.Book;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class MojiToolBox : UserControl
    {
        public MojiToolBox()
        {
            lock (this.lockObject)
            {
                InitializeComponent();
            }
            //MakeTable();
        }
        public object lockObject = new object();
        public EditorInfo DLLFucntion { get; set; }

        ColumnHeader columnName = new ColumnHeader();
        public List<EditingFile> MojiTable = new List<EditingFile>();





        public void MakePage(EditingFile ef)
        {
            var f = TextThumbImage.CreateTextBitmaps(ef, SuouEditorConst.AAFontYInterval);
            this.thumContViewBox1.Operator.ImageItemSet(f);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var n = this.listBox1.SelectedIndex;
            var ef = this.MojiTable[n];
            MakePage(ef);
        }

        private void importToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Import();
        }



        private void thumbnailViewBox1_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            var p = (AstPage)e.Item.Page;
            if (DLLFucntion != null) DLLFucntion.TextBoxPlugin.InsertText(BSystem.Text.StringExtensions.ToParagraphJoin(p.ListText));
        }
        public string SaveDataName = SuouEditorConst.GetDataFile("Moji.dtn");
        public void Save()
        {
            if (ImportFileName == null)
            {
            }
            else
            {
                if (this.MojiTable != null) BSystem.XML.BinarySerialize.SaveToBinaryFile(ImportFileName, SaveDataName);
            }
        }
        public new void Load()
        {
            lock (this.lockObject)
            {
                var p = BSystem.XML.BinarySerialize.LoadFromBinaryFile<string>(SaveDataName);
                if (p != null)
                {
                    var fi = new FileInfo((string)p);
                    if (fi.Exists)
                    {
                        ImportFileName = fi.FullName;
                    }
                }
                else
                {
                    var fi = new FileInfo(SuouEditorConst.GetDataFile("aalist.txt"));
                    if (fi.Exists)
                    {
                        ImportFileName = fi.FullName;
                    }
                }
                MakeFromTable();
            }

        }
        public void MakeFromTable()
        {
            while (!this.IsHandleCreated)
            {
            }
            if (ImportFileName == null) return;
            StreamReader sr = new StreamReader(ImportFileName, Encoding.GetEncoding("Shift_JIS"));
            string s = sr.ReadToEnd();
            sr.Close();
            var p = MojiTool.Read(s);
            MojiTable = p.ToList();
            Action t = this.AsyncResgisterName;
            this.listBox1.BeginInvoke(t, null);

        }
        public void AsyncResgisterName()
        {
            foreach (EditingFile mp in MojiTable)
            {
                listBox1.Items.Add(mp.Name);
            }
        }

        public new void Load(string filename)
        {


            ImportFileName = filename;
            MakeFromTable();
        }

        public string ImportFileName;
        private static string OpenTitle = "インポートするAAファイルを選んでください";
        private static string OpenFilter = "ファイル(*.txt)|*.txt|すべてのファイル(*.*)|*.*";
        private OpenFileDialog OpenDia = SuouBase.GeneralMethod.CreateOpenDialogBox(OpenTitle, OpenFilter);

        public void Import()
        {
            if (OpenDia.ShowDialog() == DialogResult.OK)
            {
                string s = OpenDia.FileName;
                this.Load(s);
            }
        }

        private void thumContViewBox1_ItemMouseDown(object sender, ThumbnailViewEventArgs e)
        {
            if (Control.MouseButtons == System.Windows.Forms.MouseButtons.Left)
            {
                var p = (AstPage)e.Item.Page;
                if (DLLFucntion != null) DLLFucntion.TextBoxPlugin.InsertText(BSystem.Text.StringExtensions.ToParagraphJoin(p.ListText));
            }
        }
        private bool LoadFlag = false;
        private void MojiToolBox_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible && !this.LoadFlag && this.IsHandleCreated)
            {
                this.Load();
                this.LoadFlag = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.MojiToolBox_VisibleChanged(sender, e);
        }

    }
}
