﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using System.IO;
using SuouBase.Book;
namespace StoryViewer
{
    class MojiTool
    {
        
        public static EditingFile[] Read(string text)
        {
            List<EditingFile> mplst = new List<EditingFile>();
            string[] s = text.Split(new string[] { "[end]" }, StringSplitOptions.None);
            foreach (string str in s)
            {
                var p = str.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);

                System.Text.RegularExpressions.MatchCollection mc = System.Text.RegularExpressions.Regex.Matches(
        str, @"(?<=\[ListName=).*(?=\])");
                if (mc.Count > 0)
                {
                    var a = mc[0].Value;
                    
                    var b = (string[])(p.Skip(1)).ToArray();

                    var f = new EditingFile();
                    f.Name = a;
                    foreach (string c in b)
                    {
                        if (c.Length > 9 && c.Substring(0, 9) == "[ListName") continue;
                        f.Pages.Add(new AstPage("", c));
                    }
                    mplst.Add(f);
                }

            }
            return mplst.ToArray();
        }
    }
}
