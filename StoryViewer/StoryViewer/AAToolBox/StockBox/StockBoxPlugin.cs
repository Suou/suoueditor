﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using SuouBase.Plugin;
using SuouBase;
using System.Windows.Forms;
using SuouBase.Text;
using System.IO;
using System.Drawing;
using SuouBase.Book;
namespace StoryViewer.AAToolBox
{
    [IComponentPlugin("StockBoxPlugin", PluginType.Control)]
    [Serializable]
    public class StockBoxPlugin : IComponentPlugin
    {
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public string Name
        {
            get
            {
                return "AA選択";
            }
        }
        public StockBoxPlugin()
        {
            _Control = new StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay() { EditLock = false };
            this.SaveTimer.Tick += this.Timer_Tick;
        }
        public Timer SaveTimer = new Timer() { Interval = 60 * 1000 * 10, Enabled = true };
        public IBook Book = new ObsoleteSectionBook();
        public SettingPage[] SettingPage { get { return null; } }
        public StockBoxPlugin(EditorInfo pd):this()
        {
            this._Control.EditorSmallInfo = pd.Info;
            DLLFunctinon = pd;
            this.LoadBook();
        }
        public void Timer_Tick(object sender, EventArgs e)
        {
            this.Book.Save(StockBookFilePath, true);
        }
        public void LoadBook()
        {
            var fileio = new FileInfo(StockBoxPlugin.StockBookFilePath);
            if (fileio.Exists)
            {
                this.Book = IBookHandler.Load(StockBoxPlugin.StockBookFilePath);
            }
            else
            {
                
            }
            this._Control.Item = this.Book;
            this._Control.EditLock = false;

        }
        [NonSerialized]
        private EditorInfo DLLFunctinon = null;

        [NonSerialized]
        public StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay _Control = null;
        [NonSerialized]

        public static string StockBookFilePath = SuouEditorConst.UserDirectory + "/" + "StockBook"+SuouBase.SuouEditorConst.SectionBookExtension;
        private string SaveFile = SuouEditorConst.SettingDirectory + "/" + "StockBoxPlugin.stn";
        public ContainerControl Control
        {
            get { return this._Control; }
        }
        public Form Form { get { return null; } }

        public bool DisposeCheck()
        {
            this.Book.Save(StockBookFilePath, true);
            //BinarySerialize.SaveToBinaryFile(this,SaveFile);
            return true;
        }
    }
}
