﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Book;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class DetailMontage : UserControl
    {
        public DetailMontage()
        {
            InitializeComponent();
            var v = this.aaThumContViewControl1.GetSaveData();
            v.OrderMode = ThumbnailOrder.Thread;
            v.ViewMode = SuouBase.Drawing.ThumbnailMode.Raw;
        }

        private EditorInfo _DLLFunction = null;
        public EditorInfo DLLFunction
        {
            get { return this._DLLFunction; }
            set
            {
                this._DLLFunction = value;
                this.aaThumContViewControl1.EditorSmallInfo = value.Info;
            }
        }

        public IBookCollection Item
        {
            get { return this.iBooksViewer1.Item; }
            set { this.iBooksViewer1.Item = value; }
        }
        private void iBooksViewer1_SelectedItemChanged(object sender, EventArgs e)
        {
            if (DLLFunction != null)
            {
                var s = this.iBooksViewer1.SelectedItemPath;
                if (s == null) return;

                var f = IBookHandler.Load(s);
                this.aaThumContViewControl1.Item = f;
            }
        }
        public static Form Create(EditorInfo dllfunction,MontageBox mb)
        {
            var v = new DetailMontage();
            v.DLLFunction = dllfunction;
            v.Item = mb.Books;
            var f = new BSystem.Controls.SingleForm(v,"モンタージュ詳細設定");
            return f;
        }
    }
}
