﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Text.Search;
namespace StoryViewer
{
    class MontagePart
    {
        public AATextComboBox Control = new AATextComboBox();
        public MatchedReplaceItem MatchItem { get; set; }
        public event EventHandler TextChanged;
        public MontagePart(MatchedReplaceItem item)
        {
            this.MatchItem = item;
            this.Control.SetPicture(this.MatchItem.MergeItems.ParentItem[this.MatchItem.SelectedIndex].SText);
            this.Control.BookItem = this.MatchItem.MergeItems.ParentItem;
            this.Control.SelectedChanged += this.ControlItem_SelectedChanged;
        }
        private void ControlItem_SelectedChanged(object sender,EventArgs e)
        {

            this.MatchItem.Replace(this.Control.SelectedIndex);
            if (this.TextChanged != null) TextChanged(sender, e);
        }
        public static IList<MontagePart> CreateBoxes(MatchedReplaceItems items)
        {
            var p = items.Select((v) => new MontagePart(v));
            return p.ToList();

        }
    }
}
