﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Plugin;
using SuouBase;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using WebExtension;
using SuouBase.Text.Search;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class MontageBox : UserControl
    {
        public MontageBox()
        {
            InitializeComponent();
            this.toolStripMenuItem1.Click += this.Montage_Click;
            this.bookPagePicture1.ContextMenuStrip.Opening += this.contextMenuStrip1_Opening;
            ToolStripManager.Merge(this.contextMenuStrip1, bookPagePicture1.ContextMenuStrip);
        }
        private EditorInfo _DLLFunction = null;
        public EditorInfo DLLFunction
        {
            get { return this._DLLFunction; }
            set
            {
                if (value != null)
                {
                    this._DLLFunction = value;
                    this._DLLFunction.Common.Montage = this.GoToMontage;
                    this._DLLFunction.Common.ShowMontageDialog = this.GoToMontageDialog;
                }
            }
        }
        public void GoToMontage(SuouText text)
        {
            this.MontageText = text;
            var v = this.DLLFunction.PluginList.FirstOrDefault((w)=>w.PluginName =="箱");
            this.DLLFunction.Framework.Focus(v.Plugin);
        }
        public SuouText GoToMontageDialog(SuouText text)
        {
            var v = new MontageDialog();
            v.montageBox1.Books = this.Books;
            v.montageBox1.MontageText = text;
            var res = v.ShowDialog();
            if (res == DialogResult.OK)
            {
                var w = v.montageBox1.MontageText.Clone();
                w.NoTagize();
                return w;
            }
            else
            {
                return null;
            }
        }

        private MatchedReplaceItems _MatchedItems = null;
        public MatchedReplaceItems MatchedItems
        {
            get { return this._MatchedItems; }
            set
            {
                this._MatchedItems = value;
                if (this._MatchedItems != null)
                {
                    this.CreateBoxes();
                }
            }
        }
        private SuouText _MontageText = null;
        public SuouText MontageText
        {
            get { return this._MontageText; }
            set
            {
                this._MontageText = value;
                if (value != null)
                {
                    this.MontageSearch();
                }
            }
        }
        public ISpaceMaker SpaceCreater = SpaceMaker.Default();
        private void MontageSearch()
        {
            this.MatchedItems = MatchedReplaceItems.Create(this.MontageText, this.Books, this.SpaceCreater);
        }
        IList<MontagePart> PartItems { get; set; }
        private void CreateBoxes()
        {
            this.groupBox1.Controls.Clear();
            this.PartItems = MontagePart.CreateBoxes(this.MatchedItems);
            SuouBase.Api.WINAPI.StopControlDraw(this.groupBox1.Handle);
            this.groupBox1.Controls.Add(this.label1);
            this.label1.Visible = this.PartItems.Count == 0;
            foreach (var v in this.PartItems)
            {
                v.TextChanged += this.MatchedTextChanged;
                this.groupBox1.Controls.Add(v.Control);
                v.Control.Dock = DockStyle.Top;
                v.Control.AutoSize = true;
            }
            this.GroupBox_Resize();
            SuouBase.Api.WINAPI.StartControlDraw(this.groupBox1.Handle);
            this.groupBox1.Refresh();

            this.bookPagePicture1.SText = this.MatchedItems.Text.Clone();
        }
        private void MatchedTextChanged(object sender, EventArgs e)
        {
            this.bookPagePicture1.SText = this.MatchedItems.Text.Clone();
        }
        public IBookCollection Books { get; set; }

        private void MontageBox_Load(object sender, EventArgs e)
        {
            this.Books = IBookCollection.Load("Data/Montage");
        }
        private void GroupBox_Resize()
        {
            /*
            this.groupBox1.MinimumSize = new Size(100, 100);
            var w = this.PartItems.Select((v) => v.Control.Width + 20).Max();
            this.groupBox1.MinimumSize = new Size(w, this.groupBox1.MinimumSize.Height);
            */
        }
        private void groupBox1_ControlAdded(object sender, ControlEventArgs e)
        {
        }

        private void MontageBox_Resize(object sender, EventArgs e)
        {
            this.bookPagePicture1.Loading();
        }

        private void Montage_Click(object sender, EventArgs e)
        {

            var v = Clipboard.GetText().CheckNewLine().DecodeToSjis();
            this.MontageText = new SuouText(v);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            this.toolStripMenuItem1.Enabled = SuouBase.ClipBoardExtensions.CanPaste;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.bookPagePicture1.Item != null)
            {
                var v = this.bookPagePicture1.SText.Text;
                Clipboard.SetText(v);
            }
            else
            {
                MessageBox.Show("データがありません");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var f = DetailMontage.Create(this.DLLFunction,this);
            f.Show();
        }

    }
}
