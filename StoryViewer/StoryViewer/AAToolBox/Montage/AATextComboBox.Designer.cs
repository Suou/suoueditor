﻿namespace StoryViewer
{
    partial class AATextComboBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.bookPagePicture1 = new SuouBase.Controls.BookPagePicture();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Left;
            this.button1.Location = new System.Drawing.Point(210, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(28, 181);
            this.button1.TabIndex = 1;
            this.button1.Text = "▼";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(153, 26);
            // 
            // bookPagePicture1
            // 
            this.bookPagePicture1.AutoSizeType = SuouBase.Controls.ShrinkType.Tate;
            this.bookPagePicture1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.bookPagePicture1.Dock = System.Windows.Forms.DockStyle.Left;
            this.bookPagePicture1.EnableCopy = false;
            this.bookPagePicture1.EnablePaste = false;
            this.bookPagePicture1.Item = null;
            this.bookPagePicture1.Location = new System.Drawing.Point(0, 0);
            this.bookPagePicture1.Name = "bookPagePicture1";
            this.bookPagePicture1.Page = null;
            this.bookPagePicture1.Size = new System.Drawing.Size(210, 181);
            this.bookPagePicture1.TabIndex = 0;
            this.bookPagePicture1.SizeChanged += new System.EventHandler(this.bookPagePicture1_SizeChanged);
            // 
            // AATextComboBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.button1);
            this.Controls.Add(this.bookPagePicture1);
            this.Name = "AATextComboBox";
            this.Size = new System.Drawing.Size(238, 181);
            this.ResumeLayout(false);

        }

        #endregion

        private SuouBase.Controls.BookPagePicture bookPagePicture1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
    }
}
