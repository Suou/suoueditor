﻿namespace StoryViewer
{
    partial class DetailMontage
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.iBooksViewer1 = new SuouBase.Controls.IBooksViewer();
            this.aaThumContViewControl1 = new StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay();
            this.SuspendLayout();
            // 
            // iBooksViewer1
            // 
            this.iBooksViewer1.Dock = System.Windows.Forms.DockStyle.Left;
            this.iBooksViewer1.Item = null;
            this.iBooksViewer1.Location = new System.Drawing.Point(0, 0);
            this.iBooksViewer1.Name = "iBooksViewer1";
            this.iBooksViewer1.Size = new System.Drawing.Size(207, 382);
            this.iBooksViewer1.TabIndex = 0;
            this.iBooksViewer1.SelectedItemChanged += new System.EventHandler(this.iBooksViewer1_SelectedItemChanged);
            // 
            // aaThumContViewControl1
            // 
            this.aaThumContViewControl1.DataBook = null;
            //this.aaThumContViewControl1.DLLFunction = null;
            this.aaThumContViewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aaThumContViewControl1.EditLock = false;
            this.aaThumContViewControl1.Item = null;
            this.aaThumContViewControl1.Location = new System.Drawing.Point(207, 0);
            this.aaThumContViewControl1.Name = "aaThumContViewControl1";
            this.aaThumContViewControl1.Size = new System.Drawing.Size(316, 382);
            this.aaThumContViewControl1.TabIndex = 1;
            this.aaThumContViewControl1.UseContext = true;
            // 
            // DetailMontage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.aaThumContViewControl1);
            this.Controls.Add(this.iBooksViewer1);
            this.Name = "DetailMontage";
            this.Size = new System.Drawing.Size(523, 382);
            this.ResumeLayout(false);

        }

        #endregion

        private SuouBase.Controls.IBooksViewer iBooksViewer1;
        private StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay aaThumContViewControl1;
    }
}
