﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using StoryViewer;
using SuouBase.Debug;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Book;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class AATextComboBox : UserControl
    {
        public AATextComboBox()
        {
            InitializeComponent();
            
            this.HostPanel = new ToolStripControlHost(this.PopupBox);
            this.contextMenuStrip1.Items.Add(this.HostPanel);
            //this.PopupBox.Item = DebugClass.SampleIBook();
            this.PopupBox.ItemClicked += this.AAViewer_ItemClicked;

            var savedata = new ThumbnailView_SaveData();
            savedata.OrderMode = ThumbnailOrder.Thread;
            savedata.ViewMode = ThumbnailMode.Raw;
            this.PopupBox.SetSaveData(savedata);
            this.bookPagePicture1.PictureSeted += this.Picture_Changed;
        }
        //public bool IsAutoSize { get; set; }
        private void Picture_Changed(object sender, EventArgs e)
        {
            var height = this.bookPagePicture1.PictureSize.Height;
            var width = this.bookPagePicture1.PictureSize.Width + 50;
            var size = new Size(width, height);
            this.MinimumSize = size;
            this.Size = size;
        }
        public event EventHandler SelectedChanged;
        public SuouText SelectedText
        {
            get
            {
                return this.bookPagePicture1.Item.Page.SText.Clone();
            }
        }
        public void SetPicture(SuouText text)
        {
            this.bookPagePicture1.SText = text;
        }
        public IBook BookItem
        {
            get { return this.PopupBox.Item; }
            set { this.PopupBox.Item = value; }
        }
        public int SelectedIndex
        {
            get { return this.PopupBox.SelectedIndex; }
        }
        public ToolStripControlHost HostPanel { get; set; }
        public StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay PopupBox = new StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay() { Width = 100, Height = 100, MinimumSize = new Size(300, 400) };
        public Point ContextPopLocation
        {
            get
            {
                var v = new Point(this.Location.X, this.Location.Y+this.Size.Height);
                return v;
            }
        }
        /*
        public Size DefaultSize
        {
            get
            {
                return new Size(300, 400);
            }
        }
        */
        public int PossibleHeight
        {
            get
            {
                var h = System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height-100;
                var h2 = this.PointToScreen(this.button1.Location).Y + this.button1.Height;
                return Math.Abs(h - h2);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.PopupBox.MinimumSize = this.DefaultSize;
            if (this.PopupBox.MinimumSize.Height > this.PossibleHeight)
            {
                this.PopupBox.MinimumSize = new Size(this.PopupBox.MinimumSize.Width, this.PossibleHeight);
            }
            //this.contextMenuStrip1.MaximumSize = new Size(1000, 400);
            this.contextMenuStrip1.Show(this.Parent,this.ContextPopLocation);
        }
        private void AAViewer_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            var v = new TextThumbImage(e.SelectedPage,0);
            v.PictureSize = this.bookPagePicture1.Size;
            this.bookPagePicture1.Item = v;
            this.contextMenuStrip1.Close();
            if (this.SelectedChanged != null) this.SelectedChanged(sender, e);
        }

        private void bookPagePicture1_SizeChanged(object sender, EventArgs e)
        {

        }
    }
}
