﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
namespace StoryViewer.AAToolBox.Hukidasi
{
    [ToolboxItem(false)]
    public partial class WakuViewer : UserControl
    {
        public WakuViewer()
        {
            InitializeComponent();

            this.TextMaker = AltText.Default();
            this._WakuItems = new ObservableCollection<HukidasiSaveData>();
        }
        public event EventHandler ItemSelected;
        private Padding PicturePadding = new Padding(3, 5, 3, 5);
        private ObservableCollection<HukidasiSaveData> _WakuItems { get; set; }
        private AltText TextMaker { get; set; }
        [ReadOnly(true)]
        public ObservableCollection<HukidasiSaveData> Items
        {
            get
            {
                return this._WakuItems;
            }
            set
            {
                if (value != null)
                {
                    this._WakuItems = value;
                    this._WakuItems.CollectionChanged += this.Item_Changed;
                    this.Set();
                }
            }
        }
        public SuouText SampleText { get; set; }
        private void Set()
        {
            this.Items.Select((v) => v.ToTextThumbImage(this.TextMaker,this.SampleText))
                .ForEach2((v)=>v.Padding = this.PicturePadding).VoidPipe((v) => this.thumContViewBox1.Operator.ImageItemSet(v));
            //this.thumContViewBox1.Operator.ImageItemSet(this._WakuItems.Create());
            //this.thumContViewBox1.Operator.AddRange(this.WakuItems.Create());
            //this.WakuItems.Create().ForEach((v) => this.thumContViewBox1.Operator.Add(v));
        }

        public HukidasiSaveData SelectedItem
        {
            get {
                var n = this.thumContViewBox1.Operator.MapOperator.LastSelectedIndex;
                if (n == -1) return null;
                return this.Items[n]; }
        }
        private void thumContViewBox1_ItemSelected(object sender, ThumbnailViewEventArgs e)
        {
            
        }

        private void thumContViewBox1_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            if (e.SelectedIndex != -1)
            {
                if (this.ItemSelected != null) ItemSelected(this, new EventArgs());
            }
        }
        public void Item_Changed(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    var v = e.NewItems.Cast<HukidasiSaveData>().ToList();
                    var p = v.Select((w) => w.ToTextThumbImage(this.TextMaker,this.SampleText));
                    this.thumContViewBox1.Operator.InsertRange(e.NewStartingIndex, p);
                    break;
                case NotifyCollectionChangedAction.Move: 
                    if (e.OldStartingIndex < e.NewStartingIndex)
                    {
                        this.thumContViewBox1.Operator.MoveDown(e.OldStartingIndex);
                    }
                    else
                    {
                        this.thumContViewBox1.Operator.MoveUp(e.OldStartingIndex);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    var n = e.OldStartingIndex;
                    this.thumContViewBox1.Operator.Delete(n);
                    break;
                case NotifyCollectionChangedAction.Replace: break;
                case NotifyCollectionChangedAction.Reset: break;
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.thumContViewBox1.Operator.MapOperator.LastSelectedIndex;
            if (n != -1)
            {
                this.Items.RemoveAt(n);
                //this.thumContViewBox1.Operator.Delete(n);
            }
        }

        private void moveUpToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.thumContViewBox1.Operator.MapOperator.LastSelectedIndex;
            if (n > 0)
            {
                this.Items.Move(n, n-1);
            }
        }

        private void moveDownToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.thumContViewBox1.Operator.MapOperator.LastSelectedIndex;
            if (n >= 0 && n < this.Items.Count-1)
            {
                this.Items.Move(n, n + 1);
            }

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            var n = this.thumContViewBox1.Operator.MapOperator.LastSelectedIndex;
            this.moveUpToolStripMenuItem.Enabled = n > 0;
            this.moveDownToolStripMenuItem.Enabled = n >= 0 && n < this.Items.Count - 1;
        }

    }
    /*
    public class HukidasiEventArgs:EventArgs{
        public HukidasiSaveData SelectedItem{get;private set;}
        public HukidasiEventArgs(HukidasiSaveData item):base()
        {
            this.SelectedItem = item;
        }
    }
    */
}
