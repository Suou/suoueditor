﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using SuouBase.Interface;
using SuouTextBox2;
namespace StoryViewer
{
    [IComponentPlugin("HukidasiTool", PluginType.Control)]
    [Serializable]
    public class HukidasiToolPlugin:IHukidasiClass
    {
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public string Name
        {
            get
            {
                return "噴出し";
            }
        }
        public string SettingPath = SuouEditorConst.GetSettingFile("HukidasiPlugin.stn");
        public HukidasiToolPlugin()
        {
        }
        public HukidasiToolPlugin(EditorInfo pd)
        {
            DLLFunction = pd;
            _Control = new HukidasiToolBox(pd);
            Action t = this.AsyncLoad;
            t();
            var setting = BSystem.XML.NonBinarySerializer.Load<HukidasiSelecterFormSetting>(this.SettingPath);
            if (setting != null) this.SelecterForm.Setting = setting;
            //t.BeginInvoke(null, null);
        }
        public void AsyncLoad()
        {
            //CreateForm();

            //var hsf = new HukidasiSelecterForm(this.DLLFunction);
            this.SelecterForm = new HukidasiSelecterForm(this.DLLFunction) { OrderMode = ThumbnailOrder.SmallTile };
            this.DLLFunction.HukidasiSelecterForm = () => this.SelecterForm;
         
        }
        public HukidasiSelecterForm SelecterForm { get; set; }
        private void CreateForm()
        {
            _Form = new Form();
            var formControl = new HukidasiToolBox(DLLFunction) { Dock = DockStyle.Fill };
            _Form.Controls.Add(formControl);
            formControl.HukidasiSelected += this.Form_HukidasiSelected;
        }
        EditorInfo DLLFunction { get; set; }

        public SettingPage[] SettingPage { get { return null; } }
        public Form Form
        {
            get { CreateForm(); return _Form; }
        }

        public event HukidasiEventHandler HukidasiSelected;
        private void Form_HukidasiSelected(object sender, HukidasiEventArgs e)
        {
            if (HukidasiSelected != null) HukidasiSelected(this, e);
        }
        public Form _Form;
        public HukidasiToolBox _Control;
        public ContainerControl Control
        {
            get {
                return null;
                //return _Control; 
            }
        }

        public bool DisposeCheck()
        {
            BSystem.XML.NonBinarySerializer.Save(this.SettingPath, this.SelecterForm.Setting);
            return true;
        }
    }
}
