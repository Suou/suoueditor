﻿namespace StoryViewer.AAToolBox.Hukidasi
{
    partial class WakuSerihuCreater
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.sentenceLocationBox1 = new SuouBase.Controls.SentenceLocationBox();
            this.spacerComboBox1 = new SuouBase.Controls.SpacerComboBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lockSimoziBox1 = new SuouTextBox2.Controls.TextLayer.LockSimoziBox();
            this.wakuViewer1 = new StoryViewer.AAToolBox.Hukidasi.WakuViewer();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 15);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(580, 94);
            this.textBox1.TabIndex = 4;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.splitter1.Location = new System.Drawing.Point(303, 277);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(586, 3);
            this.splitter1.TabIndex = 5;
            this.splitter1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(303, 280);
            this.panel1.MinimumSize = new System.Drawing.Size(0, 100);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(586, 177);
            this.panel1.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 65);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(586, 112);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "枠内テキスト";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.sentenceLocationBox1);
            this.groupBox2.Controls.Add(this.spacerComboBox1);
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(586, 65);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "操作";
            // 
            // sentenceLocationBox1
            // 
            this.sentenceLocationBox1.Location = new System.Drawing.Point(323, 15);
            this.sentenceLocationBox1.Name = "sentenceLocationBox1";
            this.sentenceLocationBox1.Size = new System.Drawing.Size(199, 25);
            this.sentenceLocationBox1.TabIndex = 9;
            this.sentenceLocationBox1.SelectedChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // spacerComboBox1
            // 
            this.spacerComboBox1.Location = new System.Drawing.Point(100, 14);
            this.spacerComboBox1.Name = "spacerComboBox1";
            this.spacerComboBox1.Size = new System.Drawing.Size(217, 26);
            this.spacerComboBox1.TabIndex = 8;
            this.spacerComboBox1.SelectedChanged += new System.EventHandler(this.spacerComboBox1_SelectedChanged);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(6, 18);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(88, 16);
            this.checkBox1.TabIndex = 7;
            this.checkBox1.Text = "行間を空ける";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(6, 40);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 5;
            this.button1.Text = "コピー";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(87, 40);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 6;
            this.button2.Text = "クリア";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // splitter2
            // 
            this.splitter2.Location = new System.Drawing.Point(300, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 457);
            this.splitter2.TabIndex = 7;
            this.splitter2.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lockSimoziBox1
            // 
            this.lockSimoziBox1.BookPage = null;
            this.lockSimoziBox1.CanMerge = true;
            this.lockSimoziBox1.DLLFunction = null;
            this.lockSimoziBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            //this.lockSimoziBox1.EndLocation = new System.Drawing.Point(0, 0);
            //this.lockSimoziBox1.FirstLocation = new System.Drawing.Point(0, 0);
            this.lockSimoziBox1.IsDragEndMerge = true;
            this.lockSimoziBox1.IsSharrowHide = false;
            this.lockSimoziBox1.IsMultiMode = true;
            this.lockSimoziBox1.Location = new System.Drawing.Point(303, 0);
            this.lockSimoziBox1.Margin = new System.Windows.Forms.Padding(4);
            this.lockSimoziBox1.MergePase = 300;
            this.lockSimoziBox1.Name = "lockSimoziBox1";
            this.lockSimoziBox1.NeedMerge = false;
            this.lockSimoziBox1.PenItem = null;
            //this.lockSimoziBox1.ShowNumber = 0;
            this.lockSimoziBox1.Size = new System.Drawing.Size(586, 277);
            this.lockSimoziBox1.TabIndex = 3;
            this.lockSimoziBox1.UseDebug = false;
            this.lockSimoziBox1.UseTemporaryContext = false;
            this.lockSimoziBox1.Load += new System.EventHandler(this.lockSimoziBox1_Load);
            // 
            // wakuViewer1
            // 
            this.wakuViewer1.Dock = System.Windows.Forms.DockStyle.Left;
            this.wakuViewer1.Location = new System.Drawing.Point(0, 0);
            this.wakuViewer1.MinimumSize = new System.Drawing.Size(300, 0);
            this.wakuViewer1.Name = "wakuViewer1";
            this.wakuViewer1.Size = new System.Drawing.Size(300, 457);
            this.wakuViewer1.TabIndex = 0;
            this.wakuViewer1.ItemSelected += new System.EventHandler(this.wakuViewer1_ItemSelected);
            // 
            // WakuSerihuCreater
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.lockSimoziBox1);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.splitter2);
            this.Controls.Add(this.wakuViewer1);
            this.Name = "WakuSerihuCreater";
            this.Size = new System.Drawing.Size(889, 457);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private WakuViewer wakuViewer1;
        private SuouTextBox2.Controls.TextLayer.LockSimoziBox lockSimoziBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Timer timer1;
        private SuouBase.Controls.SpacerComboBox spacerComboBox1;
        private SuouBase.Controls.SentenceLocationBox sentenceLocationBox1;
    }
}
