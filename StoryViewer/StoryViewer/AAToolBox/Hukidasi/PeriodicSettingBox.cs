﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text.Hukidasi.WakuLogic;
using SuouBase.Text;
namespace StoryViewer.AAToolBox.Hukidasi
{
    [ToolboxItem(false)]
    public partial class PeriodicSettingBox : UserControl
    {
        public PeriodicSettingBox()
        {
            InitializeComponent();
        }

        private PeriodicRepresent UpPeriodic
        {
            get
            {
                var v = new PeriodicRepresent(this.textBox1.Text,this.textBox5.Text);
                return v;
            }
            set
            {

                this.textBox1.Text = value.SerializeString;
                this.textBox5.Text = value.SerializeFixString;
            }
        }
        private PeriodicRepresent LeftPeriodic
        {
            get
            {
                var v = new PeriodicRepresent(this.textBox2.Text,this.textBox6.Text);
                return v;
            }
            set
            {

                this.textBox2.Text = value.SerializeString;
                this.textBox6.Text = value.SerializeFixString;
            }
        }
        private PeriodicRepresent RightPeriodic
        {
            get
            {
                var v = new PeriodicRepresent(this.textBox3.Text,this.textBox7.Text);
                return v;
            }
            set
            {

                this.textBox3.Text = value.SerializeString;
                this.textBox7.Text = value.SerializeFixString;
            }
        }
        private PeriodicRepresent BottomPeriodic
        {
            get
            {
                var v = new PeriodicRepresent(this.textBox4.Text,this.textBox8.Text);
                return v;
            }
            set
            {
                this.textBox4.Text = value.SerializeString;
                this.textBox8.Text = value.SerializeFixString;
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public WakuArrangement Periodics
        {
            get
            {
                return new WakuArrangement()
                {
                    _UpPathPeriodic = this.UpPeriodic,
                    _LeftPathPeriodic = this.LeftPeriodic,
                    _RightPathPeriodic = this.RightPeriodic,
                    _DownPathPeriodic = this.BottomPeriodic
                };
            }
            set
            {
                if (value != null)
                {
                    this.UpPeriodic = value._UpPathPeriodic;
                    this.LeftPeriodic = value._LeftPathPeriodic;
                    this.RightPeriodic = value._RightPathPeriodic;
                    this.BottomPeriodic = value._DownPathPeriodic;
                }
            }
        }
        public TextBox[] PeriodicBoxes
        {
            get
            {
                return new TextBox[] { this.textBox1, this.textBox2, this.textBox3, this.textBox4,this.textBox5,this.textBox6,this.textBox7,this.textBox8 };
            }
        }
        public CheckBox[] CheckBoxes
        {
            get
            {
                return new CheckBox[] {};
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            int[][] s = new int[][]{PeriodicRepresent.Parse(this.textBox1.Text),PeriodicRepresent.Parse(this.textBox2.Text),PeriodicRepresent.Parse(this.textBox3.Text),
            PeriodicRepresent.Parse(this.textBox4.Text)};
            this.textBox1.BackColor = s[0] == null ? Color.Red : Color.White;
            this.textBox2.BackColor = s[1] == null ? Color.Red : Color.White;
            this.textBox3.BackColor = s[2] == null ? Color.Red : Color.White;
            this.textBox4.BackColor = s[3] == null ? Color.Red : Color.White;
        }
    }
}
