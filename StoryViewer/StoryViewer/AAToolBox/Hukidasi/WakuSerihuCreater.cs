﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;
using System.Collections.ObjectModel;
namespace StoryViewer.AAToolBox.Hukidasi
{
    [ToolboxItem(false)]
    public partial class WakuSerihuCreater : UserControl
    {
        public WakuSerihuCreater()
        {
            InitializeComponent();

            this.ItemSet(HukidasiSaveData.FiveDefault());
            this.textBox1.Text = serifu;
        }
        [ReadOnly(true)]
        public EditorInfo DLLFunction { get; set; }
        [ReadOnly(true)]
        public ObservableCollection<HukidasiSaveData> Items
        {
            get { return this.wakuViewer1.Items; }
            set { this.wakuViewer1.Items = value; }
        }
        public void Set()
        {
            if(this.DLLFunction != null)this.wakuViewer1.Items = this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList;
        }
        string serifu = @"ほんとはＶＩＰでやりたいんだお… 
でもＶＩＰＰＥＲはクオリティ高いスレしか相手してくれないお… 
だからニュー速でやるお！";
        private void button1_Click(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            this.IsTextChanged = true;
        }

        private void wakuViewer1_ItemSelected(object sender, EventArgs e)
        {
            this.StockItem = (HukidasiSaveData)this.wakuViewer1.SelectedItem.Clone();
            ItemSet();
            //this.lockSimoziBox1.SetWaku(this.wakuViewer1.SelectedItem);
        }
        public HukidasiSaveData StockItem { get; set; }
        private void ItemSet(HukidasiSaveData item)
        {
            this.StockItem = (HukidasiSaveData)item.Clone();
            this.ItemSet();
        }
        private void ItemSet()
        {
            if (this.StockItem != null)
            {
                var v = (HukidasiBaseSaveData)this.StockItem;
                v.Options.MergeAlgorithm = this.spacerComboBox1.SelectedSpaceSet;
                v.Arrangement.Align = this.sentenceLocationBox1.Align;
                this.lockSimoziBox1.SetWaku((HukidasiSaveData)v);
            }
        }
        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBox1.Checked)
            {
                var v = this.lockSimoziBox1.Waku;
                v.Padding.CenterSpace = 1;
                this.lockSimoziBox1.SetWaku(v);
            }
            else
            {
                var v = this.lockSimoziBox1.Waku;
                v.Padding.CenterSpace = 0;
                this.lockSimoziBox1.SetWaku(v);
            }
        }

        private void lockSimoziBox1_Load(object sender, EventArgs e)
        {
        }

        private void textBox1_Leave(object sender, EventArgs e)
        {

        }
        private bool IsTextChanged { get; set; }
        private void CheckTextChange()
        {
            if (this.IsTextChanged)
            {

                this.lockSimoziBox1.LayerText = this.textBox1.Text;
                this.lockSimoziBox1.ResizeLayer();
                this.IsTextChanged = false;
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.CheckTextChange();
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.lockSimoziBox1.Copy();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = "";
            this.lockSimoziBox1.Clear();
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public WakuSerihuCreaterDisplaySetting DisplaySetting
        {
            get
            {
                return new WakuSerihuCreaterDisplaySetting() { WakuViewerSize = this.wakuViewer1.Size, InputSize = this.panel1.Size };
            }
            set
            {
                if (value != null)
                {
                    this.wakuViewer1.Size = value.WakuViewerSize;
                    this.panel1.Size = value.InputSize;
                }
            }
        }

        private void spacerComboBox1_SelectedChanged(object sender, EventArgs e)
        {
            ItemSet();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ItemSet();
        }
        

        private void button3_Click(object sender, EventArgs e)
        {

        }
    }
    [Serializable]
    public class WakuSerihuCreaterDisplaySetting
    {
        public Size WakuViewerSize { get; set; }
        public Size InputSize { get; set; }
    }
}
