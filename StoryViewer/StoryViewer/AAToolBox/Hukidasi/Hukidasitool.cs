﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using SuouTextBox2.Base;
using SuouTextBox2;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Book;
namespace StoryViewer
{
    [Serializable]
    public class HukidasiCollection
    {
        public List<IHukidasiLogic> Items = new List<IHukidasiLogic>();
        public HukidasiCollection()
        {
            //this.Items.Add();
        }
        public HukidasiCollection(string fileName):this()
        {
        }

        public IEnumerator<IHukidasiLogic> GetEnumerator()
        {
            for (int i = 0; i < this.Items.Count; i++)
                yield return Items[i];
        }
        public TextThumbImage[] ThumbItems()
        {
            List<TextThumbImage> rlist = new List<TextThumbImage>();
            var TextMaker = AltText.Default();
            var sam = new SuouText("sample");
            
            foreach (var v in this.Items)
            {
                var a = v.CreateWaku(sam, TextMaker);
                var b = new AstPage() { SText = a.Clone()};
                var c = new TextThumbImage(b, 0);
                rlist.Add(c);
            }
            return rlist.ToArray();
            
        }

        public void Add(IHukidasiLogic v)
        {
            Items.Add(v);
        }
        public void Add(HukidasiSaveData v,PrintFontSetting pfs)
        {
            Items.Add(HukidasiExtension.Create(v));
        }

        public List<string> CreateWaku(List<string> s, int i)
        {
            throw new NotImplementedException();
            
        }

        public List<string> CreateWaku(List<string> s, IHukidasiLogic h)
        {
            throw new NotImplementedException();
            
        }

        public static SaveFileDialog SaveFileDialogBox()
        {

            //SaveFileDialogクラスのインスタンスを作成
            SaveFileDialog sfd = new SaveFileDialog();

            //はじめに表示されるフォルダを指定する
            sfd.InitialDirectory = SuouEditorConst.DataDirectory + "/";
            //[ファイルの種類]に表示される選択肢を指定する
            sfd.Filter =
                "ファイル(*.hkd)|*.hkd|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに
            //「すべてのファイル」が選択されているようにする
            sfd.FilterIndex = 2;
            //タイトルを設定する
            sfd.Title = "保存先のファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            sfd.RestoreDirectory = false;
            //既に存在するファイル名を指定したとき警告する
            //デフォルトでTrueなので指定する必要はない
            sfd.OverwritePrompt = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            sfd.CheckPathExists = true;

            return sfd;
        }
        public static OpenFileDialog LoadFileDialogBox()
        {
            //SaveFileDialogクラスのインスタンスを作成
            OpenFileDialog ofd = new OpenFileDialog();

            //はじめに表示されるフォルダを指定する
            ofd.InitialDirectory = SuouEditorConst.DataDirectory+"/";
            //[ファイルの種類]に表示される選択肢を指定する
            ofd.Filter =
                "ファイル(*.hkd2)|*.hkd2|すべてのファイル(*.*)|*.*";
            ofd.FilterIndex = 1;
            ofd.Title = "開くファイルを選択してください";
            ofd.RestoreDirectory = false;
            ofd.CheckPathExists = true;

            return ofd;
        }
    }
}
