﻿namespace StoryViewer.AAToolBox.Hukidasi
{
    partial class FiveBlockWakuInputBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.TB54 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB14 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB53 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB13 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB52 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB12 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB51 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB41 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB31 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB21 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB11 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.previewTextBox1 = new SuouTextBox2.Controls.PreviewTextBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.sample1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sample2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sample3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.RegisterButton = new System.Windows.Forms.Button();
            this.TB55 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB45 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB35 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB25 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.TB15 = new StoryViewer.AAToolBox.Hukidasi.ElementInputBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.periodicSettingBox1 = new StoryViewer.AAToolBox.Hukidasi.PeriodicSettingBox();
            this.optionBox1 = new StoryViewer.AAToolBox.Hukidasi.OptionBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.TB54, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.TB14, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.TB53, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.TB13, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.TB52, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB12, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB51, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.TB41, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.TB31, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.TB21, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TB11, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB55, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.TB45, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.TB35, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.TB25, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.TB15, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(839, 526);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // TB54
            // 
            this.TB54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB54.ElementName = "(5,4)";
            this.TB54.ElementText = "";
            this.TB54.EnableOffset = true;
            this.TB54.IsLeft = false;
            this.TB54.Location = new System.Drawing.Point(673, 317);
            this.TB54.Name = "TB54";
            this.TB54.Offset = 0;
            this.TB54.Part = SuouBase.Text.HukidasiPart.RightCenterDown;
            this.TB54.Size = new System.Drawing.Size(161, 96);
            this.TB54.TabIndex = 32;
            // 
            // TB14
            // 
            this.TB14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB14.ElementName = "(1,4)";
            this.TB14.ElementText = "";
            this.TB14.EnableOffset = true;
            this.TB14.IsLeft = true;
            this.TB14.Location = new System.Drawing.Point(5, 317);
            this.TB14.Name = "TB14";
            this.TB14.Offset = 0;
            this.TB14.Part = SuouBase.Text.HukidasiPart.LeftCenterDown;
            this.TB14.Size = new System.Drawing.Size(159, 96);
            this.TB14.TabIndex = 31;
            // 
            // TB53
            // 
            this.TB53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB53.ElementName = "(5,3)";
            this.TB53.ElementText = "";
            this.TB53.EnableOffset = true;
            this.TB53.IsLeft = false;
            this.TB53.Location = new System.Drawing.Point(673, 213);
            this.TB53.Name = "TB53";
            this.TB53.Offset = 0;
            this.TB53.Part = SuouBase.Text.HukidasiPart.RightCenter;
            this.TB53.Size = new System.Drawing.Size(161, 96);
            this.TB53.TabIndex = 30;
            // 
            // TB13
            // 
            this.TB13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB13.ElementName = "(1,3)";
            this.TB13.ElementText = "";
            this.TB13.EnableOffset = true;
            this.TB13.IsLeft = true;
            this.TB13.Location = new System.Drawing.Point(5, 213);
            this.TB13.Name = "TB13";
            this.TB13.Offset = 0;
            this.TB13.Part = SuouBase.Text.HukidasiPart.LeftCenter;
            this.TB13.Size = new System.Drawing.Size(159, 96);
            this.TB13.TabIndex = 29;
            // 
            // TB52
            // 
            this.TB52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB52.ElementName = "(5,2)";
            this.TB52.ElementText = "";
            this.TB52.EnableOffset = true;
            this.TB52.IsLeft = false;
            this.TB52.Location = new System.Drawing.Point(673, 109);
            this.TB52.Name = "TB52";
            this.TB52.Offset = 0;
            this.TB52.Part = SuouBase.Text.HukidasiPart.RightCenterUp;
            this.TB52.Size = new System.Drawing.Size(161, 96);
            this.TB52.TabIndex = 28;
            // 
            // TB12
            // 
            this.TB12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB12.ElementName = "(1,2)";
            this.TB12.ElementText = "";
            this.TB12.EnableOffset = true;
            this.TB12.IsLeft = true;
            this.TB12.Location = new System.Drawing.Point(5, 109);
            this.TB12.Name = "TB12";
            this.TB12.Offset = 0;
            this.TB12.Part = SuouBase.Text.HukidasiPart.LeftCenterUp;
            this.TB12.Size = new System.Drawing.Size(159, 96);
            this.TB12.TabIndex = 27;
            // 
            // TB51
            // 
            this.TB51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB51.ElementName = "(5,1)";
            this.TB51.ElementText = "";
            this.TB51.EnableOffset = true;
            this.TB51.IsLeft = false;
            this.TB51.Location = new System.Drawing.Point(673, 5);
            this.TB51.Name = "TB51";
            this.TB51.Offset = 0;
            this.TB51.Part = SuouBase.Text.HukidasiPart.RightUpCorner;
            this.TB51.Size = new System.Drawing.Size(161, 96);
            this.TB51.TabIndex = 26;
            // 
            // TB41
            // 
            this.TB41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB41.ElementName = "(4,1)";
            this.TB41.ElementText = "";
            this.TB41.EnableOffset = false;
            this.TB41.IsLeft = true;
            this.TB41.Location = new System.Drawing.Point(506, 5);
            this.TB41.Name = "TB41";
            this.TB41.Offset = 0;
            this.TB41.Part = SuouBase.Text.HukidasiPart.UpCenterRight;
            this.TB41.Size = new System.Drawing.Size(159, 96);
            this.TB41.TabIndex = 25;
            // 
            // TB31
            // 
            this.TB31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB31.ElementName = "(3,1)";
            this.TB31.ElementText = "";
            this.TB31.EnableOffset = false;
            this.TB31.IsLeft = true;
            this.TB31.Location = new System.Drawing.Point(339, 5);
            this.TB31.Name = "TB31";
            this.TB31.Offset = 0;
            this.TB31.Part = SuouBase.Text.HukidasiPart.UpCenter;
            this.TB31.Size = new System.Drawing.Size(159, 96);
            this.TB31.TabIndex = 24;
            // 
            // TB21
            // 
            this.TB21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB21.ElementName = "(2,1)";
            this.TB21.ElementText = "";
            this.TB21.EnableOffset = false;
            this.TB21.IsLeft = true;
            this.TB21.Location = new System.Drawing.Point(172, 5);
            this.TB21.Name = "TB21";
            this.TB21.Offset = 0;
            this.TB21.Part = SuouBase.Text.HukidasiPart.UpCenterLeft;
            this.TB21.Size = new System.Drawing.Size(159, 96);
            this.TB21.TabIndex = 23;
            // 
            // TB11
            // 
            this.TB11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB11.ElementName = "(1,1)";
            this.TB11.ElementText = "";
            this.TB11.EnableOffset = true;
            this.TB11.IsLeft = true;
            this.TB11.Location = new System.Drawing.Point(5, 5);
            this.TB11.Name = "TB11";
            this.TB11.Offset = 0;
            this.TB11.Part = SuouBase.Text.HukidasiPart.LeftUpCorner;
            this.TB11.Size = new System.Drawing.Size(159, 96);
            this.TB11.TabIndex = 22;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.tableLayoutPanel1.SetColumnSpan(this.panel1, 3);
            this.panel1.Controls.Add(this.groupBox3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(172, 109);
            this.panel1.Name = "panel1";
            this.tableLayoutPanel1.SetRowSpan(this.panel1, 3);
            this.panel1.Size = new System.Drawing.Size(493, 304);
            this.panel1.TabIndex = 16;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.previewTextBox1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(493, 186);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "テストビュー";
            // 
            // previewTextBox1
            // 
            this.previewTextBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.previewTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previewTextBox1.Location = new System.Drawing.Point(3, 15);
            this.previewTextBox1.Name = "previewTextBox1";
            this.previewTextBox1.Size = new System.Drawing.Size(487, 168);
            this.previewTextBox1.TabIndex = 0;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.sample1ToolStripMenuItem,
            this.sample2ToolStripMenuItem,
            this.sample3ToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(180, 76);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(176, 6);
            // 
            // sample1ToolStripMenuItem
            // 
            this.sample1ToolStripMenuItem.Name = "sample1ToolStripMenuItem";
            this.sample1ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.sample1ToolStripMenuItem.Text = "サンプルテキスト1";
            this.sample1ToolStripMenuItem.Click += new System.EventHandler(this.sample1ToolStripMenuItem_Click);
            // 
            // sample2ToolStripMenuItem
            // 
            this.sample2ToolStripMenuItem.Name = "sample2ToolStripMenuItem";
            this.sample2ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.sample2ToolStripMenuItem.Text = "サンプルテキスト2";
            this.sample2ToolStripMenuItem.Click += new System.EventHandler(this.sample2ToolStripMenuItem_Click);
            // 
            // sample3ToolStripMenuItem
            // 
            this.sample3ToolStripMenuItem.Name = "sample3ToolStripMenuItem";
            this.sample3ToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.sample3ToolStripMenuItem.Text = "サンプルテキスト3";
            this.sample3ToolStripMenuItem.Click += new System.EventHandler(this.sample3ToolStripMenuItem_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listBox1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox2.Location = new System.Drawing.Point(0, 186);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(493, 90);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "エラーメッセージ";
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(3, 15);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(487, 72);
            this.listBox1.TabIndex = 9;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.RegisterButton);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 276);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(493, 28);
            this.panel2.TabIndex = 7;
            // 
            // button2
            // 
            this.button2.AutoSize = true;
            this.button2.Location = new System.Drawing.Point(13, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(97, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "既存の枠をコピー";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.Location = new System.Drawing.Point(415, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "クリア";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // RegisterButton
            // 
            this.RegisterButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.RegisterButton.Enabled = false;
            this.RegisterButton.Location = new System.Drawing.Point(334, 3);
            this.RegisterButton.Name = "RegisterButton";
            this.RegisterButton.Size = new System.Drawing.Size(75, 23);
            this.RegisterButton.TabIndex = 6;
            this.RegisterButton.Text = "登録する";
            this.RegisterButton.UseVisualStyleBackColor = true;
            this.RegisterButton.Click += new System.EventHandler(this.button4_Click);
            // 
            // TB55
            // 
            this.TB55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB55.ElementName = "(5,5)";
            this.TB55.ElementText = "";
            this.TB55.EnableOffset = true;
            this.TB55.IsLeft = false;
            this.TB55.Location = new System.Drawing.Point(673, 421);
            this.TB55.Name = "TB55";
            this.TB55.Offset = 0;
            this.TB55.Part = SuouBase.Text.HukidasiPart.RightDownCorner;
            this.TB55.Size = new System.Drawing.Size(161, 100);
            this.TB55.TabIndex = 17;
            // 
            // TB45
            // 
            this.TB45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB45.ElementName = "(4,5)";
            this.TB45.ElementText = "";
            this.TB45.EnableOffset = false;
            this.TB45.IsLeft = true;
            this.TB45.Location = new System.Drawing.Point(506, 421);
            this.TB45.Name = "TB45";
            this.TB45.Offset = 0;
            this.TB45.Part = SuouBase.Text.HukidasiPart.DownCenterRight;
            this.TB45.Size = new System.Drawing.Size(159, 100);
            this.TB45.TabIndex = 18;
            // 
            // TB35
            // 
            this.TB35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB35.ElementName = "(3,5)";
            this.TB35.ElementText = "";
            this.TB35.EnableOffset = false;
            this.TB35.IsLeft = true;
            this.TB35.Location = new System.Drawing.Point(339, 421);
            this.TB35.Name = "TB35";
            this.TB35.Offset = 0;
            this.TB35.Part = SuouBase.Text.HukidasiPart.DownCenter;
            this.TB35.Size = new System.Drawing.Size(159, 100);
            this.TB35.TabIndex = 19;
            // 
            // TB25
            // 
            this.TB25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB25.ElementName = "(2,5)";
            this.TB25.ElementText = "";
            this.TB25.EnableOffset = false;
            this.TB25.IsLeft = true;
            this.TB25.Location = new System.Drawing.Point(172, 421);
            this.TB25.Name = "TB25";
            this.TB25.Offset = 0;
            this.TB25.Part = SuouBase.Text.HukidasiPart.DownCenterLeft;
            this.TB25.Size = new System.Drawing.Size(159, 100);
            this.TB25.TabIndex = 20;
            // 
            // TB15
            // 
            this.TB15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB15.ElementName = "(1,5)";
            this.TB15.ElementText = "";
            this.TB15.EnableOffset = true;
            this.TB15.IsLeft = true;
            this.TB15.Location = new System.Drawing.Point(5, 421);
            this.TB15.Name = "TB15";
            this.TB15.Offset = 0;
            this.TB15.Part = SuouBase.Text.HukidasiPart.LeftDownCorner;
            this.TB15.Size = new System.Drawing.Size(159, 100);
            this.TB15.TabIndex = 21;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.periodicSettingBox1);
            this.groupBox4.Controls.Add(this.optionBox1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 526);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(839, 179);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "詳細設定";
            // 
            // periodicSettingBox1
            // 
            this.periodicSettingBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.periodicSettingBox1.Location = new System.Drawing.Point(3, 89);
            this.periodicSettingBox1.Name = "periodicSettingBox1";
            this.periodicSettingBox1.Size = new System.Drawing.Size(833, 87);
            this.periodicSettingBox1.TabIndex = 6;
            // 
            // optionBox1
            // 
            this.optionBox1.CenterZureLength = 0;
            this.optionBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.optionBox1.Location = new System.Drawing.Point(3, 15);
            this.optionBox1.Name = "optionBox1";
            this.optionBox1.Size = new System.Drawing.Size(833, 74);
            this.optionBox1.TabIndex = 5;
            this.optionBox1.UpCenterBottomValue = 0;
            this.optionBox1.WakuName = "";
            // 
            // FiveBlockWakuInputBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "FiveBlockWakuInputBox";
            this.Size = new System.Drawing.Size(839, 705);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.contextMenuStrip1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button RegisterButton;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListBox listBox1;
        private ElementInputBox TB55;
        private ElementInputBox TB45;
        private ElementInputBox TB35;
        private ElementInputBox TB25;
        private ElementInputBox TB15;
        private ElementInputBox TB54;
        private ElementInputBox TB14;
        private ElementInputBox TB53;
        private ElementInputBox TB13;
        private ElementInputBox TB52;
        private ElementInputBox TB12;
        private ElementInputBox TB51;
        private ElementInputBox TB41;
        private ElementInputBox TB31;
        private ElementInputBox TB21;
        private ElementInputBox TB11;
        private System.Windows.Forms.GroupBox groupBox3;
        private SuouTextBox2.Controls.PreviewTextBox previewTextBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private OptionBox optionBox1;
        private PeriodicSettingBox periodicSettingBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem sample1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sample2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sample3ToolStripMenuItem;
    }
}
