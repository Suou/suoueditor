﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Controls.Hukidasi;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using BSystem.Collections;
using SuouBase;
namespace StoryViewer.AAToolBox.Hukidasi
{
    public partial class HukidasiCreateForm : BSystem.Controls.SerializableForm
    {
        public HukidasiCreateForm()
        {
            InitializeComponent();
        }
        public HukidasiCreateForm(ObservableCollection<HukidasiSaveData> items):this()
        {
            this.fiveBlockWakuInputBox1.Items = items;
        }

        private void fiveBlockWakuInputBox1_Registed(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Visible = false;
            
        }
        public DialogResult ShowDialogReplaceMode(HukidasiSaveData data)
        {
            this.fiveBlockWakuInputBox1.SetItem(data);
            this.fiveBlockWakuInputBox1.IsReplaceMode = true;
            return this.ShowDialog();
        }
        public DialogResult ShowDialogAddMode()
        {
            this.fiveBlockWakuInputBox1.SetItem(HukidasiSaveData.FiveDefault());
            this.fiveBlockWakuInputBox1.IsReplaceMode = false;
            return this.ShowDialog();

        }
        public void Reset()
        {
            this.fiveBlockWakuInputBox1.SetItem(HukidasiSaveData.FiveDefault());
            this.fiveBlockWakuInputBox1.IsReplaceMode = false;
        }
        
        public void SetItem(HukidasiSaveData savedata)
        {
            this.fiveBlockWakuInputBox1.SetItem(savedata);
        }
        
        public HukidasiSaveData GetItem()
        {
            return this.fiveBlockWakuInputBox1.GetItem();
        }

        private void HukidasiCreateForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Visible = false;
            }
        }

    }
}
