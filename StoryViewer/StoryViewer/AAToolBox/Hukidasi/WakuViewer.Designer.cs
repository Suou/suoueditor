﻿namespace StoryViewer.AAToolBox.Hukidasi
{
    partial class WakuViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveUpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveDownToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.thumContViewBox1 = new StoryViewer.ThumContViewBox();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.moveUpToolStripMenuItem,
            this.moveDownToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 70);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.deleteToolStripMenuItem.Text = "削除";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // moveUpToolStripMenuItem
            // 
            this.moveUpToolStripMenuItem.Name = "moveUpToolStripMenuItem";
            this.moveUpToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.moveUpToolStripMenuItem.Text = "上に移動";
            this.moveUpToolStripMenuItem.Click += new System.EventHandler(this.moveUpToolStripMenuItem_Click);
            // 
            // moveDownToolStripMenuItem
            // 
            this.moveDownToolStripMenuItem.Name = "moveDownToolStripMenuItem";
            this.moveDownToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.moveDownToolStripMenuItem.Text = "下に移動";
            this.moveDownToolStripMenuItem.Click += new System.EventHandler(this.moveDownToolStripMenuItem_Click);
            // 
            // thumContViewBox1
            // 
            this.thumContViewBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.thumContViewBox1.BoxType = StoryViewer.ImageItemBoxType.Normal;
            this.thumContViewBox1.CanItemDrag = false;
            this.thumContViewBox1.CanItemMove = false;
            this.thumContViewBox1.ControlContext = null;
            this.thumContViewBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumContViewBox1.EnableNameTextBox = StoryViewer.NameTextBoxType.ReadOnly;
            this.thumContViewBox1.IsLoose = false;
            this.thumContViewBox1.IsMouseMoveFocus = false;
            this.thumContViewBox1.IsTransparentDrag = false;
            this.thumContViewBox1.ItemContext = this.contextMenuStrip1;
            this.thumContViewBox1.ItemInterval = 5;
            this.thumContViewBox1.ItemSize = new System.Drawing.Size(100, 100);
            this.thumContViewBox1.Location = new System.Drawing.Point(0, 0);
            this.thumContViewBox1.ManualDragDo = false;
            this.thumContViewBox1.MinItemSize = new System.Drawing.Size(16, 16);
            this.thumContViewBox1.MouseDownItem = null;
            this.thumContViewBox1.Name = "thumContViewBox1";
            this.thumContViewBox1.OrderMode = StoryViewer.ThumbnailOrder.Thread;
            this.thumContViewBox1.Percent = 0.5F;
            this.thumContViewBox1.PermitCategory = true;
            this.thumContViewBox1.ShiftItemContext = null;
            this.thumContViewBox1.Size = new System.Drawing.Size(380, 418);
            this.thumContViewBox1.TabIndex = 0;
            this.thumContViewBox1.ViewMode = SuouBase.Drawing.ThumbnailMode.Raw;
            this.thumContViewBox1.ItemClicked += new StoryViewer.ThumbnailViewEventHandler(this.thumContViewBox1_ItemClicked);
            this.thumContViewBox1.ItemSelected += new StoryViewer.ThumbnailViewEventHandler(this.thumContViewBox1_ItemSelected);
            // 
            // WakuViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.thumContViewBox1);
            this.Name = "WakuViewer";
            this.Size = new System.Drawing.Size(380, 418);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private ThumContViewBox thumContViewBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveUpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem moveDownToolStripMenuItem;
    }
}
