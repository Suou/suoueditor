﻿namespace StoryViewer.AAToolBox.Hukidasi
{
    partial class OptionBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.UpCenterBottomValueBox = new System.Windows.Forms.NumericUpDown();
            this.CenterValueBox = new System.Windows.Forms.NumericUpDown();
            this.spacerComboBox1 = new SuouBase.Controls.SpacerComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpCenterBottomValueBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CenterValueBox)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.spacerComboBox1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.UpCenterBottomValueBox);
            this.groupBox1.Controls.Add(this.CenterValueBox);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(502, 99);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "設定";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(267, 47);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(117, 19);
            this.textBox1.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 5;
            this.label3.Text = "吹き出し名";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "中段内のズレ許容";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "上中下段のズレ許容";
            // 
            // UpCenterBottomValueBox
            // 
            this.UpCenterBottomValueBox.Location = new System.Drawing.Point(118, 18);
            this.UpCenterBottomValueBox.Name = "UpCenterBottomValueBox";
            this.UpCenterBottomValueBox.Size = new System.Drawing.Size(50, 19);
            this.UpCenterBottomValueBox.TabIndex = 1;
            // 
            // CenterValueBox
            // 
            this.CenterValueBox.Location = new System.Drawing.Point(118, 45);
            this.CenterValueBox.Name = "CenterValueBox";
            this.CenterValueBox.Size = new System.Drawing.Size(50, 19);
            this.CenterValueBox.TabIndex = 0;
            // 
            // spacerComboBox1
            // 
            this.spacerComboBox1.Location = new System.Drawing.Point(204, 15);
            this.spacerComboBox1.Name = "spacerComboBox1";
            this.spacerComboBox1.Size = new System.Drawing.Size(217, 26);
            this.spacerComboBox1.TabIndex = 7;
            // 
            // OptionBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "OptionBox";
            this.Size = new System.Drawing.Size(502, 99);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UpCenterBottomValueBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CenterValueBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown UpCenterBottomValueBox;
        private System.Windows.Forms.NumericUpDown CenterValueBox;
        private SuouBase.Controls.SpacerComboBox spacerComboBox1;
    }
}
