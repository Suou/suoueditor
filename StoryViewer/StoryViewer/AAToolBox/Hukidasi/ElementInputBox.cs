﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
namespace StoryViewer.AAToolBox.Hukidasi
{
    [ToolboxItem(false)]
    public partial class ElementInputBox : UserControl
    {
        public ElementInputBox()
        {
            InitializeComponent();
        }
        public bool IsLeft
        {
            get { return this.panel1.Dock == DockStyle.Left; }
            set
            {
                this.panel1.Dock = value ? DockStyle.Left : DockStyle.Right;
            }
        }
        public HukidasiPart Part
        {
            get;
            set;
        }
        public int Offset
        {
            get { return (int)this.numericUpDown1.Value; }
            set { this.numericUpDown1.Value = value; }
        }
        public string ElementText
        {
            get { return this.textBox1.Text; }
            set { this.textBox1.Text = value; }
        }
        private bool _EnableOffset = true;
        public bool EnableOffset
        {
            get { return this._EnableOffset; }
            set 
            {
                this._EnableOffset = value;
                if (_EnableOffset)
                {
                    this.panel1.Visible = true;
                }
                else
                {
                    this.panel1.Visible = false;
                }
            }
        }
        public string ElementName
        {
            get { return this.groupBox1.Text; }
            set { this.groupBox1.Text = value; }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public LogicElement Item
        {
            get
            {
                return new LogicElement(new SuouText(this.textBox1.Text), this.Offset, this.Part);
            }
            set
            {
                if (value != null)
                {
                    this.textBox1.Text = value.Text.Text;
                    this.Offset = value.Offset;
                    this.Part = value.Part;
                }
            }
        }
        public TextBox TextBox
        {
            get { return this.textBox1; }
        }
        public NumericUpDown NumericBox { get { return this.numericUpDown1; } }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
