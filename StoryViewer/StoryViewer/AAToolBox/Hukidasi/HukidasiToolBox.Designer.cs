﻿namespace StoryViewer
{
    partial class HukidasiToolBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }
        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BunretuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.addingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.initializeFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.thumbnailViewBox1 = new StoryViewer.ThumContViewBox();
            this.contextMenuStrip1.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.BunretuToolStripMenuItem,
            this.settingToolStripMenuItem,
            this.cutToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(125, 92);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.deleteToolStripMenuItem.Text = "削除";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // BunretuToolStripMenuItem
            // 
            this.BunretuToolStripMenuItem.Name = "BunretuToolStripMenuItem";
            this.BunretuToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.BunretuToolStripMenuItem.Text = "分裂";
            this.BunretuToolStripMenuItem.Click += new System.EventHandler(this.BunretuToolStripMenuItem_Click);
            // 
            // settingToolStripMenuItem
            // 
            this.settingToolStripMenuItem.Name = "settingToolStripMenuItem";
            this.settingToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.settingToolStripMenuItem.Text = "設定";
            this.settingToolStripMenuItem.Click += new System.EventHandler(this.settingToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.cutToolStripMenuItem.Text = "切り取り";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addingToolStripMenuItem,
            this.pasteToolStripMenuItem});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(125, 48);
            this.contextMenuStrip2.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip2_Opening);
            // 
            // addingToolStripMenuItem
            // 
            this.addingToolStripMenuItem.Name = "addingToolStripMenuItem";
            this.addingToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.addingToolStripMenuItem.Text = "追加";
            this.addingToolStripMenuItem.Click += new System.EventHandler(this.addingToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.pasteToolStripMenuItem.Text = "貼り付け";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(373, 26);
            this.menuStrip1.TabIndex = 4;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.initializeFileToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(68, 22);
            this.fileToolStripMenuItem.Text = "ファイル";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.loadToolStripMenuItem.Text = "フキダシファイルを開く";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.saveToolStripMenuItem.Text = "フキダシファイルを保存する";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // initializeFileToolStripMenuItem
            // 
            this.initializeFileToolStripMenuItem.Name = "initializeFileToolStripMenuItem";
            this.initializeFileToolStripMenuItem.Size = new System.Drawing.Size(232, 22);
            this.initializeFileToolStripMenuItem.Text = "初期化する";
            this.initializeFileToolStripMenuItem.Click += new System.EventHandler(this.initializeFileToolStripMenuItem_Click);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // thumbnailViewBox1
            // 
            this.thumbnailViewBox1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.thumbnailViewBox1.BoxType = StoryViewer.ImageItemBoxType.Normal;
            this.thumbnailViewBox1.CanItemDrag = true;
            this.thumbnailViewBox1.CanItemMove = false;
            this.thumbnailViewBox1.ControlContext = this.contextMenuStrip2;
            this.thumbnailViewBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thumbnailViewBox1.EnableNameTextBox = StoryViewer.NameTextBoxType.None;
            this.thumbnailViewBox1.IsLoose = false;
            this.thumbnailViewBox1.IsMouseMoveFocus = false;
            this.thumbnailViewBox1.IsTransparentDrag = false;
            this.thumbnailViewBox1.ItemContext = this.contextMenuStrip1;
            this.thumbnailViewBox1.ItemInterval = 5;
            this.thumbnailViewBox1.ItemSize = new System.Drawing.Size(100, 100);
            this.thumbnailViewBox1.Location = new System.Drawing.Point(0, 26);
            this.thumbnailViewBox1.ManualDragDo = true;
            this.thumbnailViewBox1.MinItemSize = new System.Drawing.Size(16, 16);
            this.thumbnailViewBox1.MouseDownItem = null;
            this.thumbnailViewBox1.Name = "thumbnailViewBox1";
            this.thumbnailViewBox1.OrderMode = StoryViewer.ThumbnailOrder.Thread;
            this.thumbnailViewBox1.Percent = 0.9F;
            this.thumbnailViewBox1.PermitCategory = true;
            this.thumbnailViewBox1.ShiftItemContext = null;
            this.thumbnailViewBox1.Size = new System.Drawing.Size(373, 316);
            this.thumbnailViewBox1.TabIndex = 1;
            this.thumbnailViewBox1.ViewMode = SuouBase.Drawing.ThumbnailMode.Raw;
            this.thumbnailViewBox1.ItemClicked += new StoryViewer.ThumbnailViewEventHandler(this.thumbnailViewBox1_ItemClicked);
            this.thumbnailViewBox1.ItemDragStarting += new StoryViewer.ThumbnailViewEventHandler(this.thumbnailViewBox1_ItemDragStarting);
            this.thumbnailViewBox1.ItemSelected += new StoryViewer.ThumbnailViewEventHandler(this.thumbnailViewBox1_ItemSelected);
            this.thumbnailViewBox1.ItemDataChanged += new StoryViewer.ThumbnailChangeItemEventHandler(this.thumbnailViewBox1_ItemDataChanged);
            this.thumbnailViewBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.thumbnailViewBox1_MouseMove);
            // 
            // HukidasiToolBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.thumbnailViewBox1);
            this.Controls.Add(this.menuStrip1);
            this.Name = "HukidasiToolBox";
            this.Size = new System.Drawing.Size(373, 342);
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ThumContViewBox thumbnailViewBox1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem addingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BunretuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem initializeFileToolStripMenuItem;
        private System.Windows.Forms.Timer timer1;


    }
}
