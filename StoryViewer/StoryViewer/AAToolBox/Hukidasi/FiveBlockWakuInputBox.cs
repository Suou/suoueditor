﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Controls.Hukidasi;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using BSystem.Collections;
namespace StoryViewer.AAToolBox.Hukidasi
{
    public partial class FiveBlockWakuInputBox : UserControl,IWakuParent
    {
        public FiveBlockWakuInputBox()
        {
            InitializeComponent();

            this.SampleText = global::StoryViewer.Properties.Resources.SampleText2;
            this.Checker = new WakuBoxCheck(this.TextBoxItems,this.ButtonItems,this.listBox1,this.OffsetBoxItems,this) { VisibleErrorMessage = true};
            this.Checker.SetItem(HukidasiSaveData.FiveDefault());
        }
        public event EventHandler Registed;
        //public new event EventHandler TextChanged;
        #region Property
        private bool _IsReplaceMode;
        public bool IsReplaceMode
        {
            get { return this._IsReplaceMode; }
            set
            {
                this._IsReplaceMode = value;
                this.RegisterButton.Text = this.IsReplaceMode ? "変更する" : "登録する";
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public Button[] ButtonItems
        {
            get
            {
                return new Button[] { RegisterButton };
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public WakuArrangement Periodics
        {
            get { return this.periodicSettingBox1.Periodics; }
            set
            {
                this.periodicSettingBox1.Periodics = value;
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public TextBox[] PeriodicBoxes
        {
            get { return this.periodicSettingBox1.PeriodicBoxes; }
        }

        public ListBox ErrorBox
        {
            get { return this.listBox1; }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public IWakuOptionParent OptionBox
        {
            get
            {
                return this.optionBox1;
            }
        }
        private ObservableCollection<HukidasiSaveData> _Items = new ObservableCollection<HukidasiSaveData>();
        [Browsable(false)]
        [ReadOnly(true)]
        public ObservableCollection<HukidasiSaveData> Items
        {
            get { return this._Items; }
            set
            {
                if (value == null) return;
                this._Items = value;
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        private WakuBoxCheck Checker { get; set; }

        [Browsable(false)]
        [ReadOnly(true)]
        public TextBox[] TextBoxItems
        {
            get
            {
                return new TextBox[] { this.TB11.TextBox,this.TB21.TextBox,this.TB31.TextBox,this.TB41.TextBox,this.TB51.TextBox,
                this.TB12.TextBox,this.TB52.TextBox,this.TB13.TextBox,this.TB53.TextBox,this.TB14.TextBox,this.TB54.TextBox,
                this.TB15.TextBox,this.TB25.TextBox,this.TB35.TextBox,this.TB45.TextBox,this.TB55.TextBox};
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public NumericUpDown[] OffsetBoxItems
        {
            get
            {
                return new NumericUpDown[]{ this.TB11.NumericBox,this.TB12.NumericBox,this.TB13.NumericBox,this.TB14.NumericBox,this.TB15.NumericBox,
                    this.TB51.NumericBox,this.TB52.NumericBox,this.TB53.NumericBox,this.TB54.NumericBox,this.TB55.NumericBox
                };
            }
        }
        #endregion
        private string SampleText { get; set; }
        public HukidasiSaveData GetItem()
        {
            return this.Checker.GetItem();
        }
        public CheckBox[] CheckBoxes
        {
            get
            {
                return this.periodicSettingBox1.CheckBoxes;
            }

        }
        #region Method
        public void SetItem(HukidasiSaveData h)
        {
            this.Checker.SetItem(h);
        }
        public void Preview()
        {
            if (this.Checker.Check())
            {
                var v = this.Checker.GetItem();
                var fg = v.CreateWakuText(new SuouText(this.SampleText));
                this.previewTextBox1.SText = fg;
            }
        }
        private void Clear()
        {
            //this.UpCenterBottomValue = 0;
            //this.CenterZureLength = 0;
            this.Checker.SetItem(HukidasiSaveData.FiveDefault());
        }
        #endregion
        #region Event
        private void button1_Click(object sender, EventArgs e)
        {
            var v = new ToolTip();
            //v.Show("test", this.TB11,-100,0);
            
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            this.Checker.VisibleErrorMessage = true;
            this.Checker.Check();
        }
        

        private void button2_Click(object sender, EventArgs e)
        {
            var v = new StoryViewer.AAToolBox.Hukidasi.WakuViewer();
             
            v.Items = this.Items;
            var f = new BSystem.Controls.SingleForm(v, "枠を選択してください") { Icon = SuouTextBox2.Properties.Resources.turu_perple};
            v.ItemSelected += (action, ee) => f.DialogResult = DialogResult.OK;
            v.Dock = DockStyle.Fill;
            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                this.SetItem(v.SelectedItem);
            }
        }

        private void UpCenterBottomValueBox_ValueChanged(object sender, EventArgs e)
        {
            //this.Checker.UpCenterBottomValue = this.UpCenterBottomValue;
            this.Checker.Check();
        }


        private void CenterValueBox_ValueChanged(object sender, EventArgs e)
        {
            //this.Checker.CenterZureLength = this.CenterZureLength;
            this.Checker.Check();
        }
        
        private void button3_Click(object sender, EventArgs e)
        {
            if (this.Checker.Check())
            {
                var v = this.Checker.GetItem();
                //var p = new StoryViewer.AAToolBox.Hukidasi.WakuViewer() { SampleText = new SuouText(this.serifu) };
                //p.Items = new ObservableCollection<HukidasiSaveData>() { v };

                var fg = v.CreateWakuText(new SuouText(this.SampleText));
                this.previewTextBox1.SText = fg;
                //this.bookPagePicture1.SText = fg;
                //var f = new SuouBase.Controls.GeneralForm() { Item = p, Text = "テストビュー" };
                //f.ShowDialog();
            }
        }
        private void button4_Click(object sender, EventArgs e)
        {
            if (this.Checker.Check())
            {
                if (this.IsReplaceMode)
                {
                }
                else
                {
                    //this.Items.Add(this.Checker.GetItem());
                    MessageBox.Show("登録しました");
                }
                if (this.Registed != null) this.Registed(this, e);
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Clear();
            //this.TextBoxItems.ForEach((v) => v.Text = "");
        }


        
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {

        }


        private void sample1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SampleText = global::StoryViewer.Properties.Resources.SampleText2;
            this.Preview();
        }

        private void sample2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SampleText = global::StoryViewer.Properties.Resources.SampleText1;
            this.Preview();
        }

        private void sample3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SampleText = global::StoryViewer.Properties.Resources.SampleText3;
            this.Preview();
        }
        #endregion
    }
}
