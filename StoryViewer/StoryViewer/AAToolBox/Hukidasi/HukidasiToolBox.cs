﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
using System.IO;
using SuouTextBox2.Base;
using SuouTextBox2;
using SuouBase.Text;
using SuouBase.Drawing;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Xml.Linq;
using BSystem;
using BSystem.Collections;
using System.Xml.Serialization;
using SuouBase.Text.Hukidasi;
using StoryViewer.AAToolBox.Hukidasi;
using BSystem.Controls;
namespace StoryViewer
{
    [ToolboxItem(false)]
    public partial class HukidasiToolBox : UserControl
    {
        #region Property
        private EditorInfo _DLLFunction;
        public EditorInfo DLLFunction
        {
            get { return this._DLLFunction; }
            set
            {
                this._DLLFunction = value;
                if (this.DLLFunction != null)
                {
                    this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.CollectionChanged += this.HukidasiSaveDataList_Changed;
                    this.RegisterForm = new HukidasiCreateForm(this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList);
                    this.RefreshItem();
                }
            }
        }
        public event HukidasiEventHandler HukidasiSelected;
        //private List<IHukidasiLogic> HukidasiList { get; set; }
        public bool IsInstant { get; set; }
        private int ThumControlSelectedIndex { get; set; }
        public AltText TextMaker = AltText.Default();

        private HukidasiCreateForm RegisterForm { get; set; }
        public ObservableCollection<HukidasiSaveData> DLLSaveList
        {
            get
            {
                return this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList;
            }
        }
        public HukidasiSaveData SelectedHukdiasi
        {
            get
            {
                if (this.ThumControlSelectedIndex >= 0 && this.ThumControlSelectedIndex < this.DLLSaveList.Count)
                {
                    return this.DLLSaveList[this.ThumControlSelectedIndex];
                }
                else
                {
                    return null;
                }
            }
        }

        public FormSaveData Setting
        {
            get { return this.RegisterForm.Setting; }
            set { this.RegisterForm.Setting = value; }
        }
        #endregion
        public ThumbnailOrder Mode
        {
            get { return this.thumbnailViewBox1.OrderMode; }
            set { this.thumbnailViewBox1.OrderMode = value; }
        }
        #region Constructor
        public HukidasiToolBox()
        {
            InitializeComponent();
            

        }
        public HukidasiToolBox(EditorInfo dllfunction):this()
        {
            this.DLLFunction = dllfunction;
            this.Items = this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList;
            //if (SaveFileName != null) this.Load();

        }
        #endregion
        #region Method
        public void RefreshItem()
        {
             SetPicture();
        }
        public void SetPicture()
        {
            
            Action func = SetPicFunc;
            func();
            
        }
        public ObservableCollection<HukidasiSaveData> Items { get; set; }
        public bool IsReadItem { get; private set; }

        private void SetPicFunc()
        {

            //HukidasiList = new List<IHukidasiLogic>();
            this.thumbnailViewBox1.Operator.Clear();
            
            if(this.Items != null)this.SetRangePicturePart(this.Items);
        }
        //いつか削除する
        private void SetRangePicturePart(IEnumerable<HukidasiSaveData> items)
        {
            var pitems = items.Select((v) => this.SetPicturePart2(v));
            this.thumbnailViewBox1.Operator.ImageItemSet(pitems);
            //this.thumbnailViewBox1.Operator.AddRange(pitems);
        }
        private TextThumbImage SetPicturePart2(HukidasiSaveData item)
        {
            if (item.MergeAlgorithm == null) throw new MyException("");
            //item.Logic = HukidasiExtension.Create(item);
            var v = item.Logic.CreateSamplePage(TextMaker);
            var f = new TextThumbImage(v, 0);
            return f;
        }
        private void Replace(int n)
        {
            //this.DLLSaveList[n].Logic = HukidasiExtension.Create(this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList[n]);
            var v = this.DLLSaveList[n].Logic.CreateSamplePage(TextMaker);
            var f = new TextThumbImage(v, 0);
            this.thumbnailViewBox1.Operator.Delete(n);
            this.thumbnailViewBox1.Operator.Insert(n, f);
        }
        
        private void Insert(int n)
        {
            //this.DLLSaveList[n].Logic = HukidasiExtension.Create(this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList[n]);
            var v = this.DLLSaveList[n].Logic.CreateSamplePage(TextMaker);
            var f = new TextThumbImage(v, 0);
            this.thumbnailViewBox1.Operator.Insert(n, f);
        }
        
        private void Add(HukidasiSaveData w)
        {
            
            //w.Logic = HukidasiExtension.Create(w);
            
            var v = w.Logic.CreateSamplePage(TextMaker);
            var f = new TextThumbImage(v, 0);
            this.thumbnailViewBox1.Operator.Add(f);


        }

        #endregion
        #region Event
        private void thumbnailViewBox1_ItemClicked(object sender, ThumbnailViewEventArgs e)
        {
            this.ThumControlSelectedIndex = e.SelectedIndex;
            this.ParentForm.DialogResult = DialogResult.OK;
            if (HukidasiSelected != null) HukidasiSelected(this, new HukidasiEventArgs(this.SelectedHukdiasi));
        }
        private void thumbnailViewBox1_ItemDragStarting(object sender, ThumbnailViewEventArgs e)
        {
            if (0 <= e.SelectedIndex && e.SelectedIndex < this.DLLSaveList.Count)
            {
                var f = this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList[e.SelectedIndex];
                this.thumbnailViewBox1.DoDragDrop(f, DragDropEffects.All);
            }
            //var f = ()e.Item.Tag;
        }
        private void HukidasiSaveDataList_Changed(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (e.NewStartingIndex == this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.Count - 1)
                    {
                        foreach (HukidasiSaveData v in e.NewItems)
                        {
                            this.Add(v);
                        }                        
                    }
                    else
                    {
                        if(e.NewItems.Count > 1)throw new MyException("");
                        this.Insert(e.NewStartingIndex);
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Remove:
                    this.thumbnailViewBox1.Operator.Delete(e.OldStartingIndex);
                    break;
                case NotifyCollectionChangedAction.Replace:
                    this.Replace(e.OldStartingIndex);
                    break;
                case NotifyCollectionChangedAction.Reset:
                    this.thumbnailViewBox1.Operator.Clear();
                    break;
            }
        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            
        }
        public void LoadFile()
        {
            var dialog = HukidasiCollection.LoadFileDialogBox();
            var b = dialog.ShowDialog();
            if (b == DialogResult.OK)
            {
                var filename = dialog.FileName;
                this.LoadFile(filename);

            }
        }
        private void LoadFile(string filename)
        {
            var f = XElement.Load(filename);
            this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.Clear();
            HukidasiParser.ListParse(f)
                //.ForEach2((v) => v.Options.ZureOption = MergeZureOption.Destructive)
                .ForEach((v) => this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.Add(v));
            MessageBox.Show("読み込みました");
        }
        public void SaveFile()
        {
            var dialog = HukidasiCollection.SaveFileDialogBox();
            var b = dialog.ShowDialog();
            if (b == DialogResult.OK)
            {
                var filename = dialog.FileName;

                HukidasiSaveData.CreateXElement(this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.ToList()).Save(filename);
                MessageBox.Show("保存しました");
                //BinarySerialize.SaveToBinaryFile(this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList, filename);
            }
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.ThumControlSelectedIndex;
            if (n != -1)
            {
                this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.RemoveAt(n);
            }
        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            this.ThumControlSelectedIndex = this.thumbnailViewBox1.Operator.MapOperator.SelectedItemIndex;
            if (this.ThumControlSelectedIndex != -1)
            {
                this.deleteToolStripMenuItem.Enabled = true;
            }
            else
            {
                this.deleteToolStripMenuItem.Enabled = false;
            }
        }

        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.ThumControlSelectedIndex;
            if (n == -1) return;

            var result = this.RegisterForm.ShowDialogReplaceMode(this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList[n]);
            if (result == DialogResult.OK)
            {
                this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList[n] = this.RegisterForm.GetItem();
            }
            this.RegisterForm.Reset();
        }

        private void addingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = this.RegisterForm.ShowDialogAddMode();
            if (result == DialogResult.OK)
            {
                this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.Add((HukidasiSaveData)this.RegisterForm.GetItem().Clone());
            }
            this.RegisterForm.Reset();
        }

        private void BunretuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.ThumControlSelectedIndex;
            if (n != -1)
            {
                this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.Insert(n, (HukidasiSaveData)this.DLLSaveList[n].Clone());
            }
        }

        private void thumbnailViewBox1_ItemSelected(object sender, ThumbnailViewEventArgs e)
        {
            
        }

        private void thumbnailViewBox1_MouseMove(object sender, MouseEventArgs e)
        {
            var n = this.thumbnailViewBox1.Operator.MapOperator.SelectedItemIndex;
            if (n != -1)
            {
                var f = this.DLLSaveList[n];
                //this.DLLFunction.MasterForm.NotifyCaretCharInformation(f.ZureOption + "," + f.Type);

            }
        }

        private void thumbnailViewBox1_ItemDataChanged(object sender, ThumbnailChangeItemEventArgs e)
        {

        }

        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.ThumControlSelectedIndex;
            if (n != -1)
            {
                CopyItem = this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList[n];
                this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.RemoveAt(n);

            }
        }
        HukidasiSaveData CopyItem = null;

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.thumbnailViewBox1.Operator.MapOperator.NearItemIndex;
            if (CopyItem != null && n != -1)
            {
                this.DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.Insert(n + 1, CopyItem);
            }
        }

        private void contextMenuStrip2_Opening(object sender, CancelEventArgs e)
        {
            this.pasteToolStripMenuItem.Enabled = this.CopyItem != null;
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.LoadFile();
        }
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.SaveFile();
        }


        private void initializeFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("フキダシファイルを初期化しますか？",
                "質問",
                MessageBoxButtons.YesNoCancel,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);
            if (result == DialogResult.OK)
            {
                this.LoadFile(SuouBase.SuouEditorConst.GetDataFile("hukidasi.hkd"));
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (this.Visible && this.Items != null && !this.IsReadItem)
            {
                this.SetPicFunc();
                this.IsReadItem = true;
            }
        }
        #endregion


        
    }

}
