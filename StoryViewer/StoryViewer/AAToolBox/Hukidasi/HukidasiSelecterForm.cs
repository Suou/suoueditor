﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;

namespace StoryViewer
{
    public partial class HukidasiSelecterForm : BSystem.Controls.SerializableForm,IHukidasiSelecter
    {
        public HukidasiSelecterForm()
        {
            InitializeComponent();
        }
        public HukidasiSelecterForm(EditorInfo pd):this()
        {
            this.hukidasiToolBox1.IsInstant = true;
            this.hukidasiToolBox1.DLLFunction = pd;
            this.hukidasiToolBox1.Items = pd.AltHukidasiPlugin.HukidasiSaveDataList;

        }
        public ThumbnailOrder OrderMode
        {
            get
            {
                return this.hukidasiToolBox1.Mode;
            }
            set
            {
                this.hukidasiToolBox1.Mode = value;
            }
        }
        public HukidasiSaveData SelectedSaveData
        {
            get
            {
                return this.hukidasiToolBox1.SelectedHukdiasi;
                
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public new HukidasiSelecterFormSetting Setting
        {
            get
            {
                var data = base.Setting;
                var p = new HukidasiSelecterFormSetting(this, data);
                
                return p;
            }
            set
            {
                if (value == null) return;
                this.hukidasiToolBox1.Setting = value.CreaterFormSetting;
                base.Setting = value;
            }
        }

        private void HukidasiSelecterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
                this.Visible = false;
            }
        }

        private void hukidasiToolBox1_HukidasiSelected(object sender, SuouBase.HukidasiEventArgs e)
        {
            //this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            //this.Visible = false;
        }
    }
    
    [Serializable()]
    public class HukidasiSelecterFormSetting : BSystem.Controls.FormSaveData
    {
        public BSystem.Controls.FormSaveData CreaterFormSetting { get; set; }
        public HukidasiSelecterFormSetting():base()
        {
        }
        public HukidasiSelecterFormSetting(HukidasiSelecterForm f, BSystem.Controls.FormSaveData data)
            : base(f)
        {
            this.CreaterFormSetting = data;
        }
    }
    
}
