﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using SuouBase.Interface;
namespace StoryViewer
{
    public partial class HukidasiRegisterForm : Form
    {
        public HukidasiRegisterForm()
        {
            InitializeComponent();
        }
        public HukidasiRegisterForm(EditorInfo pd):this()
        {
            this.hukidasiSetting1.DLLFunction = pd;
        }
        public HukidasiSaveData SaveData
        {
            get { return this.hukidasiSetting1.Item; }
            set { this.hukidasiSetting1.Item = value; }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            var b = this.hukidasiSetting1.Item.ErrorCheck();
            if (b)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                MessageBox.Show("噴出しの設定に問題があります");
            }
            
        }

       

    }

}
