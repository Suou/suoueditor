﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Controls.Hukidasi;
namespace StoryViewer.AAToolBox.Hukidasi
{
    [ToolboxItem(false)]
    public partial class OptionBox : UserControl,IWakuOptionParent
    {
        public OptionBox()
        {
            InitializeComponent();
        }
        public NumericUpDown[] ValueBoxes
        {
            get { return new NumericUpDown[] { this.UpCenterBottomValueBox, this.CenterValueBox }; }
        }
        public string WakuName
        {
            get { return this.textBox1.Text; }
            set { this.textBox1.Text = value; }
        }
        public int UpCenterBottomValue
        {
            get { return (int)this.UpCenterBottomValueBox.Value; }
            set { this.UpCenterBottomValueBox.Value = value; }
        }
        public int CenterZureLength
        {
            get { return (int)this.CenterValueBox.Value; }
            set { this.CenterValueBox.Value = value; }
        }

        [ReadOnly(true)]
        public WakuOption Item
        {
            get
            {
                var v = new WakuOption()
                {
                    Name = this.WakuName,
                    UpCenterBottomPreZure = this.UpCenterBottomValue,
                    //PermitZureLength = 0,
                    CenterPreZure = this.CenterZureLength,
                    Type = HukidasiType.FiveBlock,
                    IsTrimSpace = true,
                    MergeAlgorithm = this.spacerComboBox1.SelectedSpaceSet,
                    //ZureOption = MergeZureOption.Destructive

                };
                return v;
            }
            set
            {
                if (value != null)
                {
                    this.Name = value.Name;
                    this.UpCenterBottomValue = value.UpCenterBottomPreZure;
                    this.CenterZureLength = value.CenterPreZure;
                    this.spacerComboBox1.SelectedSpaceSet = value.MergeAlgorithm;
                }
            }
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public SpaceLineSet SelectedSpaceSet
        {
            get
            {
                return this.spacerComboBox1.SelectedSpaceSet;
            }
            set
            {
                this.spacerComboBox1.SelectedSpaceSet = value;
            }
        }
        
    }
}
