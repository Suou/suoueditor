﻿namespace StoryViewer.AAToolBox.Hukidasi
{
    partial class HukidasiCreateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.fiveBlockWakuInputBox1 = new StoryViewer.AAToolBox.Hukidasi.FiveBlockWakuInputBox();
            this.SuspendLayout();
            // 
            // fiveBlockWakuInputBox1
            // 
            this.fiveBlockWakuInputBox1.IsReplaceMode = false;
            this.fiveBlockWakuInputBox1.Location = new System.Drawing.Point(0, -1);
            this.fiveBlockWakuInputBox1.Name = "fiveBlockWakuInputBox1";
            this.fiveBlockWakuInputBox1.Size = new System.Drawing.Size(839, 705);
            this.fiveBlockWakuInputBox1.TabIndex = 0;
            this.fiveBlockWakuInputBox1.Registed += new System.EventHandler(this.fiveBlockWakuInputBox1_Registed);
            // 
            // HukidasiCreateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(862, 529);
            this.Controls.Add(this.fiveBlockWakuInputBox1);
            this.Name = "HukidasiCreateForm";
            this.Text = "フキダシ作成フォーム";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.HukidasiCreateForm_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private FiveBlockWakuInputBox fiveBlockWakuInputBox1;
    }
}