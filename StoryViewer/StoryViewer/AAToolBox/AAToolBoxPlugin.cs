﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using System.Windows.Forms;
using System.Xml.Serialization;
using System.Drawing;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;
using SuouBase.Interface;


namespace StoryViewer
{
    [IComponentPlugin("AAToolBox", PluginType.Control)]
    [Serializable]
    public class AAToolBoxPlugin:IComponentPlugin
    {
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public string Name
        {
            get
            {
                return "AAツール";
            }
        }
        public AAToolBoxPlugin()
        {
            _Control = new AAToolBoxControl();
        }

        public SettingPage[] SettingPage { get { return null; } }
        public AAToolBoxPlugin(EditorInfo pd)
        {
            _Control = new AAToolBoxControl(pd);
            DLLFunctinon = pd;
            //this._Control.mojiToolBox1.DLLFucntion = pd;

            FileInfo fi = new FileInfo(SaveFile);
            if (fi.Exists)
            {
                try
                {
                    var f = BSystem.XML.BinarySerialize.LoadFromBinaryFile<AAToolBoxPlugin>(SaveFile);
                    
                    //this._Control.hukidasiToolBox1.Load();
                }
                catch
                {
                }
                finally
                {
                }
            }
            //this._Control.mojiToolBox1.Load();
        }
        [NonSerialized]
        private EditorInfo DLLFunctinon = null;

        [NonSerialized]
        public AAToolBoxControl _Control = null;
        [NonSerialized]
        private string SaveFile = SuouEditorConst.SettingDirectory + "/" + "AAToolBox.stn";
        public ContainerControl Control
        {
            get { return this._Control; }
        }
        public Form Form { get { return null; } }

        public bool DisposeCheck()
        {
            BSystem.XML.BinarySerialize.SaveToBinaryFile(this, SaveFile);
            //this._Control.mojiToolBox1.Save();
            return true;
        }
    }
}
