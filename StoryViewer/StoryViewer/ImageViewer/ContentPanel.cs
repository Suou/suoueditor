﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoryViewer.ImageViewer
{
    public partial class ContentPanel : Panel
    {
        public ContentPanel()
        {
            InitializeComponent();
        }

        private void ContentPanel_Enter(object sender, EventArgs e)
        {
        }

        private void ContentPanel_Leave(object sender, EventArgs e)
        {
        }

        private void ContentPanel_Click(object sender, EventArgs e)
        {
            this.Focus();
        }
    }
}
