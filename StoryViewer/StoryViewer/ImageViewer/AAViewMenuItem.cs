﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using BSystem;
using BSystem.Collections;
using BSystem.Collections.Specific;

namespace StoryViewer.ImageViewer
{
    public class AAViewMenuItem : ToolStripMenuItem
    {
        public AAViewMenuItem():base()
        {
            this.Text = "AA表示";
            this.SizeMenuItem = new ToolStripMenuItem() { Text = "サイズ" };
            this.s100MenuItem = new ToolStripMenuItem() { Text = "s100" };
            this.s150MenuItem = new ToolStripMenuItem() { Text = "s150" };
            this.s200MenuItem = new ToolStripMenuItem() { Text = "s200" };
            this.s100MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.Size, new Size(100, 100), null); };
            this.s150MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.Size, new Size(150, 150), null); };
            this.s200MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.Size, new Size(200, 200), null); };
            this.SizeMenuItem.DropDownItems.Add(this.s100MenuItem);
            this.SizeMenuItem.DropDownItems.Add(this.s150MenuItem);
            this.SizeMenuItem.DropDownItems.Add(this.s200MenuItem);
            this.PercentMenuItem = new ToolStripMenuItem() { Text = "倍率" };
            this.p50MenuItem = new ToolStripMenuItem() { Text = "p50" };
            this.p75MenuItem = new ToolStripMenuItem() { Text = "p75" };
            this.p100MenuItem = new ToolStripMenuItem() { Text = "p100" };
            this.p50MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.Percent, null, 0.5f); };
            this.p75MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.Percent, null, 0.75f); };
            this.p100MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.Percent, null, 1.00f); };
            this.PercentMenuItem.DropDownItems.Add(this.p50MenuItem);
            this.PercentMenuItem.DropDownItems.Add(this.p75MenuItem);
            this.PercentMenuItem.DropDownItems.Add(this.p100MenuItem);

            this.HeightPercentMenuItem = new ToolStripMenuItem() { Text = "倍率A" };
            this.p75s500MenuItem = new ToolStripMenuItem() { Text = "p75s500" };
            this.p50s500MenuItem = new ToolStripMenuItem() { Text = "p50s500" };
            this.p50s350MenuItem = new ToolStripMenuItem() { Text = "p50s350" };
            this.p75s500MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer, new Size(500, 500), 0.75f); };
            this.p50s500MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer, new Size(500, 500), 0.50f); };
            this.p50s350MenuItem.Click += (sender, e) => { this.SetMode(SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer, new Size(350, 350), 0.50f); };
            this.HeightPercentMenuItem.DropDownItems.Add(this.p50s500MenuItem);
            this.HeightPercentMenuItem.DropDownItems.Add(this.p75s500MenuItem);
            this.HeightPercentMenuItem.DropDownItems.Add(this.p50s350MenuItem);
            InitializeComponent();
            
        }
        private ImageControlOption _Option = null;
        public ImageControlOption Option
        {
            get
            {
                return this._Option;
            }
            set
            {
                this._Option = value;
                if(this.Option != null)this.SetControl();
            }

        }
        private ToolStripMenuItem s100MenuItem { get; set; }
        private ToolStripMenuItem s150MenuItem { get; set; }
        private ToolStripMenuItem s200MenuItem { get; set; }
        private ToolStripMenuItem p50MenuItem { get; set; }
        private ToolStripMenuItem p75MenuItem { get; set; }
        private ToolStripMenuItem p100MenuItem { get; set; }
        private ToolStripMenuItem SizeMenuItem { get; set; }
        private ToolStripMenuItem PercentMenuItem { get; set; }
        private ToolStripMenuItem p75s500MenuItem { get; set; }
        private ToolStripMenuItem p50s500MenuItem { get; set; }
        private ToolStripMenuItem p50s350MenuItem { get; set; }
        private ToolStripMenuItem HeightPercentMenuItem { get; set; }

        private void InitializeComponent()
        {
            // 
            // AAViewMenuItem
            // 
            this.DropDownOpening += new System.EventHandler(this.AAViewMenuItem_DropDownOpening);

        }
        private void SetMode(SuouBase.Drawing.ThumbnailMode mode, Size? size, float? percent)
        {
            if (mode == SuouBase.Drawing.ThumbnailMode.Raw)
            {
            }
            else if (mode == SuouBase.Drawing.ThumbnailMode.Size)
            {
                this.Option.ChangeSizeMode(size.Value);
            }
            else if (mode == SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer)
            {
                this.Option.Size = size.Value;
                this.Option.Percent = percent.Value;
            }
            else if (mode == SuouBase.Drawing.ThumbnailMode.Percent)
            {
                this.Option.Percent = percent.Value;
                this.Option.Mode = SuouBase.Drawing.ThumbnailMode.Percent;
            }
        }
        private void SetControl()
        {
            this.DropDownItems.Clear();
            if (this.Option.Mode == SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer)
            {
                this.DropDownItems.Add(this.HeightPercentMenuItem);


                this.p50s350MenuItem.Checked = false;
                this.p50s500MenuItem.Checked = false;
                this.p75s500MenuItem.Checked = false;

                if (this.Option.Size.Width == 350 && this.Option.Percent == 0.50f)
                {
                    this.p50s350MenuItem.Checked = true;
                }
                else if (this.Option.Size.Width == 500 && this.Option.Percent == 0.50f)
                {
                    this.p50s500MenuItem.Checked = true;
                }
                else if (this.Option.Size.Width == 500 && this.Option.Percent == 0.75f)
                {
                    this.p75s500MenuItem.Checked = true;
                }
            }
            else
            {
                this.s100MenuItem.Checked = false;
                this.s150MenuItem.Checked = false;
                this.s200MenuItem.Checked = false;
                this.p100MenuItem.Checked = false;
                this.p50MenuItem.Checked = false;
                this.p75MenuItem.Checked = false;

                this.SizeMenuItem.Checked = false;
                this.PercentMenuItem.Checked = false;
                if (this.Option.Mode == SuouBase.Drawing.ThumbnailMode.Size)
                {
                    this.SizeMenuItem.Checked = true;
                    if (this.Option.Size == new Size(100, 100))
                    {
                        this.s100MenuItem.Checked = true;
                    }
                    else if (this.Option.Size == new Size(150, 150))
                    {
                        this.s150MenuItem.Checked = true;
                    }
                    else if (this.Option.Size == new Size(200, 200))
                    {
                        this.s200MenuItem.Checked = true;
                    }
                }
                else if (this.Option.Mode == SuouBase.Drawing.ThumbnailMode.Percent)
                {
                    this.PercentMenuItem.Checked = true;
                    if (this.Option.Percent == 0.5f)
                    {
                        this.p50MenuItem.Checked = true;
                    }
                    else if (this.Option.Percent == 0.75f)
                    {
                        this.p75MenuItem.Checked = true;
                    }
                    else if (this.Option.Percent == 1.00f)
                    {
                        this.p100MenuItem.Checked = true;
                    }
                }
                else
                {
                    this.p100MenuItem.Checked = true;
                }

                this.DropDownItems.Add(this.SizeMenuItem);
                this.DropDownItems.Add(this.PercentMenuItem);
            }
        }

        private void AAViewMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            this.SetControl();
        }
    }
}
