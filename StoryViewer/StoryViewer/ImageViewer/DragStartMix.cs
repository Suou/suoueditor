﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using SuouBase.Controls;
using StoryViewer.ThumbnailViewer;
using BSystem.Collections;
using BSystem.Controls;
namespace StoryViewer.ImageViewer
{
    public class DragStartMix:DragMapRectangle
    {
        public new event EventHandler DragStarted;
        public DragStartMix(Control ctrl):base(ctrl)
        {

        }
        public Func<object> DragItemFunc { get; set; }
        private bool IsDragStart { get; set; }
        protected override void ContentPanel_MouseMove(object sender, MouseEventArgs e)
        {
            base.ContentPanel_MouseMove(sender, e);
            if (this.IsDragging && !this.IsDragStart && this.DistanceBetweenStartAndEnd > 3)
            {
                this.IsDragStart = true;

                if (this.DragItemFunc != null)
                {
                    //ドラッグ&ドロップ処理を開始する
                    DragDropEffects dde =
                        this.Ctrl.DoDragDrop(this.DragItemFunc(), DragDropEffects.All);
                }
                if (DragStarted != null) this.DragStarted(sender, new EventArgs());
            }
        }
        public override bool IsDragging
        {
            get
            {
                return base.IsDragging;
            }
            protected set
            {
                if (base.IsDragging && !value)
                {
                    this.IsDragStart = false;
                }
                base.IsDragging = value;
            }
        }
    }
}
