﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoryViewer.ImageViewer
{
    public partial class PictureControl : ImageControl
    {
        public PictureControl()
        {
            InitializeComponent();
            this.CreateFunction();
        }
        public PictureControl(IIMageControlTag drawTag,ParentControlInfo info,ImageControlOption option):base(drawTag,option)
        {
            InitializeComponent(); 
            this.WindowInfo = info;
            this.DragMixer = new DragStartMix(this.pictureBox1);
            this.DragMixer.DragStarted += (obj, e) => this.DragStart();
            this.CreateFunction();
            
        }
        private DragStartMix DragMixer { get; set; }
        /*
        public bool IsLoadPicture
        {
            get
            {
                return this.NormalImage != null;
            }
        }
        */
        protected Bitmap NormalImage { get; set; }
        
        protected override void ChangeReverseImage()
        {
            //if (!this.IsLoadPicture) this.LoadPicture();
            if (!this.NeedLoadImage)
            {
                this.pictureBox1.Image = BSystem.Drawing.MainDrawingMethod.Reverse(this.NormalImage);
            }
            base.ChangeReverseImage();
        }
        protected override void ChangeNormalImage()
        {
            //if (!this.IsLoadPicture) this.LoadPicture();
            if (!this.NeedLoadImage)
            {
                this.pictureBox1.Image = this.NormalImage;
            }
            base.ChangeNormalImage();
        }

        public void SyncLoadingImage()
        {
            var sz = this.pictureBox1.Size;
            this.NormalImage = this.DrawItem.Create(sz);
            this.NeedLoadImage = false;
        }

        public override void LoadingPicture()
        {
            if (this.DesignMode) return;
            this.LoadPicture();
        }
        protected override void ChangeDragImage()
        {
            //if (!this.IsLoadPicture) this.LoadPicture();
            if (!this.NeedLoadImage)
            {
                this.pictureBox1.Image = BSystem.Drawing.MainDrawingMethod.Reverse(this.NormalImage);
                //this.pictureBox1.Image = BSystem.Drawing.MainDrawingMethod.Dye(this.NormalImage,Color.White,Color.Green);
            }
            base.ChangeDragImage();
        }

        protected virtual void CreateFunction()
        {
            Action<Bitmap> action1 = (img) =>
            {
                if (this.pictureBox1.Size == img.Size)
                {
                    this.pictureBox1.Image = img;
                    this.NormalImage = img;
                    this.NeedLoadImage = false;
                    this.ImageCheck();
                }
            };
            
            this.SetImageAction = action1;

            Action<Bitmap> action2 = (img) =>
            {
                if (this.IsHandleCreated && !this.IsDisposed)
                {
                    
                    this.EndInvoke(this.BeginInvoke(this.SetImageAction, img));
                }
            }; 
            Func<Bitmap> func1 = () =>
            {
                lock (this.DrawItem)
                {
                    var sz = this.pictureBox1.Size;
                    return this.DrawItem.Create(sz);
                }
            };
            this.CompleteRunSetImageAction = action2;
            this.CreateBitmapFunc = func1;
        }
        
        protected Action<Bitmap> SetImageAction { get; set; }
        protected Action<Bitmap> CompleteRunSetImageAction { get; set; }
        protected Func<Bitmap> CreateBitmapFunc { get; set; }
        protected virtual void LoadPicture()
        {
            if (!this.IsHandleCreated || !this.WindowInfo.ContainControl(this)) return;
            if (this.DrawItem.IsNeedPreAsyncProcess) this.DrawItem.PreAsyncProcess();

            this.DrawItem.DeterminImageData();
            BSystem.Async<Bitmap>.Run(this.CreateBitmapFunc, this.CompleteRunSetImageAction);
        }
        public override void ClearImage()
        {
            if (!this.NeedLoadImage)
            {
                this.pictureBox1.Image = null;
                this.NormalImage.Dispose();
                this.NormalImage = null;
                this.NeedLoadImage = true;
            }
        }
        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            //this.IsSelected = true;
            //this.OnMouseMove(e);
        }

        public override bool CanCreateImage
        {
            get
            {
                return this.IsInner && this.NeedLoadImage && this.IsHandleCreated;
            }
        }

        private void PictureControl_IsInnerChanged(object sender, EventArgs e)
        {
            if (this.IsInner && this.NeedLoadImage)
            {                
                this.LoadPicture();
            }
        }

        private void pictureBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                this.OnItemLeftClick();
                if (!this.NeedLoadImage && this.ToolTip != null)
                {
                    this.ToolTip.DrawItem = this.DrawItem;
                    this.ToolTip.PopUpSize = this.DrawItem.OriginalSize;
                    var p = this.WindowInfo.ClientLocation(this);
                    this.ToolTip.ToolTipShow(new Point(p.X + e.X, p.Y + e.Y));
                }
            }
        }

        private void PictureControl_ContextMenuStripChanged(object sender, EventArgs e)
        {
            this.pictureBox1.ContextMenuStrip = this.ContextMenuStrip;
        }
        protected override void ImageControl_MouseEnter(object sender, EventArgs e)
        {
        }
        
        protected override void ImageControl_MouseLeave(object sender, EventArgs e)
        {
        }
        
        private void pictureBox1_SizeChanged(object sender, EventArgs e)
        {
            this.NeedLoadImage = true;
            //this.NormalImage = null;
            //this.pictureBox1.Image = null;
        }

        private void pictureBox1_MouseEnter(object sender, EventArgs e)
        {
            this.IsMouseOn = true;
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            var mouseLocation = this.PointToClient(Control.MousePosition);
            if (!this.DisplayRectangle.Contains(mouseLocation))
            {
                this.IsMouseOn = false;
            }
            
        }


    }
}
