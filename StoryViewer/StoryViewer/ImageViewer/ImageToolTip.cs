﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase.Drawing;
using SuouBase.Text;
using SuouBase;
using SuouBase.Plugin;
using SuouBase.Interface;
namespace StoryViewer.ImageViewer
{
    public class ImageToolTip : ToolTip
    {
        //private IContainer components;

        Point StartPoint { get; set; }
        [ReadOnly(true)]
        [Browsable(false)]
        public IIMageControlTag DrawItem { get; set; }
        public bool IsSensitivePopup { get; set; }
        public Size PopUpSize { get; set; }

        public Timer TimerWatch = null;
        private Control _ParentControl = null;
        public Control ParentControl
        {
            get { return this._ParentControl; }
            set
            {
                if (value != null)
                {
                    this._ParentControl = value;
                    this.ParentControl.MouseMove += this.ParentControl_MouseMove;
                }
            }
        }
        //public Stopwatch ToolTipOpeningTime = new Stopwatch();
        public ImageToolTip()
            : base()
        {
            this.InitializeComponent();
            this.StartPoint = new Point(-1, -1);
        }

        #region Method
        private int GetDistance(Point a,Point b)
        {
            var p = new Point(a.X - b.X, a.Y - b.Y);
            return (int)Math.Sqrt(p.X * p.X + p.Y * p.Y);
        }
        private void InitializeComponent()
        {
            // 
            // AAToolTip
            // 
            this.AutoPopDelay = 8000;
            this.InitialDelay = 100;
            this.OwnerDraw = true;
            this.ReshowDelay = 100;
            this.Draw += new System.Windows.Forms.DrawToolTipEventHandler(this.AAToolTip_Draw);
            this.Popup += new System.Windows.Forms.PopupEventHandler(this.AAToolTip_Popup);

        }
        
        private Timer CreateTimer()
        {
            var r = new Timer() { Enabled = true,Interval = this.AutoPopDelay};
            r.Tick += this.timer1_Tick;
            return r;
        }
        public void ToolTipShow(Point p)
        {
            this.StartPoint = p;
            this.InitializeTimer();
            this.TimerWatch = this.CreateTimer();
            this.TimerWatch.Start();

            //ToolTipOpeningTime.Start();
            this.Show(" ", this.ParentControl, new Point(p.X, p.Y));
        }
        public void ToolTipShow(Point p,BSystem.RectCornerDirection d)
        {
            var r = BSystem.IntRectangle.GetTopLeftCorner(BSystem.PointConverter.ToBasicPoint(p),BSystem.PointConverter.ToBasicPoint(this.PopUpSize), d);
            this.ToolTipShow(BSystem.PointConverter.ToDesktopPoint(r));
        }
        public void ToolTipShow()
        {
            this.ToolTipShow(BSystem.RectCornerDirection.TopLeftCorner);
        }
        public void ToolTipShow(BSystem.RectCornerDirection d)
        {
            if (this.ParentControl == null) return;
            var p = this.ParentControl.PointToClient(Control.MousePosition);
            
            var p2 = BSystem.PointConverter.ToBasicPoint(p);
            var sz = new BSystem.BSize(this.PopUpSize.Width, this.PopUpSize.Height);
            var p3 = BSystem.IntRectangle.GetTopLeftCorner(p2, sz, d);
            var p4 = BSystem.PointConverter.ToDesktopPoint(p3);
            this.StartPoint = p;
            this.ToolTipShow(p4);
        }
        private void InitializeTimer()
        {
            this.Tag = null;
            if (this.TimerWatch != null)
            {
                this.TimerWatch.Stop();
                this.TimerWatch.Tick -= this.timer1_Tick;
                this.TimerWatch = null;
            }
            
        }
        public void ToolTipHide()
        {
            if (this.ParentControl == null || this.TimerWatch == null) return;
            //throw new Exception();
            if (this.Active)
            {
                this.StartPoint = new Point(-1, -1);
                this.Hide(this.ParentControl);
            }
            this.InitializeTimer();
            //ToolTipOpeningTime.Reset();
        }
        #endregion
        private bool IsNotShow
        {
            get { return this.StartPoint.X == -1 && this.StartPoint.Y == -1; }
        }
        #region EventMethod
        private void ParentControl_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.IsNotShow) return;
            if (this.GetDistance(this.StartPoint, e.Location) > 10 && this.IsSensitivePopup)
            {
                this.ToolTipHide();
            }
        }
        private void AAToolTip_Draw(object sender, DrawToolTipEventArgs e)
        {

            if (this.DrawItem != null)
            {
                var bmp = this.DrawItem.Create(this.PopUpSize);
                e.Graphics.DrawImage(bmp, Point.Empty);
            }
        }

        private void AAToolTip_Popup(object sender, PopupEventArgs e)
        {
            if (this.DrawItem != null)
            {
                e.ToolTipSize = this.PopUpSize;
                //e.ToolTipSize = this.TextItem.Decode().Text.GetSize(SuouBase.SuouEditorConst.AAFontYInterval);
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.ToolTipHide();
        }
        #endregion
        protected override void Dispose(bool disposing)
        {
            if(this.ParentControl != null)this.ParentControl.MouseMove -= this.ParentControl_MouseMove;
            this.Draw -= new System.Windows.Forms.DrawToolTipEventHandler(this.AAToolTip_Draw);
            this.Popup -= new System.Windows.Forms.PopupEventHandler(this.AAToolTip_Popup);
            this.DrawItem = null;
            this.ParentControl = null;
            base.Dispose(disposing);
        }
    }
}
