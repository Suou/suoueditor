﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
namespace StoryViewer.ImageViewer
{
    public class MapItems<T> : SuperObservableCollection<T> where T : IRectObject
    {
        public MapItems()
        {
        }

        public T GetNearItem(T item)
        {
            return this.Items.Min2((v) => (int)RectangleArrange.GetDistance(item, v));
        }
        public int GetNearIndex(Point p)
        {
            if (this.Items.Count == 0)
            {
                return -1;
            }
            else
            {
                return this.Items.MinValueIndex((v) => (int)RectangleArrange.GetDistance(v, p));
            }
        }
        
    }
}
