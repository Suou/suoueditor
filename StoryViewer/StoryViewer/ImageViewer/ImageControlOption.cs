﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
namespace StoryViewer.ImageViewer
{

    [TypeConverter(typeof(ExpandableObjectConverter))]
    [Serializable]
    public class ImageControlOption
    {
        public ImageControlOption()
        {
            this._Size = new Size(100, 100);
            this._Mode = SuouBase.Drawing.ThumbnailMode.Size;
            this._Percent = 0.5f;
        }
        public ImageControlOption(SuouBase.Drawing.ThumbnailMode mode, float percent, Size size)
        {
            this._Size = size;
            this._Percent = percent;
            this.Mode = mode;
        }
        private SuouBase.Drawing.ThumbnailMode _Mode;
        [DefaultValue(SuouBase.Drawing.ThumbnailMode.Size)]
        public SuouBase.Drawing.ThumbnailMode Mode
        {
            get { return this._Mode; }
            set
            {
                this._Mode = value;
                if (this.OptionChanged != null) this.OptionChanged(this, new EventArgs());
            }
        }
        private Size _Size;
        public Size Size
        {
            get { return this._Size; }
            set
            {
                this._Size = value;
                //this._Mode = SuouBase.Drawing.ThumbnailMode.Size;
                if (this.OptionChanged != null) this.OptionChanged(this, new EventArgs());
            }
        }
        public void ChangeSizeMode(Size size)
        {

            this._Size = size;
            this._Mode = SuouBase.Drawing.ThumbnailMode.Size;
            if (this.OptionChanged != null) this.OptionChanged(this, new EventArgs());
        }
        private float _Percent;
        [DefaultValue(0.5f)]
        public float Percent
        {
            get { return this._Percent; }
            set
            {
                if (this._Percent != value)
                {
                    this._Percent = value;
                    //this._Mode = SuouBase.Drawing.ThumbnailMode.Percent;
                    if (this.OptionChanged != null) this.OptionChanged(this, new EventArgs());
                }
            }
        }
        [field: NonSerialized]
        public event EventHandler OptionChanged;
        public Size GetSize(Size originalSize)
        {
            if (this.Mode == SuouBase.Drawing.ThumbnailMode.Percent)
            {
                return new Size((int)(originalSize.Width * this.Percent), (int)(originalSize.Height * this.Percent));
            }
            else if (this.Mode == SuouBase.Drawing.ThumbnailMode.Size)
            {
                return this.Size;
            }
            else
            {
                return originalSize;
            }
        }
    }
}
