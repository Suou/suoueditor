﻿namespace StoryViewer.ImageViewer
{
    partial class ImageControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // ImageControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Name = "ImageControl";
            this.Size = new System.Drawing.Size(277, 152);
            this.SizeChanged += new System.EventHandler(this.ImageControl_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.ImageControl_VisibleChanged);
            this.GiveFeedback += new System.Windows.Forms.GiveFeedbackEventHandler(this.ImageControl_GiveFeedback);
            this.QueryContinueDrag += new System.Windows.Forms.QueryContinueDragEventHandler(this.ImageControl_QueryContinueDrag);
            this.MouseEnter += new System.EventHandler(this.ImageControl_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.ImageControl_MouseLeave);
            this.ParentChanged += new System.EventHandler(this.ImageControl_ParentChanged);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
