﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Book;
namespace StoryViewer.ImageViewer
{
    public partial class AstPagePicture : PictureControl
    {
        public AstPagePicture()
        {
            InitializeComponent();
        }
        
        public AstPagePicture(IIMageControlTag drawTag,ParentControlInfo info,ImageControlOption option)
            : base(drawTag,info,option)
        {
            InitializeComponent();
            var tag = (ImageAATag)drawTag;
            tag.Page.EditStateChanged += this.Page_Changed;
        }
        public bool IsChanged { get; set; }
        protected virtual void Page_Changed(object sender, SuouBase.BookPageContentChangeEventArgs e)
        {
            if (e.Type == SuouBase.Text.PageContentChangeType.Text)
            {
                this.NeedLoadImage = true;
                this.SetControlSize();
            }
            else if (e.Type == SuouBase.Text.PageContentChangeType.TextPart)
            {
                this.NeedLoadImage = true;
                //this.SetControlSize();
            }
            else if (e.Type == SuouBase.Text.PageContentChangeType.TextPartsCount)
            {
                this.NeedLoadImage = true;
                this.SetControlSize();
            }
            
        }
        public override void SetControlSize()
        {
            base.SetControlSize();
        }
        public new ImageAATag DrawItem
        {
            get
            {
                return (ImageAATag)base.DrawItem;
            }
        }
        public AstPage Page
        {
            get
            {
                return (AstPage)this.DrawItem.Page;
            }
        }
        public override Size OriginalSize
        {
            get
            {
                return this.DrawItem.OriginalSize;
            }
        }
        private void MyDispose()
        {
            var tag = (ImageAATag)this.DrawItem;
            tag.Page.EditStateChanged -= this.Page_Changed;
        }
    }
}
