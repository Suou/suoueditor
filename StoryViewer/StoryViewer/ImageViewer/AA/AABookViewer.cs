﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Book;
using SuouBase.Text;
using SuouBase.Interface;
using BSystem.Collections;

namespace StoryViewer.ImageViewer.AA
{
    public partial class AABookViewer : ImageViewer
    {
        public AABookViewer():base()
        {
            InitializeComponent();
            
            this.mainContextMenuStrip.AllowMerge = true;
            this.contextMenuStrip1.AllowMerge = true;
            
            ToolStripManager.Merge(this.contextMenuStrip1, this.mainContextMenuStrip);
            
        }

        
        protected override void SetEvent(ImageControl c)
        {
            c.ItemLeftClicked += this.Item_LeftClicked;
            base.SetEvent(c);
        }
        protected override void RemoveEvent(ImageControl c)
        {
            c.ItemLeftClicked -= this.Item_LeftClicked;
            base.RemoveEvent(c);
        }

        private void Item_LeftClicked(object sender, EventArgs e)
        {
            if (sender is AstPagePicture)
            {
                var item = (AstPagePicture)sender;
                if (this.EditorSmallInfo != null && this.CanEditPage)
                {
                    this.EditorSmallInfo.Edit(item.Page, this.Replace);
                }
            }
        }
        public bool CanEditPage { get; set; }
        
        [ReadOnly(true)]
        [Browsable(false)]
        public SmallInfo EditorSmallInfo { get; set; }
        protected IBook _Book = null;
        private IEnumerable<IBookPage> SelectedPages
        {
            get
            {
                if (this.SelectedItems.Count() == 1 && this.SelectedItems.First() == null)
                {
                    return null;
                }
                else
                {
                    return this.SelectedItems.Select((v) => (ImageAATag)v.DrawItem).Select((v) => v.Page);
                }
            }
        }
        protected SuouEditorDataClass BookData { get; set; }
        
        public IBook Book
        {
            get { return (IBook)this._Book; }
            set
            {
                if (this._Book == value) return;
                if (this._Book != null && this._Book != value)
                {
                    if (!this.Close())
                    {
                        throw new Exception();
                    }

                }
                if (value == null) return;
                if (value.Owner != null) throw new Exception();
                this._Book = value;
                this._Book.Owner = this;



                var pics = this.Create(value.BookPages);
                this.Set(pics);

                this.BookData = new SuouEditorDataClass(this._Book);
            }
        }
        public bool IsNotWarningClose { get; set; }

        public bool Close()
        {
            if (this._Book == null) return true;
            if (this.IsNotWarningClose)
            {
                this.Book.Owner = null;
                this._Book = null;
                this.BookData = null;
                this.Items.Clear();
                return true;
            }
            else
            {
                if (this.BookData == null)
                {
                    this.Items.Clear();
                    return true;
                }

                if (this.BookData.Close())
                {
                    this.Book.Owner = null;
                    this.BookData = null;
                    this._Book = null;
                    this.Items.Clear();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            
        }
        
        private void CopySelectedItem()
        {
            if (this.SelectedPages != null && this.EditorSmallInfo != null)
            {
                this.EditorSmallInfo.Copy(this.SelectedPages, EncodeType.UTF8);
            }
            
        }

        protected override void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CopySelectedItem();
            this.DragRectangle.Clear();
        }
        protected override void copySJIS10ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CopySelectedItem();
            this.DragRectangle.Clear();
        }

        protected bool IsBookEditing { get; set; }
        protected bool IsControlItemsEditing { get; set; }
        private ImageControl GetImageControl(IBookPage page)
        {
            var p = this.Items.Select((v) => v.DrawItem).Cast<ImageAATag>().FindIndex((v) => v.Page == page);
            return this.Items[p];
        }

        protected virtual void Book_ContentChanged(object sender, BSystem.Collections.SuperObservableCollectionEventArgs<IBookPage> e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (this.IsBookEditing) return;
            this.IsControlItemsEditing = true;
            
            switch (e.Action)
            {
                case SuperObservableCollectionAction.Add:
                    this.Add(e.AddedItems.ToArray());
                    break;
                case SuperObservableCollectionAction.Clear:
                    throw new NotSupportedException();
                    //break;
                case SuperObservableCollectionAction.Insert:
                    throw new NotSupportedException();
                   //break;
                case SuperObservableCollectionAction.Move:
                    throw new NotSupportedException();
                    //break;
                case SuperObservableCollectionAction.Remove:
                    var gp = e.RemovedItems.Select((v) => this.GetImageControl(v)).ToArray();
                    this.Items.RemoveRange(gp);
                    break;
                case SuperObservableCollectionAction.Replace:
                    throw new NotSupportedException();
                    //break;


            }
            this.IsControlItemsEditing = false;
        }
        protected override void Items_CollectionChanged(object sender, BSystem.Collections.SuperObservableCollectionEventArgs<ImageControl> e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            base.Items_CollectionChanged(sender, e);
            this.ReNumber();
            if (this.BookData == null) return;
            if (this.IsControlItemsEditing) return;

            var b1 = e.AddedItems != null;
            var b2 = e.RemovedItems != null;

            var addedItems = b1 ? e.AddedItems.Select((v) => v.DrawItem).Cast<ImageAATag>().Select((v)=>v.Page).ToList() : null;
            var removedItems = b2 ? e.RemovedItems.Select((v) => v.DrawItem).Cast<ImageAATag>().Select((v) => v.Page).ToList() : null;
            int t = 0;

            this.IsBookEditing = true;
            switch (e.Action)
            {
                case BSystem.Collections.SuperObservableCollectionAction.Add:
                    
                    foreach (var v in addedItems)
                    {                        
                        this.Book.Add(v);
                        if (v.GetHashCode() != this.Book.BookPages.Last().GetHashCode()) throw new Exception();
                    }
                    break;
                case BSystem.Collections.SuperObservableCollectionAction.Clear:
                    this.Book.Clear();
                    break;
                case BSystem.Collections.SuperObservableCollectionAction.Insert:
                    foreach (var v in addedItems)
                    {
                        this.BookData.StoryFile.Insert(e.ChangedIndex + (t++), v);
                        if (v.GetHashCode() != this.BookData.StoryFile[e.ChangedIndex + t - 1].GetHashCode()) throw new Exception();
                    }
                    break;
                case BSystem.Collections.SuperObservableCollectionAction.Move:
                    this.BookData.StoryFile.Move(e.ChangedIndex, e.MoveToIndex);
                    break;
                case BSystem.Collections.SuperObservableCollectionAction.Remove:

                    foreach (var v in removedItems)
                    {
                        this.Book.Remove(v);
                    }
                    break;
                case BSystem.Collections.SuperObservableCollectionAction.Replace:
                    this.BookData.StoryFile[e.ChangedIndex] = addedItems.First();
                    break;
            }
            this.IsBookEditing = false;
            this.ConsistencyCheck();

        }
        private void ConsistencyCheck()
        {
            var zips = this.Book.BookPages.Zip(this.Items.Select((v) => (ImageAATag)v.DrawItem).ToArray());
            foreach (var v in zips)
            {
                if (v.Key != v.Value.Page) throw new Exception();
            }
        }

        protected virtual void Add(IBookPage[] pages)
        {
            var items = this.Create(pages);
            this.Items.AddRange(items);
        }
        public virtual void Insert(int position, IBookPage[] pages)
        {
            var items = this.Create(pages);
            this.Items.Insert(position, items);
        }
        protected virtual ImageControl[] Create(IEnumerable<IBookPage> pages)
        {
            var p = this.TopNumber;
            var p2 = pages.Select((v) => new ImageAATag(v)).Select((v) => new AstPagePicture(v, this.WindowInfo, this.SizeOption)).ToArray();
            foreach (var v in p2)
            {
                v.Number = p++;
            }
            return p2;
        }
        protected override void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CopySelectedItem();
            this.DeleteSelectedItem();
        }
        protected override void Paste()
        {
            if (this.EditorSmallInfo != null)
            {
                if (this.EditorSmallInfo.IsClipPage)
                {
                    this.Paste(this.EditorSmallInfo.GetCopy().ToArray());
                }
            }
        }
        private void Paste(IBookPage[] pages)
        {
            if (this.DragRectangle.InsertPosition != -1)
            {
                this.Insert(this.DragRectangle.InsertPosition, pages);
            }
            else
            {
                this.Add(pages);
            }
        }
        protected override bool CanPaste
        {
            get
            {
                return this.EditorSmallInfo != null && this.EditorSmallInfo.IsClipPage;
            }
        }
        public override ImageControl CreateImageControl(IIMageControlTag tag)
        {
            return new AstPagePicture(tag,this.WindowInfo,this.SizeOption);
        }
        protected override void mainContexitMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            this.addNewPageToolStripMenuItem2.Enabled = (this.BookData != null);
            base.mainContexitMenuStrip_Opening(sender, e);
        }
        protected virtual ImageAATag CreateAATag(IBookPage page)
        {
            return new ImageAATag(page);
        }
        private void addNewPageToolStripMenuItem1_Click_1(object sender, EventArgs e)
        {
            var page = this.Book.CreateNewPage();
            var tag = this.CreateAATag(page);
            var img = this.CreateImageControl(tag);
            this.InsertOnNowPosition(img);
        }
        protected override IIMageControlTag GetDropItem(IDataObject obj)
        {
            var page = this.GetDroppedPage(obj);
            if (page == null) return null;
            if (this.Book == null)
            {
                //if (this.PageDroppedWithEmptyBook != null) this.PageDroppedWithEmptyBook(this, page);
                return null;
            }
            else
            {
                return this.GetDropIBookPage(page);
            }
        }
        protected virtual IIMageControlTag GetDropIBookPage(IBookPage page)
        {
            return new ImageAATag(page);
        }
        public IBookPage GetDroppedPage(IDataObject obj)
        {
            if (obj.GetDataPresent(typeof(ImageAATag)))
            {
                var item = (ImageAATag)obj.GetData(typeof(ImageAATag));
                return item.Page;
            }
            else if (obj.GetDataPresent(typeof(ImageSectionAATag)))
            {
                var item = (ImageSectionAATag)obj.GetData(typeof(ImageSectionAATag));
                return item.Page;
            }
            else if (obj.GetDataPresent(typeof(SuouBase.Drawing.TextThumbImage)))
            {
                var item = (SuouBase.Drawing.TextThumbImage)obj.GetData(typeof(SuouBase.Drawing.TextThumbImage));
                return item.Page;
            }
            else
            {
                return null;
            }
        }
        protected override void AcceptDropItem(IIMageControlTag tag)
        {
            if (tag is ImageAATag || tag is ImageSectionAATag)
            {
                var ntag = (ImageAATag)tag;
                if (this.EditorSmallInfo != null)
                {
                    this.EditorSmallInfo.InvokePageCopied(ntag.Page);
                }
            }
            base.AcceptDropItem(tag);
        }

        protected void Replace(IBookPage page, IBookPage newPage)
        {
            var p = this.Items.FindIndex((v) =>
            {
                var tag = (ImageAATag)v.DrawItem;
                return tag.Page == page;
            });
            if (p != -1)
            {
                var newTag = this.CreateAATag(newPage);
                var newControl = this.CreateImageControl(newTag);
                this.Items[p] = newControl;
            }
        }

        public Action<string> NotifyPageInfoFunc { get; set; }
        private void AABookViewer_OnMouseItemChanged(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (this.NotifyPageInfoFunc != null)
            {
                var p = this.SelectedPages.ToArray();
                if (p.Length == 1)
                {
                    var p1 = p.First();
                    this.NotifyPageInfoFunc(p1.SText.NotifyPageSizeMessage());
                }
                else
                {
                    this.NotifyPageInfoFunc("");
                }
            }
        }
        
    }
}
