﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading.Tasks;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using SuouBase.Controls;
using StoryViewer.ThumbnailViewer;
using BSystem.Collections;
namespace StoryViewer.ImageViewer
{

    public class DragMapRectangle : DragEventer
    {
        public DragMapRectangle(Control ctrl):base(ctrl)
        {
        }
        protected Rectangle DragRectangle
        {
            get
            {
                return PlusRectangle.Create(this.DragStartLocation, this.DragEndLocation);
            }
        }
    }
    public class DragSelectRectangle :DragMapRectangle
    {
        public DragSelectRectangle(Control ctrl,SuperObservableCollection<ImageControl> items,RectangleArrangeInfo info):base(ctrl)
        {
            this.Info = info;
            this.Ctrl = ctrl;
            this.Items = items;
            this.Ctrl.Paint += this.ContentPanel_Paint;
            this.Items.CollectionChanged += this.Item_CollectionChanged;
            this.SelectedItems = new List<ImageControl>();
            this.Recreate();
        }

        private RectangleArrangeInfo Info { get; set; }
        private void Item_CollectionChanged(object sender,SuperObservableCollectionEventArgs<ImageControl> e)
        {
            this.InsertPosition = -1;
            this.Recreate();
        }
        public int InsertPosition { get; set; }
        private void ComputeInsertPosition()
        {
            if (this.Info.IsLimitedOneArrange)
            {
                var p = this.Items.Select((v) => new BSystem.Range(v.Location.Y, v.Bottom + this.Info.VerticalInterval)).ToList();
                this.InsertPosition = p.FindIndex((v) => v.Left <= this.NowPoint.Y && this.NowPoint.Y < v.Right);
                if (this.InsertPosition == -1) this.InsertPosition = this.Items.Count;
                if (this.Items.Count + 1 == this.InsertPosition) throw new Exception();
            }
            else
            {
                var p = this.Items.Select((v) => new BSystem.Range(v.Location.Y, v.Bottom + this.Info.VerticalInterval)).ToList();
                this.InsertPosition = p.FindIndex((v) => v.Left <= this.NowPoint.Y && this.NowPoint.Y < v.Right);
                if (this.InsertPosition == -1) this.InsertPosition = this.Items.Count;
                if (this.Items.Count + 1 == this.InsertPosition) throw new Exception();
            }
        }
        public void Clear()
        {
            this.SelectedItems.ForEach((v) => v.IsDragged = false);
            this.SelectedItems.Clear();
            this.DragStartLocation = Point.Empty;
            this.DragEndLocation = Point.Empty;
        }

        public void Recreate()
        {
            this.MapItems = new MapList<ImageControl>(this.Items);
        }
        private SuperObservableCollection<ImageControl> Items { get; set; }
        private MapList<ImageControl> MapItems { get; set; }
        public IList<ImageControl> SelectedItems { get; private set; }

        private void DrawLines(Graphics g)
        {
            g.Clear(this.Ctrl.BackColor);
            this.DrawLine(g);
            this.DrawInsertLine(g);
        }
        private void DrawLine(Graphics g)
        {
            if (this.IsDragging)
            {
                g.DrawRectangle(Pens.Red, this.DragRectangle);
            }
            else
            {
                this.DrawHorizontalLine(g, this.NowPoint.Y);
            }

        }
        private void DrawInsertLine(Graphics g)
        {
            if (this.Info.IsLimitedOneArrange)
            {
                if (this.InsertPosition < 0 || this.InsertPosition >= this.Items.Count)
                {
                    if (this.Items.Count == 0)
                    {
                        g.DrawLine(Pens.Blue, new Point(0, 0), new Point(this.Ctrl.Width, 0));
                    }
                    else
                    {
                        g.DrawLine(Pens.Blue, new Point(0, this.Items.Last().Bottom), new Point(this.Ctrl.Width, this.Items.Last().Bottom));
                    }
                }
                else
                {
                    var y = this.Items[this.InsertPosition].Top;
                    g.DrawLine(Pens.Blue, new Point(0, y), new Point(this.Ctrl.Width, y));
                }
            }
            else
            {
            }
        }
        private void DrawHorizontalLine(Graphics g, int y)
        {
            g.DrawLine(Pens.Red, new Point(0, y), new Point(this.Ctrl.Width, y));
        }
        #region Property
        #endregion
        #region Event
        
        protected override void ContentPanel_MouseMove(object sender, MouseEventArgs e)
        {
            base.ContentPanel_MouseMove(sender, e);
            if (this.IsDragging)
            {
                this.SelectAction();
            }
            this.ComputeInsertPosition();
            using (var g = this.Ctrl.CreateGraphics())
            {
                this.DrawLines(g);
            }
            
        }

        private void ContentPanel_Paint(object sender, PaintEventArgs e)
        {
            this.DrawLines(e.Graphics);
            e.Graphics.Dispose();
        }
        #endregion
        private void CreateSelectedItems()
        {
            Func<MapRowItemList<ImageControl>, int> f = (obj) => obj.Rect.Bottom < this.DragRectangle.Top ? -1 :
                this.DragRectangle.Bottom < obj.Rect.Top ? 1 : 0;
            var r = BSystem.Collections.Specific.SortedList.BinarySearchRange(this.MapItems, f);
            var r1 = new List<MapRowItemList<ImageControl>>();
            if (r.Left != -1) Enumerable.Range(r.Left, r.Count).ForEach((v) => r1.Add(this.MapItems[v]));
            var r2 = r1
                .SelectMany((v) => v)
                .Where((v) => v.SRect.IntersectsWith(this.DragRectangle)).ToList();
            this.SelectedItems.Except(r2).ForEach((v) => v.IsDragged = false);
            var r3 = new List<ImageControl>();
            foreach (var v in r2)
            {
                if (v != null) r3.Add(v);
            }
            this.SelectedItems = r3;
            if (this.SelectedItems.Contains(null)) throw new Exception();
        }
        private void SelectAction()
        {
            this.CreateSelectedItems();
            this.SelectedItems.ForEach((v) => v.IsDragged = true);
        }
        /*
        private List<ImageControl> GetInternalY_Valid(IList<ImageControl> items,BSystem.Range range)
        {
            Func<IRectObject, int> f = (obj) => range.Left > obj.SLocation.Y ? -1 :
                range.Right < obj.SRect.Bottom ? 1 : 0;
            var r = BSystem.Collections.Specific.SortedList.BinarySearchRange(items, f);
            var r1 = new List<ImageControl>();
            if (r.Left != -1) Enumerable.Range(r.Left, r.Count).ForEach((v) => r1.Add(items[v]));
            return r1;
        }
        private List<ImageControl> GetInternalX_Valid(IList<ImageControl> items, Rectangle rect)
        {
            return items.Where((v)=> rect.IntersectsWith(v.SRect)).ToList();
        }
        private List<ImageControl> GetInternal_Valid(IList<ImageControl> items, Rectangle rect)
        {
            var r1 = GetInternalY_Valid(items, new BSystem.Range(rect.Top,rect.Bottom));
            var r2 = GetInternalX_Valid(r1, rect);
            return r2;
        }
        */
    }

    public class PlusRectangle
    {
        private Point FirstLocation { get; set; }
        public Point FreePoint { get; set; }
        public Rectangle Rectangle
        {
            get
            {
                var w = RightBottomLocation.X - this.LeftTopLocation.X;
                var h = RightBottomLocation.Y - this.LeftTopLocation.Y;
                return new Rectangle(this.LeftTopLocation.X, this.LeftTopLocation.Y, w, h);
            }
        }
        public PlusRectangle(Point p,Point q)
        {
            this.FirstLocation = p;
            this.FreePoint = q;
        }

        private Point LeftTopLocation
        {
            get
            {
                var x = FirstLocation.X < FreePoint.X ? FirstLocation.X :
                    FirstLocation.X == FreePoint.X ? FirstLocation.X :
                    FreePoint.X;
                var y = FirstLocation.Y < FreePoint.Y ? FirstLocation.Y :
                    FirstLocation.Y == FreePoint.Y ? FirstLocation.Y :
                    FreePoint.Y;
                return new Point(x, y);
            }
        }
        private Point RightBottomLocation
        {
            get
            {
                var x = FirstLocation.X > FreePoint.X ? FirstLocation.X :
                    FirstLocation.X == FreePoint.X ? FirstLocation.X :
                    FreePoint.X;
                var y = FirstLocation.Y > FreePoint.Y ? FirstLocation.Y :
                    FirstLocation.Y == FreePoint.Y ? FirstLocation.Y :
                    FreePoint.Y;
                return new Point(x, y);
            }
        }
        public static Rectangle Create(Point p, Point q)
        {
            var r = new PlusRectangle(p, q);
            return r.Rectangle;
        }

        public static int Hikaku(Point a, Point b)
        {
            if (a.Y < b.Y)
            {
                return -1;
            }
            else if (a.Y > b.Y)
            {
                return 1;
            }
            else
            {
                if (a.X < b.X)
                {
                    return -1;
                }
                else if (a.X > b.X)
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
    }
    
}
