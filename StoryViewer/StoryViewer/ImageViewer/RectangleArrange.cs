﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;
namespace StoryViewer.ImageViewer
{
    public interface IRectObject
    {
        Point SLocation { get; set; }
        Size SSize { get; set; }
        Rectangle SRect { get; }
    }
    class RectangleArrange
    {
        public static double GetDistance(IRectObject obj1, IRectObject obj2)
        {
            return BSystem.MathExtension.Distance(obj1.SLocation.X, obj1.SLocation.Y, obj2.SLocation.X, obj2.SLocation.Y);
        }
        public static double GetDistance(IRectObject obj1, Point p)
        {
            var a = BSystem.MathExtension.Distance(obj1.SLocation.X, obj1.SLocation.Y, p.X, p.Y);
            var b = BSystem.MathExtension.Distance(obj1.SRect.Right, obj1.SRect.Top, p.X, p.Y);
            var c = BSystem.MathExtension.Distance(obj1.SRect.Right, obj1.SRect.Bottom, p.X, p.Y);
            var d = BSystem.MathExtension.Distance(obj1.SRect.Left, obj1.SRect.Bottom, p.X, p.Y);
            return new[] { a, b, c, d }.Min();
        }
        private static bool CheckFunc(IRectObject item,RectangleArrangeInfo info,RectangleArrangeRunningInfo subInfo)
        {
            var rect = new Rectangle(subInfo.Location, item.SSize);
            if (!subInfo.IsOneArranged) return true;
            if (subInfo.IsOneArranged && info.IsLimitedOneArrange) return false;
            if (rect.Right > info.LimitedWidth) return false;
            return true;
            
        }
        /*
        public static void Arrange(IRectObject[] items, RectangleArrangeInfo info)
        {
        }
        */
        private static Rectangle GetArrangeInfo(IRectObject item, RectangleArrangeInfo info, RectangleArrangeRunningInfo subInfo)
        {
            var rec = new Rectangle() { Width = item.SSize.Width, Height = item.SSize.Height };
            var b = CheckFunc(item, info,subInfo);
            if (!b)
            {
                UpdateHorizontalList(info, subInfo);
                subInfo.IsOneArranged = false;            
            }
            rec.Location = subInfo.Location;
            subInfo.Location = new Point(subInfo.Location.X + rec.Width + info.HorizontalInterval, subInfo.Location.Y);
            if (rec.Height > subInfo.TmpHeight) subInfo.TmpHeight = rec.Height;
            if (rec.Right > subInfo.MaxWidth) subInfo.MaxWidth = rec.Right + info.HorizontalInterval;
            info.Height = subInfo.Location.Y + subInfo.TmpHeight;
            subInfo.IsOneArranged = true;
            return rec;
        }
        public static void Arrange(IRectObject item, RectangleArrangeInfo info, RectangleArrangeRunningInfo subInfo)
        {
            var rec = GetArrangeInfo(item, info, subInfo);
            item.SLocation = rec.Location;
        }
        public static void ReverseArrange(IRectObject[] items, RectangleArrangeInfo info, RectangleArrangeRunningInfo subInfo)
        {
            var recs = items.Select((v) => GetArrangeInfo(v, info, subInfo)).ToArray();
            for (int i = items.Length - 1; i >= 0; i--)
            {
                items[i].SLocation = recs[i].Location;
            }
        }


        public static void EasyTopDownArrange(IEnumerable<IRectObject> items)
        {
            int y = 0;
            int x = 0;
            foreach (var v in items)
            {
                v.SLocation = new Point(x, y);
                y += v.SSize.Height;
            }
        }
        public static Size EasyTopDownArrangeControl(IEnumerable<Control> items)
        {
            int y = 0;
            int x = 0;
            int maxWidth = 0;
            foreach (var v in items)
            {
                v.Location = new Point(x, y);
                y += v.Height;
                if (maxWidth < v.Width) maxWidth = v.Width;
            }
            return new Size(maxWidth, y);
        }

        /// <summary>
        /// tmpHorizontalItemsのアイテムをrに追加し、tmpHorizontalItemsの要素を削除します。
        /// それに従ってパラメータを適切に更新します。
        /// </summary>
        /// <param name="r"></param>
        /// <param name="tmpHorizontalItems"></param>
        /// <param name="location"></param>
        /// <param name="tmpHeight"></param>
        /// <param name="info"></param>
        private static void UpdateHorizontalList(RectangleArrangeInfo info,RectangleArrangeRunningInfo subInfo)
        {
            var y = subInfo.Location.Y + subInfo.TmpHeight + info.VerticalInterval;
            var x = info.HorizontalInterval;
            subInfo.Location = new Point(x, y);
            subInfo.TmpHeight = 0;
        }

    }
    public class MapRowItemList<T> : List<T> where T :IRectObject
    {
        public MapRowItemList(IEnumerable<T> items)
        {
            items.ForEach((v) => this.Add(v));
            var h = this.Max((v) => v.SSize.Height);
            var left = items.First().SLocation;
            var rightx = items.Last().SRect.Right;
            this.Rect = new Rectangle(left.X, left.Y, rightx - left.X, h);
        }
        public Rectangle Rect { get; set; }
    }
    public class MapList<T> : List<MapRowItemList<T>> where T : IRectObject
    {
        public MapList(IList<T> items)
        {
            if (!MapItemsCheck(items.Cast<IRectObject>())) throw new Exception();
            Filter(items).ForEach((v) => this.Add(v));
        }
        private static bool MapItemsCheck(IEnumerable<IRectObject> items)
        {
            return items.Pair().All((v) => v.Key.SLocation.Y <= v.Value.SLocation.Y);
        }
        private static IEnumerable<MapRowItemList<T>> Filter(IEnumerable<T> items)
        {
            if (items.FirstOrDefault() != null)
            {
                List<T> tmpList = new List<T>();
                int y = items.First().SLocation.Y;
                foreach (var v in items)
                {
                    if (v.SLocation.Y != y)
                    {
                        yield return new MapRowItemList<T>(tmpList);
                        tmpList.Clear();
                        tmpList.Add(v);
                        y = v.SLocation.Y;
                    }
                    else
                    {
                        tmpList.Add(v);
                    }
                }
                yield return new MapRowItemList<T>(tmpList);
            }
        }
    }


    public class RectangleArrangeInfo
    {
        public int VerticalInterval { get; set; }
        public int HorizontalInterval { get; set; }
        public int LimitedWidth { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        //public Point StartLocation { get; set; }
        public Size Size
        {
            get { return new Size(Width, Height); }
        }
        public bool IsLimitedOneArrange { get; set; }
        //public bool IsLimitedArrangeInRectangle { get; set; }
    }
    public class RectangleArrangeRunningInfo
    {
        public RectangleArrangeRunningInfo(RectangleArrangeInfo info)
        {
            this.Location = new Point(info.HorizontalInterval, info.VerticalInterval);
        }
        public Point Location { get; set; }
        public bool IsOneArranged { get; set; }
        public int TmpHeight { get; set; }
        public int MaxWidth { get; set; }
    }

    public class ParentControlInfo
    {
        private HScrollProperties HBar { get; set; }
        private VScrollProperties VBar { get; set; }
        private Control Panel { get; set; }
        public ParentControlInfo(Control panel, HScrollProperties hBar, VScrollProperties vBar)
        {
            this.Panel = panel;
            this.HBar = hBar;
            this.VBar = vBar;
        }
        private Rectangle Window
        {
            get
            {
                var p = new Point(HBar.Value, VBar.Value);
                return new Rectangle(p, this.Panel.Size);
            }
        }

        public bool ContainControl(IRectObject obj)
        {
            return this.Window.IntersectsWith(obj.SRect);
        }
        public Point ClientLocation(IRectObject obj)
        {
            var x = obj.SLocation.X - this.Window.X;
            var y = obj.SLocation.Y - this.Window.Y;
            return new Point(x, y);
        }
    }
}
