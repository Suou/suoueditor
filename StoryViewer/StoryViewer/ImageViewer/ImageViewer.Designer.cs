﻿namespace StoryViewer.ImageViewer
{
    partial class ImageViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            this.MyDispose();
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.mainContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ItemContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copySJIS10ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.operationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upMoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downMoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aaViewMenuItem = new StoryViewer.ImageViewer.AAViewMenuItem();
            this.panel1 = new StoryViewer.ImageViewer.ContentPanel();
            this.imageToolTip1 = new StoryViewer.ImageViewer.ImageToolTip();
            this.mainContextMenuStrip.SuspendLayout();
            this.ItemContextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 800;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // mainContextMenuStrip
            // 
            this.mainContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aaViewMenuItem,
            this.pasteToolStripMenuItem});
            this.mainContextMenuStrip.Name = "mainContexitMenuStrip";
            this.mainContextMenuStrip.Size = new System.Drawing.Size(125, 48);
            this.mainContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.mainContexitMenuStrip_Opening);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.pasteToolStripMenuItem.Text = "貼り付け";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // ItemContextMenuStrip
            // 
            this.ItemContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.operationToolStripMenuItem, this.copySJIS10ToolStripMenuItem});
            this.ItemContextMenuStrip.Name = "ItemContextMenuStrip";
            this.ItemContextMenuStrip.Size = new System.Drawing.Size(125, 92);
            this.ItemContextMenuStrip.Closing += new System.Windows.Forms.ToolStripDropDownClosingEventHandler(this.ItemContextMenuStrip_Closing);
            this.ItemContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.ItemContextMenuStrip_Opening);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.copyToolStripMenuItem.Text = "コピー";
            this.copyToolStripMenuItem.Click += this.copyToolStripMenuItem_Click;

            // 
            // copyUNIToolStripMenuItem
            // 
            this.copySJIS10ToolStripMenuItem.Name = "copyUNIToolStripMenuItem";
            this.copySJIS10ToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.copySJIS10ToolStripMenuItem.Text = "コピー(SJIS10)";
            this.copySJIS10ToolStripMenuItem.Click += this.copySJIS10ToolStripMenuItem_Click;


            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.cutToolStripMenuItem.Text = "切り取り";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.deleteToolStripMenuItem.Text = "削除";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // operationToolStripMenuItem
            // 
            this.operationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.upMoveToolStripMenuItem,
            this.downMoveToolStripMenuItem});
            this.operationToolStripMenuItem.Name = "operationToolStripMenuItem";
            this.operationToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.operationToolStripMenuItem.Text = "移動";
            // 
            // upMoveToolStripMenuItem
            // 
            this.upMoveToolStripMenuItem.Name = "upMoveToolStripMenuItem";
            this.upMoveToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.upMoveToolStripMenuItem.Text = "上へ移動";
            this.upMoveToolStripMenuItem.Click += new System.EventHandler(this.upMoveToolStripMenuItem_Click);
            // 
            // downMoveToolStripMenuItem
            // 
            this.downMoveToolStripMenuItem.Name = "downMoveToolStripMenuItem";
            this.downMoveToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.downMoveToolStripMenuItem.Text = "下へ移動";
            this.downMoveToolStripMenuItem.Click += new System.EventHandler(this.downMoveToolStripMenuItem_Click);
            // 
            // aaViewMenuItem
            // 
            this.aaViewMenuItem.Name = "aaViewMenuItem";
            this.aaViewMenuItem.Option = null;
            this.aaViewMenuItem.Size = new System.Drawing.Size(124, 22);
            this.aaViewMenuItem.Text = "AA表示";
            // 
            // panel1
            // 
            this.panel1.AllowDrop = true;
            this.panel1.AutoSize = true;
            this.panel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.panel1.ContextMenuStrip = this.mainContextMenuStrip;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.MinimumSize = new System.Drawing.Size(100, 100);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(0, 0, 0, 500);
            this.panel1.Size = new System.Drawing.Size(313, 500);
            this.panel1.TabIndex = 2;
            this.panel1.DragDrop += new System.Windows.Forms.DragEventHandler(this.panel1_DragDrop);
            this.panel1.DragEnter += new System.Windows.Forms.DragEventHandler(this.panel1_DragEnter);
            this.panel1.DragOver += new System.Windows.Forms.DragEventHandler(this.panel1_DragOver);
            this.panel1.MouseEnter += new System.EventHandler(this.panel1_MouseEnter);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            // 
            // imageToolTip1
            // 
            this.imageToolTip1.AutoPopDelay = 8000;
            this.imageToolTip1.InitialDelay = 100;
            this.imageToolTip1.IsSensitivePopup = true;
            this.imageToolTip1.OwnerDraw = true;
            this.imageToolTip1.ParentControl = this;
            this.imageToolTip1.PopUpSize = new System.Drawing.Size(0, 0);
            this.imageToolTip1.ReshowDelay = 100;
            // 
            // ImageViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ContextMenuStrip = this.mainContextMenuStrip;
            this.Controls.Add(this.panel1);
            this.Name = "ImageViewer";
            this.Size = new System.Drawing.Size(313, 274);
            this.Load += new System.EventHandler(this.ImageViewer_Load);
            this.Scroll += new System.Windows.Forms.ScrollEventHandler(this.ImageViewer_Scroll);
            this.SizeChanged += new System.EventHandler(this.ImageViewer_SizeChanged);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ImageViewer_MouseMove);
            this.mainContextMenuStrip.ResumeLayout(false);
            this.ItemContextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private ImageToolTip imageToolTip1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copySJIS10ToolStripMenuItem;

        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        protected System.Windows.Forms.ContextMenuStrip mainContextMenuStrip;
        private StoryViewer.ImageViewer.ContentPanel panel1;
        private System.Windows.Forms.ToolStripMenuItem pasteToolStripMenuItem;
        protected System.Windows.Forms.ContextMenuStrip ItemContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem operationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upMoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downMoveToolStripMenuItem;
        private StoryViewer.ImageViewer.AAViewMenuItem aaViewMenuItem;
    }
}
