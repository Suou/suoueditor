﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoryViewer.ImageViewer
{
    public partial class ImageControl : UserControl,IRectObject
    {
        public ImageControl()
        {
            InitializeComponent();
            this.DragStartMix = new DragStartMix(this);
            this.DragStartMix.DragStarted += this.DragStartMix_DragStart;
            this.HandleCreated += this.Handle_Created;
        }
        public ImageControl(IIMageControlTag drawItem,ImageControlOption option):this()
        {
            this.DrawItem = drawItem;
            this.Option = option;
        }
        public event EventHandler IsOnMouseChanged;
        public event EventHandler IsInnerChanged;
        public event CancelEventHandler IsOnMouseChanging;
        public event EventHandler DragStarting;
        public event EventHandler ItemLeftClicked;
        private DragStartMix DragStartMix { get; set; }
        public ImageControlOption Option { get; private set; }
        public ImageToolTip ToolTip { get; set; }
        public ParentControlInfo WindowInfo { get; set; }

        public virtual int Number { get; set; }
        
        private bool _IsMouseOn = false;
        public bool IsMouseOn
        {
            get { return this._IsMouseOn; }
            set
            {
                if (this._IsMouseOn == value) return;
                var e = new CancelEventArgs();
                if (this.IsOnMouseChanging != null) this.IsOnMouseChanging(this, e);
                if (e.Cancel) return;
                this._IsMouseOn = value;
                this.ImageCheck();
                if (this.IsOnMouseChanged != null) this.IsOnMouseChanged(this, new EventArgs());
            }
        }
        private Point _SLocation;
        public Point SLocation
        {
            get {

                return this._SLocation; }
            set
            {
                this._SLocation = value;
                if (value == Point.Empty) throw new Exception();
                this.Location = value;
            }
        }
        private Size _SSize { get; set; }
        public Size SSize
        {
            get { return this._SSize; }
            set
            {
                if (this._SSize == value) return;
                this._SSize = value;
                if(this.Size != value)this.Size = value;
                //this.ClearImage();
            }
        }
        public Rectangle SRect
        {
            get
            {
                return new Rectangle(this.Location, this.Size);
            }
        }
        private bool _IsDragged = false;
        public bool IsDragged
        {
            get { return this._IsDragged; }
            set {
                if (this._IsDragged == value) return;
                this._IsDragged = value;
                this.ImageCheck();
            }
        }
        public bool IsSelected
        {
            get { return this.IsMouseOn || this.IsDragged; }
        }

        private bool _EditLock = true;
        public bool EditLock
        {
            get { return this._EditLock; }
            set
            {
                this._EditLock = value;
            }

        }

        public IIMageControlTag DrawItem { get; set; }
        private bool _IsInner;
        public bool IsInner
        {
            get { return this._IsInner; }
            set
            {
                if (this._IsInner == value) return;
                this._IsInner = value;
                if (this.IsInnerChanged != null) this.IsInnerChanged(this, new EventArgs());
            }
        }
        protected void OnItemLeftClick()
        {
            if (this.ItemLeftClicked != null) this.ItemLeftClicked(this, new EventArgs());
        }
        protected void DragStart()
        {
            if (this.DragStarting != null) this.DragStarting(this, new EventArgs());
        }
        protected void ImageCheck()
        {
            if (this.IsDragged)
            {
                this.ChangeDragImage();
            }
            else if (this.IsMouseOn || this.IsDragged)
            {
                this.ChangeReverseImage();
            }
            else
            {
                this.ChangeNormalImage();
            }
        }
        private void Handle_Created(object sender, EventArgs e)
        {
            if(this.Visible)this.LoadingPicture();
        }
        private bool _NeedLoadImage = true;
        public virtual bool NeedLoadImage
        {
            get { return this._NeedLoadImage; }
            protected set { this._NeedLoadImage = value; }
        }
        public virtual bool CanCreateImage { get; private set; }
        public virtual void LoadingPicture()
        {
            
        }
        protected virtual void ChangeReverseImage()
        {
            this.BackColor = this.SelectedColor;
        }
        protected virtual void ChangeNormalImage()
        {
            this.BackColor = this.NonSelectedColor;
        }
        protected virtual void ChangeDragImage()
        {
            this.BackColor = this.DraggedColor;
        }
        public virtual void ClearImage()
        {

        }
        private void ImageControl_VisibleChanged(object sender, EventArgs e)
        {
            
        }
        public virtual Size OriginalSize
        {
            get
            {
                return new Size(200, 200);
            }
        }
        public virtual void SetControlSize()
        {
            if (this.Option.Mode == SuouBase.Drawing.ThumbnailMode.Raw)
            {
                this.SetModeRow();
            }
            else if (this.Option.Mode == SuouBase.Drawing.ThumbnailMode.Size)
            {
                this.SetModeSize();
            }
            else if (this.Option.Mode == SuouBase.Drawing.ThumbnailMode.WidthSizeHeightPer)
            {
                this.SetModeWidthSizeHeigthPer();
            }
            else
            {
                this.SetModePercent();
            }
        }
        protected virtual void SetModeSize()
        {
            var sz = this.Option.Size;
            this.SSize = sz;
        }
        public virtual void SetModePercent()
        {
            var percent = this.Option.Percent;
            var w = (int)(this.DrawItem.OriginalSize.Width * percent);
            var h = (int)(this.DrawItem.OriginalSize.Height * percent);
            this.SSize = new Size(w, h);
        }
        public virtual void SetModeRow()
        {
            throw new NotSupportedException();
        }
        public virtual void SetModeWidthSizeHeigthPer()
        {
            throw new NotSupportedException();
        }

        protected virtual void ImageControl_MouseEnter(object sender, EventArgs e)
        {
            this.IsMouseOn = true;
        }
        
        protected virtual void ImageControl_MouseLeave(object sender, EventArgs e)
        {
            this.IsMouseOn = false;
        }

        private Color _SelectedColor = Color.Yellow;
        private Color _NonSelectedColor1 = Color.Aqua;
        private Color _NonSelectedColor2 = Color.LightPink;
        private Color _DraggedColor = Color.Green;
        public Color SelectedColor
        {
            get { return this._SelectedColor; }
        }
        public virtual Color NonSelectedColor
        {
            get {
                if (this.Number % 2 == 0)
                {
                    return this._NonSelectedColor1; 
                }
                else
                {
                    return this._NonSelectedColor2; 
                }
            }
        }
        public Color DraggedColor
        {
            get { return this._DraggedColor; }
        }

        private void ImageControl_SizeChanged(object sender, EventArgs e)
        {
            this._SSize = this.Size;
            if (this.DesignMode) return;
            //if (this.Width == 315) throw new Exception();
        }

        private void ImageControl_QueryContinueDrag(object sender, QueryContinueDragEventArgs e)
        {
        }

        private void ImageControl_GiveFeedback(object sender, GiveFeedbackEventArgs e)
        {
        }
        private void DragStartMix_DragStart(object sender, EventArgs e)
        {
            this.DragStart();
        }

        private void ImageControl_ParentChanged(object sender, EventArgs e)
        {
        }
        
    }
}
