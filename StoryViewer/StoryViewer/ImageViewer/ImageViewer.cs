﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using BSystem;
using BSystem.Collections;
using BSystem.Collections.Specific;
using System.IO;
namespace StoryViewer.ImageViewer
{
    public partial class ImageViewer : UserControl
    {
        #region Constructor
        public ImageViewer()
        {
            
            InitializeComponent();
            this.InnerSet = new HashSet<ImageControl>();
            this.Items = new MapItems<ImageControl>();
            this._Info = new RectangleArrangeInfo() { HorizontalInterval = 5, VerticalInterval = 5, LimitedWidth = 400, IsLimitedOneArrange = false };
            this.RunningInfo = new RectangleArrangeRunningInfo(this.Info);
            this.Items.CollectionChanged += this.Items_CollectionChanged;
            this.WindowInfo = new ParentControlInfo(this, this.HorizontalScroll, this.VerticalScroll);
            this.DragRectangle = new DragSelectRectangle(this.panel1, this.Items,this.Info);

            this.panel1.Padding = new Padding(3, 3, 3, 200);
            //this.panel1.Controls.Add(this.EmptyLabel);
            //this.EmptyLabel.Location = new Point(0,2000);
        }
        private Label EmptyLabel = new Label();
        #endregion
        public event Action<object, IIMageControlTag> ItemClicked;
        public event EventHandler OnMouseItemChanged;
        #region Property
        public bool IsInnerDragMove { get; set; }
        private ImageControlOption _SizeOption = null;

        //[Browsable(false)]

        protected ImageControlOption CreateSizeOptionSample()
        {
            return new ImageControlOption();
        }

        [ReadOnly(true)]
        public ImageControlOption SizeOption
        {
            get
            {
                if (this._SizeOption == null) this.SizeOption = new ImageControlOption();
                return this._SizeOption;
            }
            set
            {
                if (value == null) return;
                if (this._SizeOption != null)
                {
                    this._SizeOption.OptionChanged -= this.SizeOption_Changed;
                }
                this._SizeOption = value;
                this._SizeOption.OptionChanged += this.SizeOption_Changed;
                this.SizeOption_Changed(value, new EventArgs());
                this.aaViewMenuItem.Option = this._SizeOption;
            }
        }
        private HashSet<ImageControl> InnerSet { get; set; }
        private Point NowMouseLocation
        {
            get
            {
                
                var x = this.PointToClient(Control.MousePosition).X + this.ScrollLocation.X;
                var y = this.PointToClient(Control.MousePosition).Y + this.ScrollLocation.Y;
                return new Point(x, y);
            }
        }
        protected ParentControlInfo WindowInfo { get; set; }
        protected MapItems<ImageControl> Items { get; private set; }
        public ImageControl[] ArrayItems
        {
            get
            {
                return this.Items.ToArray();
            }
        }
        private ImageControl _OnMouseItem = null;
        protected ImageControl OnMouseItem
        {
            get { return this._OnMouseItem; }
            set
            {
                if (this._OnMouseItem != value)
                {
                    if (this._OnMouseItem != null)
                    {
                        this._OnMouseItem.IsMouseOn = false;
                    }
                    this._OnMouseItem = value;
                    if (this.OnMouseItemChanged != null) this.OnMouseItemChanged(value, new EventArgs());
                }
            }
        }
        protected IEnumerable<ImageControl> SelectedItems
        {
            get
            {
                if (this.DragRectangle.SelectedItems.Count > 0)
                {
                    foreach(var v in this.DragRectangle.SelectedItems) yield return v;
                }
                else if(this.OnMouseItem != null)
                {
                    yield return this.OnMouseItem;
                }
            }
        }
        /*
        private List<ImageControl> _SelectedItems = new List<ImageControl>();
        protected List<ImageControl> SelectedItems
        {
            get { return this._SelectedItems; }
        }
        */
        private RectangleArrangeInfo _Info;
        protected RectangleArrangeInfo Info
        {
            get { return this._Info; }
        }
        private bool MultiSelectMode { get; set; }
        private Size PastSize { get; set; }
        private int PastSizeInterval = 50;
        private bool IsRearranging = false;
        private bool SetFlag { get; set; }
        protected DragSelectRectangle DragRectangle { get; set; }
        private RectangleArrangeRunningInfo RunningInfo { get; set; }
        private Point ScrollLocation
        {
            get
            {
                var x = this.HorizontalScroll.Value;
                var y = this.VerticalScroll.Value;
                return new Point(x, y);
            }
        }
        #endregion

        #region Drag

        private ImageControl _DraggingItem = null;
        private ImageControl AcceptedDraggingItem
        {
            get { return this._DraggingItem; }
            set
            {
                if (this._DraggingItem == value)
                {
                }
                else
                {
                    if (this._DraggingItem != null)
                    {
                        this._DraggingItem.IsDragged = false;
                    }
                    this._DraggingItem = value;
                    if(value != null)this._DraggingItem.IsDragged = true;
                }
            }
        }
        private bool IsDragging { get; set; }
        private Point PreDragMoveLocation { get; set; }
        private int DragMoveDistance()
        {
            return (int)BSystem.MathExtension.Distance(this.NowMouseLocation.X, this.NowMouseLocation.Y, this.PreDragMoveLocation.X, this.PreDragMoveLocation.Y);
        }
        private bool DragMovable
        {
            get
            {
                return this.DragMoveDistance() > 5;
            }
        }
        #endregion
        #region Number
        private int _TopNumber { get; set; }
        public int TopNumber
        {
            get { return this._TopNumber; }
            set
            {
                this._TopNumber = value;
                this.ReNumber();
            }
        }
        protected void ReNumber()
        {
            int t = this.TopNumber;
            foreach (var v in this.Items)
            {
                v.Number = t++;
            }
        }
        #endregion
        #region SetAndRemoveEvent
        protected virtual void SetEvent(ImageControl c)
        {
            c.SetControlSize();
            c.ToolTip = this.imageToolTip1;
            c.ContextMenuStrip = this.ItemContextMenuStrip;
            c.IsOnMouseChanged += this.ImageControl_IsOnMouseChanged;
            c.MouseMove += this.ImageControl_MouseMove;
            c.SizeChanged += this.ImageControl_SizeChanged;
            c.IsOnMouseChanging += this.ImageControl_IsOnMouseChanging;
            c.DragStarting += this.ImageControl_DragStarting;
            c.ItemLeftClicked += this.Picture_LeftClicked;
        }
        protected virtual void RemoveEvent(ImageControl c)
        {
            c.ToolTip = null;
            c.ContextMenuStrip = null;
            c.IsOnMouseChanged -= this.ImageControl_IsOnMouseChanged;
            c.MouseMove -= this.ImageControl_MouseMove;
            c.SizeChanged -= this.ImageViewer_SizeChanged;
            c.IsOnMouseChanging -= this.ImageControl_IsOnMouseChanging;
            c.DragStarting -= this.ImageControl_DragStarting;
            c.ItemLeftClicked -= this.Picture_LeftClicked;
            c.Dispose();
        }
        #endregion

        private void Picture_LeftClicked(object sender,EventArgs e)
        {
            if (this.ItemClicked != null) this.ItemClicked(this, this.OnMouseItem.DrawItem);
        }
        public virtual bool FocusName(string name)
        {
            throw new Exception();
        }
        #region Method
        public void Set(IEnumerable<ImageControl> items)
        {
            SetFlag = true;
            this.Items.Clear();
            this.Items.AddRange(items);
            this.ReNumber();
            //this.Rearrange();
            //this.CheckInnerOrOuterControl();
            SetFlag = false;
            this.CheckLoadPictures();
        }
        public void InsertOnNowPosition(ImageControl item)
        {
            //var p = this.Items.GetNearIndex(this.NowMouseLocation);
            var p = this.DragRectangle.InsertPosition;
            if (p == -1) p = 0;
            this.Items.Insert(p, item);
        }
        private void Rearrange()
        {
            this.RunningInfo = new RectangleArrangeRunningInfo(this.Info);
            this.IsRearranging = true;

            SuouBase.Api.WINAPI.StopControlDraw(this.Handle);
            //var firstItem = this.InnerSet.Count > 0 ? this.InnerSet.Max2((v)=>v.SLocation.Y) : null;
            var p = new Point(this.HorizontalScroll.Value, this.VerticalScroll.Value);
            //これがないとAutoScrollの仕様から画像の位置がおかしくなる
            //this.VerticalScroll.Value = 0;
            //RectangleArrange.ReverseArrange(this.Items.ToArray(), this.Info, this.RunningInfo);
            this.Items.ForEach((v) => RectangleArrange.Arrange(v, this.Info, this.RunningInfo));            
            //if (firstItem != null) this.ScrollControlIntoView(firstItem);
            SuouBase.Api.WINAPI.StartControlDraw(this.Handle);
            this.Refresh();


            this.IsRearranging = false;
        }
        private void AddArrange(ImageControl obj)
        {
            this.IsRearranging = true;
            RectangleArrange.Arrange(obj, this.Info,this.RunningInfo);
            this.IsRearranging = false;
        }
        public void ClearCache()
        {
            this.Items.Where((v)=>!v.IsInner).ForEach((v) => v.ClearImage());
            
        }
        public virtual void Clear()
        {
            this.Items.Clear();
           
        }

        public void ResetArrange(Point p)
        {
            this.Rearrange();
            this.CheckInnerOrOuterControl();
            this.AutoScrollPosition = p;
            this.DragRectangle.Recreate();
            //this.VerticalScroll.Value = p.Y;
            //this.HorizontalScroll.Value = p.X;
        }
        public void ResetArrange()
        {
            this.Rearrange();
            this.CheckInnerOrOuterControl();
            this.DragRectangle.Recreate();
        }
        protected override Point ScrollToControl(Control activeControl)
        {
            return this.AutoScrollPosition;
            //return base.ScrollToControl(activeControl);
        }
        public void FocusPage(int i)
        {
            var p = this.Items[i];
            this.ScrollControlIntoView(p);
        }
        public void ScrollControlIntoView(ImageControl c)
        {
            this.AutoScrollPosition = c.SLocation;
            //base.ScrollControlIntoView(c);
            this.CheckInnerOrOuterControl();
        }
        private int CheckFunc(ImageControl cont)
        {
            //ClientRectangleをより大きくとってもいいかも
            var b = this.WindowInfo.ContainControl(cont);
            if (b)
            {
                return 0;
            }
            else
            {
                if (cont.Location.Y < this.VerticalScroll.Value)
                {
                    return -1;
                }
                else
                {
                    return 1;
                }
            }
        }
        private void CheckInnerOrOuterControl()
        {
            
            if (this.IsRearranging) return;
            HashSet<ImageControl> innerSet = new HashSet<ImageControl>();

            var r = BSystem.Collections.Specific.SortedList.BinarySearchRange(this.Items, this.CheckFunc);
            if (r.Left != -1) Enumerable.Range(r.Left, r.Count).ForEach((v) => innerSet.Add(this.Items[v]));

            var outingSet = this.InnerSet.Where((v) => !innerSet.Contains(v));
            innerSet.ForEach((v) => v.IsInner = true);
            outingSet.ForEach((v) => v.IsInner = false);
            this.InnerSet = innerSet;
        }
        private void CheckInnerOrOuterControl(ImageControl c)
        {
            var b = this.WindowInfo.ContainControl(c);
            if (this.WindowInfo.ContainControl(c))
            {
                this.InnerSet.Add(c);
                c.IsInner = true;
            }
            else
            {
                c.IsInner = false;
            }
        }


        private bool IsSetSizing { get; set; }

        #endregion
        private void SetSize()
        {
            this.IsSetSizing = true;
            this.Items.ForEach((v) => v.SetControlSize());
            this.IsSetSizing = false;
        }
        private void ResizeAndReset()
        {
            SuouBase.Api.WINAPI.StopControlDraw(this.panel1.Handle);
            var time = System.Diagnostics.Stopwatch.StartNew();

            this.SetSize();

            this.ResetArrange();
            time.Stop();

            SuouBase.Api.WINAPI.StartControlDraw(this.panel1.Handle);
            this.Refresh();
        }
        private void ImageControl_DragStarting(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;
            this.IsDragging = true;
            var item = (ImageControl)sender;
            if (this.IsInnerDragMove)
            {
                this.AcceptedDraggingItem = item;
            }
            else
            {
            }
            var copy = item.DrawItem.Copy();
            DragDropEffects dde = item.DoDragDrop(copy, DragDropEffects.All);
            
        }
        private void SizeOption_Changed(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (!this.IsRearranging)
            {
                this.ResizeAndReset();
            }
        }
        #region ImageViewerEvent
        private void ImageViewer_Scroll(object sender, ScrollEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.CheckInnerOrOuterControl();
        }
        private void ImageViewer_Load(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.AutoScrollPosition = this.AutoScrollPosition;
            this.MouseWheel += ((a, b) => this.CheckInnerOrOuterControl());
            //this.SizeOption.OptionChanged += this.SizeOption_Changed;
        }
        private void ImageViewer_SizeChanged(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.panel1.MinimumSize = this.Size;
            if (this.Info.IsLimitedOneArrange)
            {
                this.PastSize = this.Size;
                this.Info.LimitedWidth = this.Width;
                this.panel1.Width = this.Info.LimitedWidth;
            }
            else
            {
                var d = System.Math.Abs(this.PastSize.Width - this.Size.Width);
                if (d > this.PastSizeInterval)
                {
                    this.PastSize = this.Size;
                    this.Info.LimitedWidth = this.Width;
                    this.panel1.Width = this.Info.LimitedWidth;
                    this.ResizeAndReset();
                }
            }
            this.CheckInnerOrOuterControl();
            
        }

        #endregion
        #region ImageControlEvent
        private void ImageControl_IsOnMouseChanging(object sender, CancelEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            //アイテム上で右クリックした場合
            if (this.IsContextOpening)
            {
                var cont = this.OnMouseItem;

                if (!this.DragRectangle.SelectedItems.Contains(cont)) this.DragRectangle.Clear();
                e.Cancel = true;
            }
        }
        private void ImageControl_SizeChanged(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (this.IsSetSizing) return;
            this.ResetArrange();
        }
        private void ImageControl_IsOnMouseChanged(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            var cont = (ImageControl)sender;
            if (cont.IsMouseOn)
            {
                this.OnMouseItem = cont;
            }
            else
            {
                this.OnMouseItem = null;
            }
            

        }
        private void ImageControl_MouseMove(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            var cont = (ImageControl)sender;
            if (this.MultiSelectMode)
            {
            }
            else
            {
                //this.SelectedItems.Where((v) => v != cont).ToArray().ForEach((v) => v.IsSelected = false);
            }
        }
        #endregion
        #region OtherEvent
        protected virtual void Items_CollectionChanged(object sender, SuperObservableCollectionEventArgs<ImageControl> e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (e.Action == SuperObservableCollectionAction.Add)
            {
                //this.AutoScroll = false;
                foreach (ImageControl v in e.AddedItems)
                {
                    this.SetEvent(v);
                    this.AddArrange(v);
                    this.panel1.Controls.Add(v);
                    this.CheckInnerOrOuterControl(v);
                }
                if (e.AddedItems.Count > 0)
                {
                    var fstItem = e.AddedItems.First();
                    if (!this.WindowInfo.ContainControl(fstItem)) this.ScrollControlIntoView(fstItem);
                }

            }
            else if (e.Action == SuperObservableCollectionAction.Insert)
            {
                var p = this.ScrollLocation;
                //this.AutoScroll = false;
                foreach (ImageControl v in e.AddedItems)
                {
                    this.SetEvent(v);
                    this.panel1.Controls.Add(v);
                }
                //this.AutoScroll = true;
                this.ResetArrange(p);
                
            }
            else if (e.Action == SuperObservableCollectionAction.Move)
            {
                this.ResetArrange();
            }
            else if (e.Action == SuperObservableCollectionAction.Remove)
            {
                var p = this.ScrollLocation;
                //this.AutoScroll = false;
                foreach (var v in e.RemovedItems)
                {
                    if (v == null) throw new Exception();
                    this.panel1.Controls.Remove(v);
                    this.RemoveEvent(v);
                }
                //this.AutoScroll = true;
                this.ResetArrange(p);
                
            }
            else if (e.Action == SuperObservableCollectionAction.Replace)
            {
                var p = this.ScrollLocation;
                var preItem = e.RemovedItems.First();
                this.panel1.Controls.Remove(preItem);
                this.RemoveEvent(preItem);

                var newItem = e.AddedItems.First();
                this.SetEvent(newItem);
                this.panel1.Controls.Add(newItem);

                this.ResetArrange(p);

            }
            else if (e.Action == SuperObservableCollectionAction.Clear)
            {
                SuouBase.Api.WINAPI.StopControlDraw(this.Handle);
                foreach (ImageControl v in e.RemovedItems)
                {
                    this.panel1.Controls.Remove(v);
                    this.RemoveEvent(v);
                }
                SuouBase.Api.WINAPI.StartControlDraw(this.Handle);
                this.Refresh();
                this.RunningInfo = new RectangleArrangeRunningInfo(this.Info);
            }
            else
            {
                throw new NotSupportedException();
            }
        }
        #endregion

        #region ClickEvent

        protected virtual void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        protected virtual void Paste()
        {
        }
        protected void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Paste();
        }
        protected virtual void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.DeleteSelectedItem();
        }
        protected virtual void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }
        protected virtual void copySJIS10ToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        #endregion
        protected void DeleteSelectedItem()
        {
            if (this.SelectedItems.Contains(null)) throw new Exception();
            this.Items.RemoveRange(this.SelectedItems);
        }

        protected virtual void ItemContextMenuStrip_Opening(object sender, CancelEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.IsContextOpening = true;
            if (this.OnMouseItem == null) this.DragRectangle.Clear();
        }
        protected virtual bool CanPaste
        {
            get { return false; }
        }
        protected bool IsContextOpening { get; set; }

        private void ItemContextMenuStrip_Closing(object sender, ToolStripDropDownClosingEventArgs e)
        {
            this.IsContextOpening = false;
        }

        

        protected virtual void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.IsDragging = false;
            if (this.AcceptedDraggingItem != null) this.AcceptedDraggingItem = null;
            if (this.MultiSelectMode)
            {
            }
            else
            {
                this.OnMouseItem = null;
                //this.SelectedItems.ForEach((v) => v.IsSelected = false);
            }
            this.OnMouseMove(e);
        }

        protected virtual void mainContexitMenuStrip_Opening(object sender, CancelEventArgs e)
        {

            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.pasteToolStripMenuItem.Enabled = this.CanPaste;

            this.DragRectangle.Clear();
            //this.SetSizeViewArrow();
        }
        
        
        private void ImageViewer_MouseMove(object sender, MouseEventArgs e)
        {
        }

        protected virtual IIMageControlTag GetDropItem(IDataObject obj)
        {
            if (obj.GetDataPresent(typeof(ImageAATag)))
            {
                var item = (ImageAATag)obj.GetData(typeof(ImageAATag));
                return item;
            }
            else if (obj.GetDataPresent(typeof(SuouBase.Drawing.TextThumbImage)))
            {
                var item = (SuouBase.Drawing.TextThumbImage)obj.GetData(typeof(SuouBase.Drawing.TextThumbImage));
                return new ImageAATag(item);
            }
            else
            {
                return null;
            }
        }
        protected virtual void AcceptDropItem(IIMageControlTag tag)
        {
        }
        private void panel1_DragEnter(object sender, DragEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            var item = this.GetDropItem(e.Data);
            
            if (item is ImageAATag || item is ImageSectionAATag)
            {
                if (this.IsDragging)
                {
                    if (this.IsInnerDragMove)
                    {
                        e.Effect = DragDropEffects.Move;
                        this.DragMove();
                    }
                    else
                    {
                        e.Effect = DragDropEffects.None;
                    }
                }
                else
                {
                    e.Effect = DragDropEffects.Copy;
                    var insertItem = this.CreateImageControl(item);
                    this.InsertOnNowPosition(insertItem);
                    this.AcceptedDraggingItem = insertItem;
                    this.AcceptDropItem(item);
                }
            }
            else
            {

                e.Effect = DragDropEffects.None;
                this.OnDragEnter(e);
            }
        }

        private void panel1_DragDrop(object sender, DragEventArgs e)
        {
            if (this.AcceptedDraggingItem != null)
            {
                this.AcceptedDraggingItem = null;
            }
            else
            {
                this.OnDragDrop(e);
            }
        }
        public virtual ImageControl CreateImageControl(IIMageControlTag tag)
        {
            var p = new ImageControl(tag, this.SizeOption);
            return p;
        }
        private void DragMove()
        {
            if (this.AcceptedDraggingItem == null) return;
            var oldIndex = this.Items.IndexOf(this.AcceptedDraggingItem);
            var newIndex = this.Items.GetNearIndex(this.NowMouseLocation);
            if (newIndex == -1) return;
            this.Items.Move(oldIndex, newIndex);
            this.PreDragMoveLocation = this.NowMouseLocation;
        }
        private void panel1_DragOver(object sender, DragEventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            if (this.DragMovable)
            {
                this.DragMove();
            }
        }

        private void upMoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var index = this.Items.IndexOf(this.OnMouseItem);
            var newIndex = index - 1;
            if(newIndex >= 0)this.Items.Move(index, newIndex);
        }

        private void downMoveToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var index = this.Items.IndexOf(this.OnMouseItem);
            var newIndex = index + 1;
            if(newIndex < this.Items.Count)this.Items.Move(index, newIndex);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.CheckLoadPictures();
        }
        private void CheckLoadPictures()
        {
            this.InnerSet.Where((v) => v.CanCreateImage).ForEach((v) => v.LoadingPicture());

        }

        private void panel1_MouseEnter(object sender, EventArgs e)
        {
            if (BSystem.Diagnostics.IsDesignMode(this)) return;

            this.OnMouseEnter(e);
        }
        public void MyDispose()
        {
        }
        
    }
    public enum SelectMode
    {
        Single, Multi, Shift
    }
}
