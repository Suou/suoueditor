﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace StoryViewer.ImageViewer
{
    public abstract class BasicContextOperation
    {
        public void Copy()
        {
        }
        public void Cut()
        {
        }
        public void Paste()
        {
        }
        public void Delete()
        {
        }

        public bool CanCopy { get; set; }

        public bool CanCut { get; set; }

        public bool CanPaste { get; set; }

        public bool CanDelete { get; set; }
        

    }
}
