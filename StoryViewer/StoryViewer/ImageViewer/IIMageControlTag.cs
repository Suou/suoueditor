﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using SuouBase.Book;
using SuouBase.Drawing;
namespace StoryViewer.ImageViewer
{
    public interface IIMageControlTag
    {
        Bitmap Create(Size size);
        Size OriginalSize { get; }
        bool IsNeedPreAsyncProcess { get; }
        void PreAsyncProcess();
        IIMageControlTag Copy();
        void DeterminImageData();
    }
    public class ImageTag : IIMageControlTag
    {
        public ImageTag(Bitmap bmp)
        {
            this.Image = bmp;
        }
        public Bitmap Image { get; private set; }
        public Size OriginalSize
        {
            get { return this.Image.Size; }
        }
        public Bitmap Create(Size size)
        {
            return BSystem.Drawing.EnlargeBmp.EnlargeBitmap(this.Image, size);            
        }
        public bool IsNeedPreAsyncProcess
        {
            get { return false; }
        }
        public void PreAsyncProcess()
        {
        }
        public IIMageControlTag Copy()
        {
            var bmp = (Bitmap)this.Image.Clone();
            var r = new ImageTag(bmp);
            return r;
        }

        public void DeterminImageData()
        {
        }
    }
    public class ImagePathTag : IIMageControlTag
    {
        public ImagePathTag(string imagePath)
        {
            this.ImagePath = imagePath;
        }
        public string ImagePath { get; private set; }
        private Size? _Size = null; 
        public Size OriginalSize
        {
            get
            {
                if (this._Size == null)
                {
                    var bmp = BSystem.Drawing.MainDrawingMethod.LoadBitmap(this.ImagePath);
                    this._Size = bmp.Size;
                }
                return this._Size.Value;
            }
        }
        public Bitmap Create(Size size)
        {
            var bmp = BSystem.Drawing.MainDrawingMethod.LoadBitmap(this.ImagePath);
            return BSystem.Drawing.EnlargeBmp.EnlargeBitmap(bmp, size);
        }
        public bool IsNeedPreAsyncProcess
        {
            get { return false; }
        }
        public void PreAsyncProcess()
        {
        }
        public IIMageControlTag Copy()
        {
            var r = new ImagePathTag(this.ImagePath);
            return r;
        }
        public void DeterminImageData()
        {
        }
    }
    public class ImageAATag : IIMageControlTag
    {
        public ImageAATag(IBookPage page)
        {
            this.Page = page;
            this.Page.EditStateChanged += this.Page_Changed;
        }
        public ImageAATag(SuouBase.Drawing.TextThumbImage img)
        {
            this.Page = img.Page.IBookPageCopy();
            this.Page.EditStateChanged += this.Page_Changed;
        }

        public virtual IIMageControlTag Copy()
        {
            var r = new ImageAATag(this.Page.IBookPageCopy());
            r.Padding = this.Padding;
            return r;
        }

        public IBookPage Page { get; private set; }
        public Padding Padding { get; set; }
        private Size? _Size { get; set; }
        public Size OriginalSize
        {
            get
            {
                if (this._Size == null) this._Size = this.Page.SText.Size;
                return this._Size.Value;
            }
        }
        public Bitmap Create(Size size)
        {
            var bmp = new Bitmap(size.Width, size.Height,System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            using (var g = Graphics.FromImage(bmp))
            {
                this.DrawText(g, Color.Black, Color.White,size);
            };
            return bmp;
        }
        public Bitmap CreateProportional(Size size)
        {
            if (size.Width == 0) size = new Size(1, size.Height);
            var bmp = new Bitmap(size.Width, size.Height, System.Drawing.Imaging.PixelFormat.Format16bppRgb555);
            using (var g = Graphics.FromImage(bmp))
            {
                this.DrawTextProportional(g, Color.Black, Color.White, size);
            };
            return bmp;
        }
        private void DrawText(Graphics g, Color textColor, Color backGround,Size size)
        {            
            g.DrawText(this.DecodeText(), this.Page.SText.YInterval, size, this.Page.SText.Size, backGround, textColor, new Point(this.Padding.Left, this.Padding.Top));
        }
        private void DrawTextProportional(Graphics g, Color textColor, Color backGround, Size size)
        {
            var page = this.Page.GetSTextWithoutInvokeEvent();
            g.DrawText(this.DecodeText(), page.YInterval, size, page.Size, backGround, textColor, new Point(this.Padding.Left, this.Padding.Top),true);
        }
        public SuouText DecodeText()
        {
            var v = this.Page.SText;
            v.Decode();
            return v;
        }
        public bool IsNeedPreAsyncProcess
        {
            get { return this._Size == null; }
        }
        public void PreAsyncProcess()
        {
            this._Size = this.Page.SText.Size;
        }
        private void Page_Changed(object sender, EventArgs e)
        {
            this._Size = null;
        }
        public void Dispose()
        {
            this.Page.EditStateChanged -= this.Page_Changed;
        }
        public void DeterminImageData()
        {
            this.Page.RefreshText();
        }
    }
    public class ImageSectionAATag : ImageAATag
    {
        public ImageSectionAATag(IBookPage page, SuouBookPageModel model):base(page)
        {
            this.Model = model;
        }
        
        public SuouBookPageModel Model { get; private set; }
        public override IIMageControlTag Copy()
        {
            var r = new ImageSectionAATag(this.Page.IBookPageCopy(),this.Model);
            r.Padding = this.Padding;
            return r;
        }
    }
}
