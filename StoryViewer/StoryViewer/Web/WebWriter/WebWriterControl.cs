﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using PreviewWindow;
using SuouBase.Book;
namespace StoryViewer.WebWriter
{
    [ToolboxItem(false)]
    public partial class WebWriterControl : UserControl
    {
        public WebWriterControl()
        {
            InitializeComponent();
            this.comboBox1.DisplayMember = "Title";
        }
        public Random Rand = new Random();
        #region Property
        bool IsLock = false;
        private string CheckingURL { get; set; }
        public long Interval
        {
            get
            {
                return ((int)this.numericUpDown1.Value) * 1000;
            }
        }
        public IBook Item
        {
            get { return this.iBookEViewer1.Item; }
            set 
            {
                this.iBookEViewer1.Item = value;
            }
        }
        private SitarabaPage SelectedPage
        {
            get
            {
                return this.comboBox1.SelectedItem as SitarabaPage;
            }
        }
        
        private void AddURL(string url,string title)
        {
            var p = new SitarabaPage() { URL = url, Title = title };
            this.comboBox1.Items.Add(p);

        }

        private bool IsCreatingURL { get; set; }
        #endregion
        #region Method
        public static Form EasyForm(IBook ib)
        {
            var ctrl = new WebWriterControl();
            ctrl.Item = ib;
            var v = new BSystem.Controls.SingleForm(ctrl,"");
            
            return v;
        }

        private void Touka()
        {
            var page = this.iBookEViewer1.TopNoCheckedPage;
            if (page == null) return;
            var v = new SitarabaWriter(this.SelectedPage.URL, page.SText.Text, this.NameBox.Text, this.MailBox.Text);
            var tex = v.Post();
            if (tex.IndexOf("ERROR") == -1)
            {
                this.iBookEViewer1.CheckTouka(this.iBookEViewer1.TopNoCheckItem);

                this.webBrowser1.DocumentText = tex;
                this.CheckNextPage();
                this.SetTimer(this.Interval + this.Rand.Next(10));
                
            }
            else
            {
                MessageBox.Show("ERROR!");
            }
        }
        private void CheckNextPage()
        {
            var p = this.iBookEViewer1.TopNoCheckedPage;
            if (p != null)
            {
                this.PageViewBrowser.DocumentText = p.CreateHTMLTag();
            }
        }
        private void SetTimer(long time)
        {
            this.countDownTimer1.StartTime = time;
            this.timer1.Interval = (int)time;
            this.timer1.Enabled = true;
        }
        #endregion


        
        #region Event
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.timer1.Enabled = false;
            if (this.AutoCheckBox.Checked)
            {
                this.Touka();
            }
            else
            {
                this.ToukaButton.Enabled = this.countDownTimer1.IsCountStop;
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.AutoCheckBox.Checked = false;
        }
        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            if (!this.IsLock)
            {
                this.IsLock = true;
                if (this.CheckingURL != null && this.IsCreatingURL)
                {
                    var title = this.webBrowser1.Document.Title;

                    var rst = SuouBase.MessageBoxExtension.CreateYesNoMessageBox("スレの設定", title + "でよろしいですか？");
                    if (rst != DialogResult.Yes)
                    {
                    }
                    else
                    {
                        var page = new SitarabaPage() { URL = this.CheckingURL, Title = title };
                        this.comboBox1.Items.Add(page);
                        this.comboBox1.SelectedItem = page;
                    }
                    this.CheckingURL = null;

                    this.IsCreatingURL = false;
                }
                this.IsLock = false;
            }
        }
        private void ToukaButton_Click(object sender, EventArgs e)
        {
            this.Touka();
        }

        private void iBookEViewer1_ItemSelected(object sender, EventArgs e)
        {
            var p = this.iBookEViewer1.SelectedPage;

            if (p != null)
            {
                this.PageViewBrowser.DocumentText = p.CreateHTMLTag();

            }
            else
            {
            }
            //this.bookPagePicture1.Page = p;
        }

        private void URLButton_Click(object sender, EventArgs e)
        {

            var v = new SuouBase.Controls.InputForm();
            var f = v.ShowDialog();
            if (f == DialogResult.OK)
            {

                this.IsCreatingURL = true;
                this.CheckingURL = v.Message;
                if (this.CheckingURL != null)
                {
                    this.webBrowser1.Navigate(this.CheckingURL);
                }
                else
                {
                    this.webBrowser1.Stop();
                    webBrowser1.Navigate("about:blank");
                }
            }
        }
        #endregion

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            var p = this.comboBox1.SelectedItem as SitarabaPage;
            
        }
    }
    public class SitarabaPage
    {
        public string URL { get; set; }
        public string Title { get; set; }
    }
}
