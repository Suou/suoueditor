﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace StoryViewer.WebWriter
{
    [ToolboxItem(false)]
    public partial class WebWriterInputBox : UserControl
    {
        public WebWriterInputBox()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var v = new SitarabaWriter(this.textBox1.Text, this.textBox2.Text,"","");
            var tex = v.Post();
            this.webBrowser1.DocumentText = tex;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
        }
        
    }

}
