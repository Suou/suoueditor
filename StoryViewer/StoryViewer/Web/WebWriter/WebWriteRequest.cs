﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace StoryViewer.WebWriter
{

    public class SitarabaWriter
    {
        public SitarabaURL Url { get; private set; }
        public string Text { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public SitarabaWriter(string url, string text,string name,string mail)
        {
            this.Url = new SitarabaURL(url);
            this.Text = text;
            this.Name = System.Web.HttpUtility.UrlEncode(name);
            this.Mail = System.Web.HttpUtility.UrlEncode(mail);
        }

        public string Post()
        {
            var postData = this.EncodeText();
            System.Net.WebClient wc = new System.Net.WebClient();
            //文字コードを指定する
            wc.Encoding = this.Url.Encode;

            //ヘッダにContent-Typeを加える
            wc.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
            //データを送信し、また受信する
            string resText = wc.UploadString(this.Url.WriteCgi, postData);
            
            wc.Dispose();
            return resText;
        }
        public string EncodeText()
        {
            var post = string.Format("{0}&NAME={1}&MAIL={2}&{3}&{4}&{5}&{6}&TIME=1346895305",
                this.URLEncode,this.Name,this.Mail,this.Message,this.Ganlre,this.BBS,this.Key);
            return post;
        }
        public string URLEncode
        {
            get
            {
                return System.Web.HttpUtility.UrlEncode("submit=%BD%F1%A4%AD%B9%FE%A4%E0&NAME=", this.Url.Encode);
            }
        }
        public string Key
        {
            get
            {
                return "KEY=" + this.Url.ThreadNumber;
            }
        }
        public string Ganlre
        {
            get
            {
                return "DIR=" + this.Url.Ganrle;
            }
        }
        public string BBS
        {
            get
            {
                return "BBS=" + this.Url.BoradNumber;
            }
        }
        public string Message
        {
            get
            {
                string s = "MESSAGE=" + System.Web.HttpUtility.UrlEncode(this.Text);
                return s;
            }
        }
    }

    public class SitarabaURL : Uri
    {
        public SitarabaURL(string url)
            : base(url)
        {
        }
        public string Ganrle
        {
            get
            {
                var v = this.Segments[3];
                return v.Substring(0, v.Length - 1);
            }
        }
        public string BoradNumber
        {
            get
            {
                var v = this.Segments[4];
                return v.Substring(0, v.Length - 1);
            }
        }
        public string ThreadNumber
        {
            get
            {
                var v = this.Segments[5];
                return v.Substring(0, v.Length - 1);
            }
        }
        public string WriteCgi
        {
            get
            {
                return "http://jbbs.livedoor.jp/bbs/write.cgi/";
            }
        }
        public Encoding Encode
        {
            get
            {

                System.Text.Encoding enc =
                    System.Text.Encoding.GetEncoding("euc-jp");
                return enc;
            }
        }
    }
}
