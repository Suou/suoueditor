﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.IO;
using System.Drawing.Drawing2D;
using SuouBase.Text;
using StoryViewer;
using StoryViewer.Web;
using SuouBase.Book;
namespace PreviewWindow
{
    //[ToolboxItem(false)]
    public partial class WebBrowserControl : UserControl
    {
        public WebBrowserControl()
        {
            InitializeComponent();
            //this.WebData = new WebBrowserClass(this.webBrowser1);
        }
        private IBook _Book = null;
        public IBook Book
        {
            get
            {
                return this._Book;
            }
            set
            {
                if (value == null) return;
                this._Book = value;
                this.oneToolStripMenuItem.Enabled = this.Book.VirtualSelectedPage != null;
                if (value is ObsoleteChapterBook)
                {
                    this.chapterMenubar1.Book = ((ObsoleteChapterBook)value).ToChapterBook2();
                    this.GrupeHtml();
                    
                }
                else if (value is ObsoleteSectionBook)
                {
                    this.webBrowser1.DocumentText = this.BrowserInfo.Create(value);
                }
                else if (value is EditingFile)
                {
                    this.webBrowser1.DocumentText = this.BrowserInfo.Create(value);
                }
            }
        }
        private IBookPage _BookPage = null;
        public IBookPage BookPage
        {
            get { return this._BookPage; }
            set
            {
                if (value != null)
                {
                    this._BookPage = value;
                    this.webBrowser1.DocumentText = this.BrowserInfo.Create(value);
                }
            }
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            var g = e.Graphics;
            Rectangle rec = new Rectangle(Point.Empty, this.menuStrip1.Size);
            LinearGradientBrush gb = new LinearGradientBrush(g.VisibleClipBounds, Color.LightGreen, Color.LightSkyBlue, LinearGradientMode.Horizontal);

            //四角を描く
            g.FillRectangle(gb, g.VisibleClipBounds);

        }
        public PreviewPlugin Plugin { get; set; }
        public EditorInfo DLLFunction
        {
            get
            {
                return this.Plugin.DLLFunction;
            }
        }
        
        public BookBrowser BrowserInfo
        {
            get { return this.Plugin.BrowserInfo; }
        }
        public void Preview()
        {
        }

        private void ClickAllButton(object sender, EventArgs e)
        {
            if (this.chapterMenubar1.Book != null)
            {
                this.webBrowser1.DocumentText = this.BrowserInfo.Create(PageType.All, this.chapterMenubar1.Book);
            }
            else
            {
                if (this.DLLFunction.SelectedStoryFile == null) return;
                this.webBrowser1.DocumentText = this.BrowserInfo.Create(PageType.All, this.DLLFunction.SelectedStoryFile);
            }
            
            //var tex = this.wbc.OutputEditingFileHtml(this.DLLFunction.SelectedStoryFile);
            //this.webBrowser1.DocumentText = this.wbc.OutputEditingFileHtml(this.DLLFunction.SelectedStoryFile);
        }

        private void ClickOneButton(object sender, EventArgs e)
        {
            this.OneHtml();
        }
        public void OneHtml()
        {
            if (this.DLLFunction.SelectedStoryFile == null) return;
            this.webBrowser1.DocumentText = this.BrowserInfo.Create(PageType.One, this.DLLFunction.SelectedStoryFile);
            //this.webBrowser1.DocumentText = this.wbc.OutputAstPageHtml(this.DLLFunction.SelectedStoryFile.VirtualSelectedPage);

        }
        public void GrupeHtml()
        {
            this.GrupeHtml(this.DLLFunction.SelectedStoryFile);
        }
        public void GrupeHtml(IBook book)
        {
            if (this.DLLFunction.SelectedStoryFile == null) return;
            this.webBrowser1.DocumentText = this.BrowserInfo.Create(PageType.Groupe, book);
        }

        private void grupeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.GrupeHtml();
            //this.webBrowser1.DocumentText = this.wbc.OutputBookPagesHtml(this.DLLFunction.SelectedStoryFile.VirtualSelectedGrupePages);
        }

        private void chapterMenubar1_ItemSelected(SectionNBook arg1, EventArgs arg2)
        {
            this.GrupeHtml(arg1);
        }

        private void debug1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.chapterMenubar1.Book = ((ObsoleteChapterBook)this.Book).ToChapterBook2();

        }

    }

}
