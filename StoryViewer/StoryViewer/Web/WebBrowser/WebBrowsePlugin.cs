﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;
using PreviewWindow;
using SuouBase.Expansion;
using SuouBase;
using StoryViewer.Web;
using System.Drawing;
using SuouBase.Book;
namespace StoryViewer
{
    public class PreviewPlugin : IPreviewClass, ITabPlugin
    {
        public Size CompactSize
        {
            get
            {
                return new Size(500, 500);
            }
        }
        public BookBrowser BrowserInfo = new BookBrowser();
        private string SaveFileName = SuouEditorConst.GetSettingFile("Preview.stn");
        private WebBrowserForm _Form;
        public Form Form 
        { 
            get 
            {
                if (this._Form != this._Control.ParentForm)
                {
                    this._Form.Controls.Add(this._Control);
                    this._Control.Dock = DockStyle.Fill;
                }
                return _Form; 
            } 
        }
        public void TabClick()
        {
            
            if (this.DLLFunction.FileData.SelectData == null)
            {
            }
            else
            {
                if (this.DLLFunction.SelectedStoryFile != this._Control.Book && this.DLLFunction.SelectedStoryFile is ObsoleteChapterBook)
                {
                    this._Control.Book = this.DLLFunction.SelectedStoryFile;
                }

                if (this.DLLFunction.DisplayType == EditorDisplay.PageViewer)
                {
                    this._Control.GrupeHtml();
                }
                else if (this.DLLFunction.DisplayType == EditorDisplay.TextEdit)
                {
                    this._Control.OneHtml();
                }
            }
            
        }
        public void SetItem(IBook book)
        {
            this._Control.Book = book;
            this.TabClick();

        }
        public void SetPageItem(IBookPage bookpage)
        {
            this._Control.BookPage = bookpage;
        }

        private WebBrowserControl _Control;
        public ContainerControl Control { get { return this._Control; } }
        public string Name { get { return "プレビュー"; } }
        public EditorInfo DLLFunction { get; set; }
        public PreviewPlugin(EditorInfo dLLFunction):this()
        {
            this.DLLFunction = dLLFunction;
            this._Control.Plugin = this;
        }
        public bool DisposeCheck()
        {
            BSystem.XML.NonBinarySerializer.Save(this.SaveFileName,this._Form.Setting);
            return true;
        }
        public SettingPage[] SettingPage
        {
            get
            {
                return null;
            }
        }

        public PreviewPlugin()
        {
            _Form = new WebBrowserForm();
            this._Control = new WebBrowserControl();

            var setting = BSystem.XML.NonBinarySerializer.Load<BSystem.Controls.FormSaveData>(this.SaveFileName);
            if (setting != null) this._Form.Setting = setting;
        }
    }
}
