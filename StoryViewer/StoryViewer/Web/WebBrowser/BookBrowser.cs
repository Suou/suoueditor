﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using RazorEngine;
using System.IO;
using BSystem;
using BSystem.Collections;
using SuouBase.Book;
namespace StoryViewer.Web
{
    public class BookBrowser
    {
        //private IBook Item { get; set; }

        public BookBrowser()
        {
            //this.Item = book;
            Load();
        }

        public static void Load()
        {
            BookBrowser.Template = BSystem.IOLibrary.Load(StoryViewer.Properties.Resources.BrowserTamplatePath);
        }
        public static string Template { get; set; }

        public static string CreateHtml(IBook book, string tmp)
        {
            BookBrowser.Template = BSystem.IOLibrary.Load(StoryViewer.Properties.Resources.BrowserTamplatePath);
            var pages = BookBrowser.Translate(book.OutputPages).ToArray();
            var result = Razor.Parse(Template, pages);
            return result;
        }
        public static System.Windows.Forms.Form Browse(IBook book)
        {
            var tmp = "Data/Template/BrowserTemplate.cshtml";
            var html = CreateHtml(book, tmp);
            var con = new System.Windows.Forms.WebBrowser();
            con.DocumentText = html;
            var form = new BSystem.Controls.SingleForm(con, "プレビュー");
            return form;
        }

        public string Create(PageType type,IBook item)
        {
            switch (type)
            {
                case PageType.All:
                    return this.Create(item);
                case PageType.Groupe:
                    return this.Create(item.VirtualSelectedGrupePages);
                case PageType.One:
                    return this.Create(item.VirtualSelectedPage);
                default:
                    return null;
            }

        }
        public string Create(IBook item)
        {
            var pages = BookBrowser.Translate(item.OutputPages).ToArray();
            var result = Razor.Parse(Template, pages);
            return result;
        }
        public string Create(IBookPage item)
        {
            var pitem = item;
            if (item is ObsoleteChapterBookPage)
            {
                pitem = ((ObsoleteChapterBookPage)item).Items.VirtualSelectedPage;

            }
            var pages = BookBrowser.Translate(pitem).ToArray();
            var result = Razor.Parse(Template, pages);
            return result;
        }
        public string Create(IEnumerable<IBookPage> items)
        {
            var pages = BookBrowser.Translate(items).ToArray();
            var result = Razor.Parse(Template, pages);
            return result;
        }
        /*
        public static SuouText[] Translate(IBook item)
        {
            return item.VirtualBookPages.Select((v) => v.Text).ToArray();
        }
        */
        public static IEnumerable<BookPageRes> Translate(IEnumerable<IBookPage> items)
        {
            int n = 1;
            return items.Select((v) => new BookPageRes(n++,v.SText));
        }
        public static IEnumerable<BookPageRes> Translate(IBookPage item)
        {
            //int n = 1;
            return new[] {new BookPageRes(1, item.SText)};
        }
    }
    public enum PageType
    {
        All,Groupe,One
    }
    public class BookPageRes
    {
        public int Number { get; private set; }
        public string[] Text { get; private set; }
        public BookPageRes(int num,SuouText tex)
        {
            //var split = System.Web.HttpUtility.("<br/>" + System.Environment.NewLine);
            this.Number = num;
            this.Text = tex.Lines.Select((v) => v.Sentence).ToArray();
            //this.Text = String.Join(split, lines);
            
        }

    }
    
}
