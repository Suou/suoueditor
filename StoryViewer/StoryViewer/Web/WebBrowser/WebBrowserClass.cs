﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.IO;
using SuouBase.Text;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Book;
namespace PreviewWindow
{

    public class WebBrowserClass
    {
        public string Template;
        public WebBrowserClass()
        {
            this.Read();
            //this.WebControl = ctrl;
        }
        public bool Read()
        {
            StreamReader sr;
            try
            {
                sr = new StreamReader(SuouEditorConst.GetDataFile("WebBrowser.html"), Encoding.GetEncoding("Shift_JIS"));
                Template = sr.ReadToEnd();
                sr.Close();
                IsOK = true;
            }
            catch
            {
                IsOK = false;
            }
            finally
            {

            }
            return IsOK;
        }
        private bool IsOK { get; set; }


        public string OutputAstPageHtml(IBookPage page)
        {
            if (page == null) return "";
            var v = SuouTextWebExtension.Kakou(page);

            var grp = new Antlr3.ST.StringTemplate(Template);
            grp.SetAttribute("Pages", v);
            return grp.ToString();

        }
        public string OutputEditingFileHtml(IBook book)
        {
            if (book == null) throw new Exception();
            var v = SuouTextWebExtension.Kakou(book);

            var grp = new Antlr3.ST.StringTemplate(Template);
            grp.SetAttribute("Pages", v);
            return grp.ToString();
        }
        public string OutputBookPagesHtml(IEnumerable<IBookPage> pages)
        {
            if (pages == null) return "";
            var v = SuouTextWebExtension.Kakou(pages.ToList());

            var grp = new Antlr3.ST.StringTemplate(Template);
            grp.SetAttribute("Pages", v);
            return grp.ToString();
        }
    }

    public static class SuouTextWebExtension
    {
        public static string CreateHTMLTag(this IBookPage page)
        {
            var v = new WebBrowserClass();
            v.Read();
            return v.OutputAstPageHtml(page);
        }
        public static string CreateHTMLTag(this IBook book)
        {
            var v = new WebBrowserClass();
            v.Read();
            return v.OutputEditingFileHtml(book);
        }
        public static List<ViewAstPage> Kakou(IBookPage ap)
        {
            var v = new ViewAstPage(ap);

            var vv = new List<ViewAstPage>();
            vv.Add(v);
            return vv;
        }
        public static List<ViewAstPage> Kakou(IEnumerable<IBookPage> ap)
        {
            //var v = new ViewAstPage(ap);

            var vv = new List<ViewAstPage>();
            ap.Select((p) => new ViewAstPage(p)).ForEach((p) => vv.Add(p));
            //vv.Add(new ViewAstPage());
            return vv;
        }

        public static List<ViewAstPage> Kakou(IBook ef)
        {

            var vv = new List<ViewAstPage>();
            foreach (IBookPage ap in ef.OutputPages)
            {
                var v = new ViewAstPage(ap);
                vv.Add(v);
            }

            return vv;
        }
    }
}
