﻿namespace PreviewWindow
{
    partial class WebBrowserControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.allToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oneToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.grupeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.chapterMenubar1 = new StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.ChapterMenubar();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(0, 30);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(461, 372);
            this.webBrowser1.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.allToolStripMenuItem,
            this.oneToolStripMenuItem,
            this.grupeToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 4);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(461, 26);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            this.menuStrip1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // allToolStripMenuItem
            // 
            this.allToolStripMenuItem.Name = "allToolStripMenuItem";
            this.allToolStripMenuItem.Size = new System.Drawing.Size(80, 22);
            this.allToolStripMenuItem.Text = "全てを表示";
            this.allToolStripMenuItem.Click += new System.EventHandler(this.ClickAllButton);
            // 
            // oneToolStripMenuItem
            // 
            this.oneToolStripMenuItem.Name = "oneToolStripMenuItem";
            this.oneToolStripMenuItem.Size = new System.Drawing.Size(140, 22);
            this.oneToolStripMenuItem.Text = "現在のページのみ表示";
            this.oneToolStripMenuItem.Click += new System.EventHandler(this.ClickOneButton);
            // 
            // grupeToolStripMenuItem
            // 
            this.grupeToolStripMenuItem.Checked = true;
            this.grupeToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.grupeToolStripMenuItem.Name = "grupeToolStripMenuItem";
            this.grupeToolStripMenuItem.Size = new System.Drawing.Size(104, 22);
            this.grupeToolStripMenuItem.Text = "グループで表示";
            this.grupeToolStripMenuItem.Click += new System.EventHandler(this.grupeToolStripMenuItem_Click);
            // 
            // chapterMenubar1
            // 
            this.chapterMenubar1.AllowDrop = true;
            this.chapterMenubar1.BackColor = System.Drawing.Color.PaleVioletRed;
            this.chapterMenubar1.Book = null;
            this.chapterMenubar1.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            this.chapterMenubar1.Location = new System.Drawing.Point(0, 0);
            this.chapterMenubar1.Name = "chapterMenubar1";
            this.chapterMenubar1.Size = new System.Drawing.Size(461, 4);
            this.chapterMenubar1.TabIndex = 3;
            this.chapterMenubar1.Text = "chapterMenubar1";
            this.chapterMenubar1.ItemSelected += new System.Action<SuouBase.Book.SectionNBook, System.EventArgs>(this.chapterMenubar1_ItemSelected);
            // 
            // WebBrowserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.chapterMenubar1);
            this.Name = "WebBrowserControl";
            this.Size = new System.Drawing.Size(461, 402);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem allToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oneToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem grupeToolStripMenuItem;
        private StoryViewer.BookEditor.ChapterViewer.BookGrouperViewerF.ChapterMenubar chapterMenubar1;
    }
}
