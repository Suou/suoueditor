﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using System.IO;
using SuouBase;
using System.Web;
using SuouBase.Book;
namespace StoryViewer.WebYaruoAAViewer
{
    [ToolboxItem(false)]
    public partial class FileUpgradeBox : UserControl
    {
        public FileUpgradeBox()
        {
            InitializeComponent();
        }
        private IBook _UpgradeItem = null;
        public IBook UpgradeItem
        {
            get { return this._UpgradeItem; }
            set
            {
                this._UpgradeItem = value;

                this.textBox2.Text = this.UpgradeItem.FullFilePath;
            }
        }
        public IBook NewItem { get; set; }
        public string url = "http://yaruo.b4t.jp/index.php?id=Hukutemp/";
        public string CreateNewUrl()
        {
            var v = new FileInfo(this.UpgradeItem.FullFilePath);
            var p = PathExtension.GetSuffixPath(v, "HukuTemp");
            var g = String.Join("/", p.Select((c) => HttpUtility.UrlEncode(c)));
            return Path.Combine(url, g);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            this.textBox1.Text = this.CreateNewUrl();
            this.NewItem = YaruoAA2SpanItem.Load(this.textBox1.Text);
            this.aaThumContViewControl1.Item = MergeBookCollection.CreateSabunBook(this.UpgradeItem, this.NewItem);
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
        
    }
}
