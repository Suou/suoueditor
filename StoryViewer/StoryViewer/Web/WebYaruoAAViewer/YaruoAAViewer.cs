﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WebExtension;
using SuouBase.Text;
using System.Net;
using SuouBase;
namespace StoryViewer.WebYaruoAAViewer
{
    [ToolboxItem(false)]
    public partial class YaruoAAViewer : UserControl
    {
        public YaruoAAViewer()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var url = this.textBox1.Text;
            this.aaThumContViewControl1.Item = YaruoAA2SpanItem.Load(url);
        }
        
    }

}
