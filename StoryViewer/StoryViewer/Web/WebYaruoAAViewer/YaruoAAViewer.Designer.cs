﻿namespace StoryViewer.WebYaruoAAViewer
{
    partial class YaruoAAViewer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.aaThumContViewControl1 = new StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // aaThumContViewControl1
            // 
            this.aaThumContViewControl1.DataBook = null;
            //this.aaThumContViewControl1.DLLFunction = null;
            this.aaThumContViewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.aaThumContViewControl1.EditLock = true;
            this.aaThumContViewControl1.EnableNameTextBox = StoryViewer.NameTextBoxType.None;
            this.aaThumContViewControl1.IsTransparentDrag = false;
            this.aaThumContViewControl1.Item = null;
            this.aaThumContViewControl1.Location = new System.Drawing.Point(0, 71);
            this.aaThumContViewControl1.ManualDrag = false;
            this.aaThumContViewControl1.Name = "aaThumContViewControl1";
            this.aaThumContViewControl1.PermitCategory = true;
            this.aaThumContViewControl1.Size = new System.Drawing.Size(394, 299);
            this.aaThumContViewControl1.TabIndex = 0;
            this.aaThumContViewControl1.UseContext = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(394, 71);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(299, 28);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(18, 28);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(275, 19);
            this.textBox1.TabIndex = 0;
            // 
            // YaruoAAViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.aaThumContViewControl1);
            this.Controls.Add(this.groupBox1);
            this.Name = "YaruoAAViewer";
            this.Size = new System.Drawing.Size(394, 370);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private StoryViewer.BookControl.Display.SingleBookDisplay.SingleBookDisplay aaThumContViewControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
    }
}
