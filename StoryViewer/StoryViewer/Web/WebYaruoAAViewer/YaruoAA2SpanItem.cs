﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WebExtension;
using SuouBase.Text;
using System.Net;
using SuouBase;
using SuouBase.Book;
namespace StoryViewer.WebYaruoAAViewer
{

    public class YaruoAA2SpanItem : DynamicHtmlNode
    {
        public YaruoAA2SpanItem(DynamicHtmlNode hn)
            : base(hn.Item)
        {
        }
        public string _id
        {
            get { return this.GetAttributes("_id"); }
        }
        public string _title
        {
            get { return this.GetAttributes("_title"); }
        }
        public IBookPage BookPage
        {
            get
            {
                //var text = this.ContainsText;
                var text = WebUtility.HtmlDecode(this.ContainsText);
                var title = this._title;
                var p = new AstPage(title, text);
                return p;
            }
        }
        public static IBook Load(string url)
        {
            var p = new WebText(url);
            var dhn = new DynamicHtmlNode(p.ParseTree.DocumentNode);
            var gpn = dhn.Descendant.Where((v) => v.NodeType == HtmlTagNodeType.Span).Where((v) => ((dynamic)v)._id != null && ((dynamic)v)._title != null).Select((v) => new YaruoAA2SpanItem(v));
            var PageItems = gpn.Select((v) => v.BookPage).ToList();
            return new EditingFile(PageItems);
        }
    }
}
