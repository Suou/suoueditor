﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using BSystem;
using BSystem.Text;
namespace WebExtension
{
    public class WebText
    {
        public string Text { get; set; }
        public Uri URL { get; set; }
        public HtmlDocument ParseTree
        {
            get
            {
                var Tree = new HtmlAgilityPack.HtmlDocument();
                Tree.LoadHtml(this.Text);
                return Tree;
            }
        }
        public string ParseText
        {
            get
            {
                return this.ParseTree.GetParse();
            }
        }

        public WebText(string text,Uri url)
        {
            this.Text = text;
            this.URL = url;
        }
        public WebText(string uri)
        {
            //var f = new Uri(uri);
            var p = WebAnalyzer.GetText(uri);
            this.Text = p.Text;
            this.URL = p.URL;

            this.Text = String.Join(System.Environment.NewLine,this.Text.LineSprit());
            var pf = this.Text.LineSprit();
            Console.WriteLine(pf.Length);
        }

        public bool Save(string basefilename)
        {
            var v = new URLAnalyzer(this.URL.AbsoluteUri, this.URL.AbsoluteUri);
            if (!v.IsCompleteUrl || !v.NeedSave) return false;
            v.CreateSaveFolder(basefilename);
            WriterExtensions.WriteLine(this.ParseText, basefilename+"/" + v.FilePath);
            return true;
        }
        public IEnumerable<string> GetUrlItems()
        {
            var https = WebPageGet.GetHttps(this.Text, this.URL);

            return https.Where((v)=>!v.IsInvalidURL).Select((v) => v.Url);
        }
    }

    public class URLCollection : Dictionary<string, bool>
    {
        public string RandPick()
        {
            return this.Where((v) => v.Value == false).First().Key;
        }
        public void Add(string url)
        {
            var b = Uri.IsWellFormedUriString(url, UriKind.Absolute);
            if (b)
            {
                if (!this.ContainsKey(url)) this[url] = false;
            }
        }
        public URLCollection(string url)
        {
            this.Add(url);
        }
    }
    public static class WebAnalyzer
    {
        public static WebText GetText(this string url)
        {
            Uri uri = null;
            var b = Uri.IsWellFormedUriString(url, UriKind.Absolute);
            if (b)
            {
                uri = new Uri(url);
                var text = WebPageGet.DownLoad(url);
                if (text == null) return null;
                var webtext = new WebText(text, uri);
                return webtext;
            }
            else
            {
                throw new Exception();
            }
        }
        
    }
}
