﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Dynamic;
using System.Xml.Linq;
using HtmlAgilityPack;

namespace WebExtension
{
    public enum HtmlTagNodeType
    {
        Text,Br,P,Div,Img,Other,Span
    }


    public class DynamicHtmlNode : DynamicObject
    {
        public HtmlNode Item { get; private set; }
        public DynamicHtmlNode(HtmlNode node)
        {
            this.Item = node;
        }
        public IEnumerable<DynamicHtmlNode> ChildNodesIterator
        {
            get
            {
                return this.Item.ChildNodes.Select(x => new DynamicHtmlNode(x));
            }
        }
        public static HtmlTagNodeType GetHtmlTagNodeType(HtmlNode node)
        {
            if (node.NodeType == HtmlNodeType.Text)
            {
                return HtmlTagNodeType.Text;
            }
            else if (node.Name == "Br")
            {
                return HtmlTagNodeType.Br;
            }
            else if (node.Name == "p")
            {
                return HtmlTagNodeType.P;
            }
            else if (node.Name == "div")
            {
                return HtmlTagNodeType.Div;
            }
            else if (node.Name == "span")
            {
                return HtmlTagNodeType.Span;
            }
            else
            {
                return HtmlTagNodeType.Other;
            }
        }
        public static IEnumerable<DynamicHtmlNode> GetChildren(DynamicHtmlNode item)
        {
            var v = item.ChildNodesIterator;
            yield return item;
            var p = v.SelectMany((w) => GetChildren(w));
            foreach (var q in p) { yield return q; }

        }
        public IEnumerable<DynamicHtmlNode> Descendant
        {
            get
            {
                return GetChildren(this);
            }
        }
        public bool IsEnd
        {
            get
            {
                return this.Item.ChildNodes.Count == 0;
            }
        }
        public string Name
        {
            get
            {
                return this.Item.Name;
            }
        }
        public HtmlTagNodeType NodeType
        {
            get { return DynamicHtmlNode.GetHtmlTagNodeType(this.Item); }
        }
        public string ContainsText
        {
            get
            {
                var ef = this.Descendant.Where((v) => v.NodeType == HtmlTagNodeType.Text).Select((v) => v.Text);
                var gf = ef.Where((v) => v != null);
                return String.Join(System.Environment.NewLine, gf);
            }
        }
        public string Text
        {
            get
            {

                if (this.Item.NodeType == HtmlNodeType.Text)
                {
                    return this.Item.InnerText;
                }
                else
                {
                    return null;
                }
            }
        }

        public void WriteLine()
        {
            foreach (var v in this.ChildNodesIterator)
            {
                Console.WriteLine(v.Item.Name);
                v.WriteLine();
            }
        }
        public override bool TryInvokeMember(System.Dynamic.InvokeMemberBinder binder, object[] args, out object result)
        {
            switch (binder.Name)
            {
                case "GetEnumerator": // IEnumerable へのキャストと同様の理由。
                    result = new[] { this }.GetEnumerator();
                    return true;

                case "All": // All() 呼び出しで、子要素を全部取得できるようにする。
                    result = this.ChildNodesIterator.ToList();
                    return true;

                case "Name": // Name() で要素名を取得。
                    result = this.Item.Name.ToString();
                    return true;
            }

            return base.TryInvokeMember(binder, args, out result);
        }

        public string GetAttributes(string name)
        {
            var attName = name.Substring(1);
            var att = this.Item.Attributes[attName];
            if (att == null) return null;
            return att.Value;
        }
        public override bool TryGetMember(System.Dynamic.GetMemberBinder binder, out object result)
        {
            var name = binder.Name;

            // 属性値は _属性名 で取得。文字列として返す。
            if (name.StartsWith("_"))
            {
                result = this.GetAttributes(name);
                return true;
            }

            var subElements = this.Item.Elements(name).ToList();

            // 要素がないときは null 返す。
            if (subElements.Count == 0)
            {
                result = (string)null;
                return true;
            }

            // 要素が1個だけの時は素直にその要素を返す。
            if (subElements.Count == 1)
            {
                var e = subElements[0];

                result = new DynamicHtmlNode(this.Item.Element(name));

                return true;
            }

            // 要素が複数ある時はリストで要素一覧を返す。
            result = this.Item.Elements(name).Select((v) => new DynamicHtmlNode(v)).ToList();
            return true;
        }
    }

}
