﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using HtmlAgilityPack;
using BSystem.Collections;
namespace WebExtension
{
    public static class TreeExtension
    {
        public static void Set(this TreeView tree, HtmlAgilityPack.HtmlDocument doc)
        {
            var f = new TreeNode("root");
            tree.Nodes.Add(f);
            Set(f, doc.DocumentNode);
            
        }
        public static void Set(TreeNode tree, HtmlNode doc)
        {
            foreach (var node in doc.ChildNodes)
            {
                var f = new TreeNode(node.Name);
                f.Tag = node;
                tree.Nodes.Add(f);
                Set(f, node);
            }
            foreach (HtmlAttribute tag in doc.Attributes)
            {
                
                var f = new TreeNode("_"+tag.Name);
                f.Tag = tag;
                tree.Nodes.Add(f);
                
            }
        }
    }
    
    public static class StringExtension
    {
        public static string ToSjis10String(this string s)
        {
            return HTTPEncode.ToEncodeSJIS10(s);
        }
        public static string ToSjis16String(this string s)
        {
            return HTTPEncode.ToEncodeSJIS16(s);
        }
        public static string ToJoinString(this IEnumerable<char> v){
            StringBuilder sb = new StringBuilder("");
            v.ToList().ForEach((c) => sb.Append(c));
            return sb.ToString();
            
        }
        
        
    }

    public static class HTTPEncode
    {
        private static Dictionary<char, string> Unicode10Dic = new Dictionary<char, string>();
        private static Dictionary<char, string> Unicode16Dic = new Dictionary<char, string>();
        private static Dictionary<char, bool> BoolDic = new Dictionary<char, bool>();
        static Encoding enc = Encoding.GetEncoding("Shift_JIS");
        static Encoding unienc = Encoding.GetEncoding("utf-8");

        private static bool IsNeedEncode(char c)
        {
            if (!BoolDic.ContainsKey(c))
            {
                byte[] sjisresult = enc.GetBytes(c.ToString());
                byte[] uniresult = unienc.GetBytes(c.ToString());
                string sjisstr = enc.GetString(sjisresult);
                string unistr = unienc.GetString(uniresult);
                char c1 = sjisstr.ToCharArray().First();
                char c2 = unistr.ToCharArray().First();
                BoolDic[c] = !(c1 == c2);

            }
            return BoolDic[c];
        }
        public static bool ContainReferenceCharPossible(this string text)
        {
            return text.Contains('&');
        }
        public static string ToEncodeSJIS10(this string text)
        {
            StringBuilder s = new StringBuilder();
            foreach (char c in text)
            {
                if (IsNeedEncode(c))
                {
                    s.Append(Unicode10Dic[c]);
                }
                else
                {
                    s.Append(c);
                }
            }
            return s.ToString();
        }
        public static string ToEncodeSJIS10(this char c)
        {
            return ToEncodeSJIS10(c.ToString());
        }
        public static string ToEncodeSJIS16(this string text)
        {
            StringBuilder s = new StringBuilder();
            foreach (char c in text)
            {
                if (IsNeedEncode(c))
                {
                    s.Append(Unicode16Dic[c]);
                }
                else
                {
                    s.Append(c);
                }
            }
            return s.ToString();
        }
        public static string ToEncodeSJIS16(this char c)
        {
            return ToEncodeSJIS16(c.ToString());
        }

        private static string Uni10Encoder(int n)
        {
            string s = n.ToString();
            for (int i = 0; i < 4 - s.Length; i++) s = "0" + s;
            s = "&#" + s + ";";
            return s;
        }
        private static string Uni16Encoder(int n)
        {
            string s = n.ToString("X");
            for (int i = 0; i < 4 - s.Length; i++) s = "0" + s;
            s = "&#x" + s + ";";
            return s;
        }

        public static string DecodeToSjis(this string s)
        {
            var str = System.Web.HttpUtility.HtmlDecode(s);
            return str;
        }

        static HTTPEncode()
        {
            Kiroku();
        }

        private static void Kiroku()
        {
            for (int i = 0; i < 65535; i++)
            {
                string str = Uni10Encoder(i);
                string str2 = Uni16Encoder(i);

                char c = System.Web.HttpUtility.HtmlDecode(str).ToCharArray()[0];

                Unicode10Dic[c] = str;
                Unicode16Dic[c] = str2;
            }
        }

        private static void Write()
        {

            foreach (KeyValuePair<char, string> kvp in Unicode10Dic)
            {
                Console.Write(kvp.Key + "/" + kvp.Value);
            }


        }
    }
}
