﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using BSystem.Text.Encoding;
using BSystem.Text.Japanese;
namespace WebExtension
{
    
    /// <summary>
    /// URL解析
    /// </summary>
    public class URLAnalyzer
    {
        public string ParentUrl { get; private set; }
        public string UrlText { get; private set; }

        public string CompleteUrl;
        public string HostName;
        public string TailName;
        public bool IsSameHost;
        public bool IsCompleteUrl;
        public bool IsMiniUrl;

        public Uri Uri { get; private set; }
        public URLAnalyzer(string parentUrl,string urlText)
        {
            this.ParentUrl = parentUrl;
            this.UrlText = urlText;
            var httpreg = new Regex("http://");
            this.IsCompleteUrl = httpreg.IsMatch(this.UrlText);
            if (this.IsCompleteUrl)
            {
                this.CompleteUrl = this.UrlText;
                this.Uri = new System.Uri(this.CompleteUrl);
            }
            else
            {
            }
        }
        public void CreateSaveFolder(string basedir)
        {
            BSystem.IODirectory.CreateFolder(this.FilePathSegments.Take(this.FilePathSegments.Count() - 1).ToArray(), basedir);
        }
        public string FilePath
        {
            get
            {
                return String.Join("/", this.FilePathSegments);
            }
        }
        private string[] FilePathSegments
        {
            get
            {
                var paths = this.PathSegments.Select((v) => v.Replace('?', '-')).ToArray();
                var dics = paths.Take(paths.Length - 1);
                var filename = paths.Last();
                if (filename.Length == 0) filename = "index.html";

                if (Path.HasExtension(filename))
                {
                    return dics.Append(filename).ToArray();
                }
                else
                {
                    return dics.Append(filename + ".html").ToArray();
                }
            }
        }
        private string[] PathSegments
        {
            get
            {
                var n = this.Uri.GetLeftPart(UriPartial.Scheme).Length;
                var s = this.Uri.AbsoluteUri.Substring(n);
                var f = s.Split('/');
                return f;
            }
        }

        public bool NeedSave
        {
            get
            {
                return (this.Uri.Query.Length == 0);
            }
        }

    }
    
    /// <summary>
    /// URLの詳細情報
    /// </summary>
    public class HrefHttp
    {
        public string Url;
        public string Title;
        public string Extension;
        public Uri Uri { get; set; }
        public bool IsInvalidURL { get; set; }
        public bool IsLive { get; set; }
        public HrefHttp(string url, string title)
        {
            this.Url = url;
            this.Title = title;
            this.IsInvalidURL = false;
            try
            {
                
                this.Extension = Path.GetExtension(url);
            }
            catch (System.ArgumentException e)
            {
                System.Console.WriteLine(e);
                this.IsLive = true;
            }
            finally
            {
            }
        }

       
        

    }
    public static class HtmlNodeExtension
    {
        public static string GetParse(this HtmlDocument tree)
        {
            StringBuilder sb = new StringBuilder();
            var itr = tree.Iterator().Where((p) => p.NodeType == HtmlNodeType.Text).Select((p => p.InnerText.DeleteInvisibleChar())).Where((p) => p.Length != 0);
            foreach (var v in itr)
            {
                sb.Append(v);
                //Console.WriteLine(((int)v[0]) + "/" + v.Length);
                sb.Append(System.Environment.NewLine);

            }
            return sb.ToString();
        }
        public static IEnumerable<HtmlNode> Iterator(this HtmlDocument Tree)
        {
            foreach (var v in Tree.DocumentNode.ChildNodes)
            {
                if (v.ChildNodes.Count != 0)
                {
                    foreach (var w in HtmlNodeExtension.Iterator2(v))
                    {
                        yield return w;
                    }
                }
                else
                {
                    yield return v;
                }
            }
        }
        public static IEnumerable<HtmlNode> Iterator2(HtmlNode node)
        {

            if (node.Name != "script" && node.Name != "noscript")
            {
                if (node.ChildNodes.Count != 0)
                {
                    foreach (var v in node.ChildNodes)
                    {
                        foreach (var w in HtmlNodeExtension.Iterator2(v))
                        {
                            yield return w;
                        }
                    }
                }
                else
                {
                    yield return node;
                }
            }
        }
    }
    public class WebPageGet
    {

        public void Get()
        {
            string uri = "http://dobon.net/vb/dotnet/string/ishiragana.html";
            WebClient wc = new WebClient();
            WebRequest req = WebRequest.Create(uri);
            Stream st = wc.OpenRead(uri);

            StreamReader sr = new StreamReader(st, Encoding.GetEncoding("Shift_JIS"));
            var s = sr.ReadToEnd();
            var arr = s.GetJapanese().ToList();
            var strs = String.Join(System.Environment.NewLine, arr);
            var url = new Uri(uri);
            var hr = WebPageGet.GetHttps(s, url);
            //hr.ToList().ForEach((v) => Console.WriteLine(v.Title + "___" + v.Url));
            sr.Close();
            st.Close();
        }
        public static string DownLoad(string uri)
        {
            WebClient wc = new WebClient();
            try
            {
                WebRequest req = WebRequest.Create(uri);

            }
            catch (NotSupportedException e)
            {
                System.Console.WriteLine(e);

                return null;
            }
            finally
            {
            }
            Stream st = null;
            StreamReader sr = null;
            string s = null;
            try
            {
                st = wc.OpenRead(uri);
                //sr = new StreamReader(st, Encoding.UTF8);

                s = BSystem.WebExtension.ReadtoEnd(st);
                //s = BaseExtension.EncodeExtension.ChagneEncode(BaseExtension.EncodeExtension.EUCJPEncode,Encoding.UTF8,s);
            }
            catch (WebException e)
            {
                System.Console.WriteLine(e);

            }
            finally
            {
                if (sr != null) sr.Close();
                if (st != null) st.Close();
            }
            return s;
        }
        public static void Save(string s, string filename)
        {
            StreamWriter sw = new StreamWriter(filename, true, System.Text.Encoding.GetEncoding("shift_jis"));
            sw.Write(s);
            sw.Close();
        }
        public static IList<HrefHttp> GetHttps(string textstr, Uri baseURL)
        {
            //var urler = new Uri(baseURL);
            List<HrefHttp> r = new List<HrefHttp>();
            //string hreffilter = "<a href=\"(?<url>http.*?)\".*?>(?<text>.*?)</a>";
            string simplehrefFilter = "<a href=\"(?<url>.*?)\".*?>(?<text>.*?)</a>";
            Regex reg = new Regex(simplehrefFilter,
                 RegexOptions.IgnoreCase | RegexOptions.Singleline);

            for (Match m = reg.Match(textstr); m.Success; m = m.NextMatch())
            {
                string url = m.Groups["url"].Value;
                string text = m.Groups["text"].Value;
                if (url.Length > 1 && (url.Substring(0, 1) == "." || url.Substring(0, 1) == "/"))
                {
                    var f = new Uri(baseURL, url);
                    r.Add(new HrefHttp(f.AbsoluteUri, text));
                }
                else
                {
                    r.Add(new HrefHttp(url, text));
                }
            }
            return r;

        }

    }
}
