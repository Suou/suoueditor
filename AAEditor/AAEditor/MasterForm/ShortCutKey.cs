﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using System.Windows.Forms;

namespace SuouEditor
{
    [Serializable]
    public class MasterFormMenuShortCutKeys : SuouBase.Interface.ShortCutCommandCollection
    {
        /*
        public ShortCutCommand FileManu = new ShortCutCommand("ファイル") { IsAlt = true, NaiveKey = Keys.F };
        public ShortCutCommand EditMenu = new ShortCutCommand("編集") { IsAlt = true, NaiveKey = Keys.E };
        public ShortCutCommand PageMenu = new ShortCutCommand("ページ") { IsAlt = true, NaiveKey = Keys.P };
        public ShortCutCommand ToolMenu = new ShortCutCommand("ツール") { IsAlt = true, NaiveKey = Keys.T };
        public ShortCutCommand OtherMenu = new ShortCutCommand("その他") { IsAlt = true, NaiveKey = Keys.O };
        */
        public MasterFormMenuShortCutKeys()
        {
            base.Name = "メニュー";
            /*
            Items = new ShortCutCommand[] { FileManu, EditMenu
            ,PageMenu,ToolMenu,OtherMenu};
            */
        }
    }
    [Serializable]
    public class MasterFormFileMenuShortCutKeys : SuouBase.Interface.ShortCutCommandCollection
    {
        /*
        public ShortCutCommand AddNewPage = new ShortCutCommand("新規作成") { IsCtrl = true, NaiveKey = Keys.N };
        public ShortCutCommand OpenPage = new ShortCutCommand("開く") { IsCtrl = true, NaiveKey = Keys.O };
        public ShortCutCommand OverRidePage = new ShortCutCommand("上書き保存") { IsCtrl = true, NaiveKey = Keys.S };
        public ShortCutCommand SaveWithName = new ShortCutCommand("名前をつけて保存") { IsCtrl = true,IsAlt = true, NaiveKey = Keys.S };
        */
        public MasterFormFileMenuShortCutKeys()
        {
            base.Name = "ファイルメニュー";
            //Items = new ShortCutCommand[] { AddNewPage, OpenPage, OverRidePage, SaveWithName};
        }
    }
}
