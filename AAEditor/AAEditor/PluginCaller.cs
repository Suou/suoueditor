﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.Reflection;
using System.IO;

namespace SuouEditor
{
    class PluginCaller
    {
        public static FileInfo[] GetPluginList(string FolderName)
        {
            List<FileInfo> r = new List<FileInfo>();
            DirectoryInfo di = new DirectoryInfo(FolderName);
            if (!di.Exists) return null;
            var fs = di.GetFiles();
            
            foreach (FileInfo fi in fs)
            {
                if (fi.Exists && fi.Extension == ".dll") r.Add(fi);
            }
            return r.ToArray();
        }
        static Dictionary<string, Assembly> AssembleDic = new Dictionary<string, Assembly>();
        public static bool CanError = true;
    }
}
