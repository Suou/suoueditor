﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Drawing;
using BSystem.Drawing;

namespace SuouEditor.OptionalWindow
{
    public partial class PluginControler : Form
    {
        public PluginControler()
        {
            InitializeComponent();
        }
        public EditorInfo DLLFunction { get; set; }
        
        public void Check()
        {
            Font ff = new Font("ＭＳ Ｐゴシック", 12, FontStyle.Regular);
            var bmp = DrawTextBmp.CreateBitmap(ArrangeMap, ff, 2);
            var bmp2 = EnlargeBmp.EnlargeBitmap(bmp, (float)1.0);
            this.pictureBox1.Image = bmp2;
            this.pictureBox1.Width = bmp2.Width;
            this.pictureBox1.Height = bmp2.Height;

            var f = DLLFunction.PluginList;
            comboBox1.Items.Clear();
            foreach (var pb in f)
            {
                var v = new ComboObject(pb.Plugin);
                comboBox1.Items.Add(v);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           var f = (ComboObject)comboBox1.SelectedItem;
           if (f != null)
           {
               //ここを疑問に思え
               throw new Exception();
#pragma warning disable
               comboBox2.SelectedIndex = DLLFunction.GetPluginMetaData(f.Item).Position + 1;
               comboBox2.Enabled = true;
               LabelCheck(f.Item);
#pragma warning restore
           }
        }
        private void LabelCheck(IComponentPlugin pb)
        {
            if (PluginOperator.CanControlize(pb))
            {
                CanControlLabel.Text = "コントロール化できます";
                CanControlLabel.BackColor = Color.Blue;
            }
            else
            {
                CanControlLabel.Text = "コントロール化できません";
                CanControlLabel.BackColor = Color.Red;
            }
            if (PluginOperator.CanFormize(pb))
            {
                CanFormLabel.Text = "フォーム化できます";
                CanFormLabel.BackColor = Color.Blue;
            }
            else
            {
                CanFormLabel.Text = "フォーム化できません";
                CanFormLabel.BackColor = Color.Red;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            throw new Exception();
            //var f = (ComboObject)comboBox1.SelectedItem;
            //var n = comboBox2.SelectedIndex;
            //DLLFunction.Framework.SetPlugin(f.Item, n - 1);
        }


        string ArrangeMap = @"┏━━━━━━━━━━━━━━━━━━━━┓
┃　　　　　　　　　　　　　　⑦　　　 　 　 　 　 　 　 　 ┃
┠────┬─────────┬─────┨
┃　 　 　 　 │　 　 　 　 　 　 　 　 　 │　 　 　 　 　 ┃
┃　　 ① 　 │　 　 　 　 　 　 　 　 　 │　 　 ④ 　 　┃
┃　 　 　 　 │　 　 　 　 　 　 　 　 　 │　 　 　 　 　 ┃
┃────┤　 　 　 　 　 　 　 　 　 ├─────┃
┃　 　 　 　 │　 　 　 　 　 　 　 　 　 │　 　 　 　 　 ┃
┃　　 ② 　 │　　　 　 　 ◎　　　　　.│　 　 ⑤ 　 　┃
┃　 　 　 　 │　 　 　 　 　 　 　 　 　 │　 　 　 　 　 ┃
┃────┤　 　 　 　 　 　 　 　 　 ├─────┃
┃　 　 　 　 │　 　 　 　 　 　 　 　 　 │　 　 　 　 　 ┃
┃　　 ③ 　 │　 　 　 　 　 　 　 　 　 │　 　 ⑥ 　 　┃
┃　 　 　 　 │　 　 　 　 　 　 　 　 　 │　 　 　 　 　 ┃
┃────┴───────────────┃
┃　　　　　　　　　　　　　　　　　 　 　 　 　 　 　 　 　 ┃
┃　　　　　　　　　　　　 　⑧　　　　　 　 　 　 　 　 　 ┃
┗━━━━━━━━━━━━━━━━━━━━┛";

        private void button2_Click(object sender, EventArgs e)
        {
            throw new Exception();
            //DLLFunction.Framework.InitializePluginList();
            //Check();
        }

        private void PluginControler_Load(object sender, EventArgs e)
        {

        }

        private void PluginControler_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                this.Visible = false;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.colorDialog1.ShowDialog();
            var f = this.colorDialog1.Color;
            DLLFunction.TextBoxPlugin.BackColor = f;
        }
    }
    public class ComboObject
    {
        public IComponentPlugin Item { get; set; }

        public ComboObject(IComponentPlugin pb)
        {
            Item = pb;
        }

    }

    
}
