﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using SuouBase.Drawing;
using System.Windows.Forms;

using BSystem.Drawing;
namespace SuouEditor.OptionalWindow
{
    public partial class HeartsWindow : Form
    {
        public HeartsWindow()
        {

            InitializeComponent();
            
            var bmp = DrawTextBmp.CreateBitmap(s, f, 2);
            
            var bmp2 = EnlargeBmp.EnlargeBitmap(bmp, (float)0.55);
            this.pictureBox1.Image = bmp2;
            this.pictureBox1.Width = bmp2.Width;
            this.pictureBox1.Height = bmp2.Height;
        }
        Font f = new Font("ＭＳ Ｐゴシック", 12,FontStyle.Bold);
        string s = @"＿＿＿_
　　　 　 ／　　 　 　＼
　　　 ／　─　 　 ─　＼
　　／ 　 （●） 　（●） 　＼
　...|　 　 　 （__人__）　　 　 |
　 ..＼　 　　 ｀⌒´ 　　　,／
　 ..／　　　 　ー‐　　　　＼

";

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void HeartsWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }
        
    }
}
