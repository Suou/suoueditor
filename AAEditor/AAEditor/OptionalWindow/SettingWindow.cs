﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;

namespace SuouEditor.OptionalWindow
{
    public partial class SettingWindow : Form
    {
        private EditorInfo DLLFunction { get; set; }
        public SettingWindow()
        {
            InitializeComponent();
        }
        private void SettingWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }
        private List<SettingPage> PageList = new List<SettingPage>();
        public SettingWindow(EditorInfo DLLFunction):this()
        {
            this.PageList.Clear();
            this.DLLFunction = DLLFunction;
            foreach (var v in this.DLLFunction.PluginList)
            {
                var SettingPages = v.Plugin.SettingPage;
                if (SettingPages == null) continue;
                
                foreach (var page in SettingPages)
                {
                    this.PageList.Add(page);
                }
                

            }
            foreach (var v in this.DLLFunction.Framework.SettingPage) this.PageList.Add(v);
            DLLFunction.MasterForm.SettingPage.ToList().ForEach((v) => this.PageList.Add(v));

            PageList.ForEach((v) => this.Add(v));
        }
        public void Add(SettingPage page)
        {
            var p = new TabPage();
            p.Controls.Add(page.Ctrl);
            p.Tag = page.Ctrl;
            page.Ctrl.Dock = DockStyle.Fill;
            p.AutoScroll = true;
           
            var gt = (UserControl)page.Ctrl;
            gt.AutoScroll = true;
            gt.AutoSize = true;
            p.Text = page.PageName;
            this.tabControl1.TabPages.Add(p);
        }
        
        public TabControl Tab
        {
            get { return this.tabControl1; }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var f = (UserControl)this.tabControl1.SelectedTab.Tag;
            if (f != null)
            {
                f.AutoScroll = true;
            }
        }

        private void Button_OK_Click(object sender, EventArgs e)
        {
            this.PageList.ForEach((v) => v.OK());
        }

        
    }

}
