﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;

namespace SuouEditor.OptionalWindow
{
    public partial class LogForm : Form
    {
        public LogForm()
        {
            InitializeComponent();
            ErrorMessage.LogWindow = this.textBox1;
        }

        private void LogForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;
                Visible = false;
            }
        }
    }
}
