﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase.Interface;
using SuouBase;
using AAEditTabControl;

namespace SuouEditor
{
    public static class Converter
    {
        public static void ConvertItem(ToolStripMenuItem tsi, ToolStripItemCollection tsi2)
        {
            if (tsi2 == null) return;
            tsi.DropDownItems.Clear();

            foreach (ToolStripItem tsi3 in tsi2)
            {
                if (tsi3 is ToolStripMenuItem)
                {
                    tsi.DropDownItems.Add(Conv((ToolStripMenuItem)tsi3));
                }
                else if (tsi3 is ToolStripSeparator)
                {
                    tsi.DropDownItems.Add(Conv((ToolStripSeparator)tsi3));
                }
            }
        }
        public static ToolStripMenuItem Conv(ToolStripMenuItem tsi)
        {
            ToolStripMenuItem r = new ToolStripMenuItem();
            
            r.Tag = tsi;
            r.Name = tsi.Name;
            r.Text = tsi.Text;
            r.Enabled = tsi.Enabled;
            r.ShortcutKeys = tsi.ShortcutKeys;
            foreach (ToolStripMenuItem tsi2 in tsi.DropDownItems)
            {
                r.DropDownItems.Add(Conv(tsi2));
            }
            r.Click += ClickEvent;
            return r;
            
        }
        public static ToolStripSeparator Conv(ToolStripSeparator tss)
        {
            ToolStripSeparator r = new ToolStripSeparator();
            r.Tag = tss;
            r.Name = tss.Name;
            r.Text = tss.Text;
            r.Enabled = tss.Enabled;
            
            return r;

        }

        public static void ClickEvent(object sender, EventArgs e)
        {
            var p = (ToolStripMenuItem)sender;
            var q = (ToolStripMenuItem)p.Tag;
            q.PerformClick();
        }

    }
    class MyClass
    {

        public static string OpenDialogBox(OpenFileDialog ofd)
        {
            //OpenFileDialogクラスのインスタンスを作成

            //はじめのファイル名を指定する
            //はじめに「ファイル名」で表示される文字列を指定する
            ofd.FileName = "";
            //はじめに表示されるフォルダを指定する
            //指定しない（空の文字列）の時は、現在のディレクトリが表示される
            //ofd.InitialDirectory = @"C:\";
            //[ファイルの種類]に表示される選択肢を指定する
            //指定しないとすべてのファイルが表示される
            ofd.Filter =
                "ファイル(*.ast;*.mlt)|*.ast;*.mlt|すべてのファイル(*.*)|*.*";
            //[ファイルの種類]ではじめに
            //「すべてのファイル」が選択されているようにする
            ofd.FilterIndex = 2;
            //タイトルを設定する
            ofd.Title = "開くファイルを選択してください";
            //ダイアログボックスを閉じる前に現在のディレクトリを復元するようにする
            ofd.RestoreDirectory = true;
            //存在しないファイルの名前が指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckFileExists = true;
            //存在しないパスが指定されたとき警告を表示する
            //デフォルトでTrueなので指定する必要はない
            ofd.CheckPathExists = true;

            //ダイアログを表示する
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                return ofd.FileName;
            }
            return null;
        }
    }
    public enum WindowMode
    {
        Nothing,
        InitializeMode,
        ViewMainDrawMode,
        ViewSubDrawMode,
        ControlTabFormMode,
        SubDrawFormMode,
        MainDrawFormMode,
        AAFileTreeFormMode,
        TabMainDrawMode,
        TemplateListMode
    }
    public enum EditMode
    {
        Edit,Search
    }
}
