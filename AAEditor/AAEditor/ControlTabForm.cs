﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Threading.Tasks;
using SuouBase;
using SuouBase.Interface;
using AAEditTabControl;
using SuouBase.Text;
using SuouEditor.SuouOperator;
using BSystem.Collections;
using BSystem.Controls;
using SuouBase.Book;
namespace SuouEditor
{
    public partial class ControlTabForm : SerializableForm,ISupervisorForm
    {

        #region Property
        public Form Form
        {
            get
            {
                return this;
            }
        }
        public Operators OperationObject { get; set; }
        public int SplitLine1
        {
            get { return this.splitContainer1.SplitterDistance; }
            set { this.splitContainer1.SplitterDistance = value; }
        }
        public TestScript.CompileScript CompileScriptClass;
        public SettingPage1 SettingPageCtrl = null;
        public EditorSettingControl SettingPageCtrl2;
        public SettingPage[] SettingPage
        {
            get
            {
                return new SettingPage[] { new SuouBase.Controls.ShortCutRegister(this.MainMenuStrip){PageName ="メインフォーム"} };
                //this.SettingPageCtrl2 = new EditorSettingControl(this.OperationObject.DLLFunction);
                //return new SettingPage[] { SettingPageCtrl, SettingPageCtrl2 };
            }
        }
        public Menubar GetMenubar()
        {
            Menubar mMenubar = new Menubar();
            mMenubar.MenuStrip = this.MainMenuStrip;
            return mMenubar;
        }
        #endregion
        #region Initialize
        public ControlTabForm()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.Text = global::SuouEditor.Properties.Resources.Title;
            
        }
        
        private void InitialLoad()
        {
#if DEBUG
            //TimeDebug.StopWatchWindow.Show();
#endif

            TimeDebug.Watch(() => OperationObject = new Operators(this), "OperationObject");

            CompileScriptClass = new TestScript.CompileScript();

            OperationObject.AAEditorStartSetting();
            this.suouEditorMenuStrip1.MenuKeyRefresh();

#if DEBUG
            TimeDebug.DLLFunction = this.OperationObject.DLLFunction;
#endif
            this.suouEditorMenuStrip1.OperationObject = this.OperationObject;
            //GeneralMethod.SetIbookSaveDialog(this.OperationObject.DLLFunction.Setting.StoryFileFolder);

        }
        #endregion
        #region InterfaceMethod
        public void ViewPageSize(IBookPage page)
        {
            this.NotifyPageSize(page.SText.CreateLightInformation());
        }
        public void SetFramework(IFrameworkPlugin mw)
        {
            if (mw == null) throw new MyException("フレームワークが存在しません");
            this.splitContainer1.Panel2.Controls.Clear();
            this.splitContainer1.Panel2.Controls.Add(mw.Ctrl);
            mw.Ctrl.Dock = DockStyle.Fill;
        }
        public void NotifyPageSize(string text)
        {

            this.SizeLabel.Text = text;
            statusStrip1.Refresh();
        }
        public void NotifyCaretInformation(string text)
        {
            this.NotifyLabel2.Text = text;
            statusStrip1.Refresh();
        }
        public void NotifyCaretCharInformation(string text)
        {
            this.NotifyLabel3.Text = text;
            statusStrip1.Refresh();
        }
        #endregion
        #region Event
        private void ControlTabForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                SuouEditorConst.CheckFolder();
                BSystem.XML.NonBinarySerializer.Save(SuouEditorConst.GetSettingFile("ControlTab.stn"), this.Setting);
                var b = OperationObject.AAEditorEndSetting();
                if (!b)
                {
                    e.Cancel = true;
                }
                else
                {
                    if (OperationObject.GeneralData.CloseAllTab(OperationObject.sfd))
                    {

                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }

            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (OperationObject != null && OperationObject.GeneralData != null && this.OperationObject.DLLFunction.Setting.EnableBackup)
            {
                OperationObject.GeneralData.AutoBackUp();
            }
        }
        #endregion

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var b = this.OperationObject.DLLFunction.FileData.SelectedClose(this.OperationObject.sfd);

        }

        private void toukaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var book = this.OperationObject.DLLFunction.SelectedStoryFile;
            var p = StoryViewer.WebWriter.WebWriterControl.EasyForm(book);
            p.Show();
        }

        private void collectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GC.Collect();
        }


        private void ControlTabForm_Shown(object sender, EventArgs e)
        {
            this.InitialLoad();
        }


    }
    

}
