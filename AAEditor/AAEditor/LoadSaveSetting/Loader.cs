﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;
using SuouBase;
using SuouEditor.SuouOperator;
using SuouBase.Interface;

namespace SuouEditor
{
    class Loader
    {

        public static BaseSetting DeserializeSavedata(string filepath)
        {
            var f = new FileInfo(filepath);
            if (!f.Exists) return null;

            try
            {
                var fp = XMLGeneralSerialize.ReadBinary(filepath);

                if (fp is BaseSetting)
                {
                    //ope.DLLFunction.Setting = (BaseSetting)fp;
                    //this.Setting = ope.DLLFunction.Setting;
                    return (BaseSetting)fp;
                }
                else
                {
                    return null;
                }
            }
            catch (InvalidCastException)
            {
                ErrorMessage.AddLog(ErrorMessage.Warning.LoadFileException, filepath);
            }
            catch
            {
                ErrorMessage.AddLog(ErrorMessage.Warning.LoadFileException, filepath);
                return null;
            }
            finally
            {
            }
            return null;
        }
        public static IList<IComponentPluginMetaData> LoadPluginItems(EditorInfo dLLFunction, BaseSetting item)
        {
            if (item.PluginSetting != null)
            {
                foreach (IComponentPluginMetaData pcs in item.PluginSetting)
                {
                    if (pcs.Check())
                    {
                        pcs.Plugin = TimeDebug.Watch2(() => SuouXMLSerialize.DirectCall(pcs.PluginName, dLLFunction), "DirectCall" + pcs.PluginName);
                        if (pcs.Plugin == null) pcs.Plugin = PluginOperator.Call(pcs.DLLName, pcs.NameSpace, pcs.ClassName, dLLFunction, pcs.Position, pcs.PluginName);
;
                        
                  
                    }
                }
                return item.PluginSetting;
            }
            else
            {
                return null;
            }

            
        }
        public static IFrameworkPlugin LoadFramework(IFrameworkPluginMetaData frameSetting,EditorInfo dLLFunction)
        {
            if (frameSetting == null) return null;
            if (frameSetting.PluginName == "SimpleFramePlugin")
            {
                return new StoryViewer.Framework.SimpleFramework.SimpleFramePlugin(dLLFunction);
            }
            else
            {
                return PluginOperator.GetCallFramework(dLLFunction, frameSetting);
            }
        }
    }
}
