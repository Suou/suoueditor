﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase;
namespace SuouEditor
{


    static class Program
    {
        /// <summary>
        /// アプリケーションのメイン エントリ ポイントです。
        /// </summary>
        /// 
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            SuouBase.ErrorMessage.Start("FirstRun");

            var setting = BSystem.XML.NonBinarySerializer.Load<BSystem.Controls.FormSaveData>(SuouEditorConst.GetSettingFile("ControlTab.stn"));
            var form = new ControlTabForm();
            if(setting != null)form.Setting = setting;
            Application.Run(form);
        }
    }
}
