﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;

namespace TestScript
{
    public class CompileScript
    {
        //ScriptBase.Nantoka Nan;
        public CompileScript()
        {
            //Nan = nan;
        }
           

        public void Compile(string ScriptPath)
        {
            string script = File.ReadAllText(ScriptPath, Encoding.GetEncoding("shift-jis"));

            //コンパイルするための準備
            
            using (var cp = new CSharpCodeProvider(new Dictionary<string, string> { { "CompilerVersion", "v4.0" } }))
            {
                var cres = this.Prepare(cp, script);

                this.ErrorShow(cres, ScriptPath);

                //var sb = new ScriptBase.AAEditorScriptBaseClass(this.Nan);

                object[] args = new object[] {};
                this.Run(cres, args);

            }

        }

        public CompilerResults Prepare(CSharpCodeProvider cp, string script)
        {
            #pragma warning disable
            ICodeCompiler icc = cp.CreateCompiler();
            #pragma warning restore
            CompilerParameters cps = new CompilerParameters();

            script = ReferDLL(cps, script);
            CompilerResults cres;
            //メモリ内で出力を生成する
            cps.CompilerOptions = "/r:script/YaruoBlockGame/TestLibrary.dll";
            cps.GenerateInMemory = true;
            //コンパイルする
            cres = icc.CompileAssemblyFromSource(cps, script);
            return cres;
        }

        public void Run(CompilerResults cres,object[] args)
        {

            //コンパイルしたアセンブリを取得
            
            Assembly asm = cres.CompiledAssembly;
            
            //MainClassクラスのTypeを取得
            Type t = asm.GetType("Test.Test");
            //EValメソッドを実行し、結果を取得
            try
            {
                string d = (string)t.InvokeMember("Main",
                    BindingFlags.InvokeMethod,
                    null,
                    null,
                    args);

                //結果を表示
                MessageBox.Show(d);
            }
            catch (MissingMethodException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public string[] TextSplit(string text)
        {
            string script2 = text.Replace(System.Environment.NewLine, "");
            string[] s = { ";" };
            var strs = script2.Split(s, StringSplitOptions.None);
            return strs;
        }

        public string[] LineSplit(string s)
        {
            string[] strs = s.Split(new string[] {" "}, StringSplitOptions.RemoveEmptyEntries);
            return strs;
        }

        public void ErrorShow(CompilerResults cr,string path)
        {
            var snal = new List<SourceNameAndLine>();
            int headerLineCount = 0;
            /*
            // HeaderSourceの行数をカウントしておき、コンパイルエラーのときの行数を調整する。
            // 改行文字列(2文字)→1文字に置換すれば、その置換された回数が出現回数である。
            headerLineCount = HeaderSource.Length - HeaderSource.Replace("\r\n", " ").Length;
            */

            if (cr.Errors.Count > 0)
            {
                string errors = Path.GetFileName(path) + "のコンパイル中にエラーが発生しました。\n";
                foreach (CompilerError err in cr.Errors)
                {
                    int line = err.Line - headerLineCount - 1; // err.Lineは1-originなので。
                    string fn = "";
                    if (0 <= line && line < snal.Count)
                        fn = Path.GetFileName(snal[line].SourceName) + "の" + snal[line].lineNo;
                    else
                        fn = line.ToString(); // 範囲外なのでfooter scriptか何かでエラーになったんだろ。
                    errors += fn + "行目の" + err.Column + "文字目 → " + err.ErrorText + "\n";
                }

                throw new System.Exception(errors);
            }
        }

        public string CompileUsing(string[] strs,CompilerParameters cps)
        {
            if (strs.Length >= 4 && strs[2] == "in")
            {
                var s = strs[3].Replace("\"","");
                
                
                
                if (s.StartsWith("System"))
                {
                    cps.ReferencedAssemblies.Add(s);  
                }
                else
                {
                    string filepath = Path.Combine(Application.StartupPath, s);
                    FileInfo fi = new FileInfo(filepath);
                    cps.ReferencedAssemblies.Add(filepath);
                }
                 
            }
            return strs[0] +" "+ strs[1];
        }

        public string ReferDLL(CompilerParameters cps,string ScriptText)
        {
            
            List<string> referencedAssemblies = TextSplit(ScriptText).ToList();
            //referencedAssemblies = new List<string>();
            // 他に追加アセンブリが指定されているならそれも追加する。
            for (int i = 0; i < referencedAssemblies.Count; i++)
            {
                string[] strs = this.LineSplit(referencedAssemblies[i]);
                switch (strs[0])
                {
                    case "using": referencedAssemblies[i] = CompileUsing(strs, cps); break;
                }
            }


            cps.ReferencedAssemblies.Add("System.dll");
            cps.ReferencedAssemblies.Add("System.Core.dll");
            cps.ReferencedAssemblies.Add("System.Data.dll");
            cps.ReferencedAssemblies.Add("System.Drawing.dll");
            cps.ReferencedAssemblies.Add("System.Windows.Forms.dll");
            cps.ReferencedAssemblies.Add(Path.Combine(Application.StartupPath, "ScriptBase.dll"));


            string rstr =  String.Join(";", referencedAssemblies);
            return rstr;
        }
    }

    class SourceNameAndLine
    {
        public string SourceName {get;set;}
        public int lineNo{get;set;}
    }


}
