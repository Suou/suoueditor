﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Drawing;
using System.Windows.Forms;
using System.Threading.Tasks;
using SuouBase;
using SuouEditor.SuouOperator;
using SuouBase.Interface;
using BSystem;
using BSystem.Text;
using BSystem.Collections;

namespace SuouEditor
{
    [Serializable()]
    public struct LocalForm
    {
        public string Name;
        public Point Location;
        public Size Size;

        public LocalForm(Form form)
        {
            this.Name = form.Name;
            this.Location = form.Location;
            this.Size = form.Size;
        }

        public void Read(Dictionary<string,Form> FormDic)
        {
            if (FormDic.ContainsKey(this.Name))
            {
                FormDic[this.Name].Location = this.Location;
                FormDic[this.Name].Size = this.Size;
            }
            
        }
    }
    [Serializable()]
    public struct SplitControl
    {
        public string Name;
        public int Distance;

        public SplitControl(SplitContainer sc)
        {
            this.Name = sc.Name;
            this.Distance = sc.SplitterDistance;
        }

        public void Read(Dictionary<string, SplitContainer> SplitDic)
        {
            SplitDic[this.Name].SplitterDistance = this.Distance;

        }
    }

    


    [Serializable()]
    public class SuouXMLSerialize
    {
        /*
        public SuouXMLSerialize()
        {
            this.Setting = new BaseSetting();
        }
        public SuouXMLSerialize(Operators ope):this()
        {
            
        }
        */
        /*
        public static SuouXMLSerialize Load(string filename,Operators ope)
        {
            var v = new SuouXMLSerialize();
            //v.Deserialize(filename, ope);
            return v;
        }
        */
        
        public static IComponentPlugin DirectCall(string PluginName,EditorInfo DLLFunction)
        {
            IComponentPlugin plg = null;
            if (PluginName == "AA選択")
            {
                plg = new StoryViewer.AASelecter.AASelecterPlugin(DLLFunction);
            }
            else if (PluginName == "レイヤー")
            {
                plg = new SuouTextBox2.Controls.LayerSupervisorPlugin(DLLFunction);
            }
            else if (PluginName == "噴出し")
            {
                plg = new StoryViewer.HukidasiToolPlugin(DLLFunction);
            }
            else if (PluginName == "SuperStoryViewerPlugin")
            {
                plg = new StoryViewer.IStoryViewer.IStoryViewerPlugin(DLLFunction);
                
            }
            else if (PluginName == "文字")
            {
                plg = new StoryViewer.MojiToolPlugin(DLLFunction);
            }
            else if (PluginName == "箱")
            {
                plg = new StoryViewer.AAToolBoxPlugin(DLLFunction);
            }
            else if (PluginName == "蘇芳ビューワー")
            {
                plg = new StoryViewer.BookControl.Viewer.SuouViewer.SuouViewerPlugin(DLLFunction);
            }
            else if (PluginName == "アウトラインエディタ")
            {
                plg = new StoryViewer.OutlineEditor.OutlineEditorPlugin(DLLFunction);
            }
            

            return plg;
        }

        
    }
    /*
    public class TranslateXML
    {

        public static void Write(Dictionary<string, List<ToolStripItem>> ButtonNameDic)
        {
            XmlTextWriter xtw = null;
            string fname = @"Language.xml";
            try
            {
                string[] separator = { "\r\n" };
                xtw = new XmlTextWriter(fname, Encoding.Default);
                //インデントするかどうか
                xtw.Formatting = Formatting.Indented;

                xtw.WriteStartDocument();//XML宣言
                xtw.WriteStartElement("Language");//開始タグ

                foreach (string s in ButtonNameDic.Keys)
                {
                    xtw.WriteStartElement(s);
                    {
                        xtw.WriteAttributeString("Name", s);
                    }
                    xtw.WriteEndElement();
                }
                xtw.WriteEndElement();//htmlタグを閉じる
                xtw.WriteEndDocument();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                xtw.Close();
            }
        }

        public static Dictionary<string, string> Read(string xmlFile)
        {
            var Dic = new Dictionary<string, string>();

            var DI = new FileInfo(xmlFile);
            if (!DI.Exists)
            {
                ErrorMessage.Show(ErrorMessage.Warning.NotFoundFile, xmlFile);
                return null;
            }

            //XmlDocumentインタンスを作成し、XMLファイルを読み込む
            XmlDocument xmlr = new XmlDocument();
            try
            {
                xmlr.Load(xmlFile);
            }
            catch
            {
                ErrorMessage.Show(ErrorMessage.Warning.NotFoundFile);
                return null;
            }

            string s = "Language";
            XmlNode xl = xmlr.SelectSingleNode(s);
            foreach (XmlElement xn in xl.ChildNodes)
            {
                string Name = xn.Name;
                string TranslateName = xn.GetAttribute("Name");
                Dic[Name] = TranslateName;
                //this.TranslateNameDic(Name, TranslateName);
            }
            return Dic;
        }

    }
    */

}
