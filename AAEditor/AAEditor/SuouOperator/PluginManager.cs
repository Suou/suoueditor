﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase;
using AAEditTabControl;
using System.Xml;
using SuouBase.Expansion;
using SuouBase.Interface;
using SuouEditor.OptionalWindow;
using BSystem.Text;
using BSystem.Collections;

namespace SuouEditor.SuouOperator
{
    public class PluginManager
    {
        private EditorInfo DLLFunction;

        public PluginManager(EditorInfo pd)
        {
            this.DLLFunction = pd;
        }
        public void LoadPluginList()
        {
        }

        public void PluginClickEvent(object sender, EventArgs e)
        {
            throw new Exception();
        }

        public void InitializePluginList()
        {
            //DLLFunction.Framework.InitializePluginList();
            ClearMainPlugin();
            SetDefaultPlugin();
        }

        public void ClearMainPlugin()
        {
            //DLLFunction.StoryPageViewPlugin = null;
            DLLFunction.GlobalTemplatePlugin = null;
            DLLFunction.LocalTemplatePlugin = null;
            DLLFunction.TemplatePageViewPlugin = null;
            DLLFunction.TextBoxPlugin = null;
            this.DLLFunction.DisposeCheck();
            this.DLLFunction.Framework.Clear();
            this.DLLFunction.PluginList.Clear();
            
        }

        public void SetDefaultPlugin()
        {
            PluginOperator.CallSet(DLLFunction, PluginSet.SimpleFrameSet);
        }
        public ToolStripMenuItem[] GetScriptItems()
        {
            var r = new List<FileInfo>();
            var f = new DirectoryInfo("Script");
            if (f.Exists)
            {
                var files = f.GetFiles();
                files.ForEach((v) => r.Add(v));
            }
            return r.Select((v) => new ToolStripScriptButton(v.Name, v.FullName, this.Enzine)).Select((v) => v.Create()).ToArray();
        }
        public void Enzine(string fullpath)
        {
        }
    }
}
