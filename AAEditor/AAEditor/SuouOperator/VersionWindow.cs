﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Drawing;
using SuouBase.Interface;
namespace SuouEditor.OptionalWindow
{
    public partial class VersionWindow : Form
    {
        public EditorInfo DLLFunction { get; set; }
        public VersionWindow()
        {
            InitializeComponent();
            this.LastUpdateLabel.Text = global::SuouEditor.Properties.Resources.LastUpdate;
        }
        public VersionWindow(EditorInfo dll):this()
        {
            this.DLLFunction = dll;
        }
        public void ShowRandomText()
        {
            /*
            if (this.DLLFunction == null) return;
            var pic = this.DLLFunction.Info.IBookDictionary.RandomPick();
            if (pic != null) this.bookPagePicture1.Page = pic;
            */
        }

        private void VersionWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
        }

        private void VersionWindow_ParentChanged(object sender, EventArgs e)
        {
            if (this.ParentForm != null)
            {
                this.ParentForm.Size = new Size(600, 400);
            }
        }
    }
}
