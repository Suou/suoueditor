﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase;
using AAEditTabControl;
using System.Xml;
using SuouBase.Expansion;
using SuouBase.Interface;
using SuouEditor.OptionalWindow;
using SuouEditor;
using BSystem.Collections;
using SuouBase.Book;
namespace SuouEditor.SuouOperator
{

    public class Operators
    {
        #region Property
        public AstPage[] CopyItems { get; set; }
        private ISupervisorForm _oISupervisorForm;
        public ISupervisorForm oISupervisorForm
        {
            get { return _oISupervisorForm; }
            set
            {
                _oISupervisorForm = value;
            }
        }
        public SuouEditorDataCollection GeneralData { get; set; }
        public ControlTabForm mCTForm { get; set; }
        //public EditMode OATBMode = EditMode.Edit;
        //public AstPage EditPage = null;
        public WindowManager WindowMangerClass { get; set; }
        public PluginManager PluginManagerClass { get; set; }
        public RecentFileButtonMaker RecentMenuButtons { get; set; }
        public EditorInfo DLLFunction;
        
        public SaveFileDialog sfd { get; set; }
        //public BaseSetting Setting { get; private set; }   
       #endregion
        private void recentlyUsingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (sender is ToolStripMenuItem)
            {
                var item = (ToolStripMenuItem)sender;
                this.GeneralData.AddFilePath((string)item.Tag);
            }
        }
        public Operators(ControlTabForm ctf)
        {
            mCTForm = ctf;
            //ofd = GeneralMethod.OpenDialogBox();
            sfd = GeneralMethod.SaveDialogBox();
            this.GeneralData = new SuouBase.Interface.SuouEditorDataCollection(mCTForm.tabControl1,mCTForm.GetMenubar());
            DLLFunction = new EditorInfo(this.GeneralData,mCTForm);
            this.PluginManagerClass = new PluginManager(this.DLLFunction);
            this.WindowMangerClass = new WindowManager(mCTForm, DLLFunction);
            this.oISupervisorForm = this.mCTForm;
            this.PluginManagerClass.LoadPluginList();
            this.GeneralData.CollectionChanged += this.DataClass_DataChanged;
        }

        private void DataClass_DataChanged(object sender, SuperObservableCollectionEventArgs<SuouEditorDataClass> e)
        {
            switch (e.Action)
            {
                case SuperObservableCollectionAction.Add:
                    foreach (var v in e.AddedItems)
                    {
                        
                        if(v.StoryFile.FullFilePath != null)this.DLLFunction.Setting.RecentUsingFileList.LimitAdd(v.StoryFile.FullFilePath);
                    }
                    break;
            }
        }
        #region StartEnd
        public void AAEditorStartSetting()
        {

            SuouEditorConst.CheckFolder();

            //ControlTabFormSerialize sts = ControlTabFormSerialize.Load(mCTForm,SuouEditorConst.GetSettingFile("ControlTab.stn"));
            this.DLLFunction.LoadPlugin(Loader.LoadFramework, Loader.LoadPluginItems);

            if (DLLFunction.Framework == null)
            {
                this.PluginManagerClass.SetDefaultPlugin();
            }
            this.SetClass();
            DLLFunction.Framework.NotifyFinishAddPluginList();

            TimeDebug.Watch(() => DLLFunction.FileData.StartEnd(), "StartFileData");
            SuouBase.ErrorMessage.Stop("FirstRun");
            this.DLLFunction.FinishStartSetting();
            if (this.DLLFunction.Setting.MainFormShortCutKeyDic != null) this.DLLFunction.Setting.MainFormShortCutKeyDic.SetShortCutKey(this.mCTForm.MainMenuStrip);

            if (!this.DLLFunction.Setting.IsNormalEnd)
            {
                foreach (var v in this.DLLFunction.Setting.FileTabList)
                {
                    this.RestoreFile(v);
                }
            }
            this.DLLFunction.LoadFileData();

            this.DLLFunction.Setting.IsNormalEnd = false;
            this.DLLFunction.SaveSetting();
        }
        public void RestoreFile(string path)
        {
            var oldFile = new FileInfo(path);
            var backupFolderPath = SuouEditorConst.GetBackupFolderPath(oldFile.Name);
            var newFile = SuouBase.Base.BackupCollection.GetLastBackupFileInfo(backupFolderPath, System.IO.Path.GetExtension(oldFile.Name));
            if (newFile != null)
            {
                var a = string.Format("前回のファイル情報 {0}, {1} byte, {2}", oldFile.Name, oldFile.Length, oldFile.LastAccessTime);
                var b = string.Format("最新のバックアップ情報 {0}, {1} byte, {2}", newFile.Name, newFile.Length, newFile.CreationTime);
                var c = string.Format("前回異常終了したのでファイルが正しく保存されていない可能性があります。{0}{1}{2}{3}{4} 最新のバックアップに置き換えますか？",
                    System.Environment.NewLine,a,System.Environment.NewLine,b,System.Environment.NewLine);
                DialogResult result = MessageBox.Show(c,
                    "ファイルの復元",MessageBoxButtons.YesNoCancel,MessageBoxIcon.Exclamation,MessageBoxDefaultButton.Button2);
                if (result == DialogResult.Yes)
                {
                    var p = newFile.CopyTo(oldFile.FullName,true);
                    
                }
            }

            
        }

        public void SetClass()
        {
            foreach (var pcs in this.DLLFunction.PluginList)
            {
                if (pcs.Plugin is ITextEditClass)
                {
                    this.DLLFunction.TextBoxPlugin = (ITextEditClass)pcs.Plugin;
                }
                //if (pcs.Plugin is IStoryPageListClass) DLLFunction.StoryPageViewPlugin = (IStoryPageListClass)pcs.Plugin;
                if (pcs.Plugin is ILocalTemplateClass) DLLFunction.LocalTemplatePlugin = (ILocalTemplateClass)pcs.Plugin;
                if (pcs.Plugin is ITemplatePageListClass) DLLFunction.TemplatePageViewPlugin = (ITemplatePageListClass)pcs.Plugin;
                if (pcs.Plugin is IGlobalTemplateClass) DLLFunction.GlobalTemplatePlugin = (IGlobalTemplateClass)pcs.Plugin;
                if (pcs.Plugin is IPreviewClass) DLLFunction.PreviewPlugin = (IPreviewClass)pcs.Plugin;
            }
        }

        public bool AAEditorEndSetting()
        {
            //SeとDeで分けるべきでは？

            //SuouXMLSerialize sb = new SuouXMLSerialize(this);

            var b = DLLFunction.DisposeCheck();
            this.GeneralData.CollectionChanged -= this.DataClass_DataChanged;
            return b;

        }
        #endregion
    }



}
