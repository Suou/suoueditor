﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase;

namespace SuouEditor.SuouOperator
{
    public class ToolStripScriptButton
    {
        public string Title { get; set; }
        public string FullPath { get; set; }
        public Action<string> Action { get; set; }
        public ToolStripScriptButton(string name, string fullpath, Action<string> t)
        {
            this.Title = name;
            this.FullPath = fullpath;
            this.Action = t;
        }
        public ToolStripMenuItem Create()
        {
            var f = new ToolStripMenuItem();
            f.Text = this.Title;
            f.Click += (a, b) => this.Action(this.FullPath);
            return f;
        }

    }
}
