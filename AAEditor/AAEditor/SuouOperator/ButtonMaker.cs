﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase.Interface;
using SuouBase;
using AAEditTabControl;
using BSystem.Collections;
using SuouBase.Book;

namespace SuouEditor
{
    public class HanyouButtonMaker
    {

        public static void CreateButtons(ToolStripMenuItem tsmi, string[] Names, EventHandler eh)
        {
            foreach (string s in Names)
            {
                ToolStripMenuItem fruitToolStripMenuItem = new ToolStripMenuItem(s, null, eh, s);
                fruitToolStripMenuItem.Tag = s;

                tsmi.DropDownItems.Add(fruitToolStripMenuItem);
            }
        }

        
    }

    public class RecentFileButtonMaker
    {
        private ToolStripMenuItem Button { get; set; }
        private ToolStripMenuItem ParentButton { get; set; }
        private EditorInfo DLLFunction { get; set; }
        public int MaxCount = 8;

        public RecentFileButtonMaker(ToolStripMenuItem button,ToolStripMenuItem parent,EditorInfo dllfunction)
        {
            this.Button = button;
            this.ParentButton = parent;
            this.DLLFunction = dllfunction;
            this.DLLFunction.FileData.TabSizeChanged += this.TabSizeChanged;
            ParentButton.DropDownOpening += this.ParentButton_DropDownOpening;
        }
        private void ParentButton_DropDownOpening(object sender,EventArgs e)
        {
            this.Button.DropDownItems.Clear();
            foreach (var f in this.DLLFunction.Setting.RecentUsingFileList)
            {
                var name = Path.GetFileName(f);
                ToolStripMenuItem item = new ToolStripMenuItem(name, null, this.OnClick, name);
                item.Tag = f;
                this.Button.DropDownItems.Add(item);
            }
        }
        private void OnClick(object sender, EventArgs e)
        {
            var item = (ToolStripMenuItem)sender;
            var fileinfo = new FileInfo((string)item.Tag);
            this.DLLFunction.FileData.AddFilePath(fileinfo.FullName);
        }
        
        private void TabSizeChanged(object sender, BookContentChangeEventArgs e)
        {
            this.DLLFunction.Setting.RecentUsingFileList.LimitAdd(e.Book);
        }
    }

    /*
    public class ButtonMaker
    {
        private ToolStripMenuItem ParentButton { get; set; }
        private event EventHandler ClickEvent;
        private List<FileInfo> _ButtonNames = new List<FileInfo>();
        private int Num;

        public List<FileInfo> ButtonNames
        {
            get
            {
                return this._ButtonNames;
            }
            set
            {
                this._ButtonNames = value;
            }
        }

        public ButtonMaker(ToolStripMenuItem tsmi,EventHandler eh,int n)
        {
            this.ParentButton = tsmi;
            this.ClickEvent = eh;
            this.Num = n;
        }

        public static void CreateButtons(ToolStripMenuItem tsmi,string[] Names, EventHandler eh)
        {
            var m = new ButtonMaker(tsmi, eh, 100);
        }

        private void Reload()
        {
        }

        public void Add(string filename)
        {
            var p = new FileInfo(filename);
            if (!p.Exists) return;//ファイルが存在しなかったら加えない
            //ファイルが重複していたら加えない
            if (!_ButtonNames.Exists(v => { return v.FullName == p.FullName; }))
            {
                this._ButtonNames.Insert(0, p);
                WriteRecentFiles();
                if (this._ButtonNames.Count > this.Num) this._ButtonNames.RemoveAt(this.Num);
            }
        }

        private void WriteRecentFiles()
        {
            ParentButton.DropDownItems.Clear();

            
            foreach (FileInfo p in _ButtonNames)
            {
                ToolStripMenuItem fruitToolStripMenuItem = new ToolStripMenuItem(p.Name, null, this.ClickEvent, p.Name);
                
                fruitToolStripMenuItem.Tag = p.FullName;

                ParentButton.DropDownItems.Add(fruitToolStripMenuItem);
            }
        }
    }
    */
    
}
