﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase;
using AAEditTabControl;
using System.Xml;
using SuouBase.Expansion;
using SuouBase.Interface;
using SuouEditor.OptionalWindow;
using SuouEditor;
using SuouBase.Text;

namespace SuouEditor.SuouOperator
{
    public class WindowManager
    {
        
        //WebBrowsePlugin WebBrowserForm1;
        VersionWindow VersionWindow1 = null;
        HeartsWindow HeartsWindow1 = null;
        SettingWindow SettingWindow1 = null;
        //SettingIntegralControl SettingIntegralTabPage = null;
        LogForm logform1 = null;

        ControlTabForm mCTForm { get; set; }
        EditorInfo DLLFunction { get; set; }
        public PluginControler PluginControllerForm = new PluginControler();

        public WindowManager(ControlTabForm ctf, EditorInfo dll)
        {
            this.mCTForm = ctf;
            this.DLLFunction = dll;
        }


        

        public void ShowLogForm()
        {
            logform1 = logform1 ?? new LogForm();
            logform1.Show();


        }
        

        public void ShowPluginControler()
        {
            if (PluginControllerForm == null)
            {
                PluginControllerForm = new PluginControler();
            }
            PluginControllerForm.DLLFunction = DLLFunction;
            PluginControllerForm.Show();
            PluginControllerForm.Check();


        }
        

        public void ShowWebBrowser()
        {
            this.DLLFunction.PreviewPlugin.SetItem(this.DLLFunction.FileData.SelectData.StoryFile);
            this.DLLFunction.PreviewPlugin.Form.Show(this.mCTForm);
            //this.DLLFunction.TemplatePageViewPlugin
        }
        public void ShowHeartsWindow()
        {
            if (HeartsWindow1 == null) HeartsWindow1 = new HeartsWindow() { StartPosition = FormStartPosition.CenterParent};
            HeartsWindow1.Owner = mCTForm;
            HeartsWindow1.ShowDialog(this.mCTForm);

        }
        public void ShowVersionWindow()
        {
            if (VersionWindow1 == null) VersionWindow1 = new VersionWindow(this.DLLFunction) { FormBorderStyle = FormBorderStyle.FixedSingle,StartPosition = FormStartPosition.CenterParent};
            VersionWindow1.TopLevel = true;
            VersionWindow1.Owner = mCTForm;
            VersionWindow1.ShowRandomText();
            VersionWindow1.Show();
        }
        public void ShowSettingWindow()
        {
            SettingWindow1 = new SettingWindow(this.DLLFunction);
            //SettingWindow1.TopLevel = true;
            SettingWindow1.Owner = mCTForm;
            SettingWindow1.ShowDialog();
        }
    }
}
