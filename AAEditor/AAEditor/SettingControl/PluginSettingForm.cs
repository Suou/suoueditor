﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Plugin;
using SuouBase;
using SuouBase.Interface;
namespace SuouEditor.SettingControl
{
    public partial class PluginSettingForm : Form
    {
        public PluginSettingForm()
        {
            InitializeComponent();
        }
        public PluginSettingForm(EditorInfo pd):this()
        {
            this.DLLFunction = pd;
        }
        public EditorInfo DLLFunction { get; set; }
        private void button1_Click(object sender, EventArgs e)
        {
            if (this.radioButton1.Checked)
            {
                //this.CreateAAFormer();
            }
            else
            {
                this.CreateAASelecter();
            }
        }
        /*
        private void CreateAAFormer()
        {
            var selecter = this.DLLFunction.PluginList.FirstOrDefault((v) => v.ClassName == "AASelecterPlugin");
            if (selecter != null)
            {
                this.DLLFunction.Remove(selecter);
            }
            var f = PluginSet.AASelectFormerPlugin;
            f.Plugin = new StoryViewer.AAFormer.AASelectFormPlugin(this.DLLFunction);
            f.Position = -1;
            PluginOperator.DirectCall(this.DLLFunction, f);
        }
        */
        private void CreateAASelecter()
        {
            var former = this.DLLFunction.PluginList.FirstOrDefault((v) => v.ClassName == "AASelectFormPlugin");
            if (former != null)
            {
                this.DLLFunction.Remove(former);
            }
            var f = PluginSet.AASelecterPlugin;
            f.Plugin = new StoryViewer.AASelecter.AASelecterPlugin(this.DLLFunction);
            f.Position = 1;
            PluginOperator.DirectCall(this.DLLFunction, f);
            
        }
    }
}
