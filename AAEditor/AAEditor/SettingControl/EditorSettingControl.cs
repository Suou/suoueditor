﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouEditor.SuouOperator;

namespace SuouEditor
{
    public partial class EditorSettingControl : UserControl, SettingPage
    {
        public EditorSettingControl()
        {
            InitializeComponent();
        }
        public EditorSettingControl(EditorInfo dll):this()
        {
            this.DLLFunction = dll;

            //this.StoryFileFolder = this.DLLFunction.Setting.StoryFileFolder;
            this.checkBox1.Checked = this.DLLFunction.Setting.EnableBackup;
            this.Check();

        }
        public EditorInfo DLLFunction { get; set; }
        public ControlTabForm ParentControl { get; set; }

        //public string StoryFileFolder { get; set; }

        public Control Ctrl { get { return this; } }
        public void OK()
        {

            DLLFunction.Setting.EnableBackup = this.checkBox1.Checked;
            //DLLFunction.Setting.StoryFileFolder = this.StoryFileFolder;
        }
        public void Cancel()
        {
        }
        public string PageName { get { return "全体オプション(設定)"; } }

        public void Check()
        {
            //this.textBox1.Text = this.StoryFileFolder;
        }
        private void textBox1_Click(object sender, EventArgs e)
        {
            var v = this.folderBrowserDialog1.ShowDialog();
            if (v == DialogResult.OK)
            {
                //this.StoryFileFolder = this.folderBrowserDialog1.SelectedPath;
                this.Check();
            }
        }
    }
}
