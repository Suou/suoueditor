﻿namespace SuouEditor
{
    partial class SettingPage1
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.shortCutRegister1 = new SuouBase.Controls.ShortCutRegister();
            this.shortCutRegister2 = new SuouBase.Controls.ShortCutRegister();
            this.SuspendLayout();
            // 
            // shortCutRegister1
            // 
            this.shortCutRegister1.Dock = System.Windows.Forms.DockStyle.Top;
            this.shortCutRegister1.Items = null;
            this.shortCutRegister1.Location = new System.Drawing.Point(0, 0);
            this.shortCutRegister1.Name = "shortCutRegister1";
            this.shortCutRegister1.Size = new System.Drawing.Size(406, 142);
            this.shortCutRegister1.TabIndex = 0;
            // 
            // shortCutRegister2
            // 
            this.shortCutRegister2.Dock = System.Windows.Forms.DockStyle.Top;
            this.shortCutRegister2.Items = null;
            this.shortCutRegister2.Location = new System.Drawing.Point(0, 142);
            this.shortCutRegister2.Name = "shortCutRegister2";
            this.shortCutRegister2.Size = new System.Drawing.Size(406, 194);
            this.shortCutRegister2.TabIndex = 1;
            // 
            // SettingPage1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.shortCutRegister2);
            this.Controls.Add(this.shortCutRegister1);
            this.Name = "SettingPage1";
            this.Size = new System.Drawing.Size(406, 339);
            this.ResumeLayout(false);

        }

        #endregion

        private SuouBase.Controls.ShortCutRegister shortCutRegister1;
        private SuouBase.Controls.ShortCutRegister shortCutRegister2;

    }
}
