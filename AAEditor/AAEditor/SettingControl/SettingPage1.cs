﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouEditor.SuouOperator;

namespace SuouEditor
{
    public partial class SettingPage1 : UserControl,SettingPage
    {
        public SettingPage1()
        {
            InitializeComponent();
            
        }
        public ControlTabForm ParentControl { get; set; }
        public ShortCutCommandCollection MainMenuShortCutKeys
        {
            get { return this.shortCutRegister1.Items; }
            set { this.shortCutRegister1.Items = value;}
        }
        public ShortCutCommandCollection FileMenuShortCutKeys
        {
            get { return this.shortCutRegister2.Items; }
            set { this.shortCutRegister2.Items = value;}
        }
        
        public Control Ctrl { get { return this; } }
        public void OK()
        {
            /*
            this.MainMenuShortCutKeys = this.ParentControl.suouEditorMenuStrip1.MainMenuShortCutKeys;
            this.FileMenuShortCutKeys = this.ParentControl.suouEditorMenuStrip1.FileMenuShortCutKeys;
            
            this.shortCutRegister1.Determine();
            this.shortCutRegister2.Determine();

            this.ParentControl.suouEditorMenuStrip1.MainMenuShortCutKeys = (MasterFormMenuShortCutKeys)this.MainMenuShortCutKeys;
            this.ParentControl.suouEditorMenuStrip1.FileMenuShortCutKeys = (MasterFormFileMenuShortCutKeys)this.FileMenuShortCutKeys;
            */
            
        }
        public void Cancel()
        {
        }
        public string PageName { get { return "全体オプション(キー)"; } }
    }
}
