﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Threading.Tasks;
using SuouBase;
using SuouBase.Interface;
using AAEditTabControl;
using SuouBase.Text;
using SuouEditor.SuouOperator;
using BSystem.Collections;
using SuouBase.Book;
namespace SuouEditor
{
    public class SuouEditorMenuStrip : MenuStrip
    {
        private ToolStripMenuItem toolMenuItem1;
        private ToolStripMenuItem otherMenuItem;
        private ToolStripMenuItem file_NewFileMenuItem;
        private ToolStripMenuItem file_OpenMenuItem;
        private ToolStripMenuItem file_OverwriteMenuItem;
        private ToolStripMenuItem file_writeWithNameMenuItem;
        private ToolStripMenuItem file_OutputMenuItem;
        private ToolStripMenuItem file_recentMenuItem;
        private ToolStripMenuItem output_mltMenuItem;
        private ToolStripMenuItem output_astMenuItem;
        private ToolStripMenuItem output_secMenuItem;
        private OpenFileDialog openFileDialog1;
        private ToolStripMenuItem tool_previewMenuItem;
        private ToolStripMenuItem other_optionMenuItem;
        private ToolStripMenuItem tool_largeMenuItem;
        private ToolStripMenuItem other_versionMenuItem;
        private ToolStripMenuItem tool_bookdataMenuItem;
        private ToolStripMenuItem other_scriptMenuItem;
        private ToolStripMenuItem other_refreshMenuItem;
        private ToolStripMenuItem other_heratsMenuItem;
        private ToolStripMenuItem other_viewerViewerMenuItem;
        private ToolStripMenuItem FileMenuItem;
    
        private void InitializeComponent()
        {
            this.FileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.file_NewFileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.file_OpenMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.file_OverwriteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.file_writeWithNameMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.file_OutputMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.output_mltMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.output_astMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.output_secMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.file_recentMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tool_previewMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tool_bookdataMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tool_largeMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.other_optionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.otherMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.other_versionMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.other_scriptMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.other_refreshMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.other_heratsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.other_viewerViewerMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SuspendLayout();
            // 
            // FileMenuItem
            // 
            this.FileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.file_NewFileMenuItem,
            this.file_OpenMenuItem,
            this.file_OverwriteMenuItem,
            this.file_writeWithNameMenuItem,
            this.file_OutputMenuItem,
            this.file_recentMenuItem});
            this.FileMenuItem.Name = "FileMenuItem";
            this.FileMenuItem.Size = new System.Drawing.Size(68, 20);
            this.FileMenuItem.Text = "ファイル";
            this.FileMenuItem.DropDownOpening += new System.EventHandler(this.FileMenuItem_DropDownOpening);
            // 
            // file_NewFileMenuItem
            // 
            this.file_NewFileMenuItem.Name = "file_NewFileMenuItem";
            this.file_NewFileMenuItem.Size = new System.Drawing.Size(184, 22);
            this.file_NewFileMenuItem.Text = "新規作成";
            this.file_NewFileMenuItem.Click += new System.EventHandler(this.file_NewFileMenuItem_Click);
            // 
            // file_OpenMenuItem
            // 
            this.file_OpenMenuItem.Name = "file_OpenMenuItem";
            this.file_OpenMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.file_OpenMenuItem.Size = new System.Drawing.Size(184, 22);
            this.file_OpenMenuItem.Text = "開く";
            this.file_OpenMenuItem.Click += new System.EventHandler(this.file_OpenMenuItem_Click);
            // 
            // file_OverwriteMenuItem
            // 
            this.file_OverwriteMenuItem.Name = "file_OverwriteMenuItem";
            this.file_OverwriteMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.file_OverwriteMenuItem.Size = new System.Drawing.Size(184, 22);
            this.file_OverwriteMenuItem.Text = "上書き保存";
            this.file_OverwriteMenuItem.Click += new System.EventHandler(this.file_OverwriteMenuItem_Click);
            // 
            // file_writeWithNameMenuItem
            // 
            this.file_writeWithNameMenuItem.Name = "file_writeWithNameMenuItem";
            this.file_writeWithNameMenuItem.Size = new System.Drawing.Size(184, 22);
            this.file_writeWithNameMenuItem.Text = "名前をつけて保存";
            this.file_writeWithNameMenuItem.Click += new System.EventHandler(this.file_writeWithNameMenuItem_Click);
            // 
            // file_OutputMenuItem
            // 
            this.file_OutputMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.output_mltMenuItem,
            this.output_astMenuItem,
            this.output_secMenuItem});
            this.file_OutputMenuItem.Name = "file_OutputMenuItem";
            this.file_OutputMenuItem.Size = new System.Drawing.Size(184, 22);
            this.file_OutputMenuItem.Text = "ファイル出力";
            // 
            // output_mltMenuItem
            // 
            this.output_mltMenuItem.Name = "output_mltMenuItem";
            this.output_mltMenuItem.Size = new System.Drawing.Size(160, 22);
            this.output_mltMenuItem.Text = "MLT形式で出力";
            this.output_mltMenuItem.Click += new System.EventHandler(this.output_mltMenuItem_Click);
            // 
            // output_astMenuItem
            // 
            this.output_astMenuItem.Name = "output_astMenuItem";
            this.output_astMenuItem.Size = new System.Drawing.Size(160, 22);
            this.output_astMenuItem.Text = "AST形式で出力";
            this.output_astMenuItem.Click += new System.EventHandler(this.output_astMenuItem_Click);
            // 
            // output_secMenuItem
            // 
            this.output_secMenuItem.Name = "output_secMenuItem";
            this.output_secMenuItem.Size = new System.Drawing.Size(160, 22);
            this.output_secMenuItem.Text = "SEC形式で出力";
            this.output_secMenuItem.Click += new System.EventHandler(this.output_secMenuItem_Click);
            // 
            // file_recentMenuItem
            // 
            this.file_recentMenuItem.Name = "file_recentMenuItem";
            this.file_recentMenuItem.Size = new System.Drawing.Size(184, 22);
            this.file_recentMenuItem.Text = "最近使ったファイル";
            // 
            // toolMenuItem1
            // 
            this.toolMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tool_previewMenuItem,
            this.tool_bookdataMenuItem,
            this.tool_largeMenuItem});
            this.toolMenuItem1.Name = "toolMenuItem1";
            this.toolMenuItem1.Size = new System.Drawing.Size(56, 20);
            this.toolMenuItem1.Text = "ツール";
            this.toolMenuItem1.DropDownOpening += new System.EventHandler(this.toolMenuItem1_DropDownOpening);
            // 
            // tool_previewMenuItem
            // 
            this.tool_previewMenuItem.Name = "tool_previewMenuItem";
            this.tool_previewMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tool_previewMenuItem.Text = "プレビュー";
            this.tool_previewMenuItem.Click += new System.EventHandler(this.tool_previewMenuItem_Click);
            // 
            // tool_bookdataMenuItem
            // 
            this.tool_bookdataMenuItem.Name = "tool_bookdataMenuItem";
            this.tool_bookdataMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tool_bookdataMenuItem.Text = "ブックデータ";
            this.tool_bookdataMenuItem.ToolTipText = "ページサイズなどの一覧を表示します";
            this.tool_bookdataMenuItem.Click += new System.EventHandler(this.tool_bookdataMenuItem_Click);
            // 
            // tool_largeMenuItem
            // 
            this.tool_largeMenuItem.Name = "tool_largeMenuItem";
            this.tool_largeMenuItem.Size = new System.Drawing.Size(152, 22);
            this.tool_largeMenuItem.Text = "巨大文字作成";
            this.tool_largeMenuItem.Click += new System.EventHandler(this.tool_largeMenuItem_Click);
            // 
            // other_optionMenuItem
            // 
            this.other_optionMenuItem.Name = "other_optionMenuItem";
            this.other_optionMenuItem.Size = new System.Drawing.Size(196, 22);
            this.other_optionMenuItem.Text = "オプション";
            this.other_optionMenuItem.Click += new System.EventHandler(this.other_optionMenuItem_Click);
            // 
            // otherMenuItem
            // 
            this.otherMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.other_optionMenuItem,
            this.other_versionMenuItem,
            this.other_scriptMenuItem,
            this.other_refreshMenuItem,
            this.other_heratsMenuItem,
            this.other_viewerViewerMenuItem});
            this.otherMenuItem.Name = "otherMenuItem";
            this.otherMenuItem.Size = new System.Drawing.Size(56, 20);
            this.otherMenuItem.Text = "その他";
            // 
            // other_versionMenuItem
            // 
            this.other_versionMenuItem.Name = "other_versionMenuItem";
            this.other_versionMenuItem.Size = new System.Drawing.Size(196, 22);
            this.other_versionMenuItem.Text = "バージョン情報";
            this.other_versionMenuItem.Click += new System.EventHandler(this.other_versionMenuItem_Click);
            // 
            // other_scriptMenuItem
            // 
            this.other_scriptMenuItem.Enabled = false;
            this.other_scriptMenuItem.Name = "other_scriptMenuItem";
            this.other_scriptMenuItem.Size = new System.Drawing.Size(196, 22);
            this.other_scriptMenuItem.Text = "スクリプト";
            // 
            // other_refreshMenuItem
            // 
            this.other_refreshMenuItem.Name = "other_refreshMenuItem";
            this.other_refreshMenuItem.Size = new System.Drawing.Size(196, 22);
            this.other_refreshMenuItem.Text = "プラグイン再読み込み";
            this.other_refreshMenuItem.Click += new System.EventHandler(this.other_refreshMenuItem_Click);
            // 
            // other_heratsMenuItem
            // 
            this.other_heratsMenuItem.Name = "other_heratsMenuItem";
            this.other_heratsMenuItem.Size = new System.Drawing.Size(196, 22);
            this.other_heratsMenuItem.Text = "ハーツによせて";
            this.other_heratsMenuItem.Click += new System.EventHandler(this.other_heratsMenuItem_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "ファイル(*.ast;*.mlt;*sst3;*sst4)|*.ast;*.mlt;*sst3;*sst4|すべてのファイル(*.*)|*.*";
            this.openFileDialog1.RestoreDirectory = true;
            this.openFileDialog1.Title = "開くファイルを選択してください";
            // 
            // other_viewerViewerMenuItem
            // 
            this.other_viewerViewerMenuItem.Name = "other_viewerViewerMenuItem";
            this.other_viewerViewerMenuItem.Size = new System.Drawing.Size(196, 22);
            this.other_viewerViewerMenuItem.Text = "ビューワー一覧";
            this.other_viewerViewerMenuItem.Click += new System.EventHandler(this.other_viewerViewerMenuItem_Click);
            // 
            // SuouEditorMenuStrip
            // 
            this.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.FileMenuItem,
            this.toolMenuItem1,
            this.otherMenuItem});
            this.ResumeLayout(false);

        }
        private Operators _OperationObject = null;
        public Operators OperationObject
        {
            get { return this._OperationObject; }
            set
            {
                if (value == null) return;
                this._OperationObject = value;
                this.Set();
            }
        }
        public SuouEditorMenuStrip():base()
        {
            this.InitializeComponent();
        }
        public void Set()
        {
            //this.FileMenuShortCutKeys = new MasterFormFileMenuShortCutKeys();
            //this.MainMenuShortCutKeys = new MasterFormMenuShortCutKeys();
            this.OperationObject.RecentMenuButtons = new RecentFileButtonMaker(this.file_recentMenuItem, this.FileMenuItem, this.OperationObject.DLLFunction);
            /*
            {
                var folder = this.OperationObject.DLLFunction.Setting.StoryFileFolder;
                var io = new DirectoryInfo(folder);
                if (io.Exists) this.openFileDialog1.InitialDirectory = folder;
            }
            */
            this.openFileDialog1.Filter = SuouBase.SuouEditorConst.ExtFilter;
            this.OperationObject.DLLFunction.FileData.SaveChanged += (sender, args) =>
            {
                this.OperationObject.DLLFunction.Setting.RecentUsingFileList.LimitAdd((SuouBase.Book.IBook)sender);
            };
            
        }

        #region File

        private void new_chpMenuItem_Click(object sender, EventArgs e)
        {

            OperationObject.GeneralData.AddNewFile(AAFileExtension.Gst);
        }

        private void file_OpenMenuItem_Click(object sender, EventArgs e)
        {
            var v = this.openFileDialog1.ShowDialog();
            string filename = null;
            if (v == System.Windows.Forms.DialogResult.OK) filename = this.openFileDialog1.FileName;
            if (filename != null)
            {
                ErrorMessage.Start("ReadStoryFile");
                
                OperationObject.GeneralData.AddFilePath(filename);
            }
        }

        private void file_OverwriteMenuItem_Click(object sender, EventArgs e)
        {
            var b = OperationObject.GeneralData.SaveOverRide();
            if (b)
            {
                MessageBox.Show("保存しました");
            }
        }

        private void file_writeWithNameMenuItem_Click(object sender, EventArgs e)
        {
            var b = OperationObject.GeneralData.SaveWithName();
            if (b)
            {
                MessageBox.Show("保存しました");
            }
        }

        private void file_NewFileMenuItem_Click(object sender, EventArgs e)
        {

            OperationObject.GeneralData.AddNewFile(AAFileExtension.Gst);
        }

        private void output_mltMenuItem_Click(object sender, EventArgs e)
        {
            var fi = Path.GetFileNameWithoutExtension(this.OperationObject.GeneralData.SelectData.StoryFile.Name) + SuouEditorConst.MltExtension;

            string filename = GeneralMethod.OutPutSaveDialogBox(fi);
            if (filename != null)
            {
                this.OperationObject.DLLFunction.FileData.SelectData.PutFile(filename);
                MessageBox.Show("出力しました");
            }
            
        }

        private void output_astMenuItem_Click(object sender, EventArgs e)
        {
            var fi = Path.GetFileNameWithoutExtension(this.OperationObject.GeneralData.SelectData.StoryFile.Name) + SuouEditorConst.AstExtension;

            string filename = GeneralMethod.OutPutSaveDialogBox(fi);
            if (filename != null)
            {
                this.OperationObject.DLLFunction.FileData.SelectData.PutFile(filename);
                MessageBox.Show("出力しました");
            }
        }

        private void output_secMenuItem_Click(object sender, EventArgs e)
        {
            var fi = Path.GetFileNameWithoutExtension(this.OperationObject.GeneralData.SelectData.StoryFile.Name) + SuouEditorConst.SectionBookExtension;

            string filename = GeneralMethod.OutPutSaveDialogBox(fi);
            if (filename != null)
            {
                this.OperationObject.DLLFunction.FileData.SelectData.PutFile(filename);
                MessageBox.Show("出力しました");
            }
        }


        private void FileMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            if (OperationObject.GeneralData.SelectData == null)
            {
                file_OverwriteMenuItem.Enabled = false;
                file_writeWithNameMenuItem.Enabled = false;
                file_OutputMenuItem.Enabled = false;
            }
            else
            {

                file_OverwriteMenuItem.Enabled = true;
                file_writeWithNameMenuItem.Enabled = true;
                file_OutputMenuItem.Enabled = true;
            }
        }
        #endregion
        #region Tool
        private void toolMenuItem1_DropDownOpening(object sender, EventArgs e)
        {
            var SelectData = this.OperationObject.DLLFunction.FileData.SelectData;
            if (SelectData == null)
            {
                this.tool_previewMenuItem.Enabled = false;
                this.tool_bookdataMenuItem.Enabled = false;
            }
            else
            {
                this.tool_bookdataMenuItem.Enabled = true;
                if (SelectData.StoryFile.Count == 0)
                {
                    this.tool_previewMenuItem.Enabled = false;
                }
                else
                {
                    this.tool_previewMenuItem.Enabled = true;
                }
            }
        }

        private void tool_previewMenuItem_Click(object sender, EventArgs e)
        {

            OperationObject.WindowMangerClass.ShowWebBrowser();
        }


        private void tool_largeMenuItem_Click(object sender, EventArgs e)
        {

            var v = StoryViewer.LargeCharBox.Create(this.OperationObject.DLLFunction);

            v.Show();
        }
        private void tool_bookdataMenuItem_Click(object sender, EventArgs e)
        {

            if (this.OperationObject.DLLFunction.FileData.SelectData == null) return;
            var v = new SuouBase.Controls.IBookEViewer() { DLLFunction = this.OperationObject.DLLFunction };
            v.Item = this.OperationObject.DLLFunction.FileData.SelectData.StoryFile;
            var f = new BSystem.Controls.SingleForm(v, "ブックデータ") { Icon = SuouTextBox2.Properties.Resources.turu_perple};
            f.StartPosition = FormStartPosition.CenterParent;
            f.Size = new System.Drawing.Size(500, 600);
            f.Show(this.OperationObject.DLLFunction.MasterForm.Form);
        }
        #endregion

        #region Other

        private void other_optionMenuItem_Click(object sender, EventArgs e)
        {
            OperationObject.WindowMangerClass.ShowSettingWindow();

        }
        private void other_versionMenuItem_Click(object sender, EventArgs e)
        {

            OperationObject.WindowMangerClass.ShowVersionWindow();
        }

        private void other_refreshMenuItem_Click(object sender, EventArgs e)
        {
            this.OperationObject.PluginManagerClass.InitializePluginList();

        }
        public void MenuKeyRefresh()
        {
            /*
            this.SettingPageCtrl.FileMenuShortCutKeys = this.FileMenuShortCutKeys;
            this.SettingPageCtrl.MainMenuShortCutKeys = this.MainMenuShortCutKeys;
            this.SettingPageCtrl.ParentControl = this;
            */
        }

        

        private void other_heratsMenuItem_Click(object sender, EventArgs e)
        {
            OperationObject.WindowMangerClass.ShowHeartsWindow();


        }
        #endregion

        private void other_viewerViewerMenuItem_Click(object sender, EventArgs e)
        {
            var p = new SuouBase.Controls.Plugin.ViewerViewer(this.OperationObject.DLLFunction);
            p.StartPosition = FormStartPosition.CenterParent;
            p.ShowDialog(this.FindForm());
        }

    }
}
