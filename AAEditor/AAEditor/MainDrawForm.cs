﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using YAEExtension;
using Microsoft.VisualBasic;
using System.Windows.Forms;


namespace WindowsFormsApplication1
{

    public partial class MainDrawForm : Form
    {
        public ControlTabForm pointer;



        public MainDrawForm()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            StopWatcher.Stop();
            //this.OATB.SetKeyCheckFunc(KeyCheck);

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            //OATB.SetSizeFunc(StatusPageSize);
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {   
        }

        public void KeyCheck(Keys k)
        {
            pointer.AAES.FocusMode(k);
        }

        public void AddText(List<string> LText)
        {
            this.OATB.AddListText(LText);
            //WindowsMode(WindowMode.ControlTabFormMode);
            //pointer.WindowsMode(WindowMode.ViewMainDrawMode);
        }

        private void MainDrawForm_MouseCaptureChanged(object sender, EventArgs e)
        {
            pointer.AAES.WindowsMode(WindowMode.MainDrawFormMode);
        }

        private void MainDrawForm_Deactivate(object sender, EventArgs e)
        {
            pointer.TopMost = false;
        }

        public void StatusPageSize(AstPage page)
        {
            statusStrip1.Items[0].Text = page.PageSize().ToString();
            statusStrip1.Refresh();
        }

        private void MainDrawForm_Move(object sender, EventArgs e)
        {
            //if (this.Location.Y > 10) this.Location = new Point(0, 0);
        }

    }
}
