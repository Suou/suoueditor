﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.IO;               // デイレクトリ検索に用いるクラス
using System.Management;  // ドライブ情報取得に用いるクラス
using SuouBase;
using AAEditTabControl;
using System.Xml;
using SuouBase.Expansion;
using SuouBase.Interface;
using SuouEditor.OptionalWindow;
using SuouEditor;

namespace SuouEditor
{
    class PluginSet
    {
        public static IComponentPluginMetaData StockBoxPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.AAToolBox", "StockBoxPlugin", "ストック");
        public static IComponentPluginMetaData AASelectFormerPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.AAFormer", "AASelectFormPlugin", "AASelectFormPlugin");
        public static IComponentPluginMetaData AATemplateSelectControlPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.AAFormer", "AATemplateSelectControlPlugin", "AATemplateSelectControlPlugin");
        public static IComponentPluginMetaData StoryPagePlugin = new IComponentPluginMetaData("AAEditTabControl", "AAEditTabControl", "StoryListOperation","StoryList");
        public static IComponentPluginMetaData StoryViewerPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer", "StoryViewerPlugin", "StoryViewerPlugin");
        public static IComponentPluginMetaData SuperStoryViewerPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.IStoryViewer", "IStoryViewerPlugin", "SuperStoryViewerPlugin");

        public static IComponentPluginMetaData StandardEditorPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.BookEditor.StandardEditor", "StandardEditorPlugin", "StandardEditorPlugin");
        public static IComponentPluginMetaData OutlineEditorPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.OutlineEditor", "OutlineEditorPlugin", "OutlineEditorPlugin");
        
        public static IComponentPluginMetaData ChapterListBoxPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.BookEditor.ChapterViewer", "ChapterListBoxPlugin", "ChapterListBoxPlugin");


        //public static IComponentPluginMetaData TextBoxPlugin = new IComponentPluginMetaData("SuouTextBox", "SuouTextBox2", "SuouTextBoxPlugin", "SuouText");
        public static IComponentPluginMetaData SuouTextBox2Plugin = new IComponentPluginMetaData("SuouTextBox2", "SuouTextBox2", "SuouTextBoxPlugin", "SuouTextBox2");
        public static IComponentPluginMetaData LocalTemplatePlugin = new IComponentPluginMetaData("AAEditTabControl", "AAEditTabControl", "LocalTemplateOperation", "LocalTemplate");
        public static IComponentPluginMetaData TemplatePageViewPlugin = new IComponentPluginMetaData("AAEditTabControl", "AAEditTabControl.TemplateViewListForm", "TemplateViewListClass", "TemplateViewList");
        public static IComponentPluginMetaData GlobalTemplatePlugin = new IComponentPluginMetaData("AAEditTabControl", "AAEditTabControl.SimpleGlobalTemplate", "SimpleGlobalTemplate", "MLTツリー");
        public static IComponentPluginMetaData AAThumbChoicerPlugin = new IComponentPluginMetaData("AAEditTabControl", "AAEditTabControl.AAChoicer", "AAChoicerPlugin", "AAChoicerPlugin");
        public static IComponentPluginMetaData AAThumbChoicerPlugin2 = new IComponentPluginMetaData("StoryViewer", "StoryViewer", "AAChoicerPlugin", "AA");
        public static IComponentPluginMetaData ThumbnailViewPlugin = new IComponentPluginMetaData("AAEditTabControl", "AAEditTabControl.AAThumbnailView", "AAThumbnailViewPlugin", "AAThumbnailViewPlugin");
        public static IComponentPluginMetaData AAToolBoxPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer", "AAToolBoxPlugin", "箱");
        public static IComponentPluginMetaData MojiToolPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer", "MojiToolPlugin", "文字");
        public static IComponentPluginMetaData HukidasiToolPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer", "HukidasiToolPlugin", "噴出し");
        public static IComponentPluginMetaData StoryViewerEditPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer", "StoryViewerEditPlugin", "StoryViewerEdit");
        public static IComponentPluginMetaData LayerSupervisorPlugin = new IComponentPluginMetaData("SuouTextBox2", "SuouTextBox2.Controls", "LayerSupervisorPlugin", "レイヤー");
        public static IComponentPluginMetaData PreviewPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer", "PreviewPlugin", "PreviewPlugin");
        public static IComponentPluginMetaData AASelecterPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.AASelecter", "AASelecterPlugin", "AA選択");
        public static IComponentPluginMetaData TestPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.IStoryViewer", "TestPlugin", "test");

        public static IComponentPluginMetaData SuouViewerPlugin = new IComponentPluginMetaData("StoryViewer", "StoryViewer.BookControl.Viewer.SuouViewer", "SuouViewerPlugin", "蘇芳ビューワー");

        public static IFrameworkPluginMetaData StandardFramework = new IFrameworkPluginMetaData("AAEditTabControl", "AAEditTabControl.Framework", "StandardFramework", "StandardFramework");
        public static IFrameworkPluginMetaData DrawerFramework = new IFrameworkPluginMetaData("AAEditTabControl", "AAEditTabControl.Framework", "DrawerFramework", "DrawerFramework");
        public static IFrameworkPluginMetaData TripleFramework = new IFrameworkPluginMetaData("AAEditTabControl", "AAEditTabControl.Framework", "TripleFrame", "TripleFrame");
        public static IFrameworkPluginMetaData ViewerFramework = new IFrameworkPluginMetaData("AAEditTabControl", "AAEditTabControl.Framework.ViewerFrame", "ViewerFramework", "ViewerFramework");
        public static IFrameworkPluginMetaData SimpleFramework = new IFrameworkPluginMetaData("StoryViewer", "StoryViewer.Framework.SimpleFramework", "SimpleFramePlugin", "SimpleFramePlugin");
        
        public static CompleteSet SimpleFrameSet
        {
            get
            {
                CompleteSet cs = new CompleteSet();
                cs.Frame = SimpleFramework;

                //cs.PluginSet.Add(new IComponentPluginMetaData(SuperStoryViewerPlugin, 0, null));
                
                cs.PluginSet.Add(new IComponentPluginMetaData(StandardEditorPlugin, 0, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(ChapterListBoxPlugin, 7, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(MojiToolPlugin, 12, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(HukidasiToolPlugin, 13, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(OutlineEditorPlugin, -1, null));

                cs.PluginSet.Add(new IComponentPluginMetaData(LayerSupervisorPlugin, 14, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(SuouTextBox2Plugin, 11, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(PreviewPlugin, -1, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(SuouViewerPlugin, 6, null));

                return cs;
            }
        }
        /*
        public static CompleteSet DrawerFrameSet
        {
            get
            {
                CompleteSet cs = new CompleteSet();
                cs.Frame = DrawerFramework;

                cs.PluginSet.Add(new IComponentPluginMetaData(StoryPagePlugin, 1, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(AAThumbChoicerPlugin, -1, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(AAToolBoxPlugin, 2, null));
                return cs;
            }
        }
        public static CompleteSet StoryViewerSet
        {
            get
            {
                CompleteSet cs = new CompleteSet();
                cs.Frame = ViewerFramework;

                cs.PluginSet.Add(new IComponentPluginMetaData(StoryViewerPlugin, 0, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(AAThumbChoicerPlugin2, 2, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(SuouTextBox2Plugin, 3, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(MojiToolPlugin, 5, null));
                return cs;
            }
        }
        public static CompleteSet StoryViewerEditSet
        {
            get
            {
                CompleteSet cs = new CompleteSet();
                cs.Frame = ViewerFramework;

                cs.PluginSet.Add(new IComponentPluginMetaData(StoryViewerEditPlugin, 0, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(AAThumbChoicerPlugin2, 2, null));
                
                cs.PluginSet.Add(new IComponentPluginMetaData(MojiToolPlugin, 5, null));
                return cs;
            }
        }
        public static CompleteSet StandardSet
        {
            

            get
            {
                CompleteSet cs = new CompleteSet();
                cs.Frame = StandardFramework;

                cs.PluginSet.Add(new IComponentPluginMetaData(StoryPagePlugin, 4,null));
                //cs.PluginSet.Add(new IComponentPluginMetaData(TextBoxPlugin, 0,null));
                cs.PluginSet.Add(new IComponentPluginMetaData(LocalTemplatePlugin, 5, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(TemplatePageViewPlugin, -1, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(GlobalTemplatePlugin, -1, null));
                return cs;
            }
        }
        public static CompleteSet TripleSet
        {


            get
            {
                CompleteSet cs = new CompleteSet();
                cs.Frame = TripleFramework;

                cs.PluginSet.Add(new IComponentPluginMetaData(StoryPagePlugin, 2, null));
                //cs.PluginSet.Add(new IComponentPluginMetaData(TextBoxPlugin, 0, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(LocalTemplatePlugin, 11, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(ThumbnailViewPlugin, 9, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(GlobalTemplatePlugin, 14, null));
                cs.PluginSet.Add(new IComponentPluginMetaData(AAToolBoxPlugin, 5, new SuouFrameTagData() { IsOmit = true }));
                return cs;
            }
        }
        */
    }
}
