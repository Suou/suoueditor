﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;
using PreviewWindow;
namespace SuouBase.Expansion
{
    [IComponentPlugin("WebBrowserPlugin", PluginType.Form | PluginType.Control)]
    public class WebBrowsePlugin : IComponentPlugin, ITabPlugin
    {
        private WebBrowserFormSerialize SaveSerialize;

        private string SaveFileName = SuouEditorConst.GetSettingFile("WebBrowse.stn");
        public int Position { get; set; }
        private WebBrowserForm _Form;
        public Form Form { get { return _Form; } }
        public void TabClick()
        {
        }
        private WebBrowserControl _Control;
        public ContainerControl Control { get { return this._Control; } }
        public string Name { get { return "プレビュー"; } }
        public AstPage NowAstPage { set { this._Control.WebData.NowAstPage = value; } }
        public EditingFile NowFile { set { this._Control.WebData.NowFile = value; } }

        public void ViewAstPage()
        {
            this._Control.WebData.ViewAstPage();

        }

        public void Dispose()
        {
            SaveSerialize.Save(SaveFileName);
        }
        public SettingPage[] SettingPage
        {
            get
            {
                return null;
            }
        }
        


        public string NameSpace { get { return "SuouBase.Expansion"; } }
        public string PluginName { get { return "WebBrowsePlugin"; } }
        public string DLLName { get { return "PreviewWindow"; } }
        public string Class { get { return "WebBrowsePlugin"; } }


        public WebBrowsePlugin()
        {
            _Form = new WebBrowserForm();
            this._Control = new WebBrowserControl();
            SaveSerialize = new WebBrowserFormSerialize(_Form);
            SaveSerialize.Load(SaveFileName);
        }

        public void Preview(IBookPage ap, IBook ef)
        {
            EditingFile book = null;
            if (ef is EditingFile)
            {
                book = (EditingFile)ef;
            }
            else if (ef is MergeBook)
            {
                book = ((MergeBook)ef).ToEditingFile();
            }

            if (ap == null)
            {
                var p = ef[0];
                this._Control.WebData.Preview(p, book);
            }
            else
            {
                this._Control.WebData.Preview(ap, book);
            }
            
        }
    }
}
