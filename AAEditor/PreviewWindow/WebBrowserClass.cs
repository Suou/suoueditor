﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.IO;
using SuouBase.Text;

namespace PreviewWindow
{
    public class WebBrowserClass
    {
        public string Template;
        public IBookPage NowAstPage;
        public IBook NowFile;
        public WebBrowser WebControl;
        public WebBrowserClass(WebBrowser ctrl)
        {
            this.WebControl = ctrl;
        }
        public bool Read()
        {
            StreamReader sr;
            try
            {
                sr = new StreamReader(SuouEditorConst.GetDataFile("WebBrowser.html"), Encoding.GetEncoding("Shift_JIS"));
                Template = sr.ReadToEnd();
                sr.Close();
                IsOK = true;
            }
            catch
            {
                IsOK = false;
            }
            finally
            {

            }
            return IsOK;
        }
        private bool IsOK { get; set; }

        public void Preview(IBookPage ap, EditingFile ef)
        {
            if (!IsOK)
            {
                if (!Read())
                {
                    ErrorMessage.Show(ErrorMessage.Warning.NotFoundWebHTMLFile);
                    return;
                }
            }
            if (ap == null)
            {
                ErrorMessage.Show(ErrorMessage.Warning.NotFoundEditPage);
                return;
            }
            this.NowAstPage = ap;
            if (ef == null)
            {
                ErrorMessage.Show(ErrorMessage.Warning.NotFoundEditFile);
                return;
            }
            this.NowFile = ef;
            this.ViewAstPage();
            this.WebControl.Show();
        }

        private List<ViewAstPage> Kakou(IBookPage ap)
        {
            var v = new ViewAstPage(ap);

            var vv = new List<ViewAstPage>();
            vv.Add(v);
            return vv;
        }

        private List<ViewAstPage> Kakou(IBook ef)
        {

            var vv = new List<ViewAstPage>();
            foreach (IBookPage ap in ef)
            {
                var v = new ViewAstPage(ap);
                vv.Add(v);
            }

            return vv;
        }


        public void ViewAstPage()
        {

            var v = this.Kakou(this.NowAstPage);

            var grp = new Antlr3.ST.StringTemplate(Template);
            grp.SetAttribute("Pages", v);
            this.WebControl.DocumentText = grp.ToString();

        }

        public void ViewEditingFile()
        {
            var v = this.Kakou(this.NowFile);

            var grp = new Antlr3.ST.StringTemplate(Template);
            grp.SetAttribute("Pages", v);
            this.WebControl.DocumentText = grp.ToString();


        }
    }
}
