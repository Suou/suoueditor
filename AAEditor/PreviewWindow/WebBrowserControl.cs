﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.IO;
using SuouBase.Text;
namespace PreviewWindow
{
    public partial class WebBrowserControl : UserControl
    {
        public WebBrowserControl()
        {
            InitializeComponent();
            this.WebData = new WebBrowserClass(this.webBrowser1);
        }

        public WebBrowserClass WebData { get; set; }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WebData.ViewEditingFile();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.WebData.ViewAstPage();
        }

    }

}
