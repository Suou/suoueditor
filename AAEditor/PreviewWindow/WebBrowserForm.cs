﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Interface;
using System.IO;
using SuouBase.Text;
namespace SuouBase.Expansion
{



    public partial class WebBrowserForm : Form
    {
        public WebBrowserForm()
        {
            InitializeComponent();
            //Read();
        }

        private void WebBrowserForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Hide();
                e.Cancel = true;
            }
            else
            {
            }
        }

    }

    [Serializable()]
    public class WebBrowserFormSerialize
    {
        public Point Position;
        public Size Size;
        public FormWindowState FormState;
        //public int SplitLine1;
        //public int SplitLine2;
        [NonSerialized()]
        private WebBrowserForm form;

        public WebBrowserFormSerialize()
        {
        }

        public WebBrowserFormSerialize(WebBrowserForm form)
        {
            this.form = form;
        }

        public void Save(string filename)
        {
            if (form.WindowState == FormWindowState.Minimized)
            {
                return;
            }
            this.Position = form.Location;
            this.Size = form.Size;
            this.FormState = form.WindowState;

            SuouEditorConst.CheckFolder();

            BinarySerialize.SaveToBinaryFile(this, filename);
        }
        public bool Load(string filename)
        {
            FileInfo v = new FileInfo(filename);
            if (v.Exists)
            {

                WebBrowserFormSerialize cls = null;

                cls = (WebBrowserFormSerialize)BinarySerialize.LoadFromBinaryFile(filename);

                if (cls != null)
                {
                    form.Location = cls.Position;
                    form.Size = cls.Size;
                    form.WindowState = cls.FormState;
                }

            }
            return true;
        }
    }
}
