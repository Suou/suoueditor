﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BaseExtension;
using SuouBase.Text;

namespace SuouTest
{
    public class SuouTestMain
    {
        public static void Main()
        {
            var v = new UnitTest1();
            v.SuouTextMergeTest();
        }
    }
    [TestClass]
    public class UnitTest1
    {
        public static int UnitNumber = 100;
        [TestMethod]
        public void TestMethod1()
        {
            System.Threading.Thread.Sleep(100);
        }
        [TestMethod]
        public void SuouWordCreateTest()
        {
            Enumerable.Range(0,UnitNumber).Select((v)=>RandExtension.RandomSentence(1000)).ForEach((v)=>new SuouWord(v));
        }
        [TestMethod]
        public void SuouLineCreateTest()
        {
            Enumerable.Range(0, UnitNumber).Select((v) => RandExtension.RandomSentence(1000)).ForEach((v) => new SuouLine(v));
        }
        [TestMethod]
        public void SuouLineInsertTest()
        {
            Enumerable.Range(0, UnitNumber).Select((v) => RandExtension.RandomSentence(1000)).Select((v) => new SuouLine(v)).ForEach((v) => InsertSuouLine(v));
        }
        [TestMethod]
        public void SuouLineDeleteTest()
        {
            Enumerable.Range(0, UnitNumber).Select((v) => RandExtension.RandomSentence(1000))
                .Select((v) => new SuouLine(v))
                .ForEach((v) => DeleteSuouLine(v));
        }
        [TestMethod]
        public void LoadChpFileTest()
        {
            IBookHander.Load(SuouTest.Resource1.ChpPath);
        }
        [TestMethod]
        public void SuouTextCreateTest()
        {
            Enumerable.Range(0, UnitNumber).Select((v) => RandExtension.RandomSentence(1000)).ForEach((v) => new SuouText(v));
        }
        [TestMethod]
        public void LightLineCreateTest()
        {
            Enumerable.Range(0, UnitNumber).Select((v) => RandExtension.RandomSentence(1000)).ForEach((v) => new Class1(v));
        }
        [TestMethod]
        public void EmptyTest()
        {
            Enumerable.Range(0, UnitNumber).ForEach((v) => RandExtension.RandomSentence(1000));
        }
        [TestMethod]
        public void SuouTextMergeTest()
        {
            var book = TestFileLoad();
            MergeTest(book);
        }
        [TestMethod]
        public void BookLoadTest()
        {
            var book = TestFileLoad();
        }
        [TestMethod]
        public void SuouTextCopyTest()
        {
            var book = TestFileLoad();
            book.BookPages.ForEach((v) => v.SText.Clone());
        }
        [TestMethod]
        public void BooksLoadTest()
        {
            var books = this.TestFolderLoad();
            books.ItemPathList.ForEach((v) => IBookHander.Load(v));
            
        }
        public void InsertSuouLine(SuouLine line)
        {
            line.Insert(RandExtension.RandInt(line.Length), new SuouWord("test"));
        }
        public void DeleteSuouLine(SuouLine line)
        {
            int index = RandExtension.RandInt(line.Length);
            int len = RandExtension.RandInt(line.Length - index);
            line.Delete(index,len);
        }
        public void MergeTest(IBook item)
        {
            AltText tex = AltText.Default();
            item.BookPages.Pair().ForEach((v) => Merge(v.Key.SText, v.Value.SText,tex));
        }
        public IBook TestFileLoad()
        {
            return IBookHander.Load(SuouTest.Resource1.SuouMLTPath);
        }
        public IBookCollection TestFolderLoad()
        {
            var books = IBookCollection.Load(SuouTest.Resource1.ch2FolderPath);
            
            return books;
        }
        public void Merge(SuouText a, SuouText b,AltText tex)
        {
            tex.MergeEachLineCarelessly(a, b.ToLineMerge(false,tex.SpaceMaker,false));
        }
    }
}
