﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Interface;
using System.Windows.Forms;
using System.ComponentModel;
using System.Xml.Linq;
namespace SuouTextBox2.Base
{
    
    [Serializable]
    [ReadOnly(true)]
    public class TextBoxShortCutKeys:SuouBase.Interface.ShortCutCommandCollection,IParse
    {
        
        private ShortCutCommand _Paste = new ShortCutCommand("貼り付け",Keys.V | Keys.Control);
        public ShortCutCommand Paste
        {
            get { return this._Paste; }
            set { this._Paste = value; }
        }

        public ShortCutCommand Cut = new ShortCutCommand("切り取り", Keys.X | Keys.Control);
        public ShortCutCommand Copy = new ShortCutCommand("コピー", Keys.C | Keys.Control);
        public ShortCutCommand StoreUndo = new ShortCutCommand("元に戻す", Keys.Z | Keys.Control);
        public ShortCutCommand StoreRedo = new ShortCutCommand("やり直し", Keys.R | Keys.Control);
        public ShortCutCommand AllSelect = new ShortCutCommand("全て選択", Keys.A | Keys.Control);
        public ShortCutCommand Delete = new ShortCutCommand("削除", Keys.D | Keys.Control);
        
        public TextBoxShortCutKeys()
        {
            this.Name = "蘇芳テキストボックス";
            //Items = new ShortCutCommand[] { Paste, Cut, Copy, StoreUndo, StoreRedo, AllSelect };
        }
        public XElement CreateSerializer()
        {
            var v = new XElement("SuouTextBoxShortCutKeys");
            return null;
        }
    }
    
}
