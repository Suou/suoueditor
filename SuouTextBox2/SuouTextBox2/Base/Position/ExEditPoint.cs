﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;

namespace SuouTextBox2.Base
{
    /// <summary>
    /// 
    /// </summary>
    public class ExEditPoint : ExPositionPoint
    {
        public ExEditPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet)
            : base(x, y, displaySet, textSet)
        {
        }
        protected override void PositionCheck()
        {
        }
        public SuouLine Line
        {
            get
            {
                if (this.Y>=0 && this.Y < this.TextSet.Count)
                {
                    return this.TextSet[this.Y];
                }
                else
                {
                    return null;
                }
            }
        }
        
        public SuouLine PrefixLine
        {
            get
            {
                var SplitIndex = this.ToStringPoint();
                var LeftRight = this.Line.Split(SplitIndex.X);
                return LeftRight.Left;
            }
        }
        public SuouLine SuffixLine
        {
            get
            {
                var SplitIndex = this.ToStringPoint();
                var LeftRight = this.Line.Split(SplitIndex.X);
                return LeftRight.Right;
            }
        }
        
        public ExEditPoint Copy()
        {
            return new ExEditPoint(this.X, this.Y, DisplaySet, TextSet);
        }
        /// <summary>
        /// 端数は切り上げ
        /// </summary>
        /// <returns></returns>
        public ExStringPoint ToStringPoint()
        {
            var y = this.GetCorrectStringY(this.Y);
            var x = this.TextSet[y].TruncateFromWidthToLength(this.X,SuouBase.Text.NearestOption.None).Index;
            //14/06/19
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            return new ExStringPoint(x, y, DisplaySet, TextSet);
        }
        public ExDotPoint ToDotPoint()
        {
            var y = TextSet.GetVirtualLineLocation(this.Y).Y;
            
            var r = new ExDotPoint(this.X, y, DisplaySet, TextSet);
            return r;
        }
        public EditPoint ToEditPoint()
        {
            return new EditPoint(this.X, this.Y);
        }
        public ExDotPoint ToBandPoint()
        {
            var r = this.ToDotPoint();
            r.Y -= TextSet.YInterval;
            var r2 = new ExDotPoint(this.X, r.Y, DisplaySet, TextSet);
            return r2;
        }
        public int Height
        {
            get
            {
                if (this.Line != null)
                {
                    return this.Line.Height;
                }
                else
                {
                    return TextSet.Font.Height;
                }
            }
        }
        public int BandHeight
        {
            get
            {
                if (this.Line != null)
                {
                    return this.Line.Height+TextSet.YInterval;
                }
                else
                {
                    return TextSet.Font.Height + TextSet.YInterval;
                }
            }
        }
        /*
        public Rectangle CreateDotBandTextAreaRectangle(int length)
        {
            var r = this.ToDotPoint().ToTextAreaPoint();
            r.Y -= TextSet.YInterval;
            var rec = new Rectangle(r.Position, new Size(length,this.Height+TextSet.YInterval));
            return rec;
        }
        */
        public Rectangle CreateDotBandDotRectangle(int length)
        {
            var r = this.ToDotPoint();
            r.Y -= TextSet.YInterval;
            var rec = new Rectangle(r.Position, new Size(length, this.Height + TextSet.YInterval));
            return rec;
        }
        public static explicit operator ExDotPoint(ExEditPoint edp)
        {
            return edp.ToDotPoint();
        }
        public static explicit operator ExDisplayPoint(ExEditPoint edp)
        {
            return ((ExDotPoint)edp).ToDisplayPoint();
        }
        public static explicit operator ExTextAreaPoint(ExEditPoint edp)
        {
            return ((ExDotPoint)edp).ToTextAreaPoint();
        }
        public static explicit operator ExStringPoint(ExEditPoint edp)
        {
            return edp.ToStringPoint();
        }
        public ExEditPoint Stringize()
        {
            return this.ToStringPoint().ToEditPoint();
        }
        public void Exchange(ExEditPoint b)
        {
            var tmp = this.Copy();
            this.Position = b.Position;
            b.Position = tmp.Position;
        }
    }
}
