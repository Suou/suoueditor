﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;

namespace SuouTextBox2.Base
{
    public abstract class ExPositionPoint
    {
        protected DisplaySetting DisplaySet;
        protected TextComposition TextSet;

        public ExPositionPoint(int x, int y,DisplaySetting displaySet,TextComposition textSet)
            :this(x,y,displaySet,textSet,false)
        {

        }
        public ExPositionPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet,bool b)
        {
            this.DisplaySet = displaySet;
            this.TextSet = textSet;
            if (b)
            {
                this._Position = new Point(x, y);
            }
            else
            {
                this.Position = new Point(x, y);
            }

        }

        private Point _Position;
        public Point Position
        {
            get { return _Position; }
            set 
            { 
                _Position = value;
                PositionCheck();
            }
        }

        protected abstract void PositionCheck();


        public int X
        {
            get
            {
                return Position.X;
            }
            set
            {
                Position = new Point(value, this.Y);
                PositionCheck();
            }
        }
        public int Y
        {
            get
            {
                return Position.Y;
            }
            set
            {
                Position = new Point(this.X, value);
                PositionCheck();
            }
        }

        public ExPositionPoint(int x, int y)
        {
            this.Position = new Point(x, y);
        }

        #region Operator

        public static bool operator ==(ExPositionPoint a, ExPositionPoint b)
        {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b))
            {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null))
            {
                return false;
            }

            return (a.X == b.X && a.Y == b.Y);
        }
        public static bool operator !=(ExPositionPoint a, ExPositionPoint b)
        {
            return !(a==b);
        }

        public static bool operator <(ExPositionPoint a, ExPositionPoint b)
        {
            //a<b
            if (a.Y > b.Y)
            {
                return false;
            }
            else if (a.Y == b.Y)
            {
                if (a.X < b.X)
                {
                    return true;
                }
                else//(a.X >= b.X)
                {
                    return false;
                }
            }
            else //(a.Y < b.Y)
            {
                return true;
            }
        }
        public static bool operator <=(ExPositionPoint a, ExPositionPoint b)
        {
            //a<b
            if (a.Y > b.Y)
            {
                return false;
            }
            else if (a.Y == b.Y)
            {
                if (a.X <= b.X)
                {
                    return true;
                }
                else//(a.X >= b.X)
                {
                    return false;
                }
            }
            else //(a.Y < b.Y)
            {
                return true;
            }
        }
        public static bool operator >(ExPositionPoint a, ExPositionPoint b)
        {
            return !(a <= b);
        }
        public static bool operator >=(ExPositionPoint a, ExPositionPoint b)
        {
            return !(a < b);
        }

        protected int GetCorrectStringY(int stringy)
        {
            var r = stringy >= TextSet.Count ? TextSet.Count - 1 : stringy;
            r = r < 0 ? 0 : r;
            return r;
        }
        
        protected DotPoint GetCorrectDotPosition(StringPoint sp)
        {
            int y = this.GetCorrectStringY(sp.Y);
            return new DotPoint(TextSet.GetLineWidth(y, sp.X), TextSet.GetRealLineLocation(y).Y);
        }

        #endregion
        public override string ToString()
        {
            string s = this.GetType()+"/";
            s += "Position = {" + this.X + "/" + this.Y + "}";

            return s;
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }


    public class ExDotPoint : ExPositionPoint
    {
        public ExDotPoint Create(Point p)
        {
            return new ExDotPoint(p.X, p.Y, this.DisplaySet, this.TextSet);
        }

        public ExDotPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet)
            : base(x, y,displaySet,textSet)
        {
        }
        protected override void PositionCheck()
        {
        }
        public ExDotPoint Copy()
        {
            return new ExDotPoint(this.X, this.Y, DisplaySet, TextSet);
        }
        public ExEditPoint ToEditPoint()
        {
            var x = this.X;
            var y = this.TextSet.TruncateFromDotYToStringY(this.Y);
            return new ExEditPoint(x, y, DisplaySet, TextSet);
        }
        public ExDisplayPoint ToDisplayPoint()
        {
            var x = this.X - DisplaySet.TextAreaMapLocation.X + DisplaySet.Padding.Left;
            var y = this.Y - DisplaySet.TextAreaMapLocation.Y + DisplaySet.Padding.Top;
            return new ExDisplayPoint(x, y, DisplaySet, TextSet);
        }
        public ExTextAreaPoint ToTextAreaPoint()
        {
            var x = this.X;// - DisplaySet.TextAreaMapLocation.X;
            var y = this.Y - DisplaySet.TextAreaMapLocation.Y;
            return new ExTextAreaPoint(x, y, DisplaySet, TextSet);
        }
        public ExTextAreaPoint ToTextAreaPoint(Rectangle displayRectangle)
        {
            var x = this.X - displayRectangle.X;
            var y = this.Y - displayRectangle.Y;
            return new ExTextAreaPoint(x, y, DisplaySet, TextSet);
        }
        public static explicit operator ExDisplayPoint(ExDotPoint edp)
        {
            return edp.ToDisplayPoint();
        }
        public static explicit operator ExTextAreaPoint(ExDotPoint edp)
        {
            return edp.ToTextAreaPoint();
        }
        public static explicit operator ExEditPoint(ExDotPoint edp)
        {
            return edp.ToEditPoint();
        }
    }
    public class ExTextAreaPoint : ExPositionPoint
    {
        public ExTextAreaPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet)
            : base(x, y,displaySet,textSet)
        {
        }
        protected override void PositionCheck()
        {
        }
        public ExDisplayPoint Copy()
        {
            return new ExDisplayPoint(this.X, this.Y, DisplaySet, TextSet);
        }
        public Point ToPoint()
        {
            return new Point(this.X, this.Y);
        }
        public ExDotPoint ToDotPoint()
        {
            var x = this.X + DisplaySet.TextAreaMapLocation.X;
            var y = this.Y + DisplaySet.TextAreaMapLocation.Y;
            return new ExDotPoint(x, y, DisplaySet, TextSet);
        }
        public static explicit operator ExDotPoint(ExTextAreaPoint edp)
        {
            return edp.ToDotPoint();
        }
        public static explicit operator ExDisplayPoint(ExTextAreaPoint edp)
        {
            return ((ExDotPoint)edp).ToDisplayPoint();
        }
    }
    public class ExDisplayPoint : ExPositionPoint
    {
        public ExDisplayPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet)
            : base(x, y,displaySet,textSet)
        {
        }
        protected override void PositionCheck()
        {
        }
        public ExDisplayPoint Copy()
        {
            return new ExDisplayPoint(this.X, this.Y, DisplaySet, TextSet);
        }
        public Point ToPoint()
        {
            return new Point(this.X, this.Y);
        }
        public ExDotPoint ToDotPoint()
        {
            var x = this.X + DisplaySet.TextAreaMapLocation.X - DisplaySet.Padding.Left;
            var y = this.Y + DisplaySet.TextAreaMapLocation.Y - DisplaySet.Padding.Top;
            
            return new ExDotPoint(x, y, DisplaySet, TextSet);
        }
        public static explicit operator ExDotPoint(ExDisplayPoint edp)
        {
            return edp.ToDotPoint();
        }
        public static explicit operator ExEditPoint(ExDisplayPoint edp)
        {
            return ((ExDotPoint)edp).ToEditPoint();
        }
        public static explicit operator ExStringPoint(ExDisplayPoint edp)
        {
            return ((ExEditPoint)edp).ToStringPoint();
        }
        public static explicit operator ExTextAreaPoint(ExDisplayPoint edp)
        {
            return ((ExDotPoint)edp).ToTextAreaPoint();
        }

        public bool IsInternalTextDisplay
        {
            get
            {
                return DisplaySet.IsInternalTextArea(this);
            }
        }
    }
    /*
    public class ExBandPoint : ExPositionPoint
    {
        public ExBandPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet)
            : base(x, y, displaySet, textSet)
        {
        }
        protected override void PositionCheck()
        {
        }
    }
     */
}
