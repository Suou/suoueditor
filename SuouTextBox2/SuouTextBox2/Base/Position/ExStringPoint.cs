﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;


namespace SuouTextBox2.Base
{
    /// <summary>
    /// Violateを許容しない
    /// </summary>
    public class ExStringPoint : ExPositionPoint
    {
        public ExStringPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet)
            : base(x, y, displaySet, textSet)
        {
        }
        public ExStringPoint(int x, int y, DisplaySetting displaySet, TextComposition textSet,bool b)
            : base(x, y, displaySet, textSet,b)
        {
        }

        protected override void PositionCheck()
        {
            if (this.IsViolate)
            {
                throw new ViolatePositionException(this);
            }
        }

        public char Character
        {
            get 
            {
                if (this.TextSet[this.Y].Sentence.Length <= this.X)
                {
                    throw new Exception();
                }
                else
                {
                    return this.TextSet[this.Y].Sentence[this.X]; 
                }
            }
        }
        public SuouLetter PreCharacter
        {
            get
            {
                var x = this.X - 1;
                var len = this.TextSet[this.Y].Sentence.Length;
                if (len <= x || x < 0)
                {
                    return null;
                }
                else
                {
                    if (len <= x)
                    {
                        throw new MyException("Error");
                    }
#pragma warning disable CS0612 // 型またはメンバーが旧型式です
                    return TextSet[this.Y].GetLetter(x);
#pragma warning restore CS0612 // 型またはメンバーが旧型式です
                }
            }
        }

        public ExStringPoint Copy()
        {
            return new ExStringPoint(this.X, this.Y, this.DisplaySet, this.TextSet, true);
        }
        public ExEditPoint ToEditPoint()
        {
            var p = this.GetCorrectDotPosition((StringPoint)this);
            var r = new ExEditPoint(p.X, this.Y, DisplaySet, TextSet);
            return r;
        }






        public string Sentence
        {
            get
            {
                return TextSet[this.Y].Sentence;
            }
        }
        public bool IsRightMost
        {
            get
            {
                if (this.IsViolate) throw new ViolatePositionException(this);
                return this.Sentence.Length == this.X;
            }
        }
        public string RightString
        {
            get
            {
                if (this.IsViolate) throw new ViolatePositionException(this);
                return TextSet[this.Y].Split(this.X).Right.Sentence;
            }
        }
        public string LeftString
        {
            get
            {
                if (this.IsViolate) throw new ViolatePositionException(this);
                return TextSet[this.Y].Split(this.X).Left.Sentence;
            }
        }

        public bool IsLeftMost
        {
            get
            {
                return this.X == 0;
            }
        }
        public bool IsStartPoint
        {
            get { return (this.X == 0 && this.Y == 0); }
        }
        public bool IsEndPoint
        {
            get
            {
                var a = this.X == TextSet[this.Y].CharCount;
                var b = this.Y == TextSet.Count - 1;
                return a && b;
            }
        }
        public override string ToString()
        {
            string s = "ExStringPoint/";
            s += "Position = {" + this.X + "/" + this.Y + "}/";
            s += "Sentence = {" + this.Sentence+ "}/";

            return s;
        }
        /*
        public static ExStringPoint operator +(ExStringPoint z, StringPoint sp)
        {
            var nz = z.Copy();
            nz += sp.X;
            if (sp.Y > 0)
            {
                for (int i = 0; i < sp.Y; i++)
                {
                    if (nz.Y != nz.TextSet.Count - 1)
                    {
                        var p = nz.ToEditPoint();
                        p.Y++;
                        nz = p.ToStringPoint();
                    }
                }
            }
            else
            {
                sp.Y = -sp.Y;
                for (int i = 0; i < sp.Y; i++)
                {
                    if (nz.Y > 0)
                    {
                        var p = nz.ToEditPoint();
                        p.Y--;
                        nz = p.ToStringPoint();
                    }
                }
            }
            nz.PositionCheck();
            return nz;
        }
        */
        public static ExStringPoint operator +(ExStringPoint z, int len)
        {
            if (len < 0) return z - (-len);


            var v = z.Copy();
            int nlen = len;
            while (nlen > 0)
            {
                var rlen = v.RightString.Length;
                if (v.IsEndPoint)
                {
                    break;
                }
                else if (rlen == 0 && nlen >= 1)
                {
                    nlen--;
                    v.X = 0;
                    v.Y++;
                }
                else if (nlen < rlen)
                {
                    v.X += nlen;
                    nlen = 0;
                }
                else
                {
                    nlen -= rlen;
                    v.X += rlen;
                }
            }



            return v;
        }
        public static ExStringPoint operator -(ExStringPoint z, int len)
        {
            if (len < 0) return z + (-len);

            var v = z.Copy();
            int nlen = len;

            while (nlen > 0)
            {
                if (v.IsViolate) throw new Exception();
                var llen = v.LeftString.Length;
                
                if (v.IsStartPoint)
                {
                    break;
                }
                else if (llen == 0 && nlen >= 1)
                {
                    nlen--;
                    v.Y--;
                    v.X = v.Sentence.Length;

                }
                else if (nlen < llen)
                {
                    v.X -= nlen;
                    nlen = 0;
                }
                else
                {
                    nlen -= llen;
                    v.X -= llen;
                }
            }
            return v;
        }
        public static explicit operator StringPoint(ExStringPoint esp)
        {
            return new StringPoint(esp.X, esp.Y);
        }
        public static explicit operator ExEditPoint(ExStringPoint esp)
        {
            return esp.ToEditPoint();
        }
        public static explicit operator ExDotPoint(ExStringPoint esp)
        {
            return ((ExEditPoint)esp).ToDotPoint();
        }
        public static explicit operator ExDisplayPoint(ExStringPoint esp)
        {
            return ((ExDotPoint)esp).ToDisplayPoint();
        }
        public bool IsViolate
        {
            get
            {
                if (this.Y >= 0 && this.Y < TextSet.Count)
                {
                    if (this.X >= 0 && this.X <= TextSet[this.Y].CharCount) return false;
                }
                return true;
            }
        }
    }
}
