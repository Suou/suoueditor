﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using SuouBase.Text;
using System.Diagnostics;

using SuouBase.Drawing;

using SuouTextBox2.Base;

namespace SuouTextBox2
{
    public class Recter
    {
        public int Top;
        public int Left;
        public int Right;
        public int Bottom;

    }
    public class DisplaySetting
    {
        //public MemoriClass BasePaddingC = new MemoriClass(new Size(10, 20));
        public Recter Padding { get; private set; }
        public HScrollBar Hsb;
        public VScrollBar Vsb;
        public bool IsFixedScroll { get; set; }
        public UserControl ctrl;
        public TextComposition Texter { get; set; }
        public Size TextSize
        {
            get
            {
                return new Size(Texter.VirtualWidth + 1000, Texter.Height + 400);
            }
        }
        public int ValuePadding = 20;

        public int _DefaultPaddingTop = 25;
        public int DefaultPaddingTop
        {
            get { return _DefaultPaddingTop; }
            set { _DefaultPaddingTop = value; }
        }
        public int _DefaultPaddingLeft = 5;
        public int DefaultPaddingLeft
        {
            get { return _DefaultPaddingLeft; }
            set { _DefaultPaddingLeft = value; }
        }
        
        private bool _IsRuler;
        public bool IsRuler
        {
            get { return _IsRuler; }
            set
            {
                _IsRuler = value;
                if (IsRuler)
                {
                    Padding.Left = DefaultPaddingLeft;
                    Padding.Top = DefaultPaddingTop;
                }
                else
                {
                    Padding.Left = 0;
                    Padding.Top = 0;
                }
            }
        }

        public Size DisplaySize
        {
            get 
            {
                var width = ctrl.Width - Padding.Right;
                var height = ctrl.Height - Padding.Bottom;
                if (width <= 0) width = 1;
                if (height <= 0) height = 1;
                return new Size(width, height);
            }
        }
        public bool DisplayEmpty
        {
            get
            {
                var width = DisplaySize.Width;
                var height = DisplaySize.Height;
                return width > 0 && height > 0;
            }
        }
        public Size TextAreaSize
        {
            get
            {
                var p = new Size(ctrl.Width - Padding.Left - Padding.Right, ctrl.Height - Padding.Top - Padding.Bottom);
                return p;
            }
        }

        public void Caret_OutFromTextArea(object sender, CaretEventArg e)
        {
            if(!IsFixedScroll)CaretInternaling(e.Caret);
        }
        private void CaretInternaling(TextBoxCaret Caret)
        {
            var p = (ExDotPoint)Caret.StringPos;
            
            var pUnder = new DotPoint(p.X - ValuePadding, p.Y - ValuePadding);
            pUnder = new DotPoint(pUnder.X < 0 ? 0 : pUnder.X, pUnder.Y < 0 ? 0 : pUnder.Y);
            var pUpper = new DotPoint(p.X + ValuePadding, p.Y + ValuePadding);
            if (pUnder.Y < this.TextAreaMapLocation.Y)
            {
                this.TextAreaMapLocation = new ExDotPoint(this.TextAreaMapLocation.X, p.Y-ValuePadding,this,this.Texter);
            }
            else if (pUpper.Y > this.TextAreaMapLocation.Y + this.TextAreaSize.Height)
            {
                this.TextAreaMapLocation = new ExDotPoint(this.TextAreaMapLocation.X, pUpper.Y - this.TextAreaSize.Height, this, this.Texter);
            }

            if (pUnder.X < this.TextAreaMapLocation.X)
            {
                this.TextAreaMapLocation = new ExDotPoint(pUnder.X, this.TextAreaMapLocation.Y, this, this.Texter);
            }
            else if (pUpper.X > this.TextAreaMapLocation.X + this.TextAreaSize.Width)
            {
                this.TextAreaMapLocation = new ExDotPoint(pUpper.X - this.TextAreaSize.Width, this.TextAreaMapLocation.Y, this, this.Texter);
            }
        }

        public Rectangle TextAreaRectangle
        {
            get
            {
                return new Rectangle(TextAreaPaddingLocation.Position, TextAreaSize);
            }
        }
        public Rectangle TextAreaMapRectangle
        {
            get
            {
                return new Rectangle(TextAreaMapLocation.Position,TextAreaSize);
            }
        }
        public Rectangle UpperPaddingRectangle
        {
            get
            {
                return new Rectangle(Point.Empty, new Size(this.DisplaySize.Width, Padding.Top));
            }
        }
        public Rectangle LeftPaddingRectangle
        {
            get
            {
                return new Rectangle(new Point(0,Padding.Top), new Size(Padding.Left, this.DisplaySize.Height-Padding.Top));
            }
        }
        public ExDisplayPoint TextAreaPaddingLocation
        {
            get {
                return new ExDisplayPoint(Padding.Left, Padding.Top, this, this.Texter); }
        }
        public ExDotPoint TextAreaMapLocation
        {
            get
            {
                return new ExDotPoint(Hsb.Value, Vsb.Value,this,this.Texter);
            }
            set
            {
                if (value.X < 0) value.X = 0;
                if (value.Y < 0) value.Y = 0;
                if (value.X > Hsb.Maximum || value.Y > Vsb.Maximum) ScrollBarSet();
                if (!this.IsFixedScroll)
                {
                    Hsb.Value = value.X;
                    Vsb.Value = value.Y;
                }
            }
        }

        private PrintFontSetting PrintFontCont;
        public DisplaySetting(UserControl ctrl,PrintFontSetting pfc,VScrollBar vsb,HScrollBar hsb)
        {
            this.ctrl = ctrl;
            this.PrintFontCont = pfc;

            this.Padding = new Recter();
            IsRuler = false;
            this.SetScrollVar(vsb, hsb);
            
        }
        public void SetScrollVar(VScrollBar Vsb,HScrollBar Hsb)
        {
            this.Hsb = Hsb;
            this.Vsb = Vsb;
            if (Hsb == null && Vsb == null)
            {
                throw new Exception();
            }
            else
            {

                this.Vsb.LargeChange = this.PrintFontCont.Font.Height * this.PrintFontCont.Font.Height;
                this.Vsb.SmallChange = this.PrintFontCont.Font.Height;
                this.Hsb.SmallChange = this.PrintFontCont.Font.Height;
                this.Hsb.LargeChange = this.PrintFontCont.Font.Height * this.PrintFontCont.Font.Height;
                if (Hsb.Visible && Hsb.Parent == this.ctrl)
                {
                    Padding.Bottom = Hsb.Height;
                }
                else
                {
                    Padding.Bottom = 0;
                }

                if (Vsb.Visible && Vsb.Parent == this.ctrl)
                {
                    Padding.Right = Vsb.Width;
                }
                else
                {
                    Padding.Right = 0;
                }
            }
        }
        public void ScrollBarSet()
        {
            if (Texter == null) return;
            int MaxVsb = TextSize.Height - this.TextAreaSize.Height;
            int MaxHsb = TextSize.Width - this.TextAreaSize.Width;
            if (MaxVsb > 0)
            {
                Vsb.Enabled = true;
                Vsb.Maximum = MaxVsb + Vsb.LargeChange - 1;
            }
            else
            {
                Vsb.Value = 0;
                Vsb.Enabled = false;
            }

            if (MaxHsb > 0)
            {
                Hsb.Enabled = true;
                Hsb.Maximum = MaxHsb + Hsb.LargeChange - 1;
            }
            else
            {
                Hsb.Value = 0;
                Hsb.Enabled = false;
            }
        }
        public void TextOperator_TextChanged(object sender,TextChangeEventArg e)
        {
            ScrollBarSet();
        }
        public bool IsInternalTextArea(ExDisplayPoint dp)
        {
            var prec = TextAreaRectangle;
            var rec = new Rectangle(prec.X + ValuePadding, prec.Y + ValuePadding, prec.Width - ValuePadding, prec.Height - ValuePadding);
            return rec.Contains(dp.Position);
        }
        public void Control_SizeChanged(object sender, EventArgs e)
        {
            ScrollBarSet();
        }
        public void Control_MouseWheel(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            int k = SystemInformation.MouseWheelScrollLines * this.Vsb.SmallChange;
            if (e.Delta < 0) k = -k;
            var max = 1 + this.Vsb.Maximum - this.Vsb.LargeChange;
            var m = this.Vsb.Value;
            if (m - k > max) k = m - max;
            if (m - k < this.Vsb.Minimum) k = m - this.Vsb.Minimum;
            this.Vsb.Value -= k;
        }
        public Size DisplayLineSize(int y)
        {
            int sx = this.TextAreaSize.Width;
            int sy = Texter.GetLineHeight(y);
            return new Size(sx, sy);
        }

        
    }
}
