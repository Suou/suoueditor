﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Collections.ObjectModel;

namespace SuouTextBox2.Base
{
    public delegate void TextStockEventHandler(object sender,TextStockEventArgs e);
    public class TextStockEventArgs : EventArgs
    {
        public Stack<EditorState> RedoStock { get; set; }
        public Stack<EditorState> UndoStock { get; set; }
        public EditorState NowState { get; set; }
        public TextStockEventArgs(Stack<EditorState> redo, Stack<EditorState> undo, EditorState now)
        {
            this.RedoStock = redo;
            this.UndoStock = undo;
            this.NowState = now;
        }
    }
    public class EditorState
    {
        public TextComposition Text;
        public StringPoint SP;

        public EditorState(TextComposition Text, StringPoint SP)
        {
            this.Text = Text;
            this.SP = SP;
        }
    }

    public class TextStock
    {
        /// <summary>
        /// kは常にスタックの最上位の要素を差すこととする
        /// 存在しない場合は-1
        /// ただし、Back,Forward,NoInclimentの場合は例外
        /// </summary>
        //int k = -1;
        //List<EditorState> StateStock = new List<EditorState>();
        private Stack<EditorState> RedoStock = new Stack<EditorState>();//元に戻す
        private Stack<EditorState> UndoStock = new Stack<EditorState>();//やり直す
        private EditorState NowState { get; set; }
        public event TextStockEventHandler ItemChanged;
        public TextStock()
        {
        }


        public void Clear(TextComposition text, StringPoint CursorLocation)
        {
            NowState = new EditorState(text, CursorLocation);
            RedoStock.Clear();
            UndoStock.Clear();
            if (ItemChanged != null) ItemChanged(this, new TextStockEventArgs(RedoStock,UndoStock,NowState));
        }
        public void Store(TextComposition text, StringPoint CursorLocation,StringPoint BeforeCaretPoint)
        {
            NowState.SP = BeforeCaretPoint;
            var es = new EditorState(text, CursorLocation);

            RedoStock.Push(NowState);
            UndoStock.Clear();

            NowState = es;
            if (ItemChanged != null) ItemChanged(this, new TextStockEventArgs(RedoStock, UndoStock, NowState));
        }
        public bool CanBack
        {
            get
            {
                return RedoStock.Count > 0;
            }

        }
        public bool CanForward
        {
            get
            {
                return UndoStock.Count > 0;
            }
        }
        /// <summary>
        /// 定義:TextList[--k]を返す;
        /// </summary>
        /// <returns></returns>
        public EditorState Back()
        {
            UndoStock.Push(NowState);
            var f = RedoStock.Pop();
            NowState = f;
            if (ItemChanged != null) ItemChanged(this, new TextStockEventArgs(RedoStock, UndoStock, NowState));
            return f;
        }
        /// <summary>
        /// 定義:TextList[++k]を返す;
        /// </summary>
        /// <returns></returns>
        public EditorState Forward()
        {
            RedoStock.Push(NowState);
            var f = UndoStock.Pop();
            NowState = f;
            if (ItemChanged != null) ItemChanged(this, new TextStockEventArgs(RedoStock, UndoStock, NowState));
            return f;
        }
    }
}
