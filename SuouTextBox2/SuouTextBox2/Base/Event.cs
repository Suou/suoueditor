﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase;
using SuouBase.Text;

using SuouTextBox2.Base;
using System.Windows.Forms;
using System.Drawing;

namespace SuouTextBox2
{
    public enum InputKeyType
    {
        BackSpace,Enter,Char,Text
    }
    /*
    public enum TextChangedType
    {
        BackSpaceDelete,SelectDelete,InsertString,Paste
    }
    */
    public enum CursorDirection
    {
        Null,Up,Down,Left,Right
    }
    public struct CursorData
    {
        public CursorDirection Direction;
        public Keys KeyData;
        public bool IsShift;
        public bool IsAlt;
        public bool IsCtrl;
    }

    public class InputKeyEventArg : EventArgs
    {
        public InputKeyType Type;
        public string Text;
        public InputKeyEventArg(InputKeyType type, string text):base()
        {
            this.Type = type;
            this.Text = text;
        }
    }
    public class InputCursorEventArg : EventArgs
    {
        public CursorData KeyData;
        private ExStringPoint _CaretPosition;
        public ExStringPoint CaretPosition { get { return _CaretPosition.Copy(); } }
        public int ExDotMemory { get; set; }
        public InputCursorEventArg(CursorData keyData,ExStringPoint CaretPosition,int xdotmemory)
            : base()
        {
            this.ExDotMemory = xdotmemory;
            this.KeyData = keyData;
            this._CaretPosition = CaretPosition;
        }

    }

    public delegate void InputKeyEventHandler(object sender,InputKeyEventArg e);
    public delegate bool InputCursorEventHandler(object sender, InputCursorEventArg e);
    public class TextChangeEventArg : EventArgs
    {
        public TextComposition TextData;
        public TextChangeType Type;
        public StringPoint BeforeCaretLocation;
        public StringPoint AfterCaretLocation;
        public int[] ChangeY;
        public bool IsTextLengthChange;
        public TextChangeEventArg(TextComposition text,TextChangeType Type,StringPoint afterCaretLocation,StringPoint beforeCaretLocation,int[] ChangeY,bool IsTextLengthChange):base()
        {
            this.Type = Type;
            this.TextData = text;
            this.BeforeCaretLocation = beforeCaretLocation;
            this.AfterCaretLocation = afterCaretLocation;
            this.ChangeY = ChangeY;
            this.IsTextLengthChange = IsTextLengthChange;
        }
        
    }
    public enum TextChangeType
    {
        Stock,Other,Initialize,TemporaryMerge
    }
    public delegate void TextChangeEventHandler(object sender, TextChangeEventArg e);

    public delegate void CaretEventHandler(object sender, CaretEventArg e);
    public delegate void CaretInfoEventHandler(object sender, CaretInfoEventArg e);
    public class CaretEventArg : EventArgs
    {
        public TextBoxCaret Caret;
        public CaretEventArg(TextBoxCaret caret)
            : base()
        {
            this.Caret = caret;
        }

    }
    public class CaretInfoEventArg : EventArgs
    {
        public string PageSizeInfo { get; set; }
        public string CaretInfo { get; set; }
        public string CaretCharInfo { get; set; }
        public CaretInfoEventArg()
            : base()
        {
        }

    }
    public delegate void TurnEndEventHandler(object sender, TurnEndEventArg e);
    public class TurnEndEventArg : EventArgs
    {
        public Graphics ControlGraphic;
        public TurnEndEventArg(Graphics g)
            : base()
        {
            this.ControlGraphic = g;
        }

    }

    public class SuouMouseEventArg : EventArgs
    {
        public ExDisplayPoint ExLocation;
        public SuouLine ChosenLine { get; private set; }
        public SuouLine ChosenLineAfter { get; set; }


        public EditPoint EditLocation
        {
            get { return ((ExEditPoint)this.ExLocation).ToEditPoint(); }
        }
        public MouseEventArgs MouseEventArgs;
        public SuouMouseEventArg(MouseEventArgs e,ExDisplayPoint p,SuouLine chosenLine)
            : base()
        {
            this.ChosenLine = chosenLine; 

            this.MouseEventArgs = new MouseEventArgs(e.Button, e.Clicks, e.X, e.Y, e.Delta);
            this.ExLocation = p;
        }

    }
    public delegate void TextLineEventHandler(object sender, TextLineEventArg e);
    public class TextLineEventArg : EventArgs
    {
        public TextLineComposition Line;
        public bool IsHeightChange { get; set; }
        public TextLineEventArg(TextLineComposition line,bool IsHeightChange)
            : base()
        {
            this.Line = line;
            this.IsHeightChange = IsHeightChange;
        }

    }

    public delegate void SuouMouseEventHandler(object sender, SuouMouseEventArg e);

    public class ViolatePositionException : MyException
    {
        public ExStringPoint Location { get; set; }
        public ViolatePositionException(ExStringPoint sp)
            : base("無効な位置が決定されました")
        {
            this.Location = sp;
        }
    }

    public class InsertException : MyException
    {
        public InsertException(string str,int n,string inserttext)
            : base("無効な位置に挿入されました")
        {
        }
    }
}
