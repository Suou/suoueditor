﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using System.Drawing;
using System.Threading.Tasks;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Controls;
using SuouBase.Text;
using SuouTextBox2.Settings;

namespace SuouTextBox2.Base
{
    public static class DLLFunctionExtension
    {
        public static SuouTextBoxPlugin SuouTextBoxPlugin(this EditorInfo item)
        {
            return (SuouTextBoxPlugin)item.TextBoxPlugin;
        }
        public static SpaceLineSet GetProperSpaceSet(HukidasiSaveData item, EditorInfo dllFunction)
        {
            if (item.Type == HukidasiType.Nothing)
            {
                return dllFunction.SuouTextBoxPlugin().SaveData.SerihuAlgorithm;
            }
            else
            {
                return dllFunction.SuouTextBoxPlugin().SaveData.WakuAlgorithm;
            }
        }
    }
}
