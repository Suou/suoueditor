﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace SuouTextBox2.Base
{
    public class Win32Api
    {
        [DllImport("User32.Dll", EntryPoint = "SetWindowText")]
        public static extern void SetWindowText(int hwnd, String text);
        [DllImport("User32.Dll", EntryPoint = "GetWindowText")]
        public static extern int GetWindowText(int hwnd, StringBuilder buf, int nMaxCount);
        [DllImport("User32.Dll", EntryPoint = "CreateCaret")]
        public static extern int CreateCaret(IntPtr hwnd, IntPtr hbm, int cx, int cy);
        [DllImport("user32.dll")]
        public static extern int SetCaretPos(int x, int y);
        [DllImport("user32.dll")]
        public static extern int ShowCaret(IntPtr hwnd);
        //[DllImport("user32.dll")]
        //public static extern int HideCaret(IntPtr hwnd);
        [DllImport("user32.dll")]
        public static extern int GetKeyNameText(IntPtr lParam, StringBuilder lpString, int nSize);
    }

    public class IMEApi
    {

        public const int WM_IME_SETCONTEXT = 0x0281;
        public const int WM_IME_COMPOSITION = 0x010F;
        public const int WM_CHAR = 0x102;
        public const int WM_KEYDOWN = 0x0100;

        public const int WM_IME_NOTIFY = 0x0282;
        public const int WM_IME_CONTROL = 0x0283;
        public const int WM_IME_COMPOSITIONFULL = 0x0284;
        public const int WM_IME_SELECT = 0x0285;
        public const int WM_IME_CHAR = 0x0286;
        public const int WM_IME_STARTCOMPOSITION = 0x010D;

        // WM_IME_COMPOSITION
        public const int GCS_COMPREADSTR = 0x0001;
        public const int GCS_COMPREADATTR = 0x0002;
        public const int GCS_COMPREADCLAUSE = 0x0004;
        public const int GCS_COMPSTR = 0x0008;
        public const int GCS_COMPATTR = 0x0010;
        public const int GCS_COMPCLAUSE = 0x0020;
        public const int GCS_CURSORPOS = 0x0080;
        public const int GCS_DELTASTART = 0x0100;
        public const int GCS_RESULTREADSTR = 0x0200;
        public const int GCS_RESULTREADCLAUSE = 0x0400;
        public const int GCS_RESULTSTR = 0x0800;
        public const int GCS_RESULTCLAUSE = 0x1000;

        public const int CFS_POINT = 0x0002;


        public struct RECT
        {
            public int Left;
            public int Top;
            public int Right;
            public int Bottom;
        }
        public struct POINTAPI
        {
            public int x;
            public int y;
        }
        public struct COMPOSITIONFORM
        {
            public uint dwStyle;
            public POINTAPI ptCurrentPos;
            public RECT rcArea;
        }
        public const int LF_FACESIZE = 32;
        public unsafe struct LOGFONT
        {
            public int lfHeight;
            public int lfWidth;
            public int lfEscapement;
            public int lfOrientation;
            public int lfWeight;
            public byte lfItalic;
            public byte lfUnderline;
            public byte lfStrikeOut;
            public byte lfCharSet;
            public byte lfOutPrecision;
            public byte lfClipPrecision;
            public byte lfQuality;
            public byte lfPitchAndFamily;
            public fixed ushort lfFaceName[LF_FACESIZE]; // 固定サイズ
        }

        [DllImport("imm32.dll")]
        public static extern IntPtr ImmCreateContext();
        [DllImport("Imm32.dll")]
        public static extern IntPtr ImmGetContext(IntPtr hWnd);
        [DllImport("Imm32.dll")]
        public static extern IntPtr ImmGetDefaultIMEWnd(IntPtr hWnd);
        [DllImport("imm32.dll")]
        public static extern int ImmGetCompositionString(IntPtr hIMC, int dwIndex, byte[] lpBuf, int dwBufLen);
        [DllImport("imm32.dll")]
        public static extern bool ImmAssociateContextEx(IntPtr hWnd, IntPtr hIMC, uint dwFlags);
        [DllImport("imm32.dll", CharSet = CharSet.Auto)]
        public static extern bool ImmReleaseContext(IntPtr hWnd, IntPtr hIMC);
        [DllImport("imm32.dll")] 
        public static extern int ImmSetCompositionFont(IntPtr hIMC, ref LOGFONT lplf);
        [DllImport("imm32.dll")] 
        public extern static int ImmSetOpenStatus(IntPtr hMMC, int fOpen);
        [DllImport("imm32.dll")]
        public extern static bool ImmDestroyContext(IntPtr hIMC);
        //----------------------------------------------------------------
        // IMEの状態取得
        //
        [DllImport("Imm32.dll")]
        public static extern int ImmGetOpenStatus(int hIMC);
        [DllImport("Imm32.dll")]
        public static extern bool ImmSetCompositionWindow(IntPtr hIMC, ref COMPOSITIONFORM lpCompForm);
    }


}
