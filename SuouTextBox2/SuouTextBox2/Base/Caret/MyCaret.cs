﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;

namespace SuouTextBox2.Base
{
    public class MyCaret
    {
        [DllImport("user32.dll")]
        public static extern int CreateCaret(IntPtr hwnd, IntPtr hbm, int cx, int cy);
        [DllImport("user32.dll")]
        public static extern int DestroyCaret();
        [DllImport("user32.dll")]
        private static extern int SetCaretPos(int x, int y);
        [DllImport("user32.dll")]
        public static extern int ShowCaret(IntPtr hwnd);
        [DllImport("user32.dll")]
        public static extern int HideCaret(IntPtr hwnd);
        [DllImport("user32.dll")]
        static extern bool SetCaretBlinkTime(int MSeconds);

        private Control _Control;
        private Size size;
        private Point pos = Point.Empty;
        private IntPtr hIMC;
        private IntPtr immHandle;
        private object boxedLogFont;
        public bool _CanHaveCaret = true;

        public virtual bool CanHaveCaret
        {
            get { return this._CanHaveCaret; }
            set { this._CanHaveCaret = value; }
        }

        public EventHandler CaretMoved;


        public MyCaret(Control ctrl)
        {
            this._Control = ctrl;
            Position = Point.Empty;
            Size = new Size(1, ctrl.Font.Height + 3);

            Control.Enter += new EventHandler(OnGotFocus);
            Control.Leave += new EventHandler(OnLostFocus);

            boxedLogFont = new IMEApi.LOGFONT();
            ctrl.Font.ToLogFont(boxedLogFont);

        }

        public Control Control
        {
            get { return _Control; }
        }

        public Size Size
        {
            get { return size; }
            set { size = value; }
        }

        public Point Position
        {
            get
            {
                return pos;
            }
            set
            {
                pos = value;
                if (this.CaretMoved != null)
                {
                    this.CaretMoved(this, new EventArgs());
                }
                if (CanHaveCaret)
                {
                    this.ShowCaret();
                }
            }
        }

        public void Hide()
        {
            if (!this.Control.IsHandleCreated) return;
            HideCaret(Control.Handle);

        }

        public bool DisposeCheck()
        {
            if (_Control.Focused)
                OnLostFocus(_Control, new EventArgs());
            Control.GotFocus -= new EventHandler(OnGotFocus);
            Control.LostFocus -= new EventHandler(OnLostFocus);
            Control.MouseClick -= new MouseEventHandler(OnGotFocus);
            return true;
        }

        public void ShowCaret()
        {
            if (!this.Control.IsHandleCreated || !CanHaveCaret) return;
            CreateCaret(Control.Handle, IntPtr.Zero, Size.Width, Size.Height);
            ImmSetContext();
            //ImmSetCompositionWindow();
            var n = SetCaretPos(pos.X, pos.Y);
            var a = ShowCaret(Control.Handle);
        }

        private void OnGotFocus(object sender, EventArgs e)
        {
            if (CanHaveCaret)
            {
                ShowCaret();
                //ImmSetContext();
            }
        }

        private void OnLostFocus(object sender, EventArgs e)
        {
            Hide();
            DestroyCaret();
            //IMEApi.ImmDestroyContext(hIMC);
        }
        public void CaretLost()
        {
            OnLostFocus(this, null);
        }

        public void VoidImmGetContext()
        {
            if (!this._Control.IsHandleCreated) return;

            _Control.EndInvoke(_Control.BeginInvoke((Action)(() =>
            {
                hIMC = IMEApi.ImmGetContext(_Control.Handle);
            })));

        }
        public IntPtr ImmGetOrCreateContext()
        {
            hIMC = IMEApi.ImmGetContext(_Control.Handle);
            if (hIMC == IntPtr.Zero)
            {
                immHandle = IMEApi.ImmCreateContext();//入力コンテキストの作成
                IMEApi.ImmAssociateContextEx(_Control.Handle, immHandle, 0x0010);//Handleにコンテキストを関連付ける
                return immHandle;
            }
            else
            {
                return hIMC;
            }
        }

        private void ImmSetCompositionWindow()
        {
            IMEApi.COMPOSITIONFORM form = new IMEApi.COMPOSITIONFORM();
            form.dwStyle = IMEApi.CFS_POINT;

            form.ptCurrentPos.x = pos.X;// MEMO:X位置設定 
            form.ptCurrentPos.y = pos.Y;// MEMO:Y位置設定
            var b = IMEApi.ImmSetCompositionWindow(hIMC, ref form);
        }
        public void ImmReleaseContext()
        {
            _Control.EndInvoke(_Control.BeginInvoke((Action)(() =>
            {
                var n = IMEApi.ImmReleaseContext(_Control.Handle, hIMC);
            })
            ));
        }

        public int ImmGetCompositionString(byte[] buffer, int n)
        {
            return IMEApi.ImmGetCompositionString(hIMC, IMEApi.GCS_RESULTSTR, buffer, n);
        }
        public void ImmSetContext()
        {
            if (!this.Control.Focused) return;
            Action ac = () =>
            {
                /*
                immHandle = IMEApi.ImmCreateContext();//入力コンテキストの作成
                IMEApi.ImmAssociateContextEx(_Control.Handle, immHandle, 0x0010);//Handleにコンテキストを関連付ける
                */
                var immHandle = this.ImmGetOrCreateContext();
                IMEApi.LOGFONT p = (IMEApi.LOGFONT)boxedLogFont;//キャスト
                IMEApi.ImmSetCompositionFont(hIMC, ref p);
                ImmSetCompositionWindow();
            };
            _Control.EndInvoke(_Control.BeginInvoke(ac));

        }

        public int ImmGetOpenStatus()
        {
            return IMEApi.ImmGetOpenStatus(hIMC.ToInt32());
        }
    }
}
