﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Runtime.InteropServices;


namespace SuouTextBox2.Base
{
    public delegate void ChangeEventHandler(object sender, EventArgs e);
    public delegate void DelegateGraphic();

    public class TextBoxCaret : MyCaret
    {
        private ExStringPoint _StringPos;
        public event ChangeEventHandler CaretChanged;
        public event CaretEventHandler CaretOutFromTextArea;
        public int XDotMemory = 0;
        public ExStringPoint StringPos
        {
            get
            {
                return _StringPos.Copy();
            }
            private set
            {
                _StringPos = value.Copy();
                OutCheck();
            }
        }
        public bool VisibleMode = true;

        public void OutCheck()
        {
            var p = (ExDisplayPoint)this.StringPos;
            if (!p.IsInternalTextDisplay)
            {
                if (CaretOutFromTextArea != null) CaretOutFromTextArea(this, new CaretEventArg(this));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ctrl">TextBox</param>
        /// <param name="DG">Caretの再描画関数</param>
        public TextBoxCaret(SuouTextBox nctrl,ExStringPoint sp)
            : base(nctrl)
        {
            this.Nctrl = nctrl;
            StringPos = sp;
            this.Control.GotFocus += this.Control_Enter;
            CaretDotPos();
            this.VoidImmGetContext();
        }
        private SuouTextBox Nctrl { get; set; }
        public override bool CanHaveCaret
        {
            get
            {
                if (this.Nctrl != null)
                {
                    return base.CanHaveCaret && this.Nctrl.UserInputEnable;
                }
                else
                {
                    return base.CanHaveCaret;
                }
            }
            set
            {
                base.CanHaveCaret = value;
            }
        }

        #region Event
        protected void OnCaretChange(EventArgs e)
        {
           // 外部のイベントハンドラが接続されているなら
            if (CaretChanged != null)
            {
                CaretChanged(this, e);
            }
        }
        #endregion
        
        /// <summary>
        /// キャレットのドット位置を更新する
        /// </summary>
        private void CaretDotPos()
        {
            ExDisplayPoint dp = (ExDisplayPoint)StringPos;
            if(this.VisibleMode)
            {
                this.Position = dp.ToPoint();
                this.ShowCaret();
            }
            else
            {
                this.Position = dp.ToPoint();
                this.Hide();
            }

        }
        
        /// <summary>
        /// 再描画
        /// </summary>
        public void Update()
        {
            if (this.StringPos.IsViolate) throw new Exception();
            CaretDotPos();
            OnCaretChange(new EventArgs());
        }

        private void Control_Enter(object sender, EventArgs e)
        {
            this.Update();
        }
        public void TextInserted_Caret(object sender, TextChangeEventArg e)
        {
            var sp = this.StringPos;
            sp.Position = new Point(e.AfterCaretLocation.X, e.AfterCaretLocation.Y);
            this.StringPos = sp;

            XDotMemory = this.StringPos.ToEditPoint().X;
            Update();
        }
        public void MouseInput_MouseDown(object sender, SuouMouseEventArg e)
        {
            if (e.MouseEventArgs.Button == MouseButtons.Left)
            {
                
                this.StringPos = (ExStringPoint)e.ExLocation;
                XDotMemory = this.StringPos.ToEditPoint().X;
                Update();
            }
        }

        public bool InputedCursor_Caret(object sender, InputCursorEventArg e)
        {
            switch (e.KeyData.Direction)
            {
                case CursorDirection.Down:
                    this.StringPos = InclimentYCaret(this.StringPos,1,XDotMemory);
                    Update();
                    return true;
                case CursorDirection.Right:
                    
                    this.StringPos += 1;
                    XDotMemory = this.StringPos.ToEditPoint().X;
                    
                    Update();
                    return true;
                case CursorDirection.Up:
                    this.StringPos = InclimentYCaret(this.StringPos, -1, XDotMemory);
                    Update();
                    return true;
                case CursorDirection.Left:
                    this.StringPos -= 1;
                    XDotMemory = this.StringPos.ToEditPoint().X;
                    Update();
                    return true;
            }
            return false;
        }
        public static ExStringPoint InclimentYCaret(ExStringPoint sp, int y, int koteiX)
        {
            var ep = sp.ToEditPoint();
            ep.X = koteiX;
            ep.Y += y;
            
            var r = ep.ToStringPoint();
            return r;
        }
        public void ScrollBar_ValueChanged(object sender, EventArgs e)
        {
            CaretDotPos();
        }
    }
}
