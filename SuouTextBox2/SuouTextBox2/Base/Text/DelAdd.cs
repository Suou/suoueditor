﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using SuouTextBox2.Base;
using SuouBase.Text;

namespace SuouTextBox2.Base
{
    [Obsolete]
    public abstract class DelAddData
    {
        public StringPoint Location;
        //public DelAddType Type;
        public abstract DelAddData Copy();
        public int Y
        {
            get { return this.Location.Y; }
        }
    }
    [Obsolete]
    public class DelLineData:DelAddData
    {
        public int Length;

        public DelLineData(StringPoint Location, int Length)
        {
            this.Length = Length;
            this.Location = Location;
        }
        public override DelAddData Copy()
        {
            return new DelLineData(this.Location,this.Length);
        }
        public override string ToString()
        {
            var s = "";
            s += this.GetType().Name;
            s += "/" + "Length = " + this.Length + "/ Location =" + this.Location.Location;
            return s;
        }
    }
    [Obsolete]
    public class DelParagraphData:DelAddData
    {
        public StringPoint DelAfterLocation { get; set; }
        
        public DelParagraphData(StringPoint Location,StringPoint DelAfterLocation)
        {
            this.Location = Location;
            this.DelAfterLocation = DelAfterLocation;
        }
        public override DelAddData Copy()
        {
            return new DelParagraphData(this.Location,this.DelAfterLocation);
        }
        public override string ToString()
        {
            var s = "";
            s += this.GetType().Name;
            s += "/" + "AfterLocation = " + this.DelAfterLocation + "/ Location =" + this.Location.Location;
            return s;
        }
    }
    [Obsolete]
    public class AddLineData:DelAddData
    {
        public SuouLine Text;
        public AddLineData(StringPoint Location, SuouLine text)
        {
            this.Text = text;
            this.Location = Location;
        }
        public StringPoint TailLocation
        {
            get
            {
                var p = new StringPoint(this.Location.X + Text.Length, this.Location.Y);
                return p;
            }
        }
        public override DelAddData Copy()
        {
            return new AddLineData(this.Location,this.Text.Clone());
        }
        public override string ToString()
        {
            var s = "";
            s += this.GetType().Name;
            s += "/" + "Text = " + this.Text.Sentence + "/ Location =" + this.Location.Location;
            return s;
        }
    }
    [Obsolete]
    public class AddParagraphData:DelAddData
    {
        public AddParagraphData(StringPoint Location)
        {
            this.Location = Location;
        }
        public override DelAddData Copy()
        {
            return new AddParagraphData(this.Location);
        }
        public override string ToString()
        {
            var s = "";
            s += this.GetType().Name;
            s += "/  Location =" + this.Location.Location;
            return s;
        }
    }
    /*
    class DelAddParagraphData : DelAddData
    {
        public int YLength;
        public DelAddParagraphData(StringPoint sp, int YPos,DelAddType type)
        {
            this.Location = sp;
            this.YLength = YPos;
            this.Type = type;
        }
        public override string ToString()
        {
            var str = "DelAddParagraphData/";
            str += this.Location.ToString();
            return str;
        }
        public override DelAddData Copy()
        {
            var r = new DelAddParagraphData(this.Location, this.YLength, this.Type);
            return r;
        }
    }
    class DelAddWordData : DelAddData
    {
        public int Length;
        public LineComposition LineWord;
        public DelAddWordData(StringPoint location, int len, LineComposition LineWord,DelAddType type)
        {
            this.Location = location;
            this.Length = len;
            this.LineWord = LineWord;
            this.Type = type;
        }
        public override string ToString()
        {
            var str = "DelAddWordData/" + this.Location.ToString() + "/"+this.Type+"/"+"Length"+Length+"/";
            if (LineWord != null)
            {
                foreach (var w in LineWord.RealWords)
                {
                    str += "{" + w.Word + "}";
                }
            }
            return str;
        }
        public override DelAddData Copy()
        {
            var r = new DelAddWordData(this.Location, this.Length, this.LineWord, this.Type);
            return r;
        }
    }

    
    enum WordDataType
    {
        Word, Paragraph
    }
    enum DelAddType
    {
        Add,Delete
    }
    */
    [Obsolete]
    public class DelAddTextData
    {
        public List<DelAddData> WordData = new List<DelAddData>();
        public StringPoint LastLocation;

        public void Add(DelAddData dad)
        {
            if (dad is DelLineData)
            {
                var r = (DelLineData)dad;
                this.Add(r);
            }
            else if (dad is AddLineData)
            {
                var r = (AddLineData)dad;
                this.Add(r);
            }
            else if (dad is DelParagraphData)
            {
                var r = (DelParagraphData)dad;
                this.Add(r);
            }
            else if (dad is AddParagraphData)
            {
                var r = (AddParagraphData)dad;
                this.Add(r);
            }
        }
        public void Add(DelLineData dd)
        {
            WordData.Add(dd);
            if (dd.Length >= 0)
            {
                this.LastLocation = dd.Location;
            }
            else
            {
                this.LastLocation = new StringPoint(dd.Location.X + dd.Length, dd.Y);
            }
        }
        public void Add(DelParagraphData dd)
        {
            WordData.Add(dd);
            this.LastLocation = dd.DelAfterLocation;
        }
        public void Add(AddLineData dd)
        {
            WordData.Add(dd);
            this.LastLocation = new StringPoint(dd.Location.X+dd.Text.Length,dd.Location.Y);
        }
        public void Add(AddParagraphData dd)
        {
            WordData.Add(dd);
            this.LastLocation = new StringPoint(0,dd.Location.Y+1);
        }
        

        public DelAddTextData()
        {
        }
        public int MinStringY
        {
            get
            {
                return WordData.Min((t) => t.Location.Y);
            }
        }
        public bool ContainsPragraph
        {
            get
            {
                return WordData.Exists((t) => t is DelParagraphData || t is AddParagraphData);
            }
        }
        public bool IsSimpleAddParagraph
        {
            get
            {
                return ContainsPragraph && this.WordData.Count == 3 && this.WordData.All((t) => t is AddParagraphData || t is AddLineData);
            }
        }
        public int[] ChengedStrY
        {
            get
            {
                List<int> r = new List<int>();
                foreach (var w in WordData)
                {
                    if (w is DelParagraphData)
                    {
                        if (!r.Contains(w.Location.Y-1)) r.Add(w.Location.Y-1);
                    }
                    else
                    {
                        if (!r.Contains(w.Location.Y)) r.Add(w.Location.Y);
                    }
                }
                return r.ToArray();
            }
        }

        public override string ToString()
        {
            string str = "DelAddTextData/" + this.LastLocation + System.Environment.NewLine;
            foreach (var v in this.WordData)
            {
                str += v.ToString();
            }
            return str;
        }
        public DelAddTextData Copy()
        {
            
            var r = new DelAddTextData();
            foreach (var v in this.WordData)
            {
                r.Add(v.Copy());
            }
            
            return r;
            
        }
        public static DelAddTextData operator +(DelAddTextData a, DelAddTextData b)
        {
            var a1 = a.Copy();
            var b1 = b.Copy();
            a1.WordData.AddRange(b1.WordData);
            a1.LastLocation = b1.LastLocation;
            return a1;
        }
        public void Reverse()
        {
            var r = new List<DelAddData>(this.WordData);
            this.WordData = new List<DelAddData>();
            for (int i = r.Count() - 1; i >= 0; i--)
            {
                if (r[i] is DelLineData)
                {
                    this.Add((DelLineData)r[i]);
                }
                else if (r[i] is AddLineData)
                {
                    this.Add((AddLineData)r[i]);
                }
                else if (r[i] is DelParagraphData)
                {
                    this.Add((DelParagraphData)r[i]);
                }
                else if (r[i] is AddParagraphData)
                {
                    this.Add((AddParagraphData)r[i]);
                }

            }
        }
        public static DelAddTextData CreateAddTextData(TextComposition tc)
        {
            var r = new DelAddTextData();
            for(int i=0;i<tc.Count;i++)
            {
                var sp = new StringPoint(0,i);
                var a = new AddLineData(sp, tc[i]);
                r.Add(a);
                if (i != tc.Count - 1)
                {
                    var sp2 = new StringPoint(tc[i].Length, i);
                    var b = new AddParagraphData(sp2);
                    r.Add(b);
                }
            }
            return r;
        }
        public static DelAddTextData CreateAddTextData(SuouLine[] lcs,StringPoint InsertPoint)
        {
            //リファクタリングしたい
            var r = new DelAddTextData();
            for (int i = 0; i < lcs.Length; i++)
            {
                var a = new AddLineData(InsertPoint, lcs[i]);
                r.Add(a);
                InsertPoint = a.TailLocation;
                if (i != lcs.Length - 1)
                {
                    var b = new AddParagraphData(InsertPoint);
                    r.Add(b);
                    
                    InsertPoint = new StringPoint(0, InsertPoint.Y + 1);
                }
            }
            return r;
        }
        public static DelAddTextData CreateDelTextData(int n, ExStringPoint sp,TextComposition Text)
        {
            var r = new DelAddTextData();
            if (n == 1)
            {
                if (sp.IsEndPoint)
                {
                    return r;
                }
                else if (sp.IsRightMost)
                {
                    var v = new DelParagraphData(new StringPoint(0, sp.Y + 1), (StringPoint)sp);
                    r.Add(v);
                }
                else
                {
                    var v = new DelLineData((StringPoint)sp, 1);
                    r.Add(v);
                }
            }
            else if (n == -1)
            {
                if (sp.IsStartPoint)
                {
                    return r;
                }
                else if (sp.IsLeftMost)
                {
                    var v = new DelParagraphData((StringPoint)sp, new StringPoint(Text.Lines[sp.Y - 1].Length, sp.Y - 1));
                    r.Add(v);
                }
                else
                {
                    var v = new DelLineData(new StringPoint(sp.X, sp.Y), -1);
                    r.Add(v);
                }
            }
            else
            {
                throw new Exception();
            }
            return r;
        }
    }
}
