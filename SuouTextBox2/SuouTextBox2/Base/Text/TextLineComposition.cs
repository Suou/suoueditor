﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouTextBox2.Base;
using System.Windows.Forms;
using System.Drawing.Imaging;
using SuouTextBox2.Draw;
using SuouBase;
using SuouBase.Text;
using SuouBase.Drawing;
namespace SuouTextBox2.Base
{
    public class TextLineComposition:SuouLine
    {
        #region Property
        private Bitmap _Image;
        private Bitmap _InverseImage;
        public DotPoint Location { get; set; }
        public StringPoint StringLocation { get; set; }
        public Rectangle DotVirtualRectangle
        {
            get { return new Rectangle(this.Location.Position, this.VirtualLineSize); }
        }
        public Rectangle Insertsect(Rectangle DotRectangle)
        {
            var dotrec = Rectangle.Intersect(this.BandRectangle, DotRectangle);
            var p = new Point(dotrec.X - this.BandRectangle.X, dotrec.Y - this.BandRectangle.Y);
            return new Rectangle(p, dotrec.Size);
        }
        public Rectangle RecallRec(Rectangle LineRectangle)
        {
            var p = new Point(LineRectangle.X + this.BandRectangle.X, LineRectangle.Y + this.BandRectangle.Y);
            return new Rectangle(p, LineRectangle.Size);
        }

        private SuouWord _EndWord;
        public SuouWord EndWord
        {
            get { return _EndWord; }
            set
            {
                _EndWord = value;
            }
        }
        public List<SuouWord> RealWords
        {
            get { return this.Words; }
        }
        public List<SuouWord> VirtualWords
        {
            get
            {
                var r = base.Words.ToList();
                r.Add(EndWord);
                return r;
            }
        }
        public IEnumerable<SuouLetter> VirtualLetterIterator()
        {
            foreach (var v in this.VirtualWords)
            {
                foreach (var c in v)
                {
                    yield return c;
                }
            }
        }
        public void SetLine(SuouLine lc)
        {
            base.Words = lc.Words;
            
            this.ClearImage();
            SetSize();
        }
        public int RealWidth
        {
            get { return this.Width; }
        }

        public int Yinterval { get; private set; }
        public PrintFontSetting PrintFontSet { get; set; }
        public int VirtualWidth
        {
            get { return this.RealWidth + EndWord.Width; }
        }
        public Size VirtualLineSize
        {
            get
            {
                return new Size(VirtualWidth, Height);
            }
        }
        public Rectangle RealRectangle
        {
            get
            {
                return new Rectangle(this.Location.Position, LineSize);
            }
        }
        public DotPoint BandLocation
        {
            get { return new DotPoint(this.Location.X, this.Location.Y - this.Yinterval); }
        }
        public Rectangle BandRectangle
        {
            get { return new Rectangle(this.BandLocation.Position, new Size(this.VirtualWidth, this.BandHeight)); }
        }
        
        public Bitmap Image
        {
            get
            {
                if (_Image == null) this.CreateImage();
                return _Image;
            }
            private set
            {
                this._Image = value;
                if (this._Image == null) this._InverseImage = null;
            }
        }
        public Bitmap InverseImage
        {
            get
            {
                if (_InverseImage == null) this.CreateInverseImage();
                return _InverseImage;
            }
            private set
            {
                this._InverseImage = value;
            }
        }
        #endregion

        #region Constructor
        
        public TextLineComposition(SuouWord w, int Yinterval, PrintFontSetting PFS,StringPoint Location)
            : base(w)
        {
            this.StringLocation = Location;
            this.Yinterval = Yinterval;
            this.PrintFontSet = PFS;
            this.EndWord = new SuouWord(w.Font, w.Color, "");
        }

        public TextLineComposition(Font font, Color c, int Yinterval, string text, SuouWord EndWord, PrintFontSetting PFS,StringPoint StringLocation)
            : base(font, c, text)
        {
            this.Yinterval = Yinterval;
            this.PrintFontSet = PFS;
            this.StringLocation = StringLocation;
            this.EndWord = EndWord;
        }
        public TextLineComposition(Font font, Color c, int Yinterval, List<SuouWord> f, SuouWord EndWord, PrintFontSetting PFS,StringPoint StringLocation)
            : base(font, c, f)
        {
            this.Yinterval = Yinterval;
            this.PrintFontSet = PFS;
            this.EndWord = EndWord;
            this.StringLocation = StringLocation;
        }
        public TextLineComposition(TextLineComposition tfl)
            : base(tfl)
        {
            if (tfl.HasImage) this.Image = tfl.Image;
            this.PrintFontSet = tfl.PrintFontSet;
            this.Yinterval = tfl.Yinterval;
            this.StringLocation = tfl.StringLocation;
            this.EndWord = tfl.EndWord;
        }
        public TextLineComposition(SuouLine tfl,int Yinterval,PrintFontSetting PFS, SuouWord EndWord,StringPoint stringLocation)
            : base(tfl)
        {
            this.PrintFontSet = PFS;
            this.Yinterval = Yinterval;
            this.StringLocation = stringLocation;
            this.EndWord = EndWord;
        }
        
        #endregion

        public int BandHeight
        {
            get { return this.Height + Yinterval; }
        }

        #region Method
        public new TextLineComposition Clone()
        {
            return new TextLineComposition(this);
        }
        private void CreateImage()
        {
            var bmp = new Bitmap(this.VirtualLineSize.Width, this.VirtualLineSize.Height + Yinterval, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(bmp);
            IntPtr hDC = g.GetHdc();
            TextElementDraw.LineSentenceDraw(this, hDC);
            g.ReleaseHdc();
            this.Image = bmp;
        }
        /*
        public override LineComposition Reverse()
        {
            var r = base.Reverse();
            var rr = new TextLineComposition(r, this.EndWord,this.StringLocaction);
            return rr;
        }
        */
        public void Inverse(int StartX, int Length)
        {
            if (Length <= 0) return;
            var p = new Point(StartX, 0);
            var sz = new Size(Length, this.BandHeight);
            var rec = new Rectangle(p, sz);
            TurboArgbPixelFilter32.FastFilteringRectangle(this.InverseImage, TurboArgbPixelFilter32.InverseProc, rec);
        }
        public void ClearInverseImage()
        {
            this.InverseImage = null;
        }
        public override void Clear()
        {
            this.Image = null;
            RealWords.Clear();
            SetSize();

        }
        public bool HasImage
        {
            get { return this._Image != null; }
        }
        public void ClearImage()
        {
            this.Image = null;
            this.InverseImage = null;
        }
        private void CreateInverseImage()
        {
            this.InverseImage = (Bitmap)this.Image.Clone();
        }
        
        protected override void Add(SuouWord tf)
        {
            base.Add(tf);
            this.Image = null;
        }
        protected override void Add(SuouLine tf)
        {
            base.Add(tf);
            this.Image = null;
        
        }
        /*
        public override WordComposition TrimLeft()
        {
            var leftword = base.TrimLeft();
            this.Image = null;
            
            return leftword;
            
        }
        */
        public new void Insert(int strn, SuouWord tf)
        {
            this.SetLine(base.Insert(strn, tf));
        }
        public new void Insert(int strn, SuouLine tf)
        {
            var line = base.Insert(strn, tf);
            this.SetLine(line);
        }

        public new void Delete(int Index, int Length)
        {
            this.SetLine(base.Delete(Index, Length));
            //return thpdata.Center;
        }

        protected override void SetSize()
        {
            int BeforeHeight = this.Height;
            base.SetSize();
            int AfterHeight = this.Height;
            this.Image = null;
            var HeightChange = BeforeHeight != AfterHeight && BeforeHeight != -1;

        }
        protected override void SetSizeCheck(SuouLine wc)
        {
            int BeforeHeight = this.Height;
            base.SetSizeCheck(wc);
            int AfterHeight = this.Height;
            this.Image = null;
            var HeightChange = BeforeHeight != AfterHeight && BeforeHeight != -1;
        }
        protected override void SetSizeCheck(SuouWord wc)
        {
            int BeforeHeight = this.Height;
            base.SetSizeCheck(wc);
            int AfterHeight = this.Height;
            this.Image = null;
            var HeightChange = BeforeHeight != AfterHeight && BeforeHeight != -1;
        }
        #endregion
        
        public static TextLineComposition operator +(TextLineComposition a, SuouLine b)
        {
            var ar = (TextLineComposition)a.Clone();
            var br = b.Clone();
            foreach (var c in br.Words)
            {
                ar.Add(c);
            }
            return ar;
        }
        
    }
}
