﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase.Drawing;
namespace SuouTextBox2.Base
{
    public class WordPosser
    {
        public SuouWord Word;
        public ExDotPoint Location;
        public WordPosser(SuouWord word,ExDotPoint location)
        {
            this.Word = word;
            this.Location = location;
        }
    }
    public class LinePosser
    {
        private TextLineComposition Sentence;
        public int Height { get { return Sentence.Height; } }
        public ExDotPoint Location;
        public Rectangle Rectangle
        {
            get
            {
                return new Rectangle(Location.Position, Sentence.VirtualLineSize);
            }
        }
        public WordPosser EndWord { get; set; }
        public IEnumerator<WordPosser> GetEnumerator()
        {
            int x = 0;
            for (int i = 0; i < Sentence.Count; i++)
            {
                ExDotPoint p = Location.Create(new Point(x, Location.Y));
                x += Sentence[i].Width;
                WordPosser wp = new WordPosser(Sentence[i], p);
                yield return wp;
            }
            if (EndWord != null) yield return EndWord;
        }
        public LinePosser(TextLineComposition sentence,ExDotPoint location,WordPosser endWord)
        {
            this.Sentence = sentence;
            this.Location = location;
            this.EndWord = endWord;
        }
    }
    public class TextPosser
    {
        public TextComposition Text;
        public ExDotPoint Location;
        public PrintFontSetting PrintSet;
        

        public IEnumerator<LinePosser> GetEnumerator()
        {
            var ep = Location.ToEditPoint();
            for (int i = 0; i < Text.Count; i++)
            {
                var endword = CreateEndWord(i,(TextLineComposition)Text[i]);
                LinePosser lp = new LinePosser((TextLineComposition)Text[i], ep.ToDotPoint(), endword);
                ep.Y++;
                yield return lp;
            }
        }
        private WordPosser CreateEndWord(int i,TextLineComposition lc)
        {
            var p = Location.Create(new Point(lc.Location.X + lc.RealWidth, lc.Location.Y));
            SuouWord w = (i == this.Text.Count - 1) ? PrintSet.EndForm : PrintSet.ParagraphForm;

            var g = new WordPosser(w, p);
            return g;
        }
        
        public LinePosser GetLinePosser(int i)
        {
            var endword = CreateEndWord(i, (TextLineComposition)Text[i]);
            var p = Location.Create(((TextLineComposition)Text[i]).Location.Position);
            LinePosser lp = new LinePosser((TextLineComposition)Text[i], p, endword);
            return lp;
        }
        
        public TextPosser(TextComposition text,ExDotPoint location,PrintFontSetting printSet)
        {
            this.Text = text;
            this.Location = location;
            this.PrintSet = printSet;
        }
    }




    public enum TextType
    {
        Space,Normal,ViolateSpace
    }

}
