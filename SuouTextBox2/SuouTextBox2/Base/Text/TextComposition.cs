﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using SuouBase.Text;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase;
using SuouBase.Drawing;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
namespace SuouTextBox2.Base
{
    public class TextComposition:SuouText
    {
        #region Property
        private DotPoint Location
        {
            get { return new DotPoint(0, 0); }
        }
        public PrintFontSetting PrintSet = new PrintFontSetting();
        public override int YInterval
        {
            get
            {
                return PrintSet.YLineSpace;
            }
            set
            {
                PrintSet.YLineSpace = value;
            }
        } 
        public override Font Font
        {
            get { return this.PrintSet.Font; }
            set { this.PrintSet.Font = value; }
        }
        public override Color Color
        {
            get { return PrintSet.TextColor; }
            set { PrintSet.TextColor = value; }
        }
        public SuouWord ParagraphWord { get { return PrintSet.ParagraphForm; } }
        public SuouWord EndWord { get { return PrintSet.EndForm; } }

        public TextLineComposition[] GetLines()
        {
            return base.Lines.Select((v) => (TextLineComposition)v).ToArray();
        }
        private SuouWord GetTailForm(int n)
        {
            return n == this.Count - 1 ? PrintSet.EndForm : PrintSet.ParagraphForm;
        }
        public TextLineComposition Line(int i)
        {
            return (TextLineComposition)base.Lines[i];
        }

        public new EditPoint LastLocation
        {
            get
            {
                var y = this.Count-1;
                var x = this[y].Width;
                var r = new EditPoint(x,y);
                return r;
            }
        }
        
        private void CheckLines()
        {
            foreach (var v in this)
            {
                if (!(v is TextLineComposition)) throw new MyException("");
            }
        }
        public override void ReSize()
        {
            this.MaxVirtualWidthLine = this.GetLines().Max2((v) => v.VirtualWidth);
            base.ReSize();
            this.SetLineLocation();
            this.SetEndForm();
        }
        protected override void AddLine(SuouLine line)
        {
            var textline = new TextLineComposition(line, this.YInterval, this.PrintSet,this.PrintSet.EndForm, new StringPoint(0, this.Count));
            
            base.AddLine(textline);
        }
        protected override void InsertLine(int n, SuouLine line)
        {
            var endword = n == this.Count - 1 ? this.PrintSet.EndForm : this.PrintSet.ParagraphForm;
            var textline = new TextLineComposition(line, this.YInterval, this.PrintSet, endword, new StringPoint(0, n));
            
            base.InsertLine(n, textline);
        }
        protected override void InsertRangeLine(int n, IEnumerable<SuouLine> lines)
        {
            int i = 0;
            var textlines = lines.Select((v) => new TextLineComposition(v, this.YInterval, this.PrintSet, this.PrintSet.ParagraphForm, new StringPoint(0, n + (i++))));
            base.InsertRangeLine(n, textlines);
        }

        public override SuouLine this[int i]
        {
            get
            {
                if (i >= this.Count)
                {
                    return ToSentence(new SuouLine(this.Font, this.Color,""), i);
                }
                else
                {
                    return base[i];
                }
            }
            set
            {
                base[i] = new TextLineComposition(value,this.YInterval,this.PrintSet,this.GetTailForm(i),new StringPoint(0,i));
            }
        }
        public override void MakeBottomLine(int n)
        {
            while (n >= this.Count) this.AddLine(ToSentence(new SuouLine(this.Font, this.Color,""), this.Count));
        }
        public Size VirtualSize
        {
            get
            {
                return new Size(VirtualWidth, Height);
            }
        }
        [NonSerialized]
        private TextLineComposition _MaxVirtualWidthLine;
        private TextLineComposition MaxVirtualWidthLine 
        {
            get {
                if (this._MaxVirtualWidthLine == null) this.ReSize();
                return this._MaxVirtualWidthLine; 
            }
            set
            {
                this._MaxVirtualWidthLine = value;
            }
        }
        public int VirtualWidth 
        {
            get
            {
                return MaxVirtualWidthLine.VirtualWidth;
            }
        }
        public int RealWidth
        {
            get
            {
                return base.Width;
            }
        }


        #endregion
        #region Operarion
        private void SetLineLocation()
        {
            DotPoint p = new DotPoint();
            int width = 0;
            for (int i = 0; i < this.Count; i++)
            {
                p.Y += YInterval;
                this.Line(i).Location = p;
                if (this.Line(i).VirtualWidth > width) width = this.Line(i).VirtualWidth;
                p.Y += Lines[i].Height;
                this.Line(i).StringLocation = new StringPoint(0, i);
            }
        }
        private void SetEndForm()
        {
            for (int i = 0; i < Lines.Count; i++)
            {
                this.Line(i).EndWord = this.GetTailForm(i);                
            }
        }
        
        #endregion
        #region Resize
        private void InsertResize(TextLineComposition newline)
        {
            if (newline.VirtualWidth > this.MaxVirtualWidthLine.VirtualWidth) this.MaxVirtualWidthLine = newline;
        }
        protected override void InsertResize(SuouLine newline)
        {
            ReSize();
        }
        private void DeleteResize(TextLineComposition deleteline)
        {
            if (deleteline == this.MaxVirtualWidthLine) this.MaxVirtualWidthLine = null;
        }
        protected override void DeleteResize(SuouLine deleteline)
        {
            ReSize();
        }
        
        private void ReplaceResize(TextLineComposition preline, TextLineComposition newline)
        {
            newline.Location = preline.Location;
            if (preline == this.MaxVirtualWidthLine)
            {
                if (newline.VirtualWidth > preline.VirtualWidth)
                {
                    this.MaxVirtualWidthLine = newline;
                }
                else
                {
                    this.MaxVirtualWidthLine = null;
                }
            }
            else
            {
                if (newline.VirtualWidth > this.MaxVirtualWidthLine.VirtualWidth) this.MaxVirtualWidthLine = newline;
            }
        }
        protected override void ReplaceResize(SuouLine preline, SuouLine newline)
        {
            var preline2 = (TextLineComposition)preline;
            var newline2 = (TextLineComposition)newline;
            this.ReplaceResize(preline2, newline2);
            

            base.ReplaceResize(preline, newline);
        }
        protected override void AddResize(SuouLine newline)
        {
            var line = (TextLineComposition)newline;
            if (line.VirtualWidth > this.MaxVirtualWidthLine.VirtualWidth) this.MaxVirtualWidthLine = line;
            base.AddResize(line);
        }
        #endregion

        #region Delete
        private TextLineComposition ToSentence(SuouLine line, int y)
        {
            return new TextLineComposition(line, this.YInterval, this.PrintSet,this.GetTailForm(y), new StringPoint(0, y));
        }
        public override StringPoint Replace(int y, SuouLine sentence)
        {
            var newline = ToSentence(sentence, y);
            var preline = this[y];
            base.Replace(y, newline);
            this.ReplaceResize(preline, newline);
            return new StringPoint(sentence.Length, y);

        }
        
        private void SuperDeleteALL()
        {
            base.Lines.Clear();
            this.ReSize();
        }
        #endregion
        #region Constructor
        public TextComposition(PrintFontSetting PrintSet):base(PrintSet.Font,PrintSet.TextColor)
        {
            if (PrintSet == null) throw new MyException("PrintSetが存在しません");
            this.PrintSet = PrintSet;
        }
        public TextComposition(SuouText Text, PrintFontSetting PFS):base(Text)
        {
            if (Text == null) throw new MyException("テキストが存在しません。");
            this.PrintSet = PFS;
        }
        public TextComposition(PrintFontSetting PrintSet, string text)
            : base(text)
        {
            this.PrintSet = PrintSet;
            this.Initialize(text);
        }
        /// <summary>
        /// コピー
        /// </summary>
        /// <param name="tfc"></param>
        public TextComposition(TextComposition tfc):base()
        {
            this.PrintSet = tfc.PrintSet;
            List<TextLineComposition> r = new List<TextLineComposition>();
            foreach (TextLineComposition tfl in tfc.Lines) r.Add(tfl.Clone());
            this.Height = tfc.Height;
            base.Lines.Clear();
            r.ForEach((v) => this.AddLine(v));
            this.ReSize();
        }
        #endregion

        #region BangMethod
        public override void Initialize(string text)
        {
            base.Lines.Clear();
            var p = text.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None);
            List<TextLineComposition> r = new List<TextLineComposition>();
            for (int i = 0; i < p.Count(); i++)
            {
                TextLineComposition lc = null;
                if (i < p.Count() - 1)
                {
                    lc = new TextLineComposition(this.Font, this.Color,this.YInterval, p[i], ParagraphWord,this.PrintSet,new StringPoint(0,i));
                }
                else
                {
                    lc = new TextLineComposition(this.Font, this.Color, this.YInterval, p[i], EndWord, this.PrintSet,new StringPoint(0,1));
                }
                r.Add(lc);
            }
            r.ForEach((v) => this.AddLine(v));
            this.ReSize();
            
        }
        public void Initialize(TextComposition text)
        {
            base.Lines.Clear();
            foreach (var v in text.Lines)
            {
                var w = v.Clone();
                base.Lines.Add(w);
            }
            this.ReSize();
        }
        public override void Initialize(SuouText text)
        {
            base.Lines.Clear();
            var t = 0;
            text.Lines.ForEach((v) => this.AddLine(new TextLineComposition(v.Clone(), this.YInterval, this.PrintSet, this.EndWord, new StringPoint(0, t++))));
            this.ReSize();
        }
        #endregion

        #region NoBangMethod
        public new TextComposition Clone()
        {
            return new TextComposition(this);
        }

        public List<int> ImageN = new List<int>();

        public void ImageGC(int[] n)
        {
            if (ImageN.Count > 150)
            {
                var f = ImageN.FindAll((v) => v < this.Count && !n.Contains(v));
                foreach (var v in f)
                {
                    this.Line(v).ClearImage();
                }
                ImageN.Clear();
            }
            ImageN.AddRange(n);
            ImageN.Distinct();

        }

        public SuouLine[] GetLines(Rectangle DotRectangle)
        {
            //時間O(n)
            List<SuouLine> r = new List<SuouLine>();
            
            for (int i = 0; i < this.Count;i++ )
            {
                if (this.Line(i).Location.Y + Lines[i].Height > DotRectangle.Y && this.Line(i).Location.Y < DotRectangle.Bottom)
                {
                    r.Add(Lines[i]);
                    
                }
            }
            
            return r.ToArray();
        }
        public int[] GetRealLinesNth(Rectangle DotRectangle)
        {
            //時間O(n)
            List<int> r = new List<int>();
            List<int> ns = new List<int>();
            for (int i = 0; i < this.Count;i++)
            {
                if (this.Line(i).Location.Y + Lines[i].Height > DotRectangle.Y && this.Line(i).BandLocation.Y < DotRectangle.Bottom)
                {
                    r.Add(i);
                    ns.Add(i);
                }
            }
            ImageGC(ns.ToArray());
            return r.ToArray();
        }
        
        public int[] GetVirtualLinesNth(Rectangle DotRectangle)
        {
            //時間O(n)
            List<int> r = new List<int>();
            int i = 0;
            while(this.GetVirtualLineBandLocation(i).Y<DotRectangle.Bottom)
            {
                if (this.GetVirtualLineBottom(i) > DotRectangle.Y && this.GetVirtualLineBandLocation(i).Y < DotRectangle.Bottom)
                {
                    r.Add(i);
                }
                i++;
            }
            return r.ToArray();
        }

        public DotPoint GetRealLineLocation(int yElement)
        {
            if (yElement < this.Count)
            {
                return this.Line(yElement).Location;
            }
            else
            {
                throw new Exception();
            }
        }
        public DotPoint GetVirtualLineLocation(int yElement)
        {
            if (yElement < 0)
            {
                return new DotPoint(0, this.Font.Height * yElement);
            }
            else if (yElement >= 0 && yElement < this.Count)
            {
                return this.Line(yElement).Location;
            }
            else
            {
                int n = this.Line(this.Count-1).Location.Y + Lines.Last().Height + YInterval;
                int m = yElement - this.Count;

                n += (this.Font.Height + YInterval) * m;

                return new DotPoint(0,n);
            }
        }
        public DotPoint GetVirtualLineBandLocation(int yElement)
        {
            if (yElement < this.Count)
            {
                return this.Line(yElement).BandLocation;
            }
            else
            {
                var p = this.GetVirtualLineLocation(yElement);

                return new DotPoint(p.X,p.Y-this.YInterval);
            }
        }
        public int GetVirtualLineBandHeight(int yElement)
        {
            if (yElement < this.Count)
            {
                return this.Line(yElement).BandHeight;
            }
            else
            {
                return this.Font.Height + this.YInterval;
            }
        }
        public int GetVirtualLineBottom(int yElement)
        {
            if (yElement < this.Count)
            {
                return this.Line(yElement).Location.Y + Lines[yElement].Height;
            }
            else
            {
                return this.GetVirtualLineLocation(yElement).Y+this.Font.Height;
            }
        }

        public int GetLineHeight(int y)
        {
            if (y >= 0 && y < this.Count)
            {
                return this.Lines[y].Height;
            }
            else
            {
                return this.Font.Height;
            }

        }
        public int GetLineWidth(int yElement, int xElement)
        {
            return Lines[yElement].MeasureWidth(xElement);
        }
        public int TruncateFromDotYToStringY(int doty)
        {
            int i = 0;
            int mino;
            int maxo;
            if (doty < 0)
            {
                var t = (doty / this.Font.Height) - 1;
                return t;
            }
            else if (doty == 0) return 0;
            for (i = 0; i < this.Count; i++)
            {
                mino = this.Line(i).Location.Y - YInterval;
                maxo = this.Line(i).Location.Y + Lines[i].Height;
                if (mino < doty && doty <= maxo) return i;
            }
            int n = this.Line(this.Count-1).Location.Y + Lines.Last().Height + YInterval;
            i = this.Count;
            while (n < doty)
            {
                mino = n;
                maxo = n + this.Font.Height + YInterval;
                n = maxo;
                if (mino < doty && doty < maxo) return i;
                i++;
            }
            return i;
        }

        public override string ToString()
        {
            var str = "Text/" + System.Environment.NewLine;
            foreach (var v in this.Lines) str += v.Sentence + System.Environment.NewLine;
            return str;
            
        }
        public bool CheckBlock(int EditWidth, int EditHeight)
        {
            if (this.Count != EditHeight) return false;
            for (int i = 0; i < this.Count; i++)
            {
                if (this.Line(i).RealWidth != EditWidth) return false;
            }
            return true;
        }
        public SuouText ToSuouText()
        {
            return base.Clone();
        }
        public void ClearImage()
        {
            this.GetLines().ForEach((v) => v.ClearImage());
        }
        /*
        public DelAddTextData ToDelAddTextData()
        {
            var r = DelAddTextData.CreateAddTextData(this);
            return r;
        }
        */
        #endregion
        #region Empty
        public static TextComposition Empty(PrintFontSetting pfs)
        {
            var r = new TextComposition(pfs);
            return r;
        }
        public static TextComposition Empty(TextComposition tc)
        {
            var r = new TextComposition(tc.PrintSet);
            return r;
        }
        public static TextComposition Empty(TextComposition tc, int YCount)
        {
            var r = new TextComposition(tc.PrintSet);
            return r;
        }
        public static TextComposition EmptyCopy(TextComposition tc)
        {
            var r = new TextComposition(tc.PrintSet);
            return r;
        }
        #endregion

    }
}
