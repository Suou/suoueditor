﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using System.Diagnostics;
using SuouBase;
namespace SuouTextBox2.Base
{
    public static class InverseExtension
    {
        public static DotBand[] GetDotBands(this InverseDataCollection data, int nth, TextComposition tc)
        {
            List<DotBand> r = new List<DotBand>();
            if (!data.Items.ContainsKey(nth)) return r.ToArray();
            if (data.Items[nth].Key != null) r.AddRange(data.Items[nth].Key);

            if (data.Items[nth].Value)
            {
                var line = (TextLineComposition)tc[nth];
                var db = new DotBand(line.Location.X + line.RealWidth, nth, line.EndWord.Width);
                r.Add(db);
            }

            return r.ToArray();
        }
        public static DotBand[] CreateDotBands(this InverseDataCollection data, TextComposition tc)
        {
            List<DotBand> r = new List<DotBand>();
            foreach (var v in data.Items.Values)
            {
                if (v.Key != null) r.AddRange(v.Key);
            }
            foreach (var v in data.Items)
            {
                int n = v.Key;
                if (v.Value.Value)
                {
                    var line = (TextLineComposition)tc[n];
                    var db = new DotBand(line.Location.X + line.RealWidth, n, line.EndWord.Width);
                    r.Add(db);
                }
            }
            return r.ToArray();
        }
    }
    public class Inverser
    {
        

        private ExEditPoint _StartPoint;
        private ExEditPoint _EndPoint;
        private ExEditPoint StartPoint
        {
            get { return _StartPoint; }
            set
            {
                _StartPoint = value;
            }
        }
        private ExEditPoint EndPoint
        {
            get { return _EndPoint; }
            set
            {
                _EndPoint = value;
            }
        }
        public InverseChoiceBase Choice { get; set; }
        public InverseChoiceBase ShiftChoice { get; set; }
        public DisplayPointMaker PointMaker { get; set; }

        public bool ChoiceStartFrag { get; set; }

        private InverseDataLapper _ActualItems;
        private InverseDataCollection PreviousItems = new InverseDataCollection();
        private InverseDataCollection TemporaryItems = new InverseDataCollection();

        public InverseDataCollection Items
        {
            get
            {
                return _ActualItems.Item;
            }
            
            set
            {
                if (value == null) throw new Exception();
                this._ActualItems.Item = value;
                //OnInverseChanged(this, new EventArgs());
            }
        }
        public InverseDataLapper ActualItem
        {
            get
            {
                return this._ActualItems;
            }

            set
            {
                this._ActualItems = value;
            }
        }


        
        public Inverser(InverseChoiceBase ic,DisplayPointMaker PM,InverseDataLapper item)
        {
            this.PointMaker = PM;
            Choice = ic;
            ShiftChoice = new PrimitiveChoice();
            this._ActualItems = item;
        }
        public void UpdateItems()
        {
            this.Items = InverseDataCollection.MergeXOR(PreviousItems, TemporaryItems);
            this.PreviousItems = this.Items.Copy();
            this.TemporaryItems.Clear();
        }
        
        
        public void AllInverse(TextComposition tc)
        {
            this.PreviousItems.AllChoice(tc);
            this.TemporaryItems.Clear();
            this.Items = this.PreviousItems.Copy();
        } 
        public void Clear()
        {
            this.Items = new InverseDataCollection();
            this.PreviousItems.Clear();
            this.TemporaryItems.Clear();
        }
        
        public void ContinuityCheck()
        {
            if (!Choice.IsContinuity)
            {
                this.Clear();
            }
        }
        public void ChoiceStart(ExDisplayPoint MouseLocation)
        {
            this.ChoiceStartFrag = true;
            ContinuityCheck();
            var p = (ExEditPoint)MouseLocation;
            StartPoint = p;
        }
        public void ChoiceMove(ExDisplayPoint MouseLocation)
        {
            if (Choice.IsInstance && (StartPoint != null))
            {
                DecideChoice(MouseLocation);
            }
            else
            {
                if (StartPoint != null)
                {
                    TemporaryChoice(MouseLocation);
                }
            }
        }
        public bool ChoiceEnd(ExDisplayPoint MouseLocation)
        {
            bool b = false;
            if (!Choice.IsInstance && (StartPoint != null) && this.ChoiceStartFrag)
            {
                DecideChoice(MouseLocation);
                b = true;
            }
            this.ChoiceStartFrag = false;
            return b;
        }
        private void TemporaryChoice(ExDisplayPoint MouseLocation)
        {
            EndPoint = (ExEditPoint)MouseLocation;
            //IDCの逐次追加変換を行うことにより高速化可能
            TemporaryItems =  Choice.Translate(StartPoint.Copy(), EndPoint.Copy(),PointMaker);//高速化可能

            this.Items = InverseDataCollection.MergeXOR(this.PreviousItems,this.TemporaryItems);
            

        }
        private void DecideChoice(ExDisplayPoint MouseLocation)
        {
            
            EndPoint = (ExEditPoint)MouseLocation;

            this.TemporaryItems = Choice.Translate(StartPoint.Copy(), EndPoint.Copy(),PointMaker);

            this.Items = InverseDataCollection.MergeXOR(PreviousItems, TemporaryItems);
            this.PreviousItems = this.Items.Copy();
            this.TemporaryItems.Clear();
        }
        
        public void CreateDotLines(ExEditPoint a, ExEditPoint b, InverseChoiceBase icb)
        {
            StartPoint = a;
            EndPoint = b;

            this.TemporaryItems = icb.Translate(StartPoint, EndPoint,this.PointMaker);

            this.Items = InverseDataCollection.MergeXOR(PreviousItems, TemporaryItems);
            this.PreviousItems = this.Items.Copy();
            this.TemporaryItems.Clear();
        }
        
        public void ShiftCreateDotLines(ExEditPoint a, ExEditPoint b)
        {
            StartPoint = a;
            EndPoint = b;
            CreateDotLines(a, b, ShiftChoice);
        }

    }
}
