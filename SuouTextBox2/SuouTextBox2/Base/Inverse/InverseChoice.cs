﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Numerics;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;

using SuouTextBox2.Base;

namespace SuouTextBox2.Base
{    
    public interface InverseChoiceBase
    {
        bool IsInstance { get; }
        bool IsContinuity { get; set; }
        Cursor Image { get; }
        InverseDataCollection Translate(ExEditPoint a, ExEditPoint b, DisplayPointMaker PointMaker);
    }

    public class GataRectangleChoice : InverseChoiceBase
    {
        private bool _IsContinuity = true;
        public bool IsContinuity
        {
            get { return _IsContinuity; }
            set { _IsContinuity = value; }
        }
        public bool IsInstance { get { return false; } }
        public Cursor Image { get { return Cursors.IBeam; } }

        public GataRectangleChoice(bool isContinuity)
        {
            this.IsContinuity = isContinuity;
        }
        public InverseDataCollection Translate(ExEditPoint a, ExEditPoint b,DisplayPointMaker PointMaker)
        {
            InverseDataCollection DLL = new InverseDataCollection();


            EditPoint eLeftUp = new EditPoint();
            EditPoint eRightDown = new EditPoint();

            eLeftUp.X = a.X < b.X ? a.X : b.X;
            eLeftUp.Y = a.Y < b.Y ? a.Y : b.Y;
            eRightDown.X = a.X > b.X ? a.X : b.X;
            eRightDown.Y = a.Y > b.Y ? a.Y : b.Y;

            int limit = eRightDown.Y - eLeftUp.Y + 1;
            for (int k = 0; k < limit; k++)
            {

                var seLeft = PointMaker.CreateExEditPoint(eLeftUp.X,eLeftUp.Y);
                var seRight = PointMaker.CreateExEditPoint(eRightDown.X, eLeftUp.Y);
                seLeft.Y += k; seRight.Y += k;
                var seLeft2 = seLeft.ToStringPoint().ToEditPoint();
                var seRight2 = seRight.ToStringPoint().ToEditPoint();
                
                var len = seRight2.X - seLeft2.X;
                if (len > 0)
                {
                    DotBand dl = new DotBand(seLeft2.X, seLeft2.Y, len);
                    DLL.MergeXOR(dl);
                }
            }

            return DLL;
        }
    }
    public class RectangleChoice : InverseChoiceBase
    {
        private bool _IsContinuity = true;
        public bool IsContinuity
        {
            get { return _IsContinuity; }
            set { _IsContinuity = value; }
        }
        public bool IsInstance { get { return false; } }
        public Cursor Image { get { return Cursors.Cross; } }

        public RectangleChoice(bool continuity)
        {
            this.IsContinuity = continuity;
        }

        public InverseDataCollection Translate(ExEditPoint a, ExEditPoint b, DisplayPointMaker PointMaker)
        {
            InverseDataCollection DLL = new InverseDataCollection();


            EditPoint eLeftUp = new EditPoint();
            EditPoint eRightDown = new EditPoint();

            eLeftUp.X = a.X < b.X ? a.X : b.X;
            eLeftUp.Y = a.Y < b.Y ? a.Y : b.Y;
            eRightDown.X = a.X > b.X ? a.X : b.X;
            eRightDown.Y = a.Y > b.Y ? a.Y : b.Y;


            int limit = eRightDown.Y - eLeftUp.Y + 1;
            for (int k = 0; k < limit; k++)
            {
                var len = eRightDown.X - eLeftUp.X;
                if (len == 0) continue;
                DotBand dl = new DotBand(eLeftUp.X, eLeftUp.Y + k, len);
                DLL.MergeXOR(dl);
            }
            return DLL;
        }
    }
    public class PrimitiveChoice : InverseChoiceBase
    {
        private bool _IsContinuity = false;
        public bool IsContinuity
        {
            get { return _IsContinuity; }
            set { _IsContinuity = value; }
        }
        public bool IsInstance { get { return false; } }
        public Cursor Image { get { return Cursors.IBeam; } }

        
        private InverseDataCollection CreateDotBondsArea(ExEditPoint x, ExEditPoint y)
        {
            InverseDataCollection IDC = new InverseDataCollection();
            
            while (x.Y <= y.Y)
            {

                if (x.Y == y.Y)
                {
                    var x1 = x.ToStringPoint().X;
                    var x2 = y.ToStringPoint().X;
                    var len = x2 - x1;
                    var w = x.Line.Subline(x1, len);
                    if (w.Width > 0)
                    {
                        DotBand d = new DotBand(x.X, x.Y, w.Width);
                        IDC.MergeXOR(new KeyValuePair<DotBand[], bool>(new DotBand[] { d }, false),x.Y);
                    } 
                }
                else
                {
                    
                    if (x.SuffixLine.Width != 0)
                    {
                        DotBand d = new DotBand(x.X, x.Y, x.SuffixLine.Width);
                        IDC.MergeXOR(new KeyValuePair<DotBand[], bool>(new DotBand[] { d }, true),x.Y);
                    }
                    else
                    {
                        IDC.MergeXOR(new KeyValuePair<DotBand[], bool>(new DotBand[] { }, true),x.Y);
                    }
                    
                }
                
                x.Y++; x.X = 0;
            }
            return IDC;
        }
        public InverseDataCollection Translate(ExEditPoint a, ExEditPoint b, DisplayPointMaker PointMaker)
        {
            InverseDataCollection IDC = new InverseDataCollection();

            int limit = b.Y - a.Y + 1;
            ExEditPoint c, d;
            c = a.Copy(); d = b.Copy();
            
            c = c.Stringize();
            d = d.Stringize();
            if (c > d) c.Exchange(d);
            IDC = CreateDotBondsArea(c, d);
            
            return IDC;
            
        }
        
        private int[] ParagraphTranslate(ExEditPoint a, ExEditPoint b)
        {
            var r = new List<int>();
            ExEditPoint c, d;
            if (a < b)
            {
                c = a; d = b;
            }
            else if (a > b)
            {
                c = b; d = a;
            }
            else { c = b; d = a; }
            for (int i = c.Y; i < d.Y; i++) r.Add(i+1);
            return r.ToArray();
        }
    }

}
