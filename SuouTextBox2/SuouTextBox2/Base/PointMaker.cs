﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SuouBase.Text;
using System.Drawing;

namespace SuouTextBox2.Base
{
    public class DisplayPointMaker
    {
        DisplaySetting DisplaySet;
        TextComposition Texter;

        public DisplayPointMaker(DisplaySetting displaySet, TextComposition texter)
        {
            this.DisplaySet = displaySet;
            this.Texter = texter;
        }

        public ExStringPoint Zero()
        {
            return new ExStringPoint(0, 0, DisplaySet, Texter);
        }

        public ExStringPoint CreateExStringPoint(int x, int y)
        {
            return new ExStringPoint(x, y, DisplaySet, Texter);
        }
        public ExEditPoint CreateExEditPoint(int x, int y)
        {
            return new ExEditPoint(x, y, DisplaySet, Texter);
        }
        public ExDotPoint CreateExDotPoint(int x, int y)
        {
            return new ExDotPoint(x, y, DisplaySet, Texter);
        }
        public ExDotPoint CreateExDotPoint(Point p)
        {
            return new ExDotPoint(p.X, p.Y, DisplaySet, Texter);
        }
        public ExDisplayPoint CreateExDisplayPoint(int x, int y)
        {
            return new ExDisplayPoint(x, y, DisplaySet, Texter);
        }
        public ExDisplayPoint CreateExDisplayPoint(Point p)
        {
            return new ExDisplayPoint(p.X, p.Y, DisplaySet, Texter);
        }
        public ExTextAreaPoint CreateExTextAreaPoint(Point p)
        {
            return new ExTextAreaPoint(p.X, p.Y, DisplaySet, Texter);
        }
        public Rectangle GetTextAreaBondLine(int rownum)
        {
            var p = new ExEditPoint(0, rownum, DisplaySet, Texter);
            var sz = new Size(DisplaySet.TextAreaSize.Width, p.BandHeight);
            var p2 = (ExTextAreaPoint)p;
            p2.Y -= Texter.YInterval;
            var rec = new Rectangle(p2.Position , sz);

            return rec;
        }
        /*
        public Rectangle GetDisplayTextAreaBondLine(int rownum)
        {
            var p = new ExEditPoint(0, rownum, DisplaySet, Texter);
            var sz = new Size(DisplaySet.TextAreaSize.Width, p.BandHeight);
            var p2 = (ExDisplayPoint)p;
            p2.Y -= Texter.YInterval;
            var rec = new Rectangle(p2.Position, sz);

            return rec;
        }
        */
        public Rectangle GetTextAreaBondLineInDisplay(int rownum)
        {
            var p = new ExEditPoint(0, rownum, DisplaySet, Texter);
            var sz = new Size(DisplaySet.TextAreaSize.Width, p.BandHeight);
            var p2 = (ExDisplayPoint)p;
            p2.Y -= Texter.YInterval;
            p2.X = DisplaySet.TextAreaPaddingLocation.X;
            var rec = new Rectangle(p2.Position, sz);

            return rec;
        }
        public Rectangle GetTextAreaBondLinesInDisplay(int uprow,int downrow)
        {
            if (uprow == downrow) return this.GetTextAreaBondLineInDisplay(uprow);
            var rec1 = this.GetTextAreaBondLineInDisplay(uprow);
            var rec2 = this.GetTextAreaBondLineInDisplay(downrow);
            var r = new Rectangle(rec1.Location, new Size(rec1.Width, rec2.Bottom - rec1.Top));
            return r;
        }
        public Rectangle GetDotTextAreaBondLine(int rownum)
        {
            var rec = this.GetTextAreaBondLine(rownum);
            var p = this.CreateExTextAreaPoint(rec.Location);
            var np = p.ToDotPoint();
            var rec2 = new Rectangle(np.Position, rec.Size);
            return rec2;
        }
        public Rectangle CreateDotRectangle(DotBand band)
        {
            var exp = this.CreateExEditPoint(band.Location.X, band.Location.Y);
            var rec = exp.CreateDotBandDotRectangle(band.Width);
            return rec;
        }
        [Obsolete]
        public Rectangle CreateTextAreaRectangle(DotBand band)
        {
            
            var exp = this.CreateExEditPoint(band.Location.X, band.Location.Y);
            var rec = exp.CreateDotBandDotRectangle(band.Width);
            var r = ((ExTextAreaPoint)exp);
            r.Y -= Texter.YInterval;
            var rec2 = new Rectangle(r.Position, rec.Size);
            return rec2;
        }
        
        public Rectangle CreateDisplayRectangle(DotBand band)
        {

            var exp = this.CreateExEditPoint(band.Location.X, band.Location.Y);
            var rec = exp.CreateDotBandDotRectangle(band.Width);
            var r = ((ExDisplayPoint)exp);
            r.Y -= Texter.YInterval;
            var rec2 = new Rectangle(r.Position, rec.Size);
            return rec2;
        }
        public Rectangle CreateTextAreaRectangle(int rownum, int nthlength)
        {
            //高さが一定しないことに注意
            if (nthlength <= 0) throw new Exception();
            var recs = new List<Rectangle>();
            for (int i = 0; i < nthlength; i++)
            {
                recs.Add(this.GetTextAreaBondLine(rownum + i));
            }
            var tp = recs.First().Location;
            var width = recs.First().Size.Width;
            var height = recs.Sum((t) => t.Size.Height);
            return new Rectangle(tp, new Size(width,height));
        }
        public Rectangle CreateBottomTextArea(int rownum)
        {
            var p = this.CreateExBandPoint(0, rownum).ToTextAreaPoint();
            var height = this.DisplaySet.TextAreaSize.Height - p.Y;
            var rec = new Rectangle(p.Position, new Size(this.DisplaySet.TextAreaSize.Width, height));
            return rec;
        }
        public ExDotPoint CreateExBandPoint(int x, int rownum)
        {
            var p = this.CreateExEditPoint(x, rownum);
            var q = p.ToDotPoint();
            return new ExDotPoint(q.X, q.Y, DisplaySet, Texter);
        }
        public Rectangle TranslateFromDisplayToTextArea(Rectangle DisplayRec)
        {
            var p = (ExTextAreaPoint)this.CreateExDisplayPoint(DisplayRec.Location);
            return new Rectangle(p.Position, DisplayRec.Size);

        }
        public Rectangle TranslateFromDotToDisplay(Rectangle DotRec)
        {
            var p = (ExDisplayPoint)this.CreateExDotPoint(DotRec.Location.X, DotRec.Location.Y);
            return new Rectangle(p.Position, DotRec.Size);
        }
        public Rectangle TranslateFromDotToTextAreaInDisplay(Rectangle DotRec)
        {
            var p = (ExDisplayPoint)this.CreateExDotPoint(DotRec.Location.X, DotRec.Location.Y);
            var vrec = new Rectangle(p.Position, DotRec.Size);
            return Rectangle.Intersect(DisplaySet.TextAreaRectangle, vrec);
        }
        public Rectangle TranslateFromDisplayToDot(Rectangle DisplayRec)
        {
            var p = (ExDotPoint)this.CreateExDisplayPoint(DisplayRec.Location);
            var vrec = new Rectangle(p.Position, DisplayRec.Size);
            return vrec;
        }
        public int TranslateDotToDisplayX(int x)
        {
            return this.CreateExDotPoint(x, 0).ToDisplayPoint().X;
        }
    }
}
