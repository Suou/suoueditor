﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using System.Drawing;
using System.Threading.Tasks;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Controls;
using SuouBase.Text;
using SuouTextBox2.Base;
using SuouTextBox2.Settings;
using SuouBase.Drawing;
using SuouBase.Book;
namespace SuouTextBox2
{
    [IComponentPlugin("SuouTextBox2",PluginType.Control)]
    public class SuouTextBoxPlugin : SuouBase.Interface.ITextEditClass
    {

        public SuouTextBoxPlugin()
        {
#if DEBUG
            this.TextBox = new SimoziBox() { UseDebug = false };
#else
            this.TextBox = new SimoziBox() { UseDebug = false};
#endif
            this.TextBox.TextChanged += this.OnInnerTextChanged;
            this.TextBox.ShortCutKeys = this.ShortCutKeys;
            this.TextBox.CaretPositionChanged += this.OnCaretPositionChanged;
        }
        public SuouTextBoxPlugin(EditorInfo DLLFunction)
            : this()
        {
            this.MenuItem.HoldingSimoziBox = this.TextBox;
            this.DLLFunction = DLLFunction;
            //this.DLLFunction.MasterForm.TextEditorMenuItem.DropDownOpening += this.EditMenu_Opening;

            this.TextBox.DLLFunction = DLLFunction;

            var f = SimoziBoxSaveData.Load(this.SaveFileName);
            this.TextBox.SetSaveData(f);

            this.SettingPageCtrl.ParentTextBox = this.TextBox;
            //this.SettingPageCtrl.ShortCutItems = this.TextBox.ShortCutKeys;
            this.TextBox.DropPageAccepted += this.DropPageItem_Accepted;
            this.DLLFunction.StartSettingEnded += this.DLLFunction_StartSettingEnded;

            this.Control.ParentChanged += (a, b) => this.Control.ParentForm.MainMenuStrip.Items.Insert(1, this.MenuItem);


        }
        //public bool IsDirectPage { get; set; }
        //public IBookPage PointerPage { get; set; }

        

        #region Property
        public string SaveFileName = SuouEditorConst.SettingDirectory + "/" + "SuouTextBox.stn";
        public EditorInfo DLLFunction { get; set; }
        public string Name
        {
            get
            {
                return this.Control.Name;
            }
        }
        public Size CompactSize {
            get
            {
                return new Size(500, 500);
            }
        }
        public SimoziBox TextBox{get;set;}
        public event EventHandler InnerTextChanged;
        public event EventHandler PageChanged;
        public IBookPage BookPage
        {
            get { return this.TextBox.BookPage; }
            set 
            { 
                this.TextBox.BookPage = value;
                if (this.PageChanged != null) this.PageChanged(this, null);
            }
        }
        private SimoziSetting _SimoziSettingPage = null;
        public SimoziSetting SimoziSettingPage
        {
            get
            {
                if(this._SimoziSettingPage == null)this._SimoziSettingPage = new SimoziSetting(this.DLLFunction.AltHukidasiPlugin,this.TextBox);
                this._SimoziSettingPage.Set(this.TextBox.SaveData);
                return this._SimoziSettingPage;
            }
        }
        public SimoziBoxSaveData SaveData
        {
            get
            {
                return this.TextBox.SaveData;
            }
        }
        public TextBoxSetting SettingPageCtrl = new TextBoxSetting();
        public SettingPage[] SettingPage { get { return new SettingPage[] { this.SimoziSettingPage }; } }
        public SuouTextBoxMenuItem MenuItem = new SuouTextBoxMenuItem();
        public ContainerControl Control
        {
            get
            {
                return TextBox;
            }
        }
        public Form Form
        {
            get { return null; }
        }
        public bool DisposeCheck()
        {
            this.TextBox.SaveData.Save(SaveFileName);
            return true;
        }
        public ToolStripItemCollection ToolStripItems
        {
            get
            {
                return null;
            }
        }
        public PrintFontSetting PrintFontSetting
        {
            get
            {
                return this.TextBox.PrintFontSetting;
            }
        }

        public SectionScenePart CopyLayer
        {
            get;
            set;
        }

        public Color BackColor
        {
            get { return TextBox.BackColor; }
            set { TextBox.BackColor = value; }
        }
        private TextBoxShortCutKeys ShortCutKeys = new TextBoxShortCutKeys();
        //private SettingPage[] TextBoxSettingPages {get;set;}
        #endregion
        #region TextMethod
        public string Text
        {
            get { return this.TextBox.Text.ToString(); }
            set
            {
                throw new Exception();
                /*
                if (value == null)
                {
                    this.TextBox.Initialize("");
                }
                else
                {
                    this.TextBox.Initialize(value);
                }
                */
            }
        }
        public string LayerText
        {
            get { return null; }
            set { }
        }
        public string CopyText
        {
            get
            {
                throw new NotImplementedException();
                //return null;
            }
        }
        public string LayerCopyText
        {
            get
            {
                throw new NotImplementedException();
                //return null;
            }
        }
        public void InsertText(string text)
        {
            var textbox = this.TextBox.MergeLayerItemCollection.SelectedTextBox;
            if (textbox != null)
            {
                textbox.TextOperator.InsertText(text);
                textbox.Focus();
            }
        }
        public void InsertLayerText(string text)
        {
            throw new NotImplementedException();
            //this.OATB.InsertLayerText(text);
        }
        public void AddText(string text)
        {
            throw new NotImplementedException();
            //this.TextBox.AddText(text);
        }
        public void AddLayerText(string text)
        {
            var v = new SuouText(text);
            var page = this.TextBox.MergeLayerItemCollection.CreateNewPage(v, new EditPoint(0, 0));
            this.TextBox.MergeLayerItemCollection.Scene.Parts.Add(page);
            //var v = new SuouText(text);
            //this.TextBox.PageItems.AddNewLayer(v,new EditPoint(0,0));
        }
        #endregion
        public void TemporaryDispose()
        {
            this.TextBox.TemporaryDispose();
        }

        private ToolStripMenuItem CreateEditMenuStripItem()
        {
            var f = new ToolStripMenuItem() { Text = "編集" };
            //f.DropDownOpening += this.EditMenu_Opening;
            return f;
        }

        
        private void SendNotifyMessage()
        {
            if (this.Page == null) return;
            var p = this.TextBox.ParentForm as INotifyStripForm;
            if (p != null)
            {
                var v = this.TextBox.MergeLayerItemCollection.SelectedNotifyPage;
                var s = string.Format("{0},{1}", v.NotifyPageSizeMessage(), SuouTextBox.NotifyCaretInformation(this.TextBox.Caret));
                p.NotifyMessage(s);
            }
        }
        private void OnCaretPositionChanged(object sender, EventArgs e)
        {
            this.SendNotifyMessage();
        }

        public void MakeNameDictionary(Dictionary<string,List<ToolStripItem>> dic)
        {
        }
        
        public void Refresh(EditingFile ef)
        {

            if (ef != null)
            {
                this.Page = ef.SelectedPage;
            }
        }
        //public IBookPage _Page = null;
        public bool IsEmpty { get; private set; }
        public IBookPage Page
        {
            get {
                if (this.IsEmpty) return null;
                return this.TextBox.MergeLayerItemCollection.Scene; 
            }
            set
            {
                if (value != null)
                {
                    if (value is ObsoleteChapterBookPage)
                    {
                        throw new Exception();
                    }
                    else
                    {

                        this.TextBox.BookPage = value;
                        this.DLLFunction.Framework.Focus(this);
                        this.IsEmpty = false;
                    }
                }
                else
                {
                    this.TextBox.BookPage = new AstPage();
                    this.IsEmpty = true;
                }
            }
        }
        private void SuperVisor_PageChanged(object sender, EditingFileEventArgs e)
        {
            var f = e.File;
            if (f == null)
            {
                this.Page = null;
            }
            else
            {
                this.Page = f.SelectedPage;
            }
        }
        private void DropPageItem_Accepted(object sender, BookPageContentChangeEventArgs e)
        {
            var f = DLLFunction.PluginList.FirstOrDefault((v) => v.Plugin is IAASelectClass);
            if (f != null)
            {
                var pf = (IAASelectClass)f.Plugin;
                pf.NotifyDropPageItem(e.Page);
            }
        }
        private void OnInnerTextChanged(object sender, EventArgs e)
        {
            if (this.Page != null)
            {
                this.SendNotifyMessage();
            }
            if (InnerTextChanged != null)
            {
                InnerTextChanged(sender, e);
            }

        }

        private void DLLFunction_StartSettingEnded(object sender, EventArgs e)
        {
            //this.Control.ParentForm.MainMenuStrip.Items.Add(this.CreateEditMenuStripItem());
        }
    }


}
