﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;
using System.Diagnostics;
namespace SuouTextBox2.Debug
{
    public partial class AltCharViewer : Form
    {
        public AltCharViewer()
        {
            InitializeComponent();
        }
        private ISpaceMaker _Item;
        public ISpaceMaker Item
        {
            get
            {
                return _Item;
            }
            set
            {
                this._Item = value;
                
                if (value != null)
                {
                    var s = new StringBuilder("");

                    Stopwatch sw = new Stopwatch();
                    sw.Start();
                    for (int i = 0; i < 1000; i++)
                    {
                        if (this.Item.CanCreateSpace(i))
                        {
                            s.Append(i + "/" + this.Item.CreateSpace(i).Sentence + System.Environment.NewLine);
                        }
                        else
                        {
                            s.Append( i + "/" + "Cannot"+System.Environment.NewLine);
                        }
                    }
                    for (int i = 0; i < 1000; i++)
                    {
                        if (this.Item.CanCreateSpace(i))
                        {
                            s.Append(i + "/" + this.Item.CreateSpace(i).Sentence + System.Environment.NewLine);
                        }
                        else
                        {
                            s.Append(i + "/" + "Cannot" + System.Environment.NewLine);
                        }
                    }
                    sw.Stop();
                    this.suouTextBox1.Text = s.ToString();
                }
            }
        }
    }
}
