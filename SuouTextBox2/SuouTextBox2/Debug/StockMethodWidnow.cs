﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;
namespace SuouTextBox2.Debug
{
    public partial class StockMethodWidnow : Form
    {
        public StockMethodWidnow()
        {
            InitializeComponent();
        }

        public void SetStockData(TextStock ts)
        {
            ts.ItemChanged += this.Stock_ItemChanged;
        }

        public List<EditorState> RedoStock { get; set; }
        public List<EditorState> UndoStock { get; set; }
        public EditorState NowState { get; set; }

        private void ResetItem()
        {
            this.listBox1.Items.Clear();
            this.listBox2.Items.Clear();
            int n = 0;
            RedoStock.ForEach((v) => this.listBox1.Items.Add(n++));
            n = 0;
            UndoStock.ForEach((v) => this.listBox2.Items.Add(n++));

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox1.SelectedIndex >= 0 && this.listBox1.SelectedIndex < this.RedoStock.Count)
            {
                var es = this.RedoStock[this.listBox1.SelectedIndex];
                this.ItemSelect(es);
            }
        }
        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listBox2.SelectedIndex >= 0 && this.listBox2.SelectedIndex < this.UndoStock.Count)
            {
                var es = this.UndoStock[this.listBox2.SelectedIndex];
                this.ItemSelect(es);
            }
        }
        private void ItemSelect(EditorState es)
        {
            this.textBox1.Text = es.Text.Text;
            this.label1.Text = "X =" + es.SP.X + ", Y =" + es.SP.Y;
        }
        private void Stock_ItemChanged(object senser, TextStockEventArgs e)
        {
            this.RedoStock = e.RedoStock.ToList();
            this.UndoStock = e.UndoStock.ToList();
            this.NowState = e.NowState;
            this.ResetItem();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.ItemSelect(this.NowState);
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
