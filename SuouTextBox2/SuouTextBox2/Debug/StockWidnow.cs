﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;
namespace SuouTextBox2.Debug
{
    public partial class StockWidnow : Form
    {
        public SuouTextBox Box { get; set; }
        public StockWidnow()
        {
            InitializeComponent();
            Items = new List<SuouText>();
        }
        private List<SuouText> Items { get; set; }
        public void Add(SuouText tc)
        {
            this.Items.Add(tc);
            this.listBox1.Items.Add("tf");
        }
        public void Add(SuouText tc,string name)
        {
            this.Items.Add(tc);
            this.listBox1.Items.Add(name);
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(this.listBox1.SelectedIndex >= 0 && this.listBox1.SelectedIndex <Items.Count){
                var text = this.Items[this.listBox1.SelectedIndex];
            this.textBox1.Text = text.Text;
            this.label1.Text = text.Width.ToString();
            this.label2.Text = text.Height.ToString();
            this.label3.Text = text.Count.ToString();
            }
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }
        private Point DestLocation
        {
            get
            {
                return new Point((int)this.numericDestX.Value, (int)this.numericDestY.Value);
            }
        }
        private Point SrcLocation
        {
            get
            {
                return new Point((int)this.numericSrcX.Value, (int)this.numericSrcY.Value);
            }
        }
        private Size ImageSize
        {
            get
            {
                return new Size((int)this.numericSizeWidth.Value, (int)this.numericSizeHeight.Value);
            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            /*
            this.pictureBox1.Size = ImageSize;
            var bmp = new Bitmap(ImageSize.Width, ImageSize.Height, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
            var gp = Graphics.FromImage(bmp);
            this.Box.DrawOperator.DrawImage(gp, DestLocation, new DotPoint(SrcLocation.X,SrcLocation.Y), ImageSize);
            this.pictureBox1.Image = bmp;
            */
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var g = this.pictureBox1.CreateGraphics();
            var v = this.Box.TextOperator.Text;
            g.Clear(Color.White);
            for (int i = 0; i < v.Count; i++)
            {
                var line = v.Line(i);
                g.DrawImage(line.InverseImage, line.Location.Position);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Box.RefleshDraw();
        }
    }
}
