﻿namespace SuouTextBox2.Debug
{
    partial class StockWidnow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.numericSizeHeight = new System.Windows.Forms.NumericUpDown();
            this.numericSizeWidth = new System.Windows.Forms.NumericUpDown();
            this.numericSrcY = new System.Windows.Forms.NumericUpDown();
            this.numericSrcX = new System.Windows.Forms.NumericUpDown();
            this.numericDestX = new System.Windows.Forms.NumericUpDown();
            this.numericDestY = new System.Windows.Forms.NumericUpDown();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.button3 = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericSizeHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSizeWidth)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSrcY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSrcX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDestX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDestY)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(3, 3);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(111, 374);
            this.listBox1.TabIndex = 0;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // textBox1
            // 
            this.textBox1.AcceptsReturn = true;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.textBox1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1.Location = new System.Drawing.Point(114, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(392, 374);
            this.textBox1.TabIndex = 1;
            this.textBox1.WordWrap = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(542, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(544, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(546, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(35, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "label3";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(611, 406);
            this.tabControl1.TabIndex = 5;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.listBox1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(603, 380);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.pictureBox1);
            this.tabPage2.Controls.Add(this.panel1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(603, 380);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Location = new System.Drawing.Point(203, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(301, 374);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.numericSizeHeight);
            this.panel1.Controls.Add(this.numericSizeWidth);
            this.panel1.Controls.Add(this.numericSrcY);
            this.panel1.Controls.Add(this.numericSrcX);
            this.panel1.Controls.Add(this.numericDestX);
            this.panel1.Controls.Add(this.numericDestY);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 374);
            this.panel1.TabIndex = 3;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(19, 165);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 126);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 7;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // numericSizeHeight
            // 
            this.numericSizeHeight.Location = new System.Drawing.Point(100, 86);
            this.numericSizeHeight.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericSizeHeight.Name = "numericSizeHeight";
            this.numericSizeHeight.Size = new System.Drawing.Size(82, 19);
            this.numericSizeHeight.TabIndex = 6;
            // 
            // numericSizeWidth
            // 
            this.numericSizeWidth.Location = new System.Drawing.Point(14, 86);
            this.numericSizeWidth.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericSizeWidth.Name = "numericSizeWidth";
            this.numericSizeWidth.Size = new System.Drawing.Size(82, 19);
            this.numericSizeWidth.TabIndex = 5;
            // 
            // numericSrcY
            // 
            this.numericSrcY.Location = new System.Drawing.Point(100, 45);
            this.numericSrcY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericSrcY.Name = "numericSrcY";
            this.numericSrcY.Size = new System.Drawing.Size(82, 19);
            this.numericSrcY.TabIndex = 4;
            // 
            // numericSrcX
            // 
            this.numericSrcX.Location = new System.Drawing.Point(25, 45);
            this.numericSrcX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericSrcX.Name = "numericSrcX";
            this.numericSrcX.Size = new System.Drawing.Size(69, 19);
            this.numericSrcX.TabIndex = 3;
            // 
            // numericDestX
            // 
            this.numericDestX.Location = new System.Drawing.Point(14, 3);
            this.numericDestX.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericDestX.Name = "numericDestX";
            this.numericDestX.Size = new System.Drawing.Size(80, 19);
            this.numericDestX.TabIndex = 1;
            this.numericDestX.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // numericDestY
            // 
            this.numericDestY.Location = new System.Drawing.Point(100, 3);
            this.numericDestY.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericDestY.Name = "numericDestY";
            this.numericDestY.Size = new System.Drawing.Size(82, 19);
            this.numericDestY.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(603, 380);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "tabPage3";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(32, 30);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "button3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // StockWidnow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 406);
            this.Controls.Add(this.tabControl1);
            this.Name = "StockWidnow";
            this.Text = "StockWidnow";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericSizeHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSizeWidth)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSrcY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericSrcX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDestX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericDestY)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.NumericUpDown numericDestX;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.NumericUpDown numericSizeHeight;
        private System.Windows.Forms.NumericUpDown numericSizeWidth;
        private System.Windows.Forms.NumericUpDown numericSrcY;
        private System.Windows.Forms.NumericUpDown numericSrcX;
        private System.Windows.Forms.NumericUpDown numericDestY;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button3;
    }
}