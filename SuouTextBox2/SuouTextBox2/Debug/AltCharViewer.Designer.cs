﻿namespace SuouTextBox2.Debug
{
    partial class AltCharViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AltCharViewer));
            this.suouTextBox1 = new SuouTextBox2.SuouTextBox();
            this.SuspendLayout();
            // 
            // suouTextBox1
            // 
            this.suouTextBox1.AllowDrop = true;
            this.suouTextBox1.CanOriginalDrag = false;
            this.suouTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.suouTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.suouTextBox1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F);
            this.suouTextBox1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.suouTextBox1.IsInnerScrollBar = true;
            this.suouTextBox1.IsRuler = false;
            this.suouTextBox1.Location = new System.Drawing.Point(0, 0);
            this.suouTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.suouTextBox1.Name = "suouTextBox1";
            this.suouTextBox1.Size = new System.Drawing.Size(284, 262);
            this.suouTextBox1.TabIndex = 0;
            this.suouTextBox1.UserInputEnable = true;
            // 
            // AltCharViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.suouTextBox1);
            this.Name = "AltCharViewer";
            this.Text = "AltCharViewer";
            this.ResumeLayout(false);

        }

        #endregion

        private SuouTextBox suouTextBox1;
    }
}