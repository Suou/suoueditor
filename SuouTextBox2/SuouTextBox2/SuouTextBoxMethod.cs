﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Operator;
using SuouTextBox2.Base;
using SuouTextBox2.Controls;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase;
using SuouTextBox2.Debug;
using SuouBase.Drawing;
using BSystem;
using WebExtension;

namespace SuouTextBox2
{
    public partial class SuouTextBox
    {
        #region Method
        public ExEditPoint ModifyLocation(Point Location)
        {
            var p = (ExEditPoint)PointMaker.CreateExDisplayPoint(Location);
            return p;
        }
        public DotPoint ModifyDotLocation(Point Location)
        {
            var p = PointMaker.CreateExDisplayPoint(Location).ToDotPoint();
            return new DotPoint(p.X, p.Y);
        }

        public void SetScrollBar(VScrollBar vsb, HScrollBar hsb)
        {
            vsb.ValueChanged += this.vScrollBar1_ValueChanged;
            hsb.ValueChanged += this.hScrollBar1_ValueChanged;
            vsb.Scroll += this.vScrollBar1_Scroll;
            hsb.Scroll += this.hScrollBar1_Scroll;
            this.DisplaySet.SetScrollVar(vsb, hsb);

        }

        protected override void WndProc(ref Message m)
        {

            if (InputOperator != null && UserInputEnable)
            {
                if (InputOperator.WndProc(ref m)) TurnEnd();
            }

            base.WndProc(ref m);
        }

        public void Merge(TextComposition text, EditPoint Location)
        {
            throw new NotImplementedException();
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (UserInputEnable)
            {
                

                if (InputOperator.ProcessDialogKey(keyData))
                {
                    TurnEnd();
                    return true;
                }
                else
                {
                    return base.ProcessDialogKey(keyData);
                }
            }
            else
            {
                return base.ProcessDialogKey(keyData);
            }
        }
        public void Initialize(string text)
        {
            var suoutext = new SuouText(text);
            this.Initialize(suoutext);
        }
        public void Initialize(SuouText Text)
        {
            if (this.TextOperator == null) return;

            this.MouseInputOperator.Clear();
            this.TextOperator.DeleteAll();
            //this.TextOperator.StockOperator.Clear();
            this.TextOperator.SetText(Text);
            TurnEnd();
        }

        public BasicLineMerge NotInverseText()
        {
            if (this.InverseOperator.InverseDataItems.IsEmpty) return this.TextOperator.Text.ToLineMerge(this.SpaceMaker, true);
            return this.TextMergeProcessor.XORPartialCopy(this.TextOperator.Text, this.InverseOperator.InverseDataItems);
        }

        public bool IsVisibleCaret
        {
            get { return this.Caret.VisibleMode; }
            set { this.Caret.VisibleMode = value; }
        }
        private void Preload()
        {
            this.suouTextBoxContextMenuStrip1.HoldingEditor = this;
            this.Debugger.Box = this;

            DisplaySet = new DisplaySetting(this, PrintFontSet, vScrollBar1, hScrollBar1);
            IsInnerScrollBar = true;

            this.Font = PrintFontSet.Font;
            TextMergeProcessor = new AltText(SpaceMaker);

            Texter = new TextComposition(PrintFontSet);
            _PointMaker = new DisplayPointMaker(DisplaySet, Texter);
            Caret = new TextBoxCaret(this, PointMaker.Zero());

            DisplaySet.Texter = Texter;

            MouseInputOperator = new MouseInputMethod(this, PointMaker);
            InputOperator = new InputMethod(Caret, this);
            DrawOperator = new DrawMethod(this, DisplaySet, PrintFontSet, Texter, Caret, PointMaker);
            TextOperator = new TextMethod(this, Texter, Caret, PointMaker, this.PrintFontSet);
            //CommandOperator = new CommandMethod(TextOperator);
            CutPasteOperator = new CutPasteMethod(PrintFontSet, MouseInputOperator, TextOperator, PointMaker, this);
            //StockOperator = new StockMethod(TextOperator,UseDebug);
            this.SetShortCutKeys(new TextBoxShortCutKeys());

            //this.TextOperator.StockOperator = this.StockOperator;
            this.TextOperator.InverseOperator = this.InverseOperator;
            this.TextOperator.CutPasteOperator = this.CutPasteOperator;

            CutPasteOperator.TextMergeProcessor = TextMergeProcessor;

            //this.TurnEnded += this.TextOperator.StockOperator.SuouEditor_TurnEnded;
            this.MouseWheel += MouseWheeled;
            this.CMouseWheel += DisplaySet.Control_MouseWheel;
            InputOperator.KeyInputed += TextOperator.KeyInputed;
            //InputOperator.CursorInputed += CommandOperator.KeyInputMethod_Inputed;
            InputOperator.KeyInputed += InverseOperator.KeyInputed;
            InputOperator.CursorInputed += CutPasteOperator.KeyInputMethod_Inputed;
            //InputOperator.CursorInputed += this.TextOperator.StockOperator.KeyInputMethod_Inputed;
            TextOperator.TextChanged += DrawOperator.TextChanged_Draw;
            TextOperator.TextChanged += Caret.TextInserted_Caret;
            Caret.CaretChanged += this.Caret_Changed;
            InputOperator.CursorSecondInputed += Caret.InputedCursor_Caret;
            MouseInputOperator.MouseDown += Caret.MouseInput_MouseDown;
            MouseInputOperator.MouseMove += Caret.MouseInput_MouseDown;

            TextOperator.TextChanged += DisplaySet.TextOperator_TextChanged;
            this.CSizeChanged += DisplaySet.Control_SizeChanged;
            DrawOperator.Drawing += Caret.ScrollBar_ValueChanged;
            this.VsbValueChanged += DrawOperator.ScrollBar_ValueChanged;
            this.HsbValueChanged += DrawOperator.ScrollBar_ValueChanged;
            Caret.CaretOutFromTextArea += DisplaySet.Caret_OutFromTextArea;
            this.CPainted += DrawOperator.Control_Paint;
            this.TurnEnded += DrawOperator.Control_TurnEnded;
            this.CSizeChanged += DrawOperator.Control_SizeChanged;
            MouseInputOperator.InverseChanged += this.DrawOperator.InverseMethod_InverseChanged;

            //TextOperator.TextChanged += StockOperator.TextOperator_TextChanged;
            TextOperator.TextChanged += this.TextOperator_TextChanged;
            this.PrintFontSet.SettingChanged += this.FontSet_SettingChanged;

            TurnEnd();
        }


        public void AddText(string s)
        {
            TextOperator.AddText(s);
            TurnEnd();
        }
        public void InsertText(string s)
        {
            TextOperator.InsertText(s);
            TurnEnd();
        }
        public IPenMouse Pen
        {
            get { return this.MouseInputOperator.LeftPenItem; }
            set
            {
                if (this.MouseInputOperator != null)
                {
                    //if(value != null)value.Ctrl = this;
                    this.MouseInputOperator.LeftPenItem = value;
                }
            }
        }
        public Bitmap GetDisplayImage(DotPoint SrcLocation, Size ImageSize, Color backColor)
        {
            return this.DrawOperator.GetImage(SrcLocation, ImageSize, backColor);
        }
        public Bitmap GetDisplayImage(Bitmap bmp, DotPoint SrcLocation)
        {
            return this.DrawOperator.GetTextImage(bmp, SrcLocation);
        }

        public Point TranslateToDisplayPoint(EditPoint ep)
        {
            var p = PointMaker.CreateExEditPoint(ep.X, ep.Y);
            return ((ExDisplayPoint)p).Position;
        }

        #endregion

        public void MoveLeft()
        {
            var p = this.RawText.MoveLeft(this.SpaceMaker);
            this.TextOperator.ReplaceText(p);
            TurnEnd();
        }
        public void DeleteRightSpace()
        {
            var v = this.RawText.DeleteRight(this.SpaceMaker);

            this.TextOperator.ReplaceText(v);
            TurnEnd();
        }

        public void Transparentize()
        {
            this.CutPasteOperator.TransParentKey();
            TurnEnd();
        }
        public void Cut()
        {
            CutPasteOperator.CutKey();
            TurnEnd();
        }
        public void Copy()
        {
            CutPasteOperator.CopyKey(this.TextSetting.CopyEncodeType);
            TurnEnd();
        }
        public void Paste()
        {
            CutPasteOperator.PasteKey();
            TurnEnd();
        }
        public void Delete()
        {
            CutPasteOperator.DeleteKey();
            TurnEnd();
        }
        public void ChoiceAll()
        {
            MouseInputOperator.ALLChoiceKey(TextOperator.Text);
            TurnEnd();
        }
        public void Undo()
        {
            this.TextOperator.StockOperator.StoreBack();
            TurnEnd();
        }
        public void SpaceReplace()
        {
            if (TextMergeProcessor.CanSpaceReplace(this.TextOperator.Text, this.MouseInputOperator.SuouInverseItem))
            {
                var r = TextMergeProcessor.SpaceReplace(this.TextOperator.Text, this.InverseOperator.InverseDataItems);
                TextOperator.SetText(r, new StringPoint(), TextChangeType.Other);
                this.MouseInputOperator.Clear();
            }
            else
            {
                MessageBox.Show("Cannot");
            }
        }
        public void Redo()
        {
            this.TextOperator.StockOperator.StoreForward();
            TurnEnd();
        }
        public void ChangePen(InverseChoiceBase pen)
        {
            InverseOperator.ICB = pen;
            TurnEnd();
        }
        public bool CanPaste
        {
            get { return this.CutPasteOperator.CanPaste(); }
        }
        public bool CanCut
        {
            get { return !InverseOperator.InverseDataItems.IsEmpty; }
        }
        public bool CanCopy
        {
            get { return !InverseOperator.InverseDataItems.IsEmpty; }
        }
        public bool CanDelete
        {
            get { return !InverseOperator.InverseDataItems.IsEmpty; }
        }
        public bool CanTransparentize
        {
            get { return !InverseOperator.InverseDataItems.IsEmpty; }
        }
        public bool CanSpaceReplace
        {
            get { return this.CutPasteOperator.CanSpaceReplace(); }
        }
        public bool CanUndo
        {
            get { return this.TextOperator.StockOperator.CanStoreBack; }
        }
        public bool CanRedo
        {
            get { return this.TextOperator.StockOperator.CanStoreBack; }
        }
        public bool CanAllSelect
        {
            get { return this.UserInputEnable; }
        }
    }
}
