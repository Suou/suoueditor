﻿namespace SuouTextBox2
{
    partial class SuouTextBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.suouTextBoxContextMenuStrip1 = new SuouTextBox2.Controls.SuouTextBoxContextMenuStrip();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Cursor = System.Windows.Forms.Cursors.Default;
            this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar1.Location = new System.Drawing.Point(305, 0);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(17, 284);
            this.vScrollBar1.TabIndex = 0;
            this.vScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.vScrollBar1_Scroll);
            this.vScrollBar1.ValueChanged += new System.EventHandler(this.vScrollBar1_ValueChanged);
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Cursor = System.Windows.Forms.Cursors.Default;
            this.hScrollBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.hScrollBar1.Location = new System.Drawing.Point(0, 267);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(305, 17);
            this.hScrollBar1.TabIndex = 1;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            this.hScrollBar1.ValueChanged += new System.EventHandler(this.hScrollBar1_ValueChanged);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // suouTextBoxContextMenuStrip1
            // 
            this.suouTextBoxContextMenuStrip1.HoldingEditor = null;
            this.suouTextBoxContextMenuStrip1.Name = "suouTextBoxContextMenuStrip1";
            this.suouTextBoxContextMenuStrip1.Size = new System.Drawing.Size(125, 280);
            this.suouTextBoxContextMenuStrip1.Text = "編集";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // SuouTextBox
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.contextMenuStrip2;
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.vScrollBar1);
            this.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.ImeMode = System.Windows.Forms.ImeMode.On;
            this.Name = "SuouTextBox";
            this.Size = new System.Drawing.Size(322, 284);
            this.BackColorChanged += new System.EventHandler(this.SuouTextBox_BackColorChanged);
            this.SizeChanged += new System.EventHandler(this.SuouTextBox2_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.SuouTextBox2_VisibleChanged);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.SuouTextBox2_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.SuouTextBox2_DragEnter);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SuouTextBox2_Paint);
            this.Enter += new System.EventHandler(this.SuouTextBox_Enter);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SuouTextBox2_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.SuouTextBox2_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SuouTextBox2_MouseUp);
            this.Resize += new System.EventHandler(this.SuouTextBox2_Resize);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        public Controls.SuouTextBoxContextMenuStrip suouTextBoxContextMenuStrip1;
        private System.Windows.Forms.Timer timer1;
    }
}
