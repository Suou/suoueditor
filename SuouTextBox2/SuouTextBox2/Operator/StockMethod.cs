﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouTextBox2.Base;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Debug;
namespace SuouTextBox2.Operator
{
    public class StockMethod
    {
        TextMethod TextOperator;

        public bool IsStockDone = false;
        private TextStock StoreData = new TextStock();
        

        public StockMethod(TextMethod TextOperator,bool usestockwindow)
        {
            this.TextOperator = TextOperator;
            //this.Stock(TextOperator.Text,new StringPoint());
            this.Clear();


        }
        public void Clear()
        {
            this.StoreData.Clear(this.TextOperator.Text.Clone(),new StringPoint(0,0));
        }
        public void RemoveStockCheck()
        {
            IsStockDone = false;
        }
        public void Stock(TextComposition text,StringPoint p,StringPoint BeforeCaretLocation)
        {
            if (IsStockDone) throw new SuouBase.MyException("1ターンにストックできるのは１つだけです。");
            var r = text.Clone();

            StoreData.Store(r, p, BeforeCaretLocation);
            IsStockDone = true;

        }
        public void StoreBack()
        {
            if (IsStockDone) throw new SuouBase.MyException("1ターンにストックできるのは１つだけです。");
            
            if (StoreData.CanBack)
            {
                var r = StoreData.Back();
                TextOperator.SetText(r.Text.Clone(), r.SP, TextChangeType.Stock);
            }
            IsStockDone = true;
        }
        public void StoreForward()
        {
            if (IsStockDone) throw new SuouBase.MyException("1ターンにストックできるのは１つだけです。");
            
            if (StoreData.CanForward)
            {
                var r = StoreData.Forward();
                TextOperator.SetText(r.Text.Clone(),r.SP,TextChangeType.Stock);
            }

            IsStockDone = true;
        }
        public bool CanStoreBack
        {
            get { return StoreData.CanBack; }
        }
        public bool CanStoreForward
        {
            get { return StoreData.CanForward; }
        }

        public void TextOperator_TextChanged(object sender, TextChangeEventArg e)
        {
            switch (e.Type)
            {
                case TextChangeType.Other:
                    Stock(e.TextData, e.AfterCaretLocation, e.BeforeCaretLocation);
                    break;
                case TextChangeType.Initialize:
                    this.Clear();
                    break;
            }
        }
        public bool KeyInputMethod_Inputed(object sender, InputCursorEventArg e)
        {
            if (e.KeyData.IsCtrl)
            {
                var IsV = e.KeyData.KeyData & ~Keys.Control;
                if (IsV == Keys.Z)
                {
                    StoreBack();
                    return true;
                }
                else if (IsV == Keys.R)
                {
                    StoreForward();
                    return true;
                }

            }
            return false;
        }
        public void SuouEditor_TurnEnded(object sender, EventArgs e)
        {
            this.IsStockDone = false;
        }
    }
}
