﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouTextBox2.Base;
using SuouBase;
using System.Diagnostics;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Drawing;
using BSystem;
using WebExtension;
using BSystem.Text;
using BSystem.Collections;
namespace SuouTextBox2.Operator
{
    public class CutPasteMethod
    {
        //public TextProcess TextProcessor { get; set; }
        public TextMethod TextOperator { get; set; }
        public InverseDataLapper IDL { get { return this.MouseOperator.SuouInverseItem; } }
        //public InverseDataCollection IDC { get { return this.MouseOperator.SuouInverseItem.Item; } }
        public MouseInputMethod MouseOperator { get; set; }
        public DisplayPointMaker PointMaker { get; set; }
        public SuouTextBox ParentTextBox { get; set; }
        public AltText TextMergeProcessor { get; set; }

        public CutPasteMethod(PrintFontSetting PrintSet,MouseInputMethod IM,TextMethod TM,DisplayPointMaker PM,SuouTextBox ParentTextBox)
        {
            this.MouseOperator = IM;
            this.TextOperator = TM;
            this.PointMaker = PM;
            this.ParentTextBox = ParentTextBox;
            //TextProcessor = new TextProcess(300, SpaceChar.SimpleSpaceSet, PrintSet.Font, PrintSet.TextColor,PrintSet.YLineSpace,PrintSet);
        }

        public bool KeyInputMethod_Inputed(object sender, InputCursorEventArg e)
        {
            
            if (ParentTextBox.ShortCutKeys.Copy == e.KeyData.KeyData)
            {
                CopyKey(this.ParentTextBox.TextSetting.CopyEncodeType);
                return true;
            }
            else if (ParentTextBox.ShortCutKeys.Cut == e.KeyData.KeyData)
            {
                CutKey();
                return true;
            }
            else if (ParentTextBox.ShortCutKeys.Paste == e.KeyData.KeyData)
            {
                PasteKey();
                return true;
            }
            else if (ParentTextBox.ShortCutKeys.StoreRedo == e.KeyData.KeyData)
            {
                this.TextOperator.StockOperator.StoreForward();
                return true;
                
            }
            else if (ParentTextBox.ShortCutKeys.StoreUndo == e.KeyData.KeyData)
            {
                this.TextOperator.StockOperator.StoreBack();
                return true;
            }
            else if (ParentTextBox.ShortCutKeys.AllSelect == e.KeyData.KeyData)
            {
                MouseOperator.ALLChoiceKey(TextOperator.Text);
                return true;
            }
                
            if (e.KeyData.IsAlt)
            {
                //Altキーは一時使用不可に
                
                var Cursor = e.KeyData.KeyData & ~Keys.Alt;
                if (Cursor == Keys.Right)
                {
                    this.TextOperator.AltSpace(1);
                    return true;
                }
                else if (Cursor == Keys.Left)
                {
                    this.TextOperator.AltSpace(-1);
                    return true;
                }
                 
            }
            else if (e.KeyData.IsShift)
            {
                //MouseOperator.InverseOperatorにバグの温床を感じる
                switch (e.KeyData.Direction)
                {
                    case CursorDirection.Down:
                        MouseOperator.InverseOperator.ShiftInverse(0, 1, e.CaretPosition, e.ExDotMemory);
                        return false;
                    case CursorDirection.Right:
                        MouseOperator.InverseOperator.ShiftInverse(1, 0, e.CaretPosition, e.ExDotMemory);
                        return false;
                    case CursorDirection.Up:
                        MouseOperator.InverseOperator.ShiftInverse(0, -1, e.CaretPosition, e.ExDotMemory);
                        return false;
                    case CursorDirection.Left:
                        MouseOperator.InverseOperator.ShiftInverse(-1, 0, e.CaretPosition, e.ExDotMemory);
                        return false;
                }
            }
            if (e.KeyData.KeyData == Keys.Back)
            {
                this.BackSpaceKey();
                return true;
            }
            else if (e.KeyData.KeyData == Keys.Delete)
            {
                this.DeleteKey();
                return true;
            }
                /*
            else if (e.KeyData.KeyData == this.ParentTextBox.ShortCutKeys.Delete.RawKey)
            {
                this.DeleteKey();
                return true;
            }
                */
            else
            {
            }
            return false;
        }
        public void AltOperator(int n,ExStringPoint CaretPosition)
        {


            //var grp = TextProcessor.AltOperator(n, TextOperator.Text, (StringPoint)CaretPosition);
            //TextOperator.SetText(grp.Key,grp.Value,TextChangeType.Other);
        }

        private void InverseLineDelete()
        {
            var tex = TextOperator.Text;
            var ep = IDL.GetFrontLocation(tex);
            var text = TextMergeProcessor.Delete(TextOperator.Text, IDL);
            //IDC.Clear();
            MouseOperator.Clear();
            TextOperator.ReplaceText(text, ep);
        }
        public void PasteKey()
        {
            if (!IDL.IsEmpty)
            {
                this.DeleteKey();
                this.ParentTextBox.TextOperator.StockOperator.RemoveStockCheck();
            }
            this.TextOperator.InsertText(Clipboard.GetText().CheckNewLine().DecodeToSjis());
        }
        public void TransParentKey()
        {
            if (!IDL.IsEmpty)
            {
                var bands = IDL.Item.BandIterator(this.TextOperator.Text).ToArray();
                
                this.TextOperator.Transparentize(bands);
                MouseOperator.Clear();
            }
        }
        public bool CanPaste()
        {
            string s = Clipboard.GetText();
            return s.Length > 0;
        }
        public void CopyKey(EncodeType type)
        {
            if (!IDL.IsEmpty)
            {
                Copy(type);
                MouseOperator.Clear();
            }
        }
        public bool CanSpaceReplace()
        {
            if (IDL.IsEmpty) return false;
            var b = TextMergeProcessor.CanSpaceReplace(this.TextOperator.Text, this.IDL);
            if (!b) return false;
            return true;
        }
        public void CutKey()
        {
            if (!IDL.IsEmpty)
            {
                Copy(this.ParentTextBox.TextSetting.CopyEncodeType);
                InverseLineDelete();
                MouseOperator.Clear();
            }
        }
        public void Copy(EncodeType type)
        {
            var tp = TextMergeProcessor.SimpleCopy(TextOperator.Text, IDL);
            //var tp = TextProcessor.SimpleCopy(TextOperator.Text, IDC);
            if (tp.Text.Length <= 0) return;
            if (type == EncodeType.UTF8)
            {
                Clipboard.SetText(tp.Text);
            }
            else if(type == EncodeType.SJIS10)
            {
                Clipboard.SetText(tp.Text.ToSjis10String());
            }
            else if (type == EncodeType.SJIS16)
            {
                Clipboard.SetText(tp.Text.ToSjis16String());
            }
        }
        public void DeleteKey()
        {
            if (!IDL.IsEmpty)
            {
                InverseLineDelete();
                
            }
            else
            {
                TextOperator.Delete();
            }
        }

        public void BackSpaceKey()
        {
            if (!IDL.IsEmpty)
            {
                InverseLineDelete();
                MouseOperator.Clear();
            }
            else
            {
                TextOperator.BackSpace();
            }
        }
        
    }
}
