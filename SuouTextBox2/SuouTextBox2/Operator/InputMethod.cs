﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using SuouBase;
using SuouTextBox2.Base;
using System.Diagnostics;
using System.Drawing;

namespace SuouTextBox2.Operator
{
    public class InputMethod
    {

        public event InputKeyEventHandler KeyInputed;
        public event InputCursorEventHandler CursorInputed;
        public event InputCursorEventHandler CursorSecondInputed;
        bool bWriteMode = true;
        TextBoxCaret Caret;
        SuouTextBox ParentControl { get; set; }
        

        public InputMethod(TextBoxCaret caret,SuouTextBox Ctrl)
        {
            this.Caret = caret;
            this.ParentControl = Ctrl;
            
        }

        public bool WndProc(ref Message m)
        {
            if (this.bWriteMode)
            {
                switch (m.Msg)
                {
                    case IMEApi.WM_IME_NOTIFY:
                        break;
                    case IMEApi.WM_IME_CHAR:
                        break;
                    case IMEApi.WM_IME_COMPOSITIONFULL:
                        break;
                    case IMEApi.WM_IME_CONTROL:
                        break;
                    case IMEApi.WM_IME_SELECT:
                        break;
                    case IMEApi.WM_CHAR:
                        if (!this.ParentControl.UserInputEnable) return false;
                        Caret.VoidImmGetContext();
                        char chr = Convert.ToChar(m.WParam.ToInt32() & 0xff);
                        if (Caret.ImmGetOpenStatus() == 0)
                        {
                            if (m.WParam.ToInt32() >= 32)
                            {
                                var e = new InputKeyEventArg(InputKeyType.Char, chr.ToString());
                                if (KeyInputed != null) KeyInputed(this, e);
                            }
                        }
                        if (m.WParam.ToInt32() == Lkey.Enter)//Enter
                        {
                            var e = new InputKeyEventArg(InputKeyType.Enter, null);
                            if (KeyInputed != null) KeyInputed(this, e);

                        }
                        Caret.ImmReleaseContext();
                        return true;

                    case IMEApi.WM_IME_COMPOSITION:
                        if (!this.ParentControl.UserInputEnable) return false;
                        if (((uint)m.LParam & (uint)IMEApi.GCS_RESULTREADSTR) != 0)
                        {
                            this.InputImm();
                            return true;
                        }
                        break;
                    case IMEApi.WM_IME_SETCONTEXT://初期化みたいなもの
                        if (!this.ParentControl.UserInputEnable) return false;
                        Caret.ImmSetContext();
                        return true;
                    case IMEApi.WM_KEYDOWN:
                        break;
                    case IMEApi.WM_IME_STARTCOMPOSITION:
                        Caret.ImmSetContext();
                        break;
                    default:
                        break;
                }
            }
            return false;


        }

        public void InputImm()
        {
            string IMEText = null;
            int intLength;
            Caret.VoidImmGetContext();
            byte[] buffer = new byte[1024];
            intLength = Caret.ImmGetCompositionString(buffer, 1024);
           if (intLength > 0)
            {
                IMEText = Encoding.GetEncoding("shift-jis").GetString(buffer);
                IMEText = IMEText.TrimEnd('\0');
                var e = new InputKeyEventArg(InputKeyType.Text, IMEText);
                if (KeyInputed != null) KeyInputed(this, e);

            }
           Caret.ImmReleaseContext();
        }
        
        public bool ProcessDialogKey(Keys keyData)
        {
            
            Keys k1 = keyData & (Keys.Shift|Keys.Alt|Keys.Control);
            CursorData cd = new CursorData();
            switch (k1 ^ keyData)
            {
                case Keys.Up:
                    cd.Direction = CursorDirection.Up;
                    break;
                case Keys.Left:
                    cd.Direction = CursorDirection.Left;
                    break;
                case Keys.Right:
                    cd.Direction = CursorDirection.Right;
                    break;
                case Keys.Down:
                    cd.Direction = CursorDirection.Down;
                    break;
                default:
                    cd.Direction = CursorDirection.Null;
                    break;
            }

            if (keyData == Keys.ProcessKey) return false;
            
            
            cd.KeyData = keyData;
            cd.IsAlt = k1.HasFlag(Keys.Alt);
            cd.IsShift = k1.HasFlag(Keys.Shift);
            cd.IsCtrl = k1.HasFlag(Keys.Control);

            var e = new InputCursorEventArg(cd, Caret.StringPos.Copy(), Caret.XDotMemory);
            if (CursorInputed != null)
            {
                var del = CursorInputed.GetInvocationList();
                List<bool> DelegateResult = new List<bool>();
                foreach (InputCursorEventHandler d in del)
                {
                    DelegateResult.Add(d(this, e));
                }
                var firstresult = DelegateResult.Exists((b) => (b == true));
                if (firstresult) return true;
            }
            if (CursorSecondInputed != null)
            {
                var del = CursorSecondInputed.GetInvocationList();
                List<bool> DelegateResult = new List<bool>();
                foreach (InputCursorEventHandler d in del)
                {
                    DelegateResult.Add(d(this, e));
                }
                var secondresult = DelegateResult.Exists((b) => (b == true));
                if (secondresult) return true;
            }
            return false;
        }

        public CursorDirection CursorKey(Keys keyData)
        {
            switch (keyData)
            {
                case Keys.Down:
                    return CursorDirection.Down;
                case Keys.Right:
                    return CursorDirection.Right;
                case Keys.Up:
                    return CursorDirection.Up;
                case Keys.Left:
                    return CursorDirection.Left;
            }
            return CursorDirection.Null;
        }
    }
    public static class Lkey
    {
        public const int Enter = 13;
        public const int BackSpace = 8;
    }
}
