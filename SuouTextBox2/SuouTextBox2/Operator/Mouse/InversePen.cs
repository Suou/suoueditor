﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouTextBox2.Base;
using System.Diagnostics;
using System.Windows.Forms;
using SuouBase.Text;
using System.Drawing;
namespace SuouTextBox2.Operator
{

    public abstract class InverseBasePen : BasePen
    {
        public InverseBasePen()
        {
        }
        public override void Set(SuouTextBox ctrl, InverseDataLapper idl, DisplayPointMaker dm)
        {
            //f (dm == null) throw new Exception();
            this.Ctrl = ctrl;
            this.PointMaker = dm;
            this.InverseMiniOperator = new Inverser(new PrimitiveChoice(), dm, idl);
            this.InverseMiniOperator.Choice = this.ICB;
            
            this.IsMoveCursor = true;
        }
        public override Cursor Cursor
        {
            get
            {
                return this.ICB.Image;
            }
        }
        private Inverser _InverseMiniOperator;
        public Inverser InverseMiniOperator
        {
            get { return this._InverseMiniOperator; }
            set
            {
                if (value == null) return;
                this._InverseMiniOperator = value;
                this._InverseMiniOperator.Choice = this.ICB;
            }
        }

        private InverseChoiceBase _ICB;
        public InverseChoiceBase ICB
        {
            get { return this._ICB; }
            set
            {
                this._ICB = value;

                if (this.InverseMiniOperator != null)
                {
                    this.InverseMiniOperator.Choice = this._ICB;
                    this.InverseMiniOperator.Clear();
                }
                
                //this.Ctrl.Cursor = this._ICB.Image;
                //this.IsUniqueCursor = true;
            }
        }
        public InverseDataCollection InverseDataItems
        {
            get
            {
                return this.InverseMiniOperator.Items;
            }
        }
        public override InverseDataLapper InverseData
        {
            get { return this.InverseMiniOperator.ActualItem; }
            /*
            private set { 
                if(this.InverseMiniOperator != null)this.InverseMiniOperator.ActualItem = value;
            }
            */
        }
        protected Rectangle NoStartRectangle { get; set; }
        protected ExDisplayPoint StartPoint { get; set; }
        public void ClearInverseLine()
        {
            if(this.InverseMiniOperator != null)this.InverseMiniOperator.Clear();
        }
        public void ShiftInverse(int x, int y, ExStringPoint CaretPoint, int xdotmemory)
        {
            var p = CaretPoint.Copy();
            var q = p.Copy();
            q += x;
            if (y != 0) q = TextBoxCaret.InclimentYCaret(q, y, xdotmemory);
            this.InverseMiniOperator.ShiftCreateDotLines(p.ToEditPoint(), q.ToEditPoint());
        }
        public void KeyInputed(object sender, InputKeyEventArg e)
        {
            this.InverseMiniOperator.ContinuityCheck();
        }
        public override IPenMouse Copy()
        {
            return null;
        }
        protected DisplayPointMaker PointMaker;

        public override void DoMouseUp(SuouMouseEventArg e)
        {
            if (e.MouseEventArgs.Button == this.ClickButton)
            {
                var b = this.InverseMiniOperator.ChoiceEnd(e.ExLocation);
                if (this.InverseMiniOperator.Items.DotBandCount > 0 && b) this.IsTurnUsed = true;
                DoMouseUp2(e);
            }
        }
        public override void DoMouseDown(SuouMouseEventArg e)
        {
            this.IsTurnUsed = false;

            if (e.MouseEventArgs.Button == this.ClickButton)
            {
                this.DoMouseDown2(e);
            }
        }
        protected abstract void DoMouseDown2(SuouMouseEventArg e);
        protected abstract void DoMouseMove2(SuouMouseEventArg e);
        protected virtual void DoMouseUp2(SuouMouseEventArg e)
        {
        }
        public override void DoMouseMove(SuouMouseEventArg e)
        {
            if (e.MouseEventArgs.Button == this.ClickButton)
            {
                this.DoMouseMove2(e);
            }
        }
        
    }
    public class InverseRightPen : InverseBasePen
    {
        public InverseRightPen():base()
        {

        }
        protected override void DoMouseDown2(SuouMouseEventArg e)
        {
            this.NoStartRectangle = SuouBase.Base.SuouUsefulMethod.CreateNoDragRectangle(e.ExLocation.ToDotPoint().Position);
            this.StartPoint = e.ExLocation;
        }
        protected override void DoMouseMove2(SuouMouseEventArg e)
        {
            if (!this.InverseMiniOperator.ChoiceStartFrag && !this.NoStartRectangle.Contains(e.ExLocation.ToDotPoint().Position))
            {
                this.InverseMiniOperator.ChoiceStart(this.StartPoint);
            }
            else if (this.InverseMiniOperator.ChoiceStartFrag)
            {
                this.InverseMiniOperator.ChoiceMove(e.ExLocation);

            }
            this.StartPoint = e.ExLocation;
        }
        
    }
    /// <summary>
    /// ドラッグでテキストを反転させるモード
    /// </summary>
    public class InversePen:InverseBasePen
    {
        
        
        //public event InverseMethodEventHandler InverseChanged;
        
        public InversePen():base()
        {

            this.ICB = (new PrimitiveChoice());
            this.Cursor = this.ICB.Image;
        }
        public InversePen(bool isUniqueCursor)
            : base()
        {
        }
        
        

        #region MouseEventMethod

        protected override void DoMouseDown2(SuouMouseEventArg e)
        {
            this.InverseMiniOperator.ChoiceStart(e.ExLocation);
        }
        protected override void DoMouseMove2(SuouMouseEventArg e)
        {
            this.InverseMiniOperator.ChoiceMove(e.ExLocation);
        }

        #endregion
        
    }
}
