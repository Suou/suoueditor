﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouTextBox2.Operator;
using SuouTextBox2.Base;
using SuouBase.Text;
using System.Windows.Forms;

namespace SuouTextBox2.Operator
{
    public interface IPenMouse
    {
        Cursor Cursor { get; }
        
        void DoMouseDown(SuouMouseEventArg e);
        void DoMouseMove(SuouMouseEventArg e);
        void DoMouseUp(SuouMouseEventArg e);
        
        bool IsMoveCursor { get; }
        bool IsTurnUsed { get; }
        InverseDataLapper InverseData { get; }
        void Set(SuouTextBox ctrl,InverseDataLapper idl, DisplayPointMaker dm);
        MouseButtons ClickButton { get; set; }

        IPenMouse Copy();
    }
    public abstract class BasePen:IPenMouse
    {
        public virtual Cursor Cursor { get; protected set; }
        public SuouTextBox Ctrl { get; set; }
        public virtual void Set(SuouTextBox ctrl, InverseDataLapper idl, DisplayPointMaker dm)
        {
            this.Ctrl = ctrl;
            this.InverseData = idl;
        }
        public virtual void DoMouseDown(SuouMouseEventArg e)
        {
        }
        public virtual void DoMouseMove(SuouMouseEventArg e)
        {
        }
        public virtual void DoMouseUp(SuouMouseEventArg e)
        {
        }

        public bool IsMoveCursor { get; protected set; }
        public bool IsTurnUsed { get; protected set; }
        public virtual InverseDataLapper InverseData { get; set; }

        public MouseButtons ClickButton { get; set; }

        public abstract IPenMouse Copy();
        
    }

    public class TankMouse:InversePen
    {
        private Cursor _Cursor;
        public override Cursor Cursor
        {
            get
            {
                return this._Cursor;
            }
            protected set
            {
                this._Cursor = value;
            }
        }   
        public bool IsTransparent { get; set; }
        public SimpleSpaceMaker InsertSpaceMaker { get; set; }
        private System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
        
        TextMethod TextOperator
        {
            get { return this.Ctrl.TextOperator; }
        }
        ISpaceMaker SpaceMaker
        {
            get { return this.Ctrl.SpaceMaker; }
        }
        public TankMouse(bool Istransparent, string word):this()
        {
            var w = new SuouWord(word);
            var nword = w.Width == 0 ? "?" : word;
            this.IsTransparent = Istransparent;
            this.InsertSpaceMaker = new SimpleSpaceMaker(nword);
            
        }
        public TankMouse()
            : base()
        {
            string str = asm.GetName().Name+".Resources" + ".tank.cur";
            var f = asm.GetManifestResourceStream(str);
            if (f == null)
            {
                throw new Exception();
            }
            this.Cursor = new Cursor(f);
        }
        public TankMouse(TankMouse tm)
            : this()
        {
            this.IsTransparent = tm.IsTransparent;
            this.InsertSpaceMaker = tm.InsertSpaceMaker;
        }
        private void PaintLine(SuouMouseEventArg e)
        {

            if (e.ChosenLine != null)
            {
                if (e.ChosenLine.CanSpaceDotSplit(e.EditLocation.X))
                {
                    if (this.IsTransparent)
                    {
                        var afterline = e.ChosenLine.PaintTransparent(e.EditLocation.X, this.SpaceMaker);
                        e.ChosenLineAfter = afterline;
                    }
                    else
                    {
                        var v = e.ChosenLine.SpaceDotSplit(e.EditLocation.X, this.SpaceMaker);
                        if (v.Center.Width != 0)
                        {
                            v.Center = this.InsertSpaceMaker.CreateSpaceCarelessly(v.Center.Width);
                            e.ChosenLineAfter = v.Join();
                        }

                    }
                }
            }
        }
        private void PaintInverseArea(SuouMouseEventArg e)
        {
            var newtext = AltText.Create(this.SpaceMaker).ReplaceArea(this.TextOperator.Text, this.InverseDataItems, this.InsertSpaceMaker.Item.Word);
            this.TextOperator.ReplaceText(newtext);
        }
        protected override void DoMouseUp2(SuouMouseEventArg e)
        {
            
            if (this.InverseDataItems.IsEmpty)
            {
                this.PaintLine(e);
            }
            else
            {
                this.PaintInverseArea(e);
            }
            
            
        }
        public override IPenMouse Copy()
        {
            return new TankMouse(this);
        }
    }
    public class MonoMouse : BasePen
    {
        private AltText TextMergeProcessor
        {
            get { return this.Ctrl.TextMergeProcessor; }
        }
        private TextMethod TextOperator
        {
            get { return this.Ctrl.TextOperator; }
        }
        public MonoMouse()
        {
            this.ChoiceLength = 20;

            string str = asm.GetName().Name + ".Resources" + ".kesigomucur.cur";
            var f = asm.GetManifestResourceStream(str);
            if (f == null)
            {
                throw new Exception();
            }
            this.Cursor = new Cursor(f);
        }
        public MonoMouse(MonoMouse mm):this()
        {
            this.ChoiceLength = mm.ChoiceLength;
        }
        public int ChoiceLength { get; set; }
        private System.Reflection.Assembly asm = System.Reflection.Assembly.GetExecutingAssembly();
        
        public override void DoMouseDown(SuouMouseEventArg e)
        {
            if (e.MouseEventArgs.Button == MouseButtons.Left)
            {
                this.InverseLineDelete();
            }
        }
        public override void DoMouseMove(SuouMouseEventArg e)
        {
            var b = this.InverseData.CreateNewLine(e.EditLocation, this.ChoiceLength);
            if (b && e.MouseEventArgs.Button == MouseButtons.Left) this.InverseLineDelete();
            
        }
        public SuouLine Replace(Band d,SuouText st)
        {
            var MotoLine = st[d.Y].Clone();
            var width = MotoLine.Subline(d.StartX, d.Length).Width;
            var dotIndex = MotoLine.Subline(0, d.StartX).Width;
            MotoLine = MotoLine.Delete(d.StartX,d.Length);
            if (MotoLine.CanSpaceDotSplit(dotIndex))
            {
                var ThreeLine = MotoLine.SpaceDotSplit(dotIndex, this.TextMergeProcessor.SpaceMaker);
                var newSpaceWidth = ThreeLine.Center.Width + width;
                ThreeLine.Center = this.TextMergeProcessor.SpaceMaker.CreateSpaceCarelessly(newSpaceWidth);
                return ThreeLine.Join();
            }
            else
            {
                return st[d.Y];
            }
        }
        public void InverseLineDelete()
        {
            var tex = TextOperator.Text;
            var iter = this.InverseData.Item.NormalBandIterator(tex);
            if (iter.Count() != 0)
            {
                var d = iter.First();
                var line = this.Replace(d, tex);
                TextOperator.ReplaceText(d.Y, line);
            }
            
        }
        public override IPenMouse Copy()
        {
            return new MonoMouse(this);
        }
    }
    
}
