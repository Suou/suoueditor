﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase;
namespace SuouTextBox2.Operator
{
    /// <summary>
    /// マウスの右クリック左クリックに持たせるテキスト変形モードを扱うクラス
    /// </summary>
    public class MouseInputMethod
    {
        #region Property
        DisplayPointMaker PointMaker;

        private SuouTextBox Ctrl { get; set; }
        TextMethod TextOperator
        {
            get { return this.Ctrl.TextOperator; }
        }

        public event SuouMouseEventHandler MouseDown;
        public event SuouMouseEventHandler MouseMove;
        public event SuouMouseEventHandler MouseUp;
        public InversePen InverseOperator { get; private set; }

        public InverseDataLapper SuouInverseItem = new InverseDataLapper();
        public event InverseMethodEventHandler InverseChanged;
        private IPenMouse _LeftPenItem = null;
        public IPenMouse LeftPenItem
        {
            get { return this._LeftPenItem; }
            set
            {
                if (this._LeftPenItem != null)
                {
                    this.SuouInverseItem.Clear();
                }
                if (value == null)
                {
                    this._LeftPenItem = this.InverseOperator;
                }
                else
                {
                    this._LeftPenItem = value;
                }

                this.LeftPenItem.Set(this.Ctrl,this.SuouInverseItem, this.PointMaker);
                this.LeftPenItem.ClickButton = MouseButtons.Left;
                if (this.Ctrl != null)
                {
                    this.Ctrl.Cursor = this._LeftPenItem.Cursor;
                    this.Ctrl.RefleshDraw();
                    this.Ctrl.IsVisibleCaret = this.LeftPenItem.IsMoveCursor;
                }
            }
        }

        private IPenMouse _RightPenItem = null;
        public IPenMouse RightPenItem
        {
            get { return this._RightPenItem; }
            set
            {
                if (this._RightPenItem != null)
                {
                    this.SuouInverseItem.Clear();
                }
                if (value == null)
                {
                    this._RightPenItem = this.InverseOperator;
                }
                else
                {
                    this._RightPenItem = value;
                }
                this.RightPenItem.Set(this.Ctrl,this.SuouInverseItem, this.PointMaker);
                this.RightPenItem.ClickButton = MouseButtons.Right;
                if (this.Ctrl != null)
                {
                    this.Ctrl.Cursor = this._RightPenItem.Cursor;
                    this.Ctrl.RefleshDraw();
                    this.Ctrl.IsVisibleCaret = this.RightPenItem.IsMoveCursor;
                }
            }
        }
        private IPenMouse SelectPen(MouseEventArgs e)
        {
            if (e.Button == this.LeftPenItem.ClickButton)
            {
                return this.LeftPenItem;
            }
            else
            {
                return this.RightPenItem;
            }
        }
        #endregion
        #region Constructor
        public MouseInputMethod(SuouTextBox ctrl,DisplayPointMaker pointMaker)
        {
            this.Ctrl = ctrl;
            this.PointMaker = pointMaker;
            this.SuouInverseItem.InverseChanged += this.InverseItem_Changed;
            this.InverseOperator = new InversePen();
            this.RightPenItem = new InverseRightPen() { ICB = new GataRectangleChoice(false) };
            
            
            this.LeftPenItem = this.InverseOperator;
            
            
        }
        #endregion
        #region Method
        public void Clear()
        {
            this.SuouInverseItem.Clear();
            this.InverseOperator.ClearInverseLine();
        }

        public void ALLChoiceKey(TextComposition tc)
        {
            var v = this.SuouInverseItem.Item.Copy();
            v.AllChoice(tc);
            this.SuouInverseItem.Item = v;
        }
        #endregion
        #region MouseEventMethod
        private void InverseItem_Changed(object sender, InverseMethodEventArg e)
        {
            if (InverseChanged != null) this.InverseChanged(sender, e);
        }
        public SuouMouseEventArg CreateMouseEventArgs(MouseEventArgs e)
        {
            var p = PointMaker.CreateExDisplayPoint(e.Location);
            var ep = ((ExEditPoint)p).ToEditPoint();
            var text = this.TextOperator.Text;
            SuouLine line = null;
            if (text.ContainDotLocation(ep))
            {
                line = text[ep.Y];
            }
            var se = new SuouMouseEventArg(e, p, line);
            return se;
        }
        public void Control_MouseDown(MouseEventArgs e)
        {
            this.Ctrl.Cursor = this.SelectPen(e).Cursor;
            var se = this.CreateMouseEventArgs(e);
            this.LeftPenItem.DoMouseDown(se);
            this.RightPenItem.DoMouseDown(se);
            if (MouseDown != null) MouseDown(this, se);

            if (se.ChosenLineAfter != null)
            {
                this.TextOperator.ReplaceText(se.EditLocation.Y,se.ChosenLineAfter);
            }
        }
        public void Control_MouseMove(MouseEventArgs e)
        {
            var se = this.CreateMouseEventArgs(e);
            this.LeftPenItem.DoMouseMove(se);
            this.RightPenItem.DoMouseMove(se);
            if (MouseMove != null) MouseMove(this, se);
        }
        public void Control_MouseUp(MouseEventArgs e)
        {
            var se = this.CreateMouseEventArgs(e);
            this.LeftPenItem.DoMouseUp(se);
            this.RightPenItem.DoMouseUp(se);
            if (!this.RightPenItem.IsTurnUsed)
            {
                if (MouseUp != null) MouseUp(this, se);
                if (e.Button == MouseButtons.Right) this.Ctrl.suouTextBoxContextMenuStrip1.Show(this.Ctrl, e.Location);
            }
            else
            {
                
            }
            if (se.ChosenLineAfter != null)
            {
                this.TextOperator.ReplaceText(se.EditLocation.Y, se.ChosenLineAfter);
            }
            this.Ctrl.Cursor = this.LeftPenItem.Cursor;
        }
        #endregion
    }
}
