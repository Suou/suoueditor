﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using SuouBase;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using System.Diagnostics;
using SuouTextBox2.Base;
using SuouBase.Text;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Drawing;
namespace SuouTextBox2.Operator
{
    public class TextMethod
    {
        #region Property
        public event TextChangeEventHandler TextChanged;
        private SuouTextBox ParentTextBox { get; set; }
        private TextComposition _Text;
        public TextComposition Text 
        {
            get { return this._Text; }
            private set { this._Text = value; }
        }

        private TextBoxCaret Caret;
        private PrintFontSetting PrintFontSet;
        private DisplayPointMaker PointMaker { get; set; }
        private AltText AltTexer
        {
            get { return this.ParentTextBox.TextMergeProcessor; }
        }
        private InverseDataCollection IDC { get { return InverseOperator.InverseDataItems; } }
        public CutPasteMethod CutPasteOperator { get; set; }
        public StockMethod StockOperator { get; private set; }
        public InversePen InverseOperator { get; set; }
        #endregion
        #region Constructor
        public TextMethod(SuouTextBox Ctrl,TextComposition tc,TextBoxCaret caret,DisplayPointMaker PM,PrintFontSetting PFS)
        {
            this.ParentTextBox = Ctrl;
            this.Text = tc;
            this.Caret = caret;
            this.PointMaker = PM;
            this.PrintFontSet = PFS;
            this.StockOperator = new StockMethod(this, false);
            this.TextChanged += this.StockOperator.TextOperator_TextChanged;
            this.ParentTextBox.TurnEnded += this.StockOperator.SuouEditor_TurnEnded;
            this.ParentTextBox.InputOperator.CursorInputed += this.StockOperator.KeyInputMethod_Inputed;
        }
        #endregion
        #region KeyOperation
        public void AltSpace(int n)
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            var beforepos = (StringPoint)Caret.StringPos;
            var NearestN = this.AltTexer.NearestCanIncrementSpaceLine(this.Text[beforepos.Y],beforepos.X,n);
            var NewLine = this.AltTexer.IncrementSpaceLine(this.Text[beforepos.Y], beforepos.X, NearestN);
            var NewX = n >= 0 ? NewLine.Left.Length + NewLine.Center.Length:NewLine.Left.Length;

            var q = this.Text.Replace(beforepos.Y, NewLine.Join());
            q.X = NewX;
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Other, q, beforeCaretLocation, null, true);
            if (TextChanged != null) TextChanged(this, e);
        }

        public void BackSpace()
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            StringPoint CaretLastLocation = (StringPoint)Caret.StringPos;

            if (Caret.StringPos.X == 0)
            {
                if (Caret.StringPos.Y != 0)
                {

                    CaretLastLocation = new StringPoint(this.Text[Caret.StringPos.Y - 1].Length, Caret.StringPos.Y - 1);
                    Text.DeleteParagraph(Caret.StringPos.Y);
                }
                else
                {
                }
            }
            else
            {
                CaretLastLocation = new StringPoint(Caret.StringPos.X - 1, Caret.StringPos.Y);
                this.Text.Delete((StringPoint)Caret.StringPos, -1);
                
            }

            TextChangeEventArg e = new TextChangeEventArg(this.Text,TextChangeType.Other,CaretLastLocation,beforeCaretLocation,null,true);
            if (TextChanged != null) TextChanged(this, e);
        }
        public void Delete()
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            StringPoint CaretLastLocation = (StringPoint)Caret.StringPos;
            //bool IsParagraphChange = false;
            if (Caret.StringPos.IsEndPoint)
            {
            }
            else if (Caret.StringPos.IsRightMost)
            {
                this.Text.DeleteParagraph(Caret.StringPos.Y + 1);
                //IsParagraphChange = true;
            }
            else
            {
                this.Text.Delete((StringPoint)Caret.StringPos, 1);
            }
            
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Other,CaretLastLocation,beforeCaretLocation,null,true);
            if (TextChanged != null) TextChanged(this, e);
        }
        #endregion
        #region TextOperation
        public void InsertText(string s)
        {
            var ls = BSystem.Text.StringExtensions.LineSprit(s);
            var r = new SuouText(s);
            this.InsertText(r, (StringPoint)Caret.StringPos);
            this.ParentTextBox.TurnEnd();
        }
        public void Transparentize(Band[] Bands)
        {
            Bands.ForEach((v) => this.Text[v.Y] = this.Text[v.Y].PaintTransparent(v.StartX, v.EndX));
            
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Other, (StringPoint)this.Caret.StringPos, (StringPoint)this.Caret.StringPos, null, false);
            if (TextChanged != null) TextChanged(this, e);
            this.ParentTextBox.TurnEnd();
        }

        public void InsertText(SuouText tc, StringPoint sp)
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            StringPoint p;
            if (tc.Count == 1)
            {
                p = this.Text.Insert(sp, tc[0]);
            }
            else
            {
                p = this.Text.Insert(sp, tc);
            }
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Other, p,beforeCaretLocation, null, true);
            if (TextChanged != null) TextChanged(this, e);
        }
        public void AddText(string s)
        {
            
            var text = this.Text.Text + System.Environment.NewLine + s;
            DeleteAll();
            this.SetText(text);
        }
        
        public void DeleteAll()
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            Text.DeleteALL();
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Other,new StringPoint(0,0),beforeCaretLocation,null,true);
            if (TextChanged != null) TextChanged(this, e);
        }
        public void ReplaceText(SuouText tc,EditPoint ep)
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            this.Text.Initialize(tc);
            var q = (StringPoint)PointMaker.CreateExEditPoint(ep.X,ep.Y).ToStringPoint();
            
            
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Other,q,beforeCaretLocation,null,true);
            if (TextChanged != null) TextChanged(this, e);
        }
        public void ReplaceText(SuouText tc)
        {
            /*
            var oldText = this._Text;
            var b = false;
            if(oldText.Lines.Count == tc.Lines.Count)
            {
                for(var i = 0; i < tc.Lines.Count; i++)
                {
                    if(oldText)
                }
            }
            */
            this.ReplaceText(tc, new EditPoint(0, 0));
        }
        

        public void ReplaceText(int y,SuouLine line)
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            var p = this.Text.Replace(y,line);
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Other, p, beforeCaretLocation, null, true);
            if (TextChanged != null) TextChanged(this, e);
        }

        public void SetText(SuouText tc,StringPoint AfterCaretLocation,TextChangeType type)
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            Text.Initialize(tc);
            TextChangeEventArg e = new TextChangeEventArg(this.Text, type, AfterCaretLocation,beforeCaretLocation, null, true);
            if (TextChanged != null) TextChanged(this, e);
        }
        public void SetText(string s)
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            this.Text.Initialize(s);
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Initialize, new StringPoint(0, 0),beforeCaretLocation, null, true);
            if (TextChanged != null) TextChanged(this, e);

        }
        public void SetText(SuouText Text)
        {
            var beforeCaretLocation = (StringPoint)Caret.StringPos;
            this.Text.Initialize(Text);
            var f = this.Text.Clone();
            TextChangeEventArg e = new TextChangeEventArg(this.Text, TextChangeType.Initialize, new StringPoint(0, 0),beforeCaretLocation, null, true);
            if (TextChanged != null) TextChanged(this, e);

        }
        #endregion
        #region Event
        public void KeyInputed(object sender,InputKeyEventArg e)
        {
            switch (e.Type)
            {
                case InputKeyType.Char:
                case InputKeyType.Text:
                    if (!IDC.IsEmpty)
                    {
                        CutPasteOperator.DeleteKey();
                        StockOperator.RemoveStockCheck();
                    }
                    this.InsertText(e.Text);
                    break;
                case InputKeyType.Enter:
                    if (!IDC.IsEmpty)
                    {
                        CutPasteOperator.DeleteKey();
                        StockOperator.RemoveStockCheck();
                    }
                    this.InsertText(System.Environment.NewLine);
                    break;
            }
        }
        #endregion
    }
}
