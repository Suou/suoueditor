﻿namespace SuouTextBox2
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SuouTextBox2.Operator.InversePen inversePen1 = new SuouTextBox2.Operator.InversePen();
            SuouTextBox2.Base.PrimitiveChoice primitiveChoice1 = new SuouTextBox2.Base.PrimitiveChoice();
            SuouBase.Text.InverseDataLapper inverseDataLapper1 = new SuouBase.Text.InverseDataLapper();
            SuouBase.Text.InverseDataCollection inverseDataCollection1 = new SuouBase.Text.InverseDataCollection();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.suouTextBox1 = new SuouTextBox2.SuouTextBox();
            this.vRuler1 = new SuouTextBox2.Controls.Ruler.VRuler();
            this.SuspendLayout();
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.ItemHeight = 12;
            this.listBox1.Location = new System.Drawing.Point(561, 24);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(216, 196);
            this.listBox1.TabIndex = 1;
            this.listBox1.SelectedIndexChanged += new System.EventHandler(this.listBox1_SelectedIndexChanged);
            // 
            // suouTextBox1
            // 
            this.suouTextBox1.AllowDrop = true;
            this.suouTextBox1.CanOriginalDrag = false;
            this.suouTextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.suouTextBox1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F);
            this.suouTextBox1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.suouTextBox1.IsFixedScroll = false;
            this.suouTextBox1.IsInnerScrollBar = true;
            this.suouTextBox1.IsRuler = false;
            this.suouTextBox1.IsVisibleCaret = true;
            this.suouTextBox1.Location = new System.Drawing.Point(44, 1);
            this.suouTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.suouTextBox1.Name = "suouTextBox1";
            this.suouTextBox1.PaddingSize = new System.Drawing.Size(5, 25);
            inversePen1.ClickButton = System.Windows.Forms.MouseButtons.Left;
            inversePen1.Ctrl = this.suouTextBox1;
            primitiveChoice1.IsContinuity = false;
            inversePen1.ICB = primitiveChoice1;
            inverseDataLapper1.Item = inverseDataCollection1;
            inversePen1.InverseData = inverseDataLapper1;
            this.suouTextBox1.Pen = inversePen1;
            this.suouTextBox1.Size = new System.Drawing.Size(471, 402);
            this.suouTextBox1.TabIndex = 0;
            this.suouTextBox1.UseDebug = false;
            this.suouTextBox1.UserInputEnable = true;
            // 
            // vRuler1
            // 
            this.vRuler1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F);
            this.vRuler1.Location = new System.Drawing.Point(2, 1);
            this.vRuler1.Name = "vRuler1";
            this.vRuler1.ParentTextBox = this.suouTextBox1;
            this.vRuler1.Pos = 0;
            this.vRuler1.Size = new System.Drawing.Size(35, 402);
            this.vRuler1.TabIndex = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 556);
            this.Controls.Add(this.suouTextBox1);
            this.Controls.Add(this.vRuler1);
            this.Controls.Add(this.listBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private SuouTextBox suouTextBox1;
        private System.Windows.Forms.ListBox listBox1;
        private Controls.Ruler.VRuler vRuler1;



    }
}

