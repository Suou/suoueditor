﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Operator;
using SuouTextBox2.Base;
using SuouTextBox2.Controls;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase;
using SuouTextBox2.Debug;
using SuouBase.Drawing;
using BSystem;
using WebExtension;

namespace SuouTextBox2
{
    public partial class SuouTextBox
    {
        #region OperatorProperty
        public MouseInputMethod MouseInputOperator;

        [Browsable(false)]
        [ReadOnly(true)]
        public InputMethod InputOperator;

        [Browsable(false)]
        [ReadOnly(true)]
        public DrawMethod DrawOperator;
        //private CommandMethod CommandOperator;

        [Browsable(false)]
        [ReadOnly(true)]
        public TextMethod TextOperator { get; private set; }
        private InversePen InverseOperator
        {
            get
            {
                return MouseInputOperator.InverseOperator;
            }
        }
        private CutPasteMethod CutPasteOperator;
        //public StockMethod StockOperator { get; private set; }
        #endregion
        #region Property

        public bool IsFixedScroll
        {
            get
            {
                if (this.DisplaySet != null)
                {
                    return this.DisplaySet.IsFixedScroll;
                }
                else
                {
                    return false;
                }
            }
            set
            {
                if (this.DisplaySet != null)
                {
                    this.DisplaySet.IsFixedScroll = value;
                }
            }
        }
        public Size PaddingSize
        {
            get
            {
                if (this.DisplaySet != null)
                {
                    return new Size(DisplaySet.DefaultPaddingLeft, DisplaySet.DefaultPaddingTop);
                }
                else
                {
                    return new Size(20, 10);
                }
            }
            set
            {
                if (this.DisplaySet != null)
                {
                    this.DisplaySet.DefaultPaddingTop = value.Height;
                    this.DisplaySet.DefaultPaddingLeft = value.Width;
                }
            }
        }
        private bool _UserInputEnable = true;
        public bool UserInputEnable
        {
            get { return _UserInputEnable; }
            set
            {
                _UserInputEnable = value;
                CanHaveCaret();
            }
        }
        public event EventHandler ContextOpening;
        private SuouTextBoxSetting _TextSetting = new SuouTextBoxSetting();

        [Browsable(false)]
        [ReadOnly(true)]
        public SuouTextBoxSetting TextSetting
        {
            get { return this._TextSetting; }
            /*
            set
            {
                if (value != null)
                {
                    this._TextSetting = value;
                }
            }
            */
        }
        public void SetTextSetting(SuouTextBoxSetting value)
        {
            if (value != null)
            {
                this._TextSetting = value;
            }
        }
        private TextComposition Texter;

        [Browsable(false)]
        [ReadOnly(true)]
        public TextBoxCaret Caret { get; private set; }
        public bool UseDebug
        {
            get { return this.Debugger.Visible; }
            set { this.Debugger.Visible = value; }
        }
        private StockWidnow Debugger = new StockWidnow();
        private Base.TextBoxShortCutKeys _ShortCutKeys = new TextBoxShortCutKeys();
        
        [Browsable(false)]
        [ReadOnly(true)]
        public Base.TextBoxShortCutKeys ShortCutKeys
        {
            get
            {
                if (_ShortCutKeys == null)
                {
                    throw new MyException("");
                }
                return _ShortCutKeys;
            }
        }
        
        
        public void SetShortCutKeys(Base.TextBoxShortCutKeys item)
        {
            
            if (item != null)
            {
                this._ShortCutKeys = item;
                /*
                this.cutToolStripMenuItem.ShortcutKeys = this.ShortCutKeys.Cut.KeyBoardKey;
                this.copyToolStripMenuItem.ShortcutKeys = this.ShortCutKeys.Copy.KeyBoardKey;
                this.pasteToolStripMenuItem.ShortcutKeys = this.ShortCutKeys.Paste.KeyBoardKey;
                this.allInverseChoiceToolStripMenuItem.ShortcutKeys = this.ShortCutKeys.AllSelect.KeyBoardKey;
                this.deleteToolStripMenuItem.ShortcutKeys = this.ShortCutKeys.Delete.KeyBoardKey;
                this.redoToolStripMenuItem.ShortcutKeys = this.ShortCutKeys.StoreRedo.KeyBoardKey;
                this.undoToolStripMenuItem.ShortcutKeys = this.ShortCutKeys.StoreUndo.KeyBoardKey;
                */
                
            }
            
        }
        
        [Browsable(false)]
        [ReadOnly(true)]
        public AltText TextMergeProcessor { get; private set; }
        private SpaceMaker _SpaceMaker = null;
        [Browsable(false)]
        [ReadOnly(true)]
        public SpaceMaker SpaceMaker
        {
            get
            {
                if (this._SpaceMaker == null) this._SpaceMaker = SpaceMaker.Default();
                return this._SpaceMaker;
            }
            //set { this._SpaceMaker = value; }
        }
        public SpaceLineSet GetSpaceSet()
        {
            return this.SpaceSet;
        }
        public void SetSpaceMaker(SpaceMaker sm)
        {
            this._SpaceMaker = sm;
        }
        public void SetSpaceSet(SpaceLineSet value)
        {
            if (value == null) return;
            this._SpaceMaker = new SpaceMaker(value, this.PrintFontSet.Font, this.PrintFontSet.TextColor);
            this.TextMergeProcessor = AltText.Create(value, this.PrintFontSet.Font, this.PrintFontSet.TextColor);
        }
        public void StockCheck()
        {
            if (this.TextOperator.StockOperator.IsStockDone)
            {
                this.TurnEnd();
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public SpaceLineSet SpaceSet
        {
            get
            {
                return this.SpaceMaker.Seting;
            }
            /*
            set
            {
                if (value != null)
                {
                    this.SpaceMaker = new SpaceMaker(value, this.PrintFontSet.Font, this.PrintFontSet.TextColor);
                    this.TextMergeProcessor = AltText.Create(value, this.PrintFontSet.Font, this.PrintFontSet.TextColor);
                }
            }
            */
        }

        private DisplayPointMaker _PointMaker;
        [Browsable(false)]
        [ReadOnly(true)]
        public DisplayPointMaker PointMaker
        {
            get
            {
                return _PointMaker;
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public DisplaySetting DisplaySet { get; private set; }
        [Browsable(false)]
        [ReadOnly(true)]
        public PrintFontSetting PrintFontSet = new PrintFontSetting();//後でprivateにすること
        public bool CanOriginalDrag { get; set; }

        public void AddContextMenuStrip(ContextMenuStrip a)
        {
            MenuStripMethod.CopyStripList(this.suouTextBoxContextMenuStrip1.Items, a.Items);
        }
        public bool _IsInnerScrollBar = true;
        public bool IsInnerScrollBar
        {
            get
            {
                return _IsInnerScrollBar;
            }
            set
            {
                if (value)
                {
                    vScrollBar1.Visible = true;
                    hScrollBar1.Visible = true;
                    DisplaySet.SetScrollVar(vScrollBar1, hScrollBar1);
                    _IsInnerScrollBar = true;
                }
                else
                {
                    vScrollBar1.Visible = false;
                    hScrollBar1.Visible = false;
                    DisplaySet.SetScrollVar(vScrollBar1, hScrollBar1);
                    _IsInnerScrollBar = false;
                }
            }
        }
        public bool IsRuler
        {
            get
            {
                return DisplaySet.IsRuler;
            }
            set
            {
                DisplaySet.IsRuler = value;
                if (DrawOperator != null) DrawOperator.IsALLDraw = true;
                TurnEnd();
            }
        }
        [Browsable(false)]
        [ReadOnly(true)]
        public SuouText RawText
        {
            get
            {
                if (this.Texter != null)
                {
                    return this.Texter.ToSuouText();
                }
                else
                {
                    return null;
                }
            }
        }
        public void SetRawText(SuouText text)
        {
            if (text != null)
            {
                if (this.TextOperator.StockOperator.IsStockDone)
                {
                    TurnEnd();
                    this.SetRawText(text);
                }
                else
                {
                    this.Initialize(text);
                    //this.TextOperator.SetText(value);
                    TurnEnd();
                }
            }
            else
            {
            }
        }
        public new string Text
        {
            get
            {
                return this.TextOperator.Text.Text;
            }
            set
            {
                if (value != null)
                {
                    this.Initialize(value);
                }
                TurnEnd();
            }
        }
        #endregion
    }
}
