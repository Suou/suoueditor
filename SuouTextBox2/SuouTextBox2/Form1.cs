﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase;
using BSystem;
namespace SuouTextBox2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            var f = SuouBase.TimeDebug.StopWatchWindow;
            f.Show();

            this.listBox1.DisplayMember = "Name";

            this.listBox1.Items.Add(SpaceLineSet.DefaultSet);
            this.listBox1.Items.Add(SpaceLineSet.CompleteCleanDefaultSet);
            this.listBox1.Items.Add(SpaceLineSet.UniDefaultSet);
            /*
            var f = new Debug.AltCharViewer();
            f.Item = AltMaker.UniDefault();
            f.ShowDialog();
            */
        }
        public void Create(SpaceLineSet set)
        {
            
            var w = set;
            var ww = TimeDebug.Watch2(()=> new SpaceMaker(w, SuouEditorConst.AAFont, SuouEditorConst.TextColor),"Create"+w.Name);
            var w3 = 300.Select((v) => this.Create(ww, v)).ToArray();
            string s = string.Join(System.Environment.NewLine, w3);
            this.suouTextBox1.Text = s;
            
        }
        public string Create(SpaceMaker item,int n)
        {
            string s = "";
            s += n + "@";
            if (item.CanCreateSpace(n))
            {
                s += item.CreateSpace(n).Sentence;
            }
            else
            {
                s += "null";
            }
            return s;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var v = (SpaceLineSet)this.listBox1.SelectedItem;
            this.Create(v);
        }
    }
}
