﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Operator;
using SuouTextBox2.Base;
using SuouTextBox2.Controls;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase;
using SuouTextBox2.Debug;
using SuouBase.Drawing;
using BSystem;
using WebExtension;
using SuouBase.Book;
namespace SuouTextBox2
{
    public partial class SuouTextBox : UserControl
    {
        #region Constructor
        public SuouTextBox()
        {

            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            InitializeComponent();
            if (!this.DesignMode)
            {
                Preload();
            }
            
        }
        #endregion
        #region EventHandler

        public event TurnEndEventHandler TurnEnded;
        public event EventHandler HsbValueChanged;
        public event EventHandler VsbValueChanged;
        public event EventHandler CSizeChanged;
        public event PaintEventHandler CPainted;
        public event MouseEventHandler CMouseWheel;
        public event EventHandler CaretMoved;

        [Browsable(true)]
        public new event EventHandler TextChanged;

        public void DoLostFocus()
        {
            this.Caret.CaretLost();
        }
        public void Caret_Changed(object sender, EventArgs e)
        {
            if (CaretMoved != null)
            {
                CaretMoved(this, new EventArgs());
            }
        }

        #endregion
        #region MouseEvent
        private void MouseWheeled(object sender, MouseEventArgs e)
        {
            if (CMouseWheel != null) CMouseWheel(sender, e);
            TurnEnd();
        }
        private void SuouTextBox2_MouseDown(object sender, MouseEventArgs e)
        {
            if (UserInputEnable)
            {
                //this.Caret.Visible = true;
                this.Select();
                //if(!this.Focus())throw new Exception();
                MouseInputOperator.Control_MouseDown(e);
                TurnEnd();
            }
        }
        private void SuouTextBox2_MouseMove(object sender, MouseEventArgs e)
        {
            if (UserInputEnable)
            {
                //if(this.Cursor != this.MouseInputOperator.LeftPenItem.Cursor)this.Cursor = this.MouseInputOperator.LeftPenItem.Cursor;
                MouseInputOperator.Control_MouseMove(e);
                TurnEnd();
            }
            else
            {
                if(this.Cursor != Cursors.Arrow)this.Cursor = Cursors.Arrow;
            }
        }

        private void SuouTextBox2_MouseUp(object sender, MouseEventArgs e)
        {
            if (UserInputEnable)
            {
                MouseInputOperator.Control_MouseUp(e);
                TurnEnd();
            }
        }
        #endregion
        #region SuouTextBox_Event
        private void SuouTextBox2_Paint(object sender, PaintEventArgs e)
        {
            if (CPainted != null) CPainted(sender, e);
            TurnEnd();
        }
        private void SuouTextBox2_SizeChanged(object sender, EventArgs e)
        {
            if (this.Size.IsEmpty) return;
            if (CSizeChanged != null) CSizeChanged(sender, e);
            TurnEnd();
        }
        private void SuouTextBox2_VisibleChanged(object sender, EventArgs e)
        {
            if (DrawOperator != null) this.DrawOperator.IsALLDraw = true;

            TurnEnd();
        }

        private void SuouTextBox2_Resize(object sender, EventArgs e)
        {
        }

        private void SuouTextBox2_DragEnter(object sender, DragEventArgs e)
        {
            if (!CanOriginalDrag) return;
            if (e.Data.GetDataPresent(typeof(AstPage)))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
            TurnEnd();
        }

        private void SuouTextBox2_DragDrop(object sender, DragEventArgs e)
        {
            if (!CanOriginalDrag) return;
            if (e.Data.GetDataPresent(typeof(AstPage)))
            {
                var ap = (AstPage)e.Data.GetData(typeof(AstPage));
                this.AddText(BSystem.Text.StringExtensions.ToParagraphJoin(ap.ListText));
            }
            TurnEnd();
        }

        private void SuouTextBox_Enter(object sender, EventArgs e)
        {
            CanHaveCaret();
        }
        #endregion
        #region EventMethod

        private void FontSet_SettingChanged(object sender, EventArgs e)
        {
            if (this.TextOperator != null)
            {
                this.TextOperator.Text.ClearImage();
            }
            this.DrawOperator.CheckRefleshFlag();
            TurnEnd();
        }
        private void vScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            if (VsbValueChanged != null) VsbValueChanged(this, e);
            TurnEnd();
        }
        public void TurnEnd()
        {
            if (TurnEnded != null) TurnEnded(this, new TurnEndEventArg(this.CreateGraphics()));
        }
        public void ReDraw()
        {
            /*
            DrawOperator.IsALLDraw = true;
            DrawOperator.UpdateGraphic(this.CreateGraphics());
            this.DrawOperator.ScrrenCopy("REDRAW");
            */
        }

        private void hScrollBar1_ValueChanged(object sender, EventArgs e)
        {
            if (HsbValueChanged != null) HsbValueChanged(this, e);
            
            TurnEnd();
        }
        private void vScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            if (VsbValueChanged != null) VsbValueChanged(this, e);
            TurnEnd();
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            if (HsbValueChanged != null) HsbValueChanged(this, e);
            TurnEnd();
        }
        private void TextOperator_TextChanged(object sender, EventArgs e)
        {
            if (TextChanged != null) TextChanged(this, e);
        }
        
        public void RefleshDraw()
        {
            if(this.DrawOperator != null)this.DrawOperator.Reflesh();
        }

        public void CanHaveCaret()
        {
            this.Caret.CanHaveCaret = this.UserInputEnable;
            if (this.UserInputEnable)
            {
                this.Cursor = this.MouseInputOperator.LeftPenItem.Cursor;
                //this.Cursor = InverseOperator.DLC.Choice.Image;
            }
            else
            {
                this.Cursor = Cursors.Arrow;
            }
        }

        private void scrollToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        [Browsable(false)]
        [ReadOnly(true)]
        public string PageSizeInfo
        {
            get
            {
                return this.RawText.GetSitarabaByteSize.ToString() + "Byte(したらば)";
            }
        }
        public static string NotifyCaretInformation(TextBoxCaret tb)
        {
            var s = "";
            var caret = tb;
            if (caret != null)
            {
                var c = caret.StringPos.PreCharacter;
                var pos = (ExEditPoint)caret.StringPos;
                s += "X = " + caret.StringPos.X + " Y = " + caret.StringPos.Y + " ";
                s += pos.X + "dot";


            }
            return s;

        }
        public static string NotifyCaretCharInfomation(TextBoxCaret tb)
        {
            var s = "";
            var caret = tb;
            if (caret != null)
            {
                var c = caret.StringPos.PreCharacter;
                if (c == null) return s;
                s += c.Letter.ToString();
                s += ", ";
                s += c.Width + "dot";
                s += ", ";
                s += c.Letter.ToEncodeSJIS10().LenB_EUC() + "Byte(したらば)";

            }
            return s;

        }

        #endregion
        #region ButtonEvent
        
        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //SettingPage.Form().Show();
        }

        private void layerToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }
        #endregion
        #region ButtonEvent

        private void uTFcopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CutPasteOperator.CopyKey(EncodeType.UTF8);
            TurnEnd();
        }

        private void sJIScopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CutPasteOperator.CopyKey(EncodeType.SJIS10);
            TurnEnd();
        }




        #endregion

        private void timer1_Tick(object sender, EventArgs e)
        {
        }

        private void SuouTextBox_BackColorChanged(object sender, EventArgs e)
        {
            this.PrintFontSet.BackGround = this.BackColor;
        }
        #region OpenEvent
        #endregion
    }
}
