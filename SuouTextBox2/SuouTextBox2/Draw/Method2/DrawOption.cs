﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase.Drawing;
using BSystem.Drawing;
namespace SuouTextBox2.Draw
{
    partial class TextElementDraw
    {
        public static void PreLineDrawLine(IntPtr hDC, SuouLine tf, Point Location, PrintFontSetting PFS)
        {
            var m = tf.Font.Height;
            foreach (var w in tf.Words)
            {
                if (w.IsTransparent)
                {
                    DrawOut.DrawRectangle(hDC, new Rectangle(Location, w.Size), PFS.TransparentColor, true);
                }
                else if (w.HasTag)
                {
                    DrawOut.DrawRectangle(hDC, new Rectangle(Location, w.Size), PFS.TagColor, true);
                }
                Location.X += w.Width;
            }


        }
        public static void DrawSpaceLine(IntPtr hDC, TextLineComposition tf, Point Location,PrintFontSetting PFS)
        {
            var m = tf.Font.Height;
            SuouLetter FrontLetter = null;
            foreach (var c in tf.VirtualLetterIterator())
            {
                if (AATextLaw.IsSpace(c.Letter))
                {
                    if (AATextLaw.IsViolate(FrontLetter == null, c.Letter))
                    {
                        DrawOut.DrawLineRight(hDC, new Point(Location.X, Location.Y + m - 1), c.Width, PFS.ViolateColor);
                    }
                    else if(FrontLetter != null && AATextLaw.IsViolate(FrontLetter.Letter,c.Letter))
                    {
                        DrawOut.DrawLineRight(hDC, new Point(Location.X, Location.Y + m - 1), c.Width, PFS.ViolateColor);
                        DrawOut.DrawLineRight(hDC, new Point(Location.X-FrontLetter.Width, Location.Y + m - 1), FrontLetter.Width, PFS.ViolateColor);
                    }
                    else
                    {
                        if (AATextLaw.IsUniSpace(c.Letter))
                        {
                            DrawOut.DrawLineUp(hDC, new Point(Location.X, Location.Y + m - 1), c.Width, PFS.SpaceUniColor, tf.Font.Height);
                        }
                        else
                        {
                            DrawOut.DrawLineRight(hDC, new Point(Location.X, Location.Y + m - 1), c.Width, PFS.SpaceColor);
                        }

                    }
                }
                Location.X += c.Width;
                FrontLetter = c;
            }


        }
        [Obsolete]
        public static void DrawLine(IntPtr hDC, SuouWord tf,DotPoint Location,DisplayGraphic dg, PrintFontSetting PS)
        {
            var p = Location.ToTextAreaPoint(dg.DotMapArea);
            
            var m = tf.Font.Height;
            int CharWidth;

            //描画
            bool first = true;
            char? BackChar = null;
            char? FrontChar = null;


            for (int j = 0; j < tf.Word.Length; j++)
            {
                CharWidth = tf.Font.Width(tf.Word[j]);
                BackChar = j != 0 ? tf.Word[j - 1] : (char?)null;
                FrontChar = j + 1 < tf.Word.Length ? tf.Word[j + 1] : (char?)null;


                if (AATextLaw.IsSpace(tf.Word[j]))
                {
                    if (AATextLaw.IsViolate(first, BackChar, tf.Word[j], FrontChar))
                    {
                        DrawOut.DrawLineRight(hDC, new Point(p.X, p.Y + m-1), CharWidth, Color.Red);
                    }
                    else
                    {
                        if (CharWidth > 4)
                        {
                            DrawOut.DrawLineRight(hDC, new Point(p.X, p.Y + m - 1), CharWidth, PS.SpaceColor);
                        }
                        else
                        {
                            DrawOut.DrawLineRight(hDC, new Point(p.X, p.Y + m - 1), CharWidth, Color.Red);
                        }
                    }
                }
                p.X += CharWidth;

                first = false;
            }
                
            
        }

        [Obsolete]
        private void Scale(Graphics g,DisplaySetting ds,PrintFontSetting ps)
        {
            //return;

            var rec = new Rectangle(new Point(0, 0), new Size(ds.TextAreaSize.Width, ds.TextAreaPaddingLocation.Y));
            var rec2 = new Rectangle(new Point(0, 0), new Size(ds.TextAreaPaddingLocation.X, ds.TextAreaSize.Height));
            IntPtr hDC = g.GetHdc();
            DrawOut.DrawRectangle(hDC, rec, ps.BackGround, true);
            DrawOut.DrawRectangle(hDC, rec2, ps.BackGround, true);
            var v = new SuouWord(ps.Font, Color.Black, "s");
            //var p = (new Pointy(0, 0, PS)).ToEditPoint();
            //var r = Vob(p.X, p.X + PS.szEdit.Width);
            /*
            
            for (int i = 0; i < r.Length; i++)
            {
                int k = ds.TextAreaPaddingLocation.X + ds.Hsb.Value;
                int n = r[i] - k;
                var f = new PointLine(new Point(n, 0), new Point(n, ds.TextAreaPaddingLocation.Y - 3));
                gf.Add(f);
                var tf = new WordComposition(ps.Font, Color.Black, r[i].ToString());
                DrawOut.Draw(hDC, n, 0, tf);
            }
            */
            var gf = new List<PointLine>();
            var Yokosen = new PointLine(new Point(0, ds.TextAreaPaddingLocation.Y - 3), new Point(ds.TextAreaSize.Width, ds.TextAreaPaddingLocation.Y - 3));
            var Tatesen = new PointLine(new Point(ds.TextAreaPaddingLocation.X - 5, 0), new Point(ds.TextAreaPaddingLocation.X - 5, ds.TextAreaSize.Height));
            gf.Add(Yokosen);
            gf.Add(Tatesen);
            DrawOut.DrawLine(hDC, gf.ToArray(), Color.Black);
            g.ReleaseHdc(hDC);
            //g.Dispose();

        }
    }
}
