﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using SuouBase.Text;
using System.Diagnostics;
using SuouTextBox2.Draw;
using System.Drawing.Drawing2D;

using SuouTextBox2.Base;
namespace SuouTextBox2.Draw
{

    public class DotProjectFunction : ProjectFunction
    {
        public DotProjectFunction(DotPoint srcLocation, Point destLocation, Size Size)
            : base(srcLocation.Position, destLocation, Size)
        {
        }
        public Rectangle DestProject(Rectangle DotRec)
        {

            var x = DotRec.X - SrcRectangle.X + DestRectangle.X;
            var y = DotRec.Y - SrcRectangle.Y + DestRectangle.Y;

            var p = new Point(x, y);
            return new Rectangle(p, DotRec.Size);
        }
        public Rectangle DestProject(TextLineComposition text, Rectangle LineRec)
        {
            var DotRec = text.RecallRec(LineRec);
            var x = DotRec.X - SrcRectangle.X + DestRectangle.X;
            var y = DotRec.Y - SrcRectangle.Y + DestRectangle.Y;

            var p = new Point(x, y);
            return new Rectangle(p, DotRec.Size);
        }
    }
    public class ProjectFunction
    {
        public Point SrcLocation { get; set; }
        public Point DestLocation { get; set; }
        public Size ImageSize { get; set; }
        public Rectangle SrcRectangle
        {
            get
            {
                return new Rectangle(SrcLocation, ImageSize);
            }
        }
        public Rectangle DestRectangle
        {
            get
            {
                return new Rectangle(DestLocation, ImageSize);
            }
        }
        public ProjectFunction(Point srcLocation, Point destLocation, Size Size)
        {
            this.SrcLocation = srcLocation;
            this.DestLocation = destLocation;
            this.ImageSize = Size;
        }
        public Rectangle Project(Rectangle rec)
        {
            var x = rec.X - SrcRectangle.X + DestRectangle.X;
            var y = rec.Y - SrcRectangle.Y + DestRectangle.Y;

            var p = new Point(x, y);
            return new Rectangle(p, rec.Size);
        }
        public BufferedGraphics GetBuffer(Graphics g)
        {
            BufferedGraphicsContext currentContext = BufferedGraphicsManager.Current;
            BufferedGraphics myBuffer = currentContext.Allocate(g, this.DestRectangle);
            return myBuffer;
        }
        public BufferedGraphics GetBuffer(Graphics g,int[] nths,DisplayPointMaker dp,DisplaySetting ds)
        {
            var DisplayRec = dp.GetTextAreaBondLinesInDisplay(nths.First(), nths.Last());
            var DisplayRec2 = Rectangle.Intersect(DisplayRec, ds.TextAreaRectangle);
            BufferedGraphicsContext currentContext = BufferedGraphicsManager.Current;
            BufferedGraphics myBuffer = currentContext.Allocate(g, DisplayRec2);
            return myBuffer;
        }
    }
}
