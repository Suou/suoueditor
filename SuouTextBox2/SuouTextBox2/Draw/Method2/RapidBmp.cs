﻿
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using SuouBase;

namespace SuouBase.Drawing
{

    public struct Bmpstr
    {
        public int Width, Height;
        public Pixel[,] pix;
        private int x, y;
        public Bmpstr(int width, int height)
        {
            Width = width;
            Height = height;
            pix = new Pixel[height, width];
            x = 0; y = 0;
        }

        public void add(Pixel p)
        {
            pix[y, x] = p;
            if (++x == Width)
            {
                x = 0; y++;
            }
        }
    }
    public struct Pixel
    {
        public byte r, g, b, a;
        public Pixel(int r,int g,int b,int a)
        {
            this.r = (byte)r;
            this.g = (byte)g;
            this.b = (byte)b;
            this.a = (byte)a;
        }
    }
    public class TurboArgbPixelFilter32
    {
        public static void InverseProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = (byte)~b;
            g = (byte)~g;
            r = (byte)~r;

        }

        /// <summary>
        /// 画像のすべてのピクセルをループして指定した処理をする。
        /// </summary>
        /// <param name="bmp">処理対象の画像。PixelFormat.Format32bppArgbのみ対応。</param>
        /// <param name="proc">ピクセルごとに行う処理。1ピクセルごとに呼び出される。</param>
        public static void FastFiltering(
            Bitmap bmp, PixelProc proc)
        {

            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            BitmapData bmpData = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppArgb);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                for (int i = 0; i < dataLength; i += 4)
                {
                    proc(
                        ref *(pixel++),
                        ref *(pixel++),
                        ref *(pixel++),
                        ref *(pixel++));
                }
            }

            bmp.UnlockBits(bmpData);
        }
        public static void FastFilteringRectangle(Bitmap bmp, PixelProc proc,Rectangle rec)
        {

            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            var bmprec = new Rectangle(Point.Empty, bmp.Size);
            rec.Intersect(bmprec);
            if (rec.Size.IsEmpty || rec.Size.Width == 0) return;
            BitmapData bmpData = bmp.LockBits(rec, ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;

                for (int y = 0; y < rec.Height; y++)
                {
                    
                    for (int x = 0; x < rec.Width; x++)
                    {
                        proc(ref *(pixel++),ref *(pixel++),ref *(pixel++),ref *(pixel++));
                    }
                    pixel += (bmp.Width-rec.Width)*4;
                }
            }

            bmp.UnlockBits(bmpData);
        }
        public static Bmpstr FastCopyStruct(Bitmap bmp)
        {

            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            BitmapData bmpData = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppArgb);

            Bmpstr bs = new Bmpstr(bmp.Width,bmp.Height);

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                for (int i = 0; i < dataLength; i += 4)
                {
                    Pixel p = new Pixel(*(pixel++),*(pixel++),*(pixel++),*(pixel++));
                    bs.add(p);
                }
            }

            bmp.UnlockBits(bmpData);
            return bs;
        }
        public static Pixel FastGetPixel(Bitmap bmp, Point p)
        {

            if (bmp.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            BitmapData bmpData = bmp.LockBits(
                new Rectangle(Point.Empty, bmp.Size),
                ImageLockMode.ReadWrite,
                PixelFormat.Format32bppArgb);

            Pixel gel;

            unsafe
            {
                byte* pixel = (byte*)bmpData.Scan0;
                int dataLength = bmpData.Stride * bmpData.Height;

                pixel += (((p.Y * bmp.Width) + p.X) * 4);
                gel = new Pixel(*(pixel++), *(pixel++), *(pixel++), *(pixel++));
            }

            bmp.UnlockBits(bmpData);
            return gel;
        }

        public static void DrawImage(Bitmap bmp1, Bitmap bmp2, Point StartLocation1, Size DrawAreaSize)
        {
            if (bmp1.Size == bmp2.Size)
            {
                var a = new Rectangle(StartLocation1, DrawAreaSize);
                var b = new Rectangle(new Point(0, 0), bmp1.Size);
                var newrec = Rectangle.Intersect(a, b);
                DrawImage2(bmp1, bmp2, newrec.Location, newrec.Location, newrec.Size);
            }
            else
            {
                DrawImage2(bmp1, bmp2, StartLocation1, StartLocation1, DrawAreaSize);
            }
        }
        /*
        public static void DrawImage(Bitmap bmp1, Bitmap bmp2, Point StartLocation1)
        {
            var k = ModifyRectangle(bmp1, bmp2, StartLocation1, new Point(0, 0), bmp2.Size);
            DrawImage2(bmp1, bmp2, k.Key.Location,k.Value.Location, k.Key.Size);
        }
        */
        public static KeyValuePair<Rectangle,Rectangle> ModifyRectangle(Bitmap bmp1, Bitmap bmp2, Point StartLocation1, Point StartLocation2, Size DrawAreaSize)
        {
            
            var x1 = StartLocation1.X < 0 ? StartLocation1.X : 0;
            var x2 = StartLocation2.X < 0 ? StartLocation2.X : 0;
            var x3 = x1 < x2 ? x1 : x2;

            var y1 = StartLocation1.Y < 0 ? StartLocation1.Y : 0;
            var y2 = StartLocation2.Y < 0 ? StartLocation2.Y : 0;
            var y3 = y1 < y2 ? y1 : y2;

            var p1 = new Point(StartLocation1.X - x3, StartLocation1.Y - y3);
            var p2 = new Point(StartLocation2.X - x3, StartLocation2.Y - y3);
            var sz = new Size(DrawAreaSize.Width + x3, DrawAreaSize.Height + y3);

            var bmprec1 = new Rectangle(new Point(0, 0), bmp1.Size);
            var bmprec2 = new Rectangle(new Point(0, 0), bmp2.Size);
            var rec1 = new Rectangle(p1, sz);
            var rec2 = new Rectangle(p2, sz);
            rec1.Intersect(bmprec1);
            rec2.Intersect(bmprec2);



            
            var szr = new Size(rec1.Width < rec2.Width ? rec1.Width : rec2.Width, rec1.Height < rec2.Height ? rec1.Height : rec2.Height);

            var rec3 = new Rectangle(rec1.Location, szr);
            var rec4 = new Rectangle(rec2.Location, szr);
            return new KeyValuePair<Rectangle, Rectangle>(rec3, rec4);
        }
        public static KeyValuePair<Rectangle, Rectangle> ModifyRectangle2(Bitmap bmp1, Bitmap bmp2, Rectangle rec1,Rectangle rec2)
        {
            Rectangle rrec1, rrec2;
            if (rec1.Contains(rec2))
            {
                rrec1 = rec2;
                rrec2 = new Rectangle(Point.Empty, rec2.Size);
                
            }
            else if (rec2.Contains(rec1))
            {
                rrec1 = new Rectangle(Point.Empty, rec1.Size);
                rrec2 = rec1;
            }
            else if (rec1.IntersectsWith(rec2))
            {
                var vrec = Rectangle.Intersect(rec1, rec2);

                var p1 = new Point(vrec.X - rec1.X, vrec.Y - rec1.Y);
                rrec1 = new Rectangle(p1, vrec.Size);

                var p2 = new Point(vrec.X - rec2.X, vrec.Y - rec2.Y);
                rrec2 = new Rectangle(p2, vrec.Size);
               
            }
            else
            {
                rrec1 = Rectangle.Empty;
                rrec2 = Rectangle.Empty;
            }
            return new KeyValuePair<Rectangle, Rectangle>(rrec1, rrec2);
            
        }
        public static void DrawImage(Bitmap bmp1, Bitmap bmp2, Point StartLocation1, Point StartLocation2, Size DrawAreaSize)
        {
            var k = ModifyRectangle(bmp1, bmp2, StartLocation1, StartLocation2, DrawAreaSize);

            if (k.Key.Size.IsEmpty) return;
            DrawImage2(bmp1, bmp2, k.Key.Location, k.Value.Location, k.Key.Size);
            
        }
        public static void DrawImage(Bitmap bmp1, Bitmap bmp2, Point StartLocation1)
        {
            var rec = new Rectangle(Point.Empty, bmp1.Size);
            var rec2 = new Rectangle(StartLocation1, bmp2.Size);
            var emp = ModifyRectangle2(bmp1,bmp2,rec, rec2);
            if (emp.Key.Size.IsEmpty || emp.Value.Size.IsEmpty) return;
            DrawImage2(bmp1, bmp2, emp.Key.Location, emp.Value.Location, emp.Key.Size);
        }

        private static void DrawImage2(Bitmap bmp1, Bitmap bmp2, Point StartLocation1, Point StartLocation2, Size DrawAreaSize)
        {

            if (bmp1.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }
            Color ToukaColor = Color.FromArgb(255, 255, 255, 1);
            BitmapData bmpData1 = bmp1.LockBits(new Rectangle(StartLocation1, DrawAreaSize), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            BitmapData bmpData2 = bmp2.LockBits(new Rectangle(StartLocation2, DrawAreaSize), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            unsafe
            {
                byte* pixel1 = (byte*)bmpData1.Scan0;
                byte* pixel2 = (byte*)bmpData2.Scan0;

                {
                    for (int y = 0; y < DrawAreaSize.Height; y++)
                    {
                        for (int x = 0; x < DrawAreaSize.Width; x++)
                        {
                            ToukaCopy(ref *(pixel1++), ref *(pixel1++), ref *(pixel1++), ref *(pixel1++), ref  *(pixel2++), ref  *(pixel2++), ref  *(pixel2++), ref  *(pixel2++), ToukaColor);
                            /*
                            for (int z = 0; z < 4; z++)
                            {
                                *(pixel1) = *(pixel2);
                                pixel1++; pixel2++;
                            }
                            */
                        }
                        pixel1 += (bmp1.Width - DrawAreaSize.Width) * 4;
                        pixel2 += (bmp2.Width - DrawAreaSize.Width) * 4;
                    }
                }
            }
            bmp1.UnlockBits(bmpData1);
            bmp2.UnlockBits(bmpData2);
        }

        private static void DrawImage(BitmapData bmpData1, Bitmap bmp2, Point StartLocation)
        {

            if (bmpData1.PixelFormat != PixelFormat.Format32bppArgb)
            {
                throw new ArgumentException(
                    "bmpのPixelFormatはFormat32bppArgbのみ有効です。", "bmp");
            }

            
            BitmapData bmpData2 = bmp2.LockBits(new Rectangle(Point.Empty, bmp2.Size),ImageLockMode.ReadWrite,PixelFormat.Format32bppArgb);
            

            unsafe
            {
                byte* pixel1 = (byte*)bmpData1.Scan0;
                byte* pixel2 = (byte*)bmpData2.Scan0;

                {
                    int dataLength = bmpData1.Stride * bmpData1.Height;

                    pixel1 += (((StartLocation.Y * bmpData1.Width)) * 4);

                    for (int y = 0; y < bmp2.Height; y++)
                    {
                        pixel1 += StartLocation.X * 4;
                        for (int x = 0; x < bmp2.Width; x++)
                        {
                            pixel2 += 3;
                            if (*(pixel2) == 0)
                            {
                                pixel1 += 4;
                                pixel2++;
                            }
                            else
                            {
                                pixel2 -= 3;
                                for (int z = 0; z < 4; z++)
                                {
                                    *(pixel1) = *(pixel2);
                                    pixel1++; pixel2++;
                                }
                            }
                        }
                        pixel1 += (bmpData1.Width - bmpData2.Width - StartLocation.X) * 4;
                    }
                }
            }
            //bmp1.UnlockBits(bmpData1);
            bmp2.UnlockBits(bmpData2);
        }


        /// <summary>
        /// ImageからPixelFormat32pbbArgbのBitmapを作成して返します。
        /// </summary>
        /// <returns>PixelFormat32pbbArgbのBitmap</returns>
        public static Bitmap GetRegularizedBitmap(Image img)
        {
            Bitmap bmp = new Bitmap(img.Width, img.Height, PixelFormat.Format32bppArgb);

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.DrawImage(img, 0, 0, img.Width, img.Height);
            }

            return bmp;
        }



        public static void ToukaCopy(ref byte b1, ref byte g1, ref byte r1, ref byte a1, ref byte b2, ref byte g2, ref byte r2, ref byte a2,Color c)
        {
            
            var c2 = Color.FromArgb(a2,r2,g2,b2);
            if (c!=c2)
            {
                b1 = b2;
                g1 = g2;
                r1 = r2;
                a1 = a2;
            }
        }
        public delegate void PixelProc(ref byte b, ref byte g, ref byte r, ref byte a);
    }

}