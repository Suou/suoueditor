﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase.Drawing;
namespace SuouTextBox2.Draw
{
    partial class TextElementDraw
    {
        /*
        public static void LineWordDraw(SuouWord w, IntPtr hDC, Point Location)
        {
            DrawOut.Draw(hDC, w.Font.ToHfont(), Location.X, Location.Y, w.Word, w.Color);
        }
        */
        public static void LineWordDraw(SuouWord w, IntPtr hDC, Point Location,PrintFontSetting pfs)
        {
            if (pfs.IsPriority)
            {
                DrawOut.Draw(hDC, w.Font.ToHfont(), Location.X, Location.Y, w.Word, pfs.TextColor);
            }
            else
            {
                DrawOut.Draw(hDC, w.Font.ToHfont(), Location.X, Location.Y, w.Word, w.Color);
            }
        }
        public static void LineSentenceDraw(TextLineComposition lc, IntPtr hDC)
        {
            var p = new Point(0, lc.Yinterval);

            PreLineDrawLine(hDC, lc, p, lc.PrintFontSet);
            var words = lc.PrintFontSet.IsDrawSpace ? lc.VirtualWords : lc.RealWords;
            
            foreach (var w in words)
            {
                LineWordDraw(w, hDC, p,lc.PrintFontSet);
                p.X += w.Width;
            }
            p.X = 0;
            if(lc.PrintFontSet.IsDrawSpace)DrawSpaceLine(hDC, lc, p, lc.PrintFontSet);
            
        }

        public static void WordDraw(DisplayGraphic dg, SuouWord w, IntPtr hDC,DotPoint Location)
        {
            
            var p = Location.ToTextAreaPoint(dg.DotMapArea);
            DrawOut.Draw(hDC, w.Font.ToHfont(), p.X, p.Y, w.Word, w.Color);
        }
        public static void SentenceDraw(DisplayGraphic dg, TextLineComposition lc,IntPtr hDC,PrintFontSetting PS)
        {
            var p = lc.Location;
            int x = 0;
            foreach (var w in lc.VirtualWords)
            {
                p = new DotPoint() { X = lc.Location.X + x, Y = lc.Location.Y };
                WordDraw(dg, w, hDC,p);
#pragma warning disable CS0612 // 型またはメンバーが旧型式です
                DrawLine(hDC, w, p,dg, PS);
#pragma warning restore CS0612 // 型またはメンバーが旧型式です
                x += w.Width;
            }
            
        }
        public static void TextDraw(DisplayGraphic dg, TextComposition tc, PrintFontSetting PS)
        {
            
            IntPtr hDC = dg.Graphic.GetHdc();

            foreach (TextLineComposition lp in tc)
            {
                if (dg.DotMapArea.IntersectsWith(lp.DotVirtualRectangle) || dg.DotMapArea.Contains(lp.DotVirtualRectangle))
                {
                    SentenceDraw(dg, lp, hDC,PS);
                }
            }
            
            dg.Graphic.ReleaseHdc();
            
        }
        public static Bitmap DrawPaddingArea(DisplaySetting displaySetting)
        {
            var sz = displaySetting.DisplaySize;
            Bitmap bmp = new Bitmap(sz.Width, sz.Height, PixelFormat.Format32bppArgb);
            var g = Graphics.FromImage(bmp);
            g.Clear(Color.Green);
            
            g.Dispose();
            return bmp;
        }
        public static Bitmap DrawTextArea(Rectangle displayArea, TextComposition tc, PrintFontSetting PS,Color TransParentColor)
        {
            //Stopwatch sw = Stopwatch.StartNew();
            var tsz = displayArea.Size;
            
            Bitmap textbmp = new Bitmap(tsz.Width, tsz.Height, PixelFormat.Format32bppArgb);
            
            var textg = Graphics.FromImage(textbmp);
            textg.Clear(TransParentColor);
            var DrawDisplayGraphic = new DisplayGraphic(displayArea,textg);
            TextDraw(DrawDisplayGraphic, tc,PS);
            textg.Dispose();
            return textbmp;

        }
        
    }
    public class DisplayGraphic
    {
        public Rectangle DotMapArea;
        public Graphics Graphic;
        public Size Size;
        public DisplayGraphic(Rectangle dDispayArea, Graphics g)
        {
            this.DotMapArea = dDispayArea;
            this.Graphic = g;
        }
        public DisplayGraphic(Rectangle dDispayArea, Graphics g,Size sz)
        {
            this.DotMapArea = dDispayArea;
            this.Graphic = g;
            this.Size = sz;
        }
    }
}
