﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Draw;
using SuouBase.Drawing;
using SuouTextBox2.Base;

namespace SuouTextBox2.Operator
{
    partial class DrawMethod
    {
        private Bitmap GetBackGroundImageDotArea(Rectangle DotTextArea)
        {
            var bmp = new Bitmap(DotTextArea.Width, DotTextArea.Height, PixelFormat.Format32bppArgb);
            var g = Graphics.FromImage(bmp);
            g.Clear(Color.White);
            g.Dispose();
            var p = PointMaker.CreateExDotPoint(0, 0).ToTextAreaPoint(DotTextArea);
            TurboArgbPixelFilter32.DrawImage(bmp, BackGroundPicture,p.Position);

            return bmp;
        }
    }
}
