﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Drawing;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouTextBox2.Draw;
using System.Diagnostics;
using SuouBase.Drawing;
namespace SuouTextBox2.Operator
{
    partial class DrawMethod
    {
        
        private static void InverseDraw(Bitmap bmp, Rectangle InverseArea)
        {
            TurboArgbPixelFilter32.FastFilteringRectangle(bmp, InverseProc, InverseArea);
        }
        private static void InverseProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = (byte)~b;
            g = (byte)~g;
            r = (byte)~r;

        }


        
        private Bitmap GetInverseTextImageDotArea(Rectangle DotTextArea)
        {
            var bmp = GetTextImageDotArea(DotTextArea);
            var dotbands = InverseData.CreateDotBands(TextSet);
            foreach (var v in dotbands)
            {
                var InverseDotArea = PointMaker.CreateDotRectangle(v);

                if (DotTextArea.IntersectsWith(InverseDotArea) || DotTextArea.Contains(InverseDotArea))
                {
                    var rec2 = new Rectangle(new Point(InverseDotArea.X - DotTextArea.X, InverseDotArea.Y - DotTextArea.Y), InverseDotArea.Size);
                    DrawMethod.InverseDraw(bmp, rec2);
                }
            }
            
            return bmp;
        }
        
        
        

        #region IDC
        private void UpdateBackTextImageIDC(InverseDataCollection NewIDC)
        {
            var OldIDC = this.InverseData;
            var BlackInverseData = NewIDC - OldIDC;
            var WhiteInverseData = OldIDC - NewIDC;
            var ChangeInverseData = NewIDC ^ OldIDC;
            
            
            this.InverseData = NewIDC;
            var ab = ChangeInverseData.LocationYs;
            foreach (var n in ab)
            {
                if (n >= 0 && n < TextSet.Count)
                {
                    var line = (TextLineComposition)TextSet[n];
                    line.ClearInverseImage();
                    foreach (var v in this.InverseData.GetDotBands(n, TextSet))
                    {
                        line.Inverse(v.Location.X,v.Width);
                    }
                }
            }
            if (ab.Length != 0)
            {
                this.StringY = ab;
                IsChanged = true;
            }
        }
        private void UpdateBackImageBand(DotBand band)
        {
            var InverseDotArea = PointMaker.CreateDotRectangle(band);
#pragma warning disable CS0612 // 型またはメンバーが旧型式です
            var InverseArea = PointMaker.CreateTextAreaRectangle(band);
#pragma warning restore CS0612 // 型またはメンバーが旧型式です
            if (DisplaySet.TextAreaMapRectangle.IntersectsWith(InverseDotArea) || DisplaySet.TextAreaMapRectangle.Contains(InverseDotArea))
            {
                //BackImage = TextElementDraw.MergeTextAndPadding(BackImage, BackInverseTextImage, InverseArea, DisplaySet, PointMaker);
            }
        }

        private void WhiteDraw(InverseDataCollection InveseIDC)
        {

            var dotbands = InveseIDC.CreateDotBands(TextSet);
            foreach (var v in dotbands)
            {
                //UpdateBackInvarseTextImageBand(v,Color.White);
                UpdateBackImageBand(v);
            }
        }
        private void BlackDraw(InverseDataCollection InveseIDC)
        {

            var dotbands = InveseIDC.CreateDotBands(TextSet);
            foreach (var v in dotbands)
            {
                //this.UpdateBackInvarseTextImageBand(v, Color.Black);
                this.UpdateBackImageBand(v);
            }
        }
        #endregion
    }
}
