﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Draw;

using SuouTextBox2.Base;
namespace SuouTextBox2.Operator
{
    partial class DrawMethod
    {
        #region UpdateLine
        private void UpdateTextLine(Graphics g, int nth,Rectangle LimitDotRec,ProjectFunction Projecter)
        {
            if (nth < 0 || nth >= TextSet.Count) return;
            var v = TextSet.Line(nth);
            var LimitDisplayRec = PointMaker.TranslateFromDotToTextAreaInDisplay(LimitDotRec);
            var DisplayDrawRec = PointMaker.GetTextAreaBondLineInDisplay(nth);
            var rDisplayDrawRec = Rectangle.Intersect(LimitDisplayRec, DisplayDrawRec);
            var rp = new Point(rDisplayDrawRec.X - DisplayDrawRec.X, rDisplayDrawRec.Y - DisplayDrawRec.Y);
            var p = PointMaker.CreateExDotPoint(v.BandLocation.Position).ToDisplayPoint();
            var TextImageDrawLocation = new Point(DisplayDrawRec.X - p.X+rp.X, DisplayDrawRec.Y - p.Y+rp.Y);
            var TextImageDrawRec = new Rectangle(TextImageDrawLocation, rDisplayDrawRec.Size);

            g.DrawImage(v.InverseImage, Projecter.Project(rDisplayDrawRec),TextImageDrawRec,GraphicsUnit.Pixel);
            
        }
        
        private void UpdateTextLineD(Graphics g, int nth, DotProjectFunction Projecter)
        {
            if (nth < 0 || nth >= TextSet.Count) return;
            var v = TextSet.Line(nth);
            var lineRec = v.Insertsect(Projecter.SrcRectangle);
            g.DrawImage(v.InverseImage, Projecter.DestProject(v,lineRec), lineRec, GraphicsUnit.Pixel);
            //g.DrawImage(v.InverseImage, v.Location.Position);
        }
        
        private void UpdateInverseLine(Graphics g, int nth, Rectangle LimitDotRec,ProjectFunction Functer)
        {
            var LimitDisplayRec = PointMaker.TranslateFromDotToTextAreaInDisplay(LimitDotRec);
            var bands = this.InverseData.GetDotBands(nth, TextSet);
            foreach (var band in bands)
            {
                var InverseArea = PointMaker.CreateDisplayRectangle(band);
                var DrawInverseArea = Rectangle.Intersect(LimitDisplayRec, InverseArea);

                g.FillRectangle(Brushes.Blue, Functer.Project(DrawInverseArea));
                
            }
        }
        private void UpdateInverseLineD(Graphics g, int nth, DotProjectFunction Functer)
        {
            var bands = this.InverseData.GetDotBands(nth, TextSet);
            foreach (var band in bands)
            {
                var InverseArea = PointMaker.CreateDotRectangle(band);
                var DrawInverseArea = Rectangle.Intersect(Functer.SrcRectangle, InverseArea);
                g.FillRectangle(Brushes.Blue, Functer.Project(DrawInverseArea));
            }
        }
        private void UpdateBackGroundLine(Graphics g, int nth,ProjectFunction Projecter)
        {
            var rec1 = PointMaker.GetDotTextAreaBondLine(nth);
            var rec2 = Rectangle.Intersect(rec1, DisplaySet.TextAreaMapRectangle);
            this.UpdateBackGroundArea(g, rec2,Projecter);
        }
        #endregion

        #region UpdateArea
        private void UpdatePaddingArea(Graphics g, ProjectFunction Functer)
        {
            var UpperRec = Rectangle.Intersect(DisplaySet.UpperPaddingRectangle, Functer.SrcRectangle);
            var LeftRec = Rectangle.Intersect(DisplaySet.LeftPaddingRectangle, Functer.SrcRectangle);


            g.FillRectangle(new SolidBrush(PrintFontSet.PaddingColor), Functer.Project(UpperRec));
            g.FillRectangle(new SolidBrush(PrintFontSet.PaddingColor), Functer.Project(LeftRec));
        }
        private void UpdateRulerArea(Graphics g, ProjectFunction Projecter)
        {
            
            var bmp = RulerImage.GetImage(DisplaySet.TextAreaMapLocation.X, DisplaySet.TextAreaSize.Width);
            var RulerSize = bmp.Size;
            var p = new Point(0-DisplaySet.TextAreaMapLocation.X,0);
            var RulerDisRec = new Rectangle(p, RulerSize);
            
            var RulerSrcRec = new Rectangle(Point.Empty, RulerSize);
            var DestRec = Projecter.Project(RulerDisRec);
            g.DrawImage(bmp, DestRec, RulerSrcRec, GraphicsUnit.Pixel);
        }
        /*
        private void UpdateRulerArea(Graphics g, Rectangle DisplayArea,ProjectFunction Projecter)
        {
            var DisRec = new Rectangle(RulerImage.Location.Position, DisplaySet.TextAreaSize);

            var DisRec2 = RulerImage.TranslateToDotRectangle(DisRec,DisplaySet);
            var bmp = RulerImage.GetImage(DisplaySet.TextAreaMapLocation.X,DisplaySet.TextAreaSize.Width);
            //g.DrawImage(bmp, Projecter.Project(DisRec), DisRec2, GraphicsUnit.Pixel);
            g.DrawImage(bmp, DisRec, DisRec2, GraphicsUnit.Pixel);
        }
        */
        private void UpdateTextArea(Graphics g, Rectangle DotArea,ProjectFunction Projecter)
        {
            var lines = TextSet.GetRealLinesNth(DotArea);
            foreach (var y in lines) this.UpdateTextLine(g,y,DotArea,Projecter);
            
        }
        private void UpdateTextAreaD(Graphics g, DotProjectFunction Projecter)
        {
            var lines = TextSet.GetRealLinesNth(Projecter.SrcRectangle);
            foreach (var y in lines) this.UpdateTextLineD(g, y, Projecter);

        }
        private void UpdateInverseArea(Graphics g, Rectangle DotArea,ProjectFunction Functer)
        {
            var lines = TextSet.GetVirtualLinesNth(DotArea);
            foreach (var y in lines) this.UpdateInverseLine(g, y,DotArea,Functer);
        }
        private void UpdateInverseAreaD(Graphics g, DotProjectFunction Functer)
        {
            var lines = TextSet.GetVirtualLinesNth(Functer.SrcRectangle);
            foreach (var y in lines) this.UpdateInverseLineD(g, y, Functer);
        }
        private void UpdateBackGroundArea(Graphics g, Rectangle DotArea,ProjectFunction Functer)
        {

            var TextAreaInDisplayRec = PointMaker.TranslateFromDotToTextAreaInDisplay(DotArea);
            //var dp = new Point(DotArea.X - DisplaySet.TextAreaPaddingLocation.X, DotArea.Y - DisplaySet.TextAreaPaddingLocation.Y);

            g.FillRectangle(new SolidBrush(PrintFontSet.BackGround), Functer.Project(TextAreaInDisplayRec));
            if (BackGroundPicture == null) return;
            var rDotArea = PointMaker.TranslateFromDisplayToDot(TextAreaInDisplayRec);
            g.DrawImage(BackGroundPicture, Functer.Project(TextAreaInDisplayRec), rDotArea, GraphicsUnit.Pixel);

        }
        private void UpdateBackGroundAreaD(Graphics g, ProjectFunction Functer)
        {

            g.FillRectangle(new SolidBrush(PrintFontSet.BackGround), Functer.DestRectangle);
            if (BackGroundPicture == null) return;
            g.DrawImage(BackGroundPicture, Functer.DestRectangle, Functer.SrcRectangle, GraphicsUnit.Pixel);

        }
        #endregion

        #region UpdateALL
        private void UpdateTextALL(Graphics g,ProjectFunction Projecter)
        {
            this.UpdateTextArea(g, DisplaySet.TextAreaMapRectangle,Projecter);
        }
        private void UpdateInverseALL(Graphics g, ProjectFunction Projecter)
        {
            this.UpdateInverseArea(g, DisplaySet.TextAreaMapRectangle, Projecter);
        }
        private void UpdateBackGroundALL(Graphics g, ProjectFunction Projecter)
        {
            this.UpdateBackGroundArea(g, DisplaySet.TextAreaMapRectangle, Projecter);
        }
        private void UpdatePaddingALL(Graphics g, ProjectFunction Projecter)
        {
            this.UpdatePaddingArea(g, Projecter);
        }
        private void UpdateRulerALL(Graphics g)
        {
            //var p = DisplaySet.TextAreaMapLocation;
            
            var sp = new Point(DisplaySet.TextAreaMapLocation.X,0);
            
            var sz = new Size(DisplaySet.TextAreaSize.Width,50);
            var bmp = RulerImage.GetImage(DisplaySet.TextAreaMapLocation.X,DisplaySet.TextAreaSize.Width);
            var np = new Point(DisplaySet.TextAreaPaddingLocation.X, DisplaySet.Padding.Top-bmp.Height);
            var srcrec = new Rectangle(sp,sz);
            var destrec = new Rectangle(np,sz);
            g.DrawImage(bmp, destrec, srcrec,GraphicsUnit.Pixel);
          
        }
        #endregion


    }
}
