﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Draw;

using SuouTextBox2.Base;


namespace SuouTextBox2.Operator
{
    partial class DrawMethod
    {

        /// <summary>
        /// DisplayAreaの拡大は伴わないこと
        /// </summary>
        /// <param name="nowlocation"></param>
        public void UpdateALLBackImageScroll(ExDotPoint nowlocation)
        {
            IsChanged = true;
            IsRefreshFlag = true;
            /*
            var previewLocation = this.NowDotPosition;
            var zureX = nowlocation.X - previewLocation.X;
            var zureY = nowlocation.Y - previewLocation.Y;

            
            if (zureX == 0 && zureY == 0) return;
            if(zureX != 0 && zureY != 0)
            {
                this.CreateALLBackImage();
                IsChanged = true;
                ALLDraw = true;
            }
            else
            {
                var DotSabunLocationX = zureX > 0 ? previewLocation.X + DisplaySet.TextAreaSize.Width : nowlocation.X;
                var DotSabunLocationY = zureY > 0 ? previewLocation.Y + DisplaySet.TextAreaSize.Height: nowlocation.Y;
                var DotSabunLocation = PointMaker.CreateExDotPoint(DotSabunLocationX, DotSabunLocationY);
                var DotSabunAreaSizeWidth = zureX != 0 ? Math.Abs(zureX) : DisplaySet.TextAreaSize.Width;
                var DotSabunAreaSizeHeight = zureY != 0 ? Math.Abs(zureY) : DisplaySet.TextAreaSize.Height;
                Size DotSabunAreaSize = new Size(DotSabunAreaSizeWidth, DotSabunAreaSizeHeight);


                Rectangle DotSabunArea = new Rectangle(DotSabunLocation.Position, DotSabunAreaSize);
                //var zureBmp = TextElementDraw.DrawTextArea(DotSabunArea, TextSet,PrintFontSet,TransParentColor);

                var SabunDrawLocationX = zureX > 0 ? DisplaySet.TextAreaSize.Width - zureX : 0;
                var SabunDrawLocationY = zureY > 0 ? DisplaySet.TextAreaSize.Height - zureY : 0;
                var SabunDrawLocation = new Point(SabunDrawLocationX, SabunDrawLocationY);
                Point ZureLocation = new Point(-zureX, -zureY);
                
                //this.UpdateBackTextImageScroll(SabunDrawLocation, ZureLocation,DotSabunArea);
                this.UpdateBackImageScroll();
                NowDotPosition = DisplaySet.TextAreaMapLocation;
                IsChanged = true;
                ALLDraw = true;
            }
            */
        }
        private void UpdateBackImageScroll()
        {
            //BackImage = TextElementDraw.MergeTextAndPadding(BackImage, DrawingTextImage, DisplaySet);
        }

    }
}
