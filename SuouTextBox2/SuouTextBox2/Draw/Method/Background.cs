﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Draw;

using SuouTextBox2.Base;


namespace SuouTextBox2.Operator
{
    partial class DrawMethod
    {
        /*
         * Graphic　　・・・ControlのGraphics
         * BackImage ・・・Graphic描画用Bitmap
         * BackALLTextImage・・・BackImage描画用の全てのTextImage
         * TextImage・・・テキストエリアが記録されたBitmap
         * InverseTextImage・・・TextImageに反転情報を付加したBitmap
         * BackALLImage・・・Graphic描画用の全てのBitmap
         * 
         * Line・・・一行エリア
         * Area・・・Rectangleエリア
         * DelAdd・・・DelAddエリア
         * 
         * Update・・・更新
         * Create・・・作成
         * 
         * 
         */

        
        private Bitmap GetTextImageDotArea(Rectangle TextArea)
        {
            Bitmap bmp = TextElementDraw.DrawTextArea(TextArea, TextSet, PrintFontSet,TransParentColor);
            return bmp;
        }
        private Bitmap GetTextImageBondLine(int rownum)
        {
            var rec = PointMaker.GetDotTextAreaBondLine(rownum);
            Bitmap bmp = TextElementDraw.DrawTextArea(rec, TextSet,PrintFontSet,TransParentColor);
            return bmp;
        }
        private Bitmap GetInverseTextImageBondLine(int rownum)
        {
            var rec = PointMaker.GetDotTextAreaBondLine(rownum);

            return this.GetInverseTextImageDotArea(rec);
        }
        private void TextClearProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = TransParentColor.B;
            g = TransParentColor.G;
            r = TransParentColor.R;
            a = TransParentColor.A;
        }
        private void ClearProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            b = Color.Green.B;
            g = Color.Green.G;
            r = Color.Green.R;
            a = Color.Green.A;
        }
        
        private void CreateALLBackImage()
        {

            PaddingImage = TextElementDraw.DrawPaddingArea(DisplaySet);
            //NowDotPosition = DisplaySet.TextAreaMapLocation;
            IsChanged = true;
            IsRefreshFlag = true;
        }
        
        /*
        private void UpdateBackImageLine(int[] y)
        {
            foreach (int n in y)
            {
                var p = TextSet.GetLinesYDotLocation(n);

                var dp = PointMaker.CreateExDotPoint(0, p);
                var disp = dp.ToDisplayPoint();
                var ChangeTextAreaPoint = PointMaker.CreateExDisplayPoint(DisplaySet.Padding.Left, disp.Y);

                var sz = DisplaySet.DisplayLineSize(n);
                var ClearRectangle = new Rectangle(ChangeTextAreaPoint.Position, sz);
                var rec = new Rectangle(ChangeTextAreaPoint.Position, sz);
                var rec2 = new Rectangle(ChangeTextAreaPoint.ToDotPoint().ToTextAreaPoint().Position, sz);
                
                //ClearBackImageLine(n);
                //TurboArgbPixelFilter32.DrawImage(BackImage,DrawingTextImage, rec.Location, rec2.Location,rec2.Size);
                
            }
        }
        */
        #region DelAdd
        private void UpdateALLBackImageDelAdd(int[] changeY)
        {
            this.UpdateBackInvarseTextImageDelAdd(changeY);
            UpdateBackImageDelAdd(changeY);
            IsChanged = true;
            IsRefreshFlag = false;
        }
        private void UpdateBackImageDelAdd(int[] changeY)
        {
            StringY = changeY;
        }
        private void UpdateBackInvarseTextImageDelAdd(int[] changeY)
        {
        }
        #endregion
    }
}
