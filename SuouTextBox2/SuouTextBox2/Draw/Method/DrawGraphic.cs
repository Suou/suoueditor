﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Draw;
using SuouBase.Text;

using SuouTextBox2.Base;
namespace SuouTextBox2.Operator
{
    partial class DrawMethod
    {
        private void UpdateGraphicArea(Graphics g, Point DestLocation,Point SrcLocation,Size RectangleSize)
        {
            var Functer = new ProjectFunction(SrcLocation, DestLocation, RectangleSize);

            Caret.Hide(); ;
            var DisplayTextRec = Rectangle.Intersect(Functer.SrcRectangle, DisplaySet.TextAreaRectangle);
            var TextDotRec = PointMaker.TranslateFromDisplayToDot(Functer.SrcRectangle);
            
            using (var myBuffer = Functer.GetBuffer(g))
            {
                //CaretのVisible.Flaseいるかな？
                this.UpdatePaddingArea(myBuffer.Graphics,Functer);
                if(DisplaySet.IsRuler)this.UpdateRulerArea(myBuffer.Graphics,Functer);
                this.UpdateBackGroundArea(myBuffer.Graphics, TextDotRec,Functer);
                this.UpdateInverseArea(myBuffer.Graphics, TextDotRec,Functer);
                this.UpdateTextArea(myBuffer.Graphics, TextDotRec,Functer);
                myBuffer.Render();
            }
            Caret.ShowCaret();
        }
        //1:エリア描画
        private void UpdateGraphicArea(Graphics g, DotProjectFunction Functer,bool IsInverseArea,bool IsText,bool IsOverDraw,Color backColor)
        {
            Caret.Hide();
            using (var myBuffer = Functer.GetBuffer(g))
            {
                if(IsOverDraw)myBuffer.Graphics.Clear(backColor);
                //this.UpdateBackGroundAreaD(myBuffer.Graphics, Functer);
                if(IsInverseArea)this.UpdateInverseAreaD(myBuffer.Graphics, Functer);
                if(IsText)this.UpdateTextAreaD(myBuffer.Graphics, Functer);
                myBuffer.Render();
            }
            Caret.ShowCaret();
        }

        

        private void UpdateAllGraphic(Graphics g)
        {
            var Functer = new ProjectFunction(Point.Empty, Point.Empty, DisplaySet.DisplaySize);
            Caret.Hide();
            using (var myBuffer = Functer.GetBuffer(g))
            {
                this.UpdatePaddingALL(myBuffer.Graphics,Functer);//非テキストエリア
                if (DisplaySet.IsRuler) this.UpdateRulerALL(myBuffer.Graphics);//ルーラー
                this.UpdateBackGroundALL(myBuffer.Graphics, Functer);//背景
                this.UpdateInverseALL(myBuffer.Graphics, Functer);//反転
                this.UpdateTextALL(myBuffer.Graphics, Functer);//テキスト
                
                myBuffer.Render();
            }
            Caret.ShowCaret();
        }
        
        private void UpdateGraphic(Graphics g)
        {
            if (IsChanged)
            {
                if (Drawing != null) Drawing(this, new EventArgs());
                
                if (IsRefreshFlag)
                {
                    Stopwatch sw1 = Stopwatch.StartNew();
                    Action<Graphics> t = this.UpdateAllGraphic;
                    //this.Ctrl.BeginInvoke(t,g);//非同期
                    //t.BeginInvoke(g,null,null);
                    UpdateAllGraphic(g);
                    sw1.Stop();
                }
                else if (StringY != null)
                {
                    ProjectFunction Functer = new ProjectFunction(Point.Empty, Point.Empty, DisplaySet.DisplaySize);
#pragma warning disable CS0612 // 型またはメンバーが旧型式です
                    foreach (var v in LocationsBunkatu(StringY)) this.UpdateGraphicLines(g, v,Functer);
#pragma warning restore CS0612 // 型またはメンバーが旧型式です
                }
                IsChanged = false;
                IsRefreshFlag = false;
                StringY = null;
            }
        }
        //2:ライン描画
        private void UpdateGraphicLines(Graphics g, int[] nths,ProjectFunction Functer)
        {
            if (nths.Length == 0) return;
            Caret.Hide();
            using (var myBuffer = Functer.GetBuffer(g,nths,PointMaker,DisplaySet))
            {
                foreach (var nth in nths)
                {
                    this.UpdateBackGroundLine(myBuffer.Graphics, nth,Functer);//背景
                    this.UpdateInverseLine(myBuffer.Graphics, nth, DisplaySet.TextAreaMapRectangle, Functer);//反転
                    this.UpdateTextLine(myBuffer.Graphics, nth, DisplaySet.TextAreaMapRectangle, Functer);//テキスト
                }
                myBuffer.Render();
            }
            Caret.ShowCaret();
        }
    }
}
