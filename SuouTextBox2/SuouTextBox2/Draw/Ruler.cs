﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Draw;

using SuouTextBox2.Base;

namespace SuouTextBox2.Draw
{
    public class Ruler
    {
        public Bitmap Image { get; set; }
        private Font Font{get;set;}
        public ExDisplayPoint Location
        {
            get
            {
                var p = this.DisplaySet.TextAreaPaddingLocation.Copy();
                p.Y -= Image.Height;
                return p;
            }
        }
        private DisplaySetting DisplaySet { get; set; }

        public Ruler(int x ,int len,Font f,DisplaySetting DS)
        {
            this.Font = f;
            CreateImage(x, len);
            this.DisplaySet = DS;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DisplayRec"></param>
        /// <param name="DS"></param>
        /// <returns></returns>
        public Rectangle TranslateToDotRectangle(Rectangle DisplayRec,DisplaySetting DS)
        {
            var x = DisplayRec.X - DS.Padding.Left + DS.TextAreaMapLocation.X;
            var y = DisplayRec.Y - this.Location.Y;
            DisplayRec.Location = new Point(x, y);
            return DisplayRec;
        }
        private void CreateImage(int x,int len)
        {
            Image = new Bitmap(x + len, 25, PixelFormat.Format32bppArgb);
            Graphics g = Graphics.FromImage(Image);

            var PensColor = Pens.DarkGray;

            g.DrawLine(PensColor, new Point(0, Image.Height-1), new Point(Image.Width, Image.Height-1));

            for (int i = 0; i < Image.Width; i += 50)
            {
                var pin = new Point(i, Image.Height);
                var strpin = new Point(i, Image.Height-this.Font.Height);
                if (i % 100 == 0)
                {
                    g.DrawLine(PensColor, pin, new Point(i, Image.Height / 4));
                }
                else
                {
                    g.DrawLine(PensColor, pin, new Point(i, Image.Height / 2));
                }
                if (i % 100 == 0) g.DrawString(i.ToString(), Font, Brushes.DarkBlue, strpin);
            }
        }
        public Bitmap GetImage(int x,int len)
        {
            if (Image.Size.Width < x + len) CreateImage(x,len);
            
            return Image;
        }
    }
}
