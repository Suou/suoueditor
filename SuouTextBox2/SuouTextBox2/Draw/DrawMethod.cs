﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Threading;
using System.Runtime.InteropServices;
using SuouBase;
using SuouBase.Text;
using System.Diagnostics;
using SuouTextBox2.Draw;
using System.Drawing.Drawing2D;

using SuouBase.Drawing;
using SuouTextBox2.Base;


namespace SuouTextBox2.Operator
{
    public partial class DrawMethod
    {
        /// <summary>
        /// Draw
        /// ・DisplayArea拡大を伴うDraw
        /// ・DisplayArea拡大を伴わないDraw
        /// </summary>

        #region Property
        public DisplayPointMaker PointMaker;
        public PrintFontSetting PrintFontSet;
        public DisplaySetting DisplaySet;
        public TextComposition TextSet;
        public Ruler RulerImage;
        public TextBoxCaret Caret;
        private Bitmap BackGroundPicture = null;//(Bitmap)Bitmap.FromFile("D:/My Pictures/1330343405574.jpg");
        private Color TransParentColor = Color.FromArgb(255, 255, 255, 1);
        public bool IsChanged = false;
        public bool IsRefreshFlag = true;
        private int[] StringY;
        //public List<Bitmap> BitmapList = new List<Bitmap>();
        private InverseDataCollection InverseData = new InverseDataCollection();

        public event EventHandler Drawing;

        public Control Ctrl;

        public bool IsALLDraw
        {
            get { return this.IsRefreshFlag; }
            set
            {
                this.IsChanged = true;
                this.IsRefreshFlag = value;
            }
        }

        //private ExDotPoint NowDotPosition;


        public Bitmap PaddingImage = null;

        #endregion

        #region Constructor
        public DrawMethod(Control Ctrl,DisplaySetting displaySetting,
            PrintFontSetting printFontSetting,TextComposition textSet,TextBoxCaret caret,DisplayPointMaker pm)
        {
            this.Ctrl = Ctrl;
            this.DisplaySet = displaySetting;
            this.PrintFontSet = printFontSetting;
            this.TextSet = textSet;
            this.Caret = caret;
            this.PointMaker = pm;
            this.RulerImage = new Ruler(0, DisplaySet.TextAreaSize.Width, TextSet.Font,DisplaySet);
            IsChanged = true;
            IsRefreshFlag = true;
        }
        #endregion

        #region staticMethod
        [Obsolete]
        public static int[][] LocationsBunkatu(int[] nths)
        {
            List<int[]> r = new List<int[]>();
            List<int> rr = new List<int>();
            for (int i = 0; i < nths.Length; i++)
            {

                if (i != 0 && nths[i] != nths[i - 1] + 1)
                {
                    r.Add(rr.ToArray());
                    rr.Clear();
                }
                rr.Add(nths[i]);
            }
            r.Add(rr.ToArray());
            return r.ToArray();
        }
        #endregion
        #region Method
        public void CheckRefleshFlag()
        {
            this.IsChanged = true;
            this.IsRefreshFlag = true;
        }
        public void Reflesh()
        {
            this.CheckRefleshFlag();
            this.Ctrl.CreateGraphics();
        }
        public Bitmap GetImage(DotPoint SrcLocation, Size ImageSize, Color backColor)
        {
            var bmp = new Bitmap(ImageSize.Width, ImageSize.Height, PixelFormat.Format32bppArgb);
            var g = Graphics.FromImage(bmp);
            var Functer = new DotProjectFunction(SrcLocation, Point.Empty, ImageSize);
            this.UpdateGraphicArea(g, Functer, true, true, true, backColor);
            g.Dispose();
            return bmp;
        }
        public Bitmap GetTextImage(Bitmap bmp, DotPoint SrcLocation)
        {
            var g = Graphics.FromImage(bmp);
            var Functer = new DotProjectFunction(SrcLocation, Point.Empty, bmp.Size);
            //this.UpdateGraphicArea(g, Functer, false, false, false);
            this.UpdateTextAreaD(g, Functer);
            g.Dispose();
            return bmp;
        }
        public void DrawImage(Graphics g, Point DestLocation, DotPoint SrcLocation, Size ImageSize, Color backColor)
        {

            var Functer = new DotProjectFunction(SrcLocation, Point.Empty, ImageSize);
            this.UpdateGraphicArea(g, Functer, true, true, true, backColor);
        }
        public void DrawImage(Graphics g, Point DestLocation, Point SrcLocation, Size ImageSize)
        {
            this.UpdateGraphicArea(g, DestLocation, SrcLocation, ImageSize);
        }
        #endregion
        #region Event

        public void Control_TurnEnded(object sender, TurnEndEventArg e)
        {
            if (!this.Ctrl.Visible) return;
            if (IsChanged)
            {
                this.UpdateGraphic(e.ControlGraphic);
            }
        }
        
        public void ScrollBar_ValueChanged(object sender, EventArgs e)
        {
            this.UpdateALLBackImageScroll(DisplaySet.TextAreaMapLocation);
            
           
        }
        public void InverseMethod_InverseChanged(object sender, InverseMethodEventArg e)
        {
            this.UpdateBackTextImageIDC(e.InverseData);
            InverseData = e.InverseData;   
        }
        public void TextChanged_Draw(object sender, TextChangeEventArg e)
        {
            if (e.IsTextLengthChange)
            {
                this.CreateALLBackImage();
            }
            else
            {
                this.UpdateALLBackImageDelAdd(e.ChangeY);
            }

        }
        public void Control_Paint(object sender, PaintEventArgs e)
        {
            
            if (PaddingImage != null)
            {
                //12/10/3 変更
                Rectangle rec = new Rectangle(Point.Empty, PaddingImage.Size);
                if (rec.Contains(e.ClipRectangle))
                {
                    this.UpdateGraphicArea(e.Graphics, e.ClipRectangle.Location,e.ClipRectangle.Location,e.ClipRectangle.Size);
                }
            }
            this.CreateALLBackImage();
            this.UpdateGraphic(e.Graphics);
            
            e.Dispose();
        }
        
        public void Control_SizeChanged(object sender, EventArgs e)
        {
            this.CreateALLBackImage();
        }
        #endregion
    }

    
}
