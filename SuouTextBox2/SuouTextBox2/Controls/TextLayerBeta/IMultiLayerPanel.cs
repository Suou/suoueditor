﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Text;
using SuouBase.Book;
using System.Windows.Forms;
using System.Drawing;
namespace SuouTextBox2.Controls.TextLayerBeta
{
    public interface IMultiLayerPanel
    {
        IBookPage Page { get; set; }
        Size Size { get; set; }
        void SetPageSize();
        void AddPanel(Control control);
        Point TranslateEditPoint(EditPoint p);
        EditPoint TranslatePoint(Point p);

        void Refresh();
        void UpdateImage();
        event EventHandler DoubleClick;
    }
}
