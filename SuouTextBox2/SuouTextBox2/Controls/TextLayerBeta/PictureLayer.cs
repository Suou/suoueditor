﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Book;
using SuouBase.Drawing;
using SuouBase.Controls;
namespace SuouTextBox2.Controls.TextLayerBeta
{
    public partial class PictureLayer : PictureBox
    {
        public PictureLayer()
        {
            InitializeComponent();

            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //this.BackColor = Color.Transparent; // 半透明// ダブルバッファ
            this.ChangeBackColor(false);
        }
        public PictureLayer(AbstractSectionScenePart part,LayerFunctions functions)
            : this()
        {
            this.Part = part;
            this.Functions = functions;
            this.Changer = new MouseDragMoveChanger(this);
            this.Changer.Moved += this.MoveChanger_Moved;
            this.Changer.DragStarted += this.MoveChanger_DragStarted;
            this.Changer.DragEnded += this.MoveChanger_DragEnded;
        }
        private MouseDragMoveChanger Changer { get; set; }
        private EditPoint DragStartedEditLocation { get; set; }
        //public event EventHandler RequireUpdating;

        private void MoveChanger_DragEnded(object sender, EventArgs e)
        {
            this.Functions.ShowPage(this.Part);
        }
        private void MoveChanger_DragStarted(object sender,EventArgs e)
        {
            this.Functions.HidePage(this.Part);
            this.DragStartedEditLocation = this.Part.EditLocation;
        }
        private void MoveChanger_Moved(object sender, Point changedPoint)
        {
            var p = this.Functions.TranslateEditLocation(this.DragStartedEditLocation);
            var newp = new Point(p.X + changedPoint.X, p.Y + changedPoint.Y);
            var newED = this.Functions.TranslatePoint(newp);
            if (this.Part.EditLocation != newED)
            {
                this.Part.EditLocation = newED;
            }
        }
        AbstractSectionScenePart Part { get; set; }
        private LayerFunctions Functions { get; set; }

        private SuouText _DrawText = null;
        private SuouText DrawText
        {
            get {
                if (this._DrawText == null) this._DrawText = this.Part.CreateWakuText();
                return this._DrawText;
            }
        }
        private void PictureLayer_Paint(object sender, PaintEventArgs e)
        {
            if (this.Changer.IsDragging)
            {
                
                e.Graphics.DrawText(this.DrawText, 2, Point.Empty);
            }
            else
            {
                //this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
                //this.BackColor = Color.Transparent; // 半透明// ダブルバッファ
            }
        }

        private void PictureLayer_MouseEnter(object sender, EventArgs e)
        {
            this.ChangeBackColor(true);
        }
        private void ChangeBackColor(bool isEntering)
        {
            if (isEntering)
            {
                this.BackColor = Color.FromArgb(100, 255, 0, 0);
            }
            else
            {
                this.BackColor = Color.FromArgb(100, 255, 255, 0); // 半透明 
            }
        }

        private void PictureLayer_MouseLeave(object sender, EventArgs e)
        {
            this.ChangeBackColor(false);
        }
    }
}
