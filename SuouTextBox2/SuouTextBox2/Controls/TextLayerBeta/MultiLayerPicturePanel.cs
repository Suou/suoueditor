﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using SuouBase.Book;
using SuouBase;
using System.Drawing;
using System.Windows.Forms;
using SuouBase.Text;
namespace SuouTextBox2.Controls.TextLayerBeta
{
    public class MultiLayerPicturePanel:SuouBase.Controls.BookPagePlainPictureBox,IMultiLayerPanel
    {
        public void SetPageSize()
        {
            this.Size = this.PictureSize;
        }
        public void AddPanel(Control c)
        {
            this.Controls.Add(c);
        }
    }
}
