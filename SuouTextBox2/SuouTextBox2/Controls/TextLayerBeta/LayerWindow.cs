﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Book;
using SuouBase;
namespace SuouTextBox2.Controls.TextLayerBeta
{
    public partial class LayerWindow : UserControl
    {
        public LayerWindow()
        {
            InitializeComponent(); 
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }
        public LayerWindow(AbstractSectionScenePart part, LayerWindowMode mode, LayerFunctions functions)
            : this()
        {
            this.Part = part;
            this.Size = part.SText.Size;
            this.Functions = functions;
            this.Mode = mode;
            if (this.Functions != null) this.Location = this.Functions.TranslateEditLocation(this.Part.EditLocation);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            //this.BackColor = Color.FromArgb(100, 255, 255, 0);
            //this.BackColor = Color.Transparent;
        }
        public event Action<LayerWindow> ModeChanged;

        private AbstractSectionScenePart _Part = null;
        public AbstractSectionScenePart Part
        {
            get { return this._Part; }
            set
            {
                this._Part = value;
                if (this.Part != null)
                {
                    this.Part.ContentChanged += this.Page_ContentChanged;
                }
            }
        }
        private void Set()
        {
            if (this._Part == null)
            {
            }
            else
            {
                switch (this.Mode)
                {
                    case LayerWindowMode.Multi:
                        this.CreateMultiLayer();
                        break;
                    case LayerWindowMode.Picture:
                        this.CreatePictureLayer();
                        break;
                    case LayerWindowMode.Text:
                        this.CreateTextLayer();
                        break;
                    case LayerWindowMode.Transparent:
                        //this.HaveControl = null;
                        this.CreatePictureLayer();
                        break;
                }
            }
        }
        private void CreateMultiLayer()
        {
            if (this.Part is SectionScene)
            {
                var scene = (SectionScene)this.Part;
                this._MultiLayer = new MultiLayer(scene);
                this.HaveControl = this._MultiLayer;
                this._MultiLayer.DoubleClick += this.MultiLayer_DoubleClick;
            }
        }
        private void MultiLayer_DoubleClick(object sender,EventArgs e)
        {
            this.Mode = LayerWindowMode.Text;
        }
        private PictureLayer CreatePictureLayer()
        {
            if (this._PictureLayer == null)
            {
                this.SetStyle(ControlStyles.SupportsTransparentBackColor, false);
                this._PictureLayer = new PictureLayer(this.Part, this.Functions);
                this.HaveControl = this._PictureLayer;
                this._PictureLayer.DoubleClick += this.PictureLayer_DoubleClicked;
                return this._PictureLayer;
            }
            else
            {
                this.HaveControl = this._PictureLayer;
                return this._PictureLayer;                
            }
        }
        private void PictureLayer_DoubleClicked(object sender, EventArgs e)
        {
            this.Mode = LayerWindowMode.Text;
        }
        private TextLayer CreateTextLayer()
        {
            if (this._TextLayer == null)
            {
                this._TextLayer = new TextLayer(this.Part);
                this.HaveControl = this._TextLayer;
                return this._TextLayer;
            }
            else
            {
                this.HaveControl = this._TextLayer;
                return this._TextLayer;
            }
        }
        private LayerFunctions Functions { get; set; }

        private LayerWindowMode _Mode;
        public LayerWindowMode Mode
        {
            get
            {
                return this._Mode;
            }
            set
            {
                if (this._Mode == value) return;
                this._Mode = value;
                this.Set();
                if (this.ModeChanged != null) this.ModeChanged(this);
            }
        }
        private void Page_ContentChanged(object sender, SectionBookPagePartContentChangeEventArgs e)
        {
            if (e.Type == MergeContentChangeType.EditLocation)
            {
                if (this.Functions != null)
                {
                    this.Location = this.Functions.TranslateEditLocation(this.Part.EditLocation);
                }
            }
        }
        private MultiLayer _MultiLayer = null;
        private TextLayer _TextLayer = null;
        private PictureLayer _PictureLayer = null;
        private Control _HaveControl = null;
        public Control HaveControl
        {
            get { return this._HaveControl; }
            set
            {
                if (this._HaveControl != null)
                {
                    this.Controls.Remove(this._HaveControl);
                }
                this._HaveControl = value;
                if (value == null) return;
                this.Controls.Add(value);
                this.HaveControl.Location = Point.Empty;
                this.HaveControl.Dock = DockStyle.Fill;
            }
        }

        private void LayerWindow_MouseEnter(object sender, EventArgs e)
        {
            /*
            if (this.Mode == LayerWindowMode.Transparent)
            {
                this.BackColor = Color.FromArgb(100, 255, 0, 0); // 半透明                        
            }
            */
        }

        private void LayerWindow_MouseLeave(object sender, EventArgs e)
        {
            /*
            if (this.Mode == LayerWindowMode.Transparent)
            {
                this.BackColor = Color.FromArgb(100, 255, 255, 0); // 半透明                       
            }
            */
        }

        private void LayerWindow_Paint(object sender, PaintEventArgs e)
        {

        }

        private void LayerWindow_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                if (this.Mode == LayerWindowMode.Transparent)
                {
                    this.Mode = LayerWindowMode.Picture;
                }
            }
        }

    }
    public enum LayerWindowMode
    {
        Text,Picture,Multi,Transparent
    }
}
