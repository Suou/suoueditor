﻿namespace SuouTextBox2.Controls.TextLayerBeta
{
    partial class LayerWindow
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LayerWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.Name = "LayerWindow";
            this.Size = new System.Drawing.Size(146, 146);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.LayerWindow_Paint);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.LayerWindow_MouseDown);
            this.MouseEnter += new System.EventHandler(this.LayerWindow_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.LayerWindow_MouseLeave);
            this.ResumeLayout(false);

        }

        #endregion
    }
}
