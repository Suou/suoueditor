﻿namespace SuouTextBox2.Controls.TextLayerBeta
{
    partial class MultiLayer
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.rulerPicturePanel1 = new SuouTextBox2.Controls.TextLayerBeta.RulerPicturePanel.RulerPicturePanel();
            this.SuspendLayout();
            // 
            // rulerPicturePanel1
            // 
            this.rulerPicturePanel1.Location = new System.Drawing.Point(0, 0);
            this.rulerPicturePanel1.Name = "rulerPicturePanel1";
            this.rulerPicturePanel1.Page = null;
            this.rulerPicturePanel1.Size = new System.Drawing.Size(351, 294);
            this.rulerPicturePanel1.TabIndex = 0;
            // 
            // MultiLayer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.Controls.Add(this.rulerPicturePanel1);
            this.Name = "MultiLayer";
            this.Size = new System.Drawing.Size(351, 294);
            this.SizeChanged += new System.EventHandler(this.MultiLayer_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private RulerPicturePanel.RulerPicturePanel rulerPicturePanel1;







    }
}
