﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Book;
using SuouBase.Text;
namespace SuouTextBox2.Controls.TextLayerBeta
{
    public partial class MultiLayer : UserControl
    {
        public MultiLayer()
        {
            InitializeComponent();
        }
        private IMultiLayerPanel _Panel = null;
        public IMultiLayerPanel Panel
        {
            get { return this._Panel; }
            set
            {
                this.RemoveEvent(this._Panel);
                this._Panel = value;
                if (this._Panel == null) return;
                this._Panel.DoubleClick += this.MultiLayerPanel_DoubleClick;
            }
        }
        private void RemoveEvent(IMultiLayerPanel panel)
        {
            if (panel == null) return;
            panel.DoubleClick -= this.MultiLayerPanel_DoubleClick;
        }
        private void MultiLayerPanel_DoubleClick(object sender, EventArgs e)
        {
            if (this.NotPictureModeLayer != null)
            {
                this.NotPictureModeLayer = null;
            }
            else
            {
                this.OnDoubleClick(e);
            }
        }
        public MultiLayer(SectionScene scene):this()
        {
            this.Panel = this.rulerPicturePanel1;
            this.Scene = scene;
            this.Panel.Page = scene;
            this.Panel.SetPageSize();
            
            this.Functions = this.CreateFunctions();
            this.Layers = new List<LayerWindow>();
            for (int i = 1; i < scene.Parts.Count; i++)
            {
                this.Set(scene.Parts[i]);
            }
            
        }
        public List<LayerWindow> Layers { get; set; }
        public SectionScene Scene { get; set; }

        private LayerWindow _NotPictureModeLayer = null;
        private LayerWindow NotPictureModeLayer
        {
            get
            {
                return this._NotPictureModeLayer;
            }
            set
            {
                if (this._NotPictureModeLayer == value) return;
                if (this._NotPictureModeLayer != null) this._NotPictureModeLayer.Mode = LayerWindowMode.Picture;
                this._NotPictureModeLayer = value;
            }
        }
        
        private void Set(AbstractSectionScenePart part)
        {
            var p = new LayerWindow(part, LayerWindowMode.Picture,this.Functions);
            p.LocationChanged += this.LayerWindow_LocationChanged;
            p.ModeChanged += this.ChildLayer_ModeChanged;
            
            
            this.Panel.AddPanel(p);
            this.Layers.Add(p);
        }
        private LayerFunctions Functions { get; set; }
        private void ChildLayer_ModeChanged(LayerWindow panel)
        {
            if (panel.Mode != LayerWindowMode.Picture)
            {
                this.NotPictureModeLayer = panel;
            }
            else
            {
                if (this.NotPictureModeLayer == panel) this.NotPictureModeLayer = null;
            }
        }

        private void suouTextBox1_VsbValueChanged(object sender, EventArgs e)
        {
            foreach (var v in this.Layers)
            {
                v.Location = this.Panel.TranslateEditPoint(v.Part.EditLocation);
            }
        }

        private void suouTextBox1_HsbValueChanged(object sender, EventArgs e)
        {

            foreach (var v in this.Layers)
            {
                v.Location = this.Panel.TranslateEditPoint(v.Part.EditLocation);
            }
        }
        private void LayerWindow_LocationChanged(object sender, EventArgs e)
        {
            this.Panel.Refresh();
        }
        private void bookPagePictureBox1_SizeChanged(object sender, EventArgs e)
        {
            //this.bookPagePictureBox1.Loading();
        }

        private void bookPagePictureBox1_Click(object sender, EventArgs e)
        {
            foreach (var v in this.Layers)
            {
                v.Mode = LayerWindowMode.Transparent;
            }
        }
        private void UpdateControl()
        {
            this.Panel.UpdateImage();
        }
        private void HidePage(AbstractSectionScenePart part)
        {
            var p = this.Scene.Parts.First((v) => v == part);
            p.Enable = false;
            this.Panel.UpdateImage();
            //p.Enable = true;
        }
        private void ShowPage(AbstractSectionScenePart part)
        {
            var p = this.Scene.Parts.First((v) => v == part);
            p.Enable = true;
            this.Panel.UpdateImage();
        }


        private LayerFunctions CreateFunctions()
        {
            var r = new LayerFunctions();
            r.TranslateEditLocation = this.Panel.TranslateEditPoint;
            r.TranslatePoint = this.Panel.TranslatePoint;
            r.UpdateBackGroundControl = this.UpdateControl;
            r.HidePage = (v) => this.HidePage(v);
            r.ShowPage = (v) => this.ShowPage(v);
            return r;
        }

        private void MultiLayer_SizeChanged(object sender, EventArgs e)
        {
            this.rulerPicturePanel1.MinimumSize = this.Size;
        }


    }
    public class LayerFunctions{

        public Func<EditPoint, Point> TranslateEditLocation { get; set; }
        public Func<Point, EditPoint> TranslatePoint { get; set; }
        public Action UpdateBackGroundControl { get; set; }
        public Action<AbstractSectionScenePart> HidePage { get; set; }
        public Action<AbstractSectionScenePart> ShowPage { get; set; }
    }
}
