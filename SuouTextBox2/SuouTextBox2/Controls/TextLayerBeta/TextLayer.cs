﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Book;
using SuouBase;
namespace SuouTextBox2.Controls.TextLayerBeta
{
    public partial class TextLayer : SuouTextBox
    {
        public TextLayer()
        {
            InitializeComponent();
        }
        private AbstractSectionScenePart Part { get; set; }
        public TextLayer(AbstractSectionScenePart part):this()
        {
            this.SetRawText(part.SText);
            this.Part = part;
        }
        private void Text_Changed(object sender, EventArgs e)
        {
            this.Part.SText = this.RawText;
        }
    }
}
