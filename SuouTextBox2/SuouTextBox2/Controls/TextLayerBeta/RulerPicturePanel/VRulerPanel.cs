﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouTextBox2.Controls.TextLayerBeta.RulerPicturePanel
{
    public partial class VRulerPanel : UserControl
    {
        public VRulerPanel()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.Interval = 2;
        }
        public int StartY { get; set; }
        public int Interval { get; set; }
        private void VRulerPanel_Paint(object sender, PaintEventArgs e)
        {
            int t = 0;
            for (int y = this.StartY; y < this.Height; y += this.Font.Height + this.Interval)
            {
                var str = t.ToString().PadLeft(3, ' ');
                e.Graphics.DrawString(str, this.Font, Brushes.Black, new Point(0, y));
                t++;
            }
        }
    }
}
