﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouTextBox2.Controls.TextLayerBeta.RulerPicturePanel
{
    public partial class HRulerPanel : UserControl
    {
        public HRulerPanel()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer | ControlStyles.AllPaintingInWmPaint, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
        }

        private void HRulerPanel_Paint(object sender, PaintEventArgs e)
        {
            for (int x = 0; x < this.Width; x += 10)
            {
                if ((x % 100) == 0)
                {
                    e.Graphics.DrawString(x.ToString(), this.Font, Brushes.Black, new Point(x, 0));
                    e.Graphics.DrawLine(Pens.Black, new Point(x, this.Height), new Point(x, 0));
                }
                else if ((x % 50) == 0)
                {
                    e.Graphics.DrawLine(Pens.Black, new Point(x, this.Height), new Point(x, this.Height / 4));
                }
                else
                {
                    e.Graphics.DrawLine(Pens.Black, new Point(x, this.Height), new Point(x, this.Height / 2));
                }
            }
        }
    }
}
