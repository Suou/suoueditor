﻿namespace SuouTextBox2.Controls.TextLayerBeta.RulerPicturePanel
{
    partial class RulerPicturePanel
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.multiLayerPicturePanel1 = new SuouTextBox2.Controls.TextLayerBeta.MultiLayerPicturePanel();
            this.hRulerPanel1 = new SuouTextBox2.Controls.TextLayerBeta.RulerPicturePanel.HRulerPanel();
            this.vRulerPanel1 = new SuouTextBox2.Controls.TextLayerBeta.RulerPicturePanel.VRulerPanel();
            ((System.ComponentModel.ISupportInitialize)(this.multiLayerPicturePanel1)).BeginInit();
            this.SuspendLayout();
            // 
            // multiLayerPicturePanel1
            // 
            this.multiLayerPicturePanel1.AutoSizeType = SuouBase.Controls.ShrinkType.None;
            this.multiLayerPicturePanel1.EnableCopy = false;
            this.multiLayerPicturePanel1.EnablePaste = false;
            this.multiLayerPicturePanel1.Item = null;
            this.multiLayerPicturePanel1.Location = new System.Drawing.Point(28, 30);
            this.multiLayerPicturePanel1.Name = "multiLayerPicturePanel1";
            this.multiLayerPicturePanel1.Page = null;
            this.multiLayerPicturePanel1.Size = new System.Drawing.Size(284, 246);
            this.multiLayerPicturePanel1.TabIndex = 2;
            this.multiLayerPicturePanel1.TabStop = false;
            this.multiLayerPicturePanel1.DoubleClick += new System.EventHandler(this.multiLayerPicturePanel1_DoubleClick);
            // 
            // hRulerPanel1
            // 
            this.hRulerPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hRulerPanel1.BackColor = System.Drawing.Color.Aquamarine;
            this.hRulerPanel1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.hRulerPanel1.Location = new System.Drawing.Point(28, 0);
            this.hRulerPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.hRulerPanel1.Name = "hRulerPanel1";
            this.hRulerPanel1.Size = new System.Drawing.Size(256, 34);
            this.hRulerPanel1.TabIndex = 1;
            // 
            // vRulerPanel1
            // 
            this.vRulerPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.vRulerPanel1.BackColor = System.Drawing.Color.Aquamarine;
            this.vRulerPanel1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.vRulerPanel1.Interval = 2;
            this.vRulerPanel1.Location = new System.Drawing.Point(0, 0);
            this.vRulerPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.vRulerPanel1.Name = "vRulerPanel1";
            this.vRulerPanel1.Size = new System.Drawing.Size(29, 246);
            this.vRulerPanel1.StartY = 34;
            this.vRulerPanel1.TabIndex = 0;
            // 
            // RulerPicturePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.multiLayerPicturePanel1);
            this.Controls.Add(this.hRulerPanel1);
            this.Controls.Add(this.vRulerPanel1);
            this.Name = "RulerPicturePanel";
            this.Size = new System.Drawing.Size(284, 246);
            this.SizeChanged += new System.EventHandler(this.RulerPicturePanel_SizeChanged);
            this.ParentChanged += new System.EventHandler(this.RulerPicturePanel_ParentChanged);
            ((System.ComponentModel.ISupportInitialize)(this.multiLayerPicturePanel1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private VRulerPanel vRulerPanel1;
        private HRulerPanel hRulerPanel1;
        private TextLayerBeta.MultiLayerPicturePanel multiLayerPicturePanel1;
    }
}
