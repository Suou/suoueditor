﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Controls.TextLayerBeta;
using SuouBase.Text;
using SuouBase.Book;
namespace SuouTextBox2.Controls.TextLayerBeta.RulerPicturePanel
{
    public partial class RulerPicturePanel : UserControl,IMultiLayerPanel
    {
        public RulerPicturePanel()
        {
            InitializeComponent();
            this.vRulerPanel1.StartY = this.hRulerPanel1.Height;
        }
        /*
        public new System.Windows.Forms.Control.ControlCollection Controls
        {
            get { return this.multiLayerPicturePanel1.Controls; }
        }
        */
        public void AddPanel(Control c)
        {
            this.multiLayerPicturePanel1.Controls.Add(c);
        }
        public Point TranslateEditPoint(EditPoint p)
        {
            var x = p.X;
            var y = (p.Y * (SuouBase.SuouEditorConst.AAFont.Height + SuouBase.SuouEditorConst.AAFontYInterval));
            return new Point(x, y);
        }
        public EditPoint TranslatePoint(Point p)
        {
            var x = p.X;
            var y = (p.Y) / (SuouBase.SuouEditorConst.AAFont.Height + SuouBase.SuouEditorConst.AAFontYInterval);
            var r = new EditPoint(x, y);
            return r;
        }
        public void UpdateImage()
        {
            this.multiLayerPicturePanel1.UpdateImage();
        }
        public IBookPage Page
        {
            get
            {
                return this.multiLayerPicturePanel1.Page;
            }
            set
            {
                this.multiLayerPicturePanel1.Page = value;
            }
        }
        //Size Size { get; set; }
        public void SetPageSize()
        {
        }

        private void RulerPicturePanel_SizeChanged(object sender, EventArgs e)
        {
            //this.vRulerPanel1.Height = this.Height;
            //this.hRulerPanel1.Width = this.Width - this.vRulerPanel1.Width;
        }

        private Control NowParent { get; set; }
        private Size FirstSize
        {
            get
            {
                var w = this.vRulerPanel1.Width + this.multiLayerPicturePanel1.PageSize.Width;
                var h = this.hRulerPanel1.Height + this.multiLayerPicturePanel1.PageSize.Height;
                return new Size(w, h);
            }
        }
        private void Parent_SizeChanged(object sender, EventArgs e)
        {
            this.MinimumSize = this.NowParent.Size;
            if (this.MinimumSize.Width < this.Width)
            {
                this.Width = this.FirstSize.Width;
            }
            if (this.MinimumSize.Height < this.Height)
            {
                this.Height = this.FirstSize.Height;
            }

        }

        private void RulerPicturePanel_ParentChanged(object sender, EventArgs e)
        {
            this.NowParent = this.Parent;
            if (this.NowParent != null) this.NowParent.SizeChanged += this.Parent_SizeChanged;
        }

        private void multiLayerPicturePanel1_DoubleClick(object sender, EventArgs e)
        {
            this.OnDoubleClick(e);
        }
    }
}
