﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase.Plugin;
namespace SuouTextBox2.Controls
{
    public partial class AltCharControl : UserControl
    {
        public string ItemName { get; set; }
        public bool IsRegister
        {
            get
            {
                return this.RegisterPanel.Visible;
            }
            set
            {
                this.RegisterPanel.Visible = value;
            }
        }
        public bool IsEnableExpansion
        {
            get { return this.groupBox2.Visible; }
            set { this.groupBox2.Visible = value; }
        }
        public string ControlName
        {
            get { return this.groupBox1.Text; }
            set{this.groupBox1.Text = value;}
        }

        public AltCharControl()
        {
            InitializeComponent();
            this.listPanel1.EmptyCreateItem = new AltCharItem();
        }
        public AltCharControl(MergeAssistCollection AltHukidasiSet):base()
        {
            this.SaveItems = AltHukidasiSet;
        }
        public bool AddOptionVisible
        {
            get { return this.RegisterPanel.Visible; }
            set { this.RegisterPanel.Visible = value; }
        }
        private MergeAssistCollection _SaveItems;
        public MergeAssistCollection SaveItems
        {
            get { return _SaveItems; }
            set
            {
                _SaveItems = value;
                if (_SaveItems != null)
                {
                    SetAltNameList();
                }
            }
        }

        private SpacePriorityType PriorityType
        {
            get
            {
                if (this.radioButton1.Checked) return SpacePriorityType.EucByte;
                if (this.radioButton1.Checked) return SpacePriorityType.Clean;
                return SpacePriorityType.Clean;
            }
            set
            {
                if (value == SpacePriorityType.EucByte) this.radioButton1.Checked = true;
                if (value == SpacePriorityType.Clean) this.radioButton2.Checked = true;
            }
        }

        private void SetAltNameList()
        {
            //this.comboBox1.Items.Clear();
            comboBox1.DataSource = SaveItems.AltSettingSaveDataList.ToList();
            
            comboBox1.DisplayMember = "Name";
            comboBox1.ValueMember = "Name";
        }
        public SpaceLineSet Item
        {
            get { return this.GetAltCharSetting(); }
            set
            {
                List<IListPanelItem> flist = new List<IListPanelItem>();
                
                var v = value;
                if (v != null)
                {
                    ItemName = v.Name;
                    if (v == null) throw new SuouBase.MyException("AltSettingItemが存在しません");
                    if (v.Items == null) throw new SuouBase.MyException("AltSettingItem.Itemsが存在しません");
                    foreach (var k in v.Items)
                    {
                        flist.Add(new AltCharItem(k));
                    }
                    this.PriorityType = v.PriorityType;
                    this.PermitRenzokuDirty = v.PermitDirtyLength;
                    this.MinCalculate = v.MinCaliculate;
                    this.listPanel1.Items = flist;
                    if (this.SaveItems != null)
                    {
                        for (int i = 0; i < this.SaveItems.AltSettingSaveDataList.Count(); i++)
                        {
                            if (value.Name == this.SaveItems.AltSettingSaveDataList[i].Name)
                            {
                                this.comboBox1.SelectedIndex = i;
                            }
                        }
                    }
                }

            }
        }
        private List<IListPanelItem> Items
        {
            get
            {
                return this.listPanel1.Items;
            }
            set
            {
                this.listPanel1.Items = value;
            }
        }
        public int PermitRenzokuDirty
        {
            get
            {
                return (int)this.numericUpDown1.Value;
            }
            set
            {
                this.numericUpDown1.Value = value;
            }
        }
        public int MinCalculate
        {
            get { return (int)this.numericUpDown2.Value; }
            set { this.numericUpDown2.Value = value; }
        }
        public SpaceLineSet GetAltCharSetting()
        {
            var items = this.listPanel1.Items;
            if (items == null) return null;
            
            List<SpaceItem> list = new List<SpaceItem>();
            foreach (var v in items)
            {
                var f = (AltCharItem)v;
                list.Add(f.Item);
            }

            var r = new SpaceLineSet(300,this.PermitRenzokuDirty,this.PriorityType,list,this.ItemName);
            
            return r;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Item = ((SpaceLineSet)this.comboBox1.SelectedItem);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!this.Item.Check())
            {
                MessageBox.Show("要素に問題があります。このアルゴリズムでは空白を作成できません");
            }
            else
            {

                var c = new InputNameForm();
                c.ShowDialog();
                if (c.DialogResult == DialogResult.OK)
                {
                    var f = this.Item;
                    var p = new SpaceLineSet(f.MinCaliculate, f.PermitDirtyLength, f.PriorityType, f.Items, c.InputText.Text);

                    this.SaveItems.AltSettingSaveDataList.Add(p);
                    
                }
                else
                {
                }
            }
        }
    }
}
