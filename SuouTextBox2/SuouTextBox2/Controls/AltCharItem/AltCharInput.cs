﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouTextBox2.Controls
{
    public partial class AltCharInput : Form
    {
        public AltCharInput()
        {
            InitializeComponent();
        }
        public event EventHandler OKClick;
        public char CharText { get; set; }
        private void button1_Click(object sender, EventArgs e)
        {
            var s = this.textBox1.Text;
            
            if (s.Length != 1)
            {
                if (s.Length == 0)
                {
                    MessageBox.Show("入力してください。");
                }
                else
                {
                    MessageBox.Show("二文字以上入力することはできません。");
                }
            }
            else
            {
                this.CharText = this.textBox1.Text.First();
                if (OKClick != null) OKClick(this, null);
                this.Close();
            }
        }
    }
}
