﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase.Text;
using SuouBase.Controls;
using BSystem.Drawing;
namespace SuouTextBox2.Controls
{
    public partial class AltCharItem : UserControl,IListPanelItem
    {
        public AltCharItem()
        {
            InitializeComponent();
        }
        public event EventHandler ItemDeleting;
        public SpaceItem Item
        {
            get
            {
                var c = this.textBox1.Text[0];
                var r = new SpaceItem(c);
                return r;
            }
        }
        public Color ItemColor { get; set; }
        public Font ItemFont { get; set; }

        public Control Control { get { return this; } }
        public AltCharItem(SpaceItem item)
            : this()
        {
            if (item.SuouWord == null) throw new Exception("Error");
            this.ItemColor = item.SuouWord.Color;
            this.ItemFont = item.SuouWord.Font;
            this.textBox1.Text = item.SuouWord.Word;
            //this.checkBox1.Checked = item.PermitFirst;
            //this.numericUpDown1.Value = item.ForbitContinuity;
            
        }
        
        public IListPanelItem Empty()
        {
            var v = new AltCharItem(new SpaceItem('?'));
            return v;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var c = this.textBox1.Text[0];
            var width = this.ItemFont.Width(c);
            this.numericUpDown2.Value = width;
        }
        public AltCharInput InputForm { get; set; }
        private void textBox1_Click(object sender, EventArgs e)
        {
            InputForm = new AltCharInput();
            InputForm.OKClick += this.InputForm_OKClicked;
            InputForm.Show();
        }
        private void InputForm_OKClicked(object sender, EventArgs e)
        {
            this.textBox1.Text = InputForm.CharText.ToString();
            //this.textBox1.Select(0, 0);
            //this.Focus();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (ItemDeleting != null) ItemDeleting(this,new EventArgs());
        }
    }
}
