﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouTextBox2.Controls.Ruler
{
    public partial class VRuler : UserControl
    {
        public int Pos { get; set; }
        public VRuler()
        {
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            InitializeComponent();
            this.VerticalScroll.Minimum = 0;
            
        }
        public event Action<object, int> RulerLineClicked;
        private List<IRulerLine> _ColorList = null;
        public List<IRulerLine> ColorList
        {
            get { return this._ColorList; }
            set
            {
                this._ColorList = value;
                this.UpdatePaint(this.CreateGraphics());
            }
        }
        private int Interval = 2;
        private SuouTextBox _ParentTextBox = null;
        public SuouTextBox ParentTextBox
        {
            get { return this._ParentTextBox; }
            set
            {
                this._ParentTextBox = value;
                if (value != null)
                {
                    this.ParentTextBox.VsbValueChanged += this.TextBox_ValueChanged;
                    this.ParentTextBox.TextChanged += this.TextBox_TextChanged;
                    this.ParentTextBox.CaretMoved += this.TextBox_CaretMoved;
                }
                this.UpdateSetting();
            }
        }
        private void TextBox_CaretMoved(object sender, EventArgs e)
        {
            var g = this.CreateGraphics();
            this.UpdatePaint(g);
        }
        private void TextBox_TextChanged(object sender, EventArgs e)
        {
        }
        private void TextBox_ValueChanged(object sender,EventArgs e)
        {
            this.Refresh();
        }
        private void UpdateSetting()
        {
            if (this.ParentTextBox == null) return;            
            this.Font = this.ParentTextBox.Font;
            this.VerticalScroll.Maximum = this.ParentTextBox.RawText.Height;
        }
        private int FontInterval
        {
            get { return this.Font.Height + this.Interval; }
        }
        private int ParentTextBoxVScrollValue
        {
            get
            {
                if (this.ParentTextBox == null)
                {
                    return 0;
                }
                else
                {
                    return this.ParentTextBox.DisplaySet.Vsb.Value;
                }
            }
        }
        private int LocationY
        {
            get
            {            
                var top = this.ParentTextBoxVScrollValue % this.FontInterval;
                //int h = this.ParentTextBoxVScrollValue / this.FontInterval;
                return -top;
            }
        }
        private int StringPosY
        {
            get
            {
                return this.ParentTextBoxVScrollValue / this.FontInterval;
            }
        }
        public int GetNthLine(int x, int y)
        {
            var p = this.ParentTextBox.PointMaker.CreateExDisplayPoint(x, y);
            return ((Base.ExStringPoint)p).Y;
        }
        private void PaintLine(Graphics g, int t, Color backColor, Color penColor)
        {
            if (this.ParentTextBox != null)
            {
                if (t >= this.ParentTextBox.RawText.Count) return;
            }
            var loc = new Point(0, this.LocationY + (t - this.StringPosY) * this.FontInterval);
            var brush = new SolidBrush(backColor);
            g.FillRectangle(brush, new Rectangle(0, loc.Y, this.Width, this.FontInterval));

            var pen = new SolidBrush(penColor);

            g.DrawString((t+1).ToString(), this.Font, pen, 0,loc.Y + 2);
        }
        /*
        private void CaretPaintLine(Graphics g)
        {
            var t = this.CaretPos.Y;
            var backColor = this.GetColorLine(t);
            var loc = new Point(0, this.LocationY + (t - this.StringPosY) * this.FontInterval);
            var brush = new SolidBrush(backColor);
            g.FillRectangle(brush, new Rectangle(0, loc.Y, this.Width, this.FontInterval));
            g.DrawString((t + 1).ToString(), this.Font, Brushes.Blue, 0, loc.Y + 2);
        }
        */
        private Point CaretPos
        {
            get
            {
                if (this.ParentTextBox == null)
                {
                    return Point.Empty;
                }
                else
                {                    
                    return new Point(this.ParentTextBox.Caret.StringPos.X,this.ParentTextBox.Caret.StringPos.Y);
                }
            }
        }
        private void UpdatePaint(Graphics g)
        {
            int t = 0;
            if (this.ParentTextBox != null)
            {
                t = this.ParentTextBoxVScrollValue / this.FontInterval;
            }
            g.Clear(this.BackColor);
            int height = this.ParentTextBox == null ? 0 : this.ParentTextBox.RawText.Count;
            while (t < height)
            {
                this.PaintLine(g, t,this.GetColorLine(t),Color.Black);
                t++;
            }
            this.PaintLine(g, this.CaretPos.Y, this.GetColorLine(this.CaretPos.Y), Color.Blue);

        }
        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            this.UpdatePaint(e.Graphics);

        }
        private Color GetColorLine(int t)
        {
            if (this.ColorList == null)
            {
                return this.BackColor;
            }
            else
            {
                if (this.ColorList.Count <= t)
                {
                    return this.BackColor;
                }

                return this.ColorList[t].BackColor;
            }
        }

        private void VRuler_MouseMove(object sender, MouseEventArgs e)
        {
            // This example assumes the existence of a form called Form1.
            BufferedGraphicsContext currentContext;
            BufferedGraphics myBuffer;
            // Gets a reference to the current BufferedGraphicsContext
            currentContext = BufferedGraphicsManager.Current;
            // Creates a BufferedGraphics instance associated with Form1, and with 
            // dimensions the same size as the drawing surface of Form1.
            myBuffer = currentContext.Allocate(this.CreateGraphics(),
               this.DisplayRectangle);
            using (myBuffer)
            {
                var t = this.MouseOnLine(e);
                this.UpdatePaint(myBuffer.Graphics);
                if (t != -1)
                {
                    this.PaintLine(myBuffer.Graphics, t, Color.Yellow, Color.Black);
                }
                myBuffer.Render();
            }
        }
        public int GetLocationY(int y)
        {
            return this.LocationY + y;
        }
        private int MouseOnLine(MouseEventArgs e)
        {
            
            if (e.X < 0 && e.X > this.Width)
            {
                return -1;
            }
            else
            {
                return this.GetLinePos(e.Y);
            }
        }
        private void VRuler_MouseDown(object sender, MouseEventArgs e)
        {
            if (this.RulerLineClicked != null)
            {
                this.RulerLineClicked(this, this.GetNthLine(e.X, e.Y));
            }
        }
        private int GetLinePos(int y)
        {
            var p = y / this.FontInterval;
            return this.LocationY + p;
        }
    }
    public interface IRulerLine
    {
        Color BackColor { get; }
        Color TextColor { get; }
    }
}
