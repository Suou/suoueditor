﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase;
using SuouBase.Book;
namespace SuouTextBox2.Controls
{
    public delegate void SelectedLayerItemEventHandler(object sender, SelectedLayerItemEventArgs e);
    public class SelectedLayerItemEventArgs : EventArgs
    {
        public LayerItem Item { get; set; }
        public SelectedLayerItemEventArgs(LayerItem item)
            : base()
        {
            this.Item = item;
        }
    }
    public delegate void MergeBookPageChangedEventHandler(object sender, RenewMergePageEventArgs e);
    public class RenewMergePageEventArgs : EventArgs
    {
        public SectionScenePart OldItem { get; private set; }
        public SectionScenePart NewItem { get; private set; }
        public RenewMergePageEventArgs(SectionScenePart old, SectionScenePart newitem)
        {
            this.OldItem = old;
            this.NewItem = newitem;
        }
    }
}
