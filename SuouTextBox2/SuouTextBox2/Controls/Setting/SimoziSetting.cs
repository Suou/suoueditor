﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouTextBox2.Base;
using SuouBase.Text;
using SuouBase.Plugin;
using SuouBase;
namespace SuouTextBox2.Controls
{
    public partial class SimoziSetting : UserControl,SettingPage
    {
        public SimoziSetting()
        {
            InitializeComponent();
        }
        public SimoziSetting(MergeAssistCollection alt,SimoziBox parent):this()
        {
            this.textSpaceSetting.SaveItems = alt;
            this.serihualtCharControl2.SaveItems = alt;
            this.layeraltCharControl3.SaveItems =alt;
            this.ParentTextBox = parent;
            
        }
        public EncodeType CopyEncodeType
        {
            get
            {
                if (this.copySjis16RadioButton.Checked)
                {
                    return EncodeType.SJIS16;
                }
                else if (this.copySjis10RadioButton.Checked)
                {
                    return EncodeType.SJIS10;
                }
                else
                {
                    return EncodeType.UTF8;
                }
            }
            set
            {
                if (value == EncodeType.SJIS16)
                {
                    this.copySjis16RadioButton.Checked = true;
                }
                else if (value == EncodeType.SJIS10)
                {
                    this.copySjis10RadioButton.Checked = true;
                }
                else
                {
                    this.copyUtf8RadioButton.Checked = true;
                }
            }
        }
        public int MergePase
        {
            get
            {
                /*
                var high = this.radioButton1.Checked;
                var middle = this.radioButton2.Checked;
                var low = this.radioButton4.Checked;
                if (high) return 50;
                if (middle) return 400;
                if (low) return 1000;
                */
                
                return 400;
            }
            /*
            set
            {
                
                if (value <= 50)
                {
                    this.radioButton1.Checked = true;
                }
                else if (50 < value && value <= 400)
                {
                    this.radioButton2.Checked = true;
                }
                else
                {
                    this.radioButton4.Checked = true;
                }
                
            }
            */
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {

        }
        private SimoziBox ParentTextBox { get; set; }
        
        public Control Ctrl { get { return this; } }

        private void button1_Click(object sender, EventArgs e)
        {
            OK();
        }
        public bool IsDragEndMerge
        {
            get
            {
                return this.DragMergeRadio.Checked;
            }
        }
        public void Set(SimoziBoxSaveData sb)
        {
            if (sb.IsDragEndMerge)
            {
                this.DragMergeRadio.Checked = true;
            }
            else
            {
                //this.MergePase = sb.MergePase;
            }
            this.textSpaceSetting.Item = sb.MergeAlgorithm;
            this.serihualtCharControl2.Item = sb.SerihuAlgorithm;
            this.layeraltCharControl3.Item = sb.LayerAlgorithm;
            this.CopyEncodeType = sb.SuouTextSetting.CopyEncodeType;
            
        }
        public void OK()
        {
            if (this.ParentTextBox != null)
            {
                this.ParentTextBox.SetSaveData(this.SaveData);
                /*
                var f = this.ParentTextBox.SaveData;
                f.MergePase = this.MergePase;
                f.IsDragEndMerge = this.IsDragEndMerge;
                f.SuouTextSetting.CopyEncodeType = this.CopyEncodeType;
                this.ParentTextBox.TextBox.SpaceMaker = new SuouBase.Text.SpaceMaker(this.altCharControl1.Item,SuouBase.SuouEditorConst.AAFont,SuouBase.SuouEditorConst.TextColor);
                this.ParentTextBox.SaveData = f;
                */
            }
        }
        public SpaceMaker SpaceMaker
        {
            get
            {
                return new SuouBase.Text.SpaceMaker(this.textSpaceSetting.Item, SuouBase.SuouEditorConst.AAFont, SuouBase.SuouEditorConst.TextColor);
            }
        }
        SimoziBoxSaveData SaveData
        {
            get
            {
                var r = new SimoziBoxSaveData();
                r.MergePase = this.MergePase;
                r.IsDragEndMerge = this.IsDragEndMerge;
                r.SuouTextSetting.CopyEncodeType = this.CopyEncodeType;
                r.MergeAlgorithm = this.textSpaceSetting.Item;
                r.SerihuAlgorithm = this.serihualtCharControl2.Item;
                r.LayerAlgorithm = this.layeraltCharControl3.Item;
                return r;

            }
        }
        public void Cancel()
        {
        }
        public string PageName { get { return "テキストボックス"; } }
    }
}
