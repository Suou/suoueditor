﻿namespace SuouTextBox2.Controls
{
    partial class SimoziSetting
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.copySjis10RadioButton = new System.Windows.Forms.RadioButton();
            this.copySjis16RadioButton = new System.Windows.Forms.RadioButton();
            this.copyUtf8RadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DragMergeRadio = new System.Windows.Forms.RadioButton();
            this.textSpaceSetting = new SuouTextBox2.Controls.AltCharControl();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.serihualtCharControl2 = new SuouTextBox2.Controls.AltCharControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.layeraltCharControl3 = new SuouTextBox2.Controls.AltCharControl();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(4, 4);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox1.Size = new System.Drawing.Size(707, 143);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "設定";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.copySjis10RadioButton);
            this.groupBox3.Controls.Add(this.copySjis16RadioButton);
            this.groupBox3.Controls.Add(this.copyUtf8RadioButton);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(4, 81);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox3.Size = new System.Drawing.Size(699, 58);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "コピー";
            // 
            // copySjis10RadioButton
            // 
            this.copySjis10RadioButton.AutoSize = true;
            this.copySjis10RadioButton.Location = new System.Drawing.Point(319, 22);
            this.copySjis10RadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.copySjis10RadioButton.Name = "copySjis10RadioButton";
            this.copySjis10RadioButton.Size = new System.Drawing.Size(114, 19);
            this.copySjis10RadioButton.TabIndex = 2;
            this.copySjis10RadioButton.TabStop = true;
            this.copySjis10RadioButton.Text = "SJIS(10進数)";
            this.copySjis10RadioButton.UseVisualStyleBackColor = true;
            // 
            // copySjis16RadioButton
            // 
            this.copySjis16RadioButton.AutoSize = true;
            this.copySjis16RadioButton.Location = new System.Drawing.Point(163, 22);
            this.copySjis16RadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.copySjis16RadioButton.Name = "copySjis16RadioButton";
            this.copySjis16RadioButton.Size = new System.Drawing.Size(114, 19);
            this.copySjis16RadioButton.TabIndex = 1;
            this.copySjis16RadioButton.TabStop = true;
            this.copySjis16RadioButton.Text = "SJIS(16進数)";
            this.copySjis16RadioButton.UseVisualStyleBackColor = true;
            // 
            // copyUtf8RadioButton
            // 
            this.copyUtf8RadioButton.AutoSize = true;
            this.copyUtf8RadioButton.Location = new System.Drawing.Point(32, 22);
            this.copyUtf8RadioButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.copyUtf8RadioButton.Name = "copyUtf8RadioButton";
            this.copyUtf8RadioButton.Size = new System.Drawing.Size(71, 19);
            this.copyUtf8RadioButton.TabIndex = 0;
            this.copyUtf8RadioButton.TabStop = true;
            this.copyUtf8RadioButton.Text = "UTF-8";
            this.copyUtf8RadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DragMergeRadio);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(4, 19);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBox2.Size = new System.Drawing.Size(699, 62);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "合成頻度";
            // 
            // DragMergeRadio
            // 
            this.DragMergeRadio.AutoSize = true;
            this.DragMergeRadio.Location = new System.Drawing.Point(409, 24);
            this.DragMergeRadio.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DragMergeRadio.Name = "DragMergeRadio";
            this.DragMergeRadio.Size = new System.Drawing.Size(128, 19);
            this.DragMergeRadio.TabIndex = 2;
            this.DragMergeRadio.TabStop = true;
            this.DragMergeRadio.Text = "移動終了時(軽)";
            this.DragMergeRadio.UseVisualStyleBackColor = true;
            // 
            // textSpaceSetting
            // 
            this.textSpaceSetting.AddOptionVisible = false;
            this.textSpaceSetting.AutoSize = true;
            this.textSpaceSetting.ControlName = "空白合成アルゴリズム設定（テキストボックス）";
            this.textSpaceSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.textSpaceSetting.IsEnableExpansion = false;
            this.textSpaceSetting.IsRegister = false;
            this.textSpaceSetting.Item = null;
            this.textSpaceSetting.ItemName = null;
            this.textSpaceSetting.Location = new System.Drawing.Point(4, 4);
            this.textSpaceSetting.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textSpaceSetting.MinCalculate = 0;
            this.textSpaceSetting.Name = "textSpaceSetting";
            this.textSpaceSetting.PermitRenzokuDirty = 0;
            this.textSpaceSetting.SaveItems = null;
            this.textSpaceSetting.Size = new System.Drawing.Size(707, 140);
            this.textSpaceSetting.TabIndex = 2;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(723, 502);
            this.tabControl1.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage1.Size = new System.Drawing.Size(715, 473);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "基本設定";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textSpaceSetting);
            this.tabPage2.Location = new System.Drawing.Point(4, 25);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage2.Size = new System.Drawing.Size(715, 473);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "空白1";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.serihualtCharControl2);
            this.tabPage3.Location = new System.Drawing.Point(4, 25);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(715, 473);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "空白2";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // serihualtCharControl2
            // 
            this.serihualtCharControl2.AddOptionVisible = false;
            this.serihualtCharControl2.AutoSize = true;
            this.serihualtCharControl2.ControlName = "空白合成アルゴリズム設定(セリフ)";
            this.serihualtCharControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.serihualtCharControl2.IsEnableExpansion = false;
            this.serihualtCharControl2.IsRegister = false;
            this.serihualtCharControl2.Item = null;
            this.serihualtCharControl2.ItemName = null;
            this.serihualtCharControl2.Location = new System.Drawing.Point(0, 0);
            this.serihualtCharControl2.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.serihualtCharControl2.MinCalculate = 0;
            this.serihualtCharControl2.Name = "serihualtCharControl2";
            this.serihualtCharControl2.PermitRenzokuDirty = 0;
            this.serihualtCharControl2.SaveItems = null;
            this.serihualtCharControl2.Size = new System.Drawing.Size(715, 140);
            this.serihualtCharControl2.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.layeraltCharControl3);
            this.tabPage4.Location = new System.Drawing.Point(4, 25);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(715, 473);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "空白3";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // layeraltCharControl3
            // 
            this.layeraltCharControl3.AddOptionVisible = false;
            this.layeraltCharControl3.AutoSize = true;
            this.layeraltCharControl3.ControlName = "空白合成アルゴリズム設定(レイヤー)";
            this.layeraltCharControl3.Dock = System.Windows.Forms.DockStyle.Top;
            this.layeraltCharControl3.IsEnableExpansion = false;
            this.layeraltCharControl3.IsRegister = false;
            this.layeraltCharControl3.Item = null;
            this.layeraltCharControl3.ItemName = null;
            this.layeraltCharControl3.Location = new System.Drawing.Point(0, 0);
            this.layeraltCharControl3.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.layeraltCharControl3.MinCalculate = 0;
            this.layeraltCharControl3.Name = "layeraltCharControl3";
            this.layeraltCharControl3.PermitRenzokuDirty = 0;
            this.layeraltCharControl3.SaveItems = null;
            this.layeraltCharControl3.Size = new System.Drawing.Size(715, 140);
            this.layeraltCharControl3.TabIndex = 0;
            // 
            // SimoziSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabControl1);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "SimoziSetting";
            this.Size = new System.Drawing.Size(723, 502);
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton DragMergeRadio;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton copySjis16RadioButton;
        private System.Windows.Forms.RadioButton copyUtf8RadioButton;
        private System.Windows.Forms.RadioButton copySjis10RadioButton;
        private AltCharControl textSpaceSetting;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private AltCharControl serihualtCharControl2;
        private System.Windows.Forms.TabPage tabPage4;
        private AltCharControl layeraltCharControl3;
    }
}
