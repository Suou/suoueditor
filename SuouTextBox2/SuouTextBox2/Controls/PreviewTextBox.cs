﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
namespace SuouTextBox2.Controls
{
    public partial class PreviewTextBox : UserControl
    {
        public PreviewTextBox()
        {
            InitializeComponent();
        }
        [ReadOnly(true)]
        public SuouText SText
        {
            get { return this.suouTextBox1.RawText; }
            set { this.suouTextBox1.SetRawText(value); }
        }

        private void suouTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if(this.ContextMenuStrip!=null)this.ContextMenuStrip.Show(this.PointToScreen(e.Location));
            }
        }
    }
}
