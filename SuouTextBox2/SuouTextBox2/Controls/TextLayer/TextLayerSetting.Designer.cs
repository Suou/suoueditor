﻿namespace SuouTextBox2.Controls
{
    partial class TextLayerSetting
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.altCharControl1 = new SuouTextBox2.Controls.AltCharControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.hukidasiSetting1 = new SuouTextBox2.Controls.HukidasiSetting();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.textLayerProperty1 = new SuouTextBox2.Controls.TextLayerProperty();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "セリフ",
            "その他"});
            this.comboBox1.Location = new System.Drawing.Point(125, 20);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(451, 6);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "変更";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OKButton_Click);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(546, 6);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "キャンセル";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(619, 46);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "レイヤー設定";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "レイヤー属性設定";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 393);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(633, 38);
            this.panel1.TabIndex = 7;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(633, 393);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.altCharControl1);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(625, 367);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "設定・合成";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // altCharControl1
            // 
            this.altCharControl1.AddOptionVisible = false;
            this.altCharControl1.AutoSize = true;
            this.altCharControl1.ControlName = "空白合成アルゴリズム設定(レイヤー)";
            this.altCharControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.altCharControl1.IsEnableExpansion = false;
            this.altCharControl1.IsRegister = false;
            this.altCharControl1.Item = null;
            this.altCharControl1.ItemName = null;
            this.altCharControl1.Location = new System.Drawing.Point(3, 49);
            this.altCharControl1.MinCalculate = 0;
            this.altCharControl1.Name = "altCharControl1";
            this.altCharControl1.PermitRenzokuDirty = 0;
            this.altCharControl1.SaveItems = null;
            this.altCharControl1.Size = new System.Drawing.Size(619, 112);
            this.altCharControl1.TabIndex = 5;
            // 
            // tabPage2
            // 
            this.tabPage2.AutoScroll = true;
            this.tabPage2.Controls.Add(this.hukidasiSetting1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(625, 367);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "フキダシ";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // hukidasiSetting1
            // 
            this.hukidasiSetting1.AutoScroll = true;
            this.hukidasiSetting1.AutoSize = true;
            this.hukidasiSetting1.DLLFunction = null;
            this.hukidasiSetting1.IsTrimSpace = false;
            this.hukidasiSetting1.Location = new System.Drawing.Point(3, 10);
            this.hukidasiSetting1.MergeAlgorithm = null;
            this.hukidasiSetting1.Name = "hukidasiSetting1";
            this.hukidasiSetting1.PermitZureLength = 0;
            this.hukidasiSetting1.PrePermitZureLength = 0;
            this.hukidasiSetting1.Size = new System.Drawing.Size(619, 361);
            this.hukidasiSetting1.TabIndex = 4;
            this.hukidasiSetting1.UpCenterBottomZure = 0;
            this.hukidasiSetting1.HukidasiTypeChanged += new System.EventHandler(this.hukidasiSetting1_HukidasiTypeChanged);
            this.hukidasiSetting1.Load += new System.EventHandler(this.hukidasiSetting1_Load);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.textLayerProperty1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(625, 367);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "情報";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // textLayerProperty1
            // 
            this.textLayerProperty1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textLayerProperty1.Item = null;
            this.textLayerProperty1.Location = new System.Drawing.Point(3, 3);
            this.textLayerProperty1.Name = "textLayerProperty1";
            this.textLayerProperty1.Size = new System.Drawing.Size(619, 361);
            this.textLayerProperty1.TabIndex = 0;
            // 
            // TextLayerSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(633, 431);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "TextLayerSetting";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "テキストレイヤー設定";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TextLayerSetting_FormClosing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private HukidasiSetting hukidasiSetting1;
        private AltCharControl altCharControl1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private TextLayerProperty textLayerProperty1;
    }
}