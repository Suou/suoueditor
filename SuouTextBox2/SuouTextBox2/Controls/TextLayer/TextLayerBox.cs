﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Interface;
using SuouBase;
using System.Drawing.Drawing2D;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
using SuouTextBox2.Controls.TextLayer;
using SuouBase.Book;
using System.Runtime.Remoting.Messaging;

namespace SuouTextBox2.Controls
{
    public partial class TextLayerBox : UserControl,LayerItem
    {
        #region Constructor
        public TextLayerBox()
        {
            InitializeComponent();
            this.Visible = false;
            
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);

            if (this.DesignMode) return;


            this.Property = new TextLayerBoxProperty(this,this.suouTextBox21,this.textLayerPictureBox1);
            this.Property.LayerOnMouseDown = (e) => this.OnMouseDown(e);
            this.Property.LayerOnEnter = (e) => this.OnEnter(e);
            

            //this.TextPictureBox.ContextMenuStrip = this.contextMenuStrip1;
            this.suouTextBox21.TextChanged += this.TextBox_TextChanged;
            
            SwitchPicture();
            this.BackgroundPage = new SectionScenePart(HukidasiSaveData.NoneDefault(), SpaceLineSet.DefaultSet);
            this.BackgroundPage.ContentChanged += this.PagePartsItem_ContentChanged;
            this.Property.NeedMerge = true;

            this.Property.PopUpBox = this.toolTip1;
            
        }
        public SectionScene Page
        {
            get
            {
                return this.Property.ParentTextBox.MergeLayerItemCollection.Scene;
            }
        }
        public bool IsPictureInnerMouse
        {
            get { return this.textLayerPictureBox1.IsInnerMouse; }
            set { this.textLayerPictureBox1.IsInnerMouse = value; }
        }
        private int minimumWidth = 100;
        private int minimumHeight = 100;


        public TextLayerBox(SectionScenePart pageparts,SimoziBox ctrl):this(ctrl)
        {

            this.BackgroundPage = pageparts;
            this.Property.ShortCutKeys = ctrl.ShortCutKeys;
            this.Property.DLLFunction = ctrl.DLLFunction;
            this.EditLocation = pageparts.EditLocation;
            this.Size = this.Property.DisplaySize;
            //this.PagePartsItem.EnableChanged += this.PagePart_EnableChanged;
            this.BackgroundPage.ContentChanged += this.PagePartsItem_ContentChanged;
            this.Type = LayerViewType.Normal;
            this.HukidasiContext = new SuouBase.Controls.HukidasiContext(this.BackgroundPage);
            //SuouBase.Controls.SectionPageContext.CreatePageToolMenuItem(pageparts, this.layerContextMenuStrip);
            this.toolStringMenuItemA = CreateA(pageparts);
            detailToolStripMenuItem.DropDownItems.Add(this.toolStringMenuItemA);
            this.toolStringMenuItemB = CreateB(pageparts);

            detailToolStripMenuItem.DropDownItems.Add(this.toolStringMenuItemB );
            this.isPermitOutsideToolStripMenuItem.Enabled = false;

            this.layerContextMenuStrip.Items.Add(this.HukidasiContext);
            //this.contextMenuStrip1.Opening += (a, b) => this.HukidasiContext.Enabled = this.BackgroundPage.Waku.Type != HukidasiType.Nothing;
            //this.contextMenuStrip1.Opening += (a, b) => this.HukidasiContext.Enabled = this.BackgroundPage.Waku.Type != HukidasiType.Nothing;

            if (this.EditLocation.X < 0 || this.EditLocation.Y < 0) this.Property.IsPermitOutSide = true;

        }
        private SuouBase.Controls.HukidasiContext HukidasiContext { get; set; }
        public TextLayerBox(SimoziBox ctrl)
            : this()
        {
            this.Property.ParentTextBox = ctrl;
            //this.Property.ParentTextBox.TextUpdated += this.ParentBox_TextUpdated;
            this.TextBox.SetSpaceSet(ctrl.SaveData.MergeAlgorithm);
            /*
            this.SpaceMaker = SpaceMakerDic.Create(this.PagePartsItem.Merger,this.suouTextBox21.PrintFontSet.Font,this.suouTextBox21.PrintFontSet.TextColor);
            this.AltTexer = new AltText(this.SpaceMaker);
            */
        }


        #endregion
        #region Property

        private ToolStripMenuItem toolStringMenuItemA;
        private ToolStripMenuItem toolStringMenuItemB;

        public bool CanMove
        {
            get { return this.Property.CanMove; }
            set { this.Property.CanMove = value; }
        }
        //public event EventHandler DragMoveModeChanged;

        private bool _IsDragMoveMode = false;
        public bool IsDragMoveMode
        {
            get { return this._IsDragMoveMode; }
            set
            {
                this._IsDragMoveMode = value;
                BSystem.Debug.ColorConsole.IncrementTabCount();
                var sw = System.Diagnostics.Stopwatch.StartNew();
                this.BackgroundPage.Enable = !value;

                if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine("a3/" + sw.ElapsedMilliseconds);
                sw.Restart();

                if (!this._IsDragMoveMode)
                {
                    if (this.Property.ParentTextBox.SaveData.IsDragEndMerge)
                    {
                        this.Property.ParentTextBox.MergeLayerItemCollection.ChangeViewMode();
                    }
                }
                if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine("a4/" + sw.ElapsedMilliseconds);
                sw.Restart();

                //if (this.DragMoveModeChanged != null) this.DragMoveModeChanged(this, new EventArgs());
                if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine("a5/" + sw.ElapsedMilliseconds);
                BSystem.Debug.ColorConsole.DecrementTabCount();

                sw.Stop();

            }
        }
        public event EventHandler LayerTextChanged;
        private LayerViewType _Type = LayerViewType.Normal;
        public LayerViewType Type
        {
            get { return this._Type; }
            set
            {
                this._Type = value;
                this.TextPictureBox.ContextMenuStrip = this.Type == LayerViewType.Normal ? this.layerContextMenuStrip : this.contextMenuStrip2;
            }
        }
        public Point LocationOnSimozi
        {
            get
            {
                //var sz = new Size(this.Property.ParentTextBox.Left, this.Property.ParentTextBox.Top);
                return Point.Subtract(this.Location, Size.Empty);
            }
            set
            {
                var x = value.X;// +this.Property.ParentTextBox.Left;
                var y = value.Y;// +this.Property.ParentTextBox.Top;
                var p = new Point(x, y);
                this.Location = p;
            }
        }
        public bool IsSharrowHide { get; set; }
        public bool IsViewMode
        {
            get { return this.Property.IsViewMode; }
        }
        TextLayerBoxProperty Property { get; set; }
        public event EventHandler EditLocationChanged;
        private event EventHandler ViewModeChanged;
        public SuouTextBox TextBox
        {
            get { return this.Property.EditLayer; }
        }
        private TextLayerSetting SettingForm;
        public static Color SimoziBackColor { get; set; }

        private TextLayerPictureBox TextPictureBox
        {
            get { return this.Property.PictureLayer; }
        }
        public bool NeedMerge
        {
            get { return this.Property.NeedMerge; }
            set { this.Property.NeedMerge = value; }
        }
        public bool EnableMerge
        {
            get { return this.Property.EnableMerge; }
        }

        public EditPoint EditLocation
        {
            get { return BackgroundPage.EditLocation; }
            set
            {
                if (EditLocation != value)
                {
                    this.BackgroundPage.EditLocation = value;
                    //2013/2/14
                    //if (this.EnableMerge)
                    if(true)
                    {
                        NeedMerge = true;
                    }
                    this.Property.IsEditLocationChange = true;
                    this.LocationOnSimozi = this.Property.TranslateLocation(this.EditLocation);
                    if (EditLocationChanged != null) EditLocationChanged(this, new EventArgs());
                    
                }
            }
        }
        public void temporaryChangeLocation(EditPoint point)
        {
            this.LocationOnSimozi = this.Property.TranslateLocation(point);

        }

        private SectionScenePart _PagePartsItem = null;
        public SectionScenePart BackgroundPage
        {
            get
            {
                return _PagePartsItem;
            }
            private set
            {
                _PagePartsItem = value;
            }
        }
        /*
        public int Index
        {
            get
            {
                var n = this.Property.ParentTextBox.PageItems.LayerItems.FindIndex((v) => v == this);
                return n;
            }
        }
        */
        public int PageIndex
        {
            get
            {
                return this.Property.ParentTextBox.MergeLayerItemCollection.Scene.Parts.FindIndex((v) => v == this.BackgroundPage);
            }
        }
        #endregion

        #region Method
        public void CopyLayer()
        {
            Clipboard.SetText(this.BackgroundPage.CreateWakuText().Text.GetEncodeText(EncodeType.UTF8));
            if (this.Property.DLLFunction != null)
            {
                ((SuouTextBoxPlugin)this.Property.DLLFunction.TextBoxPlugin).CopyLayer = this.BackgroundPage.Copy();
            } 
        }
        public void PictureResize()
        {
            this.Size = this.ComputePictureBoxSize();
            this.NeedMerge = true;
            this.Property.ParentTextBox.MergeLayerItemCollection.NeedMergeCheck();
        }

        public bool FirstMode
        {
            get { return this.Controls.Contains(this.TextBox) && this.Controls.Contains(this.textLayerPictureBox1); }
        }
        public void SwitchPicture()
        {
            //if(this.Property.ParentTextBox != null)this.Property.ParentTextBox.UpdateText();
            if (!this.Property.IsViewMode || this.FirstMode)
            {
                if(this.Property.ParentTextBox == null)
                {
                    this.Controls.Clear();
                    this.Controls.Add(this.textLayerPictureBox1);
                    this.textLayerPictureBox1.Dock = DockStyle.Fill;
                }
                else
                {
                    this.Property.ParentTextBox.TextBoxMode = TextBoxMode.PictureModeReady;

                    this.Property.ParentTextBox.MergeLayerItemCollection.NeedMergeCheck();
                    this.Property.ParentTextBox.TextBox.PrintFontSet.DoDefaultBackColor();
                    this.Property.ParentTextBox.UpdateText(true);

                    this.Controls.Clear();
                    this.Controls.Add(this.textLayerPictureBox1);
                    this.textLayerPictureBox1.Dock = DockStyle.Fill;
                    this.Property.ParentTextBox.TextBoxMode = TextBoxMode.Free;
                    this.PictureResize();
                }

                if (this.ViewModeChanged != null) ViewModeChanged(this, new EventArgs());

            }
        }
        public void SwitchTextBox()
        {
            if (this.Property.IsViewMode)
            {
                this.Property.ParentTextBox.TextBoxMode = TextBoxMode.TextBoxEditReady;
                this.minimumWidth = this.TextPictureBox.Width < 100 ? 100 : this.TextPictureBox.Width;
                this.minimumHeight = this.TextPictureBox.Height < 100 ? 100 : this.TextPictureBox.Height;

                //IsViewMode = false;
                this.Controls.Clear();
                this.Controls.Add(this.suouTextBox21);
                if (!this.Property.IsAlreadyInputText)
                {
                    if (this.BackgroundPage != null) this.suouTextBox21.SetRawText(this.BackgroundPage.SText);
                    this.Property.IsAlreadyInputText = true;
                }
                //this.Size = this.suouTextBox21.RawText.Size;
                suouTextBox21.Dock = DockStyle.Fill;
                suouTextBox21.Focus();

                var pen = this.Property.ParentTextBox.PenItem;
                if (pen != null)
                {
                    this.suouTextBox21.Pen = pen.Copy();
                }
                else
                {
                    this.suouTextBox21.Pen = null;
                }

                this.Property.ParentTextBox.TextBox.PrintFontSet.DoUnusedBackColor();
                if (ViewModeChanged != null) ViewModeChanged(this, new EventArgs());
                this.Property.ParentTextBox.TextBoxMode = TextBoxMode.TextBoxEdit;

            }
        }
        public Size ComputeTextBoxSize()
        {
            var height = this.BackgroundPage.WakuTextInfo.TextSize.Height + 10;
            if (height < this.minimumHeight)
            {
                height = this.minimumHeight;
            }
            var width = this.BackgroundPage.WakuTextInfo.TextSize.Width + 10;
            if (width < this.minimumWidth)
            {
                width = this.minimumWidth;
            }
            return new Size(width, height);
        }
        public Size ComputePictureBoxSize()
        {
            var height = this.BackgroundPage.WakuTextInfo.TextSize.Height + 10;
            if (height < 100)
            {
                height = 100;
            }
            var width = this.BackgroundPage.WakuTextInfo.TextSize.Width + 10;
            if (width < 100)
            {
                width = 100;
            }
            return new Size(width, height);
        }

        /*
        public Size OptimalLineMergeSize
        {
            get
            {
            }
        }
        */
        public Size InitialEmptySize
        {
            get { return new Size(100, 100); }
        }
        public Size GetInitialTextSize()
        {
            var v = this.BackgroundPage.Copy();
            var gp = (HukidasiSaveData)v.Waku;
            v.Waku = gp.PaddingClearClone();
            return v.WakuTextInfo.TextSize;
        }

        public void EventDispose()
        {

            this.Property.PictureLayer.EventDispose();
            this.suouTextBox21.TextChanged -= this.TextBox_TextChanged;

            //this.PagePartsItem.EnableChanged -= this.PagePart_EnableChanged;
            this.BackgroundPage.ContentChanged -= this.PagePartsItem_ContentChanged;
            //this.Property.ParentTextBox.TextUpdated -= this.ParentBox_TextUpdated;
        }
        
        public void SharrowHide()
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.None;
        }
        public void SharrowShow()
        {
            this.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.BringToFront();
        }
        
        #endregion

        private HukidasiSaveData Waku
        {
            get { return (HukidasiSaveData)this.BackgroundPage.Waku; }
        }
        

        #region EventMethod
        private void alignmentToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            this.centerToolStripMenuItem.Checked = this.Waku.Arrangement.Align == TextAlign.Center;
            this.leftToolStripMenuItem.Checked = this.Waku.Arrangement.Align == TextAlign.Left;
            this.rightToolStripMenuItem.Checked = this.Waku.Arrangement.Align == TextAlign.Right;

        }


        private void TextLayerBox_VisibleChanged(object sender, EventArgs e)
        {
            this.RefreshPicture();
        }
        private void PagePartsItem_ContentChanged(object sender, SectionBookPagePartContentChangeEventArgs e)
        {
            BSystem.Debug.ColorConsole.IncrementTabCount();
            var sw = System.Diagnostics.Stopwatch.StartNew();

            this.Property.ChangerWaku.IsEnable = this.BackgroundPage.Waku.Type != HukidasiType.Nothing;
            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine($"PagePartsItem_ContentChanged-1: {sw.ElapsedMilliseconds}");

            if (e.Type != MergeContentChangeType.EditLocation)
            {
                this.Property.ParentTextBox.UpdateText();
                this.PictureResize();
            }
            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine($"PagePartsItem_ContentChanged-2: {sw.ElapsedMilliseconds}");

            if (e.Type == MergeContentChangeType.TextDestructive)
            {
                if (this.LayerTextChanged != null) this.LayerTextChanged(this, new EventArgs());
            }
            sw.Stop();
            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine($"PagePartsItem_ContentChanged: {sw.ElapsedMilliseconds}");
            BSystem.Debug.ColorConsole.DecrementTabCount();

        }
        private void ParentBox_TextUpdated(object sender, EventArgs e)
        {
            this.RefreshPicture();
        }
        public void RefreshPicture()
        {
            
            if (this.Visible)
            {
                this.Property.PictureLayer.RefreshPictureBox();
            }
        }
        public static void InverseProc(ref byte b, ref byte g, ref byte r, ref byte a)
        {
            if(b == SimoziBackColor.B && g == SimoziBackColor.G && r == SimoziBackColor.R){
                b += (byte)0;
                g += (byte)122;
                r += (byte)0;
            }else{
            
            }
        }
        private void TextBox_TextChanged(object sender,EventArgs e)
        {
            this.BackgroundPage.SText = this.suouTextBox21.RawText;
            this.NeedMerge = true;
            if (this.Size != this.Property.DisplaySize)
            {
                this.Size = this.Property.DisplaySize;
            }
            
            
        }
        private void TextLayerBox_LocationChanged(object sender, EventArgs e)
        {
            if (this.DesignMode) return;
            if (!this.EnableMerge)
            {
                this.Property.PictureLayer.RefreshPictureBox();
            }
            //this.RefreshPictureBox();
            //this.Property.ParentTextBox.Refresh();
        }
        #endregion
        #region ButtonMethod
        private void mergeToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.Property.ParentTextBox.MergeLayerItemCollection.Scene.BackGround.SText = this.BackgroundPage.Merge(this.Property.ParentTextBox.MergeLayerItemCollection.Scene.BackGround.SText.Clone());
            this.Property.ParentTextBox.MergeLayerItemCollection.Scene.Parts.Remove(this.BackgroundPage);
            //this.Property.ParentTextBox.PageItems.Merge(this);
        }
        private void settingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingForm = new TextLayerSetting(this.BackgroundPage, this.Property.DLLFunction, this);
            //SettingForm.OKClick += this.SettingForm_OKClick;
            SettingForm.ShowDialog();
            if (SettingForm.DialogResult == DialogResult.OK)
            {
                this.PictureResize();
                //TextBox_TextChanged(this, null);
                //this.NeedMerge = true;
            }

        }
        private void leftPasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //var LayerItem = item;
            //if (!this.LayerItems.Contains(LayerItem)) return;
            var page = this.Property.ParentTextBox.MergeLayerItemCollection.Scene;

            var r = page.BackGround.SText.Clone();
            var b = this.BackgroundPage.CreateWakuText();

            var c = AltText.LeftPaste(r, b, this.EditLocation.Y);
            this.Property.ParentTextBox.MergeLayerItemCollection.BackGroundPage.SText = c.Clone();
            this.Property.ParentTextBox.MergeLayerItemCollection.Scene.Parts.Remove(this.BackgroundPage);
            //this.PageItem.Parts.Remove(LayerItem.PagePartsItem);
            //this.DetermineLayerMerge();
            //this.Property.ParentTextBox.PageItems.LeftPaste(this);
        }
        #endregion
        #region SizeChanger
        private void TextLayerBox_Load(object sender, EventArgs e)
        {
            this.Property.CreateChangerWaku();
        }
        #endregion
        #region LayerContext
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            var downb = this.Page.Parts.Count > 2 && this.Page.Parts.Second() != this.BackgroundPage;
            var upb = this.Page.Parts.Last() != this.BackgroundPage;
            this.downMoveToolStripMenuItem.Enabled = downb;
            this.upMoveToolStripMenuItem.Enabled = upb;
            this.isPermitOutsideToolStripMenuItem.Checked = this.Property.IsPermitOutSide;

            var page = this.BackgroundPage;
            if (page.Merger.isPerfect)
            {
                this.toolStringMenuItemB.Enabled = false;
            }
            else
            {
                this.toolStringMenuItemB.Enabled = true;

            }
        }
        private void upMoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.PageIndex;
            this.Property.ParentTextBox.MergeLayerItemCollection.Scene.Parts.Move(n, n + 1);
        }
        private void downMoveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.PageIndex;
            this.Property.ParentTextBox.MergeLayerItemCollection.Scene.Parts.Move(n - 1, n);
        }
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Property.ParentTextBox.MergeLayerItemCollection.Scene.Parts.Remove(this.BackgroundPage);
        }


        private void offEventToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void setWakuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.Property.DLLFunction != null && this.Property.DLLFunction.HukidasiSelecterForm != null)
            {
                var form = this.Property.DLLFunction.HukidasiSelecterForm();
                var f = form.ShowDialog();
                if (f == DialogResult.OK)
                {
                    this.BackgroundPage.Waku = form.SelectedSaveData.Clone();
                    this.BackgroundPage.Merger = DLLFunctionExtension.GetProperSpaceSet(form.SelectedSaveData, this.Property.DLLFunction);
                }
                this.Property.ParentTextBox.UpdateText();
            }
        }

        private void resetPaddingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackgroundPage.Waku = this.Waku.ResetPadding();
        }

        private void copyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.CopyLayer();
        }

        private void clearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.BackgroundPage = new SectionScenePart();
        }

        private void isTrimSpaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var p = (HukidasiBaseSaveData)this.BackgroundPage.Waku.Clone();
            p.Options.IsTrimSpace = !p.Options.IsTrimSpace;

            var f = new HukidasiSaveData(p);
            this.BackgroundPage.Waku = f;

        }

        private void resetPaddingToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            var p = (HukidasiSaveData)this.BackgroundPage.Waku.Clone();
            var np = new EditPoint(this.EditLocation.X + p.Padding.LeftSpace, this.EditLocation.Y + p.Padding.TopSpace);
            p.Padding = new WakuPadding();
            this.BackgroundPage.Waku = p;
            this.EditLocation = np;
        }

        private void isPermitOutsideToolStripMenuItem_Click(object sender, EventArgs e)
        {

            this.Property.IsPermitOutSide = !this.Property.IsPermitOutSide;
        }

        private void leftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.BackgroundPage.Waku.Type == HukidasiType.Nothing) this.BackgroundPage.Waku = HukidasiSaveData.NoWakuDefault();

            this.Waku.Arrangement.Align = TextAlign.Left;
            this.BackgroundPage.Waku = this.Waku;
        }

        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.BackgroundPage.Waku.Type == HukidasiType.Nothing) this.BackgroundPage.Waku = HukidasiSaveData.NoWakuDefault();
            this.Waku.Arrangement.Align = TextAlign.Center;
            this.BackgroundPage.Waku = this.Waku;
        }

        private void rightToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.BackgroundPage.Waku.Type == HukidasiType.Nothing) this.BackgroundPage.Waku = HukidasiSaveData.NoWakuDefault();
            
            this.Waku.Arrangement.Align = TextAlign.Right;
            this.BackgroundPage.Waku = this.Waku;
        }
        private void montageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var v = this.Property.DLLFunction.Common.ShowMontageDialog(this.BackgroundPage.SText);
            if (v != null)
            {
                this.suouTextBox21.SetRawText(v);
            }
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CopyLayer();
        }
        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CopyLayer();
            this.Property.ParentTextBox.MergeLayerItemCollection.Scene.Parts.Remove(this.BackgroundPage);
        }

        public static ToolStripMenuItem CreateB(SectionScenePart page)
        {
            var r = new ToolStripMenuItem() { Text = "合成方法" };
            var lrmPage = SuouBase.Controls.HukidasiContext.PermitLineMerge;
            var p = new ToolStripMenuItem[] { SuouBase.Controls.HukidasiContext.DestructiveMerge, SuouBase.Controls.HukidasiContext.FreeMerge, SuouBase.Controls.HukidasiContext.LineRestrictionMerge, lrmPage, SuouBase.Controls.HukidasiContext.HighDestructiveMerge };

            var numeric = new BSystem.Controls.ToolStripNumericUpDown() { AutoSize = false, Height = 32 };

            lrmPage.DropDownItems.Add(numeric);
            lrmPage.DropDownOpening += (a, b) =>
            {
                numeric.Value = page.PermitZureLength;
            };
            numeric.ValueChanged += (a, b) =>
            {
                var n = (int)numeric.Value;
                if (n != page.PermitZureLength) page.PermitZureLength = n;
            };


            p.ForEach((v) => r.DropDownItems.Add(v));
            p.ForEach((v) => v.MouseDown += (a, b) => page.MergeOption = (MergeZureOption)v.Tag);
            r.Tag = page;
            r.DropDownOpening += (a, b) =>
            {
                var tools = a as ToolStripMenuItem;
                var spage = tools.Tag as SectionScenePart;
                foreach (ToolStripMenuItem item in tools.DropDownItems)
                {
                    var set = (MergeZureOption)item.Tag;
                    item.Checked = spage.MergeOption == set;
                }
            };

            return r;
        }
        public static ToolStripMenuItem CreateA(SectionScenePart page)
        {
            
            var r = new ToolStripMenuItem() { Text = "内部合成アルゴリズム" };
            var p = new ToolStripMenuItem[] { SuouBase.Controls.HukidasiContext.UniInAlgoItem, SuouBase.Controls.HukidasiContext.DefaultInAlgoItem, SuouBase.Controls.HukidasiContext.HukidasiDefaultInAlgoItem, SuouBase.Controls.HukidasiContext.CompleteCleanInAlgoItem };
            p.ForEach((v) => r.DropDownItems.Add(v));
            p.ForEach((v) => v.Click += (a, b) => page.Merger = ((SpaceLineSet)v.Tag));
            r.Tag = page;
            r.DropDownOpening += (a, b) =>
            {
                var tools = a as ToolStripMenuItem;
                var tagPage = tools.Tag as SectionScenePart;
                foreach (ToolStripMenuItem item in tools.DropDownItems)
                {
                    var set = item.Tag as SpaceLineSet;
                    //System.Console.WriteLine(tagPage.Merger.Name+ "/" + set.Name + "/" + (tagPage.Merger == set));
                    if(tagPage.Merger != null)
                    {
                        item.Checked = tagPage.Merger.Name == set.Name;
                    }
                    else
                    {
                        item.Checked = false;
                    }
                }
            };
            return r;
        }
        #endregion

        #region Protected
        protected override bool IsInputKey(Keys keyData)
        {
            if ((keyData & Keys.Up) != 0 || (keyData & Keys.Down) != 0 || (keyData & Keys.Left) != 0 || (keyData & Keys.Right) != 0)
            {
                if (this.Property.IsViewMode)
                {
                    if (keyData == Keys.Up)
                    {
                        this.EditLocation = new EditPoint(this.EditLocation.X, this.EditLocation.Y - 1);
                    }
                    else if (keyData == Keys.Down)
                    {
                        this.EditLocation = new EditPoint(this.EditLocation.X, this.EditLocation.Y + 1);
                    }
                    else if (keyData == Keys.Left)
                    {
                        this.EditLocation = new EditPoint(this.EditLocation.X - 1, this.EditLocation.Y);
                    }
                    else if (keyData == Keys.Right)
                    {
                        this.EditLocation = new EditPoint(this.EditLocation.X + 1, this.EditLocation.Y);
                    }
                    this.NeedMerge = true;
                    this.Property.ParentTextBox.UpdateText();
                    return true;
                    
                }

            }
            return base.IsInputKey(keyData);
        }





        #endregion
    }
    public enum CaptureDirection
    {
        Up,Left,Right,Down
    }
    public enum TextLayerBoxMode
    {
        Move,Edit,View
    }
    public enum LayerViewType
    {
        Normal,WakuTest
    }

}
