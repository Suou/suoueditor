﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Interface;
using SuouBase;
using System.Drawing.Drawing2D;
using BSystem;

namespace SuouTextBox2.Controls.TextLayer
{
    class MoveModeOperator
    {
        TextLayerBoxProperty Property { get; set; }
        TextLayerBox ParentBox { get { return this.Property.Layer; } }
        PictureBox TextPictureBox { get { return this.Property.PictureLayer; } }
        public MoveModeOperator(TextLayerBoxProperty item)
        {
            this.Property = item;
        }
        
        
    }
}
