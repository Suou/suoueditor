﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase;
using SuouBase.Text;
using SuouBase.Interface;
using SuouTextBox2.Base;
using SuouBase.Book;

namespace SuouTextBox2.Controls
{
    public partial class TextLayerSetting : Form
    {
        public TextLayerSetting()
        {
            InitializeComponent();
        }

        public EditorInfo DLLFunction { get; set; }
        public TextLayerSetting(SectionScenePart pp,EditorInfo dllfunction,TextLayerBox box):this()
        {
            PagePart = pp;
            SetType();

            this.DLLFunction = dllfunction;
            this.hukidasiSetting1.DLLFunction = dllfunction;

            this.altCharControl1.SaveItems = DLLFunction.AltHukidasiPlugin;
            if (PagePart.Merger != null)
            {
                this.altCharControl1.Item = PagePart.Merger;
            }
            this.hukidasiSetting1.Item = (HukidasiSaveData)PagePart.Waku;
            this.textLayerProperty1.Item = box;
            this.textLayerProperty1.Set();
            
        }
        public SectionScenePart PagePart { get; set; }

       

        public void SetType()
        {
            switch (this.PagePart.Type)
            {
                case MergePartsType.Comment:
                    this.comboBox1.SelectedIndex = 0;
                    break;
                case MergePartsType.Other:
                    this.comboBox1.SelectedIndex = 1;
                    break;
                default:
                    throw new Exception();
            }
        }

        private void TextLayerSetting_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                this.Visible = false;
                e.Cancel = true;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        public void SetTypeFromControl()
        {
            switch (this.comboBox1.SelectedIndex)
            {
                case 0:
                    this.PagePart.Type = MergePartsType.Comment;
                    break;
                case 1:
                    this.PagePart.Type = MergePartsType.Other;
                    break;
                default:
                    throw new Exception();
            }
        }
        
        private void OKButton_Click(object sender, EventArgs e)
        {
            if (!this.altCharControl1.Item.Check())
            {
                MessageBox.Show("合成アルゴリズムに問題があります。");
            }
            else if (!this.hukidasiSetting1.SampleItem.SaveData.ErrorCheck())
            {
                MessageBox.Show("フキダシに問題があります。");
            }
            else
            {

                SetTypeFromControl();


                var f = this.altCharControl1.Item;
                if (f == null) throw new MyException("");
                this.PagePart.Merger = f;
                var g = this.hukidasiSetting1.Item;
                if (g.MergeAlgorithm == null) throw new MyException("");
                this.PagePart.Waku = g;
            }

        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            //this.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var f = this.altCharControl1.Item;
            
            var fm = SpaceMakerDic.Create(f,SuouEditorConst.AAFont,Color.Black);
            var form = new Debug.AltCharViewer();
            form.Item = fm;
            form.ShowDialog();
        }

        private void hukidasiSetting1_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
        }

        private void hukidasiSetting1_HukidasiTypeChanged(object sender, EventArgs e)
        {
            this.altCharControl1.Item = DLLFunctionExtension.GetProperSpaceSet(this.hukidasiSetting1.Item,this.DLLFunction);
        }
    }

}
