﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SuouTextBox2.Controls
{
    public class DAndDSizeChanger
    {
        Control mouseListner;
        Control sizeChangeCtrl;
        DAndDArea sizeChangeArea;
        Size lastMouseDownSize;
        Point lastMouseDownPoint;
        DAndDArea status;
        int sizeChangeAreaWidth;
        Cursor defaultCursor;
        public event MouseEventHandler TopChanged;
        public event MouseEventHandler LeftChanged;
        public event MouseEventHandler RightChanged;
        public event MouseEventHandler BottomChanged;
        public bool IsEnable { get; set; }
        public Func<Cursor> CtrlGetCursor { get; set; }
        Size MinimumSize = new Size(100, 100);
        /// <param name="mouseListner">マウス入力を受け取るコントロール</param>
        /// <param name="sizeChangeCtrl">マウス入力によってサイズが変更されるコントロール</param>
        /// <param name="sizeChangeArea">上下左右のサイズ変更が有効になる範囲を指定</param>
        /// <param name="sizeChangeAreaWidth">サイズ変更が有効になる範囲の幅を指定</param>
        public DAndDSizeChanger(Control mouseListner, Control sizeChangeCtrl, DAndDArea sizeChangeArea, int sizeChangeAreaWidth)
        {
            this.mouseListner = mouseListner;
            this.sizeChangeCtrl = sizeChangeCtrl;
            this.sizeChangeAreaWidth = sizeChangeAreaWidth;
            this.sizeChangeArea = sizeChangeArea;
            defaultCursor = mouseListner.Cursor;

            mouseListner.MouseDown += new MouseEventHandler(mouseListner_MouseDown);
            mouseListner.MouseMove += new MouseEventHandler(mouseListner_MouseMove);
            mouseListner.MouseUp += new MouseEventHandler(mouseListner_MouseUp);

            SuouTextBox2.Controls.TextLayer.TextLayerPictureBox box = mouseListner as SuouTextBox2.Controls.TextLayer.TextLayerPictureBox;
            if (box != null) this.CtrlGetCursor = box.CursorDecision;

            this.IsEnable = true;
        }
        public bool Capture { get; set; }
        void mouseListner_MouseDown(object sender, MouseEventArgs e)
        {
            if (!IsEnable) return;
            lastMouseDownPoint = e.Location;
            lastMouseDownSize = sizeChangeCtrl.Size;

            //動作を決定
            status = DAndDArea.None;
            if (getTop().Contains(e.Location))
            {
                status |= DAndDArea.Top;
            }
            if (getLeft().Contains(e.Location))
            {
                status |= DAndDArea.Left;
            }
            if (getBottom().Contains(e.Location))
            {
                status |= DAndDArea.Bottom;
            }
            if (getRight().Contains(e.Location))
            {
                status |= DAndDArea.Right;
            }

            if (status != DAndDArea.None)
            {
                mouseListner.Capture = true;
                this.Capture = true;
            }
        }
        public bool ContainMouseCapcure(MouseEventArgs e)
        {
            this.mouseListner_MouseDown(this,e);
            if (status != DAndDArea.None)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public void Refresh(Point p)
        {
            lastMouseDownPoint = p;
            lastMouseDownSize = sizeChangeCtrl.Size;
        }
        public int diffX { get; set; }
        public int diffY { get; set; }

        public DAndDArea NowMode
        {
            get
            {
                var p = this.mouseListner.PointToClient(Control.MousePosition);
                return this.GetDirection(p);
            }
        }
        private DAndDArea GetDirection(Point mouseLocation)
        {
            //カーソルを変更
            if ((getTop().Contains(mouseLocation) &&
                getLeft().Contains(mouseLocation)) ||
                (getBottom().Contains(mouseLocation) &&
                getRight().Contains(mouseLocation)))
            {
                return DAndDArea.HidariNaname;
            }
            else if ((getTop().Contains(mouseLocation) &&
              getRight().Contains(mouseLocation)) ||
              (getBottom().Contains(mouseLocation) &&
              getLeft().Contains(mouseLocation)))
            {
                return DAndDArea.MigiNaname;
            }
            else if (getTop().Contains(mouseLocation))
            {
                return DAndDArea.Top;
            }
            else if(getBottom().Contains(mouseLocation))
            {
                return DAndDArea.Bottom;
            }
            else if (getLeft().Contains(mouseLocation))
            {
                return DAndDArea.Left;
            }
            else if (getRight().Contains(mouseLocation))
            {
                return DAndDArea.Right;
            }
            else
            {
                return DAndDArea.None;
            }

        }
        private void mouseListner_MouseMove(object sender, MouseEventArgs e)
        {
            if (!IsEnable) return;
            switch (this.GetDirection(e.Location))
            {
                case DAndDArea.None: 
                    if (e.Button != MouseButtons.Left && !this.Capture)
                    {
                        mouseListner.Cursor = this.CtrlGetCursor();
                        //mouseListner.Cursor = defaultCursor;
                    }
                    break;
                case DAndDArea.Left:
                case DAndDArea.Right:
                    mouseListner.Cursor = Cursors.SizeWE;
                    break;
                case DAndDArea.Top:
                case DAndDArea.Bottom:
                    mouseListner.Cursor = Cursors.SizeNS;
                    break;
                case DAndDArea.MigiNaname:
                    mouseListner.Cursor = Cursors.SizeNESW;
                    break;
                case DAndDArea.HidariNaname:
                    mouseListner.Cursor = Cursors.SizeNWSE;
                    break;
            }
            //カーソルを変更

            /*
            if ((getTop().Contains(e.Location) &&
                getLeft().Contains(e.Location)) ||
                (getBottom().Contains(e.Location) &&
                getRight().Contains(e.Location)))
            {

                mouseListner.Cursor = Cursors.SizeNWSE;
            }
            else if ((getTop().Contains(e.Location) &&
              getRight().Contains(e.Location)) ||
              (getBottom().Contains(e.Location) &&
              getLeft().Contains(e.Location)))
            {

                mouseListner.Cursor = Cursors.SizeNESW;
            }
            else if (getTop().Contains(e.Location) ||
              getBottom().Contains(e.Location))
            {

                mouseListner.Cursor = Cursors.SizeNS;
            }
            else if (getLeft().Contains(e.Location) ||
              getRight().Contains(e.Location))
            {

                mouseListner.Cursor = Cursors.SizeWE;
            }
            else
            {
                if (e.Button != MouseButtons.Left && !this.Capture)
                {
                    mouseListner.Cursor = defaultCursor;
                }
            }
            */

            if (e.Button == MouseButtons.Left)
            {
                diffX = e.X - lastMouseDownPoint.X;
                diffY = e.Y - lastMouseDownPoint.Y;
                if (e.Location == lastMouseDownPoint) return;
                if ((status & DAndDArea.Top) == DAndDArea.Top)
                {
                    diffY = e.Y;
                    if (TopChanged != null) TopChanged(this, e);
                }
                if ((status & DAndDArea.Bottom) == DAndDArea.Bottom)
                {
                    diffY = e.Y - this.sizeChangeCtrl.Height;
                    if (BottomChanged != null) BottomChanged(this, e);
                }
                if ((status & DAndDArea.Left) == DAndDArea.Left)
                {
                    diffX = e.X;
                    if (LeftChanged != null) LeftChanged(this, e);
                }
                if ((status & DAndDArea.Right) == DAndDArea.Right)
                {
                    diffX = e.X - this.sizeChangeCtrl.Width;
                    if (RightChanged != null) RightChanged(this, e);
                }
            }
        }

        void mouseListner_MouseUp(object sender, MouseEventArgs e)
        {
            if (!IsEnable) return;
            mouseListner.Capture = false;
            this.Capture = false;
        }

        /// <summary>
        /// ポイントがD＆Dするとサイズが変更されるエリア内にあるかどうかを判定します。
        /// </summary>
        public bool ContainsSizeChangeArea(Point p)
        {
            return getTop().Contains(p) ||
                getBottom().Contains(p) ||
                getLeft().Contains(p) ||
                getRight().Contains(p);
        }
        #region GetDirection
        private Rectangle getTop()
        {
            if ((sizeChangeArea & DAndDArea.Top) == DAndDArea.Top)
            {
                return new Rectangle(0, 0, mouseListner.Width, sizeChangeAreaWidth);
            }
            else
            {
                return new Rectangle();
            }
        }

        private Rectangle getBottom()
        {
            if ((sizeChangeArea & DAndDArea.Bottom) == DAndDArea.Bottom)
            {
                return new Rectangle(0, mouseListner.Height - sizeChangeAreaWidth,
                    mouseListner.Width, sizeChangeAreaWidth);
            }
            else
            {
                return new Rectangle();
            }
        }

        private Rectangle getLeft()
        {
            if ((sizeChangeArea & DAndDArea.Left) == DAndDArea.Left)
            {
                return new Rectangle(0, 0,
                    sizeChangeAreaWidth, mouseListner.Height);
            }
            else
            {
                return new Rectangle();
            }
        }

        private Rectangle getRight()
        {
            if ((sizeChangeArea & DAndDArea.Right) == DAndDArea.Right)
            {
                return new Rectangle(mouseListner.Width - sizeChangeAreaWidth, 0,
                    sizeChangeAreaWidth, mouseListner.Height);
            }
            else
            {
                return new Rectangle();
            }
        }
        #endregion
    }

    public enum DAndDArea
    {
        None = 0,
        Top = 1,
        Bottom = 2,
        Left = 4,
        Right = 8,
        All = 15,
        MigiNaname = 32,
        HidariNaname = 64

    }
}
