﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Interface;
using SuouBase;
using System.Drawing.Drawing2D;
using BSystem;


namespace SuouTextBox2.Controls.TextLayer
{
    class TextLayerPictureBox:PictureBox
    {
        private TextLayerBoxProperty Property { get; set; }
        private TextLayerBox ParentBox { get { return this.Property.Layer; } }
        private SuouTextBox EditBox { get { return this.Property.EditLayer; } }
        public TextLayerPictureBox(TextLayerBoxProperty prop):base()
        {
            this.InitializeComponent();
            this.SetProperty(prop);
        }
        public TextLayerPictureBox()
            : base()
        {
            this.InitializeComponent();
        }
        public void SetProperty(TextLayerBoxProperty prop)
        {
            this.Property = prop;
            this.Property.PictureLayer = this;
            //this.MoveModer = new MoveModeOperator(this.Property);

        }
        
        #region PictureBox_Method
        private Bitmap GetMergeTextImage(Color backColor)
        {

            var SrcLocation = this.ParentBox.Location;
            var DotSrcLocation = this.Property.ParentTextBox.ModifyDotLocation(SrcLocation);
            var bmp = this.Property.ParentTextBox.GetDisplayImage(DotSrcLocation, this.Size, backColor);

            if (this.Property.ParentTextBox.IsDragEndMerge && this.Property.IsDragMoveMode)
            {
                var p = new DotPoint() { X = 0, Y = 0 };
                using (var g = Graphics.FromImage(bmp))
                {
                    g.TextRenderOverride(this.Property.Layer.BackgroundPage.WakuSText,2,Point.Empty);
                }
                //bmp = this.EditBox.GetDisplayImage(bmp, p);
            }
            else
            {
                var g = Graphics.FromImage(bmp);
                g.DrawRectangle(Pens.Red, new Rectangle(Point.Empty, new Size(bmp.Width - 1, bmp.Height - 1)));
            }
            return bmp;

        }
        private void SwitchTextBoxColor(bool dragStart)
        {
            if (dragStart)
            {
                if (!this.Property.IsAlreadyInputText)
                {
                    if (this.ParentBox.BackgroundPage != null) this.EditBox.SetRawText(this.ParentBox.BackgroundPage.SText);
                    this.Property.IsAlreadyInputText = true;
                }
                this.EditBox.PrintFontSet.IsPriority = true;
                this.EditBox.PrintFontSet.TextColor = Color.Red;
                this.EditBox.PrintFontSet.IsDrawSpace = false;
            }
            else
            {
                this.EditBox.PrintFontSet.IsPriority = false;
                this.EditBox.PrintFontSet.TextColor = Color.Black;
                this.EditBox.PrintFontSet.IsDrawSpace = true;
            }
        }
        private bool _IsInnerMouse = false;
        public bool IsInnerMouse
        {
            get { return this._IsInnerMouse; }
            set
            {
                this._IsInnerMouse = value;

                this.RefreshPictureBox();
            }
        }
        public void RefreshPictureBox()
        {
            if (this.DesignMode) return;
            //this.PictureResize();
            if (!this.IsHandleCreated) return;
            this.Size = this.ParentBox.ComputeTextBoxSize();
            if (this.Property.IsDragMoveMode)
            {
                this.Image = this.GetMergeTextImage(Color.AliceBlue);
            }
            else
            {
                if (this.IsInnerMouse)
                {
                    this.Image = this.GetMergeTextImage(Color.AliceBlue);
                }
                else
                {
                    this.Image = this.GetMergeTextImage(Color.Honeydew);
                }
            }
        }
        private void CheckStartDragMode(MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && !this.Property.IsDragMoveMode && !this.Property.NoDragRectangle.Contains(e.Location))
            {
                if (!this.Property.EnableMerge)
                {
                    System.Console.WriteLine("DragStart!!!!");
                    System.Console.WriteLine("@@@@@@@@@");
                    System.Console.WriteLine("@@@@@@@@@");
                    System.Console.WriteLine("@@@@@@@@@");
                    this.Property.ParentTextBox.TextBoxMode = TextBoxMode.DragStartReady;

                    Action f1 = () => this.Property.IsDragMoveMode = true;
                    BSystem.Diagnostics.ConsoleStopWatch(f1, "f1");

                    //this.Property.IsDragMoveMode = true;

                    Action f2 = () => this.SwitchTextBoxColor(true);
                    BSystem.Diagnostics.ConsoleStopWatch(f2, "SwitchTextBoxColor True");
                    //this.SwitchTextBoxColor(true);

                }
                if (this.Property.CapturePicture)
                {
                    Action f3 = () => this.Property.IsDragMoveMode = true;
                    BSystem.Diagnostics.ConsoleStopWatch(f3, "f3");

                    //this.Property.IsDragMoveMode = true;

                    Action f4 = () => this.Property.OriginalPoint = e.Location;
                    BSystem.Diagnostics.ConsoleStopWatch(f4, "f4");

                    //this.Property.OriginalPoint = e.Location;

                    Action f5 = () => this.Property.LayerOnEnter(new EventArgs());
                    BSystem.Diagnostics.ConsoleStopWatch(f5, "f5");

                    //this.Property.LayerOnEnter(new EventArgs());

                }
                if(this.Property.ParentTextBox.TextBoxMode == TextBoxMode.DragStartReady)
                {
                    this.Property.ParentTextBox.TextBoxMode = TextBoxMode.Drag;
                    this.Property.ParentTextBox.UpdateText(false);
                }

                System.Console.WriteLine("DragEND!!!!");
                System.Console.WriteLine("");
                System.Console.WriteLine("");
                System.Console.WriteLine("");

            }
        }
        #endregion


        #region PictureBox_EventMethod



        private Cursor ShouldCursor
        {
            get
            {
                if (this.Property.CanMove)
                {
                    return Cursors.Hand;
                }
                else
                {
                    return Cursors.Arrow;
                }
            }
        }
        public Func<Cursor> CursorDecision
        {
            get
            {
                return ()=>this.ShouldCursor;
            }
        }
        /*
        public override Cursor Cursor
        {
            get
            {
                return this.ShouldCursor;
            }
            set
            {
                base.Cursor = value;
            }
        }
        */

        #endregion
        public void EventDispose()
        {
            /*
            this.MouseDown -= this.TextPicture_MouseDown;
            this.MouseMove -= this.TextPicture_MouseMove;
            this.DoubleClick -= this.TextPicture_DoubleClick;
            */
        }

        private void InitializeComponent()
        {
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // TextLayerPictureBox
            // 
            this.DoubleClick += new System.EventHandler(this.TextLayerPictureBox_DoubleClick);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.TextLayerPictureBox_MouseDown);
            this.MouseEnter += new System.EventHandler(this.TextLayerPictureBox_MouseEnter);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.TextLayerPictureBox_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.TextLayerPictureBox_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        private void TextLayerPictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            Stopwatch sw1 = Stopwatch.StartNew();

            if (this.Property.CanMove)
            {
                //Action f = () => this.CheckStartDragMode(e);

                //BSystem.Diagnostics.ConsoleStopWatch(f, "CheckStartDragMode");
                this.CheckStartDragMode(e);


                if (e.Button == System.Windows.Forms.MouseButtons.Left && this.Property.CapturePicture)
                {

                    if (this.Property.IsDragMoveMode)
                    {
                        var ep = this.getEditPoint(e);
                        this.temporaryMovePictureBox(ep);

                    }

                }


                //this.CheckMovePicture(e);
            }
            sw1.Stop();
            if(sw1.ElapsedMilliseconds > 100)
            {
                System.Console.WriteLine(sw1.ElapsedMilliseconds);
            }

        }

        private void TextLayerPictureBox_MouseDown(object sender, MouseEventArgs e)
        {
            this.Property.CapturePicture = !this.Property.ChangerWaku.ContainMouseCapcure(e);

            this.Property.NoDragRectangle = SuouBase.Base.SuouUsefulMethod.CreateNoDragRectangle(e.Location);

            this.Property.Layer.BringToFront();


            this.Property.LayerOnMouseDown(e);
        }
        private void DragEnd()
        {
            BSystem.Debug.ColorConsole.IncrementTabCount();
            var sw = System.Diagnostics.Stopwatch.StartNew();
            this.Property.ParentTextBox.TextBoxMode = TextBoxMode.DragEndReady;
            this.Property.IsDragMoveMode = false;

            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine("IsDragMoveMode = false/" + sw.ElapsedMilliseconds);
            sw.Restart();
            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine("RefreshPictureBox/" + sw.ElapsedMilliseconds);
            this.Property.ParentTextBox.TextBoxMode = TextBoxMode.Free;
            this.Property.ParentTextBox.UpdateText(true);
            this.RefreshPictureBox();

            BSystem.Debug.ColorConsole.DecrementTabCount();

        }
        private void TextLayerPictureBox_MouseUp(object sender, MouseEventArgs e)
        {

            if (e.Button == System.Windows.Forms.MouseButtons.Left && this.Property.CapturePicture)
            {
                if (this.Property.IsDragMoveMode)
                {
                    var ep = this.getEditPoint(e);
                    this.movePictureBox(ep);
                }

            }


            //temporaryMovePictureBox(e);
            if(this.Property.ParentTextBox.TextBoxMode == TextBoxMode.Drag)
            {
                this.DragEnd();
            }
            //this.Property.IsDragMoveMode = false;
            //2014/02/23

            if (!this.ParentBox.EnableMerge)
            {
                this.SwitchTextBoxColor(false);
            }
            else
            {
                this.RefreshPictureBox();
            }
            

            this.Property.ParentTextBox.MergeLayerItemCollection.ReBringToFront();

        }

        private void TextLayerPictureBox_MouseEnter(object sender, EventArgs e)
        {
            this.IsInnerMouse = true;
            if (this.Property.ChangerWaku.IsEnable)
            {
            }
            else
            {
                this.Cursor = this.ShouldCursor;
            }
        }

        private void TextLayerPictureBox_DoubleClick(object sender, EventArgs e)
        {

            if (this.Property.Layer.Type == LayerViewType.Normal)
            {
                this.ParentBox.SwitchTextBox();
            }
        }
        private EditPoint getEditPoint(MouseEventArgs e)
        {
            var r = SuouBase.GeneralMethod.Shift(e.Location, this.Property.OriginalPoint);
            var p2 = new Point(this.ParentBox.Location.X + r.X + 2, this.ParentBox.Location.Y + r.Y + 1);
            var p_2 = TextLayerBoxProperty.InverseTranslateLocation(p2);
            var p3 = this.Property.ParentTextBox.ModifyLocation(p_2);
            if (!this.Property.IsPermitOutSide && p3.X < 0) p3.X = 0;
            if (!this.Property.IsPermitOutSide && p3.Y < 0) p3.Y = 0;
            return new EditPoint(p3.X, p3.Y);

        }
        private void temporaryMovePictureBox(EditPoint point)
        {
            System.Console.WriteLine("TempoMove");

            System.Console.WriteLine(point);

            this.ParentBox.temporaryChangeLocation(point);
            /*
            if (e.Button == System.Windows.Forms.MouseButtons.Left && this.Property.CapturePicture)
            {
                if (this.Property.IsDragMoveMode)
                {
                    var r = SuouBase.GeneralMethod.Shift(e.Location, this.Property.OriginalPoint);
                    var p2 = new Point(this.ParentBox.Location.X + r.X + 2, this.ParentBox.Location.Y + r.Y + 1);
                    var p_2 = TextLayerBoxProperty.InverseTranslateLocation(p2);
                    var p3 = this.Property.ParentTextBox.ModifyLocation(p_2);
                    if (!this.Property.IsPermitOutSide && p3.X < 0) p3.X = 0;
                    if (!this.Property.IsPermitOutSide && p3.Y < 0) p3.Y = 0;


                    this.ParentBox.EditLocation = new EditPoint(p3.X, p3.Y);


                }
            }
            */
        }
        private void movePictureBox(EditPoint point)
        {
            System.Console.WriteLine(point);
            this.ParentBox.temporaryChangeLocation(point);
            this.ParentBox.EditLocation = point;

            /*
            if (e.Button == System.Windows.Forms.MouseButtons.Left && this.Property.CapturePicture)
            {
                if (this.Property.IsDragMoveMode)
                {
                    var r = SuouBase.GeneralMethod.Shift(e.Location, this.Property.OriginalPoint);
                    var p2 = new Point(this.ParentBox.Location.X + r.X + 2, this.ParentBox.Location.Y + r.Y + 1);
                    var p_2 = TextLayerBoxProperty.InverseTranslateLocation(p2);
                    var p3 = this.Property.ParentTextBox.ModifyLocation(p_2);
                    if (!this.Property.IsPermitOutSide && p3.X < 0) p3.X = 0;
                    if (!this.Property.IsPermitOutSide && p3.Y < 0) p3.Y = 0;




                }
            }
            */
        }

    }
}
