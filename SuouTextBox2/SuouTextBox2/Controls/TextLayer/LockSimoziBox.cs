﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;
using SuouBase;
using System.ComponentModel;
using SuouBase.Book;
namespace SuouTextBox2.Controls.TextLayer
{
    public class LockSimoziBox : SimoziBox
    {
        public LockSimoziBox():base()
        {
            this.InitializeComponent();
            this.suouTextBox1.RefleshDraw();
            
            var p = new SectionScenePart() { EditLocation = new EditPoint(100, 8) };
            this.MergeLayerItemCollection.Scene.Parts.Add(p);
            this.SelectedBox = (TextLayerBox)this.MergeLayerItemCollection.LayerItems.First();
            this.SelectedBox.CanMove = false;
            this.SelectedBox.Type = LayerViewType.WakuTest;
            this.SelectedBox.EditLocation = this.MiddlePosition;
            
        }
        public TextLayerBox SelectedBox
        {
            get;
            private set;
        }
        public HukidasiSaveData Waku
        {
            get { return (HukidasiSaveData)this.SelectedBox.BackgroundPage.Waku; }
            private set { this.SelectedBox.BackgroundPage.Waku = value; }
        }
        public void SetWaku(HukidasiSaveData h)
        {
            this.Waku = h;
        }

        [ReadOnly(true)]
        public string LayerText
        {
            get
            {
                if (this.SelectedBox == null || this.SelectedBox.TextBox == null)
                {
                    return null;
                }
                else
                {
                    return this.SelectedBox.TextBox.Text;
                }
            }
            set
            {
                if (this.SelectedBox == null || this.SelectedBox.TextBox == null)
                {
                }
                else
                {
                    this.SelectedBox.BackgroundPage.SText = new SuouText(value);
                    this.UpdateText();
                }
            }
        }

        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // suouTextBox1
            // 
            // 
            // LockSimoziBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.Name = "LockSimoziBox";
            this.Load += new System.EventHandler(this.LockSimoziBox_Load);
            this.Resize += new System.EventHandler(this.LockSimoziBox_Resize);
            this.ResumeLayout(false);

        }
        public void Loading()
        {

        }
        private void LockSimoziBox_Load(object sender, EventArgs e)
        {
        }
        
        protected override void suouTextBox1_DoubleClick(object sender, EventArgs e)
        {
        }
        public override bool IsMultiMode
        {
            get
            {
                return base.IsMultiMode;
            }
            set
            {
                base.IsMultiMode = true;   
            }
        }
        private EditPoint MiddlePosition
        {
            get
            {
               var p = this.suouTextBox1.PointMaker.CreateExDotPoint(this.Width / 2, this.Height / 2);
               var ep = (SuouTextBox2.Base.ExEditPoint)p;
               var pp = ep.ToEditPoint();
               pp.X -= this.SelectedBox.GetInitialTextSize().Width / 2;
               pp.Y -= ((HukidasiSaveData)this.SelectedBox.BackgroundPage.Waku).Padding.TopSpace;
               pp.X -= ((HukidasiSaveData)this.SelectedBox.BackgroundPage.Waku).Padding.LeftSpace;
               pp.Y -= this.SelectedBox.BackgroundPage.SText.Count / 2;
               if (pp.Y < 4) pp.Y = 4;
               if (pp.X < 60) pp.X = 60;
               return pp;
            }
        }
        public new void Copy()
        {
            this.SelectedBox.CopyLayer();
        }
        public void Clear()
        {
            var p = this.SelectedBox.BackgroundPage.Waku;
            this.SelectedBox.BackgroundPage.SText = new SuouText();
            this.SelectedBox.BackgroundPage.Waku = HukidasiSaveData.FiveDefault();
        }
        public void ResizeLayer()
        {
            this.SelectedBox.EditLocation = this.MiddlePosition;
        }

        private void LockSimoziBox_Resize(object sender, EventArgs e)
        {
            this.ResizeLayer();
            
        }
        
    }
}
