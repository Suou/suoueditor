﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Interface;
using SuouBase;
using System.Drawing.Drawing2D;

namespace SuouTextBox2.Controls
{
    public partial class TextLayerProperty : UserControl
    {
        public TextLayerBox Item { get; set; }
        public TextLayerProperty()
        {
            InitializeComponent();
        }
        public void Set()
        {
            if (this.Item == null) return;
            this.TextWidthNumeric.Value = this.Item.BackgroundPage.SText.Width;
            this.TextHeightNumeric.Value = this.Item.BackgroundPage.SText.Count;

            this.HukidasiWidthNumeric.Value = this.Item.BackgroundPage.CreateWakuText().Width;
            this.HukidasiHeightNumeric.Value = this.Item.BackgroundPage.CreateWakuText().Count;

            this.LayerHeightNumeric.Value = this.Item.Height;
            this.LayerWidthNumeric.Value = this.Item.Width;
            //this.numericUpDown1.Value = this.Item.Index;
            this.numericUpDown2.Value = this.Item.PageIndex;
        }
    }
}
