﻿namespace SuouTextBox2.Controls
{
    partial class TextLayerBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SuouTextBox2.Operator.InversePen inversePen1 = new SuouTextBox2.Operator.InversePen();
            SuouTextBox2.Base.PrimitiveChoice primitiveChoice1 = new SuouTextBox2.Base.PrimitiveChoice();
            SuouBase.Text.InverseDataLapper inverseDataLapper1 = new SuouBase.Text.InverseDataLapper();
            SuouBase.Text.InverseDataCollection inverseDataCollection1 = new SuouBase.Text.InverseDataCollection();
            this.suouTextBox21 = new SuouTextBox2.SuouTextBox();
            this.layerContextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.leftPasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mergeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.upMoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downMoveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.setWakuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.isPermitOutsideToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alignmentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.leftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.centerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rightToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.resetPaddingToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.textLayerPictureBox1 = new SuouTextBox2.Controls.TextLayer.TextLayerPictureBox();
            this.detailToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.layerContextMenuStrip.SuspendLayout();
            this.contextMenuStrip2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.textLayerPictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // suouTextBox21
            // 
            this.suouTextBox21.AllowDrop = true;
            this.suouTextBox21.CanOriginalDrag = false;
            this.suouTextBox21.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.suouTextBox21.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.suouTextBox21.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F);
            this.suouTextBox21.ImeMode = System.Windows.Forms.ImeMode.On;
            this.suouTextBox21.IsFixedScroll = true;
            this.suouTextBox21.IsInnerScrollBar = false;
            this.suouTextBox21.IsRuler = false;
            this.suouTextBox21.IsVisibleCaret = true;
            this.suouTextBox21.Location = new System.Drawing.Point(0, 160);
            this.suouTextBox21.Margin = new System.Windows.Forms.Padding(5);
            this.suouTextBox21.Name = "suouTextBox21";
            this.suouTextBox21.PaddingSize = new System.Drawing.Size(5, 25);
            inversePen1.ClickButton = System.Windows.Forms.MouseButtons.Left;
            inversePen1.Ctrl = this.suouTextBox21;
            primitiveChoice1.IsContinuity = false;
            inversePen1.ICB = primitiveChoice1;
            inverseDataLapper1.Item = inverseDataCollection1;
            inversePen1.InverseData = inverseDataLapper1;
            this.suouTextBox21.Pen = inversePen1;
            this.suouTextBox21.Size = new System.Drawing.Size(399, 172);
            this.suouTextBox21.TabIndex = 0;
            this.suouTextBox21.UseDebug = false;
            this.suouTextBox21.UserInputEnable = true;
            // 
            // layerContextMenuStrip
            // 
            this.layerContextMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.layerContextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.cutToolStripMenuItem,
            this.toolStripSeparator3,
            this.leftPasteToolStripMenuItem,
            this.mergeToolStripMenuItem,
            this.toolStripSeparator2,
            this.upMoveToolStripMenuItem,
            this.downMoveToolStripMenuItem,
            this.toolStripSeparator1,
            this.setWakuToolStripMenuItem,
            this.isPermitOutsideToolStripMenuItem,
            this.alignmentToolStripMenuItem,
            this.detailToolStripMenuItem});
            this.layerContextMenuStrip.Name = "layerContextMenuStrip";
            this.layerContextMenuStrip.Size = new System.Drawing.Size(219, 314);
            this.layerContextMenuStrip.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.deleteToolStripMenuItem.Text = "このレイヤーを削除";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.copyToolStripMenuItem.Text = "コピー";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.cutToolStripMenuItem.Text = "切り取り";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(215, 6);
            // 
            // leftPasteToolStripMenuItem
            // 
            this.leftPasteToolStripMenuItem.Name = "leftPasteToolStripMenuItem";
            this.leftPasteToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.leftPasteToolStripMenuItem.Text = "左貼り付け";
            this.leftPasteToolStripMenuItem.Click += new System.EventHandler(this.leftPasteToolStripMenuItem_Click);
            // 
            // mergeToolStripMenuItem
            // 
            this.mergeToolStripMenuItem.Name = "mergeToolStripMenuItem";
            this.mergeToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.mergeToolStripMenuItem.Text = "背景と合成";
            this.mergeToolStripMenuItem.Click += new System.EventHandler(this.mergeToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(215, 6);
            // 
            // upMoveToolStripMenuItem
            // 
            this.upMoveToolStripMenuItem.Name = "upMoveToolStripMenuItem";
            this.upMoveToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.upMoveToolStripMenuItem.Text = "上に移動";
            this.upMoveToolStripMenuItem.Click += new System.EventHandler(this.upMoveToolStripMenuItem_Click);
            // 
            // downMoveToolStripMenuItem
            // 
            this.downMoveToolStripMenuItem.Name = "downMoveToolStripMenuItem";
            this.downMoveToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.downMoveToolStripMenuItem.Text = "下に移動";
            this.downMoveToolStripMenuItem.Click += new System.EventHandler(this.downMoveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(215, 6);
            // 
            // setWakuToolStripMenuItem
            // 
            this.setWakuToolStripMenuItem.Name = "setWakuToolStripMenuItem";
            this.setWakuToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.setWakuToolStripMenuItem.Text = "枠の変更";
            this.setWakuToolStripMenuItem.Click += new System.EventHandler(this.setWakuToolStripMenuItem_Click);
            // 
            // isPermitOutsideToolStripMenuItem
            // 
            this.isPermitOutsideToolStripMenuItem.Name = "isPermitOutsideToolStripMenuItem";
            this.isPermitOutsideToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.isPermitOutsideToolStripMenuItem.Text = "画面外への移動を許可";
            this.isPermitOutsideToolStripMenuItem.Click += new System.EventHandler(this.isPermitOutsideToolStripMenuItem_Click);
            // 
            // alignmentToolStripMenuItem
            // 
            this.alignmentToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.leftToolStripMenuItem,
            this.centerToolStripMenuItem,
            this.rightToolStripMenuItem});
            this.alignmentToolStripMenuItem.Name = "alignmentToolStripMenuItem";
            this.alignmentToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.alignmentToolStripMenuItem.Text = "テキスト配置";
            this.alignmentToolStripMenuItem.DropDownOpening += new System.EventHandler(this.alignmentToolStripMenuItem_DropDownOpening);
            // 
            // leftToolStripMenuItem
            // 
            this.leftToolStripMenuItem.Name = "leftToolStripMenuItem";
            this.leftToolStripMenuItem.Size = new System.Drawing.Size(149, 26);
            this.leftToolStripMenuItem.Text = "左揃え";
            this.leftToolStripMenuItem.Click += new System.EventHandler(this.leftToolStripMenuItem_Click);
            // 
            // centerToolStripMenuItem
            // 
            this.centerToolStripMenuItem.Name = "centerToolStripMenuItem";
            this.centerToolStripMenuItem.Size = new System.Drawing.Size(149, 26);
            this.centerToolStripMenuItem.Text = "中央揃え";
            this.centerToolStripMenuItem.Click += new System.EventHandler(this.centerToolStripMenuItem_Click);
            // 
            // rightToolStripMenuItem
            // 
            this.rightToolStripMenuItem.Name = "rightToolStripMenuItem";
            this.rightToolStripMenuItem.Size = new System.Drawing.Size(149, 26);
            this.rightToolStripMenuItem.Text = "右揃え";
            this.rightToolStripMenuItem.Click += new System.EventHandler(this.rightToolStripMenuItem_Click);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyToolStripMenuItem1,
            this.resetPaddingToolStripMenuItem1});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(190, 52);
            // 
            // copyToolStripMenuItem1
            // 
            this.copyToolStripMenuItem1.Name = "copyToolStripMenuItem1";
            this.copyToolStripMenuItem1.Size = new System.Drawing.Size(189, 24);
            this.copyToolStripMenuItem1.Text = "コピー";
            this.copyToolStripMenuItem1.Click += new System.EventHandler(this.copyToolStripMenuItem1_Click);
            // 
            // resetPaddingToolStripMenuItem1
            // 
            this.resetPaddingToolStripMenuItem1.Name = "resetPaddingToolStripMenuItem1";
            this.resetPaddingToolStripMenuItem1.Size = new System.Drawing.Size(189, 24);
            this.resetPaddingToolStripMenuItem1.Text = "パディングのリセット";
            this.resetPaddingToolStripMenuItem1.Click += new System.EventHandler(this.resetPaddingToolStripMenuItem1_Click);
            // 
            // textLayerPictureBox1
            // 
            this.textLayerPictureBox1.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.textLayerPictureBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textLayerPictureBox1.IsInnerMouse = false;
            this.textLayerPictureBox1.Location = new System.Drawing.Point(0, 25);
            this.textLayerPictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textLayerPictureBox1.Name = "textLayerPictureBox1";
            this.textLayerPictureBox1.Size = new System.Drawing.Size(399, 135);
            this.textLayerPictureBox1.TabIndex = 2;
            this.textLayerPictureBox1.TabStop = false;
            // 
            // ページの詳細設定ToolStripMenuItem
            // 
            this.detailToolStripMenuItem.Name = "detailToolStripMenuItem";
            this.detailToolStripMenuItem.Size = new System.Drawing.Size(218, 24);
            this.detailToolStripMenuItem.Text = "ページの詳細設定";
            // 
            // TextLayerBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textLayerPictureBox1);
            this.Controls.Add(this.suouTextBox21);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "TextLayerBox";
            this.Size = new System.Drawing.Size(399, 332);
            this.Load += new System.EventHandler(this.TextLayerBox_Load);
            this.LocationChanged += new System.EventHandler(this.TextLayerBox_LocationChanged);
            this.VisibleChanged += new System.EventHandler(this.TextLayerBox_VisibleChanged);
            this.layerContextMenuStrip.ResumeLayout(false);
            this.contextMenuStrip2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.textLayerPictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private SuouTextBox suouTextBox21;
        private System.Windows.Forms.ContextMenuStrip layerContextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mergeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftPasteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upMoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downMoveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem setWakuToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem copyToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem resetPaddingToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem isPermitOutsideToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alignmentToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem leftToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem centerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rightToolStripMenuItem;
        private TextLayer.TextLayerPictureBox textLayerPictureBox1;
        private System.Windows.Forms.ToolStripMenuItem detailToolStripMenuItem;
    }
}
