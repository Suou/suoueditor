﻿namespace SuouTextBox2.Controls
{
    partial class TextLayerProperty
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextHeightNumeric = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.TextWidthNumeric = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.HukidasiHeightNumeric = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.HukidasiWidthNumeric = new System.Windows.Forms.NumericUpDown();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LayerHeightNumeric = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.LayerWidthNumeric = new System.Windows.Forms.NumericUpDown();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextHeightNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextWidthNumeric)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HukidasiHeightNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HukidasiWidthNumeric)).BeginInit();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayerHeightNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayerWidthNumeric)).BeginInit();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TextHeightNumeric);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TextWidthNumeric);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(531, 65);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "元テキスト";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(17, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "行";
            // 
            // TextHeightNumeric
            // 
            this.TextHeightNumeric.Location = new System.Drawing.Point(41, 18);
            this.TextHeightNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.TextHeightNumeric.Name = "TextHeightNumeric";
            this.TextHeightNumeric.ReadOnly = true;
            this.TextHeightNumeric.Size = new System.Drawing.Size(90, 19);
            this.TextHeightNumeric.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(146, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "dot";
            // 
            // TextWidthNumeric
            // 
            this.TextWidthNumeric.Location = new System.Drawing.Point(173, 18);
            this.TextWidthNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.TextWidthNumeric.Name = "TextWidthNumeric";
            this.TextWidthNumeric.ReadOnly = true;
            this.TextWidthNumeric.Size = new System.Drawing.Size(90, 19);
            this.TextWidthNumeric.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.HukidasiHeightNumeric);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.HukidasiWidthNumeric);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(0, 65);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(531, 70);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "フキダシ合成テキスト";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(17, 12);
            this.label2.TabIndex = 3;
            this.label2.Text = "行";
            // 
            // HukidasiHeightNumeric
            // 
            this.HukidasiHeightNumeric.Location = new System.Drawing.Point(41, 18);
            this.HukidasiHeightNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.HukidasiHeightNumeric.Name = "HukidasiHeightNumeric";
            this.HukidasiHeightNumeric.ReadOnly = true;
            this.HukidasiHeightNumeric.Size = new System.Drawing.Size(90, 19);
            this.HukidasiHeightNumeric.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(146, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "dot";
            // 
            // HukidasiWidthNumeric
            // 
            this.HukidasiWidthNumeric.Location = new System.Drawing.Point(173, 18);
            this.HukidasiWidthNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.HukidasiWidthNumeric.Name = "HukidasiWidthNumeric";
            this.HukidasiWidthNumeric.ReadOnly = true;
            this.HukidasiWidthNumeric.Size = new System.Drawing.Size(90, 19);
            this.HukidasiWidthNumeric.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.LayerHeightNumeric);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.LayerWidthNumeric);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 135);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(531, 70);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "レイヤー";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(19, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(25, 12);
            this.label5.TabIndex = 3;
            this.label5.Text = "高さ";
            // 
            // LayerHeightNumeric
            // 
            this.LayerHeightNumeric.Location = new System.Drawing.Point(50, 18);
            this.LayerHeightNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.LayerHeightNumeric.Name = "LayerHeightNumeric";
            this.LayerHeightNumeric.ReadOnly = true;
            this.LayerHeightNumeric.Size = new System.Drawing.Size(90, 19);
            this.LayerHeightNumeric.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(146, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(17, 12);
            this.label6.TabIndex = 1;
            this.label6.Text = "幅";
            // 
            // LayerWidthNumeric
            // 
            this.LayerWidthNumeric.Location = new System.Drawing.Point(169, 18);
            this.LayerWidthNumeric.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.LayerWidthNumeric.Name = "LayerWidthNumeric";
            this.LayerWidthNumeric.ReadOnly = true;
            this.LayerWidthNumeric.Size = new System.Drawing.Size(90, 19);
            this.LayerWidthNumeric.TabIndex = 0;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label8);
            this.groupBox4.Controls.Add(this.numericUpDown2);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.numericUpDown1);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox4.Location = new System.Drawing.Point(0, 205);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(531, 75);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "その他";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(50, 18);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.ReadOnly = true;
            this.numericUpDown1.Size = new System.Drawing.Size(90, 19);
            this.numericUpDown1.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 12);
            this.label7.TabIndex = 7;
            this.label7.Text = "z位置";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(167, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 12);
            this.label8.TabIndex = 9;
            this.label8.Text = "ページz位置";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(237, 18);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.ReadOnly = true;
            this.numericUpDown2.Size = new System.Drawing.Size(90, 19);
            this.numericUpDown2.TabIndex = 8;
            // 
            // TextLayerProperty
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TextLayerProperty";
            this.Size = new System.Drawing.Size(531, 424);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TextHeightNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TextWidthNumeric)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.HukidasiHeightNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HukidasiWidthNumeric)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LayerHeightNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LayerWidthNumeric)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown TextHeightNumeric;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown TextWidthNumeric;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown HukidasiHeightNumeric;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown HukidasiWidthNumeric;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown LayerHeightNumeric;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown LayerWidthNumeric;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
    }
}
