﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Drawing;
using SuouBase.Interface;
using SuouBase;
using System.Drawing.Drawing2D;
using BSystem;
using SuouTextBox2.Controls.TextLayer;
namespace SuouTextBox2.Controls.TextLayer
{
    class TextLayerBoxProperty
    {
        public TextLayerBox Layer { get; set; }
        public TextLayerPictureBox PictureLayer { get; set; }
        public SuouTextBox EditLayer { get; private set; }
        public Action<MouseEventArgs> LayerOnMouseDown { get; set; }
        public Action<EventArgs> LayerOnEnter { get; set; }
        private bool _CanMove = true;
        public bool CanMove
        {
            get { return this._CanMove; }
            set { this._CanMove = value; }
        }
        public bool IsPermitOutSide { get; set; }

        public TextLayerBoxProperty(TextLayerBox parent,SuouTextBox editbox,TextLayerPictureBox picture){
            this.Layer = parent;
            this.EditLayer = editbox;
            this.CanMove = true;
            this.PictureLayer = picture;
            this.PictureLayer.SetProperty(this);
            
        }

        #region Property

        public EditorInfo DLLFunction { get; set; }
        public bool IsEditLocationChange { get; set; }
        
        public bool NeedMerge { get; set; }
        public bool IsAlreadyInputText = false;
        
        public TextBoxShortCutKeys ShortCutKeys
        {
            get { return this.EditLayer.ShortCutKeys; }
            set { this.EditLayer.SetShortCutKeys(value); }
        }
        
        public bool CapturePicture { get; set; }

        //private TextLayerPictureBox TextPictureBox = new TextLayerPictureBox() { Cursor = Cursors.Hand };
        public int ShowNumber { get; set; }
        public SimoziBox ParentTextBox { get; set; }
        public bool IsViewMode
        {
            get
            {
                return this.Layer.Controls.Contains(this.PictureLayer);
            }
        }
        public Size DisplaySize
        {
            get
            {
                return this.Layer.ComputeTextBoxSize();
                /*
                if (!this.IsAlreadyInputText)
                {
                    return this.OptimalLayerSize(this.Layer.PagePartsItem.Text);
                }
                else
                {
                    return this.OptimalLayerSize(this.EditLayer.RawText);
                }
                */
            }
        }
        public bool IsDragMoveMode
        {
            get { return this.Layer.IsDragMoveMode; }
            set
            {
                this.Layer.IsDragMoveMode = value;
            }
        }
        public Point OriginalPoint { get; set; }
        public bool EnableMerge
        {
            get
            {
                
                var b = !this.ParentTextBox.IsDragEndMerge;
                return b;
            }
        }
        public Rectangle NoDragRectangle { get; set; }
        public ToolTip PopUpBox { get; set; }
        #endregion
        /*
        private Size OptimalLayerSize(SuouText r)
        {
            var width = r.Width < 200 ? 200 : (r.Width + 200).Truncate(200);
            var height = r.Height < 100 ? 100 : (r.Height + 100).Truncate(100);

            var NewSize = new Size(width, height);
            return NewSize;

        }
        */

        public DAndDSizeChanger ChangerWaku { get; set; }

        public Point TranslateLocation(EditPoint ep)
        {
            var p = this.ParentTextBox.TextBox.TranslateToDisplayPoint(ep);
            p.X -= 6;
            p.Y -= 6;
            return p;
        }
        public static Point InverseTranslateLocation(Point p)
        {
            p.X += 6;
            p.Y += 6;
            return p;
        }
        public void CreateChangerWaku()
        {
            var f = new DAndDSizeChanger(this.PictureLayer, this.Layer, DAndDArea.All, 18);
            ChangerWaku = f;
            ChangerWaku.TopChanged += this.SizeTop_Changed;
            ChangerWaku.LeftChanged += this.SizeLeft_Changed;
            ChangerWaku.RightChanged += this.SizeRight_Changed;
            ChangerWaku.BottomChanged += this.SizeBottom_Changed;
            ChangerWaku.IsEnable = this.Layer.BackgroundPage.Waku.Type != HukidasiType.Nothing;
            
        }
        private HukidasiSaveData Waku
        {
            get { return (HukidasiSaveData)this.Layer.BackgroundPage.Waku; }
        }
        private void SizeTop_Changed(object sender, MouseEventArgs e)
        {
            int n = -this.ChangerWaku.diffY / 16;
            if (this.ChangerWaku.diffY < -16)
            {
                this.TopExtension(e, true, CaptureDirection.Up, (short)n);
            }
            else if (this.ChangerWaku.diffY > 16 && this.Waku.Padding.TopSpace > 0)
            {
                int w = this.Waku.Padding.TopSpace + n;
                if (w < 0) n = -this.Waku.Padding.TopSpace;
                this.TopExtension(e, false, CaptureDirection.Up, (short)n);
                
            }
            this.PopUpBox.Show(this.Waku.Padding.TopSpace.ToString(), this.Layer, 0, 0, 1000);
            this.ParentTextBox.UpdateText();
            this.PictureLayer.RefreshPictureBox();
        }
        private void SizeBottom_Changed(object sender, MouseEventArgs e)
        {
            int n = this.ChangerWaku.diffY / 16;
            if (this.ChangerWaku.diffY < -16 && this.Waku.Padding.BottomSpace > 0)
            {
                int w = this.Waku.Padding.BottomSpace + n;
                if (w < 0) n = -this.Waku.Padding.TopSpace;
                this.TopExtension(e, false, CaptureDirection.Down, n);
            }
            else if (this.ChangerWaku.diffY > 16)
            {
                this.TopExtension(e, true, CaptureDirection.Down, n);
            }
            this.PopUpBox.Show(this.Waku.Padding.BottomSpace.ToString(), this.Layer, 0, this.Layer.Height, 1000);
            this.ParentTextBox.UpdateText();
            this.PictureLayer.RefreshPictureBox();
        }
        private void SizeLeft_Changed(object sender, MouseEventArgs e)
        {
            if (this.ChangerWaku.diffX < -1)
            {
                this.TopExtension(e, true, CaptureDirection.Left, -this.ChangerWaku.diffX);
            }
            else if (this.ChangerWaku.diffX > 1 && this.Waku.Padding.LeftSpace > 0)
            {
                this.TopExtension(e, false, CaptureDirection.Left, -this.ChangerWaku.diffX);
            }
            this.PopUpBox.Show(this.Waku.Padding.LeftSpace.ToString(), this.Layer, 0, 0, 1000);
            this.ParentTextBox.UpdateText();
            this.PictureLayer.RefreshPictureBox();
        }
        private void SizeRight_Changed(object sender, MouseEventArgs e)
        {
            if (this.ChangerWaku.diffX < -1 && this.Waku.Padding.RightSpace > 0)
            {
                this.TopExtension(e, false, CaptureDirection.Right, this.ChangerWaku.diffX);
            }
            else if (this.ChangerWaku.diffX > 1)
            {
                this.TopExtension(e, true, CaptureDirection.Right, this.ChangerWaku.diffX);
            }

            this.PopUpBox.Show(this.Waku.Padding.RightSpace.ToString(), this.Layer, this.Layer.Width, 0, 1000);
            this.ParentTextBox.UpdateText();
            this.PictureLayer.RefreshPictureBox();
        }

        private void TopExtension(MouseEventArgs e, bool up, CaptureDirection d, int n)
        {

            var v = this.Waku;
            Point p = Point.Empty;
            //int n = up ? 1:-1;
            switch (d)
            {
                case CaptureDirection.Up:
                    if (v.Padding.TopSpace + n < 0) return;
                    v.Padding.TopSpace += n;
                    this.Layer.EditLocation = new EditPoint(this.Layer.EditLocation.X, this.Layer.EditLocation.Y - n);
                    p = new Point(e.X, e.Y + n);
                    break;
                case CaptureDirection.Left:
                    if (v.Padding.LeftSpace + n < 0) return;
                    v.Padding.LeftSpace += n;
                    this.Layer.EditLocation = new EditPoint(this.Layer.EditLocation.X - n, this.Layer.EditLocation.Y);
                    p = new Point(e.X + n, e.Y);
                    break;
                case CaptureDirection.Right:
                    if (v.Padding.RightSpace + n < 0) return;
                    v.Padding.RightSpace += n;
                    this.Layer.EditLocation = new EditPoint(this.Layer.EditLocation.X, this.Layer.EditLocation.Y);
                    p = e.Location;
                    break;
                case CaptureDirection.Down:
                    if (v.Padding.BottomSpace + n < 0) return;
                    v.Padding.BottomSpace += n;
                    p = e.Location;
                    break;
            }

            this.Layer.BackgroundPage.Waku = v;
            


            this.ChangerWaku.Refresh(p);
            this.Layer.PictureResize();
        }
    }
}
