﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouBase;
using SuouBase.Text;
using SuouTextBox2.Controls;
using SuouTextBox2;
using System.Drawing;

namespace SuouTextBox2.Controls
{
    [IComponentPlugin("LayerSupervisorPlugin", PluginType.Control)]
    public class LayerSupervisorPlugin:IComponentPlugin
    {
        public LayerSupervisorBox HaveControl;

        public EditorInfo DLLFunction { get; set; }
        public LayerSupervisorPlugin(EditorInfo DLLFunction)
        {
            HaveControl = new LayerSupervisorBox();
            this.DLLFunction = DLLFunction;
            this.HaveControl.DLLFunction = this.DLLFunction;
            this.DLLFunction.StartSettingEnded += this.SimoziBoxNotExisted;
            //this.HaveControl.SimoziItemNotExisted += this.SimoziBoxNotExisted;
            
        }
        public Size CompactSize
        {
            get
            {
                return new Size(300, 500);
            }
        }
        public string Name
        {
            get
            {
                return "レイヤー";
            }
        }
        private void SimoziBoxNotExisted(object sender,EventArgs e)
        {
            if (DLLFunction.TextBoxPlugin != null)
            {
                if (DLLFunction.TextBoxPlugin.Control is SimoziBox)
                {
                    this.HaveControl.SimoziBoxItem = (SimoziBox)DLLFunction.TextBoxPlugin.Control;
                    DLLFunction.TextBoxPlugin.Control.VisibleChanged += this.TextBox_VisibleChanged;
                    this.TextBox = DLLFunction.TextBoxPlugin.Control;
                    
                }
            }
        }
        private ContainerControl TextBox { get; set; }
        public bool DisposeCheck()
        {
            return true;
        }
        private void TextBox_VisibleChanged(object sender, EventArgs e)
        {
            
            this.HaveControl.InitializeLayerView();
        }
        public Form Form { get { return null; } }
        public ContainerControl Control { get { return this.HaveControl; } }
        public SettingPage[] SettingPage { get { return null; } }
    }
}
