﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;
using SuouBase;
using SuouBase.Interface;
using SuouBase.Book;
namespace SuouTextBox2.Controls
{
    public partial class LayerSupervisorBox : UserControl
    {
        #region Property
        private SimoziBox _SimoziBoxItem = null;
        public SimoziBox SimoziBoxItem
        {
            get
            {
                if (this._SimoziBoxItem == null)
                {
                    var p = this.DLLFunction.TextBoxPlugin as SuouTextBoxPlugin;
                    
                    this._SimoziBoxItem =  p.TextBox;
                    if (this._SimoziBoxItem == null) throw new ArgumentNullException();
                    //this.SimoziItemNotExisted(this, null);
                }

                return this._SimoziBoxItem;
            }
            set
            {
                _SimoziBoxItem = value;
                if (_SimoziBoxItem != null)
                {
                    this.InitializeLayerView();
                    this.SimoziBoxItem.MergeLayerItemCollection.LayerItemChanged += this.SimoziBox_LayerItemsChanged;
                    this.SimoziBoxItem.MergeLayerItemCollection.SelectedLayerItemChanged += this.SelectedLayerItem_Changed;
                    this.SimoziBoxItem.PageChanged += (a, b) => this.Page = this.SimoziBoxItem.MergeLayerItemCollection.Scene;
                }
            }
        }

        public EventHandler SimoziItemNotExisted;
        public EditorInfo DLLFunction { get; set; }
        public bool LayerWindowEnable
        {
            get { return this.listView1.Enabled; }
            set { this.listView1.Enabled = value; }
        }
        public bool IsCheckEvent { get; set; }
        public SectionScene _Page = null;
        public SectionScene Page
        {
            get { return this._Page; }
            set
            {
                if(this._Page != null)this._Page.EditStateChanged -= this.PageItemCount_Change;
                this._Page = value;
                if (this._Page != null) this._Page.EditStateChanged += this.PageItemCount_Change;
                this.GetLayerItems();
            }
        }

        private void PageItemCount_Change(object sender, BookPageContentChangeEventArgs e)
        {
            if (e.Type == PageContentChangeType.TextPartsCount)
            {
                this.GetLayerItems();
            }
            else
            {

            }
        }
        #endregion
        private void SimoziLoad()
        {
            
           

        }

        private void SelectedLayerItem_Changed(object sender, SelectedLayerItemEventArgs e)
        {
            var f = this.LayerItems;
            if (f == null || this.listView1.Items.Count != f.Length) return;

            for (int i = 0; i < f.Length; i++)
            {
                if (f[i] == e.Item)
                {
                    this.listView1.Items[i].Focused = true;
                    this.listView1.Items[i].Selected = true;
                }
                else
                {
                    this.listView1.Items[i].Focused = false;
                    this.listView1.Items[i].Selected = false;
                }
            }
        }

        #region Constructor
        public LayerSupervisorBox()
        {
            InitializeComponent();
            this.CheckOffOption();
        }
        #endregion


        #region Method
        public LayerItem[] LayerItems
        {
            get {
                if (this.SimoziBoxItem.Visible)
                {
                    return this.SimoziBoxItem.MergeLayerItemCollection.SimoziAndLayerItems;
                }
                else
                {
                    return null;
                }
            }
        }

        private void RenewMulti()
        {
            if (this.SimoziBoxItem.MergeLayerItemCollection.SimoziMode == SimoziMode.Single)
            {
            }
            else if (this.SimoziBoxItem.MergeLayerItemCollection.SimoziMode == SimoziMode.Multi)
            {
            }
        }
        public void InitializeLayerView()
        {
            this.Page = this.SimoziBoxItem.MergeLayerItemCollection.Scene;
            //this.GetLayerItems();
        }
        public int LayerListSelectedIndex
        {
            get
            {
                var f = this.listView1.SelectedIndices;
                if (f.Count == 0)
                {
                    return -1;
                }
                else
                {
                    return f[0];
                }
            }
        }
        private object lockobject = new object();
        private bool GetLayersLocking = false;
        public void GetLayerItems()
        {
            System.Console.WriteLine("Setting");
            GetLayersLocking = true;
            if (this.SimoziBoxItem == null) return;
            this.listView1.Items.Clear();
            if (this.SimoziBoxItem.ParentForm == null || !this.SimoziBoxItem.Visible)
            {
                this.listView1.Enabled = false;
            }
            else if(this.listView1.Items.Count == 0)
            {
                IsCheckEvent = false;
                this.listView1.Enabled = true;
                
                var r = SimoziBoxItem.MergeLayerItemCollection.SimoziAndLayerItems;
                foreach (var v in this.Page.Parts)
                {
                    var w = new ListViewItem(v.Name) { Checked = v.Enable };
                    w.Tag = v;
                    this.listView1.Items.Add(w);
                }
                IsCheckEvent = true;
            }
            GetLayersLocking = false;

        }
        private void SharrowShowLayer(int number)
        {
            for (int i = 0; i < this.SimoziBoxItem.MergeLayerItemCollection.SimoziAndLayerItems.Count(); i++)
            {
                if (i == number)
                {
                    this.SimoziBoxItem.MergeLayerItemCollection.SimoziAndLayerItems[i].SharrowShow();
                }
                else
                {
                    this.SimoziBoxItem.MergeLayerItemCollection.SimoziAndLayerItems[i].SharrowHide();
                }
            }
        }
        #endregion
        #region EventMethod
        private void LayerSupervisorBox_VisibleChanged(object sender, EventArgs e)
        {
            if(this.Visible && this.Parent != null)
            {
                this.flag = false;
            }
            System.Console.WriteLine("Visivle CHanged" + this.Visible);
            this.InitializeLayerView();
        }
        private void listView1_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!IsCheckEvent) return;
            if (this.flag) return;
            var f = (SectionScenePart)e.Item.Tag;

            if (e.Item == this.listView1.Items[0])
            {
                if (!e.Item.Checked)
                {
                    //MessageBox.Show("背景の非表示はできません");
                    e.Item.Checked = true;
                }
            }
            if (f.Enable != e.Item.Checked)
            {
                f.Enable = e.Item.Checked;
                this.SimoziBoxItem.UpdateText();
            }
        }
        private void listView1_ItemCheck(object sender, ItemCheckEventArgs e)
        {

        }
        private void SimoziBox_LayerItemsChanged(object sender, EventArgs e)
        {
            this.GetLayerItems();
        }
        private void listView1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                if (this.listView1.SelectedIndices.Count != 0)
                {
                    //var n = this.listView1.SelectedIndices[0];
                    contextMenuStrip1.Show(Control.MousePosition);
                }
            }
        }
        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            if (this.LayerListSelectedIndex <= 0)
            {
                this.upToolStripMenuItem.Enabled = false;
                this.downToolStripMenuItem.Enabled = false;
                this.deleteToolStripMenuItem.Enabled = false;
            }
            else
            {
                this.upToolStripMenuItem.Enabled = this.LayerListSelectedIndex != 1;
                this.downToolStripMenuItem.Enabled = this.LayerListSelectedIndex < this.Page.Parts.Count-1;
                this.deleteToolStripMenuItem.Enabled = true;
            }
        }
        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.listView1.SelectedIndices.Count != 0)
            {
                var n = this.listView1.SelectedIndices[0];
                SharrowShowLayer(n);
            }
        }

        

        #endregion
        #region ButtonEvent
        private void upToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedIndices.Count != 0)
            {
                var n = this.listView1.SelectedIndices[0];
                this.Page.Parts.Move(n - 1, n);
                //this.SimoziBoxItem.PageItems.MoveDown(n - 1);
                //GetLayerItems();
                //this.listView1.Items[n-1].Selected = true;
            }
        }
        private void downToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.listView1.SelectedIndices.Count != 0)
            {
                var n = this.listView1.SelectedIndices[0];
                this.Page.Parts.Move(n, n+1);
                
                //this.SimoziBoxItem.PageItems.MoveDown(n);
                //GetLayerItems();
                //this.listView1.Items[n + 1].Selected = true;
            }
        }
        /*
        private void button1_Click(object sender, EventArgs e)
        {

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var strname = saveFileDialog1.FileName;
                throw new NotImplementedException();
                //SimoziBoxItem.PageItems.PageItem.Save(strname);
            }
        }
        private void button2_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                var f = SectionBookPage.Load(openFileDialog1.FileName);
                if (f != null)
                {
                    throw new NotImplementedException();
                    //SimoziBoxItem.PageItem = f;
                }
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            RenewMulti();
        }
        */
        private void addToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var page = this.SimoziBoxItem.MergeLayerItemCollection.CreateNewPage(new EditPoint(0, 0));
            this.Page.Parts.Add(page);
            //this.SimoziBoxItem.PageItems.AddNewLayer(new EditPoint(0,0));
            
        }
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var n = this.LayerListSelectedIndex;
            if (n != -1 && n != 0)
            {
                this.Page.Parts.RemoveAt(n);
                //this.SimoziBoxItem.PageItems.Remove(n);
            }
        }
        #endregion


        public void CheckOffOption()
        {
            this.groupBox2.Visible = false;
            this.NuritubusiBox.Visible = false;
        }
        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            this.SetKesigomuLength();
        }
        private void SetKesigomuLength()
        {
            this.SimoziBoxItem.PenItem = new SuouTextBox2.Operator.MonoMouse() { ChoiceLength = (int)this.numericUpDown1.Value };
            
        }

        private void PaintButton_Click(object sender, EventArgs e)
        {
            this.CheckOffOption();
            this.NuritubusiBox.Visible = true;
            this.ResetPaint();
        }

        private void CursorButton_Click(object sender, EventArgs e)
        {
            this.CheckOffOption();
            this.SimoziBoxItem.PenItem = null;
        }

        private void MonoButton_Click(object sender, EventArgs e)
        {
            this.CheckOffOption();
            this.groupBox2.Visible = true;
            
            this.SetKesigomuLength();
        }
        public bool IsTransParentize
        {
            get { return this.radioButton1.Checked; }
        }
        public string TankText
        {
            get
            {
                if (this.textBox1.Text.Length == 0)
                {
                    return "　";
                }
                else
                {
                    return this.textBox1.Text;
                }
            }
        }
        private void TankButton_Changed(object sender, EventArgs e)
        {
            this.ResetPaint();
        }
        private void ResetPaint()
        {
            this.SimoziBoxItem.PenItem = new SuouTextBox2.Operator.TankMouse(this.IsTransParentize, this.TankText);
            

        }

        private bool flag = false;
        private void LayerSupervisorBox_ParentChanged(object sender, EventArgs e)
        {
            if(this.Parent != null)
            {
                this.flag = true;
            }
        }
    }
    public interface LayerItem
    {
        bool IsSharrowHide { get; set; }
        SectionScenePart BackgroundPage { get; }
        void SharrowHide();
        void SharrowShow();
        SuouTextBox TextBox { get; }
        //int ShowNumber { get; set; }


        //SectionScenePart BackgroundPage { get; }
        void BringToFront();
        //event EventHandler DragMoveModeChanged;
        EditPoint EditLocation { get; set; }
        event EventHandler EditLocationChanged;
        event EventHandler Enter;
        //void EventDispose();
        bool IsPictureInnerMouse { get; set; }
        bool IsViewMode { get; }
        Point Location { get; set; }
        bool NeedMerge { get; set; }
        void SwitchPicture();
        //SuouTextBox TextBox { get; }
        bool Visible { get; set; }
        void RefreshPicture();
    }
    public enum LayerType
    {
        Character,Comment,Simozi
    }
}
