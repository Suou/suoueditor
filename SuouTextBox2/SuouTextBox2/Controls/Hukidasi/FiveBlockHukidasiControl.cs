﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;

namespace SuouTextBox2.Controls
{
    public partial class FiveBlockHukidasiControl : UserControl,IHukidasiControl
    {
        public FiveBlockHukidasiControl()
        {
            InitializeComponent();


        }
        public new event EventHandler TextChanged;
        private void CaretChanged(object sender, EventArgs e)
        {
            this.BoxCheck();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        public TextBox[] TextBoxs
        {
            get
            {
                return new TextBox[] { tb11, tb13, tb15, tb31, tb35, tb51, tb53, tb55, tb12, tb14, tb21, tb25, tb41, tb45, tb52, tb54 };
            }
        }
        public string[] WakuString
        {
            get
            {
                return this.TextBoxs.Select((v) => v.Text).ToArray();
            }
            set
            {
                if (value.Length >= 8)
                {
                    tb11.Text = value[0]; tb13.Text = value[1]; tb15.Text = value[2];
                    tb31.Text = value[3]; tb35.Text = value[4];
                    tb51.Text = value[5]; tb53.Text = value[6]; tb55.Text = value[7];
                }
                if (value.Length >= 16)
                {
                    tb12.Text = value[8]; tb14.Text = value[9];
                    tb21.Text = value[10]; tb25.Text = value[11];
                    tb41.Text = value[12]; tb45.Text = value[13];
                    tb52.Text = value[14]; tb54.Text = value[15];
                }
                
            }
        }
        public void SetItem(HukidasiSaveData item)
        {
            this.WakuString = item.WakuItems.Select((v)=>v.Text).ToArray();
            this.HukidasiItem = item;
        }
        HukidasiSaveData HukidasiItem { get; set; }
        public HukidasiSaveData GetItem()
        {

            throw new NotSupportedException();
            /*
            var f = HukidasiSaveData.FiveDefault();
            var wakus = this.WakuString.Select((v)=>new SuouText(v)).ToList();
            return new HukidasiSaveData(wakus,f.SampleText,HukidasiType.FiveBlock,f.ZureOption,f.PermitZureLength,f.MergeAlgorithm,f.IsTrimSpace);
            */
        }
        public void Modify(AltText at)
        {
            var ModifyData = this.GetItem();
            
        }
        public void BoxCheck()
        {
        }

        private void tb11_TextChanged(object sender, EventArgs e)
        {
            if (this.TextChanged != null) this.TextChanged(this, new EventArgs());   
        }
        public TextBox GetTextBox(HukidasiPart part)
        {
            return this.TextBoxs[HukidasiSaveData.GetIndex(part)];
        }
    }
}
