﻿namespace SuouTextBox2.Controls
{
    partial class HukidasiSetting
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.samplePictureButton = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ExpandEditButton = new System.Windows.Forms.Button();
            this.EditButton = new System.Windows.Forms.Button();
            this.registerButton = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.CenterSpaceNumeric = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.BottomSpaceNumeric = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.UpSpaceNumeric = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.RightSpaceNumeric = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LeftSpaceNumeric = new System.Windows.Forms.NumericUpDown();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.UCBNumeric = new System.Windows.Forms.NumericUpDown();
            this.HukidasiZureNumeric = new System.Windows.Forms.NumericUpDown();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.altCharControl1 = new SuouTextBox2.Controls.AltCharControl();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.samplePictureButton)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CenterSpaceNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomSpaceNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpSpaceNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightSpaceNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftSpaceNumeric)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UCBNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.HukidasiZureNumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(481, 319);
            this.tabControl1.TabIndex = 16;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.Linen;
            this.tabPage1.Controls.Add(this.samplePictureButton);
            this.tabPage1.Controls.Add(this.panel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(473, 258);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "フキダシ選択";
            // 
            // samplePictureButton
            // 
            this.samplePictureButton.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.samplePictureButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.samplePictureButton.Location = new System.Drawing.Point(3, 69);
            this.samplePictureButton.Name = "samplePictureButton";
            this.samplePictureButton.Size = new System.Drawing.Size(467, 98);
            this.samplePictureButton.TabIndex = 18;
            this.samplePictureButton.TabStop = false;
            this.samplePictureButton.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.ExpandEditButton);
            this.panel1.Controls.Add(this.EditButton);
            this.panel1.Controls.Add(this.registerButton);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.comboBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 66);
            this.panel1.TabIndex = 19;
            // 
            // ExpandEditButton
            // 
            this.ExpandEditButton.Location = new System.Drawing.Point(392, 7);
            this.ExpandEditButton.Name = "ExpandEditButton";
            this.ExpandEditButton.Size = new System.Drawing.Size(73, 23);
            this.ExpandEditButton.TabIndex = 20;
            this.ExpandEditButton.Text = "拡張編集";
            this.ExpandEditButton.UseVisualStyleBackColor = true;
            this.ExpandEditButton.Click += new System.EventHandler(this.ExpandEditButton_Click);
            // 
            // EditButton
            // 
            this.EditButton.Location = new System.Drawing.Point(313, 7);
            this.EditButton.Name = "EditButton";
            this.EditButton.Size = new System.Drawing.Size(73, 23);
            this.EditButton.TabIndex = 19;
            this.EditButton.Text = "編集";
            this.EditButton.UseVisualStyleBackColor = true;
            this.EditButton.Click += new System.EventHandler(this.EditButton_Click);
            // 
            // registerButton
            // 
            this.registerButton.Location = new System.Drawing.Point(89, 7);
            this.registerButton.Name = "registerButton";
            this.registerButton.Size = new System.Drawing.Size(218, 23);
            this.registerButton.TabIndex = 18;
            this.registerButton.Text = "このフキダシをリストに追加する";
            this.registerButton.UseVisualStyleBackColor = true;
            this.registerButton.Click += new System.EventHandler(this.RegisterButton_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(8, 7);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 16;
            this.button1.Text = "フキダシ選択";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "枠なし型",
            "素朴型",
            "3ブロック型",
            "5ブロック型"});
            this.comboBox1.Location = new System.Drawing.Point(8, 36);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 20);
            this.comboBox1.TabIndex = 14;
            this.comboBox1.Text = "型";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.Khaki;
            this.tabPage2.Controls.Add(this.groupBox3);
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(473, 293);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "詳細設定";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.CenterSpaceNumeric);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.BottomSpaceNumeric);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.UpSpaceNumeric);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.RightSpaceNumeric);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.LeftSpaceNumeric);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(3, 181);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(467, 96);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "セリフ設定";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(139, 70);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 12);
            this.label9.TabIndex = 17;
            this.label9.Text = "行空ける";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 70);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 12);
            this.label10.TabIndex = 16;
            this.label10.Text = "行間を";
            // 
            // CenterSpaceNumeric
            // 
            this.CenterSpaceNumeric.Location = new System.Drawing.Point(59, 67);
            this.CenterSpaceNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.CenterSpaceNumeric.Name = "CenterSpaceNumeric";
            this.CenterSpaceNumeric.Size = new System.Drawing.Size(74, 19);
            this.CenterSpaceNumeric.TabIndex = 15;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(363, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "行空ける";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(239, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "下端を";
            // 
            // BottomSpaceNumeric
            // 
            this.BottomSpaceNumeric.Location = new System.Drawing.Point(283, 14);
            this.BottomSpaceNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.BottomSpaceNumeric.Name = "BottomSpaceNumeric";
            this.BottomSpaceNumeric.Size = new System.Drawing.Size(74, 19);
            this.BottomSpaceNumeric.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(139, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 12);
            this.label7.TabIndex = 11;
            this.label7.Text = "行空ける";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(15, 17);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 12);
            this.label8.TabIndex = 10;
            this.label8.Text = "上端を";
            // 
            // UpSpaceNumeric
            // 
            this.UpSpaceNumeric.Location = new System.Drawing.Point(59, 14);
            this.UpSpaceNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.UpSpaceNumeric.Name = "UpSpaceNumeric";
            this.UpSpaceNumeric.Size = new System.Drawing.Size(74, 19);
            this.UpSpaceNumeric.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(363, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "dot空ける";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(239, 42);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 12);
            this.label4.TabIndex = 7;
            this.label4.Text = "右端を";
            // 
            // RightSpaceNumeric
            // 
            this.RightSpaceNumeric.Location = new System.Drawing.Point(283, 39);
            this.RightSpaceNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.RightSpaceNumeric.Name = "RightSpaceNumeric";
            this.RightSpaceNumeric.Size = new System.Drawing.Size(74, 19);
            this.RightSpaceNumeric.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(139, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "dot空ける";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "左端を";
            // 
            // LeftSpaceNumeric
            // 
            this.LeftSpaceNumeric.Location = new System.Drawing.Point(59, 39);
            this.LeftSpaceNumeric.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.LeftSpaceNumeric.Name = "LeftSpaceNumeric";
            this.LeftSpaceNumeric.Size = new System.Drawing.Size(74, 19);
            this.LeftSpaceNumeric.TabIndex = 3;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.checkBox1);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.UCBNumeric);
            this.groupBox2.Controls.Add(this.HukidasiZureNumeric);
            this.groupBox2.Controls.Add(this.radioButton4);
            this.groupBox2.Controls.Add(this.numericUpDown1);
            this.groupBox2.Controls.Add(this.radioButton3);
            this.groupBox2.Controls.Add(this.radioButton2);
            this.groupBox2.Controls.Add(this.radioButton1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(467, 178);
            this.groupBox2.TabIndex = 15;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "ズレ設定";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(36, 129);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(82, 12);
            this.label12.TabIndex = 10;
            this.label12.Text = "上中下段のズレ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(36, 109);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(70, 12);
            this.label11.TabIndex = 9;
            this.label11.Text = "中段中のズレ";
            // 
            // UCBNumeric
            // 
            this.UCBNumeric.Location = new System.Drawing.Point(137, 127);
            this.UCBNumeric.Name = "UCBNumeric";
            this.UCBNumeric.Size = new System.Drawing.Size(66, 19);
            this.UCBNumeric.TabIndex = 8;
            // 
            // HukidasiZureNumeric
            // 
            this.HukidasiZureNumeric.Location = new System.Drawing.Point(137, 102);
            this.HukidasiZureNumeric.Name = "HukidasiZureNumeric";
            this.HukidasiZureNumeric.Size = new System.Drawing.Size(66, 19);
            this.HukidasiZureNumeric.TabIndex = 6;
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(17, 40);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(219, 16);
            this.radioButton4.TabIndex = 4;
            this.radioButton4.Text = "合成時の行のズレを許さない破壊的合成";
            this.radioButton4.UseVisualStyleBackColor = true;
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(210, 81);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(66, 19);
            this.numericUpDown1.TabIndex = 3;
            this.numericUpDown1.Visible = false;
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(17, 84);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(186, 16);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "合成時の行のズレをある程度許容";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.Visible = false;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(17, 62);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(231, 16);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "合成時の行のズレを許さない非破壊的合成";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.Visible = false;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(17, 18);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(143, 16);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "合成時の行のズレを許容";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.LightGoldenrodYellow;
            this.tabPage3.Controls.Add(this.altCharControl1);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(473, 258);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "内部合成アルゴリズム";
            // 
            // altCharControl1
            // 
            this.altCharControl1.AddOptionVisible = false;
            this.altCharControl1.AutoSize = true;
            this.altCharControl1.ControlName = "空白合成アルゴリズム設定(フキダシ内部)";
            this.altCharControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.altCharControl1.IsEnableExpansion = false;
            this.altCharControl1.IsRegister = false;
            this.altCharControl1.Item = null;
            this.altCharControl1.ItemName = null;
            this.altCharControl1.Location = new System.Drawing.Point(0, 0);
            this.altCharControl1.MinCalculate = 0;
            this.altCharControl1.Name = "altCharControl1";
            this.altCharControl1.PermitRenzokuDirty = 0;
            this.altCharControl1.SaveItems = null;
            this.altCharControl1.Size = new System.Drawing.Size(473, 212);
            this.altCharControl1.TabIndex = 21;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(38, 148);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(169, 16);
            this.checkBox1.TabIndex = 11;
            this.checkBox1.Text = "両端の余分な空白は削除する";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // HukidasiSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.tabControl1);
            this.Name = "HukidasiSetting";
            this.Size = new System.Drawing.Size(481, 324);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.samplePictureButton)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CenterSpaceNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomSpaceNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.UpSpaceNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightSpaceNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftSpaceNumeric)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UCBNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.HukidasiZureNumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox samplePictureButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button ExpandEditButton;
        private System.Windows.Forms.Button EditButton;
        private System.Windows.Forms.Button registerButton;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown CenterSpaceNumeric;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown BottomSpaceNumeric;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown UpSpaceNumeric;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown RightSpaceNumeric;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown LeftSpaceNumeric;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown UCBNumeric;
        private System.Windows.Forms.NumericUpDown HukidasiZureNumeric;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.TabPage tabPage3;
        private AltCharControl altCharControl1;
        private System.Windows.Forms.CheckBox checkBox1;

    }
}
