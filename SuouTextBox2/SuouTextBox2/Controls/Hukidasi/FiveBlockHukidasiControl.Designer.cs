﻿namespace SuouTextBox2.Controls
{
    partial class FiveBlockHukidasiControl
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tb11 = new System.Windows.Forms.TextBox();
            this.tb12 = new System.Windows.Forms.TextBox();
            this.tb13 = new System.Windows.Forms.TextBox();
            this.tb14 = new System.Windows.Forms.TextBox();
            this.tb15 = new System.Windows.Forms.TextBox();
            this.tb21 = new System.Windows.Forms.TextBox();
            this.tb31 = new System.Windows.Forms.TextBox();
            this.tb41 = new System.Windows.Forms.TextBox();
            this.tb51 = new System.Windows.Forms.TextBox();
            this.tb52 = new System.Windows.Forms.TextBox();
            this.tb53 = new System.Windows.Forms.TextBox();
            this.tb54 = new System.Windows.Forms.TextBox();
            this.tb55 = new System.Windows.Forms.TextBox();
            this.tb25 = new System.Windows.Forms.TextBox();
            this.tb35 = new System.Windows.Forms.TextBox();
            this.tb45 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tb11
            // 
            this.tb11.AcceptsReturn = true;
            this.tb11.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb11.Location = new System.Drawing.Point(12, 12);
            this.tb11.Multiline = true;
            this.tb11.Name = "tb11";
            this.tb11.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb11.Size = new System.Drawing.Size(132, 108);
            this.tb11.TabIndex = 0;
            this.tb11.WordWrap = false;
            this.tb11.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb12
            // 
            this.tb12.AcceptsReturn = true;
            this.tb12.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb12.Location = new System.Drawing.Point(150, 12);
            this.tb12.Multiline = true;
            this.tb12.Name = "tb12";
            this.tb12.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb12.Size = new System.Drawing.Size(132, 108);
            this.tb12.TabIndex = 1;
            this.tb12.WordWrap = false;
            this.tb12.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb13
            // 
            this.tb13.AcceptsReturn = true;
            this.tb13.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb13.Location = new System.Drawing.Point(288, 12);
            this.tb13.Multiline = true;
            this.tb13.Name = "tb13";
            this.tb13.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb13.Size = new System.Drawing.Size(132, 108);
            this.tb13.TabIndex = 2;
            this.tb13.WordWrap = false;
            this.tb13.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb14
            // 
            this.tb14.AcceptsReturn = true;
            this.tb14.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb14.Location = new System.Drawing.Point(426, 12);
            this.tb14.Multiline = true;
            this.tb14.Name = "tb14";
            this.tb14.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb14.Size = new System.Drawing.Size(132, 108);
            this.tb14.TabIndex = 3;
            this.tb14.WordWrap = false;
            this.tb14.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb15
            // 
            this.tb15.AcceptsReturn = true;
            this.tb15.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb15.Location = new System.Drawing.Point(564, 12);
            this.tb15.Multiline = true;
            this.tb15.Name = "tb15";
            this.tb15.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb15.Size = new System.Drawing.Size(132, 108);
            this.tb15.TabIndex = 4;
            this.tb15.WordWrap = false;
            this.tb15.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb21
            // 
            this.tb21.AcceptsReturn = true;
            this.tb21.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb21.Location = new System.Drawing.Point(12, 126);
            this.tb21.Multiline = true;
            this.tb21.Name = "tb21";
            this.tb21.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb21.Size = new System.Drawing.Size(132, 108);
            this.tb21.TabIndex = 5;
            this.tb21.WordWrap = false;
            this.tb21.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb31
            // 
            this.tb31.AcceptsReturn = true;
            this.tb31.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb31.Location = new System.Drawing.Point(12, 240);
            this.tb31.Multiline = true;
            this.tb31.Name = "tb31";
            this.tb31.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb31.Size = new System.Drawing.Size(132, 108);
            this.tb31.TabIndex = 6;
            this.tb31.WordWrap = false;
            this.tb31.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb41
            // 
            this.tb41.AcceptsReturn = true;
            this.tb41.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb41.Location = new System.Drawing.Point(12, 354);
            this.tb41.Multiline = true;
            this.tb41.Name = "tb41";
            this.tb41.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb41.Size = new System.Drawing.Size(132, 108);
            this.tb41.TabIndex = 7;
            this.tb41.WordWrap = false;
            this.tb41.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb51
            // 
            this.tb51.AcceptsReturn = true;
            this.tb51.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb51.Location = new System.Drawing.Point(12, 468);
            this.tb51.Multiline = true;
            this.tb51.Name = "tb51";
            this.tb51.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb51.Size = new System.Drawing.Size(132, 108);
            this.tb51.TabIndex = 8;
            this.tb51.WordWrap = false;
            this.tb51.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb52
            // 
            this.tb52.AcceptsReturn = true;
            this.tb52.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb52.Location = new System.Drawing.Point(150, 468);
            this.tb52.Multiline = true;
            this.tb52.Name = "tb52";
            this.tb52.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb52.Size = new System.Drawing.Size(132, 108);
            this.tb52.TabIndex = 9;
            this.tb52.WordWrap = false;
            this.tb52.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb53
            // 
            this.tb53.AcceptsReturn = true;
            this.tb53.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb53.Location = new System.Drawing.Point(288, 468);
            this.tb53.Multiline = true;
            this.tb53.Name = "tb53";
            this.tb53.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb53.Size = new System.Drawing.Size(132, 108);
            this.tb53.TabIndex = 10;
            this.tb53.WordWrap = false;
            this.tb53.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb54
            // 
            this.tb54.AcceptsReturn = true;
            this.tb54.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb54.Location = new System.Drawing.Point(426, 468);
            this.tb54.Multiline = true;
            this.tb54.Name = "tb54";
            this.tb54.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb54.Size = new System.Drawing.Size(132, 108);
            this.tb54.TabIndex = 11;
            this.tb54.WordWrap = false;
            this.tb54.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb55
            // 
            this.tb55.AcceptsReturn = true;
            this.tb55.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb55.Location = new System.Drawing.Point(564, 468);
            this.tb55.Multiline = true;
            this.tb55.Name = "tb55";
            this.tb55.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb55.Size = new System.Drawing.Size(132, 108);
            this.tb55.TabIndex = 12;
            this.tb55.WordWrap = false;
            this.tb55.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb25
            // 
            this.tb25.AcceptsReturn = true;
            this.tb25.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb25.Location = new System.Drawing.Point(564, 126);
            this.tb25.Multiline = true;
            this.tb25.Name = "tb25";
            this.tb25.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb25.Size = new System.Drawing.Size(132, 108);
            this.tb25.TabIndex = 13;
            this.tb25.WordWrap = false;
            this.tb25.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb35
            // 
            this.tb35.AcceptsReturn = true;
            this.tb35.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb35.Location = new System.Drawing.Point(564, 240);
            this.tb35.Multiline = true;
            this.tb35.Name = "tb35";
            this.tb35.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb35.Size = new System.Drawing.Size(132, 108);
            this.tb35.TabIndex = 14;
            this.tb35.WordWrap = false;
            this.tb35.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // tb45
            // 
            this.tb45.AcceptsReturn = true;
            this.tb45.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb45.Location = new System.Drawing.Point(564, 354);
            this.tb45.Multiline = true;
            this.tb45.Name = "tb45";
            this.tb45.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tb45.Size = new System.Drawing.Size(132, 108);
            this.tb45.TabIndex = 15;
            this.tb45.WordWrap = false;
            this.tb45.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // FiveBlockHukidasiControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tb45);
            this.Controls.Add(this.tb35);
            this.Controls.Add(this.tb25);
            this.Controls.Add(this.tb55);
            this.Controls.Add(this.tb54);
            this.Controls.Add(this.tb53);
            this.Controls.Add(this.tb52);
            this.Controls.Add(this.tb51);
            this.Controls.Add(this.tb41);
            this.Controls.Add(this.tb31);
            this.Controls.Add(this.tb21);
            this.Controls.Add(this.tb15);
            this.Controls.Add(this.tb14);
            this.Controls.Add(this.tb13);
            this.Controls.Add(this.tb12);
            this.Controls.Add(this.tb11);
            this.Name = "FiveBlockHukidasiControl";
            this.Size = new System.Drawing.Size(704, 591);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb11;
        private System.Windows.Forms.TextBox tb12;
        private System.Windows.Forms.TextBox tb13;
        private System.Windows.Forms.TextBox tb14;
        private System.Windows.Forms.TextBox tb15;
        private System.Windows.Forms.TextBox tb21;
        private System.Windows.Forms.TextBox tb31;
        private System.Windows.Forms.TextBox tb41;
        private System.Windows.Forms.TextBox tb51;
        private System.Windows.Forms.TextBox tb52;
        private System.Windows.Forms.TextBox tb53;
        private System.Windows.Forms.TextBox tb54;
        private System.Windows.Forms.TextBox tb55;
        private System.Windows.Forms.TextBox tb25;
        private System.Windows.Forms.TextBox tb35;
        private System.Windows.Forms.TextBox tb45;

    }
}
