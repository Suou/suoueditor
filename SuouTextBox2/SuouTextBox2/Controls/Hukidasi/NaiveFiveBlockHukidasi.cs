﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;
namespace SuouTextBox2.Controls
{
    public partial class NaiveFiveBlockHukidasi : UserControl,IHukidasiControl
    {
        public NaiveFiveBlockHukidasi()
        {
            InitializeComponent();
        }
        public new event EventHandler TextChanged;

        public TextBox[] TextBoxs
        {
            get
            {
                return new TextBox[] { tb11, tb13, tb15, tb31, tb35, tb51, tb53, tb55, tb12, tb14, tb21, tb25, tb41, tb45, tb52, tb54 };
            }
        }
        public void SetItem(HukidasiSaveData item)
        {
            int i = 0;
            this.TextBoxs.ToList().ForEach((v) => v.Text = item.WakuItems[i++].Text);
        }
        public HukidasiSaveData GetItem()
        {
            throw new NotSupportedException();
            /*
            var f = HukidasiSaveData.FiveDefault();
            var text = this.TextBoxs.Select((v) => new SuouText(v.Text)).ToList();
            var g = new HukidasiSaveData(text, f.SampleText, HukidasiType.FiveBlock, f.ZureOption, f.PermitZureLength, f.MergeAlgorithm,f.IsTrimSpace);
            
            return g;
             * */
        }
        public void Modify(AltText at)
        {
        }

        private void NaiveFiveBlockHukidasi_Load(object sender, EventArgs e)
        {

        }
        private void tb11_TextChanged(object sender, EventArgs e)
        {
            if (this.TextChanged != null) this.TextChanged(this, new EventArgs());
        }
        public TextBox GetTextBox(HukidasiPart part)
        {
            return this.TextBoxs[HukidasiSaveData.GetIndex(part)];
        }

    }
}
