﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using System.Drawing;
using BSystem.Collections;
using SuouBase.Text.Hukidasi.WakuLogic;
namespace SuouTextBox2.Controls.Hukidasi
{
    public interface IWakuParent
    {
        ListBox ErrorBox { get;}
        Button[] ButtonItems { get; }
        void Preview();
        WakuArrangement Periodics { get; set; }
        TextBox[] PeriodicBoxes { get; }

        CheckBox[] CheckBoxes { get; }
        IWakuOptionParent OptionBox { get; }
    }
    public interface IWakuOptionParent
    {
        NumericUpDown[] ValueBoxes { get; }
        
        string WakuName { get; set; }
        SpaceLineSet SelectedSpaceSet { get; }
        int UpCenterBottomValue { get; set; }
        int CenterZureLength { get; set; }
        WakuOption Item { get; set; }
    }
    public class WakuBoxCheck
    {
        public bool IsTrimSpace = true;
        public int[] Offsets
        {
            get
            {
                return this.OffsetBoxes.Select((v) => (int)v.Value).ToArray();
            }
            set
            {
                if (value != null)
                {
                    var p = this.OffsetBoxes.Zip(value);
                    p.ForEach((v) => v.Key.Value = v.Value);
                }
            }
        }
        private NumericUpDown[] _OffsetBoxes;
        public NumericUpDown[] OffsetBoxes
        {
            get { return this._OffsetBoxes; }
            set
            {
                this._OffsetBoxes = value;
                if (value != null)
                {
                    this._OffsetBoxes.ForEach((v) => v.ValueChanged += this.TextBox_TextChenged);
                }
            }
        }
        private TextBox[] _TextBoxItems = null;
        public TextBox[] TextBoxItems
        {
            get { return this._TextBoxItems; }
            set
            {
                this._TextBoxItems = value;
                this.TextBoxItems.ForEach(v => v.TextChanged += this.TextBox_TextChenged);
            }
        }
        public IWakuParent ParentBox { get; set; }
        public WakuBoxCheck(TextBox[] textboxes,Button[] items,ListBox list,NumericUpDown[] offsetBoxes,IWakuParent parent)
        {
            this.TextBoxItems = textboxes;
            this.OffsetBoxes = offsetBoxes;
            //this.ButtonItems = items;
            //this.ErrorBox = list;
            
            this.ParentBox = parent;
            this.ParentBox.PeriodicBoxes.ForEach((v) => v.TextChanged += this.TextBox_TextChenged);
            this.ParentBox.OptionBox.ValueBoxes.ForEach((v) => v.ValueChanged += this.TextBox_TextChenged);
            this.ParentBox.CheckBoxes.ForEach((v) => v.CheckedChanged += this.TextBox_TextChenged);
        }
        public TextBox[] SortedTextBoxItems
        {
            get
            {
                return HukidasiSaveData.Zyunban.Select((v) => this.TextBoxItems[GetIndex(v)]).ToArray();
            }
        }
        private bool EnableCheck = true;
        public void SetItem(HukidasiSaveData h)
        {
            this.EnableCheck = false;
            this.SortedTextBoxItems.Zip(h.WakuItems).ForEach((item) => item.Key.Text = item.Value.Text);
            this.Offsets = h.Elements.Offset;
            this.ParentBox.Periodics = h.Arrangement;
            this.ParentBox.OptionBox.Item = h.Options;
            this.EnableCheck = true;

            this.PreviewCheck();
        }
        public HukidasiSaveData GetItem()
        {
            var wakus = this.SortedTextBoxItems.Select((v) => new SuouText(v.Text)).ToArray();

            

            var f = HukidasiSaveData.FiveDefault();
            var gp = new HukidasiBaseSaveData()
            {
                //WakuItems = wakus,
                Elements = new WakuElementCollection(wakus,this.Offsets),
                Options = this.ParentBox.OptionBox.Item,
                Arrangement = this.ParentBox.Periodics
                //Offset = this.Offsets
            };
           var vg = new HukidasiSaveData(gp);
           return vg;
        }
        private void TextBox_TextChenged(object sender, EventArgs e)
        {
            if (this.EnableCheck)
            {
                this.PreviewCheck();
            }
        }
        private void PreviewCheck()
        {
            var b = this.Check();
            if (b) this.ParentBox.Preview();
        }
        public bool Check()
        {

            var f = this.GetItem();
            var error = f.CreateCheckData();
            foreach (var v in error.ErrorIterator())
            {
                this.TextBoxItems[WakuBoxCheck.GetIndex(v.Key)].BackColor = v.Value ? Color.White : Color.Red;

            }
            this.ParentBox.ErrorBox.Items.Clear();
            error.GetAllErrorMessage().ForEach((v) => this.ParentBox.ErrorBox.Items.Add(v));

            //this.Delete();
            //error.ErrorParts.ForEach((v) => this.ShowPopup(v, error.Dic[v]));
            //this.OKbutton.Enabled = error.IsError;
            //this.textBox1.Text = error.GetErrorMessage();
            this.ParentBox.ButtonItems.ForEach((v) => v.Enabled = error.IsNoError);
            return error.IsNoError;
        }
        /*
        public void ShowPopup(HukidasiPart part, string msg)
        {
            var v = new ToolTip();
            if (VisibleErrorMessage)
            {
                v.Show(msg, this.TextBoxItems[GetIndex(part)], 0, 0, 5000);
                this.Dics[part] = v;
            }
            
        }
        
        public Dictionary<HukidasiPart, ToolTip> Dics = new Dictionary<HukidasiPart, ToolTip>();
        public void Delete()
        {
            foreach (var item in this.Dics)
            {
                item.Value.Hide(this.TextBoxItems[GetIndex(item.Key)]);
            }
            this.Dics.Clear();
        }
        */
        public bool VisibleErrorMessage { get; set; }
        public static int GetIndex(HukidasiPart part)
        {
            switch (part)
            {
                case HukidasiPart.LeftUpCorner: return 0;
                case HukidasiPart.UpCenterLeft: return 1;
                case HukidasiPart.UpCenter: return 2;
                case HukidasiPart.UpCenterRight: return 3;
                case HukidasiPart.RightUpCorner: return 4;
                case HukidasiPart.LeftCenterUp: return 5;
                case HukidasiPart.RightCenterUp: return 6;
                case HukidasiPart.LeftCenter: return 7;
                case HukidasiPart.RightCenter: return 8;
                case HukidasiPart.LeftCenterDown: return 9;
                case HukidasiPart.RightCenterDown: return 10;
                case HukidasiPart.LeftDownCorner: return 11;
                case HukidasiPart.DownCenterLeft: return 12;
                case HukidasiPart.DownCenter: return 13;
                case HukidasiPart.DownCenterRight: return 14;
                case HukidasiPart.RightDownCorner: return 15;
            }
            return 0;
        }
    }
}
