﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;
using SuouBase;
using BSystem;
using BSystem.Text;
using BSystem.Collections;
namespace SuouTextBox2.Controls
{
    public partial class NaiveHukidasi : UserControl,IHukidasiControl
    {
        public NaiveHukidasi()
        {
            InitializeComponent();
        }
        public new event EventHandler TextChanged;
        public TextBox[] TextBoxs
        {
            get
            {
                return new TextBox[] { tb11, tb12,tb13, tb21, tb22, tb31,tb32,tb33 };
            }
        }
        public HukidasiSaveData GetItem()
        {
            HukidasiSaveData r = HukidasiSaveData.ThreeDefault();
            var f = this.TextBoxs.Select((v) => new SuouText(v.Text)).ToArray();

            var p = new HukidasiBaseSaveData()
            {
                //WakuItems = f,
                Elements = new WakuElementCollection(f,null),
                Options = new WakuOption()
                {
                    Type = HukidasiType.Block,
                    //ZureOption = MergeZureOption.Free,
                    //PermitZureLength = r.PermitZureLength,
                    MergeAlgorithm = r.MergeAlgorithm,
                    IsTrimSpace = r.IsTrimSpace
                }
            };
            
            
            //var g = new HukidasiSaveData(f,r.SampleText, HukidasiType.Block, HukidasiZureOption.Free, r.PermitZureLength, r.MergeAlgorithm,r.IsTrimSpace);
            return new HukidasiSaveData(p);
        }
        public void SetItem(HukidasiSaveData h)
        {
            HukidasiPartExtension.EightParts().ForEach((v) =>
            {
                var i = HukidasiSaveData.GetIndex(v);
                this.TextBoxs[i].Text = h[v].Text;
            });
        }
        public void Modify(AltText at)
        {
        }
        private void tb11_TextChanged(object sender, EventArgs e)
        {
            if (this.TextChanged != null) this.TextChanged(this, new EventArgs());
        }
        public TextBox GetTextBox(HukidasiPart part)
        {
            var n = HukidasiSaveData.GetIndex(part);
            if (this.TextBoxs.Length > n)
            {
                return this.TextBoxs[HukidasiSaveData.GetIndex(part)];
            }
            else
            {
                return null;
            }
        
        }
    }
}
