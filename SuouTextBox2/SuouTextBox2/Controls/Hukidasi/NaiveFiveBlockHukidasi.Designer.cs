﻿namespace SuouTextBox2.Controls
{
    partial class NaiveFiveBlockHukidasi
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tb55 = new System.Windows.Forms.TextBox();
            this.tb45 = new System.Windows.Forms.TextBox();
            this.tb35 = new System.Windows.Forms.TextBox();
            this.tb25 = new System.Windows.Forms.TextBox();
            this.tb54 = new System.Windows.Forms.TextBox();
            this.tb15 = new System.Windows.Forms.TextBox();
            this.tb14 = new System.Windows.Forms.TextBox();
            this.tb13 = new System.Windows.Forms.TextBox();
            this.tb12 = new System.Windows.Forms.TextBox();
            this.tb53 = new System.Windows.Forms.TextBox();
            this.tb52 = new System.Windows.Forms.TextBox();
            this.tb51 = new System.Windows.Forms.TextBox();
            this.tb41 = new System.Windows.Forms.TextBox();
            this.tb31 = new System.Windows.Forms.TextBox();
            this.tb21 = new System.Windows.Forms.TextBox();
            this.tb11 = new System.Windows.Forms.TextBox();

            this.tb11.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb12.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb13.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb14.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb15.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb21.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb31.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb41.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb51.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb52.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb53.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb54.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb55.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb25.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb35.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            this.tb45.TextChanged += new System.EventHandler(this.tb11_TextChanged);

            this.SuspendLayout();
            // 
            // tb55
            // 
            this.tb55.AcceptsReturn = true;
            this.tb55.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb55.Location = new System.Drawing.Point(474, 160);
            this.tb55.Name = "tb55";
            this.tb55.Size = new System.Drawing.Size(100, 23);
            this.tb55.TabIndex = 32;
            // 
            // tb45
            // 
            this.tb45.AcceptsReturn = true;
            this.tb45.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb45.Location = new System.Drawing.Point(474, 124);
            this.tb45.Name = "tb45";
            this.tb45.Size = new System.Drawing.Size(100, 23);
            this.tb45.TabIndex = 31;
            // 
            // tb35
            // 
            this.tb35.AcceptsReturn = true;
            this.tb35.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb35.Location = new System.Drawing.Point(474, 87);
            this.tb35.Name = "tb35";
            this.tb35.Size = new System.Drawing.Size(100, 23);
            this.tb35.TabIndex = 30;
            // 
            // tb25
            // 
            this.tb25.AcceptsReturn = true;
            this.tb25.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb25.Location = new System.Drawing.Point(474, 53);
            this.tb25.Name = "tb25";
            this.tb25.Size = new System.Drawing.Size(100, 23);
            this.tb25.TabIndex = 29;
            // 
            // tb54
            // 
            this.tb54.AcceptsReturn = true;
            this.tb54.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb54.Location = new System.Drawing.Point(358, 160);
            this.tb54.Name = "tb54";
            this.tb54.Size = new System.Drawing.Size(100, 23);
            this.tb54.TabIndex = 28;
            // 
            // tb15
            // 
            this.tb15.AcceptsReturn = true;
            this.tb15.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb15.Location = new System.Drawing.Point(474, 17);
            this.tb15.Name = "tb15";
            this.tb15.Size = new System.Drawing.Size(100, 23);
            this.tb15.TabIndex = 27;
            // 
            // tb14
            // 
            this.tb14.AcceptsReturn = true;
            this.tb14.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb14.Location = new System.Drawing.Point(358, 17);
            this.tb14.Name = "tb14";
            this.tb14.Size = new System.Drawing.Size(100, 23);
            this.tb14.TabIndex = 26;
            // 
            // tb13
            // 
            this.tb13.AcceptsReturn = true;
            this.tb13.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb13.Location = new System.Drawing.Point(243, 17);
            this.tb13.Name = "tb13";
            this.tb13.Size = new System.Drawing.Size(100, 23);
            this.tb13.TabIndex = 25;
            // 
            // tb12
            // 
            this.tb12.AcceptsReturn = true;
            this.tb12.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb12.Location = new System.Drawing.Point(128, 17);
            this.tb12.Name = "tb12";
            this.tb12.Size = new System.Drawing.Size(100, 23);
            this.tb12.TabIndex = 24;
            // 
            // tb53
            // 
            this.tb53.AcceptsReturn = true;
            this.tb53.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb53.Location = new System.Drawing.Point(243, 160);
            this.tb53.Name = "tb53";
            this.tb53.Size = new System.Drawing.Size(100, 23);
            this.tb53.TabIndex = 23;
            // 
            // tb52
            // 
            this.tb52.AcceptsReturn = true;
            this.tb52.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb52.Location = new System.Drawing.Point(128, 160);
            this.tb52.Name = "tb52";
            this.tb52.Size = new System.Drawing.Size(100, 23);
            this.tb52.TabIndex = 22;
            // 
            // tb51
            // 
            this.tb51.AcceptsReturn = true;
            this.tb51.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb51.Location = new System.Drawing.Point(12, 160);
            this.tb51.Name = "tb51";
            this.tb51.Size = new System.Drawing.Size(99, 23);
            this.tb51.TabIndex = 21;
            this.tb51.WordWrap = false;
            // 
            // tb41
            // 
            this.tb41.AcceptsReturn = true;
            this.tb41.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb41.Location = new System.Drawing.Point(11, 124);
            this.tb41.Name = "tb41";
            this.tb41.Size = new System.Drawing.Size(100, 23);
            this.tb41.TabIndex = 20;
            // 
            // tb31
            // 
            this.tb31.AcceptsReturn = true;
            this.tb31.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb31.Location = new System.Drawing.Point(11, 87);
            this.tb31.Name = "tb31";
            this.tb31.Size = new System.Drawing.Size(100, 23);
            this.tb31.TabIndex = 19;
            // 
            // tb21
            // 
            this.tb21.AcceptsReturn = true;
            this.tb21.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb21.Location = new System.Drawing.Point(11, 53);
            this.tb21.Name = "tb21";
            this.tb21.Size = new System.Drawing.Size(100, 23);
            this.tb21.TabIndex = 18;
            // 
            // tb11
            // 
            this.tb11.AcceptsReturn = true;
            this.tb11.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb11.Location = new System.Drawing.Point(11, 17);
            this.tb11.Name = "tb11";
            this.tb11.Size = new System.Drawing.Size(100, 23);
            this.tb11.TabIndex = 17;
            this.tb11.TextChanged += new System.EventHandler(this.tb11_TextChanged);
            // 
            // NaiveFiveBlockHukidasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tb55);
            this.Controls.Add(this.tb45);
            this.Controls.Add(this.tb35);
            this.Controls.Add(this.tb25);
            this.Controls.Add(this.tb54);
            this.Controls.Add(this.tb15);
            this.Controls.Add(this.tb14);
            this.Controls.Add(this.tb13);
            this.Controls.Add(this.tb12);
            this.Controls.Add(this.tb53);
            this.Controls.Add(this.tb52);
            this.Controls.Add(this.tb51);
            this.Controls.Add(this.tb41);
            this.Controls.Add(this.tb31);
            this.Controls.Add(this.tb21);
            this.Controls.Add(this.tb11);
            this.Name = "NaiveFiveBlockHukidasi";
            this.Size = new System.Drawing.Size(595, 200);
            this.Load += new System.EventHandler(this.NaiveFiveBlockHukidasi_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb55;
        private System.Windows.Forms.TextBox tb45;
        private System.Windows.Forms.TextBox tb35;
        private System.Windows.Forms.TextBox tb25;
        private System.Windows.Forms.TextBox tb54;
        private System.Windows.Forms.TextBox tb15;
        private System.Windows.Forms.TextBox tb14;
        private System.Windows.Forms.TextBox tb13;
        private System.Windows.Forms.TextBox tb12;
        private System.Windows.Forms.TextBox tb53;
        private System.Windows.Forms.TextBox tb52;
        private System.Windows.Forms.TextBox tb51;
        private System.Windows.Forms.TextBox tb41;
        private System.Windows.Forms.TextBox tb31;
        private System.Windows.Forms.TextBox tb21;
        private System.Windows.Forms.TextBox tb11;
    }
}
