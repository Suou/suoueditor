﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase;
using SuouTextBox2.Base;
namespace SuouTextBox2.Controls
{
    public partial class HukidasiSetting : UserControl
    {
        public HukidasiSetting()
        {
            InitializeComponent();
#if !DEBUG
            this.registerButton.Enabled = false;
            this.samplePictureButton.Click -= this.pictureBox1_Click;
#endif
        }
        public HukidasiSetting(EditorInfo dllfunction):base()
        {
            this.altCharControl1.SaveItems = dllfunction.AltHukidasiPlugin;
        }
        //public HukidasiYosumiForm YosumiForm = new HukidasiYosumiForm();
        //public event EventHandler PropertyChanged;
        public event EventHandler HukidasiTypeChanged;

        private EditorInfo _DLLFunction = null;
        public EditorInfo DLLFunction
        {
            get
            {
                return _DLLFunction;
            }
            set
            {
                _DLLFunction = value;
                if (_DLLFunction != null)
                {
                    this.altCharControl1.SaveItems = this.DLLFunction.AltHukidasiPlugin;
                }
            }
        }
        private HukidasiType HukidasiType
        {
            get
            {
                switch (this.comboBox1.SelectedIndex)
                {
                    case 0:
                        return HukidasiType.Nothing;
                    case 1:
                        return HukidasiType.Naive;
                    case 2:
                        return HukidasiType.Block;
                    case 3:
                        return HukidasiType.FiveBlock;
                    default:
                        return HukidasiType.Nothing;
                }
            }
            set
            {
                switch (value)
                {
                    case SuouBase.Text.HukidasiType.Nothing:
                        this.comboBox1.SelectedIndex = 0;
                        break;
                    case SuouBase.Text.HukidasiType.Naive:
                        this.comboBox1.SelectedIndex = 1;
                        break;
                    case SuouBase.Text.HukidasiType.Block:
                        this.comboBox1.SelectedIndex = 2;
                        break;
                    case SuouBase.Text.HukidasiType.FiveBlock:
                        this.comboBox1.SelectedIndex = 3;
                        break;
                }
                if (this.HukidasiTypeChanged != null) this.HukidasiTypeChanged(value, new EventArgs());
            }
        }
        /*
        public MergeZureOption ZureOption
        {
            get
            {
                if (this.radioButton1.Checked)
                {
                    return MergeZureOption.Free;
                }
                else if (this.radioButton2.Checked)
                {
                    return MergeZureOption.LineRestriction;
                }
                else if (this.radioButton3.Checked)
                {
                    return MergeZureOption.PermitZureLine;
                }
                else if (this.radioButton4.Checked)
                {
                    return MergeZureOption.Destructive;
                }
                return MergeZureOption.Free;
            }
            set
            {
                switch (value)
                {
                    case MergeZureOption.Free:
                        this.radioButton1.Checked = true;
                        break;
                    case MergeZureOption.LineRestriction:
                        this.radioButton2.Checked = true;
                        break;
                    case MergeZureOption.PermitZureLine:
                        this.radioButton3.Checked = true;
                        break;
                    case MergeZureOption.Destructive:
                        this.radioButton4.Checked = true;
                        break;
                    default:
                        this.radioButton1.Checked = true;
                        break;

                }
            }
        }
        */
        public int PermitZureLength
        {
            get
            {
                return (int)this.numericUpDown1.Value;
            }
            set
            {
                this.numericUpDown1.Value = value;
            }
        }
        public int PrePermitZureLength
        {
            get
            {
                return (int)this.HukidasiZureNumeric.Value;
            }
            set
            {
                this.HukidasiZureNumeric.Value = value;
            }
        }
        public int UpCenterBottomZure
        {
            get
            {
                return (int)this.UCBNumeric.Value;
            }
            set
            {
                this.UCBNumeric.Value = value;
            }
        }
        public bool IsTrimSpace
        {
            get
            {
                return this.checkBox1.Checked;
            }
            set
            {
                this.checkBox1.Checked = value;
            }
        }

        public HukidasiForm CreateInputHukidasiForm(HukidasiSaveData value,bool IsExtra)
        {
            var hukidasiForm = new HukidasiForm();
            

            switch (value.Type)
            {
                case SuouBase.Text.HukidasiType.Block: hukidasiForm.ControlItem = new NaiveHukidasi(); break;
                case SuouBase.Text.HukidasiType.FiveBlock:
                    hukidasiForm.ControlItem = IsExtra ? (IHukidasiControl)new FiveBlockHukidasiControl() : new NaiveFiveBlockHukidasi();
                    break;
                case SuouBase.Text.HukidasiType.Naive: hukidasiForm.ControlItem = new NaiveHukidasi(); break;
                case SuouBase.Text.HukidasiType.Nothing: hukidasiForm.ControlItem = new NaiveHukidasi(); break;
            }
            hukidasiForm.SetItem(value);
            return hukidasiForm;
        }
        public SpaceLineSet MergeAlgorithm
        {
            get
            {
                return this.altCharControl1.Item;
            }
            set
            {
                this.altCharControl1.Item = value;
            }
        }
        public HukidasiSaveData _Item;
        public HukidasiSaveData Item
        {
            get
            {

                throw new NotSupportedException();
                /*
                var f = this._Item;
                var r = new HukidasiSaveData(f.Items.ToList(),null, this.HukidasiType, this.ZureOption, this.PermitZureLength, this.MergeAlgorithm,this.IsTrimSpace);
                r.TopSpace = (int)this.UpSpaceNumeric.Value;
                r.LeftSpace = (int)this.LeftSpaceNumeric.Value;
                r.RightSpace = (int)this.RightSpaceNumeric.Value;
                r.BottomSpace = (int)this.BottomSpaceNumeric.Value;
                r.CenterSpace = (int)this.CenterSpaceNumeric.Value;
                r.CenterPreZure = this.PrePermitZureLength;
                r.UpCenterBottomPreZure = this.UpCenterBottomZure;

                if (r.MergeAlgorithm == null) throw new MyException("Error");
                return r;
                */
            }
            set
            {
                this._Item = value;
                if (value == null) return;
                this.HukidasiType = value.Type;
                //this.ZureOption = value.ZureOption;
                //this.PermitZureLength = value.PermitZureLength;
                this.MergeAlgorithm = value.MergeAlgorithm;
                this.IsTrimSpace = value.IsTrimSpace;
                

                var a = HukidasiExtension.Create(value);
                var v = new HukidasiImage(a, AltText.Default());
                var sampletext = new SuouText(System.Environment.NewLine + "sample" + System.Environment.NewLine);
                var sampleImage = v.CreateImage(sampletext);
                this.samplePictureButton.Image = sampleImage;
                this.samplePictureButton.Size = sampleImage.Size;

                this.PrePermitZureLength = value.Options.CenterPreZure;
                this.UpCenterBottomZure = value.Options.UpCenterBottomPreZure;

                this.UpSpaceNumeric.Value = value.Padding.TopSpace;
                this.LeftSpaceNumeric.Value = value.Padding.LeftSpace;
                this.RightSpaceNumeric.Value = value.Padding.RightSpace;
                this.BottomSpaceNumeric.Value = value.Padding.BottomSpace;
                this.CenterSpaceNumeric.Value = value.Padding.CenterSpace;
            }
        }
        public IHukidasiLogic SampleItem
        {
            get
            {
                var a = HukidasiExtension.Create(this.Item);
                return a;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (DLLFunction != null && DLLFunction.HukidasiSelecterForm != null)
            {
                var form = DLLFunction.HukidasiSelecterForm();
                var f = form.ShowDialog();
                if (f == DialogResult.OK)
                {
                    this.Item = form.SelectedSaveData;
                }
                form.Dispose();
            }

        }

        
        private void pictureBox1_Click(object sender, EventArgs e)
        {
            OpenEditHukidasiForm(false);
        }
        private void OpenEditHukidasiForm(bool isExtra)
        {
            var f = this.CreateInputHukidasiForm(this.Item, isExtra);

            f.ShowDialog();
            if (f.DialogResult == DialogResult.OK)
            {
                var p = f.GetItem();
                var pv = new HukidasiBaseSaveData(this.Item)
                {
                    //WakuItems = p.WakuItems,
                    Elements = new WakuElementCollection(p.WakuItems,null),
                    Options = new WakuOption()
                    {
                        //PermitZureLength = p.PermitZureLength,
                        UpCenterBottomPreZure = p.Options.UpCenterBottomPreZure
                    }
                };
                this.Item = new HukidasiSaveData(pv);
                //this.Item = new HukidasiSaveData(p.Items.ToList(),null, this.Item.Type, this.Item.ZureOption, p.PermitZureLength, this.Item.MergeAlgorithm, this.Item.IsTrimSpace);
                //this.Item = f.GetItem();
            }
            else
            {
                //YosumiForm.WakuString = f;
            }
        }

        private void RegisterButton_Click(object sender, EventArgs e)
        {
            
            if (this.SampleItem.SaveData.ErrorCheck())
            {
                DLLFunction.AltHukidasiPlugin.HukidasiSaveDataList.Add(this.Item);
                MessageBox.Show("追加しました");
            }
            else
            {
                MessageBox.Show("この要素ではフキダシを作成することができません");
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var n = this.comboBox1.SelectedIndex;
            if (n == 3)
            {
                this.ExpandEditButton.Enabled = true;
            }
            else
            {
                this.ExpandEditButton.Enabled = false;
            }
            if(this._Item != null)this.CreateInputHukidasiForm(this._Item,false);
        }

        private void EditButton_Click(object sender, EventArgs e)
        {
            OpenEditHukidasiForm(false);
        }

        private void ExpandEditButton_Click(object sender, EventArgs e)
        {
            OpenEditHukidasiForm(true);
        }
    }
}
