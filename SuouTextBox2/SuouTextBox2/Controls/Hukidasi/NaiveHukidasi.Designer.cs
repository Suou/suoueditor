﻿namespace SuouTextBox2.Controls
{
    partial class NaiveHukidasi
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.tb11 = new System.Windows.Forms.TextBox();
            this.tb21 = new System.Windows.Forms.TextBox();
            this.tb32 = new System.Windows.Forms.TextBox();
            this.tb12 = new System.Windows.Forms.TextBox();
            this.tb13 = new System.Windows.Forms.TextBox();
            this.tb22 = new System.Windows.Forms.TextBox();
            this.tb33 = new System.Windows.Forms.TextBox();
            this.tb31 = new System.Windows.Forms.TextBox();

            this.tb11.TextChanged += this.tb11_TextChanged;
            this.tb21.TextChanged += this.tb11_TextChanged;
            this.tb32.TextChanged += this.tb11_TextChanged;
            this.tb12.TextChanged += this.tb11_TextChanged;
            this.tb13.TextChanged += this.tb11_TextChanged;
            this.tb22.TextChanged += this.tb11_TextChanged;
            this.tb33.TextChanged += this.tb11_TextChanged;
            this.tb31.TextChanged += this.tb11_TextChanged;

            this.tb11.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb21.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb32.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb12.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb13.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb22.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb33.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.tb31.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.SuspendLayout();
            // 
            // tb11
            // 
            this.tb11.Location = new System.Drawing.Point(17, 16);
            this.tb11.Name = "tb11";
            this.tb11.Size = new System.Drawing.Size(100, 19);
            this.tb11.TabIndex = 0;
            // 
            // tb21
            // 
            this.tb21.Location = new System.Drawing.Point(17, 66);
            this.tb21.Name = "tb21";
            this.tb21.Size = new System.Drawing.Size(100, 19);
            this.tb21.TabIndex = 1;
            // 
            // tb32
            // 
            this.tb32.Location = new System.Drawing.Point(132, 117);
            this.tb32.Name = "tb32";
            this.tb32.Size = new System.Drawing.Size(100, 19);
            this.tb32.TabIndex = 3;
            // 
            // tb12
            // 
            this.tb12.Location = new System.Drawing.Point(132, 16);
            this.tb12.Name = "tb12";
            this.tb12.Size = new System.Drawing.Size(100, 19);
            this.tb12.TabIndex = 5;
            // 
            // tb13
            // 
            this.tb13.Location = new System.Drawing.Point(247, 16);
            this.tb13.Name = "tb13";
            this.tb13.Size = new System.Drawing.Size(100, 19);
            this.tb13.TabIndex = 6;
            // 
            // tb22
            // 
            this.tb22.Location = new System.Drawing.Point(247, 66);
            this.tb22.Name = "tb22";
            this.tb22.Size = new System.Drawing.Size(100, 19);
            this.tb22.TabIndex = 7;
            // 
            // tb33
            // 
            this.tb33.Location = new System.Drawing.Point(247, 117);
            this.tb33.Name = "tb33";
            this.tb33.Size = new System.Drawing.Size(100, 19);
            this.tb33.TabIndex = 8;
            // 
            // tb31
            // 
            this.tb31.Location = new System.Drawing.Point(17, 117);
            this.tb31.Name = "tb31";
            this.tb31.Size = new System.Drawing.Size(100, 19);
            this.tb31.TabIndex = 2;
            // 
            // NaiveHukidasi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tb33);
            this.Controls.Add(this.tb22);
            this.Controls.Add(this.tb13);
            this.Controls.Add(this.tb12);
            this.Controls.Add(this.tb32);
            this.Controls.Add(this.tb31);
            this.Controls.Add(this.tb21);
            this.Controls.Add(this.tb11);
            this.MinimumSize = new System.Drawing.Size(300, 150);
            this.Name = "NaiveHukidasi";
            this.Size = new System.Drawing.Size(391, 160);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb11;
        private System.Windows.Forms.TextBox tb21;
        private System.Windows.Forms.TextBox tb32;
        private System.Windows.Forms.TextBox tb12;
        private System.Windows.Forms.TextBox tb13;
        private System.Windows.Forms.TextBox tb22;
        private System.Windows.Forms.TextBox tb33;
        private System.Windows.Forms.TextBox tb31;
    }
}
