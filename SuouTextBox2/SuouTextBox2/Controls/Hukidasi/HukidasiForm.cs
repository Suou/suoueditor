﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Text;
using SuouTextBox2.Base;

using SuouBase.Drawing;
namespace SuouTextBox2.Controls
{
    public partial class HukidasiForm : Form
    {
        public HukidasiForm()
        {
            InitializeComponent();
            this.suouTextBox1.TextChanged += this.suouTextBox1_TextChanged;
        }
        private IHukidasiControl _ControlItem;
        public IHukidasiControl ControlItem
        {
            get { return this._ControlItem; }
            set
            {
                if (value != null)
                {
                    this._ControlItem = value;
                    
                    this.panel1.Controls.Add((Control)this.ControlItem);
                    this.ControlItem.TextChanged += this.TextBox_TextChenged;

                    var v = this.ControlItem.GetItem();
                    this.UCBNumeric.Value = v.Options.UpCenterBottomPreZure;
                    this.CNumeric.Value = v.Options.CenterPreZure;
                    //this.ControlItem.Dock = DockStyle.Fill;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }
        public static SuouText[] ToComposition(string[] r, AltText at)
        {
            var r2 = r.Select((s) => new SuouText(s)).Select((v) => new TextComposition(v, new PrintFontSetting())).ToArray();
            return at.MakeSameWidthRectangleText(r2);

        }
        public static SuouText ToComposition(string r, AltText at)
        {
            var f = new SuouText(r);
            var f2 = new TextComposition(f, new PrintFontSetting());
            return at.MakeRectangleText(f2, true);
        }
        private void TextBox_TextChenged(object sender, EventArgs e)
        {
            var f = this.GetItem();
            var error = f.CreateCheckData();
            foreach (var v in error.ErrorIterator())
            {
                if (ControlItem.GetTextBox(v.Key) == null)
                {
                }
                else
                {
                    ControlItem.GetTextBox(v.Key).BackColor = v.Value ? Color.White : Color.Red;
                }
            }
            this.OKbutton.Enabled = error.IsNoError;
            this.textBox1.Text = error.GetErrorMessage();
        }

        private void HukidasiForm_Load(object sender, EventArgs e)
        {

        }
        public void SetItem(HukidasiSaveData h)
        {
            this.ControlItem.SetItem(h);
            this.UpCenterBottomZure = h.Options.UpCenterBottomPreZure;
            this.PrePermitZureLength = h.Options.CenterPreZure;
        }
        public HukidasiSaveData GetItem()
        {
            var f = this.ControlItem.GetItem();
            f.Options.UpCenterBottomPreZure = (int)UCBNumeric.Value;
            f.Options.CenterPreZure = (int)CNumeric.Value;
            return f;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var f = this.GetItem();
            var error = f.CreateCheckData();
            error.AutoModify(AltText.Default());
            this.ControlItem.SetItem(f);
        }
        public int PrePermitZureLength
        {
            get
            {
                return (int)this.CNumeric.Value;
            }
            set
            {
                this.CNumeric.Value = value;
            }
        }
        public int UpCenterBottomZure
        {
            get
            {
                return (int)this.UCBNumeric.Value;
            }
            set
            {
                this.UCBNumeric.Value = value;
            }
        }
        private void suouTextBox1_CaretChanged(object sender, EventArgs e)
        {
            this.toolStripStatusLabel2.Text = SuouTextBox.NotifyCaretCharInfomation(this.suouTextBox1.Caret);
            this.toolStripStatusLabel1.Text = SuouTextBox.NotifyCaretInformation(this.suouTextBox1.Caret);
            
        }
        private void suouTextBox1_TextChanged(object sender, EventArgs e)
        {
            this.toolStripStatusLabel3.Text = this.suouTextBox1.RawText.NotifyPageSizeMessage();
        }
    }
    public interface IHukidasiControl
    {
        HukidasiSaveData GetItem();
        void SetItem(HukidasiSaveData h);
        DockStyle Dock { get; set; }
        void Modify(AltText at);
        TextBox GetTextBox(HukidasiPart part);
        event EventHandler TextChanged;
    }
}
