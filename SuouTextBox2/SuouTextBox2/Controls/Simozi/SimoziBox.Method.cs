﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase;
using SuouTextBox2.Operator;
using SuouBase.Drawing;
using BSystem;
using WebExtension;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Book;
using SuouTextBox2.Controls.TextLayer;

namespace SuouTextBox2.Controls
{
    public partial class SimoziBox
    {
        public event EventHandler DragMoveModeChanged;
        public EditPoint EditLocation { get; set; }
        public event EventHandler EditLocationChanged;
        public void EventDispose(){}
        public bool IsPictureInnerMouse { get; set; }
        public bool IsViewMode { get; set; }

        public TextBoxMode TextBoxMode { get; set; } = TextBoxMode.Free;
        public void SwitchPicture()
        {
        }
        

        #region Property
        public MergeLayerItemCollection MergeLayerItemCollection { get; private set; }
        private bool _UseTemporaryContext = true;
        public EditPoint AddNewLocation { get; set; }
        public bool IsSharrowHide { get; set; }
        private IPenMouse _PenItem = null;
        private bool _CanMerge = true;
        private bool _IsMultiMode = false;
        //public Point FirstLocation { get; set; }
        //public Point EndLocation { get; set; }
        //public int ShowNumber { get; set; }
        public EditorInfo DLLFunction { get; set; }
        public bool IsDragEndMerge { get; set; }
        private IBookPage _BookPage = null;
        public IBookPage BookPage
        {
            get
            {
                return this._BookPage;
            }
            set
            {
                this.MergeLayerItemCollection.DisposeMerge();//遅延の元凶になるかも
                this._BookPage = value;
                if (this._BookPage != null)
                {
                    if (this.BookPage is SectionScene)
                    {
                        this.MergeLayerItemCollection.Scene = (SectionScene)this.BookPage;
                    }
                    else
                    {
                        var p = SectionScene.Copy(this.BookPage);
                        this.MergeLayerItemCollection.Scene = p;
                        //p.TextChanged += this.PageContentChanged;

                    }
                    if (this.PageChanged != null) this.PageChanged(this, new EventArgs());

                }
            }
        }
        public event EventHandler PageChanged;
        public event EventHandler TextUpdated;
        public bool NeedMerge { get; set; }
        private SimoziBoxSaveData _SaveData = new SimoziBoxSaveData();
        public void SetSaveData(SimoziBoxSaveData value)
        {
            System.Console.WriteLine(value);
            if (value != null)
            {
                this._SaveData = value;
                this.suouTextBox1.SetShortCutKeys(value.ShortCutKeys);
                if (this.MergeLayerItemCollection != null && this.MergeLayerItemCollection.SimoziAndLayerItems != null)
                {
                    this.MergeLayerItemCollection.SimoziAndLayerItems.ToList().ForEach((v) => v.TextBox.SetTextSetting(this.SaveData.SuouTextSetting));
                }
                this.MergePase = this.SaveData.MergePase;
                this.IsDragEndMerge = this.SaveData.IsDragEndMerge;
                this.TextBox.SetSpaceSet(value.MergeAlgorithm);
                if (this.MergeLayerItemCollection != null && this.MergeLayerItemCollection.LayerItems != null) this.MergeLayerItemCollection.LayerItems.ForEach((v) => v.TextBox.SetSpaceSet(value.MergeAlgorithm));

            }
        }

        public SimoziBoxSaveData SaveData
        {
            get
            {
                _SaveData.ShortCutKeys = this.suouTextBox1.ShortCutKeys;
                
                return _SaveData;
            }
        }

        public virtual bool IsMultiMode
        {
            get { return _IsMultiMode; }
            set
            {
                _IsMultiMode = value;
                if (_IsMultiMode)
                {
                    this.MergeLayerItemCollection.NeedMergeCheck();
                    this.MergeLayerItemCollection.AllSharrowShow();
                    this.suouTextBox1.SetRawText(this.MergeLayerItemCollection.Scene.SText);
                    this.suouTextBox1.UserInputEnable = false;
                    this.MergeLayerItemCollection.RefreshAll();

                }
                else
                {
                    this.MergeLayerItemCollection.AllSharrowHide();
                    this.suouTextBox1.SetRawText(this.BackgroundPage.SText);

                    this.suouTextBox1.UserInputEnable = true;
                }
            }
        }
        public bool CanMerge
        {
            get { return _CanMerge; }
            set { _CanMerge = value; }
        }

        public IPenMouse PenItem
        {
            get { return _PenItem; }
            set
            {
                _PenItem = value;
                foreach (var v in this.TextBoxs)
                {
                    if (PenItem != null)
                    {
                        v.Pen = this.PenItem.Copy();
                    }
                    else
                    {
                        v.Pen = null;
                    }
                }
            }
        }
        public SuouTextBox[] TextBoxs
        {
            get
            {
                return this.MergeLayerItemCollection.SimoziAndLayerItems.Select((v) => v.TextBox).ToArray();
            }
        }




        private bool FirstCheck
        {
            get
            {
                return this.SelectedTextBox != null && this.ParentForm != null && this.SelectedTextBox.UserInputEnable;
            }
        }

        public int MergePase
        {
            get { return this.SaveData.MergePase; }
            set { this.SaveData.MergePase = value; }
        }
        public bool UseTemporaryContext
        {
            get { return this._UseTemporaryContext; }
            set { this._UseTemporaryContext = value; }
        }
        public bool UseDebug
        {
            get
            {
                return this.suouTextBox1.UseDebug;
            }
            set
            {
                this.suouTextBox1.UseDebug = value;
            }
        }
        //public event EventHandler ScrollValueChanged;
        public event EventHandler CaretPositionChanged;
        public new event EventHandler TextChanged;
        public BookPageContentChangeEventHandler DropPageAccepted;
        #endregion
        #region ShowProperty
        public SectionScenePart BackgroundPage
        {
            get
            {
                return this.MergeLayerItemCollection.BackGroundPage;
            }
        }
        public new SuouText Text
        {
            get
            {
                return this.suouTextBox1.RawText;
            }
        }
        public TextBoxCaret Caret
        {
            get
            {
                return this.suouTextBox1.Caret;
            }
        }
        public PrintFontSetting PrintFontSetting
        {
            get
            {
                return this.suouTextBox1.PrintFontSet;
            }
        }
        public SuouTextBox TextBox
        {
            get { return this.suouTextBox1; }
        }
        public SuouTextBox SelectedTextBox
        {
            get
            {
                return this.MergeLayerItemCollection.SelectedTextBox;
            }
        }

        public TextBoxShortCutKeys ShortCutKeys
        {
            get { return this.suouTextBox1.ShortCutKeys; }
            set { this.suouTextBox1.SetShortCutKeys(value); }
        }

        #endregion
        #region Method
        
        private void SetText(SuouText tc, TextChangeType type, bool withRefresh)
        {
            BSystem.Debug.ColorConsole.IncrementTabCount();
            var sw = System.Diagnostics.Stopwatch.StartNew();

            this.suouTextBox1.StockCheck();
            if(sw.ElapsedMilliseconds  > 100) BSystem.Debug.ColorConsole.WriteLine($"SetText1: {sw.ElapsedMilliseconds}");
            sw.Restart();
            this.suouTextBox1.TextOperator.ReplaceText(tc);
            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine($"SetText2: {sw.ElapsedMilliseconds}");

            if (withRefresh)
            {
                sw.Restart();
                this.Refresh();
                if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine($"SetText3: {sw.ElapsedMilliseconds}");

                sw.Restart();
                this.MergeLayerItemCollection.LayerItems.ForEach((v) => { v.RefreshPicture(); });
            }
            if (this.TextUpdated != null) this.TextUpdated(this, new EventArgs());
            
            sw.Stop();
            
            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine($"SetText4: {sw.ElapsedMilliseconds}");
            BSystem.Debug.ColorConsole.DecrementTabCount();

        }
        public void UpdateText(bool withRefresh = true)
        {
            var isEditMode = this.MergeLayerItemCollection.LayerItems.Exists((v) => !v.IsViewMode) && !(this.TextBoxMode == TextBoxMode.PictureModeReady);
            if(this.TextBoxMode == TextBoxMode.DragStartReady || this.TextBoxMode == TextBoxMode.DragEndReady || isEditMode)
            {
                return;
            }
            BSystem.Debug.ColorConsole.IncrementTabCount();
            var sw = System.Diagnostics.Stopwatch.StartNew();
            var newText = this.MergeLayerItemCollection.Scene.SText;
            sw.Stop();
            if (sw.ElapsedMilliseconds > 100) BSystem.Debug.ColorConsole.WriteLine($"Create NewText: {sw.ElapsedMilliseconds}");
            BSystem.Debug.ColorConsole.DecrementTabCount();

            this.SetText(newText, TextChangeType.TemporaryMerge, withRefresh);
            
            //this.Refresh();
        }
        public Bitmap GetDisplayImage(DotPoint SrcLocation, Size ImageSize, Color backColor)
        {
            return this.suouTextBox1.GetDisplayImage(SrcLocation, ImageSize, backColor);
        }
        public void GetDrawImage(Graphics g, Point DestLocation, DotPoint SrcLocation, Size ImageSize, Color backColor)
        {
            this.suouTextBox1.DrawOperator.DrawImage(g, DestLocation, SrcLocation, ImageSize, backColor);
        }
        public void GetDrawImage(Graphics g, Point DestLocation, Point SrcLocation, Size ImageSize)
        {
            this.suouTextBox1.DrawOperator.DrawImage(g, DestLocation, SrcLocation, ImageSize);
        }
        public ExEditPoint ModifyLocation(Point ClientLocation)
        {
            return suouTextBox1.ModifyLocation(ClientLocation);
        }
        public DotPoint ModifyDotLocation(Point Location)
        {
            return suouTextBox1.ModifyDotLocation(Location);
        }
        #endregion
        #region PublicMethod
        public bool CanUndo
        {
            get
            {
                return this.FirstCheck && this.SelectedTextBox.CanUndo;
            }
        }
        public bool CanRedo
        {
            get
            {
                return this.FirstCheck && this.SelectedTextBox.CanRedo;
            }
        }
        public bool CanPaste
        {
            get { return this.FirstCheck && this.SelectedTextBox.CanPaste; }
        }
        public bool CanCut
        {
            get { return this.FirstCheck && this.SelectedTextBox.CanCut; }
        }
        public bool CanCopy
        {
            get { return this.FirstCheck && this.SelectedTextBox.CanCopy; }
        }
        public bool CanDelete
        {
            get { return this.FirstCheck && this.SelectedTextBox.CanDelete; }
        }
        public bool CanTransparentize
        {
            get { return this.FirstCheck && this.SelectedTextBox.CanTransparentize; }
        }
        public bool CanSpaceReplace
        {
            get { return this.FirstCheck && this.SelectedTextBox.CanSpaceReplace; }
        }
        public bool CanAllSelect
        {
            get { return this.FirstCheck && this.SelectedTextBox.CanAllSelect; }
        }
        public void Undo()
        {
            this.SelectedTextBox.Undo();
        }
        public void Redo()
        {
            this.SelectedTextBox.Redo();
        }
        public void Paste()
        {
            this.SelectedTextBox.Paste();
        }
        public void Cut()
        {
            this.SelectedTextBox.Cut();
        }
        public void Copy()
        {
            this.SelectedTextBox.Copy();
        }
        public void Delete()
        {
            this.SelectedTextBox.Delete();
        }
        public void Transparentize()
        {
            this.SelectedTextBox.Transparentize();
        }
        public void SpaceReplace()
        {
            this.SelectedTextBox.SpaceReplace();
        }
        public void AllSelect()
        {
            this.SelectedTextBox.ChoiceAll();
        }
        #endregion
    }
}
