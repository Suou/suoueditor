﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase;
using BSystem.Collections;
using System.Collections.Specialized;
using SuouBase.Book;
namespace SuouTextBox2.Controls
{
    public class MergeLayerItemCollection
    {
        #region Property
        public event SelectedLayerItemEventHandler SelectedLayerItemChanged;
        public event EventHandler LayerItemChanged;
        private SimoziBox simoziBox { get; set; }
        private SuouTextBox suouTextBox { get; set; }
        private HScrollBar Hsb { get; set; }
        private VScrollBar Vsb { get; set; }
        private bool PictureRefleshFlag { get; set; }
        private bool NeedMerge { get; set; }
        public bool IsEnableMerge { get; set; }
        public LayerItem[] SimoziAndLayerItems
        {
            get
            {
                List<LayerItem> r = new List<LayerItem>();
                r.Add(this.simoziBox);
                foreach (var v in this.LayerItems) r.Add((TextLayerBox)v);
                return r.ToArray();
            }
        }
        private DateTime PreTime = DateTime.Now;
        private SectionScene _PageItem = new SectionScene();
        public SectionScene Scene
        {
            get { return this._PageItem; }
            set {
                if (this._PageItem != null) this._PageItem.Parts.CollectionChanged -= this.Content_Changed;
                this._PageItem = value;
                if (this._PageItem != null) this._PageItem.Parts.CollectionChanged += this.Content_Changed;
                this.InitializeLayer();
            }
        }
        public List<LayerItem> LayerItems = new List<LayerItem>();
        public SectionScenePart BackGroundPage
        {
            get
            {
                if (this.Scene.Parts.Count > 0)
                {
                    return this.Scene.Parts.First();
                }
                else
                {
                    return null;
                }
            }
        }
        public SimoziMode SimoziMode
        {
            get
            {
                if (this.LayerItems.All((t) => !t.BackgroundPage.Enable))
                {
                    return SimoziMode.Single;
                }
                else
                {
                    return SimoziMode.Multi;
                }
            }
            private set
            {
                if (value == SuouTextBox2.Controls.SimoziMode.Single)
                {
                    foreach (var v in this.LayerItems) v.BackgroundPage.Enable = false;
                }

            }
        }

        #endregion
        #region Selected

        public SuouText SelectedNotifyPage
        {
            get
            {
                if (!this.simoziBox.IsMultiMode)
                {
                    return this.simoziBox.TextBox.RawText;
                }
                else
                {
                    if (this.SelectedLayerItem is TextLayerBox)
                    {
                        var v = (TextLayerBox)this.SelectedLayerItem;
                        if (v.IsViewMode)
                        {
                            return this.Scene.SText;
                        }
                        else
                        {
                            return v.TextBox.RawText;
                        }
                    }
                    else
                    {
                        return this.Scene.SText;
                    }
                }
            }
        }
        public SuouTextBox SelectedTextBox
        {
            get
            {
                var p = this.LayerItems.FirstOrDefault((v) => !v.IsViewMode);
                if (p == null)
                {
                    return (this.simoziBox.IsMultiMode ? null : this.suouTextBox);
                }
                else
                {
                    return p.TextBox;
                }
            }
        }

        private LayerItem _SelectedLayerItem = null;
        public LayerItem SelectedLayerItem
        {
            get { return _SelectedLayerItem; }

            set
            {
                _SelectedLayerItem = value;
                if (SelectedLayerItem != null)
                {
                    if (SelectedLayerItemChanged != null) SelectedLayerItemChanged(this, new SelectedLayerItemEventArgs(SelectedLayerItem));
                }
            }

        }
        #endregion
        #region Constructor
        public MergeLayerItemCollection(SimoziBox ctrl, SuouTextBox textbox, HScrollBar hsb, VScrollBar vsb)
        {
            this.IsEnableMerge = true;
            this.PreTime = DateTime.Now;
            this.simoziBox = ctrl;
            this.suouTextBox = textbox;
            this.Hsb = hsb;
            this.Vsb = vsb;
            this.PictureRefleshFlag = true;
        }
        #endregion
        #region BaseOperatorMethod
        public SectionScenePart CreateNewPage(SuouText tex, EditPoint ep)
        {
            return new SectionScenePart(this.simoziBox.SaveData.Hukidasi, this.simoziBox.SaveData.MergeAlgorithm) { Type = MergePartsType.Other, Name = "レイヤー" + this.Scene.Parts.Count, SText = tex, EditLocation = ep };
        }
        public SectionScenePart CreateNewPage(EditPoint ep)
        {
            return CreateNewPage(SuouText.Empty(), ep);
        }
        private TextLayerBox CreateLayer(SectionScenePart pagepart)
        {
            var item = new TextLayerBox(pagepart, this.simoziBox);
            //LayerItems.Add(item);
            this.simoziBox.Controls.Add(item);

            item.Visible = true;
            item.Location = this.suouTextBox.TranslateToDisplayPoint(item.EditLocation);
            item.EditLocationChanged += this.LayerItem_EditLocationChanged;
            //item.ViewModeChanged += this.LayerItem_ViewModeChanged;
            item.Enter += this.TextLayer_Enter;
            item.TextBox.Enter += this.TextBox_Enter;
            item.TextBox.MouseDown += this.TextBox_Enter;
            //item.DragMoveModeChanged += this.LayerItem_DragMoveModeChanged;
            item.BackgroundPage.Enable = true;
            return item;
        }
        private void AddLayer(TextLayerBox item, bool isCheck)
        {
            item.BringToFront();
            Hsb.BringToFront();
            Vsb.BringToFront();
            this.LayerItems.Add(item);
            if (isCheck)
            {
                this.simoziBox.IsMultiMode = true;
                //TemporaryCheck();
            }
            if (LayerItemChanged != null) LayerItemChanged(this, new EventArgs());
        }
        public void ReBringToFront()
        {
            this.LayerItems.ForEach((v) => v.BringToFront());
            Hsb.BringToFront();
            Vsb.BringToFront();
        }
        public void pRemoveLayer(LayerItem LayerItem)
        {
            //this.PageItem.Parts.Remove(LayerItem.PagePartsItem);

            
            this.simoziBox.Controls.Remove((Control)LayerItem);
            this.LayerItems.Remove(LayerItem);
            //this.PageItem.Parts.Remove(LayerItem.PagePartsItem);
            //LayerItem.EventDispose();
            ((Control)LayerItem).Dispose();
            this.NeedMerge = true;

            if (this.Scene.Parts.Count == 1) this.simoziBox.IsMultiMode = false;
            //this.RefreshText();
        }
        public void ClearLayer()
        {
            this.DisposeMerge();
            foreach (var v in this.LayerItems)
            {
                v.EditLocationChanged -= this.LayerItem_EditLocationChanged;
                v.Enter -= this.TextLayer_Enter;
                v.TextBox.Enter -= this.TextBox_Enter;
                v.TextBox.MouseDown -= this.TextBox_Enter;
                //v.DragMoveModeChanged -= this.LayerItem_DragMoveModeChanged;
                //v.EventDispose();
                this.simoziBox.Controls.Remove((Control)v);
                var fp = (Control)v;
                fp.Dispose();
            }
            this.NeedMerge = true;

            this.LayerItems.Clear();
        }
        private void InitializeLayer()
        {
            this.PictureRefleshFlag = false;
            var n = this.Scene.Parts.Count;


            ClearLayer();
            for (int i = 1; i < this.Scene.Parts.Count(); i++)
            {
                var layer = this.CreateLayer(this.Scene.Parts[i]);
                this.AddLayer(layer, false);                
            }
            if (n == 1)
            {
                this.SimoziMode = SuouTextBox2.Controls.SimoziMode.Single;
                this.simoziBox.IsMultiMode = false;
            }
            else
            {
                this.SimoziMode = SuouTextBox2.Controls.SimoziMode.Multi;
                this.simoziBox.IsMultiMode = true;
                
            }
            this.RefreshAll();
            this.PictureRefleshFlag = true;

            //this.SimoziTextBox.SetRawText(this.PageItem.Text);//これを上にもっていけば早くなる

        }
        public void RefreshAll()
        {
            if (this.SimoziMode == Controls.SimoziMode.Multi)
            {
                this.simoziBox.UpdateText();
            }
            else
            {
            }
        }
        #endregion
        #region MergeMethod
        public void NeedMergeCheck()
        {
               /*
            var a = (LayerItems.Exists((v) => v.NeedMerge) || this.simoziBox.NeedMerge || this.NeedMerge);
            var b = this.LayerItems.All((v) => v.IsViewMode);

            if ( a && this.simoziBox.IsMultiMode && b && this.PictureRefleshFlag)
            {
                this.simoziBox.NeedMerge = false;
                this.NeedMerge = false;
                LayerItems.ForEach((v) => v.NeedMerge = false);
            }
            */

        }
        /*
        public void TemporaryLayerMerge()
        {
            //TemporaryCheck();
            //var NewText =  this.PageItem.Merge();
            this.Ctrl.SetText(this.PageItem.SText, TextChangeType.TemporaryMerge);
            this.Ctrl.Refresh();
        }
        */

        public void DisposeMerge()
        {
            this.LayerItems.Where((v) => !v.IsViewMode).ToList().ForEach((v) => v.SwitchPicture());
            //var NewText = this.PageItem.Merge();
            //this.PageItem.SText = NewText;
        }
        #endregion
        #region Method
        public void RenewPageItems()
        {
            Scene.Parts.Clear();
            Scene.Parts.Add(BackGroundPage);
            foreach (var v in this.LayerItems) Scene.Parts.Add(v.BackgroundPage);
        }
        public List<SectionScenePart> GetPageParts()
        {
            List<SectionScenePart> r = new List<SectionScenePart>();
            r.Add(this.BackGroundPage);
            foreach (var v in this.LayerItems) r.Add(v.BackgroundPage);
            return r;
        }
        public void AllSharrowHide()
        {
            this.LayerItems.ForEach((v) => v.Visible = false);
        }
        public void AllSharrowShow()
        {
            this.LayerItems.Where((v) => v.BackgroundPage.Enable).ToList().ForEach((v) => v.Visible = true);
        }
        #endregion
        private void Content_Changed(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    if (e.NewItems.Count > 1) throw new Exception();
                    var layer = this.CreateLayer(e.NewItems[0] as SectionScenePart);
                    this.AddLayer(layer,true);
                    this.simoziBox.UpdateText();
                    break;
                case NotifyCollectionChangedAction.Move:
                    var newIndexOfLayers = e.NewStartingIndex - 1;
                    var oldIndexOfLayers = e.OldStartingIndex - 1;
                    if (newIndexOfLayers >= 0 && oldIndexOfLayers >= 0)
                    {
                        var tmp = this.LayerItems[newIndexOfLayers];
                        this.LayerItems[newIndexOfLayers] = this.LayerItems[oldIndexOfLayers];
                        this.LayerItems[oldIndexOfLayers] = tmp;
                        this.simoziBox.UpdateText();
                        ReBringToFront();
                        if (LayerItemChanged != null) LayerItemChanged(this, new EventArgs());
                    }
                    else
                    {
                        throw new NotSupportedException();
                    }
                    break;

                case NotifyCollectionChangedAction.Remove:
                    if (e.OldItems.Count > 1) throw new Exception();
                    var removeLayer = this.LayerItems.First((p) => p.BackgroundPage == e.OldItems[0]);
                    this.pRemoveLayer(removeLayer);
                    this.simoziBox.UpdateText();
                    break;
                case NotifyCollectionChangedAction.Replace:
                    break;
                case NotifyCollectionChangedAction.Reset:
                    this.ClearLayer();
                    break;
            }
        }

        #region Layer_EventMethod
        private void LayerItem_ViewModeChanged(object sender, EventArgs e)
        {
            if (this.LayerItems.All((t) => t.IsViewMode))
            {
                this.simoziBox.UpdateText();
            }
        }
        private void LayerItem_DragMoveModeChanged(object sender, EventArgs e)
        {
            var p = (TextLayerBox)sender;
            if (p.IsDragMoveMode)
            {
                //this.simoziBox.UpdateText();
            }
            else
            {
                if (this.simoziBox.SaveData.IsDragEndMerge)
                {
                    this.ChangeViewMode();
                }
            }
        }
        public void ChangeViewMode()
        {
            this.NeedMerge = true;
            //this.BackGroundPage.Enable = true;
            Action f = () => this.simoziBox.UpdateText();
            BSystem.Diagnostics.ConsoleStopWatch(f, "UpdateText");
            //this.Ctrl.UpdateText();

        }
        private void LayerItem_EditLocationChanged(object sender, EventArgs e)
        {
            if (!this.IsEnableMerge) return;
            var nowtime = DateTime.Now;
            var time = nowtime - PreTime;
            
            if (time.TotalMilliseconds > this.simoziBox.SaveData.MergePase && !this.simoziBox.SaveData.IsDragEndMerge)
            {
                NeedMergeCheck();
                this.PreTime = nowtime;

                //20130425_下三行をこの括弧の中に入れてみた
                Hsb.BringToFront();
                Vsb.BringToFront();
                this.simoziBox.Refresh();//2012/12/28
            }
            
            
            
        }
        private void TextBox_Enter(object sender, EventArgs e)
        {
            var f = (SuouTextBox)sender;
            var fp = this.LayerItems.Find((v) => v.TextBox == f);
            SelectedLayerItem = fp;
        }
        private void TextLayer_Enter(object sender, EventArgs e)
        {
            var f = (TextLayerBox)sender;
            var fp = this.LayerItems.Find((v) => v == f);
            this.LayerItems.ToList().ForEach((v) => { if (v != f)v.SwitchPicture(); });
            SelectedLayerItem = fp;
            this.suouTextBox.DoLostFocus();
        }
        public void Simozi_Enter(bool userInputEnable)
        {
            this.LayerItems.ToList().ForEach((v) => { v.SwitchPicture(); });
            this.SelectedLayerItem = this.simoziBox;
        }
        #endregion
    }
    
}
