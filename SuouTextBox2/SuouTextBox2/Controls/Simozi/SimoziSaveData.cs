﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using System.Drawing;
using System.Threading.Tasks;
using SuouBase;
using System.Diagnostics;
using SuouTextBox2.Controls;
using SuouBase.Text;
using SuouTextBox2.Base;
using SuouTextBox2.Settings;
using System.Xml.Linq;
using SuouBase.Base;
using System.Xml.Serialization;
namespace SuouTextBox2.Controls
{
    [Serializable]
    public class SimoziBoxSaveData
    {
        public TextBoxShortCutKeys ShortCutKeys { get; set; }
        public int _MergePase = 300;
        public int MergePase
        {
            get { return this._MergePase; }
            set
            {
                this._MergePase = value;
            }
        }
        private bool _IsDragEndMerge = true;
        public bool IsDragEndMerge
        {
            get
            {
                return _IsDragEndMerge;
            }
            set
            {
                this._IsDragEndMerge = value;
            }
        }
        private SuouTextBoxSetting _SuouTextSetting = new SuouTextBoxSetting();
        public SuouTextBoxSetting SuouTextSetting
        {
            get { return this._SuouTextSetting; }
            set
            {
                if (value != null)
                {
                    this._SuouTextSetting = value;
                }
            }
        }
        private HukidasiSaveData _Hukidasi = HukidasiSaveData.NoneDefault();
        public HukidasiSaveData Hukidasi
        {
            get
            {
                this._Hukidasi = this._Hukidasi ?? HukidasiSaveData.NoneDefault();
                return _Hukidasi;
            }
            set
            {
                this._Hukidasi = value;
            }
        }
        private SpaceLineSet _MergeAlgorithm = SpaceLineSet.UniDefaultSet;
        public SpaceLineSet MergeAlgorithm
        {
            get
            {
                this._MergeAlgorithm = _MergeAlgorithm ?? SpaceLineSet.UniDefaultSet;
                return _MergeAlgorithm;
            }
            set
            {
                this._MergeAlgorithm = value;
            }
        }
        private SpaceLineSet _LayerMergeAlgorithm = SpaceLineSet.UniDefaultSet;
        public SpaceLineSet LayerAlgorithm
        {
            get
            {
                this._LayerMergeAlgorithm = _LayerMergeAlgorithm ?? SpaceLineSet.UniDefaultSet;
                return _LayerMergeAlgorithm;
            }
            set
            {
                this._LayerMergeAlgorithm = value;
            }
        }
        private SpaceLineSet _WakuMergeAlgorithm = SpaceLineSet.DefaultSet;
        public SpaceLineSet WakuAlgorithm
        {
            get
            {
                this._WakuMergeAlgorithm = _WakuMergeAlgorithm ?? SpaceLineSet.UniDefaultSet;
                return _WakuMergeAlgorithm;
            }
            set
            {
                this._WakuMergeAlgorithm = value;
            }
        }
        private SpaceLineSet _SerihuMergeAlgorithm = SpaceLineSet.UniDefaultSet;
        public SpaceLineSet SerihuAlgorithm
        {
            get
            {
                this._SerihuMergeAlgorithm = _SerihuMergeAlgorithm ?? SpaceLineSet.UniDefaultSet;
                return _SerihuMergeAlgorithm;
            }
            set
            {
                this._SerihuMergeAlgorithm = value;
            }
        }

     
        public void Save(string filename)
        {
            BSystem.XML.NonBinarySerializer.Save<SimoziBoxSaveData>(filename, this);
            
            //BinarySerialize.SaveToBinaryFile(this, filename);
        }
        public static SimoziBoxSaveData Load(string filename)
        {
            var f = BSystem.XML.NonBinarySerializer.Load<SimoziBoxSaveData>(filename);
            //var f = BinarySerialize.LoadFromBinaryFile(filename);
            if (f != null)
            {
                return (SimoziBoxSaveData)f;
            }
            else
            {
                return null;
            }
        }
    }
}
