﻿namespace SuouTextBox2.Controls
{
    partial class SimoziBox
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            SuouTextBox2.Operator.InversePen inversePen1 = new SuouTextBox2.Operator.InversePen();
            SuouTextBox2.Base.PrimitiveChoice primitiveChoice1 = new SuouTextBox2.Base.PrimitiveChoice();
            SuouBase.Text.InverseDataLapper inverseDataLapper1 = new SuouBase.Text.InverseDataLapper();
            SuouBase.Text.InverseDataCollection inverseDataCollection1 = new SuouBase.Text.InverseDataCollection();
            this.suouTextBox1 = new SuouTextBox2.SuouTextBox();
            this.EditMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.layerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.showLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteOnLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TemporaryMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addNewLayerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteOnLayerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteLayerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.EditMenuStrip.SuspendLayout();
            this.TemporaryMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // suouTextBox1
            // 
            this.suouTextBox1.AllowDrop = true;
            this.suouTextBox1.CanOriginalDrag = false;
            this.suouTextBox1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.suouTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.suouTextBox1.Font = new System.Drawing.Font("ＭＳ Ｐゴシック", 12F);
            this.suouTextBox1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.suouTextBox1.IsFixedScroll = false;
            this.suouTextBox1.IsInnerScrollBar = false;
            this.suouTextBox1.IsRuler = true;
            this.suouTextBox1.IsVisibleCaret = true;
            this.suouTextBox1.Location = new System.Drawing.Point(0, 0);
            this.suouTextBox1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.suouTextBox1.Name = "suouTextBox1";
            this.suouTextBox1.PaddingSize = new System.Drawing.Size(5, 25);
            inversePen1.ClickButton = System.Windows.Forms.MouseButtons.Left;
            inversePen1.Ctrl = this.suouTextBox1;
            primitiveChoice1.IsContinuity = false;
            inversePen1.ICB = primitiveChoice1;
            inverseDataLapper1.Item = inverseDataCollection1;
            inversePen1.InverseData = inverseDataLapper1;
            this.suouTextBox1.Pen = inversePen1;
            this.suouTextBox1.Size = new System.Drawing.Size(495, 379);
            this.suouTextBox1.TabIndex = 1;
            this.suouTextBox1.UseDebug = false;
            this.suouTextBox1.UserInputEnable = false;
            this.suouTextBox1.HsbValueChanged += new System.EventHandler(this.suouTextBox1_HsbValueChanged);
            this.suouTextBox1.VsbValueChanged += new System.EventHandler(this.suouTextBox1_HsbValueChanged);
            this.suouTextBox1.ContextOpening += new System.EventHandler(this.suouTextBox1_ContextOpening);
            this.suouTextBox1.DragDrop += new System.Windows.Forms.DragEventHandler(this.suouTextBox1_DragDrop);
            this.suouTextBox1.DragEnter += new System.Windows.Forms.DragEventHandler(this.suouTextBox1_DragEnter);
            this.suouTextBox1.DoubleClick += new System.EventHandler(this.suouTextBox1_DoubleClick);
            this.suouTextBox1.Enter += new System.EventHandler(this.suouTextBox1_Enter);
            this.suouTextBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.suouTextBox1_MouseClick);
            this.suouTextBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.suouTextBox1_MouseDown);
            this.suouTextBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.suouTextBox1_MouseMove);
            // 
            // EditMenuStrip
            // 
            this.EditMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.layerToolStripMenuItem,
            this.showLayerToolStripMenuItem,
            this.pasteOnLayerToolStripMenuItem,
            this.pasteLayerToolStripMenuItem,
            this.toolStripSeparator2});
            this.EditMenuStrip.Name = "contextMenuStrip1";
            this.EditMenuStrip.Size = new System.Drawing.Size(197, 104);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(193, 6);
            // 
            // layerToolStripMenuItem
            // 
            this.layerToolStripMenuItem.Name = "layerToolStripMenuItem";
            this.layerToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.layerToolStripMenuItem.Text = "新しいレイヤーを追加";
            this.layerToolStripMenuItem.Click += new System.EventHandler(this.layerToolStripMenuItem_Click_1);
            // 
            // showLayerToolStripMenuItem
            // 
            this.showLayerToolStripMenuItem.Name = "showLayerToolStripMenuItem";
            this.showLayerToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.showLayerToolStripMenuItem.Text = "レイヤーを表示";
            this.showLayerToolStripMenuItem.Click += new System.EventHandler(this.showLayerToolStripMenuItem_Click);
            // 
            // pasteOnLayerToolStripMenuItem
            // 
            this.pasteOnLayerToolStripMenuItem.Name = "pasteOnLayerToolStripMenuItem";
            this.pasteOnLayerToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.pasteOnLayerToolStripMenuItem.Text = "レイヤーに貼り付け";
            this.pasteOnLayerToolStripMenuItem.Click += new System.EventHandler(this.pasteOnLayerToolStripMenuItem_Click);
            // 
            // pasteLayerToolStripMenuItem
            // 
            this.pasteLayerToolStripMenuItem.Name = "pasteLayerToolStripMenuItem";
            this.pasteLayerToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.pasteLayerToolStripMenuItem.Text = "レイヤーを貼り付ける";
            this.pasteLayerToolStripMenuItem.Click += new System.EventHandler(this.pasteLayerToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(193, 6);
            // 
            // vScrollBar1
            // 
            this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.vScrollBar1.Location = new System.Drawing.Point(475, 0);
            this.vScrollBar1.Name = "vScrollBar1";
            this.vScrollBar1.Size = new System.Drawing.Size(20, 379);
            this.vScrollBar1.TabIndex = 2;
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.hScrollBar1.Location = new System.Drawing.Point(0, 363);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(475, 16);
            this.hScrollBar1.TabIndex = 3;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 50;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TemporaryMenuStrip
            // 
            this.TemporaryMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.addNewLayerToolStripMenuItem,
            this.pasteOnLayerToolStripMenuItem1,
            this.pasteLayerToolStripMenuItem1});
            this.TemporaryMenuStrip.Name = "contextMenuStrip1";
            this.TemporaryMenuStrip.Size = new System.Drawing.Size(197, 92);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.editToolStripMenuItem.Text = "編集";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // addNewLayerToolStripMenuItem
            // 
            this.addNewLayerToolStripMenuItem.Name = "addNewLayerToolStripMenuItem";
            this.addNewLayerToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.addNewLayerToolStripMenuItem.Text = "新しいレイヤーを追加";
            this.addNewLayerToolStripMenuItem.Click += new System.EventHandler(this.layerToolStripMenuItem_Click_1);
            // 
            // pasteOnLayerToolStripMenuItem1
            // 
            this.pasteOnLayerToolStripMenuItem1.Name = "pasteOnLayerToolStripMenuItem1";
            this.pasteOnLayerToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.pasteOnLayerToolStripMenuItem1.Text = "レイヤーに貼り付け";
            this.pasteOnLayerToolStripMenuItem1.Click += new System.EventHandler(this.pasteOnLayerToolStripMenuItem_Click);
            // 
            // pasteLayerToolStripMenuItem1
            // 
            this.pasteLayerToolStripMenuItem1.Name = "pasteLayerToolStripMenuItem1";
            this.pasteLayerToolStripMenuItem1.Size = new System.Drawing.Size(196, 22);
            this.pasteLayerToolStripMenuItem1.Text = "レイヤーを貼り付ける";
            this.pasteLayerToolStripMenuItem1.Click += new System.EventHandler(this.pasteLayerToolStripMenuItem_Click);
            // 
            // SimoziBox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ContextMenuStrip = this.EditMenuStrip;
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.vScrollBar1);
            this.Controls.Add(this.suouTextBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SimoziBox";
            this.Size = new System.Drawing.Size(495, 379);
            this.EditMenuStrip.ResumeLayout(false);
            this.TemporaryMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip EditMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem layerToolStripMenuItem;
        private System.Windows.Forms.VScrollBar vScrollBar1;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ContextMenuStrip TemporaryMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addNewLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteOnLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteOnLayerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem pasteLayerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteLayerToolStripMenuItem1;
        protected SuouTextBox suouTextBox1;
    }
}
