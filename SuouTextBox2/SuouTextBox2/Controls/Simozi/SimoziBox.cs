﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouTextBox2.Base;
using System.Diagnostics;
using SuouBase.Text;
using SuouBase.Interface;
using SuouBase;
using SuouTextBox2.Operator;
using SuouBase.Drawing;
using BSystem;
using WebExtension;
using BSystem.Text;
using BSystem.Collections;
using SuouBase.Book;
namespace SuouTextBox2.Controls
{
    /// <summary>
    /// SuouTextBoxの一番下のエディタ部分
    /// </summary>
    public partial class SimoziBox : UserControl,LayerItem
    {
        MouseButtons ClickButton { get; set; }
        public ContextMenuStrip ContextMenuList
        {
            get
            {
                return this.suouTextBox1.ContextMenuStrip;
            }
        }
        #region Constructor
        public SimoziBox()
        {
            InitializeComponent();
            this.SetStyle(ControlStyles.DoubleBuffer, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            this.suouTextBox1.SetScrollBar(vScrollBar1, hScrollBar1);

            this.SetSaveData(new SimoziBoxSaveData());

            this.MergeLayerItemCollection = new MergeLayerItemCollection(this, this.suouTextBox1, this.hScrollBar1, this.vScrollBar1);
            this.MergeLayerItemCollection.Scene = new SectionScene();
            this.suouTextBox1.TextChanged += this.SuouTextBox_TextChanged;
            this.suouTextBox1.AddContextMenuStrip(this.EditMenuStrip);
            this.suouTextBox1.Caret.CaretChanged += this.Caret_CaretChanged;
        }
        
        #endregion
        #region SuouTextBox_Event

        protected virtual void suouTextBox1_DoubleClick(object sender, EventArgs e)
        {
            if (this.IsMultiMode && this.ClickButton == System.Windows.Forms.MouseButtons.Left)
            {
                this.IsMultiMode = false;
            }
        }
        private void suouTextBox1_MouseDown(object sender, MouseEventArgs e)
        {
            this.ClickButton = e.Button;
            this.AddNewLocation = this.ModifyLocation(this.PointToClient(Control.MousePosition)).ToEditPoint();
            UserInputEnableCheck();
            if (e.Button == System.Windows.Forms.MouseButtons.Right && this.IsMultiMode && this.UseTemporaryContext)
            {
                this.TemporaryMenuStrip.Show(Control.MousePosition);
            }
        }

        private void suouTextBox1_HsbValueChanged(object sender, EventArgs e)
        {

            SuouBase.Api.WINAPI.StopControlDraw(this.Handle);
            {
                foreach (var item in this.MergeLayerItemCollection.LayerItems)
                {
                    var p = this.suouTextBox1.TranslateToDisplayPoint(item.EditLocation);
                    item.Location = p;
                }
            }
            SuouBase.Api.WINAPI.StartControlDraw(this.Handle);
            this.Refresh();
        }
        public void RefreshPicture()
        {

        }


        private void suouTextBox1_ContextOpening(object sender, EventArgs e)
        {
            var f = this.suouTextBox1.suouTextBoxContextMenuStrip1;
            foreach (var v in f.Items)
            {
                if (v is ToolStripMenuItem)
                {
                    var v2 = (ToolStripMenuItem)v;
                    if (v2.Text == this.layerToolStripMenuItem.Text || v2.Text == this.showLayerToolStripMenuItem.Text || v2.Text == this.pasteOnLayerToolStripMenuItem.Text)
                    {
                        v2.Enabled = this.ParentForm != null;
                    }
                    if (v2.Text == this.showLayerToolStripMenuItem.Text)
                    {
                        if (this.MergeLayerItemCollection.LayerItems.Count == 0)
                        {
                            v2.Enabled = false;
                        }
                        else
                        {
                            v2.Enabled = true;
                        }
                    }
                    if (this.DLLFunction != null && v2.Text == this.pasteLayerToolStripMenuItem.Text)
                    {
                        var layertext = ((SuouTextBoxPlugin)this.DLLFunction.TextBoxPlugin).CopyLayer;
                        v2.Enabled = (layertext != null);
                    }

                }
            }


        }

        private void suouTextBox1_Enter(object sender, EventArgs e)
        {
            if(this.TextBoxMode == TextLayer.TextBoxMode.TextBoxEditReady)
            {
                return;
            }
            this.UserInputEnableCheck();
            this.MergeLayerItemCollection.Simozi_Enter(this.suouTextBox1.UserInputEnable);

        }
        private void SuouTextBox_TextChanged(object sender, EventArgs e)
        {

            if (IsMultiMode)
            {
                this.suouTextBox1.DisplaySet.IsFixedScroll = true;
                //2014/02/23?
                //this.PageItems.PageItem.SText = this.suouTextBox1.RawText;
            }
            else
            {
                this.suouTextBox1.DisplaySet.IsFixedScroll = false;
                this.BackgroundPage.SText = this.suouTextBox1.RawText;
                NeedMerge = true;
            }
            if (TextChanged != null) TextChanged(this, new EventArgs());
        }
        #endregion
        #region EventMethod
        public void PageContentChanged(object sender, EventArgs e)
        {
            if (this.BookPage != null)
            {
                //2014/02/23
                //this.BookPage.SText = this.PageItems.PageItem.SText;
                this.BookPage.Name = this.MergeLayerItemCollection.Scene.Name;
            }
        }
        public void TemporaryDispose()
        {
            this.MergeLayerItemCollection.DisposeMerge();
            this.MergeLayerItemCollection.Scene = new SectionScene();

        }
        public void UserInputEnableCheck()
        {
        }
        private void Caret_CaretChanged(object sender, EventArgs e)
        {
            if (CaretPositionChanged != null) CaretPositionChanged(this, new EventArgs());
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            this.MergeLayerItemCollection.NeedMergeCheck();
        }
        #endregion
        #region Interface
        public void SharrowHide()
        {
        }
        public void SharrowShow()
        {
        }
        #endregion
        #region Drag
        private TextLayerBox DragLayerBox { get; set; }
        private void suouTextBox1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(SuouBase.Drawing.TextThumbImage)))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else if (e.Data.GetDataPresent(typeof(HukidasiSaveData)))
            {
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void suouTextBox1_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(SuouBase.Drawing.TextThumbImage)))
            {
                var p = this.suouTextBox1.ModifyLocation(this.PointToClient(Control.MousePosition));

                var ap = (SuouBase.Drawing.TextThumbImage)e.Data.GetData(typeof(SuouBase.Drawing.TextThumbImage));
                
                var mergepartpage = new SectionScenePart(this.SaveData.Hukidasi,this.SaveData.MergeAlgorithm) { Name = "レイヤー" + this.MergeLayerItemCollection.LayerItems.Count, SText = ap.Decode().SText, EditLocation = p.ToEditPoint() };
                this.MergeLayerItemCollection.Scene.Parts.Add(mergepartpage);
                //this.PageItems.PageItem.Parts.Add(mergepartpage, true);
                var pagee = new BookPageContentChangeEventArgs(ap.Page, PageContentChangeType.Other);
                if (DropPageAccepted != null) DropPageAccepted(this, pagee);
            }
            else if (e.Data.GetDataPresent(typeof(HukidasiSaveData)))
            {
                var ap = (HukidasiSaveData)e.Data.GetData(typeof(HukidasiSaveData));
                var mp = new SectionScenePart(ap,this.SaveData.MergeAlgorithm) { Name = "セリフ" + this.MergeLayerItemCollection.LayerItems.Count,Type = MergePartsType.Comment };
                this.MergeLayerItemCollection.Scene.Parts.Add(mp);
            }
        }
        #endregion
        #region ClickEvent
        private void pasteOnLayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var text = Clipboard.GetText().CheckNewLine().DecodeToSjis();
            var f = new SectionScenePart() { EditLocation = this.AddNewLocation};
            f.SText.Initialize(text);
            
            this.MergeLayerItemCollection.Scene.Parts.Add(f);
        }

        private void suouTextBox1_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left && Control.MouseButtons == System.Windows.Forms.MouseButtons.Right)
            {
                if (!this.IsMultiMode && this.MergeLayerItemCollection.LayerItems.Count > 0) this.IsMultiMode = true;
            }
        }

        private void pasteLayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (this.DLLFunction != null)
            {
                var v = ((SuouTextBoxPlugin)this.DLLFunction.TextBoxPlugin).CopyLayer;
                if (v == null) return;
                //this.PageItems.Add(v.Copy());
                this.MergeLayerItemCollection.Scene.Parts.Add(v.Copy());
            }
        }
        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.IsMultiMode = false;
        }

        private void showLayerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.IsMultiMode = true;
        }
        private void layerToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            var page = this.MergeLayerItemCollection.CreateNewPage(this.AddNewLocation);
            this.MergeLayerItemCollection.Scene.Parts.Add(page);
        }
        #endregion

        private void suouTextBox1_MouseMove(object sender, MouseEventArgs e)
        {
            this.MergeLayerItemCollection.LayerItems.Where((v) => v.IsPictureInnerMouse).ForEach((v) => v.IsPictureInnerMouse = false);
        }

    }
    public enum SimoziMode
    {
        Multi,Single
    }
}
