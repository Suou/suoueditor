﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Drawing;
using System.Windows.Forms;
using SuouTextBox2.Operator;
using SuouTextBox2.Base;
using SuouTextBox2.Controls;

using System.ComponentModel;
using SuouBase.Text;
using SuouBase;
using SuouTextBox2.Debug;
using SuouBase.Drawing;
using WebExtension;
namespace SuouTextBox2.Controls
{
    public class SuouTextBoxContextMenuStrip:ContextMenuStrip
    {
        public event EventHandler ContextOpening;
        private ToolStripMenuItem cutToolStripMenuItem;
        private ToolStripMenuItem copyToolStripMenuItem;
        private ToolStripMenuItem pasteToolStripMenuItem;
        private ToolStripMenuItem deleteToolStripMenuItem;
        private ToolStripMenuItem allInverseChoiceToolStripMenuItem;
        private ToolStripMenuItem spaceReplaceMenuItem;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripMenuItem undoToolStripMenuItem;
        private ToolStripMenuItem redoToolStripMenuItem;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripMenuItem changeToolStripMenuItem;
        private ToolStripMenuItem LineChoiceToolStripMenuItem;
        private ToolStripMenuItem RectangleChoiceToolStripMenuItem;
        private ToolStripMenuItem GataChoiceToolStripMenuItem;
        private ToolStripMenuItem rectangleChoiceTwoToolStripMenuItem;
        private ToolStripMenuItem gataChoiceTwoToolStripMenuItem;
        private ToolStripMenuItem transparentizeToolStripMenuItem;
        private ToolStripMenuItem specialToolStripMenuItem;
        private ToolStripMenuItem deleteRightSpaceToolStripMenuItem;
        private ToolStripMenuItem moveLeftToolStripMenuItem;
        private ToolStripMenuItem conditionToolStripMenuItem;
        private ToolStripMenuItem caretToolStripMenuItem;
        private ToolStripMenuItem eraserToolStripMenuItem;
        private ToolStripMenuItem Eraser4Item;
        private ToolStripMenuItem Eraser8Item;
        private ToolStripMenuItem Eraser16Item;
        private ToolStripMenuItem Eraser32Item;
        private ToolStripMenuItem tankToolStripMenuItem;
        private ToolStripTextBox toolStripTextBox1;

        
        public SuouTextBox HoldingEditor { get; set; }

        public SuouTextBoxContextMenuStrip()
            : base()
        {
            this.InitializeComponent();
        }
        private void InitializeComponent()
        {
            this.cutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.allInverseChoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.spaceReplaceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.changeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LineChoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RectangleChoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.GataChoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rectangleChoiceTwoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gataChoiceTwoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.transparentizeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.specialToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteRightSpaceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.moveLeftToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.conditionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.caretToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eraserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Eraser4Item = new System.Windows.Forms.ToolStripMenuItem();
            this.Eraser8Item = new System.Windows.Forms.ToolStripMenuItem();
            this.Eraser16Item = new System.Windows.Forms.ToolStripMenuItem();
            this.Eraser32Item = new System.Windows.Forms.ToolStripMenuItem();
            this.tankToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.SuspendLayout();
            // 
            // cutToolStripMenuItem
            // 
            this.cutToolStripMenuItem.Name = "cutToolStripMenuItem";
            this.cutToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.cutToolStripMenuItem.Text = "切り取り";
            this.cutToolStripMenuItem.Click += new System.EventHandler(this.cutToolStripMenuItem_Click);
            // 
            // copyToolStripMenuItem
            // 
            this.copyToolStripMenuItem.Name = "copyToolStripMenuItem";
            this.copyToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.copyToolStripMenuItem.Text = "コピー";
            this.copyToolStripMenuItem.Click += new System.EventHandler(this.copyToolStripMenuItem_Click);
            // 
            // pasteToolStripMenuItem
            // 
            this.pasteToolStripMenuItem.Name = "pasteToolStripMenuItem";
            this.pasteToolStripMenuItem.Size = new System.Drawing.Size(124, 22);
            this.pasteToolStripMenuItem.Text = "貼り付け";
            this.pasteToolStripMenuItem.Click += new System.EventHandler(this.pasteToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.deleteToolStripMenuItem.Text = "削除";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // allInverseChoiceToolStripMenuItem
            // 
            this.allInverseChoiceToolStripMenuItem.Name = "allInverseChoiceToolStripMenuItem";
            this.allInverseChoiceToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.allInverseChoiceToolStripMenuItem.Text = "全て選択";
            this.allInverseChoiceToolStripMenuItem.Click += new System.EventHandler(this.allInverseChoiceToolStripMenuItem_Click);
            // 
            // spaceReplaceMenuItem
            // 
            this.spaceReplaceMenuItem.Name = "spaceReplaceMenuItem";
            this.spaceReplaceMenuItem.Size = new System.Drawing.Size(166, 22);
            this.spaceReplaceMenuItem.Text = "空白置換";
            this.spaceReplaceMenuItem.Click += new System.EventHandler(this.spaceReplaceMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(163, 6);
            // 
            // undoToolStripMenuItem
            // 
            this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
            this.undoToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.undoToolStripMenuItem.Text = "元に戻す";
            this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
            // 
            // redoToolStripMenuItem
            // 
            this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
            this.redoToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.redoToolStripMenuItem.Text = "やり直し";
            this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(163, 6);
            // 
            // changeToolStripMenuItem
            // 
            this.changeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.LineChoiceToolStripMenuItem,
            this.RectangleChoiceToolStripMenuItem,
            this.GataChoiceToolStripMenuItem,
            this.rectangleChoiceTwoToolStripMenuItem,
            this.gataChoiceTwoToolStripMenuItem});
            this.changeToolStripMenuItem.Name = "changeToolStripMenuItem";
            this.changeToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.changeToolStripMenuItem.Text = "選択";
            // 
            // LineChoiceToolStripMenuItem
            // 
            this.LineChoiceToolStripMenuItem.Name = "LineChoiceToolStripMenuItem";
            this.LineChoiceToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.LineChoiceToolStripMenuItem.Text = "行選択";
            this.LineChoiceToolStripMenuItem.Click += new System.EventHandler(this.LineChoiceToolStripMenuItem_Click);
            // 
            // RectangleChoiceToolStripMenuItem
            // 
            this.RectangleChoiceToolStripMenuItem.Name = "RectangleChoiceToolStripMenuItem";
            this.RectangleChoiceToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.RectangleChoiceToolStripMenuItem.Text = "矩形選択（単）";
            this.RectangleChoiceToolStripMenuItem.Click += new System.EventHandler(this.RectangleChoiceToolStripMenuItem_Click);
            // 
            // GataChoiceToolStripMenuItem
            // 
            this.GataChoiceToolStripMenuItem.Name = "GataChoiceToolStripMenuItem";
            this.GataChoiceToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.GataChoiceToolStripMenuItem.Text = "ガタ矩形選択（単）";
            this.GataChoiceToolStripMenuItem.Click += new System.EventHandler(this.GataChoiceToolStripMenuItem_Click);
            // 
            // rectangleChoiceTwoToolStripMenuItem
            // 
            this.rectangleChoiceTwoToolStripMenuItem.Name = "rectangleChoiceTwoToolStripMenuItem";
            this.rectangleChoiceTwoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.rectangleChoiceTwoToolStripMenuItem.Text = "矩形選択（複）";
            this.rectangleChoiceTwoToolStripMenuItem.Click += new System.EventHandler(this.rectangleChoiceTwoToolStripMenuItem_Click);
            // 
            // gataChoiceTwoToolStripMenuItem
            // 
            this.gataChoiceTwoToolStripMenuItem.Name = "gataChoiceTwoToolStripMenuItem";
            this.gataChoiceTwoToolStripMenuItem.Size = new System.Drawing.Size(184, 22);
            this.gataChoiceTwoToolStripMenuItem.Text = "ガタ矩形選択（複）";
            this.gataChoiceTwoToolStripMenuItem.Click += new System.EventHandler(this.gataChoiceTwoToolStripMenuItem_Click);
            // 
            // transparentizeToolStripMenuItem
            // 
            this.transparentizeToolStripMenuItem.Name = "transparentizeToolStripMenuItem";
            this.transparentizeToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.transparentizeToolStripMenuItem.Text = "透明化";
            this.transparentizeToolStripMenuItem.Click += new System.EventHandler(this.transparentizeToolStripMenuItem_Click);
            // 
            // specialToolStripMenuItem
            // 
            this.specialToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteRightSpaceToolStripMenuItem,
            this.moveLeftToolStripMenuItem});
            this.specialToolStripMenuItem.Name = "specialToolStripMenuItem";
            this.specialToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.specialToolStripMenuItem.Text = "変形";
            // 
            // deleteRightSpaceToolStripMenuItem
            // 
            this.deleteRightSpaceToolStripMenuItem.Name = "deleteRightSpaceToolStripMenuItem";
            this.deleteRightSpaceToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.deleteRightSpaceToolStripMenuItem.Text = "右側の空白を削る";
            this.deleteRightSpaceToolStripMenuItem.Click += new System.EventHandler(this.deleteRightSpaceToolStripMenuItem_Click);
            // 
            // moveLeftToolStripMenuItem
            // 
            this.moveLeftToolStripMenuItem.Name = "moveLeftToolStripMenuItem";
            this.moveLeftToolStripMenuItem.Size = new System.Drawing.Size(196, 22);
            this.moveLeftToolStripMenuItem.Text = "テキストを左に動かす";
            this.moveLeftToolStripMenuItem.Click += new System.EventHandler(this.moveLeftToolStripMenuItem_Click);
            // 
            // conditionToolStripMenuItem
            // 
            this.conditionToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.caretToolStripMenuItem,
            this.eraserToolStripMenuItem,
            this.tankToolStripMenuItem});
            this.conditionToolStripMenuItem.Name = "conditionToolStripMenuItem";
            this.conditionToolStripMenuItem.Size = new System.Drawing.Size(166, 22);
            this.conditionToolStripMenuItem.Text = "カーソル";
            this.conditionToolStripMenuItem.DropDownOpening += new System.EventHandler(this.conditionToolStripMenuItem_DropDownOpening);
            // 
            // caretToolStripMenuItem
            // 
            this.caretToolStripMenuItem.Image = global::SuouTextBox2.Properties.Resources.cursor;
            this.caretToolStripMenuItem.Name = "caretToolStripMenuItem";
            this.caretToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.caretToolStripMenuItem.Text = "キャレット";
            this.caretToolStripMenuItem.Click += new System.EventHandler(this.caretToolStripMenuItem_Click);
            // 
            // eraserToolStripMenuItem
            // 
            this.eraserToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Eraser4Item,
            this.Eraser8Item,
            this.Eraser16Item,
            this.Eraser32Item});
            this.eraserToolStripMenuItem.Image = global::SuouTextBox2.Properties.Resources.kesigomu;
            this.eraserToolStripMenuItem.Name = "eraserToolStripMenuItem";
            this.eraserToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.eraserToolStripMenuItem.Text = "消しゴム";
            // 
            // Eraser4Item
            // 
            this.Eraser4Item.Name = "Eraser4Item";
            this.Eraser4Item.Size = new System.Drawing.Size(90, 22);
            this.Eraser4Item.Text = "4";
            this.Eraser4Item.Click += new System.EventHandler(this.Eraser4Item_Click);
            // 
            // Eraser8Item
            // 
            this.Eraser8Item.Name = "Eraser8Item";
            this.Eraser8Item.Size = new System.Drawing.Size(90, 22);
            this.Eraser8Item.Text = "8";
            this.Eraser8Item.Click += new System.EventHandler(this.Eraser8Item_Click);
            // 
            // Eraser16Item
            // 
            this.Eraser16Item.Name = "Eraser16Item";
            this.Eraser16Item.Size = new System.Drawing.Size(90, 22);
            this.Eraser16Item.Text = "16";
            this.Eraser16Item.Click += new System.EventHandler(this.Eraser16Item_Click);
            // 
            // Eraser32Item
            // 
            this.Eraser32Item.Name = "Eraser32Item";
            this.Eraser32Item.Size = new System.Drawing.Size(90, 22);
            this.Eraser32Item.Text = "32";
            this.Eraser32Item.Click += new System.EventHandler(this.Eraser32Item_Click);
            // 
            // tankToolStripMenuItem
            // 
            this.tankToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.tankToolStripMenuItem.Image = global::SuouTextBox2.Properties.Resources.tank6;
            this.tankToolStripMenuItem.Name = "tankToolStripMenuItem";
            this.tankToolStripMenuItem.Size = new System.Drawing.Size(136, 22);
            this.tankToolStripMenuItem.Text = "塗りつぶし";
            this.tankToolStripMenuItem.Click += new System.EventHandler(this.tankToolStripMenuItem_Click);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 25);
            this.toolStripTextBox1.TextChanged += new System.EventHandler(this.toolStripTextBox1_TextChanged);
            // 
            // SuouTextBoxContextMenuStrip
            // 
            this.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutToolStripMenuItem,
            this.copyToolStripMenuItem,
            this.pasteToolStripMenuItem,
            this.deleteToolStripMenuItem,
            this.allInverseChoiceToolStripMenuItem,
            this.spaceReplaceMenuItem,
            this.toolStripSeparator1,
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator2,
            this.changeToolStripMenuItem,
            this.transparentizeToolStripMenuItem,
            this.specialToolStripMenuItem,
            this.conditionToolStripMenuItem});
            this.Size = new System.Drawing.Size(125, 280);
            this.Text = "編集";
            this.ResumeLayout(false);

        }
        private void cutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Cut();
        }

        private void copyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Copy();
        }

        private void pasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Paste();
        }

        private void undoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Undo();
        }

        private void redoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Redo();
        }
        private void LineChoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.ChangePen(new PrimitiveChoice());
        }

        private void RectangleChoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.ChangePen(new RectangleChoice(false));
        }

        private void GataChoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.ChangePen(new GataRectangleChoice(false));
        }
        private void rectangleChoiceTwoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.ChangePen(new RectangleChoice(true));
        }

        private void gataChoiceTwoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.ChangePen(new GataRectangleChoice(true));
        }
        private void allInverseChoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.ChoiceAll();
        }
        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Delete();
        }
        private void spaceReplaceMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.SpaceReplace();
        }

        private void Eraser4Item_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Pen = new SuouTextBox2.Operator.MonoMouse() { ChoiceLength = 4 };
        }

        private void Eraser8Item_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Pen = new SuouTextBox2.Operator.MonoMouse() { ChoiceLength = 8 };
        }

        private void Eraser16Item_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Pen = new SuouTextBox2.Operator.MonoMouse() { ChoiceLength = 16 };
        }

        private void Eraser32Item_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Pen = new SuouTextBox2.Operator.MonoMouse() { ChoiceLength = 32 };
        }

        private void caretToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Pen = new SuouTextBox2.Operator.InversePen();
        }

        private void tankToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Pen = new SuouTextBox2.Operator.TankMouse(false, this.toolStripTextBox1.TextBox.Text);
            this.Close();
        }
        private void deleteRightSpaceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.DeleteRightSpace();
        }

        private void moveLeftToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.MoveLeft();
        }
        private void transparentizeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.HoldingEditor.Transparentize();
        }
        private void conditionToolStripMenuItem_DropDownOpening(object sender, EventArgs e)
        {
            this.caretToolStripMenuItem.Checked = false;
            this.eraserToolStripMenuItem.Checked = false;
            this.tankToolStripMenuItem.Checked = false;
            if (this.HoldingEditor.Pen is SuouTextBox2.Operator.InversePen)
            {
                this.caretToolStripMenuItem.Checked = true;
            }
            else if (this.HoldingEditor.Pen is SuouTextBox2.Operator.MonoMouse)
            {
                this.eraserToolStripMenuItem.Checked = true;
            }
            else if (this.HoldingEditor.Pen is SuouTextBox2.Operator.TankMouse)
            {
                this.tankToolStripMenuItem.Checked = true;
            }
        }
        private void toolStripTextBox1_TextChanged(object sender, EventArgs e)
        {
            this.HoldingEditor.Pen = new SuouTextBox2.Operator.TankMouse(false, this.toolStripTextBox1.TextBox.Text);
        }

        public void contextMenuStrip1_Opening(object sender, EventArgs e)
        {
            if (ContextOpening != null)
            {
                ContextOpening(this, new EventArgs());
            }
            if (!this.HoldingEditor.UserInputEnable)
            {
                undoToolStripMenuItem.Enabled = false;
                redoToolStripMenuItem.Enabled = false;

                pasteToolStripMenuItem.Enabled = false;

                cutToolStripMenuItem.Enabled = false;
                copyToolStripMenuItem.Enabled = false;
                deleteToolStripMenuItem.Enabled = false;
                allInverseChoiceToolStripMenuItem.Enabled = false;
                changeToolStripMenuItem.Enabled = false;
                spaceReplaceMenuItem.Enabled = false;
                transparentizeToolStripMenuItem.Enabled = false;
            }
            if (this.HoldingEditor.ParentForm == null)
            {

                undoToolStripMenuItem.Enabled = false;
                redoToolStripMenuItem.Enabled = false;

                pasteToolStripMenuItem.Enabled = false;

                cutToolStripMenuItem.Enabled = false;
                copyToolStripMenuItem.Enabled = false;
                deleteToolStripMenuItem.Enabled = false;
                allInverseChoiceToolStripMenuItem.Enabled = false;
                changeToolStripMenuItem.Enabled = false;
                spaceReplaceMenuItem.Enabled = false;
                transparentizeToolStripMenuItem.Enabled = false;
            }
            else
            {
                changeToolStripMenuItem.Enabled = true;
                allInverseChoiceToolStripMenuItem.Enabled = true;
                undoToolStripMenuItem.Enabled = this.HoldingEditor.TextOperator.StockOperator.CanStoreBack;
                redoToolStripMenuItem.Enabled = this.HoldingEditor.TextOperator.StockOperator.CanStoreForward;

                pasteToolStripMenuItem.Enabled = this.HoldingEditor.CanPaste;

                cutToolStripMenuItem.Enabled = this.HoldingEditor.CanCut;
                copyToolStripMenuItem.Enabled = this.HoldingEditor.CanCopy;
                deleteToolStripMenuItem.Enabled = this.HoldingEditor.CanDelete;
                transparentizeToolStripMenuItem.Enabled = this.HoldingEditor.CanTransparentize;
                spaceReplaceMenuItem.Enabled = this.HoldingEditor.CanSpaceReplace;
            }
        }
    }
}
