﻿namespace SuouTextBox2.Settings
{
    partial class TextBoxSetting
    {
        /// <summary> 
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region コンポーネント デザイナーで生成されたコード

        /// <summary> 
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を 
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.shortCutRegister1 = new SuouBase.Controls.ShortCutRegister();
            this.SuspendLayout();
            // 
            // shortCutRegister1
            // 
            this.shortCutRegister1.Dock = System.Windows.Forms.DockStyle.Top;
            this.shortCutRegister1.Items = null;
            this.shortCutRegister1.Location = new System.Drawing.Point(0, 0);
            this.shortCutRegister1.Name = "shortCutRegister1";
            this.shortCutRegister1.Size = new System.Drawing.Size(574, 159);
            this.shortCutRegister1.TabIndex = 0;
            // 
            // TextBoxSetting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.shortCutRegister1);
            this.Name = "TextBoxSetting";
            this.Size = new System.Drawing.Size(574, 438);
            this.ResumeLayout(false);

        }

        #endregion

        private SuouBase.Controls.ShortCutRegister shortCutRegister1;
    }
}
