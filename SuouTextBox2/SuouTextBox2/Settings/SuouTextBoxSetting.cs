﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SuouBase.Text;

namespace SuouTextBox2.Base
{
    [Serializable]
    public class SuouTextBoxSetting
    {
        private int _CopyEncodeType = (int)EncodeType.SJIS16;
        public EncodeType CopyEncodeType
        {
            get { return (EncodeType)this._CopyEncodeType;}
            set { this._CopyEncodeType = (int)value; }
        }

    }
}
