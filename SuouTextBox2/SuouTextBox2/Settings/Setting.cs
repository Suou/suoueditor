﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SuouBase.Interface;
using SuouTextBox2.Base;
using SuouTextBox2.Controls;
namespace SuouTextBox2.Settings
{
    public partial class TextBoxSetting : UserControl, SettingPage
    {
        public SimoziBox ParentTextBox { get; set; }
        public ShortCutCommandCollection ShortCutItems
        {
            get { return this.shortCutRegister1.Items; }
            set { this.shortCutRegister1.Items = value; }
        }
        public TextBoxSetting()
        {
            InitializeComponent();
        }
        public Form MyForm = new Form();
        public Form Form()
        {
            this.MyForm.Controls.Add(this);
            return MyForm;
        }
        public Control Ctrl { get { return this; } }

        private void button1_Click(object sender, EventArgs e)
        {
            OK();
        }
        public void OK()
        {
            this.shortCutRegister1.Determine();
            //this.ParentTextBox.ShortCutKeys = (TextBoxShortCutKeys)this.shortCutRegister1.Items;
        }
        public void Cancel()
        {
        }
        public string PageName { get { return "テキストボックス(キー)"; } }
    }
}
