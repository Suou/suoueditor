﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Collections;
namespace BSystem
{
    public static class ByteExtension
    {
        public static byte[] BitIndexArray = null;
        public static byte[] InverseIndexArray = null;
        public static int[] IntegerOneBitArray = null;
        public static short[] FirstOneBitIndexArray = null;
        static ByteExtension()
        {
            byte[] r = new byte[8];
            r[7] = _0b00000001;
            r[6] = _0b00000010;
            r[5] = _0b00000100;
            r[4] = _0b00001000;
            r[3] = _0b00010000;
            r[2] = _0b00100000;
            r[1] = _0b01000000;
            r[0] = _0b10000000;
            BitIndexArray = r;

            byte[] r1 = new byte[8];
            r1[7] = _0b11111110;
            r1[6] = _0b11111101;
            r1[5] = _0b11111011;
            r1[4] = _0b11110111;
            r1[3] = _0b11101111;
            r1[2] = _0b11011111;
            r1[1] = _0b10111111;
            r1[0] = _0b01111111;
            InverseIndexArray = r1;

            IntegerOneBitArray = new int[32];
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    var bytes = new byte[4];
                    bytes[i] = ByteExtension.BitIndexArray[7 - j];
                    var n = BitConverter.ToInt32(bytes, 0);
                    IntegerOneBitArray[i * 8 + j] = n;
                }
            }
            FirstOneBitIndexArray = new short[256];
            for (int i = 0; i < 256; i++)
            {
                FirstOneBitIndexArray[i] = GetNaiveFirstOneBit(i);
            }

        }
        public static byte Translate(BitArray items)
        {
            return Translate(items, 0);
        }
        public static byte Translate(BitArray items,int index)
        {
            byte a = 0;
            for (byte i = 0; i < 8; i++)
            {
                var ind = i+index;
                a += ind >= items.Length ? (byte)0 : items[ind] ? (byte)1 : (byte)0;
                if (i < 7) a <<= 1;
            }
            return a;
        }
        public static byte InvTranslate(BitArray items, int index)
        {
            byte a = 0;
            for (sbyte i = 7; i >= 0; i--)
            {
                var ind = i + index;
                a += ind >= items.Length ? (byte)0 : items[ind] ? (byte)1 : (byte)0;
                if (i != 0) a <<= 1;
            }
            return a;
        }

        public static byte[] ToVisualBytes(BitArray items)
        {
            var bound = items.Length / 8;
            var mbound = items.Length % 8;
            var len = bound + (mbound == 0 ? 0 : 1);
            byte[] by = new byte[len];
            for (int i = 0; i < bound; i++)
            {
                by[i] = Translate(items, i * 8);
            }
            if (mbound > 0)
            {
                by[len - 1] = Translate(items, (len - 1) * 8);
            }

            return by;
        }
        


        public static byte[] ToBytes(BitArray items,byte[] r)
        {
            for (int i = 0; i < r.Length; i++)
            {
                r[i] = Translate(items, i * 8);
            }
            return r;
        }
        public static BitArray ToBools(byte[] b)
        {
            var r = new BitArray(b.Length * 8);
            for (int i = 0; i < b.Length; i++)
            {
                ToBools(b[i], r, i * 8);
            }
            return r;
        }
        public const byte _0b00000001 = 1;
        public const byte _0b00000010 = 2;
        public const byte _0b00000100 = 4;
        public const byte _0b00001000 = 8;
        public const byte _0b00010000 = 16;
        public const byte _0b00100000 = 32;
        public const byte _0b01000000 = 64;
        public const byte _0b10000000 = 128;


        public const byte _0b11111110 = 254;
        public const byte _0b11111101 = 253;
        public const byte _0b11111011 = 251;
        public const byte _0b11110111 = 247;
        public const byte _0b11101111 = 239;
        public const byte _0b11011111 = 223;
        public const byte _0b10111111 = 191;
        public const byte _0b01111111 = 127;

        public const int _0b00000000_00000000_00000000_11111111 = 255;
        public const int _0b00000000_00000000_11111111_00000000 = 65280;
        public const int _0b00000000_11111111_00000000_00000000 = 16711680;
        public const int _0b11111111_00000000_00000000_00000000 = -16777216;
        public static BitArray ToBools(byte b)
        {
            BitArray r = new BitArray(8);
            return ToBools(b, r, 0);
        }
        public static BitArray ToBools(byte b,BitArray r,int index)
        {
            r[index + 7] = (b & _0b00000001) > 0;
            r[index + 6] = (b & _0b00000010) > 0;
            r[index + 5] = (b & _0b00000100) > 0;
            r[index + 4] = (b & _0b00001000) > 0;
            r[index + 3] = (b & _0b00010000) > 0;
            r[index + 2] = (b & _0b00100000) > 0;
            r[index + 1] = (b & _0b01000000) > 0;
            r[index + 0] = (b & _0b10000000) > 0;
            return r;
        }
        public static byte ToValue(ushort b, int index,int len)
        {
            var Base = BSystem.MathExtension.Pow(2, 15 - index - len);
            byte r = 0;
            for (int i = 0; i < len; i++)
            {
                var w = BSystem.MathExtension.Pow(2, i);
                r += (byte)(((b & (Base * w)) > 0 ? 1 : 0) * w);
            }
            return r;
        }

        public static bool NaiveGetBit(int n, int nth)
        {
            return Convert.ToString(n, 2).PadLeft(32,'0').Reverse()[nth] == '1' ? true : false;
        }
        public static bool GetBit(int n, int nth)
        {
            return (n & IntegerOneBitArray[nth]) > 0;
        }
        public static bool GetBit(byte n, int nth)
        {
            return (n & BitIndexArray[nth]) > 0;
        }
        public static short GetNaiveFirstOneBit(int a)
        {
            var s1 = Convert.ToString(a, 2).Reverse();
            for (sbyte i = 0; i < s1.Length; i++)
            {
                if (s1[i] == '1') return i;
            }
            return -1;
        }
        private static short GetNaiveFirstOneBit2(int a)
        {
            var intbytes = BitConverter.GetBytes(a);
            for (sbyte i = 0; i < 4; i++)
            {
                if (intbytes[i] != 0)
                {
                    return (short)(FirstOneBitIndexArray[intbytes[i]] + (i*8));
                }
            }
            return -1;
                
        }
        public static short GetFirstOneBit(int a)
        {
            var p = (a & _0b00000000_00000000_00000000_11111111);
            if (p != 0)
            {
                return (short)(FirstOneBitIndexArray[p]);
            }
            p = (a & _0b00000000_00000000_11111111_00000000) >> 8;
            if (p != 0)
            {
                return (short)(FirstOneBitIndexArray[p] + 8);
            }

            p = (a & _0b00000000_11111111_00000000_00000000) >> 16;
            if (p != 0)
            {
                return (short)(FirstOneBitIndexArray[p] + 16);
            }

            p = (a & _0b11111111_00000000_00000000_00000000) >> 24;
            if (p != 0)
            {
                return (short)(FirstOneBitIndexArray[p] + 24);
            }
            return -1;

        }
    }
}
