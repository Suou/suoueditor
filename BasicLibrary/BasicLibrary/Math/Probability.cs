﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem
{
    public static class Probability
    {
        /*
        public static double Average(this IEnumerable<double> items)
        {
            return items.Sum((v) => v) / items.Count();
        }
        */
        public static double Variance(this IEnumerable<double> items)
        {
            var a = items.Select((v) => v * v).Average();
            var b = items.Average();
            return a - (b*b);
        }
        public static double ChiSquaredTest(this IEnumerable<int> items, int k)
        {
            int n = 0;
            double c = 0;
            var arr = new int[k];
            foreach (int v in items)
            {
                n++;
                arr[System.Math.Abs(v % k)]++;
            }
            var f = n / k;
            foreach (var v in arr)
            {
                c += (v - f) * (v - f) / f;
            }
            return c;
        }
    }
}
