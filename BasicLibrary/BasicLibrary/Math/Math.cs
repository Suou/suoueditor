﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem
{
    public static class MathExtension
    {
        public static double Distance(double x1,double y1,double x2,double y2)
        {
            var d1 = x1 - x2;
            var d2 = y1 - y2;
            var d3 = System.Math.Sqrt(d1 * d1 + d2 * d2);
            return d3;
        }
        public static int ManhattanDistance(int x1, int y1, int x2, int y2)
        {
            return (System.Math.Abs((x1 - x2) + System.Math.Abs(y1 - y2)));
        }
        public static int Gcd(int m, int n)
        {
            if (m < n)
            {
                return Gcd(n, m);
            }
            else
            {
                if (n == 0)
                {
                    return m;
                }
                else
                {
                    return Gcd(n, m % n);
                }
            }
        }
        public static int Pow(int x, int y)
        {
            var r = 1;
            for (int i = 0; i < y; i++)
            {
                r *= x;
            }
            return r;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="n"></param>
        /// <returns>Log2Value and the rest</returns>
        public static KeyValuePair<int,int> Log2(int n)
        {
            if (n <= 0)
            {
                throw new Exception();
            }
            else
            {
                int t = 0;
                uint m = 1;
                while (true)
                {
                    var upper = checked(m * 2);
                    if (m <= n && n < upper)
                    {
                        var r = (int)(n - m);
                        return new KeyValuePair<int, int>(t, r);
                    }
                    m *= 2;
                    t++;
                }
            }
        }
        public static int LogIterate(int n)
        {
            return Log2Iterate(n, 0);
        }
        private static int Log2Iterate(int n, int m)
        {
            if (n > 1)
            {
                var log = Log2(n);
                return Log2Iterate(log.Key, m+1);
            }
            else if (n == 1)
            {
                return m;
            }
            else
            {
                throw new Exception();
            }
        }

        public static ushort GetNthValue(int value, int nth)
        {
            var v = value.ToString();
            if (nth >= v.Length)
            {
                return 0;
            }
            else
            {
                return ushort.Parse(v[(v.Length - 1) - nth].ToString());
            }
        }
    }
}
