﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem
{
    public static class RandExtension
    {
        public static string RandomSentence(int alphabetSize,int len)
        {
            return RandExtension.GetRandItems(Characters.Take(alphabetSize).ToArray(), len).Pipe((v)=>String.Join("",v));
        }
        public static IEnumerable<char> RandomAlphabetChars(int alphabetsize)
        {
            while (true)
            {
                yield return Characters[Rander.Next(alphabetsize)];
            }

        }
        public static char[] Characters = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ',', '.' };
        

        

        public static Random Rander = new Random();
        public static T GetRandItem<T>(this IList<T> items)
        {
            return items[Rander.Next(items.Count)];
        }
        public static T GetRandItem<T>(this T[] items)
        {
            return items[Rander.Next(items.Length)];
        }
        public static IEnumerable<T> GetRandItems<T>(this IList<T> items, int len)
        {
            for (int i = 0; i < len; i++)
            {
                yield return items.GetRandItem();
            }
        }
        public static IEnumerable<T> GetRandItems<T>(this T[] items, int len)
        {
            for (int i = 0; i < len; i++)
            {
                yield return items.GetRandItem();
            }
        }

        public static int[] CreateRandomPlusIntegers(int max, int len)
        {
            return Enumerable.Range(0,len).Select((v)=>Rander.Next(max)).ToArray();
        }
        public static bool[] RandBools(int width)
        {
            return Enumerable.Range(0,width).Select((v)=> Rander.Next(2))
                .Select((v) => v == 1 ? true : false)
                .ToArray();
        }

        public static int RandInt(int n)
        {
            return Rander.Next(n);
        }
        public static IEnumerable<int> RandIntInfinitySequence(int maxValue)
        {
            while (true)
            {
                yield return Rander.Next(maxValue);
            }
        }
        public static int RandInt()
        {
            return Rander.Next(int.MaxValue);
        }
        public static char RandChar(char n)
        {
            return (char)Rander.Next(n);
        }

        public static char RandChar()
        {
            return (char)Rander.Next(char.MaxValue);
        }
    }
}
