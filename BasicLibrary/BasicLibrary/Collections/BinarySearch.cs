﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Collections
{
    public enum BinarySearchOption
    {
        Near, FrontNear, RearNear, RangeNear, RangeFrontNear, RangeRearNear
    }
    /// <summary>
    /// List<>
    /// </summary>
    public static class BinarySearchExtension
    {
        public static Int32 BinarySearchIndexOf<T>(this IList<T> list, T value, IComparer<T> comparer = null)
        {
            if (list == null)
                throw new ArgumentNullException("list");

            comparer = comparer ?? Comparer<T>.Default;

            Int32 lower = 0;
            Int32 upper = list.Count - 1;

            while (lower <= upper)
            {
                Int32 middle = lower + (upper - lower) / 2;
                Int32 comparisonResult = comparer.Compare(value, list[middle]);
                if (comparisonResult == 0)
                    return middle;
                else if (comparisonResult < 0)
                    upper = middle - 1;
                else
                    lower = middle + 1;
            }

            return lower-1;
        }
        public static int BinaryNearSearch(this List<int> arr, int n, BinarySearchOption option)
        {            
            var a = arr.BinarySearch(n);
            if (a >= 0) return a;
            var b = -(a) - 2;
            bool bRange = false;
            switch (option)
            {
                case BinarySearchOption.RangeRearNear:
                case BinarySearchOption.RangeFrontNear:
                case BinarySearchOption.RangeNear:
                    bRange = true; break;
                default: bRange = false; break;
            }

            if (a == -1)
            {
                if (bRange) { return a; } else { return 0; }
            }
            if (b == arr.Count - 1)
            {
                if (bRange) { return a; } else { return b; }
            }

            var LeftNearValue = arr[b];
            var RightNearValue = arr[b + 1];
            if (option == BinarySearchOption.FrontNear || option == BinarySearchOption.RangeFrontNear) return b;
            if (option == BinarySearchOption.RangeRearNear || option == BinarySearchOption.RangeRearNear) return b + 1;

            var LeftNearDistance = n - arr[b];
            var RightNearDistance = arr[b + 1] - n;
            if (LeftNearDistance <= RightNearDistance)
            {
                return b;
            }
            else
            {
                return b + 1;
            }
            /*
            if (option == BinarySearchOption.Near || option == BinarySearchOption.RangeNear)
            {
                if (a == -1 && option == BinarySearchOption.RangeNear) return a;

            }

            return b;
            */
        }
    }
}
