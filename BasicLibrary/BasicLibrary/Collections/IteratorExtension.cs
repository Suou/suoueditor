﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Collections
{
    public static class IteratorExtension
    {

        public static IEnumerable<T> PreorderIterator<T>(T item, Func<T, IList<T>> func)
        {
            Stack<T> stack = new Stack<T>(new []{item});
            while (stack.Count != 0)
            {
                var v = stack.Pop();
                yield return v;
                var nextlist = func(v);
                nextlist.Reverse<T>().ForEach((w) => stack.Push(w));
            }
        }
    }
}
