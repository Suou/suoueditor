﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Collections
{
    public class CountDictionary<Key> : Dictionary<Key,int>
    {
        public new int this[Key key]
        {
            get
            {
                if (!this.ContainsKey(key))
                {
                    base[key] = 0;
                }

                return base[key];
            }
            set
            {
                base[key] = value;
            }
        }
    }
}
