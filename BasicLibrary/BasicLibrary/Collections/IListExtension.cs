﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Text;
namespace BSystem.Collections
{
    public static class IListExtension
    {
        public static IEnumerable<LeftRightPair<T>> NonOrderNonRepeatPairCombination<T>(IList<T> items)
        {
            var rst = Combination(items, 0, 2, new List<T>(), false, false);
            foreach(var v in rst){
                yield return new LeftRightPair<T>(v[0], v[1]);
            }
        }
        public static IEnumerable<T[]> NonOrderNonRepeatCombination<T>(IList<T> items,int pairLength)
        {
            return Combination(items, 0, pairLength, new List<T>(), false, false);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="items"></param>
        /// <param name="startIndex"></param>
        /// <param name="max"></param>
        /// <param name="nowItem"></param>
        /// <param name="permitRepeat"></param>
        /// ある列挙要素内のインデックスの重複を許すか
        /// <param name="isOrder"></param>
        /// ある列挙要素内のインデックスの並びは昇順限定であるかどうか
        /// <returns></returns>
        private static IEnumerable<T[]> Combination<T>(IList<T> items, int startIndex, int max, List<T> nowItem,bool permitRepeat,bool isOrder)
        {
            var nokori = max - nowItem.Count;
            var itemNokori = items.Count - startIndex;
            if (nokori <= itemNokori)
            {
                for (int i = startIndex; i < items.Count; i++)
                {
                    var p = nowItem.ToList();
                    p.Add(items[i]);
                    if (p.Count == max)
                    {
                        yield return p.ToArray();
                    }
                    else
                    {
                        IEnumerable<T[]> rst = null;
                        if (isOrder && permitRepeat)
                        {

                            rst = Combination(items, 0, max, p, permitRepeat, isOrder);
                        }
                        else if (isOrder && !permitRepeat)
                        {
                            throw new Exception();
                        }
                        else if (!isOrder && permitRepeat)
                        {
                            rst = Combination(items, startIndex, max, p, permitRepeat, isOrder);
                        }
                        else
                        {
                            rst = Combination(items, startIndex + 1, max, p, permitRepeat, isOrder);
                        }

                        foreach (var v in rst)
                        {
                            yield return v;
                        }
                    }
                }
            }
        }

    }
}
