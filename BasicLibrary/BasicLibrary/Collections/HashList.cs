﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Collections
{
    public class HashList<T> : System.Collections.ObjectModel.ReadOnlyCollection<T> , IComparable<HashList<T>> where T :IComparable<T>
    {
        public HashList(IList<T> items) : base(items)
        {

        }
        public int CompareTo(HashList<T> item)
        {
            if (this.GetHashCode() == item.GetHashCode())
            {
                if (this.Count == item.Count)
                {
                    for (int i = 0; i < this.Count; i++)
                    {
                        if (!this[i].Equals(item[i]))
                        {
                            return this[i].CompareTo(item[i]);
                        }
                    }
                    return 0;
                }
                else
                {
                    return this.Count.CompareTo(item.Count);
                }
            }
            else
            {
                return this.GetHashCode().CompareTo(item.GetHashCode());
            }
        }
        public override bool Equals(object obj)
        {
            var p = obj as HashList<T>;
            if (p != null)
            {
                if (this.Count == p.Count)
                {
                    return this.Zip(p).All((v) => v.Key.Equals(v.Value));
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            var p = 0;
            foreach (var v in this)
            {
                p = p ^ v.GetHashCode();
            }
            return p;
        }
    }
}
