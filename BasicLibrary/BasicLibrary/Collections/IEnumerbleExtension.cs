﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Collections
{
    public class TakeResult<T>
    {
        public TakeResult(IEnumerable<T> a,IEnumerable<T> b)
        {
            this.Head = a;
            this.Tail = b;
        }
        public IEnumerable<T> Head { get; private set; }
        public IEnumerable<T> Tail { get; private set; }
    }

    public struct PairData<K>
    {
        public bool IsOne;
        public K Key;
        public K Value;
        public PairData(K key, K value)
        {
            Key = key;
            Value = value;
            IsOne = false;
        }
        public PairData(K key)
        {
            Key = key;
            Value = default(K);
            IsOne = true;
        }
    }

    public static class IEnumerableExtension
    {
        public static KeyValuePair<T, int>[] DistinctCount<T>(this IEnumerable<T> items)
        {
            var r = new CountDictionary<T>();
            foreach (var v in items)
            {
                r[v]++;
            }
            var p = r.ToArray();
            Array.Sort(p,(a,b) => b.Value - a.Value);
            return p;
        }

        public static IEnumerable<int> SelectIndex<T>(this IEnumerable<T> items, Func<T, bool> func)
        {
            int i = 0;
            foreach (var v in items)
            {
                if (func(v)) yield return i;
                i++;
            }
        }
        public static IEnumerable<int> SelectIndex<T>(this IEnumerable<T> items, T match)
        {
            int i = 0;
            foreach (var v in items)
            {
                if (v.Equals(match)) yield return i;
                i++;
            }
        }

        public static IEnumerable<T> Duplicate<T>(this IEnumerable<T> items)
        {
            var dic = new ListDictionary<T,T>();
            items.ForEach((v) => dic[v].Add(v));
            return dic.Values.Where((v) => v.Count > 1).SelectMany((v) => v);
        }
        public static string JoinLineString<T>(this IEnumerable<T> items)
        {
            return items.JoinString(System.Environment.NewLine);            
        }
        public static string JoinString<T>(this IEnumerable<T> items)
        {
            return items.JoinString("");
        }
        public static string JoinString<T>(this IEnumerable<T> items,string renketusi)
        {
            return string.Join(renketusi, items);
        }
        public static IEnumerable<T> JoinYield<T>(this IEnumerable<T> items, T renketusi)
        {
            KeyValuePair<T, T> r;
            bool b = false;
            foreach (var v in items)
            {
                if (b)
                {
                    yield return r.Key;
                    yield return r.Value;
                }
                r = new KeyValuePair<T, T>(v, renketusi);
                b = true;
            }
            if(b) yield return r.Key;
        }

        public static string Format<T>(this IEnumerable<T> items)
        {
            return string.Format("[{0}]", string.Join(",", items));
        }
        public static IEnumerable<int> Range(int start, int end)
        {
            for (int i = start; i < end; i++)
            {
                yield return i;
            }
        }
        public static IEnumerable<KeyValuePair<T,T>> Pair<T>(this IEnumerable<T> Items)
        {
            T[] item = Items.ToArray();
            for (int i = 0; i < item.Length-1; i++)
            {
                yield return new KeyValuePair<T, T>(item[i], item[i + 1]);
            }
            /*
                while (true)
                {
                    var fst = item.FirstOrDefault();
                    var snd = item.Second();
                    item = item.Skip(1);
                    if (fst != null && snd != null)
                    {
                        yield return new KeyValuePair<T, T>(fst, snd);
                    }
                    else
                    {
                        break;
                    }
                }
            */
        }
        public static IEnumerable<PairData<T>> PairIncomplete<T>(this IEnumerable<T> Items)
        {
            var r = new Stack<T>(Items.Reverse());
            while (r.Count > 0)
            {
                var fst = r.Pop();
                if (r.Count == 0)
                {
                    yield return new PairData<T>(fst);
                }
                else
                {
                    var snd = r.Pop();
                    yield return new PairData<T>(fst,snd);
                }
            }
        }
        public static IEnumerable<KeyValuePair<A, B>> NullPermitZip<A,B>(this IEnumerable<A> a, IEnumerable<B> b) where A : class where B : class
        {
            var enu1 = a.GetEnumerator();
            var enu2 = b.GetEnumerator();
            bool x = false,y = false;
            while ((x = enu1.MoveNext()) || (y = enu2.MoveNext()))
            {
                if (x && y)
                {
                    yield return new KeyValuePair<A, B>(enu1.Current, enu2.Current);
                }
                else if (x && !y)
                {
                    yield return new KeyValuePair<A, B>(enu1.Current, null);
                }
                else if (!x && y)
                {
                    yield return new KeyValuePair<A, B>(null, enu2.Current);
                }
                else
                {
                    throw new Exception();
                }
            }
        }
        public static IEnumerable<KeyValuePair<T,int>> RunLengthEncoding<T>(this IEnumerable<T> items) where T: IComparable
        {
            T tmp = items.FirstOrDefault();
            int len = 0;
            foreach (var f in items)
            {
                if (tmp.CompareTo(f) == 0)
                {
                    len++;
                }
                else
                {
                    yield return new KeyValuePair<T, int>(tmp, len);
                    tmp = f;
                    len = 1;
                }
            }
            if (len != 0) yield return new KeyValuePair<T, int>(tmp, len);
        }
        public static T Second<T>(this IEnumerable<T> Items)
        {
            var fst = Items.Takes(1);
            if (fst.Head.Count() == 0) return default(T);
            var snd = fst.Tail.FirstOrDefault();
            return snd;
        }

        public static Result Fold<Result>(this IEnumerable<Result> Items,Func<Result,Result,Result> func,Result first)
        {
            var r = first;
            foreach (var v in Items)
            {
                r = func(r,v);
            }
            return r;
        }
        public static Result Fold<Result>(this IEnumerable<Result> Items, Func<Result, Result, Result> func)
        {
            Result r = Items.First();
            var pItems = Items.Skip(1);
            foreach (var v in pItems)
            {
                r = func(r, v); 
            }
            return r;
        }

        public static IEnumerable<int> SubSum<T>(this IEnumerable<T> Items, Func<T, int> pred)
        {
            int sum = 0;
            foreach (var v in Items)
            {
                yield return sum;
                sum += pred(v);
            }
            yield return sum;
        }
        public static TakeResult<T> MaxTake<T>(this IEnumerable<T> Items, int max)
        {
            List<T> list = new List<T>();
            int n = 0;
            foreach (var v in Items)
            {
                if (max-n == 0) break;
                list.Add(v);
                n++;
            }
            return new TakeResult<T>(list, Items.Skip(n));
        }
        public static TakeResult<T> Takes<T>(this IEnumerable<T> Items, int n)
        {            
            return new TakeResult<T>(Items.Take(n), Items.Skip(n));
        }

        public static void CoundDownForeach<T>(this IEnumerable<T> Items, int StartNumber, Func<T, int, int> pred)
        {
            var n = StartNumber;
            foreach (var v in Items)
            {
                n = pred(v, n);
                if (n == 0) break;
            }
        }
        public static IEnumerable<A> Append<A>(this IEnumerable<A> a, A b)
        {
            foreach (var p in a)
            {
                yield return p;
            }
            yield return b;
        }
        public static IEnumerable<T> ToReverse<T>(this IEnumerable<T> item)
        {
            Stack<T> r = new Stack<T>();
            foreach (var p in item)
            {
                r.Push(p);
            }
            foreach (var p in r)
            {
                yield return p;
            }
        }


        public static T Min2<T>(this IEnumerable<T> Items, Func<T, int> pred)
        {
            var n = Int32.MaxValue;
            T r = default(T);
            foreach (var v in Items)
            {
                var k = pred(v);
                if (k < n)
                {
                    n = k;
                    r = v;
                }
            }
            return r;
        }
        public static T Max2<T>(this IEnumerable<T> Items, Func<T, int> pred)
        {
            var n = Int32.MinValue;
            T r = default(T);
            foreach (var v in Items)
            {
                var k = pred(v);
                if (k > n)
                {
                    n = k;
                    r = v;
                }
            }
            return r;
        }
        public static int MinValueIndex<T>(this IEnumerable<T> Items, Func<T, int> pred)
        {
            var n = Int32.MaxValue;
            int index = 0;
            int i=0;
            T r = default(T);
            foreach (var v in Items)
            {
                var k = pred(v);
                if (k < n)
                {
                    n = k;
                    r = v;
                    index = i;
                }
                i++;
            }
            return index;
        }

        public static IEnumerable<R> SelectIndex<T,R>(this IEnumerable<T> Items, Func<int,T,R> pred)
        {
            int i = 0;
            foreach (var p in Items)
            {
                yield return pred(i++,p);
            }
        }
        public static IEnumerable<int> WhereIndex<T>(this IEnumerable<T> Items, Func<T, bool> pred)
        {
            int n = 0;
            foreach (var v in Items)
            {
                if (pred(v)) yield return n;
                n++;
            }
        }

        public static int FindIndex<T>(this IEnumerable<T> items, Func<T, bool> pred)
        {
            int n = 0;
            foreach (var v in items)
            {
                if (pred(v)) return n;
                n++;
            }
            return -1;
        }
        public static void ForEach<T>(this IEnumerable<T> arr, Action<T> pred)
        {
            foreach (var v in arr) { pred(v); }

        }
        public static IEnumerable<T> ForEach2<T>(this IEnumerable<T> arr, Action<T> pred)
        {
            foreach (var v in arr) { pred(v); yield return v; }

        }
        public static IEnumerable<T> Skips<T>(this IEnumerable<T> items, int MaxSkipNumber, Action<T> pred)
        {
            var len = items.Count();
            if (len < MaxSkipNumber) MaxSkipNumber = len;
            items.Take(MaxSkipNumber).ToList().ForEach((v) => pred(v));
            return items.Skip(MaxSkipNumber);
        }
        public static void Do<T>(this IEnumerable<T> items)
        {
            items.Count();
        }
        public static int FoldFindIndex<T>(this IEnumerable<T> items, Func<T, int> pred, Func<int, bool> pred2)
        {
            int n = 0;
            int sum = 0;
            foreach (T v in items)
            {
                sum += pred(v);
                if (pred2(sum)) return n;
                n++;
            }
            return -1;
        }
        //2013-2-5 stack -> queue
        public static IEnumerable<KeyValuePair<A, B>> Zip<A, B>(this IEnumerable<A> itemA, IEnumerable<B> itemB)
        {
            var aIter = new Queue<A>(itemA);
            var bIter = new Queue<B>(itemB);
            while (aIter.Count != 0 && bIter.Count != 0)
            {
                yield return new KeyValuePair<A, B>(aIter.Dequeue(), bIter.Dequeue());
            }

        }
        
        public static IEnumerable<IEnumerable<A>> ReverseSelectMeny<A>(this IEnumerable<A> itemA, Func<A, bool> func)
        {
            var tmp = new List<A>();
            foreach (var v in itemA)
            {
                if (func(v) && tmp.Count > 0)
                {
                    yield return tmp;
                    tmp = new List<A>();
                }
                tmp.Add(v);
            }
            if (tmp.Count > 0) yield return tmp;

        }
    }
}
