﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Collections
{
    public static class Array2DExtension
    {
        public static bool CheckEqual2D<T>(T[,] a, T[,] b)
        {
            if (a.GetLength(0) != b.GetLength(0) || a.GetLength(1) != b.GetLength(1)) return false;
            for (int y = 0; y < a.GetLength(0); y++)
            {
                for (int x = 0; x < a.GetLength(1); x++)
                {
                    if (!a[y, x].Equals(b[y, x])) return false;
                }
            }
            return true;
        }
        public static T[,] Create<T>(int width, int height, Func<T> generator)
        {
            var r = new T[height,width];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    r[y, x] = generator();
                }
            }
            return r;
        }
    }
}
