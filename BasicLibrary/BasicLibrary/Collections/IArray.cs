﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Collections
{
    public interface IArray<T>:IReadOnlyArray<T>
    {
        T this[int i] { get; set; }        
    }
    public interface IReadOnlyArray<T>
    {
        long Length { get; }
        T this[long i] { get; }
    }

    public interface ILongArray<T> : IReadOnlyLongArray<T>
    {
        new T this[long i] { get; set; }
    }
    public interface IReadOnlyLongArray<T> : IEnumerable<T>
    {
        long Length { get; }
        T this[long i] { get; }
    }
}
