﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Collections.Specific
{
    class AddLink<Value>
    {
        private LinkedList<Value> Items { get; set; }
        public AddLink(int max)
        {
            this.MaxCount = max;
            this.Items = new LinkedList<Value>();
        }
        public int MaxCount { get; private set; }
        public int Count
        {
            get
            {
                return this.Items.Count;
            }
        }
        public event Action<Value,EventArgs> Added;
        public void Add(Value value)
        {
            this.Items.AddLast(value);
            if (this.Added != null) this.Added(value,new EventArgs());
            if (this.Items.Count > this.MaxCount) this.Items.RemoveFirst();
        }
        public IEnumerable<Value> LastTake(int n)
        {
            var r = new List<Value>();
            var p = this.Items.Last;
            while (r.Count < n)
            {
                r.Add(p.Value);
                p = p.Previous;
            }
            r.Reverse();
            if (r.Count != n) throw new Exception();
            return r;
        }
    }
}
