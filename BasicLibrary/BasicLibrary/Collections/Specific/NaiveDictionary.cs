﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Collections.Specific
{
    public class NaiveDictionary<Key,Value>
    {
        private List<KeyValuePair<Key, Value>> _Items = new List<KeyValuePair<Key, Value>>();
        public IEnumerable<KeyValuePair<Key, Value>> Items
        {
            get
            {
                return this._Items;
            }
        }
        public IEnumerable<Key> Keys
        {
            get
            {
                return this._Items.Select((v) => v.Key);
            }
        }
        public IEnumerable<Value> Values
        {
            get
            {
                return this._Items.Select((v) => v.Value);
            }
        }
        public bool TryGetValue(Key key,out Value value)
        {
            var p = this._Items.Any((v) => v.Key.Equals(key));

            if (p)
            {
                value = this._Items.First((v) => v.Key.Equals(key)).Value;
                return true;
            }
            else
            {
                value = default(Value);
                return false;
            }
        }
        public bool ContainsKey(Key key)
        {
            var n = this._Items.FindIndex((v) => v.Key.Equals(key));
            return n != -1;
        }
        public virtual Value this[Key key]
        {
            get
            {
                Value value;
                if (TryGetValue(key, out value))
                {
                    return value;
                }
                else
                {
                    throw new Exception();
                }
            }
            set
            {
                var n = this._Items.FindIndex((v) => v.Key.Equals(key));
                if (n == -1)
                {
                    this._Items.Add(new KeyValuePair<Key, Value>(key, value));
                }
                else
                {
                    this._Items[n] = new KeyValuePair<Key,Value>(key,value);
                }
            }
        }
        public override string ToString()
        {
            var p = this.Items.Select((kv) => string.Format("({0},{1})", kv.Key, kv.Value));
            return "{" + string.Join(",", p) + "}";
        }
    }
}
