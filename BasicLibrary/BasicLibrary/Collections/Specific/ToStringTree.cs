﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Collections.Specific
{
    public interface IToStringTreeNode
    {
        void AddTab(string s);        
    }
    public class ToStringTree:IToStringTreeNode
    {
        public LinkedList<IToStringTreeNode> Tree { get; set; }
        
        public string Tab { get; set; }
        public string LeftParentheses { get; set; }
        public string RightParentheses { get; set; }
        public ToStringTree()
        {
            this.Tab = " ";
            this.LeftParentheses = "{";
            this.RightParentheses = "}";
            this.Tree = new LinkedList<IToStringTreeNode>();
        }
        private ToStringTree LevelUpWithNonWrap()
        {
            var r = new ToStringTree();
            r.Tree.AddLast(this);
            r.AddTab(this.Tab);
            return r;
        }
        public ToStringTree LevelUp()
        {
            var r = new ToStringTree();
            r.Tree.AddLast(this.LevelUpWithNonWrap());
            r.AppendFirst(this.LeftParentheses);
            r.Append(this.RightParentheses);
            
            return r.LevelUpWithNonWrap();
        }
        public override string ToString()
        {
            return this.Tree.ToArray().JoinLineString();
        }
        /*
        public ToStringTree Wrap(string left, string right)
        {
            var r = new ToStringTree();
            r.AppendFirst(left);
            
            r.Append(right);
            return r.LevelUp();
        }
        public ToStringTree Wrap()
        {
            return this.Wrap("{", "}");
        }
        */
        public void AppendFirst(string s)
        {
            var r = new ToStringTreeNode(s);
            this.Tree.AddFirst(r);
        }
        public void Append(string s)
        {
            var r = new ToStringTreeNode(s);
            this.Tree.AddLast(r);
        }
        public void AddTab(string s)
        {
            foreach (var v in this.Tree)
            {
                v.AddTab(s);
            }
        }
        public static ToStringTree Merge(ToStringTree tree1, ToStringTree tree2)
        {
            var r = new ToStringTree();
            r.Tree.AddFirst(tree1);
            r.Tree.AddLast(tree2);
            return r;
        }
    }
    public class ToStringTreeNode:IToStringTreeNode
    {
        public string Text { get; set; }
        public ToStringTreeNode(string s)
        {
            this.Text = s;
        }
        public void AddTab(string s)
        {
            this.Text = s + this.Text;
        }
        public override string ToString()
        {
            return this.Text;
        }
    }
}
