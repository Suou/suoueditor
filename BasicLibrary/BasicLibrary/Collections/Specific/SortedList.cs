﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Collections.Specific
{

    public static class SortedList
    {
        public static int BinarySearch<T>(this IList<T> array, Func<T, int> func)
        {
            var left = 0;
            var right = array.Count - 1;
            var now = (left + right) / 2;
            return array.BinarySearchLeft(func, left, now, right, -1);
        }
        public static Range BinarySearchRange<T>(this IList<T> array, Func<T, int> func)
        {
            var left = 0;
            var right = array.Count - 1;
            var now = (left + right) / 2;
            var leftrst = array.BinarySearchLeft(func, left, now, right, -1);
            var rightrst = array.BinarySearchRight(func, left, now, right, -1);
            return new Range(leftrst,rightrst);
        }
        public static T[] BinarySearchFind<T>(this IList<T> array, Func<T, int> func)
        {
            var range = BinarySearchRange(array, func);
            var r = new T[range.Right - range.Left];
            for (int i = range.Left; i < range.Right; i++)
            {
                r[i - range.Left] = array[i];
            }
            return r;
        }
        private static int BinarySearchLeft<T>(this IList<T> array, Func<T, int> func, int leftx, int now, int rightx, int result)
        {
            if (leftx > rightx) return result;
            if (now < leftx || now > rightx) throw new NotImplementedException();
            var rst = func(array[now]);
            if (rst == 0)
            {
                return array.BinarySearchLeft(func, leftx, (leftx + now - 1) / 2, now - 1, now);
            }
            else if (rst == 1)
            {
                return array.BinarySearchLeft(func, leftx, (leftx + now - 1) / 2, now - 1, result);
            }
            else if (rst == -1)
            {
                return array.BinarySearchLeft(func, now + 1, (now + 1 + rightx) / 2, rightx, result);
            }
            else
            {
                throw new Exception();
            }

        }
        private static int BinarySearchRight<T>(this IList<T> array, Func<T, int> func, int leftx, int now, int rightx, int result)
        {
            if (leftx > rightx) return result;
            if (now < leftx || now > rightx) throw new NotImplementedException();
            var rst = func(array[now]);
            if (rst == 0)
            {
                return array.BinarySearchRight(func, now + 1, (now + 1 + rightx) / 2, rightx, now);
            }
            else if (rst == 1)
            {
                return array.BinarySearchRight(func, leftx, (leftx + now - 1) / 2, now - 1, result);
            }
            else if (rst == -1)
            {
                return array.BinarySearchRight(func, now + 1, (now + 1 + rightx) / 2, rightx, result);
            }
            else
            {
                throw new Exception();
            }

        }

    }
}
