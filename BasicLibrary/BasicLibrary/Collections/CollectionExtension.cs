﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Collections
{

    public static class ArrayExtensions
    {
        public static int[][] RadixSort(this int[][] items)
        {
            return items.RadixSortIndex().Select((v) => items[v]).ToArray();
        }
        public static int[] RadixSortIndex(this int[][] items)
        {
            if (items.Length == 0) return new int[] { };
            var max = items.Max((v) => v.Length);
            int[] idx = Enumerable.Range(0, items.Length).ToArray();
            for (int i = max - 1; i >= 0; i--)
            {
                idx = RadixSortIndex(items, idx, i);
            }
            return idx;
        }
        private static int[] RadixSortIndex(int[][] items, int[] idx, int keta)
        {
            return idx.BucketSort((i) => items[i][keta]).ToArray();
        }
        public static T[] BucketSort<T>(this T[] items, Func<T, int> func)
        {
            var dst = BucketSortIndex(items, func);
            return dst.Select((v) => items[v]).ToArray();
        }
        public static int[] BucketSort<T>(this int[] items)
        {
            return items.BucketSort((a) => items[a]);
        }
        
        
        [Obsolete]
        public static int[] BucketSortIndex2<T>(this T[] items, Func<T, int> func)
        {
            var max = items.Select((v) => func(v)).Max();
            var min = items.Select((v) => func(v)).Min();
            if (min < 0) throw new Exception();

            var range = max + 1;
            var len = items.Count();
            var src = items.Select((v) => func(v)).ToArray();
            var dst = new int[len];

            /** 値ごとの出現回数 */
            int[] count = new int[range];
            /** ソート後配列における値ごとの開始位置 */
            int[] offset = new int[range];
            /** ループ制御用 */
            int i;

            /* 出現回数を数える */
            for (i = 0; i < len; i++)
                count[src[i]]++;
            /* 開始位置計算 */
            offset[0] = 0;
            for (i = 1; i < range; i++)
                offset[i] = offset[i - 1] + count[i - 1];
            /* ソート処理 */
            for (i = 0; i < len; i++)
            {
                int target = src[i];
                dst[offset[target]] = target;
                offset[target]++;
            }
            return offset;
        }
        
        public static int[] BucketSortIndex<T>(this T[] items, Func<T, int> func)
        {
            var max = items.Select((v) => func(v)).Max();
            var min = items.Select((v) => func(v)).Min();

            var r = new List<int>[max - min + 1];
            for (int i = 0; i < r.Length; i++)
            {
                r[i] = new List<int>();
            }
            for (int i = 0; i < items.Length; i++)
            {
                r[func(items[i]) - min].Add(i);
            }
            return r.SelectMany((v) => v).ToArray();


        }
        public static T[][] Create<T>(int width, int height)
        {
            var r = new T[height][];
            for (int i = 0; i < height; i++) { r[i] = new T[width]; }
            return r;
        }
        public static T[][] To2D<T>(this T[] items, int width)
        {
            var h = items.Length / width;
            var r = Create<T>(width, h);
            if (items.Length != h * width) throw new Exception();
            //int i=0;
            for (int y = 0; y < h; y++)
            {
                int py = y * width;
                for (int x = 0; x < width; x++)
                {
                    r[y][x] = items[py + x];
                }
            }
            return r;
        }


        public static T[] CopyEnable<T>(this T[] items, int index, int len)
        {
            var mlen = index + len < items.Length ? len : items.Length - index;
            return items.Copy(index, mlen);
        }
        public static T[] Copy<T>(this T[] items,int index,int length)
        {
            T[] r = new T[length];
            for (int i = 0; i < length; i++) r[i] = items[i + index];
            return r;
        }
        public static T[] Copy<T>(this T[] items, int index)
        {
            var len = items.Length - index;
            T[] r = new T[len];
            for (int i = 0; i < len; i++) r[i] = items[i + index];
            return r;
        }
        public static IEnumerable<IEnumerable<T>> GetSuffixes<T>(this IEnumerable<T> item)
        {
            return ArrayExtensions.CreateForInt(0, item.Count(), 1).Select((v) => item.Skip(v));
        }
        public static IEnumerable<int> CreateForInt(int start, int end, int shift)
        {
            for (int i = start; i < end; i += shift)
            {
                yield return i;
            }
        }
        public static bool IsInclude(this IEnumerable<int> arr, IEnumerable<int> parr)
        {
            foreach (int n in parr)
            {
                if (arr.Any((m) => m == n)) return true;
            }
            return false;
        }
        public static bool IsEqual<T>(T[] arr, T[] parr)
        {
            if (arr.Length != parr.Length) return false;
            for (int i = 0; i < arr.Length; i++)
            {
                if (!arr[i].Equals(parr[i])) return false;
            }
            return true;
        }


        /// <summary>
        /// arrの各要素からn引く
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="n"></param>
        /// <returns></returns>
        public static int[] EachMinus(this int[] arr, int n)
        {
            int[] iarr = new int[arr.Length];
            for (int i = 0; i < arr.Length; i++)
            {
                iarr[i] = n - arr[i];
            }
            return iarr;
        }

    }
    public static class QueueExtension
    {
        public static IEnumerable<T> DequeueIterator<T>(this Queue<T> items)
        {
            while (items.Count != 0)
            {
                yield return items.Dequeue();
            }
        }
        public static IEnumerable<T> MaxDequeue<T>(this Queue<T> Items, int max)
        {
            List<T> list = new List<T>();

            if (Items.Count == 0 || max == 0) return list;
            int n = 0;
            foreach (var v in Items.DequeueIterator())
            {
                list.Add(v);
                n++;
                if (max - n == 0) break;
            }
            return list;
        }
        public static IEnumerable<T> PopTake<T>(this Queue<T> Items, Func<T, bool> pred)
        {
            List<T> r = new List<T>();
            while (Items.Count != 0)
            {
                var v = Items.Peek();
                if (!pred(v)) break;
                r.Add(Items.Dequeue());
            }
            return r;
        }
        /*
        public static IEnumerable<IEnumerable<T>> PopTakeIterator<T>(this Queue<T> Items, ITwoDpred<T> predClass)
        {
            while (Items.Count != 0)
            {
                predClass.Clear();
                var v = Items.PopTake(predClass.Pred);
                yield return v;
            }
        }
        */
        public static IEnumerable<T> FirstPopTake<T>(this Queue<T> Items, Func<T, bool> pred)
        {
            while (Items.Count != 0 && !pred(Items.Peek())) { Items.Dequeue(); }
            return Items.PopTake(pred);
        }
        /*
        public static IEnumerable<T> PopTake<T>(this Queue<T> Items, Func<T, bool> pred)
        {
            List<T> r = new List<T>();
            while (Items.Count != 0)
            {
                var v = Items.Peek();
                if (!pred(v)) break;
                r.Add(Items.Dequeue());
            }
            return r;
        }
        */
        public static IEnumerable<IEnumerable<T>> PopSelect<T>(this Queue<T> Items, Func<T, bool> pred)
        {
            List<IEnumerable<T>> r = new List<IEnumerable<T>>();
            while (Items.Count != 0)
            {
                r.Add(Items.FirstPopTake(pred));
            }
            return r;
        }
    }

    public static class ListExtension
    {
        public static T Random<T>(this IList<T> item)
        {
            return item[RandExtension.RandInt(item.Count)];
        }
        public static void ForEachIndex<T>(this IList<T> item, Action<T,int> t)
        {
            int i = 0;
            item.ForEach((v) => t(v,i++));
        }
        public static T[] Copy<T>(this IList<T> items, int index, int length)
        {
            T[] r = new T[length];
            for (int i = 0; i < length; i++) r[i] = items[i + index];
            return r;
        }
        public static bool AllContain<T>(this IList<T> items, IList<T> subItems)
        {
            HashSet<T> dic = new HashSet<T>();
            items.ForEach((v) => dic.Add(v));
            return subItems.All((v) => items.Contains(v));
        }
        public static IList<T> NewSort<T>(this IList<T> items,Comparison<T> comparison)
        {
            var p = items.ToList();
            p.Sort(comparison);
            return p;
        }
    }
    public static class StackExtension
    {
        public static void WhilePop<T>(this Stack<T> items, Func<T,bool> func)
        {
            while (items.Count > 0)
            {
                if (func(items.Peek()))
                {
                    items.Pop();
                }
                else
                {
                    break;
                }
            }
        }
    }
    public static class LinkedListExtension
    {
        public static LinkedListNode<Key> Merge<Key>(this LinkedList<Key> item, LinkedListNode<Key> node, Key newValue)
        {
            var pairFirstNode = node;
            var preNode = pairFirstNode.Previous;
            var pairLastNode = pairFirstNode.Next;
            //var nextNode = pairLastNode.Next;
            item.Remove(pairFirstNode);
            item.Remove(pairLastNode);
            var newItem = preNode == null ? item.AddFirst(newValue) : item.AddAfter(preNode, newValue);
            /*
            var pre = new KeyValuePair<Key, Key>(preNode.Value, pairFirstNode.Value);
            var suf = new KeyValuePair<Key, Key>(pairLastNode.Value, nextNode.Value);
            */
            return newItem;
        }
        public static IEnumerable<LinkedListNode<Key>> ToNodes<Key>(this LinkedList<Key> item)
        {
            var p = item.First;
            while (p != null)
            {
                yield return p;
                p = p.Next;
            }
        }
    }

    public static class Char2DExtension
    {
        public static string ToLineString(this char[,] s)
        {
            var height = s.GetLength(0);
            var width = s.GetLength(1);
            var r = new StringBuilder[height];
            for (int y = 0; y < height; y++)
            {
                r[y] = new StringBuilder();
                for (int x = 0; x < width; x++)
                {
                    r[y].Append(s[y, x]);
                }
            }
            return r.Select((v) => v.ToString()).JoinLineString();
        }
        public static void Paint(this char[,] s,Point2D p, BSize size,char c)
        {
            var w = p.X + size.Width;
            var h = p.Y + size.Height;
            for (int y = p.Y; y < h; y++)
            {
                for (int x = p.X; x < w; x++)
                {
                    s[y, x] = c;
                }
            }
        }
        public static void Paint(this int[][] s, Point2D p, BSize size, int c)
        {
            var w = p.X + size.Width;
            var h = p.Y + size.Height;
            for (int y = p.Y; y < h; y++)
            {
                for (int x = p.X; x < w; x++)
                {
                    s[y][x] = c;
                }
            }
        }
        public static Key[][] Create<Key>(int width,int height)
        {
            var r = new Key[height][];
            for (int y = 0; y < height; y++)
            {
                r[y] = new Key[width];
            }
            return r;
        }
    }
    
}
