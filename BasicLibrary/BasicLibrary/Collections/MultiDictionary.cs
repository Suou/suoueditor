﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Collections
{
    public class ListDictionary<KEY, VALUE> : Dictionary<KEY, List<VALUE>>
    {

        public new List<VALUE> this[KEY key]
        {
            get
            {
                if (!this.ContainsKey(key))
                {
                    base[key] = new List<VALUE>();
                }

                return base[key];
            }
        }
        public void Add(KEY key, VALUE value)
        {
            if (this.ContainsKey(key))
            {
                base[key].Add(value);
            }
            else
            {
                base[key] = new List<VALUE>() { value };
            }
        }
        public void Remove(KEY key, VALUE value)
        {
            base[key].Remove(value);
            if (base[key].Count == 0) base.Remove(key);
        }
        public IEnumerable<VALUE> OneValues
        {
            get
            {
                return this.Values.Select((v) => v.First());
            }
        }
    }
    public class SetDictionary<Key, Value> : Dictionary<Key, HashSet<Value>>
    {

        public new HashSet<Value> this[Key key]
        {
            get
            {
                if (!this.ContainsKey(key))
                {
                    base[key] = new HashSet<Value>();
                }

                return base[key];
            }
        }
        public bool Remove(Key key, Value value)
        {
            var b = this[key].Remove(value);
            if (this[key].Count == 0) this.Remove(key);
            return b;
        }
        public IEnumerable<KeyValuePair<Key, Value>> ElementItems
        {
            get
            {
                foreach (var kv in this)
                {
                    foreach (var value in kv.Value)
                    {
                        yield return new KeyValuePair<Key, Value>(kv.Key, value);
                    }
                }
            }
        }
    }
}
