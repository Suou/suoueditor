﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.IO;
namespace BSystem
{
    public static class GeneralExtensions
    {
        public static TR Pipe<T, TR>(this T obj, Func<T, TR> func)
        {
            return func(obj);
        }
        public static void VoidPipe<T>(this T obj, Action<T> func)
        {
            func(obj);
        }
        public static void VoidNullPipe<T>(this T obj, Action<T> func)
        {
            if(func != null)func(obj);
            
        }
    }


    public struct LeftRightPair<ITEM>
    {
        public readonly ITEM Left;
        public readonly ITEM Right;
        public LeftRightPair(ITEM left,ITEM right){
            this.Left = left;
            this.Right = right;
        }
    }

    

    public interface ITwoDpred<T>
    {
        void Clear();
        Func<T, bool> Pred { get; }
    }


    
}
