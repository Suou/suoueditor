﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Text;
namespace BSystem
{
    public class GDICharacter
    {
        public static Dictionary<char, int> Dic = new Dictionary<char, int>();
        public static bool IsCreated = false;
        /*
        public static void Load(string path)
        {
            var lines = IOLibrary.Load(path).LineSprit();
            for (int i = 0; i < lines.Length; i++)
            {
                Dic[(char)i] = int.Parse(lines[i]);
            }
            IsCreated = true;
        }
        */
        public static void LoadText(string text)
        {
            var lines = text.LineSprit();
            for (int i = 0; i < lines.Length; i++)
            {
                Dic[(char)i] = int.Parse(lines[i]);
            }
            IsCreated = true;
        }
        public static int Width(char c){
            return Dic[c];
        }
    }

}
