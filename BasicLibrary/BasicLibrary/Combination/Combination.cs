﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Combination
{
    /*
    class Combination
    {
    }

    public class IntCombination
    {
        public int LowerBound { get; set; }
        public int UpperBound { get; set; }
        public IntCombinationElement[] Items { get; set; }
        public IntCombinationElement FirstItem { get { return this.Items.First(); } }
        public IntCombination(int lowerBound,int upperBound,int length)
        {
            this.LowerBound = lowerBound;
            this.UpperBound = upperBound;
            
        }
        public int[] Value
        {
            get
            {
                var r = this.Items.Select((v)=>v.Value).ToArray();
                return r;
            }
        }
        public IEnumerable<int[]> Enumerate()
        {
            while (this.FirstItem.Plus())
            {
                yield return this.Value;
            }
        }
    }
    public class IntCombinationElement
    {
        public int LowerBound { get; set; }
        public int UpperBound { get; set; }
        public int Value { get; set; }
        public IntCombinationElement NextElement { get; set; }
        public bool IsLast { get { return this.NextElement == null; } }
        public IntCombinationElement(int lowerBound,int upperBound)
        {
            this.LowerBound = lowerBound;
            this.UpperBound = upperBound;
            this.Value = this.LowerBound;
        }
        public bool Plus()
        {
            this.Value++;
            if (this.UpperBound > this.Value)
            {
                if (!this.IsLast)
                {
                    this.Value = this.LowerBound;
                    return this.NextElement.Plus();
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
    }
    */
}
