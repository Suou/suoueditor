﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Combination
{
    public static class CombinationMethod
    {
        private static KeyValuePair<T, IList<T>> Pull<T>(this IList<T> items,int i)
        {
            var key = items[i];
            var copy = items.ToList();
            copy.RemoveAt(i);
            return new KeyValuePair<T, IList<T>>(key, copy);
        }
        private static KeyValuePair<T, IList<T>> PullAndCutOff<T>(this IList<T> items, int i)
        {
            var key = items[i];
            var copy = items.ToList();
            copy.RemoveRange(0,i);
            return new KeyValuePair<T, IList<T>>(key, copy);
        }
        
        private static IEnumerable<KeyValuePair<T,IList<T>>> PullCombination<T>(this IList<T> items)
        {
            for (int i = 0; i < items.Count; i++)
            {
                yield return items.Pull(i);
            }
        }
        private static IEnumerable<KeyValuePair<T, IList<T>>> PullCutOffCombination<T>(this IList<T> items,int valueLength)
        {
            for (int i = 0; i < items.Count-valueLength; i++)
            {
                yield return items.Pull(i);
            }
        }
        public static IEnumerable<IList<T>> Permutation<T>(this IList<T> items,int count)
        {
            if (count == 0)
            {
                yield return new List<T>();
            }
            else
            {
                var subpairs = items.PullCombination();
                foreach (var v in subpairs)
                {
                    var subItems = v.Value.Permutation(count - 1);
                    foreach (var w in subItems)
                    {
                        w.Add(v.Key);
                        yield return w;
                    }
                }
            }
        }
        public static IEnumerable<IList<T>> _Combination<T>(this IList<T> items, int count)
        {
            if (count == 0)
            {
                yield return new List<T>();
            }
            else
            {
                var subpairs = items.PullCutOffCombination(count-1);
                foreach (var v in subpairs)
                {
                    var subItems = v.Value.Permutation(count - 1);
                    foreach (var w in subItems)
                    {
                        w.Add(v.Key);
                        yield return w;
                    }
                }
            }
        }
        public static IEnumerable<IList<T>> Combination<T>(this IList<T> items, int count)
        {
            return _Combination(items, count).Select((v)=>(IList<T>)v.Reverse().ToList());   
        }
        public static IEnumerable<IList<T>> Combination<T>(this IList<T> items)
        {
            return _Combination(items, items.Count).Select((v) => (IList<T>)v.Reverse().ToList());
        }
        public static IEnumerable<IList<T>> RepeatedCombination<T>(this IList<T> items, int length)
        {
            if (length == 1)
            {
                foreach (var v in items)
                {
                    yield return new List<T>() { v};
                }
            }
            else if (length > 1)
            {
                var p = RepeatedCombination<T>(items, length - 1);
                foreach (var v in p)
                {
                    foreach (var w in items)
                    {
                        var r = v.ToList();
                        r.Add(w);
                        yield return r;
                    }
                }
            }
            else
            {
                throw new Exception();
            }
        }
        public static IEnumerable<IList<T>> RepeatedCombination<T>(this IList<T> items)
        {
            return items.RepeatedCombination(items.Count);
        }
    }
}
