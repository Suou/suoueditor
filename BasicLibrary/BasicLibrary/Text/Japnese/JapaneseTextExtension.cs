﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using BSystem.Text.Encoding;
namespace BSystem.Text.Japanese
{
    public static class JapaneseTextExtension
    {
        private static string ToJoinString(this IEnumerable<char> v)
        {
            StringBuilder sb = new StringBuilder("");
            v.ToList().ForEach((c) => sb.Append(c));
            return sb.ToString();

        }
        static JapaneseTextExtension()
        {
            Create();
        }

        public static bool IsJapaneseOrAlphabet(this char c)
        {
            return c.IsJapanese() || c.IsUpperLatin() || c.IsLowerLatin();
        }
        public static bool IsSpace(this char c)
        {
            return c == ' ' || c == '　';
        }
        public static IList<string> GetJapanese(this string s)
        {
            Queue<char> queue = new Queue<char>(s.ToCharArray());
            var r = queue.PopSelect((c) => c.IsJapanese());
            var rr = r.Select((v) => v.ToJoinString());
            return rr.ToList();
        }
        public static bool IsHiragana(this char c)
        {
            //「ぁ」～「より」までと、「ー」「ダブルハイフン」をひらがなとする
            return ('\u3041' <= c && c <= '\u309F')
                || c == '\u30FC' || c == '\u30A0';
        }
        public static bool IsFullwidthKatakana(this char c)
        {
            //「ダブルハイフン」から「コト」までと、カタカナフリガナ拡張と、
            //濁点と半濁点を全角カタカナとする
            //中点と長音記号も含む
            return ('\u30A0' <= c && c <= '\u30FF')
                || ('\u31F0' <= c && c <= '\u31FF')
                || ('\u3099' <= c && c <= '\u309C');
        }
        public static bool IsKanji(this char c)
        {
            //CJK統合漢字、CJK互換漢字、CJK統合漢字拡張Aの範囲にあるか調べる
            return ('\u4E00' <= c && c <= '\u9FCF')
                || ('\uF900' <= c && c <= '\uFAFF')
                || ('\u3400' <= c && c <= '\u4DBF');
        }
        public static bool IsJapanese(this char c)
        {
            return c.IsHiragana() || c.IsFullwidthKatakana() || c.IsKanji() || c.IsJapaneseKigou();
        }
        public static bool IsJapaneseKigou(this char c)
        {
            return ('\u3000' <= c && c <= '\u3002') || ('\uFF01' == c) || (c == '\uFF1F');
        }
        public static string[] SentenceSplit(this string s)
        {
            return s.Split(new char[] { '。' });
        }

        public static int GetKakusu(this char c)
        {
            var n = (int)KanjiKakusu[(ushort)c];
            return n == 0 ? -1 : n;
        }
        private static byte[] KanjiKakusu { get; set; }
        private static void Create()
        {

            var fw = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
                   @"BSystem.Resources.kanji.table");
            byte[] by = new byte[ushort.MaxValue];
            int t = 0;
            using (var r = fw)
            {
                int n = 0;
                while ((n = r.ReadByte()) != -1)
                {
                    by[t++] = (byte)n;
                }
            }
            KanjiKakusu = by;

        }
    }
}
