﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace BSystem.Text.Encoding
{

    public class EncodeFileConverter
    {
        public static void Write(Stream st, Dictionary<char, byte[]> dic)
        {
            var kai = new byte[] { 0x0D, 0x0A };
            for (int i = 0; i <= char.MaxValue; i++)
            {
                var bytes = dic[(char)i];
                st.Write(bytes, 0, bytes.Length);
                st.Write(kai, 0, kai.Length);
            }
        }
        public static Dictionary<char, byte[]> Read(BinaryReader sr)
        {
            Dictionary<char, byte[]> dic = new Dictionary<char, byte[]>();
            var convbyte = new byte[4];
            var k = 0;
            ushort u = 0;
            var len = sr.BaseStream.Length;
            for (int i = 0; i < len; i++)
            {
                convbyte[k] = sr.ReadByte();
                if (k >= 1 && convbyte[k - 1] == 0x0D && convbyte[k] == 0x0A)
                {
                    //if(k==3)convbyte.WriteLine();
                    var p = new byte[k - 1];
                    Array.Copy(convbyte, p, p.Length);
                    dic[(char)u] = p;
                    u++;
                    k = 0;
                    convbyte = new byte[4];
                }
                else
                {
                    k++;
                }
            }
            return dic;

        }
    }
    public class ShiftJisEncode:System.Text.Encoding
    {
        public static ShiftJisEncode GetEncode()
        {
            return new ShiftJisEncode();
        }

        private static Dictionary<byte, char> HankakuDic = new Dictionary<byte, char>();
        private static Dictionary<ushort, char> ZenkakuDic = new Dictionary<ushort, char>();
        private static Dictionary<char, byte[]> ReverseDic = new Dictionary<char, byte[]>();
        private static bool IsLoaded = false;
        public ShiftJisEncode()
        {
            if (!IsLoaded) Create();
        }
        private static void Create()
        {
            var fw = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(
                   @"BSystem.Resources.shiftjis.table");
            using (fw)
            {
                var p = new BinaryReader(fw, System.Text.Encoding.Unicode);
                var enc = EncodeFileConverter.Read(p);
                Create(enc);
            }            
        }
        private static void Create(Dictionary<char, byte[]> rev)
        {
            ReverseDic = rev;
            for (int i = 0; i <= char.MaxValue; i++)
            {
                var c = (char)i;
                var bytes = rev[c];
                if (bytes.Length == 1)
                {
                    if (!HankakuDic.ContainsKey(bytes[0])) HankakuDic[bytes[0]] = c;
                }
                else if (bytes.Length == 2)
                {
                    ushort u = BitConverter.ToUInt16(bytes, 0);
                    ZenkakuDic[u] = c;
                }
                else
                {
                    throw new Exception();
                }
            }
            ZenkakuDic[BitConverter.ToChar(new byte[]{13,10},0)] = System.Environment.NewLine[0];
            IsLoaded = true;
        }
        public List<string> Log = new List<string>();
        public byte stock = 0;
        public bool IsStock = false;

        private char Translate(byte a, byte b)
        {
            ushort u = (ushort)(b*(byte.MaxValue+1)+a);
            if (ZenkakuDic.ContainsKey(u))
            {
                var p = ZenkakuDic[u];
                return ZenkakuDic[u];
                //Log.Add(code[i] + "/" + code[i + 1] + "/" + (int)ZenkakuDic[u]);
            }
            else
            {
                //ushort eu = BitConverter.ToUInt16(new byte[] { a, b }, 0);
                throw new Exception(a + "/" + b+"/"+u);
            }
        }
        public string Translate(byte[] code)
        {
            StringBuilder sb = new StringBuilder();
            int i = 0;
            if (IsStock)
            {
                IsStock = false;
                sb.Append(this.Translate(stock, code[0]));
                i = 1;
            }

            while (i < code.Length)
            {
                if (HankakuDic.ContainsKey(code[i]))
                {
                    sb.Append(HankakuDic[code[i]]);
                    Log.Add(code[i] + "/" + (int)HankakuDic[code[i]]+"/"+ HankakuDic[code[i]].ToString());
                    i++;
                }
                else if (i < code.Length - 1)
                {
                    sb.Append(this.Translate(code[i], code[i + 1]));
                    /*
                    ushort u = BitConverter.ToUInt16(code, i);
                    
                    if (ZenkakuDic.ContainsKey(u))
                    {
                        sb.Append(ZenkakuDic[u]);
                        Log.Add(code[i]+"/"+code[i+1] + "/" + (int)ZenkakuDic[u]);
                    }
                    else
                    {
                        throw new Exception(code[i]+"/"+code[i+1]);
                    }
                    */
                    i += 2;
                    
                }
                else
                {
                    stock = code[i];
                    IsStock = true;
                    //throw new Exception(code[i].ToString()+"/"+i);
                    i++;
                }
            }            
            return sb.ToString();
        }
        public byte[] ToBytes(char c)
        {
            return ReverseDic[c];
        }

        public override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
        {
            char[] rs = chars.SubItem<char>(charIndex, charCount);
            int k = 0;
            for (int i = 0; i < rs.Length; i++)
            {
                var by = this.ToBytes(rs[i]);
                for (int j = 0; j < by.Length; j++)
                {
                    bytes[byteIndex + k + j] = by[j];
                }
                k += by.Length;
            }
            return k;
        }
        public override int GetBytes(string s, int charIndex, int charCount, byte[] bytes, int byteIndex)
        {
            return this.GetBytes(s.ToCharArray(), charIndex, charCount, bytes, byteIndex);
        }
        public override byte[] GetBytes(char[] chars, int index, int count)
        {
            var k = this.GetByteCount(chars, index, count);
            var temp = new byte[k];
            this.GetBytes(chars, index, count, temp, 0);
            return temp;
        }
        public override byte[] GetBytes(char[] chars)
        {
            return this.GetBytes(chars, 0, chars.Length);
        }
        public override byte[] GetBytes(string s)
        {
            return this.GetBytes(s.ToCharArray());
        }
        public override int GetByteCount(char[] chars)
        {
            return this.GetByteCount(chars,0,chars.Length);
        }
        public override int GetByteCount(string s)
        {
            return this.GetByteCount(s.ToCharArray());
        }
        public override int GetByteCount(char[] chars, int index, int count)
        {
            var temp = new byte[chars.Length * 2];
            return this.GetBytes(chars, index, count, temp, 0);
        }
        public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
        {
            var by = bytes.SubItem<byte>(byteIndex, byteCount);
            var s = this.Translate(by);
            for (int i = 0; i < s.Length; i++)
            {
                chars[charIndex + i] = s[i];
            }
            return s.Length;
        }
        public override char[] GetChars(byte[] bytes)
        {
            return this.Translate(bytes).ToCharArray();
        }
        public override char[] GetChars(byte[] bytes, int index, int count)
        {
            var sub = bytes.SubItem<byte>(index, count);
            return this.GetChars(sub);
        }
        public override int GetCharCount(byte[] bytes)
        {
            return this.GetChars(bytes).Length;
        }
        public override int GetCharCount(byte[] bytes, int index, int count)
        {
            return this.GetCharCount(bytes.SubItem<byte>(index,count));
        }
        public override string GetString(byte[] bytes, int index, int count)
        {
            return this.Translate(bytes.SubItem<byte>(index, count));
        }
        public override int GetMaxByteCount(int charCount)
        {
            return charCount * 2;
        }
        public override int GetMaxCharCount(int byteCount)
        {
            return byteCount;
        }
        public override Decoder GetDecoder()
        {
            return base.GetDecoder();
        }
        public override Encoder GetEncoder()
        {            
            return base.GetEncoder();
        }
    }
    
    static class ArrayExtension
    {
        public static T[] SubItem<T>(this T[] s,int index,int count)
        {
            T[] rs = new T[count];
            Array.Copy(s, index, rs, 0, count);
            return rs;

        }
    }
    
    public struct EncResult
    {
        public char Character;
        public bool IsHankaku;
        public EncResult(char c, bool b)
        {
            Character = c;
            IsHankaku = b;
        }
    }
}
