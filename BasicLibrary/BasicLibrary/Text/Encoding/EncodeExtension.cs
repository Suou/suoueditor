﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Collections;
namespace BSystem.Text.Encoding
{

    public static class EncodeExtension
    {
        public static bool IsUpperLatin(this char c)
        {
            //半角英字と全角英字の大文字の時はTrue
            return ('A' <= c && c <= 'Z') || ('Ａ' <= c && c <= 'Ｚ');
        }

        public static bool IsLowerLatin(this char c)
        {
            //半角英字と全角英字の小文字の時はTrue
            return ('a' <= c && c <= 'z') || ('ａ' <= c && c <= 'ｚ');
        }


        
    }
}
