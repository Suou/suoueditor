﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BMath = BSystem.MathExtension;

namespace BSystem.Text
{
    public class StringMatch
    {
        public static StringCommonResult Match(string a, string b)
        {
            var len = System.Math.Min(a.Length, b.Length);
            for (int i = 0; i < len; i++)
            {
                if (a[i] != b[i])
                {
                    return new StringCommonResult(a, b, StringMatchResult.Mismatch, i);
                }
            }

            if (a.Length == b.Length)
            {
                return new StringCommonResult(a, b, StringMatchResult.Equal,0);
            }
            else if (a.Length < b.Length)
            {
                return new StringCommonResult(a, b, StringMatchResult.Longly, b.Length-a.Length);
            }
            else
            {
                return new StringCommonResult(a, b, StringMatchResult.Shortly, a.Length - b.Length);
            }
        }
    }
    public class StringCommonResult
    {
        public string SourceString { get; private set; }
        public string MatchString { get; private set; }
        public StringMatchResult Result { get; private set; }
        private int Parameter { get; set; }
        public int MismatchPosition{get;set;}
        public string NokoriString
        {
            get
            {
                if (this.Result == StringMatchResult.Shortly)
                {
                    return this.SourceString.Substring(this.SourceString.Length - this.Parameter);
                }
                else if (this.Result == StringMatchResult.Longly)
                {
                    return this.MatchString.Substring(this.MatchString.Length - this.Parameter);
                }
                else
                {
                    throw new Exception();
                }
            }
        }

        public StringCommonResult(string a,string b,StringMatchResult rst,int n)
        {
            this.SourceString = a;
            this.MatchString = b;
            this.Result = rst;
            this.Parameter = n;
        }
        
    }
    public enum StringMatchResult
    {
        Equal,Mismatch,Shortly,Longly
    }
}
