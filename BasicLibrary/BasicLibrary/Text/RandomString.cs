﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;

namespace BSystem.Text
{
    public static class RandomString
    {
        public static char[] IntCharacters = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        public static string RandomIntSequence(int len)
        {
            var r = Enumerable.Range(0, len).Select((i) => IntCharacters[RandExtension.RandInt(IntCharacters.Length)]).ToArray();
            return new string(r);
        }
        public static string RandomBinarySequence(int len)
        {
            
            var r = Enumerable.Range(0, len).Select((i) => IntCharacters[RandExtension.RandInt(2)]).ToArray();
            return new string(r);
        }
    }
}
