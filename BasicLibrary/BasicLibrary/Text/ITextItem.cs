﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace BSystem.Text
{

    public interface ITextItem
    {
        string Name { get; }
        string Text { get; }
    }
}
