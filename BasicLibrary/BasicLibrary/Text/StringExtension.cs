﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
namespace BSystem.Text
{
    public static class PrintExtension
    {
        public static string FormatArray1<T>(this IEnumerable<T> s)
        {
            return s.FormatArray1("{", "}", ",");
        }
        public static string FormatLineArray1<T>(this IEnumerable<T> s)
        {
            return s.FormatArray1("-------------------------" + System.Environment.NewLine, System.Environment.NewLine + "-----------------------------------", System.Environment.NewLine);
        }
        public static string FormatArray1<T>(this IEnumerable<T> s, string left, string right, string joinstring)
        {
            return left + string.Join(joinstring, s) + right;
        }
        public static string FormatArray2<T>(this IEnumerable<IEnumerable<T>> s, string left, string right, string joinstring, string joinstring2)
        {
            return s.Select((v) => v.FormatArray1(left, right, joinstring)).FormatArray1(left, right, joinstring2);
        }
        public static string FormatArray2<T>(this IEnumerable<IEnumerable<T>> s)
        {
            return s.FormatArray2("{", "}", ",", "," + System.Environment.NewLine);
        }
        public static string FormatShortArray2<T>(this IEnumerable<IEnumerable<T>> s)
        {
            return s.FormatArray2("{", "}", ",", ",");
        }
        public static string FormatWithName<T>(this IEnumerable<T> item,string name)
        {
            return string.Format("{0} = {1}", name, item.FormatArray1());
        }
        public static string FormatWithName(object item, string name)
        {
            return string.Format("{0} = {1}", name, item);
        }
    }
    public static class StringExtensions
    {
        public static uint GetBHashCode(this string s)
        {
            uint u = 0;
            uint t = 0;
            s.ToCharArray().ForEach((c) => u += ((uint)c) * ((uint)c) * (++t));
            return u;
        }
        public static int CommonPrefixLength(string a,string b)
        {
            var limit = System.Math.Min(a.Length, b.Length);
            int i = 0;
            for (i = 0; i < limit; i++)
            {
                if (a[i] != b[i]) break;
            }
            return i;
            
        }

        public static string SubstringEnable(this string s, int index, int len)
        {
            var mlen = index + len < s.Length ? len : s.Length - index;
            return s.Substring(index, mlen);
        }
        public static LeftRightPair<string> Halves(this string s, int firstLength)
        {
            var left = s.Substring(0, firstLength);
            var right = s.Substring(firstLength);
            return new LeftRightPair<string>(left, right);
        }
        public static List<string> ToParagraphList(this String s)
        {
            return s.Split(new string[] { System.Environment.NewLine }, StringSplitOptions.None).ToList();
        }
        public static string DeleteInvisibleChar(this string s)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var c in s)
            {
                if (!((0x00 <= c && c <= 0x1f) || (c == 0x7f))) sb.Append(c);
            }
            return sb.ToString();
        }
        /*
        public static void Save(this string s, string filename)
        {
            
            WriterExtensions.WriteLine(s, filename,false);
        }
        */
        public static string[] LineSprit(this String s)
        {
            string[] line = new string[] { "\r\n",System.Environment.NewLine,"\r","\n" };
            if (s == null) throw new Exception();
            return s.Split(line, StringSplitOptions.None);
        }

        public static string ToParagraphJoin(IEnumerable<string> list)
        {
            return string.Join(System.Environment.NewLine, list);
        }
        public static string ToParagraphJoin(string[] list)
        {
            return string.Join(System.Environment.NewLine, list);
        }

        public static string JoinString(this string[] s)
        {
            return string.Join(System.Environment.NewLine, s);
        }

        public static IEnumerable<string> CreateNGram(this string text,int n)
        {
            for(int i=0;i<text.Length-n;i++){
                yield return text.Substring(i, n);
            }
        }
        public static int GetEUCSize(List<string> text)
        {
            int n = 0;
            foreach (string s in text)
            {
                foreach (char c in s)
                {
                    n += c.LenB_EUC();
                    if (c == '"')
                    {
                        n += 5;//6
                    }
                    else if (c == '<')
                    {
                        n += 3;//4;
                    }
                    else if (c == '>')
                    {
                        n += 3;//4;
                    }
                    else if (c == '&')
                    {
                        n += 4;//5;
                    }
                }
            }

            n += (text.Count - 1) * 4;
            return n;
        }
        public static uint GetLineHash(this string s)
        {
            uint u = 0;
            uint t = 0;
            for (int i = 0; i < s.Length; i++)
            {
                u += ((uint)s[i]) * ((uint)s[i]) * (++t);
            }
            //s.ToList().ForEach((c) => u += ((uint)c) * ((uint)c) * (++t));
            return u;
        }
        public static long GetTextHash(this string s)
        {
            return (uint)s.LineSprit().Sum((v) => v.GetLineHash());
        }
        public static string Reverse(this string s)
        {
            char[] charArray = new char[s.Length];
            int len = s.Length - 1;
            for (int i = 0; i <= len; i++)
                charArray[i] = s[len - i];
            return new string(charArray);
        }
        public static string ToHTMLString(this string s)
        {
            var f = new StringBuilder(s);
            f.Replace("&", "&amp;");
            f.Replace(">", "&gt;");
            f.Replace("<", "&lt;");
            f.Replace("\"", "&quot;");
            return f.ToString();
        }

        public static string[] TwoSplit(this String s, string k)
        {
            var sp = s.Split(new string[] { k }, StringSplitOptions.None);
            if (sp.Length > 1)
            {
                string sk = String.Join(k, sp.Take(sp.Length - 1));
                return new string[] { sk, sp.Last() };
            }
            else
            {
                return new string[] { s };
            }
        }

        public static string CheckNewLine(this String s)
        {
            s = s.Replace("\r\n", "\r");
            s = s.Replace("\r", "\n");
            s = s.Replace("\n", System.Environment.NewLine);

            return s;
        }

        #region　LenB メソッド

        /// -----------------------------------------------------------------------------------------
        /// <summary>
        ///     半角 1 バイト、全角 2 バイトとして、指定された文字列のバイト数を返します。</summary>
        /// <param name="stTarget">
        ///     バイト数取得の対象となる文字列。</param>
        /// <returns>
        ///     半角 1 バイト、全角 2 バイトでカウントされたバイト数。</returns>
        /// -----------------------------------------------------------------------------------------
        public static int LenB(string stTarget)
        {
            return System.Text.Encoding.GetEncoding("Shift_JIS").GetByteCount(stTarget);
        }

        #endregion

    }
    public static class CharExtensions
    {
        #region　LenB メソッド

        /// -----------------------------------------------------------------------------------------
        /// <summary>
        ///     半角 1 バイト、全角 2 バイトとして、指定された文字列のバイト数を返します。</summary>
        /// <param name="stTarget">
        ///     バイト数取得の対象となる文字列。</param>
        /// <returns>
        ///     半角 1 バイト、全角 2 バイトでカウントされたバイト数。</returns>
        /// -----------------------------------------------------------------------------------------
        public static int LenB_JIS(this char c)
        {
            return System.Text.Encoding.GetEncoding("Shift_JIS").GetByteCount(c.ToString());
        }

        public static int LenB_EUC(this char c)
        {
            return System.Text.Encoding.GetEncoding("euc-jp").GetByteCount(c.ToString());
        }

        #endregion
    }

}
