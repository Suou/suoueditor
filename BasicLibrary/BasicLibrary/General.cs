﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem
{
    public struct Point2D : IComparable<Point2D>
    {
        public int X;
        public int Y;
        public Point2D(int x, int y)
        {
            X = x;
            Y = y;
        }
        public override string ToString()
        {
            return string.Format("[X = {0},Y = {1} ]", X, Y);
        }
        public int CompareTo(Point2D b)
        {
            if (this.Y < b.Y)
            {
                return -1;
            }
            else if (this.Y == b.Y)
            {
                if (this.X < b.X)
                {
                    return -1;
                }
                else if (this.X == b.X)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return 1;
            }
        }
        public static bool operator <(Point2D a, Point2D b)
        {
            if (a.Y < b.Y)
            {
                return true;
            }
            else if (a.Y == b.Y)
            {
                return a.X < b.X;
            }
            else
            {
                return false;
            }
        }
        public static bool operator >(Point2D a, Point2D b)
        {
            if (a.Y > b.Y)
            {
                return true;
            }
            else if (a.Y == b.Y)
            {
                return a.X > b.X;
            }
            else
            {
                return false;
            }
        }
        public bool IsMinusEmpty
        {
            get { return this.X == -1 && this.Y == -1; }
        }
        public static Point2D MinusEmpty
        {
            get
            {
                return new Point2D(-1, -1);
            }
        }
        public static int ManhattanDistance(Point2D a,Point2D b)
        {
            return BSystem.MathExtension.ManhattanDistance(a.X, a.Y, b.X, b.Y);
        }
    }
    public enum RectDirection
    {
        Top, Left, Right, Bottom
    }
    public enum RectCornerDirection
    {
        TopLeftCorner, TopRightCorner, BottomLeftCorner, BottomRightCorner
    }

    public struct BSize
    {
        public readonly int Width;
        public readonly int Height;
        public BSize(int w, int h)
        {
            this.Width = w;
            this.Height = h;
        }
        public static bool operator ==(BSize a, BSize b)
        {
            return a.Width == b.Width && a.Height == b.Height;
        }
        public static bool operator !=(BSize a, BSize b)
        {
            return !(a.Width == b.Width && a.Height == b.Height);
        }
        public override string ToString()
        {
            return string.Format("[Width = {0},Height = {1} ]", Width, Height);
        }
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
    public struct IntRectangle
    {
        public Point2D Location;
        public BSize Size;
        public IntRectangle(Point2D location, BSize size)
        {
            this.Location = location;
            this.Size = size;
        }
        public IntRectangle(int x, int y, int w, int h)
        {
            this.Location = new Point2D(x, y);
            this.Size = new BSize(w, h);
        }
        public static Point2D GetTopLeftCorner(Point2D loc, BSize size, RectCornerDirection d)
        {
            switch (d)
            {
                case RectCornerDirection.TopLeftCorner: return loc;
                case RectCornerDirection.TopRightCorner: return new Point2D(loc.X - size.Width, loc.Y);
                case RectCornerDirection.BottomLeftCorner: return new Point2D(loc.X, loc.Y - size.Height);
                case RectCornerDirection.BottomRightCorner: return new Point2D(loc.X - size.Width, loc.Y - size.Height);
            }
            throw new Exception();
        }

        public int X { get { return this.Location.X; } }
        public int Y { get { return this.Location.Y; } }
        public Point2D TopLeftCorner
        {
            get { return this.Location; }
        }
        public Point2D BottomLeftCorner
        {
            get { return new Point2D(this.Location.X, this.Location.Y + this.Size.Height - 1); }
        }
        public Point2D TopRightCorner
        {
            get { return new Point2D(this.Location.X + this.Size.Width - 1, this.Location.Y); }
        }
        public Point2D BottomRightCorner
        {
            get { return new Point2D(this.Location.X + this.Size.Width - 1, this.Location.Y + this.Size.Height - 1); }
        }
        public Point2D GetCorner(RectCornerDirection d)
        {
            switch (d)
            {
                case RectCornerDirection.TopLeftCorner: return this.TopLeftCorner;
                case RectCornerDirection.TopRightCorner: return this.TopRightCorner;
                case RectCornerDirection.BottomLeftCorner: return this.BottomLeftCorner;
                case RectCornerDirection.BottomRightCorner: return this.BottomRightCorner;
            }
            return Point2D.MinusEmpty;
        }

        public int Right
        {
            get
            {
                return this.Location.X + this.Size.Width - 1;
            }
        }
        public int Bottom { get { return this.Location.Y + this.Size.Height - 1; } }

        public int Left { get { return this.Location.X; } }
        public int Top { get { return this.Location.Y; } }
        public int Width { get { return this.Size.Width; } }
        public int Height { get { return this.Size.Height; } }

        public bool Contains(Point2D point)
        {
            return (this.Left <= point.X && point.X <= this.Right) && (this.Top <= point.Y && point.Y <= this.Bottom);
        }
    }

    public struct Range
    {
        public readonly int Left;
        public readonly int Right;
        public int Count
        {
            get { return this.Right - this.Left + 1; }
        }
        public Range(int left, int right)
        {
            this.Left = left;
            this.Right = right;
            if (this.Left - this.Right > 0) throw new Exception();
        }
        public override string ToString()
        {
            return string.Format("[{0},{1}]", this.Left, this.Right);
        }
        public IEnumerator<int> GetEnumerator()
        {

            for (int i = this.Left; i <= this.Right; i++)
                yield return i;
        }

        public IEnumerable<LeftRightPair<Range>> CreateSubRanges()
        {
            foreach (var middle in this)
            {
                if (middle != this.Right) yield return new LeftRightPair<Range>(new Range(this.Left, middle), new Range(middle + 1, this.Right));
            }
        }

    }
}
