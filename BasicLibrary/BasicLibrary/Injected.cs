﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Inject
{
    public abstract class Console
    {
        public static Console Current { get; set; }
        internal static void WriteLine(string s)
        {
            Current._WriteLine(s);
        }
        public abstract void _WriteLine(string text);
        public static Action<object> WriteLn { get; set; }
    }
}
