﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.IO;
namespace BSystem.Structure
{
    public class BitRLE
    {
        public byte[] Items { get; private set; }
        public int Length { get; private set; }
        
        public bool this[int i]
        {
            get
            {
                int n = 0;
                bool b = false;
                foreach (var f in Items)
                {
                    if (n <= i && i < n + f && f != 0) return b;
                    n += f;
                }
                throw new IndexOutOfRangeException();
            }
        }

        public virtual IEnumerator<bool> GetEnumerator()
        {
            bool b = false;
            for (int i = 0; i < this.Items.Length; i++)
            {
                for (int j = 0; i < this.Items[i]; j++) yield return b;                
                b = !b;
            }
        }
        public BitRLE(BitArray items)
        {
            this.Items = EncodeBitRLE(items);
            this.Length = this.Items.Sum((v) => v);
        }
        public string CreateRLECode()
        {
            var items = this.Items.Select((v) => string.Format("<{0}>", v));
            var str = string.Join(",", items);
            return str;
        }
        public void SaveBinary(StreamWriter stream)
        {
            stream.Write(this.Items);
        }
        public void Save(StreamWriter stream)
        {
            stream.Write(this.CreateRLECode());
        }

        private static byte[] EncodeBitRLE(BitArray items)
        {
            List<byte> r = new List<byte>();
            bool tmp = false;
            int len = 0;
            foreach (bool f in items)
            {
                if (tmp == f)
                {
                    len++;
                }
                else
                {
                    AddBytes(len, r);
                    tmp = !tmp;
                    len = 1;
                }
            }
            if (len != 0) AddBytes(len, r);
            return r.ToArray();
        }
        private static void AddBytes(int len,List<byte> r)
        {
            while (true)
            {
                if (len > 255)
                {
                    r.Add(byte.MaxValue);
                    len -= byte.MaxValue;
                }
                else
                {
                    r.Add((byte)len);
                    break;
                }
                r.Add(byte.MinValue);
            }
        }
        public BitArray CreateBitArray()
        {
            var r = new BitArray(this.Length);
            bool b = false;
            var t = 0;
            for (int i = 0; i < this.Items.Length; i++)
            {
                for (int j = 0; j < this.Items[i]; j++)
                {
                    r[t++] = b;
                }
                b = !b;
            }
            return r;
        }
    }
    public class BitmapRLE
    {
        public BitRLE[] Items { get; private set; }
        public BitmapRLE(BitArray[] items)
        {
            this.Items = items.Select((v)=>new BitRLE(v)).ToArray();
        }
        public string CreateRLECode()
        {
            var items = this.Items.Select((v)=>v.CreateRLECode());
            var str = string.Join(System.Environment.NewLine, items);
            return str;
        }
        public void Save(StreamWriter stream)
        {
            stream.Write(this.CreateRLECode());
        }
        public void SaveBinary(StreamWriter stream)
        {
            this.Items.ForEach((v)=>v.SaveBinary(stream));
        }
        public BitArray[] CreateBitArrays()
        {
            return this.Items.Select((v) => v.CreateBitArray()).ToArray();
        }
    }
}
