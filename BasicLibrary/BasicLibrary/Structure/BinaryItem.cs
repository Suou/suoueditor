﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Structure
{
    public interface IBinaryItem
    {
        byte[] Binarize();
    }
}
