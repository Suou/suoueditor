﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Xml.Serialization;
namespace BSystem.Structure.Tree
{



    public class PatriciaPointer<T> where T : TabunLabelNode<T>
    {
        public T ParentNode { get { return this.NextNode.Parent; } }
        public T NextNode { get; set; }
        public int Position { get; set; }


        public PatriciaPointer(T node)
        {
            this.NextNode = node;
            this.Position = 0;
        }
        public PatriciaPointer<T> Copy()
        {
            return new PatriciaPointer<T>(this.NextNode) { Position = Position };
        }
        public string UpperText
        {
            get { return this.NextNode.ParentLabelText.Substring(0, this.Position); }
        }
        public string BottomText
        {
            get { 
                
                return this.IsOnNextNode ? "" : this.NextNode.ParentLabelText.Substring(this.Position); 
            }
        }
        public void OneMoveOnNode(char? item)
        {
            this.NextNode = item.HasValue ? this.NextNode.Children.First((v) => v.ParentLabelText[0] == item.Value) : this.NextNode.Children.First();
            this.Position = 1;
        }
        public void MoveToNode(T node)
        {
            if (node == null) throw new Exception();
            this.NextNode = node;
            this.Position = node.ParentLabelText.Length;
        }
        public int DownToNode()
        {
            var n = this.NextNode.ParentLabelText.Length - this.Position;
            this.Position = this.NextNode.ParentLabelText.Length;
            return n;
        }

        public bool IsOnNextNode
        {
            get
            {
                return Position == this.NextNode.ParentLabelText.Length;
            }
        }
        public bool IsOnSigmaNode
        {
            get
            {
                return this.NextNode.IsSigmaNode;
            }
        }

        public override string ToString()
        {
            return string.Format("[Text = {0}, Pos = {1}, Sigma = {2}, Bottom = {3}", 
                this.NextNode.TextFromRootToNode, this.Position,this.IsOnSigmaNode,this.BottomText);
        }

    }
    public class ExpansivePatriciaPointer<T> : PatriciaPointer<T> where T : TabunLabelNode<T>
    {
        public ExpansivePatriciaPointer(T node)
            : base(node)
        {
        }
        public int GotoCount { get; set; }

        public Action<PatriciaPointer<T>, int> FailureAction { get; set; }
        public bool CanProceed(char c)
        {
            if (this.NextNode.IsSigmaNode)
            {
                return true;
            }
            else if (this.IsOnNextNode)
            {
                var p = this.NextNode.Children.FirstOrDefault((v) => v.ParentLabelText[0] == c);
                return p != null;
            }
            else
            {
                return (this.NextNode.ParentLabelText[Position] == c);
            }
        }
        
        public void Proceed(char c)
        {
            if (this.NextNode.IsSigmaNode)
            {
                this.OneMoveOnNode(null);
            }
            else if (this.IsOnNextNode)
            {
                this.OneMoveOnNode(c);
            }
            else
            {
                if (this.NextNode.ParentLabelText[Position] == c)
                {
                    this.Position++;
                }
                else
                {
                    throw new Exception();
                }
            }
        }
        public virtual void Goto(char c)
        {
            while (true)
            {
                BSystem.Inject.Console.WriteLine(c.ToString()+"/"+this.ToString());
                if (this.CanProceed(c))
                {
                    this.Proceed(c);
                    break;
                }
                else
                {
                    this.FailureAction(this, this.GotoCount);
                }
            }
            this.GotoCount++;
        }
    }
}
