﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
namespace BSystem.Structure
{


    public class Node : IBinaryTreeNode<char>,INode<char>
    {
        #region Property
        public IList<INode<char>> Children
        {
            get
            {
                if (this.Left != null && this.Right != null)
                {
                    return new List<INode<char>> { this.Left, this.Right };
                }
                else
                {
                    return new List<INode<char>> { };
                }
            }
        }
        public IList<Node> NodeChildren
        {
            get
            {
                if (this.Left != null && this.Right != null)
                {
                    return new List<Node> { this.Left, this.Right };
                }
                else
                {
                    return new List<Node> { };
                }
            }
        }

        public Node Left { get; set; }
        public Node Right { get; set; }
        public BasicTree ParentTree { get; private set; }
        public char Character { get; set; }
        public object Tag { get; set; }
        public char Item
        {
            get { return this.Character; }
        }
        public Point2D Region { get; set; }
        public Point2D Position
        {
            get
            {
                return new Point2D() { X = this.Region.X + (this.TreeWidth / 2), Y = (this.Region.Y) * (this.Height+50) };
            }
        }
        #endregion
        #region Constructor
        public Node(BasicTree parent)
        {
            this.ParentTree = parent;
        }
        public Node(Node leftNode, Node rightNode, BasicTree parent)
            : this(parent)
        {
            this.Left = leftNode;
            this.Right = rightNode;
        }
        public Node(Node leftNode, Node rightNode)
        {
            this.Left = leftNode;
            this.Right = rightNode;
        }
        public Node()
        {
        }
        #endregion
        public int Width
        {
            get
            {
                return 50;
            }
        }
        public int Height
        {
            get
            {
                return 50;
            }
        }
        public void CaliculateRegion()
        {
            if (this.Children.Count > 0)
            {
                var z = this.NodeChildren.SubSum((v) => v.TreeWidth).Take(this.Children.Count);
                var zy = this.NodeChildren.Zip(z);
                zy.ForEach((a) => a.Key.Region = new Point2D() { X = a.Value+this.Region.X, Y = this.Region.Y + 1 });
                this.NodeChildren.ForEach((v) => v.CaliculateRegion());
            }
        }

        public bool IsTop
        {
            get
            {
                return this.ParentTree.TopNodes.Contains(this);
            }
        }
        public int TreeWidth
        {
            get
            {
                if (this.IsLeaf)
                {
                    return this.Width;
                }
                else
                {
                    return this.NodeChildren.Sum((v) => v.TreeWidth);
                }
            }
        }
        public int TreeHeight
        {
            get
            {
                if (this.IsLeaf)
                {
                    return 1;
                }
                else
                {
                    return this.NodeChildren.Max((v) => v.TreeHeight) + 1;
                }
            }
        }

        public bool ContainChildNode(Node a)
        {
            if (this.IsLeaf) return false;
            return (this.Left == a) || (this.Right == a);
        }
        public IEnumerable<Node> DescendantIterator()
        {
            if (!this.IsLeaf)
            {
                foreach (var v in this.Left.DescendantIterator()) yield return v;
                foreach (var v in this.Right.DescendantIterator()) yield return v;
            }
        }
        #region Leaf
        public Node MostLeftLeaf
        {
            get
            {
                if (this.IsLeaf) return this;
                return this.Left.MostLeftLeaf;
            }
        }
        public Node MostRightLeaf
        {
            get
            {
                if (this.IsLeaf) return this;
                return this.Left.MostRightLeaf;
            }
        }
        public int LeftPosition
        {
            get
            {
                return this.ParentTree.Left(this);
            }
        }
        public int RightPosition
        {
            get
            {
                return this.ParentTree.Right(this);
            }
        }

        public bool IsLeaf
        {
            get
            {
                return (this.Left == null && this.Right == null);
            }
        }
        #endregion
        /*
        public int Height
        {
            get
            {
                if (this.IsLeaf)
                {
                    return 0;
                }
                else
                {
                    return Math.Max(this.Left.Height, this.Right.Height) + 1;
                }
            }
        }
        */
        #region IBinaryTreeNode
        public IBinaryTreeNode<char> LeftChild
        {
            get { return this.Left; }
            set { this.Left = (Node)value; }
        }
        public IBinaryTreeNode<char> RightChild
        {
            get { return this.Right; }
            set { this.Right = (Node)value; }
        }
        #endregion

        #region Operator
        public static bool operator <(Node a, Node b)
        {
            if (a.LeftPosition < b.LeftPosition)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public static bool operator >(Node a, Node b)
        {
            return !(a < b);
        }
        #endregion
    }
}
