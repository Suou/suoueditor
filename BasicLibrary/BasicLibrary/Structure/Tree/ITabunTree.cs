﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem.Structure
{
    /*
    public interface ITabunTreeNode<TAG> : System.Collections.IEnumerable
    {
        TAG IItem { get; set; }
        IEnumerable<ITabunTreeNode<TAG>> IChildren { get; set; }
    }
    public interface IBinaryTreeNode<TAG> : ITabunTreeNode<TAG>
    {
        IBinaryTreeNode<TAG> Left { get; set; }
    }
    public interface ISimpleTabunEdgeTreeNode<TAG> : System.Collections.IEnumerable
    {
    }
    */

    public interface IBinaryTreeNode<T> : INode<T>
    {
        IBinaryTreeNode<T> LeftChild { get; }
        IBinaryTreeNode<T> RightChild { get; }
    }
    public interface IBinaryTree<T>
    {
        IBinaryTreeNode<T> RootNode { get; }
    }

    public interface INode<T>
    {
        IList<INode<T>> Children { get; }
        T Item { get; }
    }


    public interface IPointerTreeNode<T> : INode<T>
    {
        IPointerTreeNode<T> PointerNode { get; set; }
    }

    public static class TreeExtension
    {
        public static IEnumerable<INode<T>> BFirstSearch<T>(this INode<T> node)
        {
            yield return node;
            foreach (var v in BFirstSearch<T>(node.Children)) { yield return v; };
        }
        private static IEnumerable<INode<T>> BFirstSearch<T>(IList<INode<T>> nodes)
        {
            IList<INode<T>> nowNodes = nodes;
            while (nowNodes.Count != 0)
            {
                foreach (var v in nowNodes) { yield return v; }
                nowNodes = nowNodes.SelectMany((v) => v.Children).ToList();
            }
        }
        public static IEnumerable<IBinaryTreeNode<T>> PreorderIterator<T>(this IBinaryTreeNode<T> node) 
        {
            Stack<KeyValuePair<IBinaryTreeNode<T>,bool>> stack = new Stack<KeyValuePair<IBinaryTreeNode<T>,bool>>();
            stack.Push(new KeyValuePair<IBinaryTreeNode<T>,bool>(node,false));
            while (stack.Count > 0)
            {
                var pop = stack.Pop();
                if (pop.Value)
                {
                    yield return pop.Key;
                    if (pop.Key.RightChild != null) stack.Push(new KeyValuePair<IBinaryTreeNode<T>, bool>(pop.Key.RightChild, false));
                }
                else
                {
                    stack.Push(new KeyValuePair<IBinaryTreeNode<T>, bool>(pop.Key, true));
                    if (pop.Key.LeftChild != null)
                    {
                        stack.Push(new KeyValuePair<IBinaryTreeNode<T>, bool>(pop.Key.LeftChild, false));
                    }
                }
            }
        }
        public static IEnumerable<IBinaryTreeNode<T>> PreorderIterator<T>(this IBinaryTree<T> tree)
        {
            if (tree.RootNode != null)
            {
                foreach (var v in tree.RootNode.PreorderIterator()) yield return v;
            }
        }
        

        public static IEnumerable<T> PreorderIteratorValue<T>(this IBinaryTree<T> tree)
        {
            if (tree.RootNode != null)
            {
                foreach (var v in tree.RootNode.PreorderIterator()) yield return v.Item;
            }
        }
    }
    /*
    public interface IChildren<T> where T : IChildren<T>
    {
        IEnumerable<T> GetChildren();
    }
    

    public static class IChildrenExtension
    {
        public static IEnumerable<T> PreorderIterator<T>(this T node) where T : IChildren<T>
        {
            yield return node;
            foreach (var v in node.GetChildren())
            {
                foreach (var w in v.PreorderIterator())
                {
                    yield return w;
                }
            }
        }
    }
    */
}
