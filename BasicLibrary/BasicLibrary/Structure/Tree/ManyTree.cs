﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Xml.Serialization;
namespace BSystem.Structure.Tree
{
    public class ManyNode : TabunNode<ManyNode>
    {
        public ManyNode():base()
        {
        }

        public string ViewString(int indent)
        {
            return this.View(indent).JoinLineString();
        }
        private string[] View(int indent)
        {
            string[] r = new string[this.Width];
            for (int i = 0; i < r.Length; i++) r[i] = "";
            //int p = 0;
            NameString(indent, r);
            KagiString(r);
            EachBlockString(r, indent);
            return r;
        }
        private void NameString(int indent, string[] r)
        {
            r[0] += string.Format("[{0}]", this.Number.ToString().PadLeft(indent, '0'));
            for (int i = 1; i < r.Length; i++) r[i] = r[i].PadLeft(indent + 2, ' ');
        }
        private void EachBlockString(string[] r, int indent)
        {
            var bls = this.BlockStarts;
            int p = 0;
            foreach (var v in this.Children)
            {
                foreach (var line in v.View(indent)) r[p++] += line;
            }
        }
        private void KagiString(string[] r)
        {

            var bls = this.BlockStarts;
            if (bls.Length == 0) return;
            int h = 0;
            for (int i = 0; i < r.Length; i++)
            {
                if (h >= bls.Length)
                {
                    r[i] += "      ";
                }
                else if (i == bls[h])
                {
                    r[i] += i != 0 ? string.Format("  |-{0}-", this.Children[h].CreateNodeText()) :
                        bls.Length == 1 ? string.Format("----{0}-", this.Children[h].CreateNodeText())
                        : string.Format("--|-{0}-", this.Children[h].CreateNodeText());
                    h++;
                }
                else
                {
                    r[i] += "  |   ";
                }
            }
        }
        private int[] BlockStarts
        {
            get
            {
                List<int> r = new List<int>();
                int p = 0;
                foreach (var v in this.Children)
                {
                    r.Add(p);
                    p += v.Width;
                }
                return r.ToArray();
            }
        }

        public static Action<ManyNode> ViewFunction { get; set; }
        public static Action<ManyNode,string> ViewFunction2 { get; set; }
        public void View()
        {
            if(ViewFunction != null)ViewFunction(this);
        }
        public void View(string fileName)
        {
            if (ViewFunction2 != null) ViewFunction2(this,fileName);
        }

        public static ManyNode operator +(ManyNode item1, ManyNode item2)
        {
            return Concatenate(new[] { item1, item2 });
        }
        public static ManyNode Concatenate(IEnumerable<ManyNode> trees)
        {
            var tree = new ManyNode();
            foreach (var v in trees)
            {
                tree.Children.Add(v);
            }
            return tree;
        }
    }
    public class TabunNode<T> where T : TabunNode<T>
    {
        public T Parent { get; set; }
        public int Number { get; set; }
        public List<T> Children { get; private set; }
        public object Tag { get; set; }
        public string CreateNodeText()
        {
            if (this.Tag == null)
            {
                return "";
            }
            else
            {
                return this.Tag.ToString();
            }
        }
        public TabunNode()
        {
            this.Children = new List<T>();
        }
        public int Width
        {
            get
            {
                return this.Children.Count == 0 ? 1 : this.Children.Sum((v) => v.Width);
            }
        }

        public System.Xml.Schema.XmlSchema GetSchema() { return null; }

        

        protected virtual void FieldWriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteStartElement("Number");
            writer.WriteValue(this.Number);
            writer.WriteEndElement();

            writer.WriteStartElement("Children");
            this.Children.ForEach((v) => v.WriteXml(writer));
            writer.WriteEndElement();

        }
        public virtual void WriteXml(System.Xml.XmlWriter writer)
        {

            writer.WriteStartElement("Node");
            {
                this.FieldWriteXml(writer);
            }
            writer.WriteEndElement();

        }
    }
    public static class TabunNodeExtension
    {
        public static IEnumerable<T> PreorderIterator<T>(T node) where T : TabunNode<T>
        {
            return IteratorExtension.PreorderIterator(node, (v) => v.Children);
        }


    }
    public abstract class TabunLabelNode<T> : TabunNode<T> where T : TabunLabelNode<T>
    {
        public abstract string ParentLabelText { get; set; }
        public virtual bool IsSigmaNode
        {
            get { return false; }
            set { }
        }
        public virtual string TextFromRootToNode
        {
            get
            {
                if (this.Parent == null) return this.ParentLabelText;
                return this.Parent.TextFromRootToNode + this.ParentLabelText;
            }
        }

        public string ViewString(int indent)
        {
            return this.View(indent).JoinLineString();
        }
        private string[] View(int indent)
        {
            string[] r = new string[this.Width];
            for (int i = 0; i < r.Length; i++) r[i] = "";
            //int p = 0;
            NameString(indent, r);
            KagiString(r);
            EachBlockString(r, indent);
            return r;
        }
        private void NameString(int indent, string[] r)
        {
            r[0] += string.Format("[{0}]", this.Number.ToString().PadLeft(indent, '0'));
            for (int i = 1; i < r.Length; i++) r[i] = r[i].PadLeft(indent + 2, ' ');
        }
        private void EachBlockString(string[] r, int indent)
        {
            var bls = this.BlockStarts;
            int p = 0;
            foreach (var v in this.Children)
            {
                foreach (var line in v.View(indent)) r[p++] += line;
            }
        }
        private void KagiString(string[] r)
        {

            var bls = this.BlockStarts;
            if (bls.Length == 0) return;
            int h = 0;
            for (int i = 0; i < r.Length; i++)
            {
                if (h >= bls.Length)
                {
                    r[i] += "      ";
                }
                else if (i == bls[h])
                {
                    r[i] += i != 0 ? string.Format("  |-{0}-", this.Children[h].ParentLabelText) :
                        bls.Length == 1 ? string.Format("----{0}-", this.Children[h].ParentLabelText)
                        : string.Format("--|-{0}-", this.Children[h].ParentLabelText);
                    h++;
                }
                else
                {
                    r[i] += "  |   ";
                }
            }
        }
        private int[] BlockStarts
        {
            get
            {
                List<int> r = new List<int>();
                int p = 0;
                foreach (var v in this.Children)
                {
                    r.Add(p);
                    p += v.Width;
                }
                return r.ToArray();
            }
        }
        protected override void FieldWriteXml(System.Xml.XmlWriter writer)
        {
            writer.WriteAttributeString("LabelText", this.ParentLabelText ?? "");
            base.FieldWriteXml(writer);
        }
    }


}
