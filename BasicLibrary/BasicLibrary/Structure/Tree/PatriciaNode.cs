﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Xml.Serialization;

namespace BSystem.Structure.Tree
{

    public class PatriciaNode : AbstractPatriciaNode<PatriciaNode>, IXmlSerializable
    {
        public PatriciaNode()
            : base()
        {
        }
        public PatriciaNode(string str)
            : this()
        {
            this._ParentLabelText = str;
        }
        public PatriciaNode(string[] items)
        {
        }
        public PatriciaNode SuffixLink { get; set; }

        public static KeyValuePair<string, string[]>[] Compact(string[] items)
        {
            throw new Exception();
        }

        #region XML

        protected override void FieldWriteXml(System.Xml.XmlWriter writer)
        {

            base.FieldWriteXml(writer);
        }
        public void FieldReadXml(System.Xml.XmlReader reader)
        {
            this.ParentLabelText = reader["LabelText"] ?? "";
            reader.ReadStartElement("Node");
            {

                //reader.MoveToAttribute("LabelText");

                reader.ReadStartElement("Number");
                this.Number = reader.ReadContentAsInt();
                reader.ReadEndElement();

                reader.ReadStartElement("Children");
                if (reader.IsStartElement("Node"))
                {
                    while (reader.IsStartElement("Node"))
                    {
                        this.Children.Add(PatriciaNode.Create(reader));
                    }
                    reader.ReadEndElement();
                }

            }
            reader.ReadEndElement();
        }
        private static PatriciaNode Create(System.Xml.XmlReader reader)
        {
            var p = new PatriciaNode();
            p.FieldReadXml(reader);
            return p;
        }

        public virtual void ReadXml(System.Xml.XmlReader reader)
        {

            reader.ReadStartElement("PatriciaNode");
            this.FieldReadXml(reader);
            reader.ReadEndElement();

        }
        #endregion
    }
}
