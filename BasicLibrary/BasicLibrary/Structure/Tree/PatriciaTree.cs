﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
using System.Xml.Serialization;
namespace BSystem.Structure.Tree
{
    public class PatriciaTree:PatriciaNode,IXmlSerializable
    {
        public string[] Items { get; set; }
        public PatriciaTree(string[] items)
        {
            this._ParentLabelText = "";
            this.Items = items;
            this.Items.ForEach((v) => this.Add(v,false));
        }
        public PatriciaTree()
        {
        }
        public override void ReadXml(System.Xml.XmlReader reader)
        {
            reader.ReadStartElement("PatriciaTree");
            this.FieldReadXml(reader);
            reader.ReadEndElement(); 
        }
        private void Add(string str,bool permitOneNode)
        {
            var pointer = new ExpansivePatriciaPointer<PatriciaNode>(this);
            for (int i = 0; i < str.Length; i++)
            {
                if (pointer.CanProceed(str[i]))
                {
                    pointer.Proceed(str[i]);
                    if (i == str.Length - 1 && (!pointer.IsOnNextNode && permitOneNode))
                    {
                        var p = new PatriciaNode(pointer.UpperText);
                        pointer.ParentNode.Children.Remove(pointer.NextNode);
                        pointer.ParentNode.Children.Add(p);
                        pointer.NextNode.ParentLabelText = pointer.BottomText;
                        p.Children.Add(pointer.NextNode);
                        break;
                    }
                }
                else
                {
                    if (pointer.IsOnNextNode)
                    {
                        var p = new PatriciaNode(str.Substring(i));
                        pointer.NextNode.Children.Add(p);
                        break;
                    }
                    else
                    {
                        var p = new PatriciaNode(pointer.UpperText);
                        pointer.ParentNode.Children.Remove(pointer.NextNode);
                        pointer.ParentNode.Children.Add(p);
                        var q = new PatriciaNode(str.Substring(i));
                        pointer.NextNode.ParentLabelText = pointer.BottomText;
                        p.Children.Add(q);
                        p.Children.Add(pointer.NextNode);
                        break;
                    }
                }
            }
        }
    }
    public class AbstractPatriciaNode<T> : TabunLabelNode<T> where T : AbstractPatriciaNode<T>
    {
        protected string _ParentLabelText;
        public override string ParentLabelText
        {
            get { return this._ParentLabelText; }
            set { this._ParentLabelText = value; }
        }
        
    }

}
