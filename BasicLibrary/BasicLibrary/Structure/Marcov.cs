﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem;
using BSystem.Collections;
using BSystem.Text;
namespace BSystem.Structure
{
    public class Marcov<Key> where Key: struct
    {
        private Collections.Specific.AddLink<Key> Items = new Collections.Specific.AddLink<Key>(10);
        public void Add(Key key)
        {
            this.Com = 4;
            this.Items.Add(key);
        }
        public int Com { get; set; }
        public TrieDictionary<Key, MarcovItem<Key>> Trie = new TrieDictionary<Key, MarcovItem<Key>>();
        public Marcov()
        {
            this.Items.Added += this.Added;
        }
        private void Added(Key key, EventArgs e)
        {
            if (this.Items.Count > this.Com)
            {
                var items = this.Items.LastTake(this.Com+1).Take(this.Com);
                if (!this.Trie.ContainsKey(items))this.Trie[items] = new MarcovItem<Key>();
                this.Trie[items][key]++;
            }
        }
        public override string ToString()
        {
            string s = "";
            foreach (var g in this.Trie.Keys)
            {
                s += string.Join("", g);
                s += this.Trie[g].ToString();
                s += System.Environment.NewLine;
            }
            return s;
        }
    }
    public class MarcovItem<Key> : Collections.Specific.NaiveDictionary<Key,int>
    {
        public override int this[Key key]
        {
            get
            {
                if (!this.ContainsKey(key)) base[key] = 0;
                return base[key];
            }
            set
            {
                base[key] = value;
            }
            
        }
    }
}
