﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BSystem.Collections;
using System.IO;
namespace BSystem.Structure
{
    public class RleStructure<Type> where Type : IComparable
    {
        public KeyValuePair<Type, int>[] Items { get; private set; }
        public int Length { get; private set; }
        

        public Type this[int i]
        {
            get
            {
                int n = 0;
                foreach (var f in Items)
                {
                    if (n <= i && i < n + f.Value) return f.Key;
                    n += f.Value;
                }
                throw new IndexOutOfRangeException();
            }
        }

        public virtual IEnumerator<Type> GetEnumerator()
        {
            foreach (var pair in this.Items)
            {
                for (int i = 0; i < pair.Value; i++) yield return pair.Key;
            }
        }
        public RleStructure(IEnumerable<Type> items)
        {
            this.Items = items.RunLengthEncoding().ToArray();
            this.Length = this.Items.Sum((v) => v.Value);
            //var b = this.Items.Pair().Any((a) => a.Key.Key.CompareTo(a.Value.Key) == 0);
            //if (b) throw new Exception();
        }
        public string CreateRLECode()
        {
            var items = this.Items.Select((v) => string.Format("<{0}/{1}>", v.Key, v.Value));
            var str = string.Join(",", items);
            return str;
        }
        public void Save(StreamWriter stream)
        {
            stream.Write(this.CreateRLECode());
        }
    }

}
