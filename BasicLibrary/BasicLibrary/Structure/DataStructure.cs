﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using BSystem.Structure.Tree;
namespace BSystem.Structure
{
    /*
    public interface ITreeNode
    {
        IList<Node> Children { get; }
    }
    */
    /*
    public interface ITrieNode
    {
        List<ITrieNodeEdge> IEdges { get; }
    }
    public interface ITrieNodeEdge
    {
        char Label { get; set; }
        ITrieNode INode { get; set; }
    }
    */

    public class BasicTree
    {
        public List<Node> Leaves { get; private set; }
        public List<Node> Items { get; private set; }
        public List<Node> TopNodes { get; private set; }

        public BasicTree()
        {
            this.Leaves = new List<Node>();
            this.Items = new List<Node>();
            this.TopNodes = new List<Node>();
        }
        public int Left(Node node)
        {
            return this.Leaves.IndexOf(node.MostLeftLeaf);
        }
        public int Right(Node node)
        {
            return this.Leaves.IndexOf(node.MostRightLeaf);
        }

        public bool IsAdjacent(Node a, Node b)
        {
            if (a < b)
            {
                return (b.LeftPosition - a.RightPosition == 1);
            }
            else
            {
                return (a.LeftPosition - b.RightPosition == 1);
            }
        }
        public Node Connect(Node a, Node b)
        {
            Node newNode = null;
            if (a < b)
            {
                newNode = new Node(a, b, this);
            }
            else
            {
                newNode = new Node(b, a, this);
            }
            this.TopNodes.Remove(a);
            this.TopNodes.Remove(b);
            this.TopNodes.Add(newNode);
            this.Items.Add(newNode);
            return newNode;
        }
    }
}
