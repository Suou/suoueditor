﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
namespace BSystem.Structure
{
    public class TrieDictionary<Key,Value> where Key : struct
    {
        private TrieDictionaryNode<Key, Value> Root = new TrieDictionaryNode<Key, Value>();
        private bool TryGetValue(Key key,out TrieDictionaryNode<Key,Value> node)
        {
            return this.Root.TryGetValue(key,out node);
        }
        private bool TryGetValue(IEnumerable<Key> items,out TrieDictionaryNode<Key,Value> node)
        {
            return this.Root.ContainsKey(items, out node);
        }
        private void EmptyAdd(Key key)
        {
            this.Root.EmptyAdd(key);
        }
        private void EmptyAdd(IEnumerable<Key> keys)
        {
            this.Root.EmptyAdd(keys);
        }
        public bool ContainsKey(IEnumerable<Key> items)
        {
            TrieDictionaryNode<Key, Value> node;
            var b = this.TryGetValue(items, out node);
            return b && node.HasValue;
        }
        public Value this[IEnumerable<Key> keys]
        {
            get
            {
                TrieDictionaryNode<Key, Value> p;
                var b = this.TryGetValue(keys,out p);
                if (!b) throw new Exception();
                if (!p.HasValue) throw new Exception();
                return p.Value;
                
            }
            set
            {
                var r = this.Root;
                foreach (var key in keys)
                {
                    r = r[key];
                }
                r.Value = value;
            }
        }
        public IEnumerable<IEnumerable<Key>> Keys
        {
            get
            {
                var r = new LinkedList<Key>();
                return this.Root.GetKeys(r);
                
            }
        }
    }
    public class TrieDictionaryNode<Key, Val> where Key : struct
    {
        public TrieDictionaryNode()
        {
            this.HasValue = false;
        }
        public TrieDictionaryNode(Val value)
        {
            this.HasValue = true;
            this.Value = value;
        }
        public TrieDictionaryNode<Key,Val> this[Key key]
        {
            get
            {
                TrieDictionaryNode<Key,Val> node;
                if (this.TryGetValue(key, out node))
                {
                    return node;
                }
                else
                {
                    return this.EmptyAdd(key);
                }

            }
            set
            {
                this.Children[key] = value;
            }
        }
        /*
        public IEnumerable<IEnumerable<Key>> Keys
        {
            get
            {
                var r = new LinkedList<Key>();
                return this.Root.GetKeys(r);

            }
        }
        */
        public IEnumerable<IEnumerable<Key>> GetKeys(LinkedList<Key> list)
        {
            if (this.Children.Items.Count() > 0)
            {
                foreach (var kv in this.Children.Items)
                {
                    list.AddLast(kv.Key);
                    foreach (var keys in kv.Value.GetKeys(list))
                    {
                        yield return keys;
                    }
                    list.RemoveLast();
                }
            }
            else
            {
                yield return list.ToArray();
            }
        }
        public Collections.Specific.NaiveDictionary<Key,TrieDictionaryNode<Key,Val>> Children = new Collections.Specific.NaiveDictionary<Key,TrieDictionaryNode<Key,Val>>();
        public bool HasValue { get; set; }
        private Val _Value;
        public Val Value
        {
            get { return this._Value; }
            set
            {
                this.HasValue = true;
                this._Value = value;
            }
        }
        public bool TryGetValue(Key key, out TrieDictionaryNode<Key, Val> node)
        {
            var p = this.Children.TryGetValue(key,out node);
            return p;
        }
        public bool ContainsKey(IEnumerable<Key> items, out TrieDictionaryNode<Key, Val> node)
        {
            node = this;

            foreach (var p in items)
            {
                if (!node.TryGetValue(p, out node)) break;
            }
            return node != null;
        }

        public TrieDictionaryNode<Key, Val> EmptyAdd(Key key)
        {

            var item = new TrieDictionaryNode<Key, Val>();
            var kv = new KeyValuePair<Key, TrieDictionaryNode<Key, Val>>(key, item);
            this.Children[key] = item;
            return item;

        }
        public void EmptyAdd(IEnumerable<Key> keys)
        {
            var r = this;
            foreach (var key in keys)
            {
                r = r.EmptyAdd(key);
            }
        }
    }


    /*
    public class TrieEdge : ITrieNodeEdge
    {
        public char Label { get; set; }
        public TrieNode Node { get; set; }
        public ITrieNode INode
        {
            get { return this.Node; }
            set { this.Node = (TrieNode)value; }
        }
    }
    public class TrieEdgeList : List<TrieEdge>
    {
        public TrieNode Parent { get; set; }
        public TrieEdgeList(TrieNode parent)
            : base()
        {
            this.Parent = parent;
        }
        public bool ContainLabel(char c)
        {
            return this.Any((v) => v.Label == c);
        }
        public TrieNode TryGetNode(char c)
        {
            var p = this.FirstOrDefault((v) => v.Label == c);
            if (p == null)
            {
                return null;
            }
            else
            {
                return (TrieNode)p.INode;
            }
        }

    }


    public class TrieNode : ITrieNode, INode
    {
        #region Property
        public TrieEdgeList Edges { get; set; }
        public List<ITrieNodeEdge> IEdges
        {
            get
            {
                return this.Edges.Select((v) => v).Cast<ITrieNodeEdge>().ToList();
            }
        }
        public IList<INode> Children
        {
            get
            {
                return this.Edges.Select((v) => v.Node).Cast<INode>().ToList();
            }
        }
        public TrieNode Parent { get; set; }

        public object Tag { get; set; }
        #endregion
        public TrieNode(TrieNode parent)
        {
            this.Parent = parent;
        }
        public bool IsLeaf
        {
            get
            {
                return this.IEdges.Count == 0;
            }
        }
    }
    */
}
