﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BSystem
{
    public static class LongExtension
    {
        public static IEnumerable<long> PlusCountUp(this long n)
        {
            for (long i = n; ; ++i)
            {
                yield return i;
            }
        }
        public static IEnumerable<long> MinusCountUp(this long n)
        {
            for (long i = n; ; --i)
            {
                yield return i;
            }
        }
    }

    public static class IntExtension
    {
        public static int Truncate(this int n, int m)
        {
            return (n / m) * m;
        }
        public static int FindIndexWidthCountUp(this int n, Func<int, bool> pred)
        {
            for (int i = n; ; i++)
            {
                if (pred(i)) return i;
            }
        }
        public static int FindIndexWidthCountDown(this int n, Func<int, bool> pred)
        {
            for (int i = n; ; i--)
            {
                if (pred(i)) return i;
            }
        }

        public static IEnumerable<int> PlusMinusCountUp()
        {
            for (int i = 0; ; ++i)
            {
                yield return i;
                if (i != 0)
                {
                    yield return -i;
                }
            }
        }
        public static IEnumerable<int> PlusCountUp()
        {
            for (int i = 0; ; ++i)
            {
                yield return i;
            }
        }
        public static int FirstMatchInclimentNumber(this int n, Func<int, bool> pred)
        {
            for (int i = n; ; ++i)
            {
                if (pred(i)) return i;
            }
        }
        public static int[] IntArray(int a, int b)
        {
            var r = new int[a - b + 1];
            for (int i = 0; i < b - a + 1; i++) r[i] = a + i;
            return r;
        }
        public static int[] PlusMinusIntArray(int diff)
        {
            return PlusMinusCountUp().Take(diff * 2 + 1).ToArray();
        }
        public static void ForEach(this int n, Action<int> pred)
        {
            for (int i = 0; i < n; i++)
            {
                pred(i);
            }
        }
        public static IEnumerable<T> Select<T>(this int n, Func<int, T> pred)
        {
            for (int i = 0; i < n; i++)
            {
                yield return pred(i);
            }
        }
        public static T Fold<T>(this int n, Func<T, T> pred, T First)
        {
            T r = First;
            for (int i = 0; i < n; i++)
            {
                r = pred(r);
            }
            return r;
        }
    }
}
