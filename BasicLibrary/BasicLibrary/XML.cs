﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;
using BSystem.Collections;
namespace BSystem
{
    public static class XMLExtension
    {
        public static void WriteTextElement(this XmlWriter writer, string name, string text)
        {
            writer.WriteStartElement(name);
            //writer.WriteCData(text);
            writer.WriteCData(System.Environment.NewLine + text);
            writer.WriteEndElement();
        }

    }
}
